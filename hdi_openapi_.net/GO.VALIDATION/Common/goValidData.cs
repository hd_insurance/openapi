﻿using System;
using System.Text.RegularExpressions;

namespace GO.VALIDATION.Common
{
    public class goValidData
    {
        #region Contructor
        private static readonly Lazy<goValidData> InitInstance = new Lazy<goValidData>(() => new goValidData());
        public static goValidData Instance => InitInstance.Value;
        #endregion
        #region Method 
        public bool IsValidEmail(string input)
        {
            Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            return regex.IsMatch(input);
        }
        #endregion
    }
}
