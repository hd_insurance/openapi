﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.VALIDATION.Common
{
    public class goValidDataType
    {
        #region Contructor
        private static readonly Lazy<goValidDataType> InitInstance = new Lazy<goValidDataType>(() => new goValidDataType());
        public static goValidDataType Instance => InitInstance.Value;
        #endregion

        #region Method

        public bool IsValidDouble(string input)
        {
            double temp;
            return double.TryParse(input, out temp);
        }

        public bool IsValidInt64(string input)
        {
            long temp;
            return long.TryParse(input, out temp);
        }

        public bool IsValidInt32(string input)
        {
            Int32 temp;
            return Int32.TryParse(input, out temp);
        }
        #endregion
    }
}
