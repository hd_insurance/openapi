﻿using FluentValidation.Results;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.VALIDATION.FluentValidator
{
    public class FluentValidatorModels
    {
        #region Variable
        #endregion

        #region Contructor
        private static readonly Lazy<FluentValidatorModels> InitInstance = new Lazy<FluentValidatorModels>(() => new FluentValidatorModels());
        public static FluentValidatorModels Instance => InitInstance.Value;
        #endregion

        #region Method

        public TokenAuthorizationInfo ValidatorRequest(BaseRequest input)
        {
            TokenAuthorizationInfo result = new TokenAuthorizationInfo();
            result.Sucess = true;
            try
            {
                RequestValidator requestValidator = new RequestValidator();
                ValidationResult resultValidation = requestValidator.Validate(input);
                if (!resultValidation.IsValid)
                {
                    List<object> lstErrors = new List<object>();
                    foreach (ValidationFailure failer in resultValidation.Errors)
                    {
                        object obj = new
                        {
                            Name = failer.PropertyName,
                            ErrorMessage = failer.ErrorMessage
                        };
                        lstErrors.Add(obj);
                    }

                    result.Sucess = false;
                    result.ErrorMessage = JsonConvert.SerializeObject(lstErrors);
                }
            }
            catch (Exception ex)
            {
                result.Sucess = false;
                result.ErrorMessage = goConstantsError.Instance.ERROR_2006;
            }
            
            return result;
        }

        #endregion
    }
}
