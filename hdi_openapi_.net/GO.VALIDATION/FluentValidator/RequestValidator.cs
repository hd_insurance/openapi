﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentValidation;
using GO.CONSTANTS;
using GO.DTO.Base;

namespace GO.VALIDATION.FluentValidator
{
    public class RequestValidator : AbstractValidator<BaseRequest>
    {
        #region Nuget
        // FluentValidation 9.0.0
        #endregion

        public RequestValidator()
        {
            string[] arrEnvironmentCode = { "WEB", "APP" };
            When(x => x != null, () =>
            {
                RuleFor(m => m.Device).NotNull().WithMessage(goConstantsError.Instance.NULL_DEVICE);
                When(i => i.Device != null, () =>
                {
                    RuleFor(m => m.Device.DeviceId).NotNull().WithMessage(goConstantsError.Instance.NULL_DEVICE_ID);
                    RuleFor(m => m.Device.DeviceCode).NotNull().WithMessage(goConstantsError.Instance.NULL_DEVICE_CODE);
                    RuleFor(m => m.Device.IpPrivate).NotNull().WithMessage(goConstantsError.Instance.NULL_DEVICE_IP_PRIVATE);
                    RuleFor(m => Array.Exists(arrEnvironmentCode, j => j.Equals(m.Device.DeviceEnvironment)) ? m.Device.DeviceEnvironment : null).NotNull().WithMessage(goConstantsError.Instance.NOT_IN_ENVIRONMENT_CODE);
                });
                RuleFor(m => m.Action).NotNull().WithMessage(goConstantsError.Instance.NULL_ACTION);
                When(i => i.Action != null, () =>
                {
                    RuleFor(m => m.Action.ActionCode).NotNull().WithMessage(goConstantsError.Instance.NULL_ACTION_CODE);
                    RuleFor(m => m.Action.UserName).NotNull().WithMessage(goConstantsError.Instance.NULL_ACTION_USER_NAME);
                    RuleFor(m => m.Action.ParentCode).NotNull().WithMessage(goConstantsError.Instance.NULL_ACTION_PARTNER_CODE);
                    RuleFor(m => m.Action.Secret).NotNull().WithMessage(goConstantsError.Instance.NULL_ACTION_SECRET);
                });
            });
        }
    }
}
