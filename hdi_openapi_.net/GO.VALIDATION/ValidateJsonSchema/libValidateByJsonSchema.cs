﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace GO.VALIDATION.ValidateJsonSchema
{
    public class libValidateByJsonSchema
    {
        #region Nuget
        //Newtonsoft.Json.Schema v 3.0.14
        #endregion

        #region Contructor
        private static readonly Lazy<libValidateByJsonSchema> InitInstance = new Lazy<libValidateByJsonSchema>(() => new libValidateByJsonSchema());
        public static libValidateByJsonSchema Instance => InitInstance.Value;
        #endregion

        #region Method
        public void Test(object objSchema, object objValid)
        {
            try
            {
                var a = "";
                string b = "{\"ORG_CODE\":\"\",\"CHANNEL\":\"\",\"USERNAME\":\"\",\"ACTION\":\"BH_M\",\"BUYER\":{\"TYPE\":\"\",\"NAME\":\"\",\"DOB\":\"\",\"GENDER\":\"\",\"ADDRESS\":\"\",\"IDCARD\":\"\",\"EMAIL\":\"\",\"PHONE\":\"\",\"PROV\":\"\",\"DIST\":\"\",\"WARDS\":\"\",\"IDCARD_D\":\"\",\"IDCARD_P\":\"\",\"FAX\":\"\",\"TAXCODE\":\"\"},\"PAY_INFO\":{\"PAYMENT_TYPE\":\"TH\"},\"VEHICLE_INSUR\":[{\"TYPE\":\"\",\"NAME\":\"\",\"DOB\":\"\",\"GENDER\":\"\",\"ADDRESS\":\"\",\"IDCARD\":\"\",\"EMAIL\":\"\",\"PHONE\":\"\",\"PROV\":\"\",\"DIST\":\"\",\"WARDS\":\"\",\"IDCARD_D\":\"\",\"IDCARD_P\":\"\",\"FAX\":\"\",\"TAXCODE\":\"\",\"RELATIONSHIP\":\"BAN_THAN\",\"IS_SPLIT_ORDER\":\"\",\"VEHICLE_GROUP\":\"XE_MAY\",\"VEHICLE_TYPE\":\"XE_50CC\",\"VEHICLE_USE\":\"KKD\",\"NONE_NUMBER_PLATE\":\"0\",\"NUMBER_PLATE\":\"\",\"SEAT_NO\":2,\"SEAT_CODE\":\"SEAT02\",\"WEIGH_CODE\":\"\",\"MFG\":\"\",\"CAPACITY\":\"\",\"REF_VEHICLE_VALUE\":\"\",\"VEHICLE_REGIS\":\"\",\"CHASSIS_NO\":null,\"ENGINE_NO\":null,\"WEIGH\":\"\",\"BRAND\":\"\",\"MODEL\":\"\",\"IS_VOLUNTARY\":\"0\",\"IS_DRIVER\":\"1\",\"IS_CARGO\":\"0\",\"IS_COMPULSORY\":\"1\",\"IS_VOLUNTARY_ALL\":\"1\",\"IS_PHYSICAL\":\"0\",\"FEES_DATA\":80500,\"FEES\":75000,\"AMOUNT\":75000,\"VAT\":5500,\"TOTAL_DISCOUNT\":0,\"TOTAL_AMOUNT\":80500,\"COMPULSORY_CIVIL\":{\"PRODUCT_CODE\":\"XCG_TNDSBB\",\"PACK_CODE\":\"TNDSBB\",\"IS_SERIAL_NUM\":\"0\",\"SERIAL_NUM\":null,\"EFF\":\"\",\"TIME_EFF\":\"\",\"EXP\":\"\",\"TIME_EXP\":\"\",\"FEES_DATA\":60500,\"FEES\":55000,\"AMOUNT\":55000,\"VAT\":5500,\"TOTAL_DISCOUNT\":0,\"TOTAL_AMOUNT\":60500},\"VOLUNTARY_CIVIL\":{\"PRODUCT_CODE\":\"\",\"PACK_CODE\":\"\",\"INSUR_3RD\":\"\",\"INSUR_3RD_ASSETS\":\"\",\"INSUR_PASSENGER\":\"\",\"TOTAL_LIABILITY\":\"\",\"DRIVER_NUM\":\"2\",\"DRIVER_INSUR\":\"NTX10\",\"WEIGHT_CARGO\":\"\",\"CARGO_INSUR\":\"\",\"FEES_DATA\":20000,\"FEES\":20000,\"AMOUNT\":20000,\"VAT\":0,\"TOTAL_DISCOUNT\":0,\"TOTAL_AMOUNT\":20000}}]}";
                //JsonSchema schema = JsonSchema.Parse(JsonConvert.SerializeObject(objSchema));
                //JObject employee = JObject.Parse(JsonConvert.SerializeObject(objValid));
                JsonSchema schema = JsonSchema.Parse(JsonConvert.SerializeObject(JsonConvert.DeserializeObject(a)));
                JObject employee = JObject.Parse(b);
                IList<string> messages;
                bool valid1 = employee.IsValid(schema, out messages);
            }
            catch (Exception)
            {
                
            }
        }
        #endregion
    }
}
