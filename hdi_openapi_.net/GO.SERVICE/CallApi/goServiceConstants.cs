﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.SERVICE.CallApi
{
    public class goServiceConstants
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goServiceConstants> _instance = new Lazy<goServiceConstants>(() =>
        {
            return new goServiceConstants();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goServiceConstants Instance { get => _instance.Value; }
        #endregion

        #region Variables
        public enum MethodApi
        {
            POST,
            PUT,
            GET,
            DELETE
        }

        public enum TypeBody
        {
            none,
            form_data,
            form_urlendcoded,
            raw
        }

        public enum TypeRaw
        {
            none,
            text,
            javascript,
            json,
            html,
            xml
        }
        #endregion

        #region Class
        public class KeyVal
        {
            public string key { get; set; }
            public string val { get; set; }
        }

        public class Byte_File
        {
            public byte[] byteFile { get; set; }
            public string name { get; set; }
        }
        #endregion
    }
}
