﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static GO.SERVICE.CallApi.goServiceConstants;

namespace GO.SERVICE.CallApi
{
    public class goServiceInvoke
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goServiceInvoke> _instance = new Lazy<goServiceInvoke>(() =>
        {
            return new goServiceInvoke();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goServiceInvoke Instance { get => _instance.Value; }
        #endregion

        #region Method
        public async Task<HttpResponseMessage> Invoke(string url, goServiceConstants.MethodApi method, goServiceConstants.TypeBody typeBody, goServiceConstants.TypeRaw typeRaw, List<KeyVal> lstHeader, List<KeyVal> lstParam, object body)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage();
                SetUri(ref request, url, lstParam);
                SetMethod(ref request, method);
                SetHeaderDefault(ref request);
                SetHeader(ref request, lstHeader);
                switch (typeBody)
                {
                    case TypeBody.form_data:
                        break;
                    case TypeBody.form_urlendcoded:
                        break;
                    case TypeBody.raw:
                        switch (typeRaw)
                        {
                            case TypeRaw.text:
                                SetBodyRawText(ref request, body);
                                break;
                            case TypeRaw.javascript:
                                break;
                            case TypeRaw.json:
                                SetBodyRawJson(ref request, body);
                                break;
                            case TypeRaw.html:
                                break;
                            case TypeRaw.xml:
                                SetBodyRawXml(ref request, body);
                                break;
                        }
                        break;
                    case TypeBody.none:
                        break;
                }
                HttpResponseMessage result;
                result = InvokeApi(request).Result;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<HttpResponseMessage> Invoke(string url, goServiceConstants.MethodApi method, goServiceConstants.TypeBody typeBody, goServiceConstants.TypeRaw typeRaw, List<KeyVal> lstHeader, string param, object body)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage();
                SetUri(ref request, url, param);
                SetMethod(ref request, method);
                SetHeaderDefault(ref request);
                SetHeader(ref request, lstHeader);
                switch (typeBody)
                {
                    case TypeBody.form_data:
                        break;
                    case TypeBody.form_urlendcoded:
                        break;
                    case TypeBody.raw:
                        switch (typeRaw)
                        {
                            case TypeRaw.text:
                                SetBodyRawText(ref request, body);
                                break;
                            case TypeRaw.javascript:
                                break;
                            case TypeRaw.json:
                                SetBodyRawJson(ref request, body);
                                break;
                            case TypeRaw.html:
                                break;
                            case TypeRaw.xml:
                                SetBodyRawXml(ref request, body);
                                break;
                        }
                        break;
                    case TypeBody.none:
                        break;
                }
                HttpResponseMessage result;
                result = InvokeApi(request).Result;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<HttpResponseMessage> Invoke(string url, goServiceConstants.MethodApi method, goServiceConstants.TypeBody typeBody, goServiceConstants.TypeRaw typeRaw, List<KeyVal> lstHeader, string param, object body, List<Byte_File> byte_Files)
        {
            try
            {
                HttpRequestMessage request = new HttpRequestMessage();
                SetUri(ref request, url, param);
                SetMethod(ref request, method);
                SetHeaderDefault(ref request);
                SetHeader(ref request, lstHeader);
                switch (typeBody)
                {
                    case TypeBody.form_data:
                        SetBodyFileFormData(ref request, byte_Files);
                        break;
                    case TypeBody.form_urlendcoded:
                        break;
                    case TypeBody.raw:
                        switch (typeRaw)
                        {
                            case TypeRaw.text:
                                SetBodyRawText(ref request, body);
                                break;
                            case TypeRaw.javascript:
                                break;
                            case TypeRaw.json:
                                SetBodyRawJson(ref request, body);
                                break;
                            case TypeRaw.html:
                                break;
                            case TypeRaw.xml:
                                SetBodyRawXml(ref request, body);
                                break;
                        }
                        break;
                    case TypeBody.none:
                        break;
                }
                HttpResponseMessage result;
                result = InvokeApi(request).Result;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetUri(ref HttpRequestMessage request, string url, string param)
        {
            try
            {
                UriBuilder builder = new UriBuilder(url);
                if (!string.IsNullOrEmpty(param)) builder.Query = param;
                request.RequestUri = builder.Uri;//new Uri(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetUri(ref HttpRequestMessage request, string url, List<KeyVal> lstParam)
        {
            try
            {
                string param = GetParamsByList(lstParam);
                SetUri(ref request, url, param);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetParamsByList(List<KeyVal> lstParam)
        {
            try
            {
                string param = "";
                if (lstParam != null && lstParam.Any())
                {
                    foreach (var item in lstParam)
                    {
                        if (string.IsNullOrEmpty(param))
                            param = item.key + "=" + item.val;
                        else
                            param = param + "&" + item.key + "=" + item.val;
                    }
                }
                return param;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public void SetMethod(ref HttpRequestMessage request, goServiceConstants.MethodApi method)
        {
            switch (method)
            {
                case goServiceConstants.MethodApi.GET:
                    request.Method = HttpMethod.Get;
                    break;
                case goServiceConstants.MethodApi.POST:
                    request.Method = HttpMethod.Post;
                    break;
                case goServiceConstants.MethodApi.PUT:
                    request.Method = HttpMethod.Put;
                    break;
                case goServiceConstants.MethodApi.DELETE:
                    request.Method = HttpMethod.Delete;
                    break;
            }
        }

        public void SetHeader(ref HttpRequestMessage request, List<KeyVal> lst)
        {
            try
            {
                //request.Headers.Clear();
                if (lst != null && lst.Any())
                {
                    foreach (var item in lst)
                    {
                        try
                        {
                            request.Headers.Add(item.key, item.val);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetHeaderDefault(ref HttpRequestMessage request)
        {
            try
            {
                request.Headers.Clear();
                request.Headers.Add("User-Agent", "HDI");
            }
            catch (Exception ex)
            {
            }
        }

        public void SetBodyRawJson(ref HttpRequestMessage request, object input)
        {
            try
            {
                if (input != null) request.Content = new StringContent(input.ToString(), Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetBodyRawText(ref HttpRequestMessage request, object input)
        {
            try
            {
                if (input != null) request.Content = new StringContent(input.ToString(), Encoding.UTF8, "text/plain");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetBodyRawXml(ref HttpRequestMessage request, object input)
        {
            try
            {
                if (input != null) request.Content = new StringContent(input.ToString(), Encoding.UTF8, "text/xml");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetBodyFileFormData(ref HttpRequestMessage request, List<Byte_File> byte_Files)
        {
            try
            {
                MultipartFormDataContent form = new MultipartFormDataContent();
                if (byte_Files != null && byte_Files.Any())
                {
                    var content = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    foreach (var item in (byte_Files))
                    {
                        content.Add(new StreamContent(new MemoryStream(item.byteFile)), "files", item.name);

                        //form.Add(new ByteArrayContent(item.byteFile, 0, item.byteFile.Length), "profile_pic", item.name);
                    }
                    request.Content = content;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<HttpResponseMessage> InvokeApi(HttpRequestMessage request)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                HttpResponseMessage result;
                HttpClient httpClient = new HttpClient();
                result = httpClient.SendAsync(request).Result;
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
