﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using com.itextpdf.text.pdf.security;
using iTextSharp.text;
using iTextSharp.text.io;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System.Security.Cryptography.X509Certificates;
using GO.DTO.Service;
using System.IO;
using EvoPdf.PdfToText;
using EvoWordToPdf;
using EvoPdf;
using System.Drawing;
using System.Windows.Forms;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Math;

namespace GO.SERVICE.Sign
{
    public class LibSign
    {
        public string strPathSign { get; set; }
        public string strPathTemplate { get; set; }
        public string pathFont { get; set; }
        public string pRoot { get; set; }
        public string pathCer { get; set; }
        private static string HASH_SHA1 = "SHA1";
        private static string CRYPT_RSA = "RSA";
        //mã hóa
        public static string Encrypt(string toEncrypt)
        {
            string key = "EvnitNApas2018";
            byte[] keyArray;
            bool useHashing = true;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock
                    (toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            string strReturn = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            strReturn = strReturn.Replace("+", "___");
            strReturn = strReturn.Replace("/", "aaaa_1");
            //strReturn = strReturn.Replace("=", "bbbb_1"); 
            return strReturn;
        }
        public static string EncryptCA(string toEncrypt)
        {
            string key = "HDinsur@nce@)@)";
            byte[] keyArray;
            bool useHashing = true;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt+"12345678");
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock
                    (toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            string strReturn = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            strReturn = strReturn.Replace("+", "___");
            strReturn = strReturn.Replace("/", "aaaa_1");
            //strReturn = strReturn.Replace("=", "bbbb_1"); 
            return strReturn;
        }
        public static string Decrypt(string cipherString)
        {
            string para = cipherString;
            para = para.Replace("___", "+");
            para = para.Replace("aaaa_1", "/");
            //para = para.Replace("bbbb_1", "=");
            string key = "HDinsur@nce@)@)";
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(para);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock
                    (toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public bool veryfiPass(string strPass)
        {
            try
            {
                string strPassWord = Decrypt(strPass);
                if (strPassWord.IndexOf("12345678") > 0)
                {
                    return true;
                }
                else
                    return false;
            }catch(Exception ex)
            {
                return false;
            }
            
        }

        public byte[] convertDocToPDF(string strPathDoc)
        {
            try
            {
                //Cursor.Current = Cursors.WaitCursor;
                WordToPdfConverter wordToPdfConverter = new WordToPdfConverter();
                wordToPdfConverter.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                wordToPdfConverter.PdfDocumentOptions.Width = EvoWordToPdf.PdfPageSize.A4.Width;
                wordToPdfConverter.PdfDocumentOptions.Height = EvoWordToPdf.PdfPageSize.A4.Height;
                //wordToPdfConverter.PdfHeaderOptions.HeaderHeight = 60;
                //EvoWordToPdf.ImageElement ilement = EvoWordToPdf.ImageElement(imageTiles);
                //wordToPdfConverter.PdfHeaderOptions.AddElement(imageTiles);
                
                wordToPdfConverter.PdfDocumentOptions.RightMargin = -10;
                
                return wordToPdfConverter.ConvertWordFile(strPathDoc);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
      
        public byte[] convertHtmlToPDF(string strPathHTML)
        {
            try
            {
                HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();
                htmlToPdfConverter.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                htmlToPdfConverter.PdfDocumentOptions.PdfPageSize = EvoPdf.PdfPageSize.A4;
                htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = EvoPdf.PdfPageOrientation.Portrait;
                htmlToPdfConverter.NavigationTimeout = 60;
                htmlToPdfConverter.ConversionDelay = 2;
                htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 40f;
                htmlToPdfConverter.PdfDocumentOptions.TopMargin = 30f;
                htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 50f;
                htmlToPdfConverter.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR" };
                htmlToPdfConverter.PdfDocumentOptions.AvoidImageBreak = true;
                htmlToPdfConverter.PdfDocumentOptions.AvoidTextBreak = true;
                return htmlToPdfConverter.ConvertUrl(strPathHTML);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #region Lib a Khoa
        public LibSign(string pathRoot)
        {
            this.strPathSign = pathRoot + @"/UploadCA/";
            this.strPathTemplate = pathRoot + @"/Template/";
            this.pathFont = pathRoot + @"/SignCA/fonts/font.ttf";
            this.pathCer = pathRoot + @"/SignCA/hdi.cer";
            this.pRoot = pathRoot;
        }

        public List<CerInforModel> getInforInFileSign(byte[] bFileSign)
        {
            try
            {
                PdfReader reader = new PdfReader(bFileSign);
                AcroFields af = reader.AcroFields;
                var names = af.GetSignatureNames();

                if (names.Count == 0)
                {
                    throw new InvalidOperationException("No Signature present in pdf file.");
                }
                List<CerInforModel> lsInfo = new List<CerInforModel>();
                foreach (string name in names)
                {
                    CerInforModel info = new CerInforModel();
                    if (!af.SignatureCoversWholeDocument(name))
                    {
                        throw new InvalidOperationException(string.Format("The signature: {0} does not covers the whole document.", name));
                    }

                    PdfPKCS7 pk = af.VerifySignature(name);
                    var cal = pk.SignDate;
                    var pkc = pk.Certificates;
                    pkc = pk.SignCertificateChain;
                    Org.BouncyCastle.X509.X509Certificate cer = pkc[0];
                    System.Security.Cryptography.X509Certificates.X509Certificate2 cert = new System.Security.Cryptography.X509Certificates.X509Certificate2();
                    cert.Import(cer.GetEncoded());                    
                    if (!pk.Verify())
                    {
                        throw new InvalidOperationException("The signature could not be verified.");
                    }
                    info.DATE_AFTER = cert.NotAfter.Date;
                    info.DATE_BEFORE = cert.NotBefore.Date;
                    info.DATE_SIGN = pk.SignDate;
                    info.ISSUER = getValueByKey(cert.Issuer.ToString(), "CN=");
                    info.ISSUER_FULL = cert.Issuer.ToString();
                    info.NAME = getValueByKey(cert.SubjectName.Name.ToString(), "CN=");
                    info.NAME_FULL = cert.SubjectName.Name.ToString();
                    info.PUBLICKEY = ((RSACryptoServiceProvider)cert.PublicKey.Key).ToXmlString(false);
                    info.SERIAL = cert.GetSerialNumberString();
                    info.FIELD_NAME = name;
                    lsInfo.Add(info);
                }
                return lsInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string getValueByKey(string name1, string serach)
        {
            try
            {
                int index = name1.IndexOf(serach);
                if (index > -1)
                {
                    name1 = name1.Substring(index + serach.Length);
                    if (name1.IndexOf(",") > 0)
                    {
                        name1 = name1.Substring(0, name1.IndexOf(","));
                    }
                    return name1;
                }
                name1 = "";
            }
            catch
            {
                return name1;
            }
            return name1;
        }
        public string signPDF(SignFileModel sign, string strPass, string strSerialEx)
        {
            try
            {
                if (!veryfiPass(strPass))
                {
                    throw new Exception("Sai pass đăng nhập chứng thư số!");
                }
                strPass = "12345678";
                INSERT_HASH lstIns = new INSERT_HASH();
                //lấy cer cho sign
                string[] strCer = getCerBase64Async(sign.DEVICE, strPass, strSerialEx, sign.CER);
                if (strCer == null || strCer[0].Equals(""))
                {
                    throw new Exception("Lỗi không lấy được CTS của " + strSerialEx);
                }
                if (createHash_PDF(sign, ref lstIns))
                {
                    lstIns.HASH_SIGN_ED = signHash_PDF(sign.DEVICE, lstIns.HASH_SIGN, strPass, strSerialEx, sign.detail.MESSAGE, sign.F12_FILE);
                    return insertHash_PDF(lstIns);
                }
                else
                {
                    throw new Exception("Lỗi tạo hash");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string signPDFBack(SignFileModel sign, string strPass, string strSerialEx, DateTime dt)
        {
            try
            {
                if (!veryfiPass(strPass))
                {
                    throw new Exception("Sai pass đăng nhập chứng thư số!");
                }
                strPass = "12345678";
                INSERT_HASH lstIns = new INSERT_HASH();
                //lấy cer cho sign
                string[] strCer = getCerBase64Async(sign.DEVICE, strPass, strSerialEx, sign.CER);
                if (strCer == null || strCer[0].Equals(""))
                {
                    throw new Exception("Lỗi không lấy được CTS của " + strSerialEx);
                }
                if (createHash_PDFBack(sign, ref lstIns,dt))
                {
                    lstIns.HASH_SIGN_ED = signHash_PDF(sign.DEVICE, lstIns.HASH_SIGN, strPass, strSerialEx, sign.detail.MESSAGE, sign.F12_FILE);
                    return insertHash_PDF(lstIns);
                }
                else
                {
                    throw new Exception("Lỗi tạo hash");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool createHash_PDF(SignFileModel sign, ref INSERT_HASH ins)
        {
            try
            {

                Org.BouncyCastle.X509.X509Certificate cert = GetX509Cert(sign.CER);
                Org.BouncyCastle.X509.X509Certificate[] chain = new Org.BouncyCastle.X509.X509Certificate[] { cert };
                ins.dtSign = DateTime.Now;
                if (sign.detail.FIELD_NAME == null || sign.detail.FIELD_NAME == "" || sign.detail.FIELD_NAME == "null")
                    ins.FIELD_NAME = sign.UserName + "_" + ins.dtSign.ToString("dd_MM_yyyy") + DateTime.Now.Millisecond.ToString();
                else
                    ins.FIELD_NAME = sign.detail.FIELD_NAME;
                ins.chainBase64 = sign.CER;
                string strGuiID_File = Guid.NewGuid().ToString("N");
                string strPath_file = strPathSign + strGuiID_File  + ins.FIELD_NAME + ".pdf";
                string strPath_file_signed = strPathSign + strGuiID_File + ins.FIELD_NAME + "ed.pdf";
                ins.PATH_TEMP = strPath_file_signed;
                ins.PATH_FILE = strPath_file;
                if (createHash(sign.detail, Convert.FromBase64String(sign.FILE_SIGN), ins.PATH_TEMP, ins.FIELD_NAME, cert, ins.dtSign, pathFont, sign.CER))
                {
                    List<byte[]> hash = preSign(ins.PATH_TEMP, ins.FIELD_NAME, chain, ins.dtSign);
                    if (hash.Count > 0)
                    {
                        ins.HASH = hash[1];
                        ins.HASH_SIGN = Convert.ToBase64String(encodeData(hash[0], "SHA1"));
                        //return Convert.ToBase64String(encodeData(hash[0], "SHA1"));
                    }

                }
                else
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool createHash_PDFBack(SignFileModel sign, ref INSERT_HASH ins, DateTime dt)
        {
            try
            {

                Org.BouncyCastle.X509.X509Certificate cert = GetX509Cert(sign.CER);
                Org.BouncyCastle.X509.X509Certificate[] chain = new Org.BouncyCastle.X509.X509Certificate[] { cert };
                ins.dtSign = dt;
                if (sign.detail.FIELD_NAME == null || sign.detail.FIELD_NAME == "" || sign.detail.FIELD_NAME == "null")
                    ins.FIELD_NAME = sign.UserName + "_" + ins.dtSign.ToString("dd_MM_yyyy") + DateTime.Now.Millisecond.ToString();
                else
                    ins.FIELD_NAME = sign.detail.FIELD_NAME;
                ins.chainBase64 = sign.CER;
                string strGuiID_File = Guid.NewGuid().ToString("N");
                string strPath_file = strPathSign + strGuiID_File + ins.FIELD_NAME + ".pdf";
                string strPath_file_signed = strPathSign + strGuiID_File + ins.FIELD_NAME + "ed.pdf";
                ins.PATH_TEMP = strPath_file_signed;
                ins.PATH_FILE = strPath_file;
                if (createHash(sign.detail, Convert.FromBase64String(sign.FILE_SIGN), ins.PATH_TEMP, ins.FIELD_NAME, cert, ins.dtSign, pathFont, sign.CER))
                {
                    List<byte[]> hash = preSign(ins.PATH_TEMP, ins.FIELD_NAME, chain, ins.dtSign);
                    if (hash.Count > 0)
                    {
                        ins.HASH = hash[1];
                        ins.HASH_SIGN = Convert.ToBase64String(encodeData(hash[0], "SHA1"));
                        //return Convert.ToBase64String(encodeData(hash[0], "SHA1"));
                    }

                }
                else
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string insertHash_PDF(INSERT_HASH ins)
        {
            try
            {
                TimestampConfig timestampConfig = new TimestampConfig();
                timestampConfig.UseTimestamp = false;
                Org.BouncyCastle.X509.X509Certificate cert = GetX509Cert(ins.chainBase64);
                Org.BouncyCastle.X509.X509Certificate[] chain = new Org.BouncyCastle.X509.X509Certificate[] { cert };
                if (insertSignature(ins.PATH_TEMP, ins.PATH_FILE, ins.FIELD_NAME, ins.HASH, Convert.FromBase64String(ins.HASH_SIGN_ED), chain, ins.dtSign, timestampConfig))
                {
                    try
                    {
                        File.Delete(ins.PATH_TEMP);
                    }catch(Exception ex)
                    {

                    }
                    return getStringBase64(ins.PATH_FILE);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return "";
        }
        public bool checkSerial(string strBase64Cer, string strSerial)
        {
            try
            {

                System.Security.Cryptography.X509Certificates.X509Certificate c = GetX509Cert2(strBase64Cer);
                string strSerialCert = c.GetSerialNumberString().ToUpper().Replace(" ", "");
                if (strSerialCert.Equals(strSerial.ToUpper().Replace(" ", "")))
                    return true;
                else
                {
                    strSerialCert = strSerialCert.Substring(1, strSerialCert.Length - 1);
                    if (strSerialCert.Equals(strSerial.ToUpper().Replace(" ", "")))
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool checkByIssuer(string strBase64Cer, string[] strIssuer)
        {
            try
            {

                System.Security.Cryptography.X509Certificates.X509Certificate c1 = GetX509Cert2(strBase64Cer);
                string strIssuerCert = getValueByKey(c1.GetIssuerName(), "CN=", "CN =");
                if (strIssuer.Contains(strIssuerCert))
                    return true;
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private  bool createHash(SignFileDetail ct, byte[] inputBytes, String tempFile, String fieldName,
                Org.BouncyCastle.X509.X509Certificate certificate, DateTime dtSign, string strPathFont, string strCer)
        {
            iTextSharp.text.pdf.PdfReader pdfReader = null;
            FileStream os = null;
            try
            {
                pdfReader = new iTextSharp.text.pdf.PdfReader(inputBytes);
                int numberOfPages = pdfReader.NumberOfPages;
                int signPage = int.Parse(ct.PAGE);
                if (signPage < 1 || signPage > numberOfPages)
                {
                    signPage = 1;
                }
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                }
                os = new FileStream(tempFile, FileMode.Create);
                PdfStamper stamper = PdfStamper.CreateSignature(pdfReader, os, '\0', null, true);
                PdfSignatureAppearance appearance = stamper.SignatureAppearance;
                if (ct.CONTACT == null || "".Equals(ct.CONTACT))
                {
                    ct.CONTACT = GetCN(certificate);
                }
                appearance.Contact = ct.CONTACT;
                appearance.SignDate = dtSign;
                appearance.Reason = ct.REASON;
                appearance.Location = ct.LOCATION;
                float coorX = float.Parse(ct.X);
                float coorY = float.Parse(ct.Y);
                float width = float.Parse(ct.WIDTH);
                float height = float.Parse(ct.HEIGHT);
                Rectangle rectangle = new Rectangle(coorX, coorY, coorX + width, coorY + height);
                //BaseFont bf = BaseFont.CreateFont(, "Identity-H", true);
                System.Text.EncodingProvider ppp = System.Text.CodePagesEncodingProvider.Instance;
                Encoding.RegisterProvider(ppp);
                BaseFont bf = BaseFont.CreateFont(strPathFont, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                if (ct.FONT_SIZE == null || ct.FONT_SIZE == "")
                    ct.FONT_SIZE = "14";
                if (ct.DISPLAY_TYPE.Equals("TEXT"))
                {
                    //appearance.Layer2Font = new iTextSharp.text.Font(bf, float.Parse(ct.FONT_SIZE));
                    System.Security.Cryptography.X509Certificates.X509Certificate2 c2 = GetX509Cert2(strCer);
                    string str = ct.MESSAGE;
                    str = str.Replace("nguoiky", c2.GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType.SimpleName, false));
                    str = str.Replace("ngayky", dtSign.ToString("dd/MM/yyyy HH:mm:ss"));
                    str = str.Replace("chucvu", getValueByKey(c2.SubjectName.Name.ToString(), "O=", "O ="));
                    str = str.Replace("donvi", getValueByKey(c2.SubjectName.Name.ToString(), "OU=", "OU ="));
                    //StringBuilder buf = new StringBuilder();
                    //buf.Append(str);
                    ct.MESSAGE = str;
                    PdfPTable table = new PdfPTable(1);
                    table.TotalWidth = float.Parse(ct.WIDTH);
                    table.SetWidths(new float[] { float.Parse(ct.WIDTH) });
                    table.SpacingAfter = 0;
                    table.SpacingBefore = 0;
                    table.LockedWidth = true;
                    PdfPCell cell = new PdfPCell();
                    cell.Border = 0;
                    cell.VerticalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                    cell.HorizontalAlignment = Element.ALIGN_TOP;
                    Paragraph ptxt11 = new Paragraph();
                    string strConLai = "";
                    iTextSharp.text.Font attachFont = new iTextSharp.text.Font(bf, float.Parse(ct.FONT_SIZE), iTextSharp.text.Font.NORMAL);
                    while (true)
                    {
                        //PdfPCell cellTemp = new PdfPCell();
                        //cellTemp = cell;
                        bool b = createCell(bf, float.Parse(ct.FONT_SIZE), str, ref strConLai, ref ptxt11);
                        if (b)
                        {
                            str = strConLai;
                            //cell = cellTemp;
                        }
                        else
                        {
                            //cell = cellTemp;
                            break;
                        }
                    }
                    cell.AddElement(ptxt11);
                    cell.BorderColor = BaseColor.WHITE;
                    //cell.PaddingTop = 0;
                    cell.NoWrap = false;
                    table.AddCell(cell);
                    table.DefaultCell.Border = 0;
                    table.DefaultCell.BorderColor = BaseColor.WHITE;

                    int totalRows = table.Rows.Count;
                    appearance.SetVisibleSignature(rectangle, signPage, fieldName);
                    PdfTemplate n2 = appearance.GetLayer(2);
                    float x2 = n2.BoundingBox.Left;
                    float y2 = n2.BoundingBox.Bottom;
                    float w2 = n2.BoundingBox.Width;
                    float h2 = n2.BoundingBox.Height;

                    ColumnText ct1 = new ColumnText(n2);
                    ct1.SetSimpleColumn(x2, y2, w2, h2);
                    ct1.AddElement(table);
                    ct1.Go();
                }
                if (ct.DISPLAY_TYPE.Equals("IMAGE"))
                {
                    appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(Convert.FromBase64String(ct.IMAGE));
                    appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC;
                    appearance.SetVisibleSignature(rectangle, signPage, fieldName);
                }
                if (ct.DISPLAY_TYPE.Equals("TABLE_GD"))
                {
                    System.Security.Cryptography.X509Certificates.X509Certificate2 c2 = GetX509Cert2(strCer);
                    string str = ct.MESSAGE;
                    str = str.Replace("nguoiky", c2.GetNameInfo(System.Security.Cryptography.X509Certificates.X509NameType.SimpleName, false));
                    str = str.Replace("ngayky", dtSign.ToString("dd/MM/yyyy HH:mm:ss"));
                    str = str.Replace("chucvu", getValueByKey(c2.SubjectName.Name.ToString(), "O=", "O ="));
                    str = str.Replace("donvi", getValueByKey(c2.SubjectName.Name.ToString(), "OU=", "OU ="));
                    appearance.Layer2Font = new iTextSharp.text.Font(bf, float.Parse(ct.FONT_SIZE), iTextSharp.text.Font.NORMAL);
                    appearance.Layer2Text = str;
                    appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(Convert.FromBase64String(ct.IMAGE));
                    appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION;
                    appearance.SetVisibleSignature(rectangle, signPage, fieldName);
                }
                if (ct.DISPLAY_TYPE.Equals("TABLE"))
                {
                    Image logo = Image.GetInstance(Convert.FromBase64String(ct.IMAGE));
                    PdfPTable table = null;
                    iTextSharp.text.Font attachFont = new iTextSharp.text.Font(bf, float.Parse(ct.FONT_SIZE), iTextSharp.text.Font.NORMAL);
                    //Kiểm tra để xoay chữ ký theo chiều dọc
                    if ((height - logo.Height - ct.MESSAGE.Split('\n').Length * 30) > 0)
                    {
                        table = new PdfPTable(1);
                        table.TotalWidth = float.Parse(ct.WIDTH);
                        table.SetWidths(new float[] { float.Parse(ct.WIDTH) });
                        table.SpacingAfter = 0;
                        table.SpacingBefore = 0;
                        table.LockedWidth = true;


                        PdfPCell cell = new PdfPCell();
                        cell.Border = 0;
                        cell.VerticalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                        cell.HorizontalAlignment = Element.ALIGN_TOP;

                        //BaseFont bf = BaseFont.CreateFont(DisplayConfig.FONT_PATH_TIMESNEWROMAN_WINDOWS, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                        Paragraph ptxt = new Paragraph(ct.MESSAGE, attachFont);
                        ptxt.IndentationLeft = 5;
                        ptxt.IndentationRight = 5;
                        ptxt.FirstLineIndent = 30f;

                        cell.AddElement(ptxt);
                        cell.BorderColor = BaseColor.WHITE;
                        cell.PaddingTop = 1;
                        table.AddCell(cell);


                        cell = new PdfPCell();
                        cell.PaddingLeft = width / 2 - logo.Width / 2;
                        cell.AddElement(logo);
                        cell.BorderColor = BaseColor.WHITE;
                        cell.FixedHeight = logo.Height;
                        table.AddCell(cell);

                        table.DefaultCell.Border = 0;
                        table.DefaultCell.BorderColor = BaseColor.WHITE;
                    }
                    else
                    {
                        table = new PdfPTable(2);
                        table.TotalWidth = float.Parse(ct.WIDTH);
                        table.SetWidths(new float[] { 60, 40 });
                        table.SpacingAfter = 0;
                        table.SpacingBefore = 0;
                        table.LockedWidth = true;

                        PdfPCell cell = new PdfPCell();
                        cell.Border = 0;
                        cell.VerticalAlignment = Element.ALIGN_JUSTIFIED_ALL;
                        cell.HorizontalAlignment = Element.ALIGN_TOP;

                        Paragraph ptxt = new Paragraph(ct.MESSAGE, attachFont);
                        ptxt.IndentationLeft = 0;

                        cell.AddElement(ptxt);
                        cell.BorderColor = BaseColor.GRAY;
                        table.AddCell(cell);
                        cell = new PdfPCell(logo);
                        cell.PaddingLeft = -10;
                        if (logo.Height < height)
                        {
                            logo.ScaleToFitHeight = false;
                        }
                        else
                        {
                            logo.ScaleToFitHeight = true;
                        }
                        cell.BorderColor = BaseColor.WHITE;
                        //cell.FixedHeight = logo.Height;
                        table.AddCell(cell);

                        table.DefaultCell.Border = 0;
                        table.DefaultCell.BorderColor = BaseColor.WHITE;
                    }

                    //int totalRows = table.Rows.Count;
                    appearance.SetVisibleSignature(rectangle, signPage, fieldName);
                    PdfTemplate n2 = appearance.GetLayer(2);
                    float x2 = n2.BoundingBox.Left;
                    float y2 = n2.BoundingBox.Bottom;
                    float w2 = n2.BoundingBox.Width;
                    float h2 = n2.BoundingBox.Height;

                    ColumnText ct1 = new ColumnText(n2);
                    ct1.SetSimpleColumn(x2, y2, w2, h2);
                    ct1.AddElement(table);
                    ct1.Go();
                }
                appearance.Acro6Layers = true;
                appearance.CertificationLevel = 0;
                IExternalSignatureContainer external = new ExternalBlankSignatureContainer(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
                MakeSignature.SignExternalContainer(appearance, external, 8192);
                pdfReader.Close();
                return true;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            finally
            {
                if (pdfReader != null)
                {
                    pdfReader.Close();
                }
                if (os != null)
                {
                    try
                    {
                        os.Close();
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine("Error emptySignature: " + ex.Message);
                    }
                }
            }
        }

        public static bool createCell(BaseFont bf, float fSize, string strContent, ref string strDoanCL, ref Paragraph ptxt1)
        {
            try
            {
                // PdfPCell cell = new PdfPCell();
                iTextSharp.text.Font attachFont = new iTextSharp.text.Font(bf, fSize, iTextSharp.text.Font.NORMAL);
                iTextSharp.text.Font attachFontBold = new iTextSharp.text.Font(bf, fSize, iTextSharp.text.Font.BOLD);
                iTextSharp.text.Font attachFontItalic = new iTextSharp.text.Font(bf, fSize, iTextSharp.text.Font.ITALIC);
                iTextSharp.text.Font attachFontBoldItalic = new iTextSharp.text.Font(bf, fSize, iTextSharp.text.Font.BOLDITALIC);
                string strTemp = "";
                //iTextSharp.text.Font attachFontBoldUnDef = new iTextSharp.text.Font(bf, fSize, iTextSharp.text.Font.UNDEFINED);
                if (strContent.IndexOf("</b>") > 0 || strContent.IndexOf("</i>") > 0)
                {
                    int b = strContent.IndexOf("<b>");
                    int i = strContent.IndexOf("<i>");
                    int b1 = strContent.IndexOf("</b>");
                    int i1 = strContent.IndexOf("</i>");
                    //Paragraph ptxt;
                    //string strContent = "Lối ra vào <b> duy nhất <i>của Chung </i>cư M.One Nam</b> Sài Gòn, cách <b> mặt tiền </b> <i>đường Bế</i> Văn Cấm  <i> duy nhất <b>của Chung </b>cư M.One Nam</i>";
                    if (b < i && b != -1)
                    {
                        if (i < b1)
                        {

                            strTemp = strContent.Substring(0, b);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFont);
                                ptxt1.Add(new Phrase(strTemp, attachFont));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(b + 3, i - b - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontBold);
                                ptxt1.Add(new Phrase(strTemp, attachFontBold));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(i + 3, i1 - i - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontBoldItalic);
                                ptxt1.Add(new Phrase(strTemp, attachFontBoldItalic));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(i1 + 4, b1 - i1 - 4);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontBold);
                                ptxt1.Add(new Phrase(strTemp, attachFontBold));
                                //cell.AddElement(ptxt);
                            }
                            strDoanCL = strContent.Substring(b1 + 4, strContent.Length - b1 - 4);
                            if (strDoanCL == "")
                            {
                                return false;
                            }

                        }
                        else
                        {
                            strTemp = strContent.Substring(0, b);
                            if (strTemp != "")
                            {
                                // ptxt = new Paragraph(strTemp, attachFont);
                                ptxt1.Add(new Phrase(strTemp, attachFont));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(b + 3, b1 - b - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontBold);
                                ptxt1.Add(new Phrase(strTemp, attachFontBold));
                                //cell.AddElement(ptxt);
                            }
                            strDoanCL = strContent.Substring(b1 + 4, strContent.Length - b1 - 4);
                            if (strDoanCL == "")
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        if (b < i1 && b != -1)
                        {

                            strTemp = strContent.Substring(0, i);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFont);
                                ptxt1.Add(new Phrase(strTemp, attachFont));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(i + 3, b - i - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontItalic);
                                ptxt1.Add(new Phrase(strTemp, attachFontItalic));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(b + 3, b1 - b - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontBoldItalic);
                                ptxt1.Add(new Phrase(strTemp, attachFontBoldItalic));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(b1 + 4, i1 - b1 - 4);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontItalic);
                                ptxt1.Add(new Phrase(strTemp, attachFontItalic));
                                //cell.AddElement(ptxt);
                            }
                            strDoanCL = strContent.Substring(i1 + 4, strContent.Length - i1 - 4);
                            if (strDoanCL.Trim() == "")
                            {
                                return false;
                            }

                        }
                        else
                        {
                            strTemp = strContent.Substring(0, i);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFont);
                                ptxt1.Add(new Phrase(strTemp, attachFont));
                                //cell.AddElement(ptxt);
                            }
                            strTemp = strContent.Substring(i + 3, i1 - i - 3);
                            if (strTemp != "")
                            {
                                //ptxt = new Paragraph(strTemp, attachFontItalic);
                                ptxt1.Add(new Phrase(strTemp, attachFontItalic));
                                //cell.AddElement(ptxt);
                            }
                            strDoanCL = strContent.Substring(i1 + 4, strContent.Length - i1 - 4);
                            if (strDoanCL == "")
                            {
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    //Paragraph ptxt = new Paragraph(strContent, attachFont);
                    //cell.AddElement(ptxt);
                    ptxt1.Add(new Phrase(strContent, attachFont));
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error text: " + strContent + ex.Message);
            }
        }

        private bool insertSignature(String src, String dest, String fieldName,
                byte[] hash, byte[] extSignature, Org.BouncyCastle.X509.X509Certificate[] chain,
                DateTime signDate, TimestampConfig timestampConfig)
        {
            PdfReader reader = null;
            FileStream os = null;

            try
            {
                reader = new PdfReader(src);
                if (File.Exists(dest))
                {
                    File.Delete(dest);
                }
                os = new FileStream(dest, FileMode.Append);
                AcroFields af = reader.AcroFields;
                PdfDictionary v = af.GetSignatureDictionary(fieldName);
                if (v == null)
                {
                    Console.WriteLine("No field");
                    throw new Exception("Không thấy field cần insert" + src);
                }
                if (!af.SignatureCoversWholeDocument(fieldName))
                {
                    Console.WriteLine("Not the last signature");
                    throw new Exception("field chèn chữ ký ko thích hợp " + fieldName + src);

                }
                PdfArray b = v.GetAsArray(PdfName.BYTERANGE);
                long[] gaps = b.AsLongArray();
                if (b.Size != 4 || gaps[0] != 0)
                {
                    Console.WriteLine("Single exclusion space supported");
                    throw new Exception("lỗi không gian chèn chữ ký " + fieldName + src);

                }
                IRandomAccessSource readerSource = reader.SafeFile.CreateSourceView();
                String hashAlgorithm = HASH_SHA1;

                PdfPKCS7 sgn = new PdfPKCS7(null, chain, hashAlgorithm, false);
                sgn.SetExternalDigest(extSignature, null, CRYPT_RSA);

                DateTime cal = signDate;

                //Timestamp
                TSAClientBouncyCastle tsc = null;
                if (timestampConfig.UseTimestamp)
                {
                    tsc = new TSAClientBouncyCastle(timestampConfig.TsaUrl,
                            timestampConfig.TsaAcc, timestampConfig.TsaPass);
                }

                byte[] signedContent = sgn.GetEncodedPKCS7(hash, cal, tsc, null, null, CryptoStandard.CMS);
                int spaceAvailable = (int)(gaps[2] - gaps[1]) - 2;
                if ((spaceAvailable & 1) != 0)
                {
                    Console.WriteLine("Gap is not a multiple of 2");
                    throw new Exception("Gap is not a multiple of 2 " + fieldName + src);

                }
                spaceAvailable /= 2;
                if (spaceAvailable < signedContent.Length)
                {
                    Console.WriteLine("Not enough space");
                    throw new Exception("Not enough space " + fieldName + src);

                }
                StreamUtil.CopyBytes(readerSource, 0, gaps[1] + 1, os);
                ByteBuffer bb = new ByteBuffer(spaceAvailable * 2);
                foreach (byte bi in signedContent)
                {
                    bb.AppendHex(bi);
                }
                int remain = (spaceAvailable - signedContent.Length) * 2;
                for (int k = 0; k < remain; ++k)
                {
                    bb.Append((byte)48);
                }
                bb.WriteTo(os);
                StreamUtil.CopyBytes(readerSource, gaps[2] - 1, gaps[3] + 1, os);
                os.Close();

                readerSource.Close();
                bb.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error insertSignature: " + ex.Message);
            }
            finally
            {
                if (os != null)
                {
                    try
                    {
                        os.Close();
                    }
                    catch (IOException ex)
                    {
                        Console.WriteLine("Error insertSignature: " + ex.Message);
                    }
                }
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }
        private string[] getCerBase64Async(String strLoaiKy, string strConffig, string serialEx, string strCer)
        {
            try
            {
                if (strLoaiKy.ToUpper().Equals("HDI"))
                {
                    return new string[] { strCer };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return new string[] { "" };
        }
        /// <summary>
        /// ký hash
        /// </summary>
        /// <param name="strLoaiKy">Thực hiện ký của đơn vị nào, HDI là tự ký p12</param>
        /// <param name="strBasse64Hash">Hash cần ký</param>
        /// <param name="strConffig">pass của file p12 hoặc là cấu hình</param>
        /// <param name="bP12">File p12 chứng thư số</param>
        /// <returns>Trả về hash đã ký</returns>
        private string signHash_PDF(String strLoaiKy, string strBasse64Hash, string strConffig, string serialEx, string strMessage, byte[] bP12)
        {
            try
            {
                if (strLoaiKy.ToUpper().Equals("HDI"))
                {
                    string strPath = pRoot + @"/SignFolderTemp/";
                    string strPathP12 = createFileP12(bP12, strPath);
                    X509Certificate2 cert = new X509Certificate2(strPathP12, strConffig, X509KeyStorageFlags.MachineKeySet);
                    var rsa = (RSACryptoServiceProvider)cert.PrivateKey;
                    byte[] signature = rsa.SignHash(Convert.FromBase64String(strBasse64Hash), CryptoConfig.MapNameToOID(HASH_SHA1));
                    // tra lai signature       
                    try
                    {
                        File.Delete(strPathP12);
                    }
                    catch { }
                    return Convert.ToBase64String(signature);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return "";
        }

        private List<byte[]> preSign(String src, String fieldName, Org.BouncyCastle.X509.X509Certificate[] chain, DateTime signDate)
        {
            PdfReader reader = null;
            try
            {
                List<byte[]> result = new List<byte[]>();
                reader = new PdfReader(src);
                AcroFields af = reader.AcroFields;
                PdfDictionary v = af.GetSignatureDictionary(fieldName);
                if (v == null)
                {
                    Console.WriteLine("No field");
                    throw new Exception("Error create hash: No field " + fieldName);
                    //return null;
                }
                PdfArray b = v.GetAsArray(PdfName.BYTERANGE);
                long[] gaps = b.AsLongArray();
                if (b.Size != 4 || gaps[0] != 0)
                {
                    Console.WriteLine("Single exclusion space supported");
                    throw new Exception("Error create hash: Single exclusion space supported");
                    //return null;
                }
                IRandomAccessSource readerSource = reader.SafeFile.CreateSourceView();
                Stream rg = new RASInputStream(new RandomAccessSourceFactory().CreateRanged(readerSource, gaps));

                PdfPKCS7 sgn = new PdfPKCS7(null, chain, HASH_SHA1, false);
                byte[] hash = DigestAlgorithms.Digest(rg, DigestUtilities.GetDigest(HASH_SHA1));
                DateTime cal = signDate;
                byte[] sh = sgn.getAuthenticatedAttributeBytes(hash, cal, null, null, CryptoStandard.CMS);
                result.Add(sh);
                result.Add(hash);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error create hash: " + e.Message);
                throw new Exception("Error create hash: " + e.Message);
                //return null;
            }
            finally
            {
                try
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error create hash: " + e.Message);
                }

            }
        }
        private static string getValueByKey(string name1, string serach, string serach2)
        {
            try
            {
                int index = name1.IndexOf(serach);
                if (index > -1)
                {
                    name1 = name1.Substring(index + serach.Length);
                    if (name1.IndexOf(",") > 0)
                    {
                        name1 = name1.Substring(0, name1.IndexOf(","));
                    }
                    return name1;
                }
                else
                {
                    index = name1.IndexOf(serach2);
                }
                name1 = "";
            }
            catch
            {
                return name1;
            }
            return name1;
        }
        private static string getStringBase64(string strPath)
        {
            Byte[] bytes = File.ReadAllBytes(strPath);
            String file = Convert.ToBase64String(bytes);
            try
            {
                File.Delete(strPath);
            }
            catch { }
            return file;
        }
        private byte[] encodeData(byte[] orginalData, String algorithm)
        {
            byte[] dataSignByte = null;
            if (HASH_SHA1.Equals(algorithm))
            {
                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1Managed();
                dataSignByte = sha.ComputeHash(orginalData);
            }
            else if (HASH_SHA1.Equals(algorithm))
            {
                System.Security.Cryptography.SHA256 sha2 = new System.Security.Cryptography.SHA256Managed();
                dataSignByte = sha2.ComputeHash(orginalData);

            }
            return dataSignByte;

        }

        public Org.BouncyCastle.X509.X509Certificate GetX509Cert(String CertStr)
        {
            try
            {
                System.Security.Cryptography.X509Certificates.X509Certificate cert = new System.Security.Cryptography.X509Certificates.X509Certificate();
                //byte[] rawData = GetBytes(CertStr);
                //cert.Import(rawData);
                //string strPath = createBase64ToFile(CertStr);
                //string strPath = pathCer;
                cert = new System.Security.Cryptography.X509Certificates.X509Certificate(Convert.FromBase64String(CertStr));
                Org.BouncyCastle.X509.X509Certificate certBouncyCastle = DotNetUtilities.FromX509Certificate(cert);
                //File.Delete(strPath);
                return certBouncyCastle;
            }
            catch (Exception e)
            {
                throw new Exception("Error get Chain cert: " + e.Message);
            }
        }
        public string createBase64ToFile(string str)
        {
            string strPath = pRoot + @"/SignFolderTemp/";
            string strName = DateTime.Now.ToString("yyyyMMddHHmmss");
            if (!File.Exists(strPath))
            {
                System.IO.Directory.CreateDirectory(strPath);
            }
            if (!File.Exists(strPath + "/" + strName + ".cer"))
            {
                File.WriteAllBytes(strPath + "/" + strName + ".cer", Convert.FromBase64String(str));
            }            
            return strPath + strName + ".cer";
        }
        public static string createFileP12(byte[] bCer, string strPath)
        {

            if (!File.Exists(strPath))
            {
                System.IO.Directory.CreateDirectory(strPath);
            }
            //string strName = DateTime.Now.ToString("yyyyMMddHHmmss");
            string strGuiID_File = Guid.NewGuid().ToString("N");
            File.WriteAllBytes(strPath + strGuiID_File + ".p12", bCer);
            return strPath + strGuiID_File + ".p12";
        }
        public  System.Security.Cryptography.X509Certificates.X509Certificate2 GetX509Cert2(String CertStr)
        {
            try
            {
                System.Security.Cryptography.X509Certificates.X509Certificate2 cert = new System.Security.Cryptography.X509Certificates.X509Certificate2();
                //byte[] rawData = GetBytes(CertStr);
                //cert.Import(rawData);
                //string strPath = createBase64ToFile(CertStr);
                //string strPath = pathCer;
                cert = new System.Security.Cryptography.X509Certificates.X509Certificate2(Convert.FromBase64String(CertStr));
                
                //File.Delete(strPath);
                return cert;
            }
            catch (Exception e)
            {
                throw new Exception("Error get Chain cert: " + e.Message);
            }
        }
        public static String GetCN(Org.BouncyCastle.X509.X509Certificate cert)
        {
            try
            {
                String subject = GetSubject(cert);
                return GetCNFromDN(subject);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getCN: " + e.Message);
                return null;
            }
        }
        /**
         * Get subject of certificate
         *
         * @param certificate X509Certificate
         * @return <CODE>String</CODE> name of subject or empty <CODE>String</CODE>
         */
        public static String GetSubject(Org.BouncyCastle.X509.X509Certificate certificate)
        {
            try
            {
                String subject = certificate.SubjectDN.ToString();
                return subject;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getSubject: " + e.Message);
                return null;
            }
        }
        public static string GetCNFromDN(string dn)
        {
            try
            {
                string[] info = dn.Split(',');
                string cn = "";
                for (int i = 0; i < info.Length; i++)
                {
                    if (info[i].IndexOf("CN=") != -1)
                    {
                        cn = info[i].Split('=')[1];
                    }
                }
                if (cn != "")
                {
                    return ConvertTVKhongDau(cn);
                }
                return cn;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error GetCNFromDN: " + e.Message);
                return "";
            }
        }
        public static string ConvertTVKhongDau(string str)
        {
            string[] a = { "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ" };
            string[] aUpper = { "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ" };
            string[] e = { "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ" };
            string[] eUpper = { "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ" };
            string[] i = { "ì", "í", "ị", "ỉ", "ĩ" };
            string[] iUpper = { "Ì", "Í", "Ị", "Ỉ", "Ĩ" };
            string[] o = { "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ" };
            string[] oUpper = { "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ" };
            string[] u = { "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ" };
            string[] uUpper = { "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ" };
            string[] y = { "ỳ", "ý", "ỵ", "ỷ", "ỹ" };
            string[] yUpper = { "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ" };
            str = str.Replace("đ", "d");
            str = str.Replace("Đ", "D");
            foreach (string a1 in a)
            {
                str = str.Replace(a1, "a");
            }
            foreach (string a1 in aUpper)
            {
                str = str.Replace(a1, "A");
            }
            foreach (string e1 in e)
            {
                str = str.Replace(e1, "e");
            }
            foreach (string e1 in eUpper)
            {
                str = str.Replace(e1, "E");
            }
            foreach (string i1 in i)
            {
                str = str.Replace(i1, "i");
            }
            foreach (string i1 in iUpper)
            {
                str = str.Replace(i1, "I");
            }
            foreach (string o1 in o)
            {
                str = str.Replace(o1, "o");
            }
            foreach (string o1 in oUpper)
            {
                str = str.Replace(o1, "O");
            }
            foreach (string u1 in u)
            {
                str = str.Replace(u1, "u");
            }
            foreach (string u1 in uUpper)
            {
                str = str.Replace(u1, "U");
            }
            foreach (string y1 in y)
            {
                str = str.Replace(y1, "y");
            }
            foreach (string y1 in yUpper)
            {
                str = str.Replace(y1, "Y");
            }
            return str;
        }
        public bool findTextInPdf(string strText, string strPath, ref float x, ref float y, ref float width, ref int ipage)
        {
            try
            {
                PdfToTextConverter pdfToTextConverter = new PdfToTextConverter();
                pdfToTextConverter.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                FindTextLocation[] findTextLocations = pdfToTextConverter.FindText(strPath, strText, true, true);
                EvoPdf.Document pdfDocument = new EvoPdf.Document(strPath);

                if (findTextLocations.Length > 0)
                {
                    FindTextLocation find = findTextLocations[findTextLocations.Length - 1];
                    ipage = find.PageNumber;
                    x = find.X;
                    y = pdfDocument.Pages[ipage - 1].PageSize.Height - find.Y;
                    width = find.Width;

                }
                pdfDocument.Close();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool findTextInPdfBatch(string strText, byte[] strPath, ref float x, ref float y, ref float width, ref int ipage)
        {
            try
            {
                PdfToTextConverter pdfToTextConverter = new PdfToTextConverter();
                pdfToTextConverter.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                FindTextLocation[] findTextLocations = pdfToTextConverter.FindText(strPath, strText, true, true);
                Stream streamPDF = new MemoryStream(strPath);
                EvoPdf.Document pdfDocument = new EvoPdf.Document(streamPDF);

                if (findTextLocations.Length > 0)
                {
                    FindTextLocation find = findTextLocations[findTextLocations.Length - 1];
                    ipage = find.PageNumber;
                    x = find.X;
                    y = pdfDocument.Pages[ipage - 1].PageSize.Height - find.Y;
                    width = find.Width;

                }
                pdfDocument.Close();
                if (ipage < 1 && x==0 && y==0)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #endregion
    }
}
