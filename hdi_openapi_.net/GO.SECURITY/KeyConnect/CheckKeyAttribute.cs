﻿using GO.CONSTANTS;
using GO.DTO.Base;
using GO.HELPER.Utility;
using System.Net.Http.Formatting;
using System.Net;
using System.Web.Http.Filters;
using System;
using System.Web.Http.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace GO.SECURITY.KeyConnect
{
    public class CheckKeyAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            BaseResponse result = new BaseResponse();
            try
            {
                if (!context.Request.Headers.TryGetValues("KeyS", out IEnumerable<string> values) || values.First() != DateTime.Now.ToString("ddMMyyyy") + "HDI-CreatePDF")
                {
                    var response = context.Response = new HttpResponseMessage();
                    result = goUtility.Instance.GetResponse(false, null, "Key not correct", "ERROR");

                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json");
                }
            }
            catch (Exception)
            {
                var response = context.Response = new HttpResponseMessage();
                result = goUtility.Instance.GetResponse(false, null, "Key not correct", "ERROR");

                response.StatusCode = HttpStatusCode.Forbidden;
                response.Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json");
            }
        }
    }
}
