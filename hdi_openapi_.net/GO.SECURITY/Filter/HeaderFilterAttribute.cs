﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;

using GO.CONSTANTS;
using System.Web;
using System.Web.Caching;
using System.Net.Http;
using GO.DTO.Base;
using GO.HELPER.Utility;
using System.Net;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using GO.COMMON.Log;

namespace GO.SECURITY.Filter
{
    public class HeaderFilterAttribute : ActionFilterAttribute
    {
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public goConstants.TimeUnit TimeUnit { get; set; }
        public int CountRequest { get; set; }
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseRequest request = new BaseRequest();
                // log check request header
                try
                {
                    Task<string> content = filterContext.Request.Content.ReadAsStringAsync();
                    string objRequest = content.Result;
                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, " 1 - " + objRequest, Logger.ConcatName(nameClass, nameMethod));
                }
                catch (Exception ex)
                {
                    try
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, " 1 - " +  JsonConvert.SerializeObject(filterContext.Request), Logger.ConcatName(nameClass, nameMethod));
                    }
                    catch (Exception)
                    {
                    }
                }
                
                try
                {
                    Task<string> content = filterContext.Request.Content.ReadAsStringAsync();
                    string objRequest = content.Result;
                    request = JsonConvert.DeserializeObject<BaseRequest>(objRequest);
                }
                catch (Exception ex)
                {
                    request = null;
                }

                BaseResponse result = new BaseResponse();

                #region Validate DDOS
                var seconds = Convert.ToInt32(TimeUnit);
                string ip = HttpContext.Current.Request.UserHostAddress;
                string ipPrivate = "";
                if (request != null && request.Device != null && request.Device.IpPrivate != null)
                {
                    ipPrivate = request.Device.IpPrivate;
                }
                var key = string.Join(
                    "-",
                    seconds,
                    filterContext.Request.Method,
                    filterContext.ActionDescriptor.ControllerDescriptor,
                    filterContext.ActionDescriptor.ActionName,
                    ip,
                    ipPrivate
                );

                // increment the cache value
                var count = 1;
                if (HttpRuntime.Cache[key] != null)
                    count = (int)HttpRuntime.Cache[key] + 1;

                HttpRuntime.Cache.Insert(key, count, null, DateTime.UtcNow.AddSeconds(seconds), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);
                if (count > CountRequest)
                {
                    var response = filterContext.Response = new HttpResponseMessage();
                    result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2001), goConstantsError.Instance.ERROR_2001);

                    response.StatusCode = HttpStatusCode.Forbidden;
                    response.Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json");
                }
                //base.OnActionExecuting(filterContext);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                //throw ex;
                var response = filterContext.Response = new HttpResponseMessage();
                BaseResponse result = new BaseResponse();
                result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2002), goConstantsError.Instance.ERROR_2002);

                response.StatusCode = HttpStatusCode.Forbidden;
                response.Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json");
            }

        }

        //public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        //{
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        //    base.OnActionExecuted(actionExecutedContext);
        //}
    }
}
