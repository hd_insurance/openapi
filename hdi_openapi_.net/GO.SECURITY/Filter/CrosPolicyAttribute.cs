﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Cors;
using System.Web.Http.Filters;

namespace GO.SECURITY.Filter
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class CrosPolicyAttribute : Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _policy;

        public CrosPolicyAttribute()
        {
            // Create a CORS policy.
            _policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            // Add allowed origins.
            _policy.Origins.Add("http://192.168.1.20");
            //_policy.Origins.Add("http://www.contoso.com");
        }
        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }
    }

    //public class CorsPolicyFactory : ICorsPolicyProviderFactory
    //{
    //    ICorsPolicyProvider _provider = new MyCorsPolicyProvider();

    //    public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
    //    {
    //        return _provider;
    //    }
    //}

    public class CrosFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.Add("Access-Control-Allow-Origin", "*");
            actionContext.Request.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
            actionContext.Request.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
            actionContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
            actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
            base.OnActionExecuting(actionContext);
        }
        //public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        //{
        //    actionExecutedContext.Request.Headers.Add("Access-Control-Allow-Origin", "*");
        //    actionExecutedContext.Request.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
        //    actionExecutedContext.Request.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
        //    actionExecutedContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
        //    base.OnActionExecuted(actionExecutedContext);
        //}
    }

    public class CrosAuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.Add("Access-Control-Allow-Origin", "*");
            actionContext.Request.Headers.Add("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            actionContext.Request.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type");
            actionContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Origin, Content-Type");
            base.OnAuthorization(actionContext);
        }
    }
}
