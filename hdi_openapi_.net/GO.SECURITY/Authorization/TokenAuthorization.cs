﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.HELPER.Utility;
using System.Net.Http.Formatting;
using GO.DTO.Common;
using GO.BUSINESS.Token;
using GO.BUSINESS.Validator;
using GO.BUSINESS.Partner;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GO.DTO.Partner;
using GO.BUSINESS.Action;
using System.Web;
using GO.COMMON.Log;

namespace GO.SECURITY.Authorization
{
    public class TokenAuthorization : AuthorizeAttribute
    {
        #region Nuget
        // Microsoft.AspNet.WebApi.Core 5.2.7
        #endregion

        #region Variables
        private string statusCode = "";
        private string errorMessage = "";
        private string keyKoken = "Token";
        private string keySignature = "";
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion

        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            // 1. check model request
            // 1.1 check black list (black list all)
            // 2. get partner is validate token, is signature, is white list, list action use, is all action
            // 3. if partner validate token
            // 4. check black list (black list ddos)
            // 5. check white list
            // 6. check signature
            // 7. check partner use action
            // 8. check domain to environment
            TokenAuthorizationInfo result = new TokenAuthorizationInfo();
            result.Sucess = true;
            string ip = HttpContext.Current.Request.UserHostAddress;
            try
            {
                string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ip rq: " + ip, Logger.ConcatName(nameClass, nameMethod));
                BaseRequest request = new BaseRequest();
                PartnerConfigModel partnerConfig = new PartnerConfigModel();

                #region 1. check request
                Task<string> content = actionContext.Request.Content.ReadAsStringAsync();
                string objRequest = content.Result;
                try
                {
                    request = JsonConvert.DeserializeObject<BaseRequest>(objRequest);
                    goBusinessValidator.Instance.ValidatorRequest(ref result, request);
                    if (!result.Sucess)
                    {
                        statusCode = result.Error;
                        errorMessage = result.ErrorMessage;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    statusCode = nameof(goConstantsError.Instance.ERROR_2002);
                    errorMessage = goConstantsError.Instance.ERROR_2002;
                    return false;
                }
                #endregion

                #region 4. check black list
                #endregion

                #region 2 get config partner
                try
                {
                    if (request != null && request.Action != null && request.Action.ParentCode != null && request.Action.Secret != null)
                    {
                        partnerConfig = goBusinessPartner.Instance.GetPartnerConfig(request.Action.ParentCode, request.Action.Secret);
                    }
                }
                catch (Exception ex)
                {
                    statusCode = nameof(goConstantsError.Instance.ERROR_2006);
                    return false;
                }

                if (partnerConfig == null)
                {
                    statusCode = nameof(goConstantsError.Instance.ERROR_2007);
                    return false;
                }
                #endregion

                #region 3. check token
                if (goUtility.Instance.CastToBoolean(partnerConfig.isToken))
                {
                    var headers = actionContext.Request.Headers;
                    if (headers.Contains(keyKoken))
                    {
                        var token = headers.GetValues(keyKoken).FirstOrDefault();
                        goBusinessToken.Instance.ValidTokenAuthorization(ref result, request, partnerConfig.Secret, token, goUtility.Instance.CastToBoolean(partnerConfig.is_Refesh_Token));
                        if (!result.Sucess)
                        {
                            statusCode = result.Error;
                            return false;
                        }
                    }
                    else
                    {
                        statusCode = nameof(goConstantsError.Instance.ERROR_2003);
                        return false;
                    }
                }
                #endregion

                #region 4. check black list
                try
                {
                    goBusinessAction.Instance.ValidIp(ref result, partnerConfig.Enviroment_Code, ip);
                    if (!result.Sucess)
                    {
                        statusCode = result.Error;
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    statusCode = nameof(goConstantsError.Instance.ERROR_2016);
                    return false;
                }
                #endregion

                #region 5. check white list
                if (goUtility.Instance.CastToBoolean(partnerConfig.isWhiteList))
                {
                    goBusinessPartner.Instance.ValidPartnerWhitelist(ref result, partnerConfig.Partner_Code, ip, partnerConfig.Enviroment_Code);
                    if (!result.Sucess)
                    {
                        statusCode = result.Error;
                        return false;
                    }
                }
                #endregion

                #region 6. check signature
                if (goUtility.Instance.CastToBoolean(partnerConfig.isSignature_Request))
                {
                    result = goBusinessAction.Instance.ValidateSignature(request, partnerConfig);
                    if (!result.Sucess)
                    {
                        statusCode = result.Error;
                        return false;
                    }
                }
                #endregion

                #region 7. check partner use action
                goBusinessPartner.Instance.ValidPartnerUseApi(ref result, partnerConfig.Partner_Code, request.Action.ActionCode);
                if (!result.Sucess)
                {
                    statusCode = result.Error;
                    return false;
                }
                #endregion

                #region 8. check domain to environment
                string domain = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "domain rq: " + domain, Logger.ConcatName(nameClass, nameMethod));
                goBusinessAction.Instance.ValidDomain(ref result, partnerConfig.Enviroment_Code, domain);
                if (!result.Sucess)
                {
                    statusCode = result.Error;
                    return false;
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            //actionContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            //actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
            //actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

            if (actionContext.Request.Method.ToString() == "OPTIONS")
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage
                {
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Content = new StringContent("Prelight OK!")
                };
            }
            else
            {
                BaseResponse result = new BaseResponse();
                switch (this.statusCode)
                {
                    case nameof(goConstantsError.Instance.ERROR_2001):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2001), goConstantsError.Instance.ERROR_2001, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2002):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2002), errorMessage, keySignature);//goConstantsError.Instance.ERROR_2002
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2003):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2003), goConstantsError.Instance.ERROR_2003, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2004):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2004), goConstantsError.Instance.ERROR_2004, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2006):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2006), goConstantsError.Instance.ERROR_2006, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2007):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2011):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2011), goConstantsError.Instance.ERROR_2011, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2012):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2012), goConstantsError.Instance.ERROR_2012, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2013):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2013), goConstantsError.Instance.ERROR_2013, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2014):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2014), goConstantsError.Instance.ERROR_2014, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2015):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2015), goConstantsError.Instance.ERROR_2015, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    case nameof(goConstantsError.Instance.ERROR_2016):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2016), goConstantsError.Instance.ERROR_2016, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    default:
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2003), goConstantsError.Instance.ERROR_2003, keySignature);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                }
            }
        }
    }
}
