﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Partner;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.HELPER.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GO.SECURITY.Authorization
{
    public class ReceiverAuthorization : AuthorizeAttribute
    {
        private string statusCode = "";
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            TokenAuthorizationInfo result = new TokenAuthorizationInfo();
            result.Sucess = true;
            try
            {
                string env = "";
                goBusinessCommon.Instance.GetEnvironmentConfig(ref env);
                string ip = HttpContext.Current.Request.UserHostAddress;
                goBusinessPartner.Instance.ValidPartnerWhitelist(ref result, ip, env);
                if (!result.Sucess)
                {
                    statusCode = result.Error;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            //actionContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            //actionContext.Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
            //actionContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

            if (actionContext.Request.Method.ToString() == "OPTIONS")
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage
                {
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Content = new StringContent("Prelight OK!")
                };
            }
            else
            {
                BaseResponse result = new BaseResponse();
                switch (this.statusCode)
                {
                    case nameof(goConstantsError.Instance.ERROR_2001):
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2015), goConstantsError.Instance.ERROR_2015);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                    default:
                        {
                            result = goUtility.Instance.GetResponse(false, null, nameof(goConstantsError.Instance.ERROR_2006), goConstantsError.Instance.ERROR_2006);
                            actionContext.Response = new System.Net.Http.HttpResponseMessage
                            {
                                StatusCode = System.Net.HttpStatusCode.Forbidden,
                                Content = new ObjectContent<BaseResponse>(result, new JsonMediaTypeFormatter(), "application/json")
                            };
                            break;
                        }
                }
            }
        }
    }
}
