﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class ReqInsurAir
    {
        public string CHANNNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public string ACTION { get; set; }
        public FlightVJC FLI_INFO { get; set; }
        public CustomerVJC BUYER_INFO { get; set; }
        public List<CustomerProductVJC> CUSTOMERS { get; set; }
        public Seller SELLER_INFO { get; set; }
    }
}
