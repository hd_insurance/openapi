﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class ResInsurAir
    {
        public OrdAir ORDER { get; set; }
    }

    public class OrdAir
    {
        public string ORD_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string TOTAL_DISCOUNT { get; set; }
        public string VAT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
        public Seller SELLER_INFO { get; set; }
        public FlightVJC FLI_INFO { get; set; }
        public CustomerVJC BUYER_INFO { get; set; }
        public List<OrdDetailAir> DETAIL { get; set; }
    }

    public class OrdDetailAir
    {
        public string DETAIL_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string TOTAL_DISCOUNT { get; set; }
        public string VAT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public List<CustomerProductVJC> CUSTOMERS { get; set; }
    }
}
