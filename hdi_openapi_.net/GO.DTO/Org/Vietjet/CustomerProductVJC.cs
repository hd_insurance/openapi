﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class CustomerProductVJC : CustomerVJC
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public string DETAIL_CODE { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string FARE_CLASS { get; set; }
        public List<ADDITIONAL> ADDITIONAL { get; set; }
        public string EFF_DATE { get; set; }
        public string EXP_DATE { get; set; }
        public List<GIFT> GIFT { get; set; }
        public string FLI_ID { get; set; }
        public string FLI_NO { get; set; }
        public string ARR { get; set; }
        public string DEP { get; set; }       
        public string ETD { get; set; }
        public string ETD1 { get; set; }
        public string ETD1_TIME { get; set; }
        public string ATD { get; set; }
        public string ATD_TIME { get; set; }
        public string DELAY { get; set; }
        public string BOOKING_DATE { get; set; }
        public string BOOKING_TIME { get; set; }
        public string IS_SENTEMAIL { get; set; }
        public string EMAIL_SENTDATE { get; set; }
        public string EMAIL_SENTTIME { get; set; }
    }
    public class ADDITIONAL
    {
        public string ADD_CODE { get; set; }
    }
    public class GIFT
    {
        public string GIFT_CODE { get; set; }
    }
}
