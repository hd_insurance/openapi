﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class FlightVJC
    {
        public string ORG_CODE { get; set; }
        /// <summary>
        /// Mã phía HDI
        /// </summary>
        public string FLI_CODE { get; set; }
        /// <summary>
        /// Mã chuyến bay phía ORG
        /// </summary>
        public string FLI_ID { get; set; }
        /// <summary>
        /// Mã chuyến bay bị đổi chuyến của ORG
        /// </summary>
        public string FLI_ID_CHANGE { get; set; }
        /// <summary>
        /// Số hiệu chuyến bay
        /// </summary>
        public string FLI_NO { get; set; }
        /// <summary>
        /// Quốc gia khởi hành
        /// </summary>
        public string COUNTRY_F { get; set; }
        /// <summary>
        /// Mã sân bay khởi hành
        /// </summary>
        public string DEP { get; set; }
        /// <summary>
        /// Quốc gia hạ cánh
        /// </summary>
        public string COUNTRY_T { get; set; }
        /// <summary>
        /// Mã sân bay hạ cánh
        /// </summary>
        public string ARR { get; set; }
        /// <summary>
        /// Ngày bay dự kiến DD/MM/YYYY
        /// </summary>
        public string FLI_DATE { get; set; }
        /// <summary>
        /// Giờ bay dự kiến HH24:MI:SS
        /// </summary>
        public string FLI_TIME { get; set; }
        /// <summary>
        /// Ngày bay thực tế
        /// </summary>
        public string FLI_R_DATE { get; set; }
        /// <summary>
        /// Giờ bay thực tế
        /// </summary>
        public string FLI_R_TIME { get; set; }        
        /// <summary>
        /// Ngày hạ cánh dự kiến DD/MM/YYYY
        /// </summary>
        public string FLI_LAND_DATE { get; set; }
        /// <summary>
        /// Giờ hạ cánh dự kiến HH24:MI:SS
        /// </summary>
        public string FLI_LAND_TIME { get; set; }
        /// <summary>
        /// Ngày hạ cánh thực tế DD/MM/YYYY
        /// </summary>
        public string FLI_LAND_R_DATE { get; set; }
        /// <summary>
        /// Giờ hạ cánh thực tế HH24:MI:SS
        /// </summary>
        public string FLI_LAND_R_TIME { get; set; }
        /// <summary>
        /// Ngày giờ đóng chuyến DD/MM/YYYY HH24:MI:SS
        /// </summary>
        public string CLOSE_DATE { get; set; }

    }
}
