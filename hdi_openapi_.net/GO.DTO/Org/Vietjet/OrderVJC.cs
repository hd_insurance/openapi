﻿using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class OrderVJC
    {
        public FlightVJC FLIGHT_INFO { get; set; }
        public List<CustomerProductVJC> CUSTOMERS { get; set; }
        /// <summary>
        /// Thêm phục vụ cho các sản phẩm khác
        /// </summary>
        public ORD_PAYINFO PAY_INFO { get; set; }
        /// <summary>
        /// Thêm phục vụ cho các sản phẩm khác
        /// </summary>
        public ORD_SELLER SELLER { get; set; }
        /// <summary>
        /// Thêm phục vụ cho các sản phẩm khác
        /// </summary>
        public ORD_BUYER BUYER { get; set; }
    }
}
