﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class ProductHDI
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PRODUCT_NAME { get; set; }
        public object PACK_INFO { get; set; }
    }
    public class PackInfo
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string PACK_NAME { get; set; }
        public string FEES { get; set; }
        public string FILE_URL { get; set; }
        public ObjBenefits BENEFITS_INFO { get; set; }
        public object ADDITIONAL { get; set; }
    }

    public class Benefits
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string BE_CODE { get; set; }
        public string BE_NAME { get; set; }
    }

    public class ObjBenefits
    {
        public object BENEFITS { get; set; }
        public string BENEFITS_FULL { get; set; }
    }

    public class Addit
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string ADD_CODE { get; set; }
        public string ADD_NAME { get; set; }
        public string ADD_FEES { get; set; }
        public string DESCRIPTION { get; set; }
    }
}
