﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Vietjet
{
    public class CustomerVJC : Customer
    {
        public string BOOKING_ID { get; set; }
        public string SEAT { get; set; }

        /// <summary>
        /// Danh sách hành lý
        /// </summary>
        public List<CustomerLuggagesVJC> LUGGAGES { get; set; }

        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Kiểu KH của VJ: Người lớn, trẻ em
        /// </summary>
        public string CUS_TYPE { get; set; }
        public string PROV_CODE { get; set; }
        public string DIST_CODE { get; set; }
        public string WARDS_CODE { get; set; }
        public string ADDRESS_SHORT { get; set; }
        public string ADDRESS_FULL { get; set; }
    }
    public class CustomerLuggagesVJC
    {
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Mã hành lý
        /// </summary>
        public string LUGGAGE_NO { get; set; }
        /// <summary>
        /// Khối lượng hành lý
        /// </summary>
        public string LUGGAGE_WEIGHT { get; set; }
        /// <summary>
        /// Loại hành lý
        /// </summary>
        public string LUGGAGE_TYPE { get; set; }
    }
}
