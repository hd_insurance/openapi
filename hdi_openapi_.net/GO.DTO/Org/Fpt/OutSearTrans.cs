﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Fpt
{
    public class OutSearTrans
    {
        public string STATUS { get; set; }
        public string MESS { get; set; }
        public List<OutSearTransDetail> DETAILS { get; set; }
    }

    public class OutSearTransDetail
    {
        public string NAME { get; set; }
        public string CERTIFICATE_NO { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
    }
}
