﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Hdi
{
    public class ReqCsvx
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public Customer BUYER { get; set; }
        public List<INSURED> INSURED { get; set; }
        
    }
    public class INSURED : Customer
    {
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string RELATIONSHIP { get; set; }
    }
}
