﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Common;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Org.Hdi
{
    /// <summary>
    ///  cấp đơn theo lô - import
    /// </summary>
    public class ReqOrdersCommon
    {
        public string ACTION { get; set; }
        public ORD_SELLER SELLER { get; set; }
        public ORD_BUYER BUYER { get; set; }
        public ReqOrdersContract CONTRACT { get; set; }
        public List<InsuredPerson> INSURED { get; set; }
    }

    public class ReqOrdersContract
    {
        public string CONTRACT_CODE { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_MODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string STATUS { get; set; }
    }
}
