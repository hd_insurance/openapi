﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.OrderSku
{
    public class OrgShortLink
    {
        public string ORG_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string SHORT_KEY { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
    }
}
