﻿namespace GO.DTO.OrderSku
{
    public class MarProduct
    {
        public string ORG_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string MAR_PRODUCT_CODE { get; set; }
        public string MAR_GROUP { get; set; }
        public string MAR_PACK_CODE { get; set; }
        public string PACK_CODE { get; set; }
    }
}
