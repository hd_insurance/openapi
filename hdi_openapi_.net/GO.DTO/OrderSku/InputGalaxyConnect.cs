﻿namespace GO.DTO.OrderSku
{
    public class InputGalaxyConnect
    {
        public string transactionId { get; set; }
        public string offerCode { get; set; }
        public CustomerInfo customerInfo { get; set; }
    }
    public class CustomerInfo
    {
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }

    }
    //public class InputGalaxyPay
    //{
    //    public string requestID { get; set; }
    //    public double requestDateTime { get; set; }
    //    public RequestDataGalaxyPay requestData { get; set; }
    //}

    //public class RequestDataGalaxyPay
    //{
    //    public string orderID { get; set; }
    //    public double orderNumber { get; set; }
    //    public double orderAmount { get; set; }
    //    public string orderCurrency { get; set; }
    //    public double orderDateTime { get; set; }
    //    public string orderDescription { get; set; }
    //    public string cardType { get; set; }
    //    public string bank { get; set; }
    //    public string otp { get; set; }
    //    public string request { get; set; }
    //    public string token { get; set; }
    //    public string language { get; set; }
    //    public ExtraData extraData { get; set; }
    //}
    //public class ExtraData
    //{
    //    public Customer customer { get; set; }
    //    public Device device { get; set; }
    //    public Application application { get; set; }
    //    public Airline airline { get; set; }
    //    public Billing billing { get; set; }
    //}
    //public class Customer
    //{
    //    public string firstName { get; set; }
    //    public string lastName { get; set; }
    //    public string identityNumber { get; set; }
    //    public string email { get; set; }
    //    public string phoneNumber { get; set; }
    //    public string phoneType { get; set; }
    //    public string gender { get; set; }
    //    public string dateOfBirth { get; set; }
    //    public string title { get; set; }
    //}
    //public class Device
    //{
    //    public string browser { get; set; }
    //    public string fingerprint { get; set; }
    //    public string hostName { get; set; }
    //    public string ipAddress { get; set; }
    //    public string deviceID { get; set; }
    //    public string deviceModel { get; set; }
    //}
    //public class Application
    //{
    //    public string applicationID { get; set; }
    //    public string applicationChannel { get; set; }
    //}

    //public class Airline
    //{
    //    public string recordLocator { get; set; }
    //    public double journeyType { get; set; }
    //    public string departureAirport { get; set; }
    //    public string departureDateTime { get; set; }
    //    public string arrivalAirport { get; set; }
    //    public string arrivalDateTime { get; set; }
    //    public List<Services> services { get; set; }
    //    public List<Flights> flights { get; set; }
    //    public List<Passengers> passengers { get; set; }
    //}
    //public class Services
    //{
    //    public string serviceCode { get; set; }
    //    public double quantity { get; set; }
    //    public double amount { get; set; }
    //    public double tax { get; set; }
    //    public double fee { get; set; }
    //    public double totalAmount { get; set; }
    //    public string currency { get; set; }
    //}
    //public class Flights
    //{
    //    public string airlineCode { get; set; }
    //    public string carrierCode { get; set; }
    //    public double flightNumber { get; set; }
    //    public string travelClass { get; set; }
    //    public string departureAirport { get; set; }
    //    public string departureDate { get; set; }
    //    public string departureTime { get; set; }
    //    public string departureTax { get; set; }
    //    public string arrivalAirport { get; set; }
    //    public string arrivalDate { get; set; }
    //    public string arrivalTime { get; set; }
    //    public double fees { get; set; }
    //    public double taxes { get; set; }
    //    public double fares { get; set; }
    //    public string fareBasisCode { get; set; }
    //    public string originCountry { get; set; }
    //}
    //public class Passengers
    //{
    //    public string passengerID { get; set; }
    //    public string passengerType { get; set; }
    //    public string firstName { get; set; }
    //    public string lastName { get; set; }
    //    public string title { get; set; }
    //    public string gender { get; set; }
    //    public string dateOfBirth { get; set; }
    //    public string identityNumber { get; set; }
    //    public string nameInPNR { get; set; }
    //    public string memberTicket { get; set; }
    //}
    //public class Billing
    //{
    //    public string countryCode { get; set; }
    //    public string stateProvine { get; set; }
    //    public string cityName { get; set; }
    //    public string postalCode { get; set; }
    //    public string streetNumber { get; set; }
    //    public string addressLine1 { get; set; }
    //    public string addressLine2 { get; set; }
    //}
}
