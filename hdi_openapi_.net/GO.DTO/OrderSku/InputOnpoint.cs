﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.OrderSku
{
    public class InputOnpoint
    {
        public string ORDER_CODE { get; set; }
        public string ORG_CODE { get; set; }
        public string EXCHANGE_CODE { get; set; }
        public string BUYER_NAME { get; set; }
        public string PHONE { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string EMAIL { get; set; }
        public string CONTENT { get; set; }
        public string ADDRESS { get; set; }
        public string CUS_CODE { get; set; }
        public string DATE_PAY { get; set; }
        public List<InputProduct> DETAIL { get; set; }
    }

    public class InputProduct
    {
        public string MAR_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public int TOTAL { get; set; }
        public double PRICE { get; set; } = 0;
        public double DISCOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; }
    }
}
