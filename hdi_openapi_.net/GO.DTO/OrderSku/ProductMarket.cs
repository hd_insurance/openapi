﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.OrderSku
{
    public class ProductMarket
    {
         public string ORG_CODE { get; set; }
         public string MAR_CODE { get; set; }
         public string MAR_NAME { get; set; }
         public string CATEGORY { get; set; }
         public string PRODUCT_CODE { get; set; }
         public string PACK_CODE { get; set; }
         public double AMOUNT { get; set; }
         public double TOTAL_DISCOUNT { get; set; }
         public double VAT { get; set; }
         public double TOTAL_AMOUNT { get; set; }
         public string DURATION { get; set; }
         public string DURATION_UNIT { get; set; }
         public string OBJ { get; set; }
         public string REF_VAL { get; set; }
         public object OBJ_VALIDATE { get; set; }
    }
}
