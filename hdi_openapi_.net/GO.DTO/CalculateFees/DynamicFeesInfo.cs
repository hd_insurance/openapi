﻿using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CalculateFees
{
    public class DynamicFeesInfo
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string CRITERIA_CODE { get; set; }
        public string KEYPAIR_CODE { get; set; }
        public double FEES { get; set; }
    }

    public class ProductFormula
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string REF_FORMULA { get; set; }
        public string FORMULA { get; set; }
        public string INCLUDE_VAT { get; set; }
        public double VAT_RATIO { get; set; }
    }

    public class CriteriaFees
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string CRITERIA_CODE { get; set; }
        public string DISPLAY_CODE { get; set; }
        public string CRITERIA_NAME { get; set; }
        public string SORTORDER { get; set; }
        public char CHAR { get; set; }
        public double VAL_DATA { get; set; }

        // 02/06/2021
        public double PERCEN_DIS { get; set; }

        /// <summary>
        /// 09/06/2021: Bố sung thêm phí thể hiện gồm vat( chưa giảm với TH XCG)
        /// </summary>
        public double FEES_DATA { get; set; } = 0;
        /// <summary>
        /// giá gốc
        /// </summary>
        public double FEES { get; set; } = 0;
        /// <summary>
        /// giá bán (Giá sau discount trừ Gift)
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        /// <summary>
        /// VAT
        /// </summary>
        public double VAT { get; set; } = 0;
        /// <summary>
        /// Tổng giảm (bảo gồm giảm của giá gốc và giảm của gift)
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;

        // VER 3
        /// <summary>
        /// loại phí: Chính, bổ sung
        /// </summary>
        public string TYPE_FEES { get; set; }
        public string IS_MULTI { get; set; }
        /// <summary>
        /// công thức tiêu chí nếu có
        /// </summary>
        public string FORMULA { get; set; }
        /// <summary>
        /// công thức ánh xạ
        /// </summary>
        public string REF_FORMULA { get; set; }
        /// <summary>
        /// có trả về cho client
        /// </summary>
        public string IS_OUT { get; set; }
        /// <summary>
        /// loại tiêu chí: CONSTANT, CACHE, FORMULA
        /// </summary>
        public string TYPE_CRITERIA { get; set; }
        /// <summary>
        /// Kiểu mã hóa: NONE, MD5
        /// </summary>
        public string TYPE_ENCRYPT { get; set; }
        /// <summary>
        /// Tỉ lệ thuế của tiêu chí
        /// </summary>
        public double VAT_RATIO { get; set; }
        /// <summary>
        /// Tiêu chí này có được gán vat không
        /// </summary>
        public string INCLUDE_VAT { get; set; }

        /// <summary>
        /// tiêu chí gốc - tham gia vào công thức gốc
        /// </summary>
        public string IS_ROOT { get; set; }

        /// <summary>
        /// Tiêu chí có được áp dụng giảm giá không -- nếu không thì sẽ k được áp dụng với mọi trường hợp
        /// </summary>
        public string IS_DISCOUNT { get; set; }
        public string TYPE_DISCOUNT { get; set; }
        public string TYPE_DISCOUNT_P { get; set; }
        public string TYPE_DISCOUNT_C { get; set; }
        public string MODE_FEES { get; set; }

        /// <summary>
        /// Tiêu chí có áp dụng min không. Nếu nhỏ hơn min => set bằng min
        /// </summary>
        public string IS_MIN { get; set; }

        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }

    public class CriteriaFeesDetail
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string CRITERIA_CODE { get; set; }
        public string PARENT_CODE { get; set; }
    }

    public class ProductDefineFees
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string DEFINE_CODE { get; set; }
        public string TYPE_CODE { get; set; }
        public string OPERATOR_MIN { get; set; }
        public string MIN_VALUE { get; set; }
        public string OPERATOR_MAX { get; set; }
        public string MAX_VALUE { get; set; }
        public string VALUE { get; set; }

    }
}
