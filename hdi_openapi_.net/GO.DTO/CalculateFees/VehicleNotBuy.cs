﻿using GO.DTO.Insurance.Vehicle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CalculateFees
{
    public class VehicleNotBuy : VehicleInfo
    {
        /// <summary>
        /// Có mua TNDS BB ?
        /// </summary>
        public string IS_COMPULSORY { get; set; }
        /// <summary>
        /// Có mua TNDS TN ?
        /// </summary>
        public string IS_VOLUNTARY_ALL { get; set; }

        /// <summary>
        /// Có mua TNDS TN (người và hàng hóa người t3, hành khách)
        /// </summary>
        public string IS_VOLUNTARY { get; set; }
        public string IS_3RD { get; set; }
        public string IS_3RD_ASSETS { get; set; }
        public string IS_PASSENGER { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho LPX, NNTX
        /// </summary>
        public string IS_DRIVER { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho hàng hóa
        /// </summary>
        public string IS_CARGO { get; set; }
        /// <summary>
        /// Có mua VCX ?
        /// </summary>
        public string IS_PHYSICAL { get; set; }
    }
}
