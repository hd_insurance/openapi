﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CalculateFees
{
    /// <summary>
    /// Xác định phí min của tiêu chi ứng với org
    /// </summary>
    public class CriteriaFeesMin
    {
        public string ORG_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string CRITERIA_CODE { get; set; }
        public double MIN_VAL { get; set; }
    }
}
