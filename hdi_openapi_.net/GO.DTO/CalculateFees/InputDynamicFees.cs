﻿using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CalculateFees
{
    public class InputDynamicFees
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string STBH { get; set; }
        public List<DynamicData> DYNAMIC_FEES { get; set; }
        public List<GiftInfo> GIFTS { get; set; }
    }

    public class InputDynamicFeesVer2
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public List<DynamicProduct> PRODUCT_INFO { get; set; }
        public List<GiftInfo> GIFTS { get; set; }
    }
    public class DynamicProduct
    {
        public int INX { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string STBH { get; set; }
        public string DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public string EFF { get; set; }
        public string EXP { get; set; }
        public List<DynamicData> DYNAMIC_FEES { get; set; }
    }

    public class DynamicData
    {
        public string KEY_CODE { get; set; }
        public string VAL_CODE { get; set; }
        /// <summary>
        /// Giá trị truyền lên
        /// 1. mặc định: là key cache
        /// 2. CONSTANT: hằng số
        /// </summary>
        public string TYPE_CODE { get; set; }
    }
    public class OutDynamicFees
    {
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<OutDynamicProduct> PRODUCT_DETAIL { get; set; }
        public List<DiscountInfo> DISCOUNT_GIFT_ALL { get; set; }
    }

    public class OutDynamicProduct
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        /// <summary>
        /// giá gốc
        /// </summary>
        public double FEES { get; set; } = 0;
        /// <summary>
        /// giá bán
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        /// <summary>
        /// VAT
        /// </summary>
        public double VAT { get; set; } = 0;
        /// <summary>
        /// Tổng giảm (bảo gồm giảm của giá gốc và giảm của gift)
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }


    public class InputDynamicFeesVer3
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public List<DynamicProductVer3> PRODUCT_INFO { get; set; }
        public List<GiftInfo> GIFTS { get; set; }
    }

    public class InputDynamicFeesVer4
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORG_CODE { get; set; }
        public List<DynamicProductVer4> PRODUCT_INFO { get; set; }
        public List<GiftInfo> GIFTS { get; set; }
    }

    public class DynamicProductVer3
    {
        public int INX { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public double DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public string TYPE_DISCOUNT { get; set; }
        public string TYPE_DISCOUNT_C { get; set; }

        // 14/02/2022: thêm eff và exp
        public string EFF { get; set; }
        public string EXP { get; set; }

        public List<DynamicData> DYNAMIC_FEES { get; set; }
    }

    public class DynamicProductVer4
    {
        public int INX { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public double DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public string TYPE_DISCOUNT { get; set; }
        public string TYPE_DISCOUNT_C { get; set; }

        // 14/02/2022: thêm eff và exp
        public string EFF { get; set; }
        public string EXP { get; set; }
        public string DOB { get; set; }

        public List<DynamicData> DYNAMIC_FEES { get; set; }
    }

    public class OutDynamicFeesVer3
    {
        public double FEES_DATA { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<OutDynamicProductVer3> PRODUCT_DETAIL { get; set; }
        public List<DiscountInfo> DISCOUNT_GIFT_ALL { get; set; }
    }
    public class OutDynamicProductVer3
    {
        public int INX { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        /// <summary>
        /// 09/06/2021: Bố sung thêm phí thể hiện gồm vat( chưa giảm với TH XCG)
        /// </summary>
        public double FEES_DATA { get; set; } = 0;
        /// <summary>
        /// giá gốc
        /// </summary>
        public double FEES { get; set; } = 0;
        /// <summary>
        /// giá bán (Giá sau discount trừ Gift)
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        /// <summary>
        /// VAT
        /// </summary>
        public double VAT { get; set; } = 0;
        public double VAT_RATIO { get; set; } = 0;
        /// <summary>
        /// Tổng giảm (bảo gồm giảm của giá gốc và giảm của gift)
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_ADD { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
        public List<ADDITIONAL_FEES> OUT_DETAIL { get; set; }
        public List<ADDITIONAL_FEES> ADDITI_DETAIL { get; set; }
    }
    public class ADDITIONAL_FEES
    {
        public string CODE { get; set; }
        public string NAME { get; set; }

        /// <summary>
        /// 09/06/2021: Bố sung thêm phí thể hiện gồm vat( chưa giảm với TH XCG)
        /// </summary>
        public double FEES_DATA { get; set; } = 0;
        /// <summary>
        /// giá gốc
        /// </summary>
        public double FEES { get; set; }
        /// <summary>
        /// giá bán
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        /// <summary>
        /// VAT
        /// </summary>
        public double VAT { get; set; } = 0;
        /// <summary>
        /// tỉ lệ VAT
        /// </summary>
        public double VAT_RATIO { get; set; } = 0;
        /// <summary>
        /// Tổng giảm (bảo gồm giảm của giá gốc và giảm của gift)
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }

    public class ProductFeesInfo
    {

        /// <summary>
        /// 09/06/2021: Bố sung thêm phí thể hiện gồm vat( chưa giảm với TH XCG)
        /// </summary>
        public double FEES_DATA { get; set; } = 0;
        /// <summary>
        /// giá gốc
        /// </summary>
        public double FEES_ROOT { get; set; }
        /// <summary>
        /// giá bán
        /// </summary>
        public double FEES_BUY { get; set; }
        /// <summary>
        /// Tổng giảm (bảo gồm giảm của giá gốc và giảm của gift)
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; }
        /// <summary>
        /// VAT
        /// </summary>
        public double VAT { get; set; }
        public double VAT_RATIO { get; set; }
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }
}
