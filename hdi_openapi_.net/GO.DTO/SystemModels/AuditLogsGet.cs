﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class AuditLogsGet
    {
        public string LogId { get; set; }
        /// <summary>
        /// Ngày tạo request (DD/MM/YYYY)
        /// </summary>
        public string DayRequest { get; set; }
    }

    public class AuditLogsResponse
    {
        public string LogId { get; set; }
        public string TimeInit { get; set; }
        /// <summary>
        /// Request gửi tới
        /// </summary>
        public object request { get; set; }
        /// <summary>
        /// Lỗi cụ thể nếu exception
        /// </summary>
        public object response { get; set; }
    }
}
