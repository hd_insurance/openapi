﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class UserInfo
    {
        public string Gid { get; set; }
        public string User_Code { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Partner_Code { get; set; }
        public string Enviroment_Code { get; set; }
        public string Client_ID { get; set; }
        public string Secret { get; set; }
        public string IsSignature_RESPONSE { get; set; }
    }
}
