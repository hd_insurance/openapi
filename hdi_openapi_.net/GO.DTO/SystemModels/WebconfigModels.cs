﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class WebconfigModels
    {
        public string PROJECT_CODE { get; set; }
        public string DEFINE_CODE { get; set; }
        public string TYPE_CODE { get; set; }
        public string TYPE_NAME { get; set; }
    }
}
