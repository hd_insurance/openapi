﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class ActionApiDatabase
    {
        public string Gid { get; set; }
        public string Api_Code { get; set; }
        public string Db_Code { get; set; }
        public string Enviroment_Code { get; set; }
        public string Db_Mode { get; set; }
        public string Data { get; set; }
        public string Schema { get; set; }
        public string Packages { get; set; }
        public string DB_Index { get; set; }
    }
}
