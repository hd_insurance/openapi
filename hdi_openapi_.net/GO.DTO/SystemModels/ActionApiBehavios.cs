﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class ActionApiBehavios
    {
        public string Gid { get; set; }
        public string BehaviosCode { get; set; }
        public string Api_Code { get; set; }
        public string PeriodValidity { get; set; }
        public string BehaviorsAppear { get; set; }
        public string ClientTracking { get; set; }
    }
}
