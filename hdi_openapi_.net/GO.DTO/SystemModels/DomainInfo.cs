﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class DomainInfo
    {
        public string Enviroment_Code { get; set; }
        public string Domain { get; set; }
    }
}
