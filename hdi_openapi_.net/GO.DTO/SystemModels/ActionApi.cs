﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SystemModels
{
    public class ActionApi
    {
        public string Gid { get; set; }
        public string Api_Code { get; set; }
        public string Pro_Code { get; set; }
        public string AutoCache { get; set; }
        public string BehaviosCache { get; set; }
        public string Action_Type { get; set; }
        public string Crud { get; set; }
    }
}
