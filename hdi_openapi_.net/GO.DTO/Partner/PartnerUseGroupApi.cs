﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Partner
{
    public class PartnerUseGroupApi
    {
        public string ApiGroup_Code { get; set; }
        public string Api_Code { get; set; }
        public string Partner_Code { get; set; }
    }
}
