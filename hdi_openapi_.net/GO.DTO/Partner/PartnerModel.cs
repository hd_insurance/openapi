﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Partner
{
    public class PartnerModel
    {
        public string Partner_Code { get; set; }
        public string PartnerName { get; set; }
        public string CompanyName { get; set; }
        public string CeoName { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string TaxCode { get; set; }
        public string Website { get; set; }
        public string ContractNo { get; set; }
        public string PartnerType { get; set; } // public / private
        public string Description { get; set; }

    }
}
