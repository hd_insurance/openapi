﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Partner
{
    public class PartnerConfigModel
    {
        public string Partner_Code { get; set; }
        public string Partner_Name { get; set; }
        public string Client_ID { get; set; }
        public string Secret { get; set; }
        public string isApproved { get; set; }
        public string isToken { get; set; }
        public string isWhiteList { get; set; }
        public string isSignature_Request { get; set; }
        public string isSignature_Response { get; set; }
        public string isAll_Action { get; set; }
        public string is_Refesh_Token { get; set; }
        public string isLimit { get; set; }
        public string CountLimit { get; set; }
        public string Enviroment_Code { get; set; }
        public string Path_Cer { get; set; }
        public string Type_Encode { get; set; }
    }
}
