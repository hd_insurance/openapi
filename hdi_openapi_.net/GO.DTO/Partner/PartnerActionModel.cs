﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Partner
{
    public class PartnerActionModel
    {
        public string Partner_Code { get; set; }
        public string Action_Code { get; set; }
    }
}
