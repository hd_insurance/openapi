﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Partner
{
    public class PartnerWhiteListModel
    {
        public string ORG_CODE { get; set; }
        public string IP { get; set; }
        public string ENV_CODE { get; set; } // Dev, Test, Uat, Live
    }

    public class BlackListModel
    {
        public string IP { get; set; }
        public string ENV_CODE { get; set; } // Dev, Test, Uat, Live
    }
}
