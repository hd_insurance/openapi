﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    class VerifyModel
    {

    }
    public class CerInforModel
    {
        public string SERIAL { get; set; }
        public string NAME { get; set; }
        public string PUBLICKEY { get; set; } //thiết bị
        public DateTime DATE_AFTER { get; set; }
        public DateTime DATE_BEFORE { get; set; }
        public DateTime DATE_SIGN { get; set; }
        public string NAME_FULL { get; set; }
        public string ISSUER { get; set; }
        public string ISSUER_FULL { get; set; }
        public string FIELD_NAME { get; set; }
    }
}
