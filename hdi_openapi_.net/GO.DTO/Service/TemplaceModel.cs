﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class TemplaceModel
    {
        public string USER_NAME { get; set; }
        public string TEMP_CODE { get; set; }
        public string TEMP_TYPE { get; set; }
        public string TYPE_RES { get; set; }
        public string PARAM_CODE { get; set; }
        public string PARAM_VALUE { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string DATA_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
    }
    public class TempFileSign
    {
        public string CERTIFICATE_NO { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string FILE_BASE64 { get; set; }
        public string FILE_URL { get; set; }
        public string STT { get; set; }
        public string ID_FILE { get; set; }
        public bool signed { get; set; }
        public string STRUCT_CODE { get; set; }
        public DateTime dtSignBack { get; set; }
    }
}
