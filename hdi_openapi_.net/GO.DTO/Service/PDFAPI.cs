﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GO.DTO.Service
{
    public class PDFAPI
    {
        public class DataActionPDF
        {
            [Required(ErrorMessage = "DataEmail is Required")]
            public object DataEmail { get; set; }
            [Required(ErrorMessage = "DataPDF is Required")]
            public object DataPDF { get; set; }
            [Required(ErrorMessage = "DataSign is Required")]
            public object DataSign { get; set; }
            public string Key { get; set; }
            public string LinkPDF { get; set; }
        }
        public class DataEmail
        {
            public bool IsSendEmail { get; set; } = false;
            public MailInfo MailInfo { get; set; }
        }
        public class MailInfo
        {
            public List<string> SendTo { get; set; }
            public List<string> CC { get; set; }
            public List<string> BBC { get; set; }
            public string Subject { get; set; }
            public string Content { get; set; }
            public bool FileAttachPDF { get; set; } = false;
        }
        public class DataPDF
        {
            public string Header { get; set; } = "";
            [Required(ErrorMessage = "Body is Required")]
            public string Body { get; set; }
            public string Footer { get; set; } = "";
        }
        public class DataSign
        {
            public bool IsSign { get; set; } = false;
            public SignatureInfo SignatureInfo { get; set; }
        }
        public class SignatureInfo
        {
            [Required(ErrorMessage = "ORG_CODE is Required")]
            public string ORG_CODE { get; set; }
            [Required(ErrorMessage = "PRODUCT_CODE is Required")]
            public string PRODUCT_CODE { get; set; }
            public string PACK_CODE { get; set; }
            public string Image { get; set; } = "M"; // S, M, F size of Image
            [Required(ErrorMessage = "FindTextInPdf is Required")]
            public string FindTextInPdf { get; set; }
            public int SangTraiX { get; set; } = 0;
            public int XuongDuoiY { get; set; } = 0;
        }
    }
}
