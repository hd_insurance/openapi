﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class ServiceEmailModel
    {
        public string subject { get; set; }
        public string content { get; set; }
        public string attach { get; set; }
        public List<EmailModel> to { get; set; }
        public List<EmailModel> cc { get; set; }
        public List<EmailModel> bcc { get; set; }
        public List<FileModel> file { get; set; }

        /// <summary>
        /// Ghi chú: Phần api send mail từ client
        /// Nếu client set To => Api or DB set nội dung
        /// Nếu client set ND, File => Api or DB set To, cc, bcc
        /// </summary>
        public string mailCode { get; set; }
        /// <summary>
        /// Kênh gọi lên
        /// </summary>
        public string channel { get; set; }

    }
    public class logEmailModel
    {
        public string ORG_CODE { get; set; }
        public string CAME_ID { get; set; }
        public string CAMPAIGN_CODE { get; set; }
        public string EMAIL { get; set; }
        public string SUBJECT { get; set; }
        public string CC { get; set; }
        public string BCC { get; set; }
        public string STATUS { get; set; }
        public string CREATE_BY { get; set; }
        public string CONTENT { get; set; }
        public string REF_ID { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string TYPE_EMAIL { get; set; }
    }
    public class EmailModel
    {
        public string email { get; set; }
    }
    public class FileModel
    {
        public string path { get; set; }
        public string name { get; set; }
    }
}
