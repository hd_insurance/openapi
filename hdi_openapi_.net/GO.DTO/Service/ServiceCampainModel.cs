﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class ServiceCampainModel
    {
        #region Contructor
        private static readonly Lazy<ServiceCampainModel> _instance = new Lazy<ServiceCampainModel>(() =>
        {
            return new ServiceCampainModel();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static ServiceCampainModel Instance { get => _instance.Value; }
        #endregion
        //public enum CamType
        //{
        //    ON_DEMAND,
        //    SCHEDULE
        //}
        public string orgCode { get; set; }
        public string camCode { get; set; }
        public string camName { get; set; }
        public string camType { get; set; } //demand
        public string camMode { get; set; } //all,sms,email,zalo...
        public string sendType { get; set; } //all,batch,user
        public string dataType { get; set; } //INTERNAL, EXTERNAL
        public string activeDate { get; set; }//ngày hiệu lực
        public string priority { get; set; } //PRIORITY --độ ưu tiên
        public string camContent { get; set; } //Nội dung chiến dịch
        public string email { get; set; } //email nhận
        public string subject { get; set; } //tiêu đề
        public string emailCC { get; set; }
        public string emailBCC { get; set; }
        public string emailContent { get; set; }
        public string phone { get; set; }
        public string smsContent { get; set; }
        public string smsID { get; set; }
        public string configCode { get; set; }
        public string total { get; set; }
        public string userName { get; set; }
    }

}
