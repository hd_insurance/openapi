﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class FileQueueSignModel
    {
        public int ID { get; set; }
        public string FILE_ID { get; set; }
        public string FILE_PATH { get; set; }
        public string BASE64 { get; set; } //thiết bị
        public TempSignModel SIGN_TEMP { get; set; }
        public TempEmailModel GCN_TEMP { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string CERTIFICATE_NO { get; set; }
        public string PACK_CODE { get; set; }
        public string CATEGORY { get; set; }
        public string TYPE { get; set; }
        public string STATUS { get; set; }
        public string FILE_LINK { get; set; }
        public string APP_ID { get; set; }
        public string ORG_SELLER { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string ERROR_CODE { get; set; }
        public string ERROR_TEXT { get; set; }
    }
    public class SignFileReturnModel
    {
        public bool Success  { get; set; }
        public string MESSAGE { get; set; }
        public string FILE_ID { get; set; } //thiết bị
        public string FILE_URL { get; set; }
        public string FILE_BASE64 { get; set; }
        public string FILE_REF { get; set; }
        public string USER_NAME { get; set; }
        public string REF_TYPE { get; set; }
        public DateTime? dtSignBack { get; set; }
    }
    public class SignFileModel
    {
        public string UserName { get; set; }
        public string PROVIDER { get; set; }
        public string DEVICE { get; set; } //thiết bị
        public string CER { get; set; }
        public string FILE_SIGN { get; set; }
        public byte[] F12_FILE { get; set; }
        public SignFileDetail detail { get; set; }
    }

    public class SignFileDetail
    {
        public string X { get; set; }
        public string Y { get; set; }
        public string WIDTH { get; set; }
        public string HEIGHT { get; set; }
        public string PAGE { get; set; }
        public string CONTACT { get; set; }
        public string LOCATION { get; set; }
        public string REASON { get; set; }
        public string MESSAGE { get; set; }
        public string FONT_SIZE { get; set; }
        public string DISPLAY_TYPE { get; set; }
        public string IMAGE { get; set; }
        public string ALERT { get; set; }
        public string FIELD_NAME { get; set; }
    }
    public class INSERT_HASH
    {
        public string PATH_TEMP { get; set; }
        public string PATH_FILE { get; set; }
        public string FIELD_NAME { get; set; }
        public string chainBase64 { get; set; }
        public byte[] HASH { get; set; }
        public string HASH_SIGN { get; set; }
        public string HASH_SIGN_ED { get; set; }
        public DateTime dtSign { get; set; }
    }

    public class TimestampConfig
    {

        /**
     * has integrate timestamp
     * default: false
     */
        private bool useTimestamp = false;
        /**
         * URL timestamp
         * default: http://tsa.viettel-ca.vn/
         */
        private String tsa_url = "http://tsa.viettel-ca.vn/";
        /**
         * account to login timestamp
         * default: null
         */
        private String tsa_acc;
        /**
         * password to login timestamp
         * default: null
         */
        private String tsa_pass;


        public bool UseTimestamp { get; set; }
        public String TsaUrl { get; set; }
        public String TsaAcc { get; set; }
        public String TsaPass { get; set; }
    }

    public class SignRequestModel
    {
        public string USER_NAME { get; set; }
        /// <summary>
        /// sử dụng để lấy cấu hình file ký số
        /// </summary>
        public string TEMP_CODE { get; set; }
        /// <summary>
        /// Kiểu base64 hay path
        /// </summary>
        public string TYPE_FILE { get; set; }
        /// <summary>
        /// xác định định dạng file cần ký
        /// </summary>
        public string EXT_FILE { get; set; }
        public string FILE { get; set; }
        public string LOCATION { get; set; }
        public string REASON { get; set; }        
        /// <summary>
        /// Kiểu trả về là base64 hay path
        /// </summary>
        public string TypeResponse { get; set; }
        public string TextNote { get; set; }
        public string Alert { get; set; }
        public string TEXT_CA { get; set; }
        /// <summary>
        /// ID của đối tác phục vụ khi tra cứu lại file ký
        /// </summary>
        public string FILE_REFF { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string FILE_CODE { get; set; }
    }
}
