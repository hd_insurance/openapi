﻿using GO.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class LogServiceModel
    {
        public DateTime datetime { get; set; } = DateTime.Now;
        public string typeService { get; set; }
        public BaseRequest request { get; set; }
        public BaseResponse response { get; set; }
    }
}
