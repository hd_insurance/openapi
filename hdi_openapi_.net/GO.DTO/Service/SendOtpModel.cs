﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class SendOtpModel
    {
        /// <summary>
        /// mã ánh xạ (username, boi thuong)
        /// </summary>
        public string REF_CODE { get; set; }
        /// <summary>
        /// Mã chiến dịch
        /// </summary>
        public string CAMPAIGN_CODE { get; set; }
        public string PHONE { get; set; }
        /// <summary>
        /// Kiểu otp (VOUCHER, Tạo user, đổi mk,...)
        /// </summary>
        public string OTP_TYPE { get; set; }
        /// <summary>
        /// số lượng số gen 4, 6, 8
        /// </summary>
        public int OTP_SIZE { get; set; }
        /// <summary>
        /// thời gian hết hạn
        /// </summary>
        public int EXP_SECONDS { get; set; }
        /// <summary>
        /// User Yêu cầu
        /// </summary>
        public string USER_CREATE { get; set; }
        /// <summary>
        /// Không cần truyền, tạo sau
        /// </summary>
        public string OTP { get; set; }
        /// <summary>
        /// Không cần truyền, tạo sau
        /// </summary>
        public string EFFECTIVE_DATE { get; set; }
        /// <summary>
        /// Không cần truyền, tạo sau
        /// </summary>
        public string EXPIRATION_DATE { get; set; }
        public int TOTAL { get; set; }
        public string MESS_SEND { get; set; }
        public string ENV_CODE { get; set; }
        // Start 2022-09-07 By ThienTVB
        public string EMAIL { get; set; }
        // End 2022-09-07 By ThienTVB
    }

    public class ResSendOtpModel
    {
        public string CAMPAIGN_CODE { get; set; }
        public string CAMSMS_ID { get; set; }
    }
}
