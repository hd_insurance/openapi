﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class RequestPaymentModel
    {
        public string Id { get; set; }
        public string lang { get; set; }
        public string callback { get; set; }
    }
}
