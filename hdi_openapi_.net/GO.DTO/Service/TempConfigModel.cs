﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
   
    public class TempConfigModel
    {
        public string ORG_CODE { get; set; }
        public string TEMP_ID { get; set; }
        public string TEMP_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string DESCRIPTION { get; set; }
        public string CHANNEL { get; set; }
        public string TEMP_TYPE { get; set; }
        public string MAIL_BCC { get; set; }
        public string USER_NAME { get; set; }
        public int ISQUEUE { get; set; }
        public List<TempEmailModel> TEMP_EMAIL { get; set; }
        public List<TempSMSModel> TEMP_SMS { get; set; }
        public TempSignModel TEMP_SIGN { get; set; }
    }
    public class TempEmailModel
    {
        public string TEMP_ID { get; set; }
        public string TEMP_TYPE { get; set; }
        public string DATA_TYPE { get; set; }
        public string TEMP_CODE { get; set; }
        public string HEADER { get; set; }
        public string FOOTER { get; set; }
        public string TITLE { get; set; }
        public string PARAM_REPLACE { get; set; }
        public string LIST_REPLACE { get; set; }
        public string URL_PAGE_ADD { get; set; }
        public string IS_ATTACH { get; set; }
        public string DATA { get; set; }
        public string TYPE_SEND { get; set; }
        public string DATA_SRC { get; set; }
        public string TEMP_CONFIG { get; set; }
        public string IS_BACK_DATE { get; set; }
        public string PACK_CODE { get; set; }
        public string ORIEN { get; set; }
        public string PAGE_SIZE { get; set; }
        public string HEADER_HEIGHT { get; set; }
        public string FOOTER_HEIGHT { get; set; }
        public string WATERMARK { get; set; }
        public List<TempEmailDTModel> DETAIL { get; set; }
    }
    public class TempSMSModel
    {
        public string TEMP_CODE { get; set; }
        public string PARAM_REPLACE { get; set; }
        public string DATA { get; set; }
        public string DESCRIPTION { get; set; }
        public string IS_ACTIVE { get; set; }
        public string DATA_REG { get; set; }
        public string TEMP_CONFIG { get; set; }
        public string TYPE_SEND { get; set; }
        public string SEND_NOW { get; set; }
        public string ISCALLBACK { get; set; }
        public string MAX_API { get; set; }
    }
    public class TempEmailDTModel
    {
        public string DETAIL_ID { get; set; }
        public string TEMP_CODE { get; set; }
        public string DATA { get; set; }
        public string LIST_REPLACE { get; set; }
        public string PARAM_NAME { get; set; }
        public string TEMP_ID { get; set; }
        public string DATA_TYPE { get; set; }
    }
    public class TempSignModel
    {
        public string ID_KEY { get; set; }
        public string USER_CODE { get; set; } //thiết bị
        public string IS_GOVERMENT { get; set; }
        public string IS_DEFAULT { get; set; }
        public string LEVLE { get; set; }
        public string TEMP_CODE { get; set; } //thiết bị
        public string DEFAULT_LOCATION { get; set; }
        public string TEXT_CA { get; set; }
        public string X { get; set; }
        public string Y { get; set; } //thiết bị
        public string ORG_CODE { get; set; }
        public string SERIAL { get; set; }
        public string NAME { get; set; }
        public string PASSWORD { get; set; } //thiết bị
        public string PATH { get; set; }
        public string DATA_FILE { get; set; }
        public string S_IMAGE { get; set; }
        public string M_IMAGE { get; set; } //thiết bị
        public string F_IMAGE { get; set; }
        public string CER_FILE { get; set; }
        public string IS_VERYFY { get; set; }
        public string IMAGE { get; set; }
        public string PRODUCT_CODE { get; set; }
        
    }
    public enum TEMP_TYPE
    {
        EMAIL,
        SMS,
        ZALO,
        VIBER,
        TELE,
        FACE
    }
    public enum TEMP_CODE
    {
        GCN,
        CONTENT
    }
    public enum TYPE_SEND
    {
        ALL,
        ALLNO,
        ONE,
        ONENO,
        ALLONE
    }
    public enum DATA_TYPE
    {
        DOC,
        HTML,
        PDF
    }

    public class NotifyConfigModel
    {
        public string ID { get; set; }
        public string ORG_CODE { get; set; }
        public int MAX { get; set; }
        public string TEMP_ID { get; set; }
        public string IS_ACTIVE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string DESCRIPTION { get; set; }
        public string NOTIFY_TYPE { get; set; }
        public string SEND_TYPE { get; set; }
    }
    public class NotifyLogModel
    {
        public string CONFIG_ID { get; set; }
        public int COUNT { get; set; }
        public string TYPE_DATA { get; set; }
        public string VALUE_DATA { get; set; }
        public string ADDRESS { get; set; }
        public string SEND_DATA { get; set; }
        public string STATUS { get; set; }
    }
}
