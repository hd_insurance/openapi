﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    public class GenPaymentModels
    {
        public string INS_CONTRACT { get; set; }
        public List<PaymentInfo> lstPayInfo { get; set; }
        public List<PaymentGateway> lstPayGateway { get; set; }
        //public List<Define> lstDefine { get; set; }
        
    }

    public class PaymentInfo
    {
        
        public string Total_Amount { get; set; }
        public string Total_Amount_Text { get; set; }
        public string Amount { get; set; }
        public string VAT { get; set; }
        public string Gif_Code { get; set; }
        public string Discount { get; set; }
        public string Transaction_Id { get; set; }
        public string Payer_Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Status_Name { get; set; }
        public string Date_Pay { get; set; }
        public string Path_Logo { get; set; }
        public string Path_Banner { get; set; }
        public string Qr_Code { get; set; }
        public string Qr_Expire { get; set; }
        public string Min_Amount { get; set; }
        public string Max_Amount { get; set; }
        public string Currency { get; set; }
        public string Contract_Code { get; set; }
        public string Description_Min { get; set; }
        public string Description_Max { get; set; }
        public string Recipient { get; set; } = "Công ty TNHH Bảo Hiểm HD";
    }

    public class PaymentGateway
    {
        public string Org_Code { get; set; }
        public string Org_Name { get; set; }
        public string Path_Logo { get; set; }
        public string Min_Amount { get; set; }
        public string Max_Amount { get; set; }
        public string Description_Min { get; set; }
        public string Description_Max { get; set; }
        public bool Disable { get; set; }
        public string Total_Pay { get; set; }
        public string LinkPay { get; set; }
    }

    public class Define
    {
        public string PROJECT_CODE { get; set; }
        public string DEFINE_CODE { get; set; }
        public string DEFINE_NAME { get; set; }
        public string TYPE_CODE { get; set; }
        public string TYPE_NAME { get; set; }
    }
}
