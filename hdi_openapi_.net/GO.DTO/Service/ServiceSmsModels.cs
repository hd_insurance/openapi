﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Service
{
    class ServiceSmsModels
    {
    }
    public class RESULT_SMS
    {
        public int error { get; set; }
        public string error_description { get; set; }
        public string MessageId { get; set; }
        public string PartnerId { get; set; }
        public string Telco { get; set; }
        public string CampaignCode { get; set; }
        /// <summary>
        /// MessageId = BatchId + số ĐT (84xxx)
        /// </summary>
        public string BatchId { get; set; }
        /// <summary>
        /// Số lượng tin nhắn đã gửi
        /// </summary>
        public int NumMessageSent { get; set; }
        /// <summary>
        /// Số lượng tin nhắn còn lại
        /// </summary>
        public int NumRemainQuota { get; set; }
        public string SMS_SUCCESS { get; set; }
        public string SMS_ERROER { get; set; }
        //public string access_token { get; set; }
    }
    public class SEND_SMS
    {
        public string funtion { get; set; }
        public string scop { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
        public string session_id { get; set; }

        public string BrandName { get; set; }
        /// <summary>
        /// Hàm khởi tạo các tham số để gửi sms fpt
        /// </summary>        
        /// <param name="_funtion">Tên của hàm được fpt cung cấp.</param>
        /// <param name="_scop">Phạm vi hàm được fpt cung cấp khi gửi sms</param>
        /// <param name="_Phone">Số điện thoại của người nhận SMS</param>
        /// <param name="_Message">Tin nhắn gửi. Nếu là tiếng việt người nhận sẽ không có dấu</param>        
        public SEND_SMS(string _funtion, string _scop, string _Phone, string _Message)
        {
            this.funtion = _funtion;
            this.scop = _scop;
            this.Phone = _Phone;
            this.Message = _Message;
        }
    }

    public class SMS_CONTENT
    {
        public string DATA { get; set; }
        public string ORG_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string TYPE { get; set; }

        public string DATE { get; set; }
        public string CONTENT { get; set; }
        public string PHONE { get; set; }
        public string REF_ID { get; set; }
        public string TOKEN { get; set; }

    }
    public class CAM_CREATE
    {
        public string access_token { get; set; }
        public string session_id { get; set; }
        public string CampaignName { get; set; }
        public string BrandName { get; set; }
        public string Message { get; set; }
        public string funtion { get; set; }
        public string scop { get; set; }
        /// <summary>
        /// Ngày giờ gửi tin nhắn định dạng yyyy-mm-dd HH:ii (8h->11h30, 13h30->20h)
        /// </summary>
        public string ScheduleTime { get; set; }
        public int Quota { get; set; }
        /// <summary>
        /// Hàm khởi tạo các tham số để gửi sms fpt
        /// </summary>
        /// <param name="_funtion">Token trả về khi gọi hàm createToken.</param>
        /// <param name="_scop">Tham số dùng trong hàm createToken.</param>
        /// <param name="_CampaignName">Tên của đợt khuyến mại, quảng cáo</param>
        /// <param name="_Message">Tin nhắn gửi. Nếu là tiếng việt người nhận sẽ không có dấu</param>   
        /// <param name="_ScheduleTime">Thời gian gửi đến KH(yyyy-mm-dd HH:ii) (8h->11h30, 13h30->20h)</param>
        /// <param name="_Quota">Số lượng tin nhắn trong 1 Campaign</param>
        public CAM_CREATE(string _funtion, string _scop, string _CampaignName,
             string _Message, string _ScheduleTime, int _Quota)
        {
            this.funtion = _funtion;
            this.scop = _scop;
            this.CampaignName = _CampaignName;
            this.Message = _Message;
            this.ScheduleTime = _ScheduleTime;
            this.Quota = _Quota;
        }
        public CAM_CREATE() { }
    }
    public class CAM_SEND
    {
        public string access_token { get; set; }
        public string session_id { get; set; }
        /// <summary>
        /// Tham số CamCode nhận về khi gọi hàm tạo Campaign
        /// </summary>
        public string CampaignCode { get; set; }
        /// <summary>
        /// Danh sách số ĐT nhận, cách nhau bởi dấu "," (84xxx, 0xxx)
        /// </summary>
        public string PhoneList { get; set; }
        public string funtion { get; set; }
        public string scop { get; set; }
        /// <summary>
        /// Hàm khởi tạo các tham số để gửi sms fpt
        /// </summary>
        /// <param name="_access_token">Token trả về khi gọi hàm createToken.</param>
        /// <param name="_session_id">Tham số dùng trong hàm createToken.</param>
        /// <param name="_CampaignCode">Code fpt gửi khi gọi hàm creatCam</param>
        /// <param name="_PhoneList">Danh sách số ĐT nhận, cách nhau bởi dấu "," (84xxx, 0xxx)</param>
        public CAM_SEND(string _funtion, string _scop, string _CampaignCode, string _PhoneList)
        {
            this.funtion = _funtion;
            this.scop = _scop;
            this.CampaignCode = _CampaignCode;
            this.PhoneList = _PhoneList;
        }


        public CAM_SEND() { }
    }
    public class CAM_CANCEL
    {
        public string access_token { get; set; }
        public string session_id { get; set; }
        public string CampaignCode { get; set; }
        public string funtion { get; set; }
        public string scop { get; set; }
        /// <summary>
        /// Hàm khởi tạo các tham số để gửi sms fpt
        /// </summary>
        /// <param name="_access_token">Token trả về khi gọi hàm createToken.</param>
        /// <param name="_session_id">Tham số dùng trong hàm createToken.</param>
        /// <param name="_CampaignCode">Code fpt gửi khi gọi hàm creatCam</param>
        public CAM_CANCEL(string _funtion, string _scop, string _CampaignCode)
        {
            this.funtion = _funtion;
            this.scop = _scop;
            this.CampaignCode = _CampaignCode;
        }
        public CAM_CANCEL() { }
    }
}
