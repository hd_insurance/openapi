﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CRM
{
    public class RequiredModel
    {
        public string DOMAIN { get; set; }
        public string REQUIRED_TYPE { get; set; }
        public string REQUESTER_NAME { get; set; }
        public string GENDER { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string CITY { get; set; }
        public string ENTERPRISE_NAME { get; set; }
        public string CAREER { get; set; }
        public string CONTENT { get; set; }
        public string LOCATION { get; set; }
        public string FIELD { get; set; }
        public string WARDS_CODE { get; set; }
        public string DISTRICT_CODE { get; set; }
        public string CITY_CODE { get; set; }
        public string ADDRESS { get; set; }
    }
}
