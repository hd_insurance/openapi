﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Voucher
{
    public class VoucherInsur
    {
        
    }

    public class VoucherInsurFree
    {
        public string channel { get; set; }
        public string userName { get; set; }
        public string otp { get; set; }
        public string giftCode { get; set; }
        public string name_title { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string idCard { get; set; }
        public string dob { get; set; }
        public string email { get; set; }
        public string prov_code { get; set; }
        public string dist_code { get; set; }
        public string wards_code { get; set; }
        public string address { get; set; }
        public DataConfirm confirm { get; set; }
        public bool isPolicy { get; set; }
        public string policyCode { get; set; }
    }

    public class DataConfirm
    {
        public string code { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }

    public class VoucherInfo
    {
        public string gift_code { get; set; }
        public string block_code { get; set; }
        public string activation_code { get; set; }
        public string campaign_code { get; set; }
        public string org_code { get; set; }
        public string channel { get; set; }
        public double discount { get; set; }
        public string discount_unit { get; set; }
        public string category { get; set; }
        public string product_code { get; set; }
        public string pack_code { get; set; }
        public string status { get; set; }
        public string effective_date { get; set; }
        public string expriration_date { get; set; }
        public double age_insured { get; set; }
        public double age_from { get; set; }
        public double age_to { get; set; }
        public string org_name { get; set; }
        public string address { get; set; }

    }
}
