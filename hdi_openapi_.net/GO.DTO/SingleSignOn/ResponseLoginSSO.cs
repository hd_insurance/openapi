﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SingleSignOn
{
    public class ResponseLoginSSO
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public string Token { get; set; }
        public int Expries { get; set; }
        public string RefeshToken { get; set; }
        public string Signature { get; set; }
        public object UserInfo { get; set; }
    }
}
