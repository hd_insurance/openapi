﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SingleSignOn
{
    public class UserSSO
    {
        public string USER_NAME { get; set; }
        public string STRUCT_CODE { get; set; }
        public string ORG_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public bool IS_CHANGE_PWD { get; set; }
    }
}
