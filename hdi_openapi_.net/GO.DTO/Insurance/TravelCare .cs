﻿using GO.DTO.Insurance.Common;
using GO.DTO.Insurance.Travel;
using GO.DTO.Org.Vietjet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class TravelCare
    {
        /// <summary>
        /// Thông tin chuyến du lịch
        /// </summary>
        /// Danh sách người được bảo hiểm
        /// </summary>
        public List<TravelInsur> INSURED { get; set; }
        /// <summary>
        /// File chung cho contract, Người mua
        /// </summary>
        public TravelInfo TRAVEL_INFO { get; set; }
    }
}
