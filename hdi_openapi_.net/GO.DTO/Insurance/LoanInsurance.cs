﻿using GO.DTO.Insurance.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class LoanInsurance
    {
        public List<LoInsured> INSURED { get; set; }
    }

    public class LoInsured : InsuredPerson
    {
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        public string LO_CODE { get; set; }
        /// <summary>
        /// Mã hđ tín dụng từ bank
        /// </summary>
        public string LO_BANK_CODE { get; set; }
        /// <summary>
        /// Số hđ tín dụng
        /// </summary>
        public string LO_CONTRACT { get; set; }
        /// <summary>
        /// TOTAL: Bảo hiểm trên dư nợ gốc
        /// DECREASE: Bảo hiểm trên dư nợ giảm dần
        /// DISBUR: Bảo hiểm trên KUNN
        /// </summary>
        public string LO_TYPE { get; set; }
        /// <summary>
        /// Loại HĐ tín dụng
        /// TRUC_TIEP: Hợp đồng trực tiếp 1-1. Thanh toán 1 lần duy nhất ứng với 1 HĐ bảo hiểm
        /// HD_BAO: Hợp đồng bao 1-n. 1 HĐ tín dụng với 1 HĐ bảo hiểm bao và n lần giải ngân ứng với n lần bảo hiểm
        /// LINH_DONG: Hợp đồng linh động n-n. 1 HĐ tín dụng ứng với n bảo hiểm, với mỗi lần giải ngân là 1 hđ bảo hiểm
        /// </summary>
        public string LO_MODE { get; set; }
        /// <summary>
        /// Người thanh toán
        /// C: Customer
        /// B: Bank
        /// O: Other -> sau
        /// </summary>
        public string PAYER { get; set; } = "C";
        public string CURRENCY { get; set; }
        public double? LO_TOTAL_AMOUNT { get; set; } = 0;
        public string LO_EFFECTIVE_DATE { get; set; }
        public string LO_EXPIRATION_DATE { get; set; }
        /// <summary>
        /// ngày ký kết
        /// </summary>
        public string LO_DATE { get; set; }
        public string INTEREST_RATE { get; set; }
        public double? DURATION { get; set; } = 0;
        public string DURATION_UNIT { get; set; }

        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; } = 0;
        public Disbur DISBUR { get; set; }
        public List<InsurFiles> FILES { get; set; }
    }
}
