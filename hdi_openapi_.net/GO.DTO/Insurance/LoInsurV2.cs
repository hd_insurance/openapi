﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.BANK_LO;
using GO.DTO.Insurance.Common;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class LoProduct : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }

        // thông tin khoản vay
        /// <summary>
        /// ID khoản vay phía BANK
        /// </summary>
        public string LO_ID { get; set; }
        /// <summary>
        /// Mã hd tín dụng phía HDI
        /// </summary>
        public string LO_CODE { get; set; }
        /// <summary>
        /// Số hđ tín dụng
        /// </summary>
        public string LO_CONTRACT { get; set; }
        /// <summary>
        /// TOTAL: Bảo hiểm trên dư nợ gốc
        /// DECREASE: Bảo hiểm trên dư nợ giảm dần
        /// DISBUR: Bảo hiểm trên KUNN
        /// </summary>
        public string LO_TYPE { get; set; }
        /// <summary>
        /// Số tiền vay
        /// </summary>
        public double? LO_TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Hiệu lực vay
        /// </summary>
        public string LO_EFF_DATE { get; set; }
        /// <summary>
        /// Hết hiệu lực vay
        /// </summary>
        public string LO_EXP_DATE { get; set; }
        /// <summary>
        /// ngày ký kết
        /// </summary>
        public string LO_DATE { get; set; }
        /// <summary>
        /// Thời hạn vay
        /// </summary>
        public double? DURATION { get; set; } = 0;
        /// <summary>
        /// Đơn vị thời hạn vay
        /// </summary>
        public string DURATION_UNIT { get; set; }
        
        /// <summary>
        /// Hợp đồng vay bắt buộc có bảo hiểm
        /// R: Yêu cầu bắt buộc
        /// N: Không yêu cầu bắt buộc
        /// </summary>
        public string LO_STATE { get; set; }
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        public string CURRENCY { get; set; }

        // Thông tin KUNN
        public DisburV2 DISBUR { get; set; }

        // Start 2022-05-31 By ThienTVB
        // Mã tài sản
        public string ASSET_CODE { get; set; }
        // Thông tin House
        public HouseInfoLo HOUSE_INFO { get; set; }
        // End 2022-05-31 By ThienTVB

        // Thông tin sản phẩm BH

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        /// <summary>
        /// Mã gói
        /// </summary>
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Địa lý mua
        /// </summary>
        public string REGION { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFF_DATE { get; set; }
        /// <summary>
        /// Ngày hết hiệu lực
        /// </summary>
        public string EXP_DATE { get; set; }
        public double FEES { get; set; } = 0;
        /// <summary>
        /// Số tiền của hợp đồng bảo hiểm
        /// </summary>
        public double AMOUNT { get; set; }
        /// <summary>
        /// Số tiền giảm
        /// </summary>
        public double? DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Đơn vị giảm
        /// </summary>
        public string DISCOUNT_UNIT { get; set; }
        /// <summary>
        /// Tổng giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_ADDITIONAL { get; set; } = 0;
        /// <summary>
        /// số tiền thuế
        /// </summary>
        public double? VAT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; }
        public string GIF_CODE { get; set; }
        public string DURATION_PAYMENT { get; set; }

        /// <summary>
        /// Danh sách kỳ thanh toán
        /// </summary>
        public List<PaymentPeriod> PERIOD { get; set; }

        public List<DetailFile> FILE { get; set; }

        // thông tin thụ hưởng

        /// <summary>
        /// Danh sách người thụ hưởng
        /// </summary>
        public List<Beneficiary> BENEFICIARY { get; set; }
    }

    public class DisburV2
    {
        public string BRANCH_CODE { get; set; }
        public string LOD_CODE { get; set; }
        public string DISBUR_CODE { get; set; }
        public string DISBUR_NUM { get; set; }
        public string DISBUR_DATE { get; set; }
        public double DISBUR_AMOUNT { get; set; } = 0;
        public double INSUR_TOTAL { get; set; } = 0;
    }

    public class OrdLoInsur
    {
        public List<LoProduct> INSURED { get; set; }
    }
}
