﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Vehicle
{
    /// <summary>
    /// Thông tin xe
    /// </summary>
    public class VehicleInfo
    {
        public string VEH_CODE { get; set; }
        /// <summary>
        /// Nhóm xe
        /// </summary>
        public string VEHICLE_GROUP { get; set; }
        /// <summary>
        /// Loại xe
        /// </summary>
        public string VEHICLE_TYPE { get; set; }
        /// <summary>
        /// Mục dích sử dụng
        /// </summary>
        public string VEHICLE_USE { get; set; }
        /// <summary>
        /// 1: chưa có biển số
        /// 0: có biển số
        /// </summary>
        public string NONE_NUMBER_PLATE { get; set; }
        /// <summary>
        /// Biển số xe
        /// </summary>
        public string NUMBER_PLATE { get; set; }
        /// <summary>
        /// Số khung
        /// </summary>
        public string CHASSIS_NO { get; set; }
        /// <summary>
        /// Số máy
        /// </summary>
        public string ENGINE_NO { get; set; }
        /// <summary>
        /// Số chỗ ngồi
        /// </summary>
        public string SEAT_NO { get; set; }
        /// <summary>
        /// Trọng tải
        /// </summary>
        public string WEIGH { get; set; }
        /// <summary>
        /// Hãng xe
        /// </summary>
        public string BRAND { get; set; }
        /// <summary>
        /// Hiệu xe
        /// </summary>
        public string MODEL { get; set; }
        /// <summary>
        /// Năm sản xuất
        /// </summary>
        public string MFG { get; set; }
        /// <summary>
        /// Dung tích
        /// </summary>
        public string CAPACITY { get; set; }
        /// <summary>
        /// Giá trị xe trên thị trường
        /// </summary>
        public string REF_VEHICLE_VALUE { get; set; }
        /// <summary>
        /// Ngày đăng ký xe
        /// </summary>
        public string VEHICLE_REGIS { get; set; }
        /// <summary>
        /// Giá trị xe
        /// </summary>
        public string VEHICLE_VALUE { get; set; }
    }
}
