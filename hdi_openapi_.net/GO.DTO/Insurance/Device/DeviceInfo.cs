﻿using System.Collections.Generic;

namespace GO.DTO.Insurance.House
{
    public class DeviceInfo
    {
        /// <summary>
        /// Screen, camera,...
        /// </summary>
        public string DEVICE_TYPE { get; set; }
        /// <summary>
        /// Hãng thiết bị
        /// </summary>
        public string BRAND { get; set; }
        /// <summary>
        /// Dòng thiết bị
        /// </summary>
        public string MODEL { get; set; }
        /// <summary>
        /// IMEI/Serial thiết bị
        /// </summary>
        public string IMEI { get; set; }
        /// <summary>
        /// Giá trị điện thoại
        /// </summary>
        public int DEVICE_VALUE { get; set; }
        /// <summary>
        /// Giá trị thiết bị
        /// </summary>
        public string DESCRIPTION { get; set; }
        /// <summary>
        /// Danh sách URL hình ảnh giám định điện thoại 
        /// </summary>
        public List<string> URL { get; set; }
        /// <summary>
        /// Danh sách URL hình ảnh giám định điện thoại 
        /// </summary>
        public string URL_IMAGE_STRING { get; set; }
    }
}
