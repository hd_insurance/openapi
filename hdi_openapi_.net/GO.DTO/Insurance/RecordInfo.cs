﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class RecordInfo
    {
    }

    public class PRODUCT_RECORD
    {
        public string INPUT_JSON { get;set;}
        public string URL { get;set;}
    }

    public class REQ_QR
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string LANG { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string TYPE_RECORD { get; set; }
        public object OBJ_DATA { get; set; }
        
    }
}
