﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.BANK_LO
{
    public class BankSearchStatus
    {
        public string LO_CONTRACT { get; set; }
        public string DISBUR_CODE { get; set; }
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
    }
    // Start 2022-06-17 By ThienTVB
    public class BankSearchStatusByLoId
    {
        public string LO_ID { get; set; }
        //public string PRODUCT_CODE { get; set; }
    }
    // End 2022-06-17 By ThienTVB
}
