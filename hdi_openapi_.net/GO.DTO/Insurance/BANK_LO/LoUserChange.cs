﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.BANK_LO
{
    public class LoUserChange
    {
        public string USER_NAME { get; set; }
        public string PASSWORD { get; set; }
        public string IS_INS { get; set; }
        public string IS_UPD { get; set; }
    }
}
