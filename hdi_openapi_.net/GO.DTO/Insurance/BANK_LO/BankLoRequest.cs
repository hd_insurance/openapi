﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GO.DTO.Base;

namespace GO.DTO.Insurance.BANK_LO
{
    public class BankLoRequest
    {
        [Required(AllowEmptyStrings =false)]
        public string USER_NAME { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string CHANNEL { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string PRODUCT_CODE { get; set; }
        [Required(AllowEmptyStrings = false)]
        public BankSellerInfo SELLER_INFO { get; set; }
        [Required(AllowEmptyStrings = false)]
        public BankLoContract LO_INFO { get; set; }
    }

    /// <summary>
    /// Thông tin người bán
    /// </summary>
    public class BankSellerInfo
    {
        [Required(AllowEmptyStrings = false)]
        public BankBranchInfo BRANCH_INFO { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string SELLER_CODE { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string SELLER_NAME { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string SELLER_EMAIL { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string SELLER_PHONE { get; set; }
        public string SELLER_GENDER { get; set; }
    }

    /// <summary>
    /// Thông tin chi nhánh người bán
    /// </summary>
    public class BankBranchInfo
    {
        [Required(AllowEmptyStrings = false)]
        public string BANK_CODE { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Mã chi nhánh cấp đơn
        /// </summary>
        public string BRANCH_CODE { get; set; }
        /// <summary>
        /// Tên chi nhánh cấp đơn
        /// </summary>
        public string BRANCH_NAME { get; set; }

        [Required(AllowEmptyStrings = false)]
        // Danh sách parent của BRANCH_CODE
        public List<BankBranchParent> ARR_PARENT { get; set; }
    }

    public class BankBranchParent
    {
        [Required(AllowEmptyStrings = false)]
        public string CODE { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string NAME { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string PARENT_CODE { get; set; }
    }

    /// <summary>
    /// Thông tin hợp đồng vay:
    /// - Thông tin KH vay
    /// - Thông tin HĐ vay
    /// </summary>
    public class BankLoContract : CustomerV2
    {
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// ID hợp đồng vay (Bank)
        /// </summary>
        public string LO_ID { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// HỢP ĐỒNG VAY
        /// </summary>
        public string LO_CONTRACT { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// TOTAL: Bảo hiểm trên dư nợ gốc
        /// DECREASE: Bảo hiểm trên dư nợ giảm dần
        /// DISBUR: Bảo hiểm trên KUNN
        /// </summary>
        public string LO_TYPE { get; set; }
        /// <summary>
        /// ngày ký HĐTD
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string LO_DATE { get; set; }
        [Required(AllowEmptyStrings = false)]
        [RegularExpression(@"^[0-9]{1,38}$", ErrorMessage = "Là số từ 1 đến 38 chữ số")]
        /// <summary>
        /// Số tiền vay
        /// </summary>
        public double LO_TOTAL { get; set; } = 0;
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Thời hạn vay
        /// </summary>
        public int DURATION { get; set; }
        /// <summary>
        /// Đơn vị thời hạn vay
        /// </summary>
        public string DURATION_UNIT { get; set; }
        public string CURRENCY { get; set; } = "VND";
        /// <summary>
        /// Người thanh toán
        /// C: Customer
        /// B: Bank
        /// O: Other -> sau
        /// </summary>
        public string PAYER { get; set; } = "C";

        /// <summary>
        /// Hợp đồng vay bắt buộc có bảo hiểm
        /// R: Yêu cầu bắt buộc
        /// N: Không yêu cầu bắt buộc
        /// </summary>
        public string LO_STATE { get; set; }

        //[Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Thông tin giải ngân
        /// - Toàn HM: 1 lần giải ngân
        /// - KUNN: n lần giải ngân
        /// </summary>
        public BankLoDisbur DISBUR { get; set; }
        // Start 2022-05-30 By ThienTVB
        /// <summary>
        /// Mã tài sản
        /// </summary>
        public string ASSET_CODE { get; set; }
        /// <summary>
        /// Thông tin ngôi nhà vay
        /// </summary>
        public HouseInfoLo HOUSE_INFO { get; set; }
        // End 2022-05-30 By ThienTVB
    }

    /// <summary>
    /// Thông tin giải ngân
    /// </summary>
    public class BankLoDisbur
    {
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Mã id giải ngân
        /// </summary>
        public string DISBUR_CODE { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// số lần giải ngân
        /// </summary>
        public string DISBUR_NUM { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Ngày giải ngân
        /// </summary>
        public string DISBUR_DATE { get; set; }
        [Required(AllowEmptyStrings = false)]
        [RegularExpression(@"^[0-9]{1,38}$", ErrorMessage = "Là số từ 1 đến 38 chữ số")]
        /// <summary>
        /// Số tiền giải ngân
        /// </summary>
        public double DISBUR_AMOUNT { get; set; } = 0;
    }

    public class CustomerV2 : PersonV2
    {
        /// <summary>
        /// Loại: KH. (CQ: Cơ quan, CN: Cá nhân)
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string TYPE { get; set; }
    }

    public class PersonV2
    {
        //public string ID { get; set; }
        /// <summary>
        /// Quốc tịch
        /// </summary>
        public string NATIONALITY { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Tên
        /// </summary>
        public string NAME { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Ngày sinh
        /// </summary>
        public string DOB { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Giới tính (M: nam, F: nữ)
        /// </summary>
        public string GENDER { get; set; }
        /// <summary>
        /// Mã tỉnh
        /// </summary>
        public string PROV { get; set; }
        /// <summary>
        /// Mã huyện
        /// </summary>
        public string DIST { get; set; }
        /// <summary>
        /// mã xã
        /// </summary>
        public string WARDS { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// địa chỉ
        /// </summary>
        public string ADDRESS { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// CMND
        /// </summary>
        public string IDCARD { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Ngày cấp
        /// </summary>
        public string IDCARD_D { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Nơi cấp
        /// </summary>
        public string IDCARD_P { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Email
        /// </summary>
        public string EMAIL { get; set; }
        [Required(AllowEmptyStrings = false)]
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string PHONE { get; set; }
    }
    public class HouseInfoLo
    {
        /// <summary>
        /// Loại nhà
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string HOUSE_TYPE { get; set; }
        /// <summary>
        /// Giá trị nhà
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public double HOUSE_VALUE { get; set; }
        /// <summary>
        /// Giá trị bảo hiểm nhà
        /// </summary>
        public double INSUR_HOUSE_VALUE { get; set; } = 0;
        /// <summary>
        /// Lo Details Code
        /// </summary>
        public string LOAD_CODE { get; set; } = "";
        /// <summary>
        /// Lo HOUSE Address
        /// </summary>
        [Required(AllowEmptyStrings = false)]
        public string HOUSE_ADD { get; set; }
    }
}
