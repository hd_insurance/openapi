﻿using GO.DTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class ContractLoans
    {
        /// <summary>
        /// HỢP ĐỒNG VAY
        /// </summary>
        public string LO_CONTRACT { get; set; }
        public string LO_TYPE { get; set; }
        /// <summary>
        /// Loại bảo hiểm
        /// BAO_HIEM_MOI: Cấp mới đơn bảo hiểm
        /// BAO_HIEM_SDBS: Sửa đổi bổ sung
        /// </summary>
        public string INS_TYPE { get; set; } = "";
        /// <summary>
        /// Số hợp đồng BH
        /// </summary>
        public string INS_CONTRACT { get; set; }
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        /// <summary>
        /// ngày ký HĐTD
        /// </summary>
        public string LO_DATE { get; set; }
        public string LO_EFF_DATE { get; set; }
        public string LO_EXP_DATE { get; set; }
        public int DURATION { get; set; }
        public string UNIT { get; set; }
        public int LO_TOTAL { get; set; }
        public string INTEREST_RATE { get; set; }
        public string CURRENCY { get; set; } = "VND";

        /// <summary>
        /// Mã giao dịch viên
        /// lần 1: brand_code: mã chi nhánh nhân viên bank
        /// lần 2: khác brand_code: => nhân viên chuyển chi nhánh => doanh thu tính cho đơn vị khác.
        /// </summary>
        public string TELLER_CODE { get; set; }
        public string TELLER_NAME { get; set; }
        public string TELLER_EMAIL { get; set; }
        public string TELLER_PHONE { get; set; }
        public string TELLER_GENDER { get; set; }
        public string CIF { get; set; }
        public string TYPE { get; set; } = "";
        public string NAME { get; set; }
        public string DOB { get; set; }
        public string GENDER { get; set; }
        public string PROV { get; set; }
        public string DIST { get; set; }
        public string WARDS { get; set; }
        public string ADD { get; set; }
        public string IDCARD { get; set; }
        public string IDCARD_D { get; set; }
        public string IDCARD_P { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string TAXCODE { get; set; }
        /// <summary>
        /// SỐ TIỀN MUỐN BẢO HIỂM
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; }
        public string EFF_DATE { get; set; }
        public string EXP_DATE { get; set; }
        public Disbur DISBUR { get; set; }

        // response cent
        public string so_id { get; set; }
        public string so_id_dt { get; set; }
        public string so_hd { get; set; }
        public string nv { get; set; }
        public string goi_bh { get; set; }
    }

    public class ContractLoansResponse
    {
        public string url { get; set; }
        public List<KeyValModel> header { get; set; }

    }
}
