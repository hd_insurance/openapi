﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;

namespace GO.DTO.Insurance
{
    public class LoContract : Customer
    {
        #region Teller Info
        /// <summary>
        /// Mã giao dịch viên
        /// lần 1: brand_code: mã chi nhánh nhân viên bank
        /// lần 2: khác brand_code: => nhân viên chuyển chi nhánh => doanh thu tính cho đơn vị khác.
        /// </summary>
        public string TELLER_CODE { get; set; }
        public string TELLER_NAME { get; set; }
        public string TELLER_EMAIL { get; set; }
        public string TELLER_PHONE { get; set; }
        public string TELLER_GENDER { get; set; }
        #endregion

        #region Lo Info
        /// <summary>
        /// HỢP ĐỒNG VAY
        /// </summary>
        public string LO_CONTRACT { get; set; }
        /// <summary>
        /// TOTAL: Bảo hiểm trên dư nợ gốc
        /// DECREASE: Bảo hiểm trên dư nợ giảm dần
        /// DISBUR: Bảo hiểm trên KUNN
        /// </summary>
        public string LO_TYPE { get; set; }
        /// <summary>
        /// TRUC_TIEP: Hợp đồng bảo hiểm trên toàn hạn mức: 1HDTD - 1HDBH
        /// HD_BAO: Bảo hiểm trên hợp đồng bao: 1HDTD - 1HDBH và 1 KUNB - 1HDBHCT
        /// LINH_DONG: Bảo hiểm trên hợp đồng bao: 1HDTD - nHDBH và 1 KUNB - 1HDBHCT
        public string LO_MODE { get; set; }
        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        /// <summary>
        /// ngày ký HĐTD
        /// </summary>
        public string LO_DATE { get; set; }
        public string LO_EFF_DATE { get; set; }
        public string LO_EXP_DATE { get; set; }
        public int LO_TOTAL { get; set; }
        public int DURATION { get; set; }
        public string UNIT { get; set; }
        public string CURRENCY { get; set; } = "VND";
        /// <summary>
        /// Người thanh toán
        /// C: Customer
        /// B: Bank
        /// O: Other -> sau
        /// </summary>
        public string PAYER { get; set; } = "C";
        #endregion
        public Disbur DISBUR { get; set; }
    }
    public class Disbur
    {
        public string BRANCH_CODE { get; set; }
        public string DISBUR_CODE { get; set; }
        public string DISBUR_NUM { get; set; }
        public string DISBUR_DATE { get; set; }
        public double DISBUR_AMOUNT { get; set; } = 0;
        public double INSUR_TOTAL { get; set; } = 0;
    }
    /// <summary>
    /// từng lần nhận nợ hoặc đơn dư nợ giảm dần
    /// </summary>
    public class Lo_attribute
    {
        public string LO_CONTRACT { get; set; }
        /// <summary>
        /// Số hợp đồng BH
        /// </summary>
        public string INS_CONTRACT { get; set; }
        /// <summary>
        /// SỐ TIỀN MUỐN BẢO HIỂM Với trường hợp dư nợ giảm dần - thời gian sẽ đc set block khi assign từng đối tác
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; }
        /// <summary>
        /// Với trường hợp từng lần nhận nợ
        /// </summary>
        public Disbur DISBUR { get; set; }
    }
    
    public class Lo_Disbur_Status
    {
        /// <summary>
        /// HỢP ĐỒNG VAY
        /// </summary>
        public string LO_CONTRACT { get; set; }
        public string DISBUR_CODE { get; set; }
        /// <summary>
        /// DD/MM/YYYY HH24:MI:SS
        /// </summary>
        public string DISBUR_DATE { get; set; }
    }

    public class Lo_Decrease
    {
        /// <summary>
        /// HỢP ĐỒNG VAY
        /// </summary>
        public string LO_CONTRACT { get; set; }
        public double INSUR_TOTAL { get; set; }
    }
}
