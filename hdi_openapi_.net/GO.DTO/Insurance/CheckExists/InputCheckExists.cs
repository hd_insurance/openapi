﻿using GO.DTO.Insurance.Vehicle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.CheckExists
{
    public class InputCheckExists
    {
        /// <summary>
        /// Đối tác bán
        /// </summary>
        public string ORG_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string EFF { get; set; }
        public VehicleInfo VEHICLE_INFO { get; set; }
    }
}
