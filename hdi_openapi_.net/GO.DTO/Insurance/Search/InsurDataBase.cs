﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.BANK_LO;
using GO.DTO.Insurance.Common;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Search
{
    public class InsurDataBase : ORD_BUYER
    {
        public string CONTRACT_CODE { get; set; }
        public string CONTRACT_NO { get; set; }
        //public string STATUS { get; set; }
        //public string STATUS_NAME { get; set; }
        public int TOTAL { get; set; }
        public int PAGE_SIZE { get; set; }
        public int PAGE_CURRENT { get; set; }
        public List<InsurDetailBase> INSURED { get; set; }
    }

    public class InsurDetailBase : Customer
    {
        public string DETAIL_CODE { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string CERTIFICATE_NO { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public double INSUR_TOTAL { get; set; } = 0;
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }

        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Danh sách người thụ hưởng
        /// </summary>
        public List<Beneficiary> BENEFICIARY { get; set; }
        /// <summary>
        /// Địa lý
        /// </summary>
        public string REGION { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFFECTIVE_DATE { get; set; }
        /// <summary>
        /// Ngày hết hiệu lực
        /// </summary>
        public string EXPIRATION_DATE { get; set; }
        /// <summary>
        /// Danh sách điều khoản mở rộng
        /// </summary>
        public List<Additional> ADDITIONAL { get; set; }
        /// <summary>
        /// Tổng phí điều khoản mở rộng
        /// </summary>
        public double ADDITIONAL_FEES { get; set; }
        /// <summary>
        /// Phí bán của sản phẩm
        /// </summary>
        public double AMOUNT { get; set; }
        /// <summary>
        /// Tổng tiền giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; }
        /// <summary>
        /// số tiền thuế
        /// </summary>
        public double? VAT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; }
        public string URL_GCN { get; set; }
        public string DATE_SIGN { get; set; }
        /// <summary>
        /// Mã cán bộ cấp phía HDI
        /// </summary>
        public string SELLER_CODE { get; set; }

        public BankLoDetail LO_INFO { get; set; }
    }

    public class BankLoDetail : BankLoContract
    {
        /// <summary>
        /// Mã cán bộ tư vấn phía BANK
        /// </summary>
        public string SELLER_CODE { get; set; }
        public string BANK_CODE { get; set; }
        /// <summary>
        /// Mã chi nhánh cấp đơn
        /// </summary>
        public string BRANCH_CODE { get; set; }
    }
}
