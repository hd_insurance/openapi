﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Search
{
    public class ObjSear
    {
        public string CHANNEL { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        public string FROM { get; set; }
        public string TO { get; set; }
        public string STATUS { get; set; }
        public Int16 PAGE { get; set; }
    }
}
