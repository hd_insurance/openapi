﻿using GO.DTO.Insurance.BANK_LO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Search
{
    class OutInsurContracts
    {
    }

    public class OutInsurDetails : InsurDetail
    {
        public double INSUR_TOTAL { get; set; }
        public string SELLER_CODE { get; set; }
        /// <summary>
        /// Ngày ký HĐBH
        /// </summary>
        public string DATE_SIGN { get; set; }
        public int TOTAL { get; set; }
        public int PAGE_SIZE { get; set; }
        public int PAGE_CURRENT { get; set; }
    }

    public class OutLo : BankLoContract
    {
        #region Teller Info
        /// <summary>
        /// Mã giao dịch viên
        /// lần 1: brand_code: mã chi nhánh nhân viên bank
        /// lần 2: khác brand_code: => nhân viên chuyển chi nhánh => doanh thu tính cho đơn vị khác.
        /// </summary>
        public string TELLER_CODE { get; set; }
        public string TELLER_NAME { get; set; }
        public string TELLER_EMAIL { get; set; }
        public string TELLER_PHONE { get; set; }
        public string TELLER_GENDER { get; set; }
        #endregion

        public string LO_CODE { get; set; }
        public string bank_code { get; set; }
        public string branch_code { get; set; }
    }

    public class OutDisbur : BankLoDisbur
    {
        public string LOD_CODE { get; set; }
    }
    // Start 2022-07-08 By ThienTVB
    public class OutHouseInfo
    {
        public string CONTRACT_CODE { get; set; }
        public string LO_CODE { get; set; }
        public string LOAD_CODE { get; set; }
        public string ASSET_CODE { get; set; }
        public double HOUSE_VALUE { get; set; }
        public string HOUSE_TYPE { get; set; }
        public string HOUSE_ADD { get; set; }
    }
    // End 2022-07-08 By ThienTVB
}
