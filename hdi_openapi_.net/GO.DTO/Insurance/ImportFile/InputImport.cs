﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.ImportFile
{
    public class InputImport
    {
        #region NĐBH
        public string I_STAFF_CODE { get; set; }
        public string I_TYPE { get; set; }
        public string I_NAME { get; set; }
        public string I_DOB { get; set; }
        public string I_GENDER { get; set; }
        public string I_PROV { get; set; }
        public string I_DIST { get; set; }
        public string I_WARDS { get; set; }
        public string I_ADDRESS { get; set; }
        public string I_IDCARD { get; set; }
        public string I_EMAIL { get; set; }
        public string I_PHONE { get; set; }
        public string I_FAX { get; set; }
        public string I_TAXCODE { get; set; }
        public string I_RELATIONSHIP { get; set; }
        public string I_RELATIONSHIP_STAFF { get; set; }
        public string I_STAFF_REF { get; set; }
        #endregion
        #region Sản phẩm
        public string P_CERTIFICATE_NO { get; set; }
        public string P_SERIAL { get; set; }
        public string P_PACK_CODE { get; set; }
        public string P_REGION { get; set; }
        public string P_DATE_SIGN { get; set; }
        public string P_EFF { get; set; }
        public string P_EXP { get; set; }
        public double P_FEES { get; set; }
        public double P_AMOUNT { get; set; }
        public double P_TOTAL_ADD { get; set; }
        public double P_TOTAL_DISCOUNT { get; set; }
        public double P_VAT { get; set; }
        public double P_TOTAL_AMOUNT { get; set; }
        #endregion
        #region Thu hưởng
        public string BE_TYPE { get; set; }
        public string BE_ORG { get; set; }
        public string BE_NAME { get; set; }
        public string BE_DOB { get; set; }
        public string BE_GENDER { get; set; }
        public string BE_PROV { get; set; }
        public string BE_DIST { get; set; }
        public string BE_WARDS { get; set; }
        public string BE_ADDRESS { get; set; }
        public string BE_IDCARD { get; set; }
        public string BE_EMAIL { get; set; }
        public string BE_PHONE { get; set; }
        public string BE_FAX { get; set; }
        public string BE_TAXCODE { get; set; }
        #endregion

        public string ADDITIONAL { get; set; }
        public string FILE { get; set; }
        public string STT { get; set; }
    }
}
