﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.ImportFile
{
    public class ImpProgram
    {
        public string PROGID { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string ORG_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string MEMBER_CODE { get; set; }
        public string ORG_SELLER { get; set; }
        public string STRUCT_SELLER { get; set; }
        public string SELLER_CODE { get; set; }
        public string ORG_BUYER { get; set; }
        public string ORG_BUYER_NAME { get; set; }
        public string PACK_OBJ { get; set; }
        public string EFF { get; set; }
        public string EXP { get; set; }
        public string CONTRACT_NO { get; set; }
        public string EMAIL_TEMP { get; set; }
        public string CERTIFICATE_TEMP { get; set; }
        public string PROG_NAME { get; set; }

        public string EMAIL_SUBJECT { get; set; }
    }
}
