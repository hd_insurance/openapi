﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Travel
{
    public class TravelInfo
    {
        /// <summary>
        /// Vùng du lịch
        /// </summary>
        public string REGION { get; set; }
        /// <summary>
        /// Quốc gia khởi hành
        /// </summary>
        public string COUNTRY_F { get; set; }
        /// <summary>
        /// Tỉnh xuất phát 
        /// </summary>
        public string PROVINCE_F { get; set; }
        /// <summary>
        /// Mã sân bay khởi hành
        /// </summary>
        public string DEP { get; set; }
        /// <summary>
        /// Quốc gia hạ cánh
        /// </summary>
        public string COUNTRY_T { get; set; }
        /// <summary>
        /// Tỉnh đến
        /// </summary>
        public string PROVINCE_T { get; set; }
        /// <summary>
        /// Mã sân bay hạ cánh
        /// </summary>
        public string ARR { get; set; }
        /// <summary>
        /// Ngày bắt đầu
        /// </summary>
        public string FROM_DATE { get; set; }
        /// <summary>
        /// Ngày về
        /// </summary>
        public string TO_DATE { get; set; }
    }
}
