﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.DTO.Insurance.Common;

namespace GO.DTO.Insurance.Travel
{
    public class TravelInsur : ProductBase
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Mã của HĐ BH
        /// </summary>
        public string CONTRACT_CODE { get; set; }
        /// <summary>
        /// Mã chi tiết của NĐBH
        /// </summary>
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Số HĐ BH
        /// </summary>
        public string CONTRACT_NO { get; set; }
        /// <summary>
        /// Số GCN của NĐBH
        /// </summary>
        public string CERTIFICATE_NO { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        /// <summary>
        /// Mã gói
        /// </summary>
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Phí gói
        /// </summary>
        public string PACK_FEES { get; set; }
        /// <summary>
        /// Địa lý mua
        /// </summary>
        public string REGION { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFFECTIVE_DATE { get; set; }
        /// <summary>
        /// Ngày hết hiệu lực
        /// </summary>
        public string EXPIRATION_DATE { get; set; }
        /// <summary>
        /// Danh sách điều khoản mở rộng
        /// </summary>
        public List<Additional> ADDITIONAL { get; set; }
        /// <summary>
        /// Tổng phí điều khoản mở rộng
        /// </summary>
        public double ADDITIONAL_FEES { get; set; }
        public double FEES { get; set; } = 0; // new
        /// <summary>
        /// Số tiền của hợp đồng bảo hiểm
        /// </summary>
        public double AMOUNT { get; set; }
        /// <summary>
        /// Số tiền giảm
        /// </summary>
        public double? DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Đơn vị giảm
        /// </summary>
        public string DISCOUNT_UNIT { get; set; }
        public double? TOTAL_DISCOUNT { get; set; } = 0; // new
        public double? TOTAL_ADD { get; set; } = 0; // new
        /// <summary>
        /// số tiền thuế
        /// </summary>
        public double? VAT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền phải thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; }
        /// <summary>
        /// Ngày ký hợp đồng BH
        /// </summary>
        public string DATE_SIGN { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }                
    }
}
