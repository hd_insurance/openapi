﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class BeneficiaryCallback : Customer
    {
        public string DETAIL_CODE { get; set; }
        public string ORG_CODE { get; set; }
    }
}
