﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class InsurCallback
    {
        public string CATEGORY { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string ORG_SELER { get; set; }
        public string SELER_CODE { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public List<InsurDetailCallback> INSUR_DETAILS { get; set; }
    }
}
