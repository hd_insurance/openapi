﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class AdditionalCallback
    {
        public string DETAIL_CODE { get; set; }
        public string KEY { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
    }
}
