﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class InsurDetailCallback : Customer
    {
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string REF_ID { get; set; }
        public string CERTIFICATE_NO { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        public string CATEGORY { get; set; }
        /// <summary>
        /// mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFF { get; set; }
        public string TIME_EFF { get; set; }
        /// <summary>
        /// Ngày hết HL
        /// </summary>
        public string EXP { get; set; }
        public string TIME_EXP { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_ADD { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }

        public List<AdditionalCallback> ADDITIONALS { get; set; }
        public List<BeneficiaryCallback> BENEFICIARY { get; set; }

        public BusinessCallback BUSINESS { get; set; }
    }

    public class BusinessCallback
    {
        public VehicleCallback VEHICLE { get; set; }
    }
}
