﻿using GO.DTO.CommonBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class OrgDataCBConfig
    { 
        public string ACTION { get; set; }
        public string REASON { get; set; }
        public string CONTRACT_CODE { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string ORG_SELLER { get; set; }
        public string IS_AFFILIATE { get; set; }
        public string IS_LOGIN { get; set; }
        public string URL_CB { get; set; }
    }

    public class OrgDataCB
    {
        public string TYPE_CB { get; set; }
        public string METHOD { get; set; }
        public string TYPE_BODY { get; set; }
        public string TYPE_RAW { get; set; }
        public string PATH_CB { get; set; }
        public string PARAMS { get; set; }
        public string HEADER { get; set; }
    }

    public class OrderDataCB : Customer
    {
        public string ORDER_CODE { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
    }

    public class OrderDetailDataCB
    {
        public string ORDER_CODE { get; set; }
        public string REF_ID { get; set; }
        public string FIELD { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
    }
}
