﻿using GO.DTO.Insurance.Vehicle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class VehicleCallback : VehicleInfo
    {
        /// <summary>
        /// Có mua TNDS BB ?
        /// </summary>
        public string IS_COMPULSORY { get; set; }
        /// <summary>
        /// Có mua TNDS TN ?
        /// </summary>
        public string IS_VOLUNTARY_ALL { get; set; }
        /// <summary>
        /// Có mua VCX ?
        /// </summary>
        public string IS_PHYSICAL { get; set; }
        /// <summary>
        /// Có mua TNDS TN (người và hàng hóa người t3, hành khách)
        /// </summary>
        public string IS_VOLUNTARY { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho LPX, NNTX
        /// </summary>
        public string IS_DRIVER { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho hàng hóa
        /// </summary>
        public string IS_CARGO { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự bắt buộc
        /// </summary>
        public COMPULSORY_CIVIL_LIABILITY COMPULSORY_CIVIL { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự tự nguyện
        /// </summary>
        public VOLUNTARY_CIVIL_LIABILITY VOLUNTARY_CIVIL { get; set; }
        /// <summary>
        ///  Bảo hiểm thiệt hại vật chất
        /// </summary>
        public PHYSICAL_DAMAGE_COVERAGE PHYSICAL_DAMAGE { get; set; }
    }
}
