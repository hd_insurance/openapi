﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Callback
{
    public class InsurStatusCallback
    {
        public string CONTRACT_CODE { get; set; }
        public string ACTION { get; set; }
        public string RESON { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string CHECKSUM { get; set; }
    }
}
