﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class PaymentPeriod
    {
        public string CODE { get; set; }
        public string NAME { get; set; }
        public string TYPE { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string CURRENCY { get; set; }
        public string AMOUNT { get; set; }
    }
}
