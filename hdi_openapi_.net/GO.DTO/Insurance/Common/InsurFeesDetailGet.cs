﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class InsurFeesDetailGet
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string TYPE_KEY { get; set; }
        public string PARENT_KEY { get; set; }
        public double FEES_DATA { get; set; }
        public double FEES { get; set; }
        public double AMOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public double TOTAL_ADD { get; set; }
    }
}
