﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class ContractExtend
    {
        public string SELLER_NAME { get; set; }
        public string SELLER_EMAIL { get; set; }
        public string SELLER_PHONE { get; set; }
        public string SELLER_GENDER { get; set; }
    }
}
