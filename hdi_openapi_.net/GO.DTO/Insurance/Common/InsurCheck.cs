﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class InsurCheck
    {
        public string name { get; set; }
        public string dob { get; set; }
        public string eff { get; set; }
        public double age { get; set; }
        public int age_y { get; set; }
        public int age_m { get; set; }
        public int age_d { get; set; }
    }

    public class InsurCheckOut
    {
        public string type { get; set; }
        public string type_mess { get; set; }
        public List<InsurCheck> lstInput { get; set; }
    }
}
