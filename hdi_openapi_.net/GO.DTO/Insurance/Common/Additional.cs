﻿using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class Additional
    {
        public string CODE { get; set; }
        public string PARENT_CODE { get; set; }
        public string NAME { get; set; }
        public string FEES { get; set; }
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public string EFF { get; set; }
        public string EXP { get; set; }
        public List<DiscountInfo> DISCOUNT_ADDITIONAL { get; set; }
    }
}
