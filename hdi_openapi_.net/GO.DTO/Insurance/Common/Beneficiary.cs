﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;

namespace GO.DTO.Insurance.Common
{
    public class Beneficiary : Customer
    {
        public string ORG_CODE { get; set; }
        public string RELATIONSHIP { get; set; }
        public string DETAIL_CODE { get; set; }
    }
}
