﻿using GO.DTO.CommonBusiness;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class ProductBase : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        /// <summary>
        /// Mã thứ tự (Từng NĐBH 1 số tiền)
        /// </summary>
        public int INX { get; set; }
        /// <summary>
        /// Mã của HĐ BH
        /// </summary>
        public string CONTRACT_CODE { get; set; }
        /// <summary>
        /// Mã của từng NĐBH
        /// </summary>
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }

        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFF { get; set; }
        public string TIME_EFF { get; set; }
        /// <summary>
        /// Ngày hết HL
        /// </summary>
        public string EXP { get; set; }
        public string TIME_EXP { get; set; }
        /// <summary>
        /// Danh sách điều khoản mở rộng
        /// </summary>
        public List<Additional> ADDITIONAL { get; set; }
        /// <summary>
        /// Danh sách người thụ hưởng
        /// </summary>
        public List<Beneficiary> BENEFICIARY { get; set; }
        public List<DetailFile> FILE { get; set; }

        public double FEES_DATA { get; set; } = 0;
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_AMOUNT { get; set; } = 0;
        public double TOTAL_ADDITIONAL { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }
}
