﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Common
{
    public class InsurFiles
    {
        public string FILE_TYPE { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_ID { get; set; }
        public string IS_DEL { get; set; } = "0";
        public string FILE_EXTENSION { get; set; }
    }
}
