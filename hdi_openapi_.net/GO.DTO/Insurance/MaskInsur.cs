﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Common;
using GO.DTO.Insurance.House;
using GO.DTO.Insurance.Travel;
using GO.DTO.Orders;

namespace GO.DTO.Insurance
{
    /// <summary>
    /// Mặt nạn cho các hàm liên quan đến bảo hiểm
    /// </summary>
    public class MaskInsur
    {
        /// <summary>
        /// Kênh bán
        /// </summary>
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ACTION { get; set; }
        public ORD_SELLER SELLER { get; set; }
        public ORD_BUYER BUYER { get; set; }
        public ORD_PAYINFO PAY_INFO { get; set; }
        public ORD_PART ORD_PARTNER { get; set; }
        public Customer BILL_INFO { get; set; }
        /// <summary>
        /// Danh mục sản phẩm
        /// </summary>
        public string CATEGORY { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        /// <summary>
        /// Kiểu sản phẩm
        /// </summary>
        public string PRODUCT_MODE { get; set; }
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_AMOUNT { get; set; } = 0;
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
        //public double TOTAL_ADD { get; set; } = 0;
        /// <summary>
        /// File chung cho contract, Người mua
        /// </summary>
        public List<DetailFile> FILE { get; set; }

        //ATTD
        public List<LoProduct> LO_INSUR { get; set; }

        // Sức khóe
        public List<InsuredPerson> HEALTH_INSUR { get; set; }

        // Nhà Tư nhân
        public List<HouseInsur> HOUSE_INSUR { get; set; }
        // Start 2022-04-13 By ThienTVB
        // Thiết bị
        public List<DeviceInsur> DEVICE_INSUR { get; set; }
        // End 2022-04-13 By ThienTVB

        //KhanhPT 2023-07-20
        public TravelCare TRAVEL { get; set; }
        // End 2023-07-20
    }

    public class MaskValidate
    {
        public bool Err { get; set; }
        public string ErrCode { get; set; }
        public string ErrMess { get; set; }
    }
}
