﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.HealthNew
{
    public class HealthCareV2
    {
        /// <summary>
        /// Loại người mua. CN hoặc GD
        /// </summary>
        public string BUYER_TYPE { get; set; }

        /// <summary>
        /// Mã của HĐ BH
        /// </summary>
        public string CONTRACT_CODE { get; set; }
        /// <summary>
        /// Số HĐ BH
        /// </summary>
        public string CONTRACT_NO { get; set; }
        /// <summary>
        /// Ngày ký hợp đồng BH
        /// </summary>
        public string DATE_SIGN { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
    }
}
