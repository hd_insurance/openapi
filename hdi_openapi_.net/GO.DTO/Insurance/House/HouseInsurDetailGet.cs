﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.House
{
    class HouseInsurDetailGet
    {
    }

    public class HouseInsurExt
    {
        public string DETAIL_CODE { get; set; }
        public double INSUR_TOTAL_AMOUNT { get; set; }
        public double INSUR_HOUSE_VALUE { get; set; }
        public double INSUR_HOUSE_ASSETS { get; set; }
        public string HOUSE_USE_CODE { get; set; }
        public string LAND_USE_RIGHTS { get; set; }
        public string INSUR_DEDUCTIBLE { get; set; }
    }
}
