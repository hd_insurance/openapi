﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Common;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.House
{
    public class HouseInsur : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public int INX { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Quyền sử dụng đất: O: chủ nhà, L: Quyền trông coi ngôi nhà, U: Quyền sử dụng ngôi nhà
        /// </summary>
        public string LAND_USE_RIGHTS { get; set; }
        public HouseInfo HOUSE_INFO { get; set; }
        public HouseProduct HOUSE_PRODUCT { get; set; }
        //public Customer BILL_INFO { get; set; }
        /// <summary>
        /// Danh sách người thụ hưởng
        /// </summary>
        public List<Beneficiary> BENEFICIARY { get; set; }
        public List<DetailFile> FILE { get; set; }
    }

    public class HouseProduct
    {
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; } = 0;
        public double INSUR_HOUSE_VALUE { get; set; } = 0;
        public double INSUR_HOUSE_ASSETS { get; set; } = 0;
        //Start 2022-06-16 By ThienTVB
        public double HOUSE_VALUE_REF { get; set; } = 0;
        public double ASSET_VALUE_REF { get; set; } = 0;
        //End 2022-06-16 By ThienTVB
        /// <summary>
        /// Mã năm sử dụng nhà: T10NTX, T1015N
        /// </summary>
        public string HOUSE_USE_CODE { get; set; }
        public string ASSETS_USE_CODE { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFF { get; set; }
        public string TIME_EFF { get; set; }
        /// <summary>
        /// Ngày hết HL
        /// </summary>
        public string EXP { get; set; }
        public string TIME_EXP { get; set; }

        /// <summary>
        /// Mức miễn thường
        /// </summary>
        public string INSUR_DEDUCTIBLE { get; set; }

        public double FEES_DATA { get; set; } = 0;
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_AMOUNT { get; set; } = 0;
        public double TOTAL_ADDITIONAL { get; set; } = 0;

        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }

        #region Thông tin phí căn nhà
        public double HOUSE_VAL_FEES_DATA { get; set; } = 0;
        public double HOUSE_VAL_FEES { get; set; } = 0;
        public double HOUSE_VAL_AMOUNT { get; set; } = 0;
        public double HOUSE_VAL_VAT { get; set; } = 0;
        public double HOUSE_VAL_TOTAL_DISCOUNT { get; set; }
        public double HOUSE_VAL_TOTAL_AMOUNT { get; set; } = 0;
        #endregion
        #region Thông tin phí nội thất căn nhà
        public double HOUSE_ASSETS_FEES_DATA { get; set; } = 0;
        public double HOUSE_ASSETS_FEES { get; set; } = 0;
        public double HOUSE_ASSETS_AMOUNT { get; set; } = 0;
        public double HOUSE_ASSETS_VAT { get; set; } = 0;
        public double HOUSE_ASSETS_TOTAL_DISCOUNT { get; set; }
        public double HOUSE_ASSETS_TOTAL_AMOUNT { get; set; } = 0;
        #endregion
    }
}
