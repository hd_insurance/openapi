﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.House
{
    public class HouseInfo
    {
        public string DETAIL_CODE { get; set; }
        public string HOUSE_CODE { get; set; }
        /// <summary>
        /// Loại nhà (VL: biệt thự, AD: liền kề, AP: chung cư)
        /// </summary>
        public string HOUSE_TYPE { get; set; }
        /// <summary>
        /// Diện tích ngôi nhà m2 (sau dấu phẩy 1 số 02/07/2021 An chốt)
        /// </summary>
        public string HOUSE_AREA { get; set; }
        /// <summary>
        /// Năm xây dựng
        /// </summary>
        public string YEAR_BUILT { get; set; }
        /// <summary>
        /// Năm hoàn thành xây dựng
        /// </summary>
        public string YEAR_COMPLETE { get; set; }
        /// <summary>
        /// Số tầng
        /// </summary>
        public string NUM_FLOOR { get; set; }
        /// <summary>
        /// Số tầng hầm
        /// </summary>
        public string NUM_BASEMENT { get; set; }
        /// <summary>
        /// Giá trị ngôi nhà thực tế (không có trong giao diện)
        /// </summary>
        public string HOUSE_VALUE { get; set; }
        /// <summary>
        /// Giá trị tài sản trong ngôi nhà(không có trong giao diện)
        /// </summary>
        public string ASSET_VALUE { get; set; }
        //Start 2022-06-16 By ThienTVB
        public double HOUSE_VALUE_REF { get; set; } = 0;
        public double ASSET_VALUE_REF { get; set; } = 0;
        //End 2022-06-16 By ThienTVB
        /// <summary>
        /// Có chữa cháy không ?
        /// </summary>
        public string IS_FIRE_PROTECTION { get; set; }
        /// <summary>
        /// Có sổ đất không?
        /// </summary>
        public string IS_LAND_BOOK { get; set; }
        /// <summary>
        /// Loại sổ (sổ đỏ, hổng,..)
        /// </summary>
        public string LAND_BOOK_TYPE { get; set; }
        /// <summary>
        /// Mã tỉnh
        /// </summary>
        public string PROV { get; set; }
        /// <summary>
        /// Mã huyện
        /// </summary>
        public string DIST { get; set; }
        /// <summary>
        /// Mã xã
        /// </summary>
        public string WARDS { get; set; }
        /// <summary>
        /// Địa chỉ số nhà, ngõ ngách đường
        /// </summary>
        public string ADDRESS { get; set; }
        public string ADDRESS_FORM { get; set; }
        /// <summary>
        /// Kinh độ (có thể âm)
        /// </summary>
        public string HOUSE_LAT { get; set; }
        /// <summary>
        /// Vĩ độ (có thể âm)
        /// </summary>
        public string HOUSE_LONG { get; set; }
        /// <summary>
        /// Tên chủ nhà (trong sổ đỏ)
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// Ngày sinh chủ nhà
        /// </summary>
        public string DOB { get; set; }
        /// <summary>
        /// Giới tính chủ nhà
        /// </summary>
        public string GENDER { get; set; }
        /// <summary>
        /// Chứng minh thư, căn cước
        /// </summary>
        public string IDCARD { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IDCARD_D { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IDCARD_P { get; set; }
        /// <summary>
        /// Địa chỉ email chủ nhà
        /// </summary>
        public string EMAIL { get; set; }
        /// <summary>
        /// Số điện thoại chủ nhà
        /// </summary>
        public string PHONE { get; set; }
    }
}
