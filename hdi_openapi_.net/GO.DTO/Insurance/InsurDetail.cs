﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class InsurDetail
    {
        public string DETAIL_CODE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string CERTIFICATE_NO { get; set; }
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string CUS_CODE { get; set; }
        public string NATIONALITY { get; set; }
        public string RELATIONSHIP { get; set; }
        public string NAME { get; set; }
        public string DOB { get; set; }
        public string GENDER { get; set; }
        public string TYPE { get; set; }
        public string PROVINCE { get; set; }
        public string DISTRICT { get; set; }
        public string WARDS { get; set; }
        public string ADDRESS { get; set; }
        public string IDCARD { get; set; }
        public string IDCARD_D { get; set; }
        public string IDCARD_P { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string TAXCODE { get; set; }
        public string REGION { get; set; }
        public double ADDTIONAL_FEES { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EFFECTIVE_NUM { get; set; }
        public string EFFECTIVE_TIME { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string EXPIRATION_NUM { get; set; }
        public string EXPIRATION_TIME { get; set; }
        public double PACK_FEES { get; set; }
        public double AMOUNT { get; set; }
        public string DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string PARENT_CODE { get; set; }
        public string ORG_CODE { get; set; }

        public string EMAIL_CC { get; set; }
        public string EMAIL_BCC { get; set; }
        public string ADDRESS_FORM { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
        public string ADDRESS_FULL { get; set; }
        public string PACK_NAME { get; set; }
        public string PRODUCT_NAME { get; set; }
        
        public string URL_GCN { get; set; }
        public string DATE_SIGN { get; set; }
        public string SERIAL { get; set; }
        public string PROV { get; set; }
        public string DIST { get; set; }
        public string IS_CER { get; set; }
        public string TYPE_SEND { get; set; }
        public string REF_ID { get; set; }

    }
    public class InsurDetailExt:InsurDetail
    {
        public string EFFECTIVE_DATE_EN { get; set; }
        public string HH_TO { get; set; }
        public string SS_TO { get; set; }
        public string DAY_TO { get; set; }
        public string MM_TO { get; set; }
        public string YY_TO { get; set; }
        public string EXPIRATION_DATE_EN { get; set; }
        public string HH_F { get; set; }
        public string SS_F { get; set; }
        public string DAY_F { get; set; }
        public string MM_F { get; set; }
        public string YY_F { get; set; }
        public string EXPIRATION_DATE1 { get; set; } //ngày hiệu lực +1 day
        public string EXPIRATION_DATE1_EN { get; set; }
        public string MONEY_VN { get; set; }
        public string MONEY { get; set; }
        public string MONEY_ENT { get; set; }
        public string URL_FILE { get; set; }
        public string RELATIONSHIP_VN { get; set; }
        public string REGION_VI { get; set; }
        public string REGION_EN { get; set; }
        public string BENEFICIARY { get; set; }
        public string STT { get; set; }
        public string NAME_EN { get; set; }
    }
    public class DetailFile
    {
        public string DETAIL_CODE { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string FILE_NAME { get; set; }
        public string FILE_ID { get; set; }
        public string FILE_TYPE { get; set; }
        public string FILE_FORMAT { get; set; }
        public string IS_DEL { get; set; }
    }
}
