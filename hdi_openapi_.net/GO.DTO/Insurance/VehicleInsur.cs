﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Common;
using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class VehicleInsur
    {
        public string ORG_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ACTION { get; set; }
        /// <summary>
        /// Tách đơn hay gộp đơn giữa các xe
        /// </summary>
        public string IS_SPLIT_ORDER { get; set; } = "0";
        public ORD_BUYER BUYER { get; set; }
        public List<VEHICLE_PRODUCT> VEHICLE_INSUR { get; set; }
        public ORD_PAYINFO PAY_INFO { get; set; }
        public Customer BILL_INFO { get; set; }
        public ORD_SKU ORD_SKU { get; set; }
        public ORD_PART ORD_PARTNER { get; set; }
    }

    public class VEHICLE_PRODUCT : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public string CONTRACT_CODE { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Tách đơn hay gộp đơn trong 1 xe (TNDS, VCX)
        /// </summary>
        public string IS_SPLIT_ORDER { get; set; } = "0";
        /// <summary>
        /// Gửi GCN qua email
        /// </summary>
        public string IS_SEND_MAIL { get; set; }
        /// <summary>
        /// Nhóm xe
        /// </summary>
        public string VEHICLE_GROUP { get; set; }
        /// <summary>
        /// Loại xe
        /// </summary>
        public string VEHICLE_TYPE { get; set; }
        /// <summary>
        /// Mục dích sử dụng
        /// </summary>
        public string VEHICLE_USE { get; set; }
        /// <summary>
        /// 1: chưa có biển số
        /// 0: có biển số
        /// </summary>
        public string NONE_NUMBER_PLATE { get; set; }
        /// <summary>
        /// Biển số xe
        /// </summary>
        public string NUMBER_PLATE { get; set; }
        /// <summary>
        /// Số khung
        /// </summary>
        public string CHASSIS_NO { get; set; }
        /// <summary>
        /// Số máy
        /// </summary>
        public string ENGINE_NO { get; set; }
        /// <summary>
        /// Số chỗ ngồi
        /// </summary>
        public string SEAT_NO { get; set; }
        /// <summary>
        /// Mã số chỗ ngồi
        /// </summary>
        public string SEAT_CODE { get; set; }
        /// <summary>
        /// Trọng tải
        /// </summary>
        public string WEIGH { get; set; }
        /// <summary>
        /// Mã code mà trọng tải rơi vào
        /// </summary>
        public string WEIGH_CODE { get; set; }
        /// <summary>
        /// Hãng xe
        /// </summary>
        public string BRAND { get; set; }
        /// <summary>
        /// Hiệu xe
        /// </summary>
        public string MODEL { get; set; }
        /// <summary>
        /// Năm sản xuất
        /// </summary>
        public string MFG { get; set; }
        /// <summary>
        /// Dung tích
        /// </summary>
        public string CAPACITY { get; set; }
        /// <summary>
        /// Giá trị xe trên thị trường
        /// </summary>
        public string REF_VEHICLE_VALUE { get; set; }
        /// <summary>
        /// Ngày đăng ký xe
        /// </summary>
        public string VEHICLE_REGIS { get; set; }
        /// <summary>
        /// Mã ngày sử dụng (tính từ đăng ký xe đến hiện tại)
        /// </summary>
        public string VEHICLE_REGIS_CODE { get; set; }
        /// <summary>
        /// Giá trị xe
        /// </summary>
        public string VEHICLE_VALUE { get; set; }
        /// <summary>
        /// Mã Giá trị xe
        /// </summary>
        public string VEHICLE_VALUE_CODE { get; set; }

        /// <summary>
        /// Có mua TNDS BB ?
        /// </summary>
        public string IS_COMPULSORY { get; set; }
        /// <summary>
        /// Có mua TNDS TN ?
        /// </summary>
        public string IS_VOLUNTARY_ALL { get; set; }
        /// <summary>
        /// Có mua VCX ?
        /// </summary>
        public string IS_PHYSICAL { get; set; }

        /// <summary>
        /// Có mua TNDS TN (người và hàng hóa người t3, hành khách)
        /// </summary>
        public string IS_VOLUNTARY { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho LPX, NNTX
        /// </summary>
        public string IS_DRIVER { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho hàng hóa
        /// </summary>
        public string IS_CARGO { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự bắt buộc
        /// </summary>
        public COMPULSORY_CIVIL_LIABILITY COMPULSORY_CIVIL { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự tự nguyện
        /// </summary>
        public VOLUNTARY_CIVIL_LIABILITY VOLUNTARY_CIVIL { get; set; }
        /// <summary>
        ///  Bảo hiểm thiệt hại vật chất
        /// </summary>
        public PHYSICAL_DAMAGE_COVERAGE PHYSICAL_DAMAGE { get; set; }
        public List<GiftInfo> GIFTS { get; set; }

        // thông tin tổng phí của cả đơn
        public double AMOUNT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;

        public double TOTAL_ADDITIONAL { get; set; } = 0;
        public double FEES_ADD { get; set; } = 0;
        public double AMOUNT_ADD { get; set; } = 0;
        public double TOTAL_DISCOUNT_ADD { get; set; } = 0;
        public double VAT_ADD { get; set; } = 0;
        public double TOTAL_ADD { get; set; } = 0;
    }

    /// <summary>
    ///  Trách nhiệm dân sự bắt buộc
    /// </summary>
    public class COMPULSORY_CIVIL_LIABILITY
    {
        public int INX { get; set; }
        public string DETAIL_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string IS_SERIAL_NUM { get; set; }
        public string SERIAL_NUM { get; set; }
        public string EFF { get; set; }
        public string TIME_EFF { get; set; }
        public string EXP { get; set; }
        public string TIME_EXP { get; set; }

        public double FEES_DATA { get; set; } = 0;
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        //public double DISCOUNT { get; set; } = 0;
        //public string DISCOUNT_UNIT { get; set; }
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;

        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }

        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }


        public List<DetailFile> FILE { get; set; }
    }

    /// <summary>
    ///  Trách nhiệm dân sự tự nguyện
    /// </summary>
    public class VOLUNTARY_CIVIL_LIABILITY
    {
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Thiệt hại thân thể người thứ 3
        /// </summary>
        public string INSUR_3RD { get; set; }
        /// <summary>
        /// Thiệt hại thân thể hành khách
        /// </summary>
        public string INSUR_PASSENGER { get; set; }
        /// <summary>
        /// Thiệt hại tài sản người thứ 3
        /// </summary>
        public string INSUR_3RD_ASSETS { get; set; }
        /// <summary>
        /// Tổng hạn mức trách nhiệm
        /// </summary>
        public string TOTAL_LIABILITY { get; set; }
        /// <summary>
        /// Số người tham gia (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_NUM { get; set; }
        /// <summary>
        /// Mã code Số người tham gia (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_SEAT_CODE { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_INSUR { get; set; }
        /// <summary>
        /// Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string WEIGHT_CARGO { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string CARGO_INSUR { get; set; }
        public double FEES_DATA { get; set; } = 0;
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;

        #region TNDS TN vượt quá mức (thân thể tài sản t3, hành khách)
        public double FEES_DATA_VOLUNTARY_3RD { get; set; } = 0;
        public double FEES_VOLUNTARY_3RD { get; set; } = 0;
        public double AMOUNT_VOLUNTARY_3RD { get; set; } = 0;
        public double TOTAL_DISCOUNT_VOLUNTARY_3RD { get; set; } = 0;
        public double VAT_VOLUNTARY_3RD { get; set; } = 0;

        /// <summary>
        /// Tổng tiền TNDS TN vượt quá mức (thân thể tài sản t3, hành khách)
        /// </summary>
        public double TOTAL_VOLUNTARY_3RD { get; set; } = 0;
        #endregion

        #region LPX,NNTX
        public double FEES_DATA_DRIVER { get; set; } = 0;
        public double FEES_DRIVER { get; set; } = 0;
        public double AMOUNT_DRIVER { get; set; } = 0;
        public double TOTAL_DISCOUNT_DRIVER { get; set; } = 0;
        public double VAT_DRIVER { get; set; } = 0;
        /// <summary>
        /// Tổng tiền LPX,NNTX
        /// </summary>
        public double TOTAL_DRIVER { get; set; } = 0;
        #endregion

        #region hàng hóa
        public double FEES_DATA_CARGO { get; set; } = 0;
        public double FEES_CARGO { get; set; } = 0;
        public double AMOUNT_CARGO { get; set; } = 0;
        public double TOTAL_DISCOUNT_CARGO { get; set; } = 0;
        public double VAT_CARGO { get; set; } = 0;
        /// <summary>
        /// Tổng tiền hàng hóa
        /// </summary>
        public double TOTAL_CARGO { get; set; } = 0;
        #endregion

        #region bảo hiểm TN thân thể t3
        public double FEES_DATA_3RD_INSUR { get; set; } = 0;
        public double FEES_3RD_INSUR { get; set; } = 0;
        public double AMOUNT_3RD_INSUR { get; set; } = 0;
        public double TOTAL_DISCOUNT_3RD_INSUR { get; set; } = 0;
        public double VAT_3RD_INSUR { get; set; } = 0;
        /// <summary>
        /// Tổng phí bảo hiểm TN thân thể t3
        /// </summary>
        public double TOTAL_3RD_INSUR { get; set; } = 0;
        #endregion

        #region bảo hiểm TN hàng hóa t3
        public double FEES_DATA_3RD_ASSETS { get; set; } = 0;
        public double FEES_3RD_ASSETS { get; set; } = 0;
        public double AMOUNT_3RD_ASSETS { get; set; } = 0;
        public double TOTAL_DISCOUNT_3RD_ASSETS { get; set; } = 0;
        public double VAT_3RD_ASSETS { get; set; } = 0;
        /// <summary>
        /// Tổng phí bảo hiểm TN hàng hóa t3
        /// </summary>
        public double TOTAL_3RD_ASSETS { get; set; } = 0;
        #endregion

        #region bảo hiểm TN TN hành khách
        public double FEES_DATA_PASSENGER { get; set; } = 0;
        public double FEES_PASSENGER { get; set; } = 0;
        public double AMOUNT_PASSENGER { get; set; } = 0;
        public double TOTAL_DISCOUNT_PASSENGER { get; set; } = 0;
        public double VAT_PASSENGER { get; set; } = 0;
        /// <summary>
        /// Tổng phí bảo hiểm TN hành khách
        /// </summary>
        public double TOTAL_PASSENGER { get; set; } = 0;
        #endregion

        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }
    /// <summary>
    ///  Bảo hiểm thiệt hại vật chất
    /// </summary>
    public class PHYSICAL_DAMAGE_COVERAGE
    {
        /// <summary>
        /// index tính phí sản phẩm
        /// </summary>
        public int INX { get; set; }
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// mã sản phẩm (VCX)
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }

        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Mức miễn thường
        /// </summary>
        public string INSUR_DEDUCTIBLE { get; set; }
        /// <summary>
        /// Ngày hiệu lực
        /// </summary>
        public string EFF { get; set; }
        public string TIME_EFF { get; set; }
        /// <summary>
        /// Ngày hết HL
        /// </summary>
        public string EXP { get; set; }
        public string TIME_EXP { get; set; }
        /// <summary>
        /// Xe có tổn thất không ?
        /// </summary>
        public string IS_VEHICLE_LOSS { get; set; }
        /// <summary>
        /// Số năm không tổn thất
        /// </summary>
        public string VEHICLE_LOSS_NUM { get; set; }
        /// <summary>
        /// Số tiền bị tổn thất
        /// </summary>
        public string VEHICLE_LOSS_AMOUNT { get; set; }
        /// <summary>
        /// Có giảm giá không
        /// </summary>
        public string IS_DISCOUNT { get; set; }
        /// <summary>
        /// Số giảm giá
        /// </summary>
        public double DISCOUNT { get; set; } = 0;
        public string TYPE_DISCOUNT { get; set; } = "VC_XE";
        /// <summary>
        /// Đơn vị giảm
        /// </summary>
        public string DISCOUNT_UNIT { get; set; }
        /// <summary>
        /// Lý do giảm phí
        /// </summary>
        public string REASON_DISCOUNT { get; set; }
        /// <summary>
        /// Gửi đến bảo hiểm giám định
        /// 1: gửi giảm định => chờ duyệt
        /// 0: đơn nháp
        /// </summary>
        public string IS_SEND_HD { get; set; }
        /// <summary>
        /// Hình thức giám định:
        /// 1. Cá nhân chụp ảnh (hiện chưa có)
        /// 2. đại lý chụp ảnh: DLCA
        /// 3. Đến giám định theo lịch KH: GDLKH
        /// 4. Gọi điện thoại thống nhất thời gian với KH: GDT
        /// </summary>
        public string SURVEYOR_TYPE { get; set; }
        /// <summary>
        /// Mã lịch hẹn
        /// </summary>
        public string CAL_CODE { get; set; }
        /// <summary>
        /// Thời gian hẹn bắt đầu
        /// </summary>
        public string CALENDAR_EFF { get; set; }
        /// <summary>
        /// Thời gian kết thúc hẹn
        /// </summary>
        public string CALENDAR_EXP { get; set; }
        /// <summary>
        /// Thời gian hẹn từ
        /// </summary>
        public string CALENDAR_TIME_F { get; set; }
        /// <summary>
        /// Thời gian hẹn đến
        /// </summary>
        public string CALENDAR_TIME_T { get; set; }
        /// <summary>
        /// Thông tin người liên hệ
        /// </summary>
        public string CALENDAR_CONTACT { get; set; }
        /// <summary>
        /// SĐT liên hệ
        /// </summary>
        public string CALENDAR_PHONE { get; set; }
        /// <summary>
        /// Mã tỉnh địa chỉ hẹn
        /// </summary>
        public string CALENDAR_PROV { get; set; }
        /// <summary>
        /// Mã huyện địa chỉ hẹn
        /// </summary>
        public string CALENDAR_DIST { get; set; }
        /// <summary>
        /// Mã xã địa chỉ hẹn
        /// </summary>
        public string CALENDAR_WARDS { get; set; }
        /// <summary>
        /// Địa chỉ hẹn
        /// </summary>
        public string CALENDAR_ADDRESS { get; set; }
        /// <summary>
        /// Địa chỉ hẹn trên form
        /// </summary>
        public string CALENDAR_ADDRESS_FORM { get; set; }
        public string CALENDAR_STATUS { get; set; }
        /// <summary>
        /// danh sách điều khoản bổ sung chọn thêm
        /// </summary>
        public List<PHYSICAL_DAMAGE_ADDITIONAL> ADDITIONAL { get; set; }
        public double FEES_DATA { get; set; } = 0;
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; }
        public double TOTAL_AMOUNT { get; set; } = 0;
        public double TOTAL_ADDITIONAL { get; set; } = 0;

        #region Thông tin phí gốc VCX
        public double PHYSICAL_FEES_DATA { get; set; } = 0;
        public double PHYSICAL_FEES { get; set; } = 0;
        public double PHYSICAL_AMOUNT { get; set; } = 0;
        public double PHYSICAL_VAT { get; set; } = 0;
        public double PHYSICAL_TOTAL_DISCOUNT { get; set; }
        public double PHYSICAL_TOTAL_AMOUNT { get; set; } = 0;
        #endregion
        #region Thông tin tổng phí Bổ sung
        public double PHYSICAL_ADD_FEES_DATA { get; set; } = 0;
        public double PHYSICAL_ADD_FEES { get; set; } = 0;
        public double PHYSICAL_ADD_AMOUNT { get; set; } = 0;
        public double PHYSICAL_ADD_VAT { get; set; } = 0;
        public double PHYSICAL_ADD_TOTAL_DISCOUNT { get; set; }
        public double PHYSICAL_ADD_TOTAL_AMOUNT { get; set; } = 0;
        #endregion

        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
        public List<DetailFile> FILE { get; set; }
        public List<DetailFile> SURVEYOR_FILES { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }

        // thông tin thụ hưởng của VCX

        /// <summary>
        /// Danh sách người thụ hưởng
        /// </summary>
        public List<Beneficiary> BENEFICIARY { get; set; }
    }

    public class PHYSICAL_DAMAGE_ADDITIONAL
    {
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string KEY { get; set; }
        public string LABEL { get; set; }
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_ADDITIONAL { get; set; }
    }

    public class VehicleInsur_V2
    {
        public string ORG_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ACTION { get; set; }
        public ORD_BUYER BUYER { get; set; }
        public List<VEHICLE_PRODUCT_V2> VEHICLE_INSUR { get; set; }
        public ORD_PAYINFO PAY_INFO { get; set; }
    }

    public class VEHICLE_PRODUCT_V2 : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public string CONTRACT_CODE { get; set; }
        /// <summary>
        /// Mối quan hệ với người mua
        /// </summary>
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Tách đơn hay gộp đơn
        /// </summary>
        public string IS_SPLIT_ORDER { get; set; }
        /// <summary>
        /// Gửi GCN qua email
        /// </summary>
        public string IS_SEND_MAIL { get; set; }
        /// <summary>
        /// Nhóm xe
        /// </summary>
        public string VEHICLE_GROUP { get; set; }
        /// <summary>
        /// Loại xe
        /// </summary>
        public string VEHICLE_TYPE { get; set; }
        /// <summary>
        /// Mục dích sử dụng
        /// </summary>
        public string VEHICLE_USE { get; set; }
        /// <summary>
        /// 1: chưa có biển số
        /// 0: có biển số
        /// </summary>
        public string NONE_NUMBER_PLATE { get; set; }
        /// <summary>
        /// Biển số xe
        /// </summary>
        public string NUMBER_PLATE { get; set; }
        /// <summary>
        /// Số khung
        /// </summary>
        public string CHASSIS_NO { get; set; }
        /// <summary>
        /// Số máy
        /// </summary>
        public string ENGINE_NO { get; set; }
        /// <summary>
        /// Số chỗ ngồi
        /// </summary>
        public string SEAT_NO { get; set; }
        /// <summary>
        /// Mã số chỗ ngồi
        /// </summary>
        public string SEAT_CODE { get; set; }
        /// <summary>
        /// Trọng tải
        /// </summary>
        public string WEIGH { get; set; }
        /// <summary>
        /// Mã code mà trọng tải rơi vào
        /// </summary>
        public string WEIGH_CODE { get; set; }
        /// <summary>
        /// Hãng xe
        /// </summary>
        public string BRAND { get; set; }
        /// <summary>
        /// Hiệu xe
        /// </summary>
        public string MODEL { get; set; }
        /// <summary>
        /// Năm sản xuất
        /// </summary>
        public string MFG { get; set; }
        /// <summary>
        /// Dung tích
        /// </summary>
        public string CAPACITY { get; set; }
        /// <summary>
        /// Giá trị xe trên thị trường
        /// </summary>
        public string REF_VEHICLE_VALUE { get; set; }
        /// <summary>
        /// Ngày đăng ký xe
        /// </summary>
        public string VEHICLE_REGIS { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự bắt buộc
        /// </summary>
        public COMPULSORY_CIVIL_LIABILITY COMPULSORY_CIVIL { get; set; }
        /// <summary>
        ///  Trách nhiệm dân sự tự nguyện
        /// </summary>
        public VOLUNTARY_CIVIL_LIABILITY_V2 VOLUNTARY_CIVIL { get; set; }
        /// <summary>
        ///  Bảo hiểm thiệt hại vật chất
        /// </summary>
        public PHYSICAL_DAMAGE_COVERAGE PHYSICAL_DAMAGE { get; set; }
        public List<GiftInfo> GIFTS { get; set; }

        // thông tin tổng phí của cả đơn
        public double AMOUNT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
    }


    /// <summary>
    ///  Trách nhiệm dân sự tự nguyện
    /// </summary>
    public class VOLUNTARY_CIVIL_LIABILITY_V2
    {
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }

        public VOLUNTARY VOLUNTARY_INFO { get; set; }
        public DRIVER DRIVER_INFO { get; set; }
        public CARGO CARGO_INFO { get; set; }
    }

    /// <summary>
    /// TNDS TN
    /// </summary>
    public class VOLUNTARY
    {
        public string IS_VOLUNTARY { get; set; }
        /// <summary>
        /// Thiệt hại thân thể người thứ 3
        /// </summary>
        public string INSUR_3RD { get; set; }
        /// <summary>
        /// Thiệt hại thân thể hành khách
        /// </summary>
        public string INSUR_PASSENGER { get; set; }
        /// <summary>
        /// Thiệt hại tài sản người thứ 3
        /// </summary>
        public string INSUR_3RD_ASSETS { get; set; }
        /// <summary>
        /// Tổng hạn mức trách nhiệm
        /// </summary>
        public string TOTAL_LIABILITY { get; set; }
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }

    /// <summary>
    /// TNDS TN -LPX:Lái phụ xe 
    /// </summary>
    public class DRIVER
    {
        public string IS_DRIVER { get; set; }
        /// <summary>
        /// Số người tham gia (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_NUM { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_INSUR { get; set; }
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }

    /// <summary>
    /// TNDS TN: HH: hàng hóa
    /// </summary>
    public class CARGO
    {
        public string IS_CARGO { get; set; }
        /// <summary>
        /// Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string WEIGHT_CARGO { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string CARGO_INSUR { get; set; }
        public double FEES { get; set; } = 0;
        public double AMOUNT { get; set; } = 0;
        public double VAT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;
        public List<DiscountInfo> DISCOUNT_DETAIL { get; set; }
    }

    /// <summary>
    /// Lấy thông tin chi tiết sản phẩm xe
    /// </summary>
    public class VehicleInsurDetail
    {

        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Tách đơn hay gộp đơn
        /// </summary>
        public string IS_SPLIT_ORDER { get; set; }

        /// <summary>
        /// Mã số chỗ ngồi
        /// </summary>
        public string SEAT_CODE { get; set; }

        /// <summary>
        /// Mã code mà trọng tải rơi vào
        /// </summary>
        public string WEIGH_CODE { get; set; }

        public string INSUR_DEDUCTIBLE { get; set; }
        public string IS_VEHICLE_LOSS { get; set; }
        public string VEHICLE_LOSS_NUM { get; set; }
        /// <summary>
        /// Gửi GCN qua email
        /// </summary>
        public string IS_SEND_MAIL { get; set; }

        /// <summary>
        /// Có mua TNDS TN (người và hàng hóa người t3, hành khách)
        /// </summary>
        public string IS_VOLUNTARY { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho LPX, NNTX
        /// </summary>
        public string IS_DRIVER { get; set; }
        /// <summary>
        /// Có mua TNDS TN cho hàng hóa
        /// </summary>
        public string IS_CARGO { get; set; }
        /// <summary>
        /// Mã số tiền bảo hiểm xe
        /// </summary>
        public string VEHICLE_VALUE_CODE { get; set; }
        /// <summary>
        /// Mã năm sử dụng
        /// </summary>
        public string VEHICLE_REGIS_CODE { get; set; }
        /// <summary>
        /// Có mua BB
        /// </summary>
        public string IS_COMPULSORY { get; set; }
        /// <summary>
        /// Có mua TN
        /// </summary>
        public string IS_VOLUNTARY_ALL { get; set; }
        /// <summary>
        /// Có mua VCX
        /// </summary>
        public string IS_PHYSICAL { get; set; }

        // TNDS BB

        public string PRODUCT_CODE_BB { get; set; }
        public string PACK_CODE_BB { get; set; }
        public string IS_SERIAL_NUM { get; set; }
        public string SERIAL_NUM { get; set; }
        public double FEES_BB { get; set; } = 0;
        public double AMOUNT_BB { get; set; } = 0;
        public double VAT_BB { get; set; } = 0;
        public double TOTAL_DISCOUNT_BB { get; set; } = 0;
        public double TOTAL_AMOUNT_BB { get; set; } = 0;

        // TNDS TN

        //public string PRODUCT_CODE_TN { get; set; }
        //public string PACK_CODE_TN { get; set; }
        /// <summary>
        /// Thiệt hại thân thể người thứ 3
        /// </summary>
        public string INSUR_3RD { get; set; }
        /// <summary>
        /// Thiệt hại thân thể hành khách
        /// </summary>
        public string INSUR_PASSENGER { get; set; }
        /// <summary>
        /// Thiệt hại tài sản người thứ 3
        /// </summary>
        public string INSUR_3RD_ASSETS { get; set; }
        /// <summary>
        /// Tổng hạn mức trách nhiệm
        /// </summary>
        public string TOTAL_LIABILITY { get; set; }
        /// <summary>
        /// Số người tham gia (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_NUM { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm (Bảo hiểm lái, phụ xe và người ngồi trên xe)
        /// </summary>
        public string DRIVER_INSUR { get; set; }
        /// <summary>
        /// Trọng lượng hàng hóa (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string WEIGHT_CARGO { get; set; }
        /// <summary>
        /// Số tiền bảo hiểm hàng hóa - mức trách nhiệm (Bảo hiểm TNDS của chủ xe đối với hàng hóa)
        /// </summary>
        public string CARGO_INSUR { get; set; }
        public double FEES_VOLUNTARY_CIVIL { get; set; }
        public double AMOUNT_VOLUNTARY_CIVIL { get; set; }
        public double VAT_VOLUNTARY_CIVIL { get; set; }
        public double DISCOUNT_VOLUNTARY_CIVIL { get; set; }
        public double TOTAL_VOLUNTARY_CIVIL { get; set; }
        public string PRODUCT_VOLUNTARY_CIVIL { get; set; }
        public string PACK_VOLUNTARY_CIVIL { get; set; }

        public double TOTAL_VOLUNTARY_3RD { get; set; }
        public double TOTAL_DRIVER { get; set; }
        public double TOTAL_CARGO { get; set; }
        public double TOTAL_3RD_INSUR { get; set; }
        public double TOTAL_3RD_ASSETS { get; set; }
        public double TOTAL_PASSENGER { get; set; }

        // VCX
        public string PHYSICAL_INSUR_DEDUCTIBLE { get; set; }
        public string PHYSICAL_IS_DISCOUNT { get; set; }
        public double PHYSICAL_DISCOUNT { get; set; }
        public string PHYSICAL_DISCOUNT_UNIT { get; set; }
        public string PHYSICAL_REASON_DISCOUNT { get; set; }
        public string IS_SEND_HD { get; set; }
        public string SURVEYOR_TYPE { get; set; }
        public double PHYSICAL_FEES { get; set; }
        public double PHYSICAL_AMOUNT { get; set; }
        public double PHYSICAL_VAT { get; set; }
        public double PHYSICAL_TOTAL_DISCOUNT { get; set; }
        public double PHYSICAL_TOTAL_ADDITIONAL { get; set; }
        public double PHYSICAL_TOTAL_AMOUNT { get; set; }

        public string VEHICLE_LOSS_AMOUNT { get; set; }
        public double INSUR_TOTAL_AMOUNT { get; set; }

    }

    /// <summary>
    /// thông tin giám định
    /// </summary>
    public class DetailCalendar
    {
        public string CATEGORY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string PACK_CODE { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string SURVEYOR_TYPE { get; set; }
        public string EFF { get; set; }
        public string EXP { get; set; }
        public string TIME_F { get; set; }
        public string TIME_T { get; set; }
        public string CONTACT { get; set; }
        public string PHONE { get; set; }
        public string PROV { get; set; }
        public string DIST { get; set; }
        public string WARDS { get; set; }
        public string ADDRESS { get; set; }
        public string ADDRESS_FORM { get; set; }
        public string CAL_CODE { get; set; }
        public string CALENDAR_STATUS { get; set; }

    }

}
