﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Landing.MultiProduct
{
    public class OutVehicleLanding : VEHICLE_PRODUCT
    {
        public string[] DISABLE { get; set; }
        public double VAT { get; set; } = 0;
    }
}
