﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Landing.MultiProduct
{
    public class OutProductInit
    {
        /// <summary>
        /// Đối tác bán
        /// </summary>
        public string ORG_SELLER { get; set; }
        /// <summary>
        /// Mã kênh bán
        /// </summary>
        public string CHANNEL { get; set; }
        public double AMOUNT { get; set; } = 0;
        public double TOTAL_DISCOUNT { get; set; } = 0;
        public double TOTAL_VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; } = 0;

        public string DURATION { get; set; }
        public string DURATION_UNIT { get; set; }

        public object FORM_VALID { get; set; }

        public List<object> LIST_INSURED { get; set; }
    }
}
