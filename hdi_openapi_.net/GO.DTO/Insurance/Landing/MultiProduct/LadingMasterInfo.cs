﻿using GO.DTO.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Lading.MultiProduct
{
    public class LadingMasterInfo
    {
        public MasterOrders ORDER { get; set; }
    }
    public class MasterOrders
    {
        /// <summary>
        /// Mã đơn hàng từ sàn
        /// </summary>
        public string ORDER_ORG { get; set; }
        /// <summary>
        /// Đối tác bán
        /// </summary>
        public string ORG_SELLER { get; set; }
        /// <summary>
        /// Mã kênh bán
        /// </summary>
        public string CHANNEL { get; set; }
        public string CHANNEL_NAME { get; set; }
        public ORD_BUYER BUYER { get; set; }
        /// <summary>
        /// Số lượng sản phẩm mua
        /// </summary>
        public int COUNT_PRODUCT { get; set; }
        /// <summary>
        /// Tổng giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Tổng vat
        /// </summary>
        public double TOTAL_VAT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Ngày thanh toán
        /// </summary>
        public string DATE_PAY { get; set; }
        /// <summary>
        /// Trạng thái đơn. Mặc định WAIT_PAY
        /// </summary>
        public string STATUS { get; set; } = "WAIT_PAY";
        public string STATUS_NAME { get; set; }
        public string PAYMENT_TYPE { get; set; }
        public string DESCRIPTION { get; set; }
        /// <summary>
        /// Danh sách sản phẩm mua
        /// </summary>
        public List<LadingProduct> LIST_PRODUCT { get; set; }
    }
    public class LadingProduct
    {
        public string DETAIL_CODE { get; set; }
        /// <summary>
        /// Mã sản phẩm đóng gói
        /// </summary>
        public string MAR_CODE { get; set; }
        /// <summary>
        /// Danh mục sản phẩm
        /// </summary>
        public string CATEGORY { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PRODUCT_NAME { get; set; }
        /// <summary>
        /// Mã gói
        /// </summary>
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int COUNT { get; set; }
        /// <summary>
        /// Số tiền
        /// </summary>
        public double AMOUNT { get; set; }
        /// <summary>
        /// Tổng giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; }
        /// <summary>
        /// Tổng vat
        /// </summary>
        public double TOTAL_VAT { get; set; }
        /// <summary>
        /// Tổng tiền thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; }
        /// <summary>
        /// Trạng thái của sản phẩm
        /// </summary>
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
        public string PATH_IMG { get; set; }
        public string IS_BUY_MORE { get; set; }
        public string REF_VAL { get; set; }
    }

    public class MasterInfo : ORD_BUYER
    {
        public string OSKU_CODE { get; set; }
        /// <summary>
        /// Mã đơn hàng từ sàn
        /// </summary>
        public string ORDER_ORG { get; set; }
        /// <summary>
        /// Đối tác bán
        /// </summary>
        public string ORG_SELLER { get; set; }
        /// <summary>
        /// Mã kênh bán
        /// </summary>
        public string CHANNEL { get; set; }
        public string CHANNEL_NAME { get; set; }
        /// <summary>
        /// Số lượng sản phẩm mua
        /// </summary>
        public int COUNT_PRODUCT { get; set; }
        /// <summary>
        /// Tổng giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; } = 0;
        /// <summary>
        /// Tổng vat
        /// </summary>
        public double TOTAL_VAT { get; set; } = 0;
        /// <summary>
        /// Tổng tiền
        /// </summary>
        public double TOTAL_AMOUNT { get; set; } = 0;
        /// <summary>
        /// Ngày thanh toán
        /// </summary>
        public string DATE_PAY { get; set; }
        /// <summary>
        /// Trạng thái đơn. Mặc định WAIT_PAY
        /// </summary>
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
    }

}
