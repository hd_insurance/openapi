﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance.Lading.MultiProduct
{
    public class LandingDetailInfo
    {
        /// <summary>
        /// Mã sản phẩm đóng gói
        /// </summary>
        public string MAR_CODE { get; set; }
        /// <summary>
        /// Danh mục sản phẩm
        /// </summary>
        public string CATEGORY { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        public string PRODUCT_NAME { get; set; }
        /// <summary>
        /// Mã gói
        /// </summary>
        public string PACK_CODE { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int COUNT { get; set; }
        /// <summary>
        /// Số tiền
        /// </summary>
        public double AMOUNT { get; set; }
        /// <summary>
        /// Tổng giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; }
        /// <summary>
        /// Tổng vat
        /// </summary>
        public double TOTAL_VAT { get; set; }
        /// <summary>
        /// Tổng tiền thanh toán
        /// </summary>
        public double TOTAL_AMOUNT { get; set; }
        /// <summary>
        /// Trạng thái của sản phẩm
        /// </summary>
        public string STATUS { get; set; }

        public object OBJ_INIT { get; set; }
        public string REF_VAL { get; set; }

        public string DURATION { get; set; }
        public string DURATION_UNIT { get; set; }

        public object FORM_VALID { get; set; }
    }
}
