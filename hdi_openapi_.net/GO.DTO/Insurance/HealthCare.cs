﻿using GO.DTO.Insurance.Common;
using GO.DTO.Org.Vietjet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class HealthCare
    {
        /// <summary>
        /// Thông tin máy bay với sản phẩm bay của VJ
        /// </summary>
        //public FlightVJC FLIGHT { get; set; }
        /// <summary>
        /// Danh sách người được bảo hiểm
        /// </summary>
        public List<InsuredPerson> INSURED { get; set; }
        /// <summary>
        /// File chung cho contract, Người mua
        /// </summary>
        public List<DetailFile> FILE { get; set; }
    }
}
