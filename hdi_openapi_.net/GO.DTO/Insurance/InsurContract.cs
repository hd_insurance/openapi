﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Insurance
{
    public class InsurContract
    {
        public string CONTRACT_CODE { get; set; }
        public string CONTRACT_NO { get; set; }
        public string CONTRACT_TYPE { get; set; }
        public string CONTRACT_MODE { get; set; }
        public string REF_ID { get; set; }
        public string CUS_CODE { get; set; }
        public string TYPE { get; set; }
        public string NATIONALITY { get; set; }
        public string NAME { get; set; }
        public string DOB { get; set; }
        public string GENDER { get; set; }
        public string PROVINCE { get; set; }
        public string DISTRICT { get; set; }
        public string WARDS { get; set; }
        public string ADDRESS { get; set; }
        public string IDCARD { get; set; }
        public string IDCARD_D { get; set; }
        public string IDCARD_P { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string TAXCODE { get; set; }
        public string CATEGORY { get; set; }
        public string CURRENCY { get; set; }
        public double AMOUNT { get; set; } // 26/07/2021 đổi string thành double
        public string DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EFFECTIVE_NUM { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string EXPIRATION_NUM { get; set; }
        public string MEMBER_CODE { get; set; }
        public string ORG_CODE { get; set; }
        public string CHANEL { get; set; }
        public string LINK_TRAFIC { get; set; }
        public string ORG_TRAFIC { get; set; }
        public string ENVIROMENT { get; set; }
        public string STATUS { get; set; }
        public string STATUS_NAME { get; set; }
        public string PARENT_CODE { get; set; }
        public string NUMBER_OBJECT { get; set; }
        public string MAX_NUMBER_OBJECT { get; set; }
        public string REGION { get; set; }
        public string PRODUCT_CODE { get; set; }

        public string PRODUCT_NAME { get; set; }
        public string PACK_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string PACK_NAME { get; set; }
        public string SELLER_CODE { get; set; }
        // Start 2022-08-18 By ThienTVB
        public string SELLER_MAIL { get; set; } = "";
        // End 2022-08-18 By ThienTVB
        public string ORG_SELLER { get; set; }
        public string ADDRESS_FORM { get; set; }
        public string ADDRESS_FULL { get; set; }
        public string TYPE_VAT { get; set; }
        public string CHANNEL { get; set; }

        public string PROV { get; set; }
        public string DIST { get; set; }

    }
    public class InsurContractExt : InsurContract
    {
        public string EFFECTIVE_DATE_EN { get; set; }
        public string HH_TO { get; set; }
        public string SS_TO { get; set; }
        public string DAY_TO { get; set; }
        public string MM_TO { get; set; }
        public string YY_TO { get; set; }
        public string EXPIRATION_DATE_EN { get; set; }
        public string HH_F { get; set; }
        public string SS_F { get; set; }
        public string DAY_F { get; set; }
        public string MM_F { get; set; }
        public string YY_F { get; set; }
        public string EXPIRATION_DATE1 { get; set; } //ngày hiệu lực +1 day
        public string EXPIRATION_DATE1_EN { get; set; }
        public string MONEY_VN { get; set; }
        public string MONEY { get; set; }
        public string MONEY_ENT { get; set; }
        public string URL_FILE { get; set; }
        public string RELATIONSHIP_VN { get; set; }
        public string REGION_VI { get; set; }
        public string REGION_EN { get; set; }
        public string BENEFICIARY { get; set; }

    }
}
