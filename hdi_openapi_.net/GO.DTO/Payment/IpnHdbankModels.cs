﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class IpnHdbankModels
    {
        public string code { get; set; }
        public string message { get; set; }
        public string msgType { get; set; }
        public string txnId { get; set; }
        public string qrTrace { get; set; }
        public string bankCode { get; set; }
        public string mobile { get; set; }
        public string accountNo { get; set; }
        public string amount { get; set; }
        public string payDate { get; set; }
        //public string merchantCode { get; set; }
        public string terminalId { get; set; }
        public string checksum { get; set; }
    }
    public class AddData
    {
        public string productId { get; set; }
        public string amount { get; set; }
        public string tipAndFee { get; set; }
        public string gifCode { get; set; }
        public string ccy { get; set; }
        public string qty { get; set; }
        public string note { get; set; }
    }
    public class IpnBankTransfer
    {
        public string orgCode { get; set; }
        public string transactionType { get; set; }
        public string accountNumber { get; set; }
        public string accountHolderName { get; set; }
        public string bankHolderName { get; set; }
        public string totalAmount { get; set; }
        public List<TransferDetail> data { get; set; }
        public string checksum { get; set; }
    }
    public class TransferDetail
    {
        public string code { get; set; }
        public string message { get; set; }
        public string transactionID { get; set; }
        public string transactionDate { get; set; }
        public string effectiveDate { get; set; }
        public string orderNumber { get; set; }

        public string cardNumber { get; set; }
        public string bankName { get; set; }
        public string cardName { get; set; }
        public string cardAddress { get; set; }

        public string amount { get; set; }
        public string currency { get; set; }
        public string detailsOfPayment { get; set; }
        public string terminalId { get; set; }
        public string transactionType { get; set; }

    }
    public class TransferDetailInsert
    {
        public string orgCode { get; set; }
        public string transactionType { get; set; }        
        public string accountNumber { get; set; }
        public string accountHolderName { get; set; }
        public string bankHolderName { get; set; }
        public string totalAmount { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public string transactionID { get; set; }
        public string orderCode { get; set; }
        public string transactionDate { get; set; }
        public string effectiveDate { get; set; }
        public string orderNumber { get; set; }

        public string cardNumber { get; set; }
        public string bankName { get; set; }
        public string cardName { get; set; }
        public string cardAddress { get; set; }

        public string amount { get; set; }
        public string currency { get; set; }
        public string detailsOfPayment { get; set; }
        public string terminalId { get; set; }

    }
}
