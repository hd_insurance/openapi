﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class QRCodeModels
    {
        public class CREATE_QR
        {
            [StringLength(100, MinimumLength = 1, ErrorMessage = " appId không vượt quá 100 ký tự, không được để trống")]
            public string appId { get; set; }
            [StringLength(25, MinimumLength = 1, ErrorMessage = " merchantName không vượt quá 25 ký tự, không được để trống")]
            public string merchantName { get; set; }
            [StringLength(20, MinimumLength = 1, ErrorMessage = " serviceCode không vượt quá 20 ký tự, không được để trống")]
            public string serviceCode { get; set; }
            [StringLength(2, MinimumLength = 1, ErrorMessage = " countryCode không vượt quá 2 ký tự, không được để trống")]
            public string countryCode { get; set; }
            [StringLength(20, MinimumLength = 1, ErrorMessage = " merchantCode không vượt quá 20 ký tự, không được để trống")]
            public string merchantCode { get; set; }
            [StringLength(100, MinimumLength = 1, ErrorMessage = " masterMerCode không vượt quá 100 ký tự, không được để trống")]
            public string masterMerCode { get; set; }
            [StringLength(9, MinimumLength = 1, ErrorMessage = " merchantType không vượt quá 9 ký tự, không được để trống")]
            public string merchantType { get; set; }
            [StringLength(15, MinimumLength = 0, ErrorMessage = " terminalId không vượt quá 15 ký tự")]
            public string terminalId { get; set; }
            [StringLength(4, MinimumLength = 1, ErrorMessage = " payType không vượt quá 4 ký tự, không được để trống")]
            public string payType { get; set; }
            [StringLength(20, MinimumLength = 0, ErrorMessage = " productId không vượt quá 20 ký tự")]
            public string productId { get; set; }
            [StringLength(15, MinimumLength = 1, ErrorMessage = " txnId không vượt quá 15 ký tự, không được để trống")]
            public string txnId { get; set; }
            [StringLength(15, MinimumLength = 1, ErrorMessage = " billNumber không vượt quá 15 ký tự, không được để trống")]
            public string billNumber { get; set; }
            [StringLength(13, MinimumLength = 1, ErrorMessage = " amount không vượt quá 13 ký tự, không được để trống")]
            public string amount { get; set; }
            [StringLength(4, MinimumLength = 0, ErrorMessage = " ccy không vượt quá 4 ký tự")]
            public string ccy { get; set; }
            [StringLength(14, MinimumLength = 0, ErrorMessage = " expDate không vượt quá 14 ký tự")]
            public string expDate { get; set; }
            [StringLength(19, MinimumLength = 0, ErrorMessage = "desc không vượt quá 19 ký tự")]
            public string desc { get; set; }

            [StringLength(20, MinimumLength = 0, ErrorMessage = " tipAndFee không vượt quá 20 ký tự")]
            public string tipAndFee { get; set; }
            [StringLength(20, MinimumLength = 0, ErrorMessage = " consumerID không vượt quá 20 ký tự")]
            public string consumerID { get; set; }
            [StringLength(19, MinimumLength = 0, ErrorMessage = "Mã purpose (dịch vụ billing) không vượt quá 19 ký tự")]
            public string purpose { get; set; }
            [StringLength(32, MinimumLength = 1, ErrorMessage = " checksum không vượt quá 32 ký tự, không được để trống")]
            public string url { get; set; }
            public string checksum { get; set; }
            public CREATE_QR()
            {
                this.billNumber = "";
                this.consumerID = "";
                this.purpose = "";
            }
        }
        public class CHECK_QR
        {

            public string txnId { get; set; }

            public string masterMerCode { get; set; }
            public string merchantCode { get; set; }
            public string terminalID { get; set; }
            public string payDate { get; set; }
            public string cusMobile { get; set; }

            public string cusName { get; set; }
            public string debitAmount { get; set; }
            public string checkSum { get; set; }
            public string secretKey { get; set; }
            public CHECK_QR()
            {

            }
        }
        public class UPDATE_QR
        {
            public string code { get; set; }
            public string message { get; set; }
            public string msgType { get; set; }

            public string txnId { get; set; }

            public string qrTrace { get; set; }

            public string bankCode { get; set; }

            public string mobile { get; set; }

            public string accountNo { get; set; }

            public string amount { get; set; }

            public string payDate { get; set; }

            public string merchantCode { get; set; }

            public string terminalId { get; set; }

            public string name { get; set; }

            public string phone { get; set; }

            public string province_id { get; set; }

            public string district_id { get; set; }

            public string address { get; set; }

            public string email { get; set; }

            public string addData { get; set; }

            public string checksum { get; set; }
            public UPDATE_QR()
            {

            }
        }
        public class DATA
        {
            public string productId { get; set; }
            public string amount { get; set; }
            public string tipAndFee { get; set; }
            public string ccy { get; set; }
            public string qty { get; set; }
            public string note { get; set; }
            public string serviceCode { get; set; }
            public DATA()
            {

            }
        }
        public class RESULT_QR
        {
            public string code { get; set; }
            public string message { get; set; }
            public string data { get; set; }
            public string idQrcode { get; set; }
            public string masterMerchantCode { get; set; }
            public string merchantCode { get; set; }
            public string terminalID { get; set; }
            public string billNumber { get; set; }
            public string txnId { get; set; }
            public string payDate { get; set; }
            public string qrTrace { get; set; }
            public string bankCode { get; set; }
            public string debitAmount { get; set; }
            public string realAmount { get; set; }
            public string checkSum { get; set; }
        }
    }
}
