﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class Order_Create
    {
        public string appId { get; set; }
        public string orderId { get; set; }
        public string refId { get; set; }
        public string amount { get; set; }
        public string payDate { get; set; }
        public string desc { get; set; }
        public string desc_en { get; set; }
        public string contentPay { get; set; }
        public string payer_name { get; set; }
        public string payer_mobile { get; set; }
        public string payer_email { get; set; }
        public string orderType { get; set; }
        public string tipAndFee { get; set; }
        public string terminalId { get; set; }
        public string url_callback { get; set; }
        public string struct_code { get; set; }
    }
    public class OrderInfo
    {
        public string OrderId { get; set; }
        public long Amount { get; set; }
        public string OrderDesc { get; set; }

        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }

        public string PaymentTranId { get; set; }
        public string BankCode { get; set; }
        public string PayStatus { get; set; }
    }
    public class StatusCheck
    {
        public string TRANSACTION { get; set; }
        public string ORG_PAY_CODE { get; set; }
        public string TRACE { get; set; }

        public string AMOUNT { get; set; }
        public string SUMMARY { get; set; }

        public string BANK_CODE { get; set; }
        public string STATUS { get; set; }
        public string PAY_DATE { get; set; }
        public string MESSAGE { get; set; }
        public string LINK_CALLBACK { get; set; }
    }
}
