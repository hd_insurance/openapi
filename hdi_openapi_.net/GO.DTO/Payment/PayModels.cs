﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class PayModels
    {
    }
    public class PayAssignModels
    {
        public string ID { get; set; }
        public string ORG_SELLER { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string BANK_ID { get; set; } //danh sách các cổng thanh toán cho phép thanh toán
        public string PAYMENTS_TYPE { get; set; } //hiển thị các loại thanh toán trên cổng của đối tác
        public string QR_ID { get; set; } //sinh QR của đối tác nào
        public string IS_TRANSFER { get; set; } // có sử dụng chuyển khoản không
        public string EXP_DATE { get; set; }
        public string DESCRIPTION { get; set; }
        public string URL_CALLBACK { get; set; }
        public string BANK_ACCOUNT { get; set; }
        public string BANK_BRANCH { get; set; }
        public string BANK_BRANCH_EN { get; set; }
        public string BANK_NAME { get; set; }
        public string BANK_NAME_EN { get; set; }
        public string ACCOUNT_NAME { get; set; }
        public string ACCOUNT_NAME_EN { get; set; }
        public PayConfigModels QR_PAY { get; set; }
        public List<PayConfigModels> PAY_CONFIG { get; set; }
    }
    public class PayOrgModels
    {
        public string ORG_ID { get; set; }
        public string ORG_CODE { get; set; }
        public string ORG_NAME { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; } 
        public string NAME { get; set; }
        public string PAY_TYPE { get; set; } //cổng thanh toán hay QR
        public string ORG_NAME_EN { get; set; } 
        public string ORG_TYPE { get; set; }
        public string LOGO { get; set; }
        public List<PayConfigModels> PAY_CONFIG { get; set; }
        public List<PayFuncModels> PAY_FUNC { get; set; }
    }
    public class PayFuncModels
    {
        public string ORG_ID { get; set; }
        public string URL { get; set; }
        public string METHOD { get; set; }
        public string HEADER { get; set; }
        public string TYPE_BODY { get; set; }
        public string TYPE_RAW { get; set; }
        public string TYPE_METHOD { get; set; }
    }
    public class PayCallBackModels
    {
        public string ORG_SELLER { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string CHANNEL { get; set; }
        public string CONFIG { get; set; }
        public string URL { get; set; }
        public string METHOD { get; set; }
        public string HEADER { get; set; }
        public string TYPE_BODY { get; set; }
        public string TYPE_RAW { get; set; }
        public string TYPE_METHOD { get; set; }
        public string IS_ACTIVE { get; set; }
        public string DESCRIPTION { get; set; }
    }
    public class PayConfigModels
    {
        public string BANK_ID { get; set; }
        public string ORG_ID { get; set; }
        public string CLIENT_ID { get; set; }
        public string SECRET { get; set; }
        public string MERCHANT_ID { get; set; }
        public string TERMINAL_ID { get; set; } 
        public string MASTER_CODE { get; set; }
        public string USERNAME { get; set; } 
        public string PASSWORD { get; set; }
        public string MIN_AMOUNT { get; set; }
        public string MAX_AMOUNT { get; set; }
        public string CLIENT_NAME { get; set; }
        public string SERVICE_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string CCY { get; set; }
        public string BANK_CODE { get; set; }
        public string DATA_EXT { get; set; }
        public string BANK_ACCOUNT { get; set; }
        public string BANK_BRANCH { get; set; }
        public string BANK_BRANCH_EN { get; set; }
        public string ACCOUNT_NAME { get; set; }
        public string ACCOUNT_NAME_EN { get; set; }
        public string DESCRIPTION { get; set; }
        public string BANK_NAME { get; set; }
        public string BANK_NAME_EN { get; set; }
        public string PAY_CODE { get; set; }
    }
    public class CreateTrans
    {
        public string orgSeller { get; set; }
        public string productCode { get; set; }
        public string structCode { get; set; }
        public string channel { get; set; }
        public string orderId { get; set; }
        public string payDate { get; set; }
        public string amount { get; set; }
        public string taxAmount { get; set; }
        public string discountAmount { get; set; }
        public string totalAmount { get; set; }
        public string desc { get; set; }
        public string desc_En { get; set; }
        public string contentPay { get; set; }
        public string payerName { get; set; }
        public string payerMobile { get; set; }
        public string payerEmail { get; set; }

        // Add pay_type
        public string payType { get; set; }
    }
    public class ResultTrans
    {
        public string orgSeller { get; set; }
        public string productCode { get; set; }
        public string structCode { get; set; }
        public string channel { get; set; }
        public string orderId { get; set; }
        public string payDate { get; set; }
        public string totalAmount { get; set; }
        public string expDate { get; set; }
        public string transactionNo { get; set; }
        public string bankCode { get; set; }
        public string url_redirect { get; set; }
        public string imageQR { get; set; }
        public string dataQR { get; set; }

    }
    public class CREATE_QR
    {
        [StringLength(100, MinimumLength = 1, ErrorMessage = " appId không vượt quá 100 ký tự, không được để trống")]
        public string appId { get; set; }
        [StringLength(25, MinimumLength = 1, ErrorMessage = " merchantName không vượt quá 25 ký tự, không được để trống")]
        public string merchantName { get; set; }
        [StringLength(20, MinimumLength = 1, ErrorMessage = " serviceCode không vượt quá 20 ký tự, không được để trống")]
        public string serviceCode { get; set; }
        [StringLength(2, MinimumLength = 1, ErrorMessage = " countryCode không vượt quá 2 ký tự, không được để trống")]
        public string countryCode { get; set; }
        [StringLength(20, MinimumLength = 1, ErrorMessage = " merchantCode không vượt quá 20 ký tự, không được để trống")]
        public string merchantCode { get; set; }
        [StringLength(100, MinimumLength = 1, ErrorMessage = " masterMerCode không vượt quá 100 ký tự, không được để trống")]
        public string masterMerCode { get; set; }
        [StringLength(9, MinimumLength = 1, ErrorMessage = " merchantType không vượt quá 9 ký tự, không được để trống")]
        public string merchantType { get; set; }
        [StringLength(15, MinimumLength = 0, ErrorMessage = " terminalId không vượt quá 15 ký tự")]
        public string terminalId { get; set; }
        [StringLength(4, MinimumLength = 1, ErrorMessage = " payType không vượt quá 4 ký tự, không được để trống")]
        public string payType { get; set; }
        [StringLength(20, MinimumLength = 0, ErrorMessage = " productId không vượt quá 20 ký tự")]
        public string productId { get; set; }
        [StringLength(15, MinimumLength = 1, ErrorMessage = " txnId không vượt quá 15 ký tự, không được để trống")]
        public string txnId { get; set; }
        [StringLength(15, MinimumLength = 1, ErrorMessage = " billNumber không vượt quá 15 ký tự, không được để trống")]
        public string billNumber { get; set; }
        [StringLength(13, MinimumLength = 1, ErrorMessage = " amount không vượt quá 13 ký tự, không được để trống")]
        public string amount { get; set; }
        [StringLength(4, MinimumLength = 0, ErrorMessage = " ccy không vượt quá 4 ký tự")]
        public string ccy { get; set; }
        [StringLength(14, MinimumLength = 0, ErrorMessage = " expDate không vượt quá 14 ký tự")]
        public string expDate { get; set; }
        [StringLength(19, MinimumLength = 0, ErrorMessage = "desc không vượt quá 19 ký tự")]
        public string desc { get; set; }

        [StringLength(20, MinimumLength = 0, ErrorMessage = " tipAndFee không vượt quá 20 ký tự")]
        public string tipAndFee { get; set; }
        [StringLength(20, MinimumLength = 0, ErrorMessage = " consumerID không vượt quá 20 ký tự")]
        public string consumerID { get; set; }
        [StringLength(19, MinimumLength = 0, ErrorMessage = "Mã purpose (dịch vụ billing) không vượt quá 19 ký tự")]
        public string purpose { get; set; }
        [StringLength(32, MinimumLength = 1, ErrorMessage = " checksum không vượt quá 32 ký tự, không được để trống")]
        public string url { get; set; }
        public string checksum { get; set; }
        public CREATE_QR()
        {
            this.billNumber = "";
            this.consumerID = "";
            this.purpose = "";
        }
    }
    public class RESULT_QR
    {
        public string code { get; set; }
        public string message { get; set; }
        public string data { get; set; }
        public string idQrcode { get; set; }
        public string masterMerchantCode { get; set; }
        public string merchantCode { get; set; }
        public string terminalID { get; set; }
        public string billNumber { get; set; }
        public string txnId { get; set; }
        public string payDate { get; set; }
        public string qrTrace { get; set; }
        public string bankCode { get; set; }
        public string debitAmount { get; set; }
        public string realAmount { get; set; }
        public string checkSum { get; set; }
    }
    public class ReqPayModel
    {
        public string Id { get; set; }
        public string lang { get; set; }
        public string callback { get; set; }
    }
    public class RepPayModel
    {
        public string INS_CONTRACT { get; set; }
        public List<PayInfo> lstPayInfo { get; set; }
        public List<PayGateway> lstPayGateway { get; set; }
    }

    public class PayInfo
    {

        public string Total_Amount { get; set; }
        public string Total_Amount_Text { get; set; }
        public string Amount { get; set; }
        public string VAT { get; set; }
        public string Gif_Code { get; set; }
        public string Discount { get; set; }
        public string Transaction_Id { get; set; }
        public string Payer_Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Status_Name { get; set; }
        public string Date_Pay { get; set; }
        public string Path_Logo { get; set; }
        public string Path_Banner { get; set; }
        public string Qr_Code { get; set; }
        public string Qr_Expire { get; set; }
        public string Min_Amount { get; set; }
        public string Max_Amount { get; set; }
        public string Currency { get; set; }
        public string Contract_Code { get; set; }
        public string Description_Min { get; set; }
        public string Description_Max { get; set; }
        public string Bank_Account { get; set; }
        public string Bank_Branch { get; set; }
        public string Bank_Name { get; set; }
        public string Account_Name { get; set; }
        public string Content_Pay { get; set; }
        public string Order_Id { get; set; }
        public string Recipient { get; set; } = "Công ty TNHH Bảo Hiểm HD";
        // Add 2022-12-06 By ThienTVB add Pay_type request from Vinh
        public string Pay_Type { get; set; }
        // End 2022-12-06
    }

    public class PayGateway
    {
        public string Org_Code { get; set; }
        public string Org_Name { get; set; }
        public string Path_Logo { get; set; }
        public string Min_Amount { get; set; }
        public string Max_Amount { get; set; }
        public string Description_Min { get; set; }
        public string Description_Max { get; set; }
        public bool Disable { get; set; }
        public string Total_Pay { get; set; }
        public string LinkPay { get; set; }
    }

    public class TransInfo
    {
        public string ORDER_CODE { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string ORG_INIT_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string PAY_DATE { get; set; }
        public string CREATE_DATE { get; set; }
        public string DISCOUNT { get; set; }
        public string TRANSACTION_TRACE { get; set; }
        public string CURRENCY { get; set; }
        public string ORDER_TITLE { get; set; }
        public string TRANSACTION_REF { get; set; }
        public string STATUS { get; set; }
        public string DATA_CHECK { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string PAY_DISCOUNT { get; set; }
        public string TOTAL_DISCOUNT { get; set; }
        public string QR_DATA { get; set; }
        public string PAYER_NAME { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string ORDER_SUMMARY { get; set; }
        public string ORG_PAY_CODE { get; set; }
        public string ASSIGN_ID { get; set; }
        public string CONTENT_TRANSFER { get; set; }
        public string QR_CODE { get; set; }
    }

    public class TransDetail
    {
        public string DETAIL_ID { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string ORG_PAY_CODE { get; set; }
        public string STATUS { get; set; }
        public string URL_PAY { get; set; }
        public string URL_CALLBACK { get; set; }
    }
}
