﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class ConfigOrgModels
    {
        public List<SYS_CALLBACK> CALLBACK { get; set; }
        public List<ORG_PAY> LS_ORG_PAY { get; set; }
    }
    public class SYS_CALLBACK
    {
        public string ORG_CODE { get; set; }
        public string ENVIROMENT { get; set; }
        public string URL_IPN { get; set; }
        public string URL_CALLBACK { get; set; }
        public string ACTIVE { get; set; }
        public string URL_CALL { get; set; }
    }
    public class ORG_PAY
    {
        public string ID { get; set; }
        public string ORG_PAY_CODE { get; set; }
        public string ORG_AGENCY_CODE { get; set; }
        public string ENVIROMENT { get; set; }
        public string IS_LOGIN { get; set; }
        public string CLIENT_ID { get; set; }
        public string SECRET { get; set; }
        public string MERCHANT_ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORD { get; set; }
        public string MIN_AMOUNT { get; set; }
        public string MAX_AMOUNT { get; set; }
        public string GUARANTEE_AMOUNT { get; set; }
        public string PAY_TYPE { get; set; }
        public string TERMINAL_ID { get; set; }
        public string CLIENT_NAME { get; set; }
        public string MASTER_CODE { get; set; }
        public string IS_ACTIVE { get; set; }        
        public string SERVICE_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string PAY_CODE { get; set; }
        public string CCY { get; set; }
        public string IPN_REPLACE { get; set; }
        public string IPN_KEY { get; set; }
        public string CHECK_MD5 { get; set; }
        public string AUTO_CREATE { get; set; }
        public string LINK_LOGO { get; set; }
        public List<PAY_FUNTION> LS_FUNTION { get; set; }

    }
    public class PAY_FUNTION
    {
        public string ORG_PAY_CODE { get; set; }
        public string PAY_CODE { get; set; }
        public string URL { get; set; }
        public string METHOD { get; set; }
        public string TYPE_BODY { get; set; }
        public string BODY { get; set; }
        public string TYPE_METHOD { get; set; }
        public string HEADER { get; set; }
        public string HEADER_REPLACE { get; set; }
        public string TYPE_RAW { get; set; }
        public string BODY_REPLACE { get; set; }
    }
}
