﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class PayTransModels
    {

    }
    public class TRANSACTION
    {
        public string ORDER_CODE { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string ORG_INIT_CODE { get; set; }
        public string AMOUNT { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string PAY_DATE { get; set; }
        public string CREATE_DATE { get; set; }
        public string DISCOUNT { get; set; }
        public string TRANSACTION_TRACE { get; set; }
        public string CURRENCY { get; set; }
        public string ORDER_TITLE { get; set; }
        public string TRANSACTION_REF { get; set; }
        public string STATUS { get; set; }
        public string DATA_CHECK { get; set; }
        public string TAX_AMOUNT { get; set; }
        public string PAY_DISCOUNT { get; set; }
        public string TOTAL_DISCOUNT { get; set; }
        public string QR_DATA { get; set; }
        public string PAYER_NAME { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
    }
    public class TRANSACTION_DETAIL
    {
        public string DETAIL_ID { get; set; }
        public string TRANSACTION_ID { get; set; }
        public string ORG_PAY_CODE { get; set; }
        public string STATUS { get; set; }
        public string URL_PAY { get; set; }
        public string URL_CALLBACK { get; set; }
    }
}
