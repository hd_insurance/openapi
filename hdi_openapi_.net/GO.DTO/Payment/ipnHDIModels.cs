﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Payment
{
    public class IpnHDIModels
    {
        public string code { get; set; }
        public string message { get; set; }
        public string trans_id { get; set; }
        public string order_trans_code { get; set; }
        public string amount { get; set; }
        public string pay_type { get; set; }
        public string user_name { get; set; }
    }    
}
