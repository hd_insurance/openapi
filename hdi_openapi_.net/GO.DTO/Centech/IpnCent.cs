﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Centech
{
    public class IpnCent
    {
        public string dvi_sl { get; set; }
        public string so_id { get; set; }
        public string tien { get; set; }
        public string currency { get; set; }
    }

    public class DataIpnCent
    {
        public string so_id { get; set; }
        public string so_id_dt { get; set; }
        public string nv { get; set; }
        public string goi_bh { get; set; }
        public string contract_code { get; set; }
        public string contract_no { get; set; }
        public string category { get; set; }
        public string product_code { get; set; }
    }
}
