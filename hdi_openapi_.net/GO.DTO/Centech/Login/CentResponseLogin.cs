﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Centech.Login
{
    public class CentResponseLogin
    {
        public string code { get; set; }
        public string message { get; set; }
        public string systemMessage { get; set; }
        public CentResponseLogin_data data { get; set; }
        public string Total { get; set; }
    }
    public class CentResponseLogin_data
    {
        public string token { get; set; }
        public string dvi_sl { get; set; }
        public string ten_dvi { get; set; }
        public string ten { get; set; }
        public string kenh { get; set; }
        public string ma_khoi { get; set; }
        public string ma_cn { get; set; }
        public string qu_nhap { get; set; }
        public string qu_qly { get; set; }
        
    }
}
