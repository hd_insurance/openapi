﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Centech.Loans
{
    public class CentInsurLoInfo
    {
        public string code { get; set; }
        public string message { get; set; }
        public string systemMessage { get; set; }
        public CentDataInfo data { get; set; }
    }

    public class CentDataInfo
    {
        public string dvi_sl { get; set; }
        public string id_sdbs_g { get; set; }
        public string id_sdbs_ct { get; set; }
        public string tb_sdbs { get; set; }
        public string kieu_hd_g { get; set; }
        public string ma_cn { get; set; }
        public List<Cent_ds_dk> ds_dk { get; set; }
        public List<Cent_ctgcn> ctgcn { get; set; }
        public List<Cent_ds_tt> ds_tt { get; set; }
    }

    public class CentMaCN
    {
        public string MA { get; set; }
        public string TEN_TAT { get; set; }
    }

    public class Cent_ds_dk
    {
        public string MA_DVI { get; set; }
        public string SO_ID { get; set; }
        public string SO_ID_DT { get; set; }
        public string TEN { get; set; }
        public string SO_CMT { get; set; }
        public string NGAY_CMT { get; set; }
        public string NOI_CMT { get; set; }
        public string DCHI { get; set; }
        public string GIOI { get; set; }
        public string QHE { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string SO_THANG_BH { get; set; }
        public string SO_NGAY_BH { get; set; }
        public string GIO_HL { get; set; }
        public string GIO_KT { get; set; }
        public string NT_TIEN { get; set; }
        public string TIEN_BH { get; set; }
        public string PT { get; set; }
        public string PHI { get; set; }
        public string GOI { get; set; }
        public string NOI_DI { get; set; }
        public string NOI_DEN { get; set; }
        public string TL_GIAM { get; set; }
        public string PVI_BH { get; set; }
        public string PVI_DLY { get; set; }
        public string NGAY_HL { get; set; }
        public string NGAY_KT { get; set; }
        public string NGAY_SINH { get; set; }
    }

    public class Cent_ctgcn
    {
        public string MA_DVI { get; set; }
        public string SO_ID { get; set; }
        public string NV { get; set; }
        public string SO_HD { get; set; }
        public string MA_KHOI { get; set; }
        public string KIEU_HD { get; set; }
        public string SO_HD_G { get; set; }
        public string HT_HD { get; set; }
        public string KENH { get; set; }
        public string MA_CN { get; set; }
        public string SALEID { get; set; }
        public string TEN_SALE { get; set; }
        public string GIO_HL { get; set; }
        public string GIO_KT { get; set; }
        public string NGAY_CAP { get; set; }
        public string NT_PHI { get; set; }
        public string K_THUE { get; set; }
        /// <summary>
        /// Trường Phí là trường trước khi giảm
        /// </summary>
        public string PHI { get; set; }
        public string THUE { get; set; }
        public string GIAM { get; set; }
        public string TRA_PHI { get; set; }
        public string SO_ID_D { get; set; }
        public string NSD { get; set; }
        public string NGAY_NH { get; set; }
        public string TTRANG { get; set; }
        public string DVI_SL { get; set; }
        public string SO_TC { get; set; }
        public string MA_DVI1 { get; set; }
        public string SO_ID1 { get; set; }
        public string SO_CIF_KH { get; set; }
        public string NHOM_DN { get; set; }
        public string TEN_DN { get; set; }
        public string MA_THUE { get; set; }
        public string SO_DKKD { get; set; }
        public string NGAY_DKKD { get; set; }
        public string DCHI_DN { get; set; }
        public string DD_DN { get; set; }
        public string CVU_DN { get; set; }
        public string EMAIL_DN { get; set; }
        public string PHONE_DN { get; set; }
        public string LOAI_TH { get; set; }
        public string NGUOI_TH { get; set; }
        public string DCHI_NGUOI_TH { get; set; }
        public string B_NG_SINH_NGUOI_TH { get; set; }
        public string CMT_NGUOI_TH { get; set; }
        public string PHONE_NGUOI_TH { get; set; }
        public string EMAIL_NGUOI_TH { get; set; }
        public string SUC_KHOE { get; set; }
        public string MA_DVI_G { get; set; }
        public string TTOAN_SDBS { get; set; }
        public string LOAI_SDBS { get; set; }
        public string PHI_SDBS { get; set; }
        public string HHONG { get; set; }
        public string TEN_TK { get; set; }
        public string MA_TK { get; set; }
        public string TEN_NH { get; set; }
        public string EMAIL_NHANG { get; set; }
        public string TB_SDBS { get; set; }
        public string NGAY_HT { get; set; }
        /// <summary>
        /// trường ttoan là trường người mua bảo hiểm phải thanh toán
        /// </summary>
        public string TTOAN { get; set; }
        public string NGAY_HL { get; set; }
        public string NGAY_KT { get; set; }
        public string SO_ID_DT { get; set; }
        public string TEN { get; set; }
        public string NGAY_SINH { get; set; }
        public string SO_CMT { get; set; }
        public string NGAY_CMT { get; set; }
        public string NOI_CMT { get; set; }
        public string DCHI { get; set; }
        public string GIOI { get; set; }
        public string QHE { get; set; }
        public string PHONE { get; set; }
        public string EMAIL { get; set; }
        public string SO_THANG_BH { get; set; }
        public string SO_NGAY_BH { get; set; }
        public string NT_TIEN { get; set; }
        public string TIEN_BH { get; set; }
        public string PT { get; set; }
        public string GOI { get; set; }
        public string NOI_DI { get; set; }
        public string NOI_DEN { get; set; }
        /// <summary>
        /// tỉ lệ giảm
        /// </summary>
        public string TL_GIAM { get; set; }
        public string PVI_BH { get; set; }
        public string PVI_DLY { get; set; }
        public string DVI_VP { get; set; }
        public string HD_KHAC { get; set; }
        public string TEN_HD_NN { get; set; }
        public string MA_THUE_HD_NN { get; set; }
        public string CTY_HD_NN { get; set; }
        public string DCHI_HD_NN { get; set; }
        public string DT_HD_NN { get; set; }
        public string EMAIL_HD_NN { get; set; }
        public string HT_HD_KENH { get; set; }
        public string SO_HD_KENH { get; set; }
        public string L_SUAT { get; set; }
        public string LOAI_GN { get; set; }
        public string NGAY_GN { get; set; }
        public string TRA_NO { get; set; }
        public string NT_VAY { get; set; }
        public string TIEN_VAY { get; set; }
        public double HAN_VAY { get; set; }
        public string HAN_VAY_NGAY { get; set; }
        public string HAN_VAY_HL { get; set; }
        public string HAN_VAY_KT { get; set; }
        public string EMAIL_NHABH { get; set; }
        public string ID_KENH { get; set; }
        public string SO_HD_VE { get; set; }
        public string SO_ID_VE { get; set; }
    }

    public class Cent_ds_tt
    {
        public string NGAY { get; set; }
        public string TIEN { get; set; }
        public string tt { get; set; }
    }

    public class CentIdInfo
    {
        public string so_id { get; set; }
        public string so_hd { get; set; }
        public string nv { get; set; }
        public string kieu_hd { get; set; }
        public string so_hd_g { get; set; }
        public string sao { get; set; }
    }
}
