﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Centech.Loans
{
    public class CentInsurLoAdd
    {
        public string dvi_sl { get; set; }
        public string SO_HD_KENH { get; set; }

        /// <summary>
        /// Mã cán bộ bán bảo hiểm
        /// </summary>
        public string SALEID { get; set; }
        /// <summary>
        /// Email cán bộ bán bảo hiểm
        /// </summary>
        public object EMAIL_NHANG { get; set; }
        /// <summary>
        /// Tên cán bộ bán bảo hiểm
        /// </summary>
        public string TEN_SALE { get; set; }
        /// <summary>
        /// HDI
        /// </summary>
        public string KENH { get; set; }
        /// <summary>
        /// Mã chi nhánh bán bảo hiểm
        /// </summary>
        public string MA_CN { get; set; }

        /// <summary>
        /// Số cif của khách hàng
        /// </summary>
        public object SO_CIF_KH { get; set; }
        /// <summary>
        /// Tên người được bảo hiểm
        /// </summary>
        public string TEN { get; set; }
        /// <summary>
        /// giới tính: 1: Nữ, 0 Nam
        /// </summary>
        public string GIOI { get; set; }
        /// <summary>
        /// Địa chỉ của người được bảo hiểm
        /// </summary>
        public string DCHI { get; set; }
        /// <summary>
        /// Địa chỉ email người được bảo hiểm
        /// </summary>
        public string EMAIL { get; set; }
        /// <summary>
        /// Địa chỉ email người được bảo hiểm
        /// </summary>
        public string PHONE { get; set; }
        /// <summary>
        /// Số điện thoại người được bảo hiểm
        /// </summary>
        public string NGAY_SINH { get; set; }
        /// <summary>
        /// Số chứng minh thư của người được bảo hiểm
        /// </summary>
        public string SO_CMT { get; set; }
        public string NOI_CMT { get; set; }
        public string NGAY_CMT { get; set; }


        public string NGUOI_TH { get; set; }
        public string DCHI_NGUOI_TH { get; set; }

        public string SO_TC { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        public string TTRANG { get; set; }

        /// <summary>
        /// nguyên tệ vay, giá trị mặc định là VND
        /// </summary>
        public string NT_VAY { get; set; }
        /// <summary>
        ///  kiểu hợp đồng, tạo mới sẽ có giá trị là G
        /// </summary>
        public string KIEU_HD { get; set; }
        /// <summary>
        /// K-điều kiện sức khỏe phù hợp, C- điều kiện sức khỏe không phù hợp với yêu cầu của bảo hiểm
        /// </summary>
        public string SUC_KHOE { get; set; }
        /// <summary>
        /// để mặc định là null
        /// </summary>
        public string HD_KHAC { get; set; }
        /// <summary>
        /// Số hợp đồng gốc nếu sdbs
        /// </summary>
        public object SO_HD_G { get; set; }
        /// <summary>
        /// Key của bản ghi định dạng double chuyển sang string để truyền qua API.Nếu truyền lên giá trị 0 thì được hiểu là cấp đơn mới, giá trị khác 0 thì là chỉnh sửa đơn đã tồn tại
        /// </summary>
        public string SO_ID { get; set; }
        public string SO_ID_VE { get; set; }
        /// <summary>
        /// Số hợp đồng hoặc số giấy chứng nhận
        /// </summary>
        public string SO_HD { get; set; }
        /// <summary>
        /// chính là ngày cấp GCN. thì hiện tại để giá trị tạm là ngày hiện tại nhập đơn, format dd/MM/yyyy
        /// </summary>
        public string NGAY_CAP { get; set; } = DateTime.Now.ToString("yyyyMMdd");

        /// <summary>
        /// Ngày hiệu lực cho đơn, format dd/mm/yyyy
        /// </summary>
        public string NGAY_HL { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        /// <summary>
        /// Ngày kết thúc đơn, format dd/mm/yyyy
        /// </summary>
        public string NGAY_KT { get; set; } = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy");
        /// <summary>
        /// "C", thể hiện cấp đơn theo cá nhân hay nhóm
        /// </summary>
        public string NHOM_DN { get; set; }
        /// <summary>
        /// trường ẩn nếu có giá trị là H thì là nhập bán theo nhóm, khác H là nhập bán lẻ
        /// </summary>
        public string HT_HD { get; set; }
        /// <summary>
        /// là mã nghiệp vụ. Hiện tại giá trị được truyền là ATTD (viết tắt An tâm tín dụng)
        /// </summary>
        public string NV { get; set; }
        public string NGAY_HT { get; set; } = DateTime.Now.ToString("dd/MM/yyyy");
        /// <summary>
        /// Số tiền bảo hiểm
        /// </summary>
        public int TIEN_BH { get; set; }
        public int TIEN_VAY { get; set; }
        public int HAN_VAY { get; set; }
        public string HAN_VAY_HL { get; set; }
    }

    public class CentGetInsurLoanRequest
    {
        public string ma_dvi { get; set; }
        public string so_id { get; set; }
        public string kieu_hd { get; set; }
        public string so_hd_g { get; set; }
        public string sao { get; set; }
    }
    public class CentLoansResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public string systemMessage { get; set; }
        public CentLoans_dataResponse data { get; set; }
        public string Total { get; set; }
    }

    public class CentLoans_dataResponse
    {
        public string so_id { get; set; }
        public string so_hd { get; set; }
        public string so_tc { get; set; }
        public string iurl { get; set; }
        public string token { get; set; }
    }

}
