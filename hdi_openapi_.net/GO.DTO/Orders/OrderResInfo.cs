﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class OrderResInfo
    {
        public string order_code { get; set; }
        public string str_period { get; set; }
        public double total_pay { get; set; }
        public string status { get; set; }
        public string contract_code { get; set; }
        public string contract_no { get; set; }
    }
}
