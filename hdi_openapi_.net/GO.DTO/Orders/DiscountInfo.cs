﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class DiscountInfo
    {
        public string REF_CODE { get; set; }
        /// <summary>
        /// Số tiền trước tính giảm
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        /// <summary>
        /// Tiền hoặc % giảm
        /// </summary>
        public double DISCOUNT { get; set; }
        /// <summary>
        /// Đơn vị giảm
        /// </summary>
        public string DISCOUNT_UNIT { get; set; }
        /// <summary>
        /// Tổng tiền giảm
        /// </summary>
        public double TOTAL_DISCOUNT { get; set; }
        /// <summary>
        /// Mã giảm giá
        /// </summary>
        public string GIFT_CODE { get; set; }

        /// <summary>
        /// loại phí: Chính, bổ sung
        /// </summary>
        public string TYPE_FEES { get; set; }
    }
}
