﻿using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Callback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class OrderCallback : Customer
    {
        public string ORDER_CODE { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string ACTION { get; set; }
        public string DESCRIPTION { get; set; }
        public List<OrderDetailCallback> DETAILS { get; set; }
        public string CHECKSUM { get; set; }
    }

    public class OrderDetailCallback
    {
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public InsurCallback INSUR_PRODUCT { get; set; }
    }
}
