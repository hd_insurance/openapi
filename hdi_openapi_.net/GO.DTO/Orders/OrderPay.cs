﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class OrderPay
    {
        public string appId { get; set; }
        public string orderId { get; set; }
        public string refId { get; set; }
        public string amount { get; set; }
        public string payDate { get; set; }
        public string desc { get; set; }
        public string desc_en { get; set; }
        public string contentPay { get; set; }
        public string payer_name { get; set; }
        public string payer_mobile { get; set; }
        public string payer_email { get; set; }
        public string orderType { get; set; }
        public string tipAndFee { get; set; }
        public string url_callback { get; set; }
        public string struct_code { get; set; }
    }
}
