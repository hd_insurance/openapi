﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class GiftInfo
    {
        public string GIFT_CODE { get; set; }
        public string GIFT_MODE { get; set; } // PARTNER_GIFT: với trường hợp gift của đối tác
        /// <summary>
        /// Số tiền trước tính giảm
        /// </summary>
        public double AMOUNT { get; set; } = 0;
        public double DISCOUNT { get; set; } // với trường hợp gift k phải của HDI
        public string DISCOUNT_UNIT { get; set; } // với trường hợp gift k phải của HDI
        public double TOTAL_DISCOUNT { get; set; } // với trường hợp gift k phải của HDI
    }
}
