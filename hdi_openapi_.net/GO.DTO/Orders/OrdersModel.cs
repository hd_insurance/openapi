﻿using GO.DTO.Insurance;
using GO.DTO.Insurance.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;
using GO.DTO.Org.Vietjet;
using GO.DTO.Insurance.House;
using GO.DTO.Insurance.Travel;

namespace GO.DTO.Orders
{
    public class OrdersModel
    {
        public string Channel { get; set; }
        public string UserName { get; set; }
        public ORD_COMMON COMMON { get; set; }
        public ORD_BUSINESS BUSINESS { get; set; }
    }

    #region Common
    public class ORD_COMMON
    {
        public ORD_SELLER SELLER { get; set; }
        public ORD_BUYER BUYER { get; set; }
        public ORD_RECEIVER RECEIVER { get; set; }
        public ORD_ORDER ORDER { get; set; }
        public List<ORD_ORDER_DETAIL> ORDER_DETAIL { get; set; }
        public ORD_PAYINFO PAY_INFO { get; set; }
        /// <summary>
        /// Thông tin in hóa đơn
        /// </summary>
        public Customer BILL_INFO { get; set; }
        public ORD_SKU ORD_SKU { get; set; }
        public ORD_PART ORD_PARTNER { get; set; }
    }
    public class ORD_SELLER: Seller
    {
    }
    public class ORD_BUYER : Customer
    {
        public string TYPE_VAT { get; set; }
        public string SHARE_CODE { get; set; }

        // 18/01/2022: thêm loại mua cá nhân hay gia đình: CN OR GD
        public string BUYER_MODE { get; set; }

    }
    public class ORD_RECEIVER : Customer
    {
    }
    public class ORD_ORDER
    {
        /// <summary>
        /// Lĩnh vực: BAO_HIEM: Bảo hiểm
        /// </summary>
        public string FIELD { get; set; }
        public string ORDER_CODE { get; set; }
        /// <summary>
        /// Kiểu: BH_M: Đơn nháp
        /// </summary>
        public string TYPE { get; set; }
        public string TITLE { get; set; }
        public string SUMMARY { get; set; }
        public double AMOUNT { get; set; }
        public double? DISCOUNT { get; set; } = 0;
        public string DISCOUNT_UNIT { get; set; }
        public double? VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string GIF_CODE { get; set; }
        /// <summary>
        /// Trạng thái đơn hàng 
        /// WAIT	Chưa thanh toán
        /// NEXT	Chuyển thanh toán
        /// PAYED Đã thanh toán
        /// </summary>
        public string STATUS { get; set; }
        /// <summary>
        /// Phương thức thanh toán
        //- ON_HDI_CHANNEL: Kênh thanh toán chảy qua HDI ghi nhận trạng thái
        //- ON_PARTNER_CHANNEL: Kênh thanh toán HDI ko nhận được giao dịch thanh toán chỉ nhận được giao dịch yêu cầu cấp đơn => Ghi nhận công nợ vào DEBIT
        /// </summary>
        public string PAY_METHOD { get; set; }

        public string TYPE_FEES { get; set; }
    }
    public class ORD_ORDER_DETAIL
    {
        public string FIELD { get; set; }
        /// <summary>
        /// Kiểu sản phẩm: VD: Bảo hiểm
        /// TRUC_TIEP: Hợp đồng trực tiếp 1-1. Thanh toán 1 lần duy nhất ứng với 1 HĐ bảo hiểm
        /// HD_BAO: Hợp đồng bao 1-n. 1 HĐ bảo hiểm bao và n chi tiết với n lần bảo hiểm (Giống n lần giải ngân ứng với n lần bảo hiểm)
        /// LINH_DONG: Hợp đồng linh động n-n. 1 HĐ tín dụng ứng với n bảo hiểm, với mỗi lần giải ngân là 1 hđ bảo hiểm
        /// </summary>
        public string PRODUCT_MODE { get; set; }
        public string PRODUCT_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string REF_ID { get; set; }
        public string ORG_STORE { get; set; }
        public string WEIGHT { get; set; }
        public int COUNT { get; set; } = 0;
        public double AMOUNT { get; set; }
        public double? DISCOUNT { get; set; } = 0;
        public string DISCOUNT_UNIT { get; set; }
        public double? VAT { get; set; } = 0;
        public double TOTAL_AMOUNT { get; set; }
        public string DESCRIPTION { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }

        public string MAPPING_ID { get; set; }
    }
    public class ORD_PAYINFO
    {
        public double? AMOUNT { get; set; }
        /// <summary>
        /// Hình thức thanh toán
        /// CTT: Cổng thanh toán
        /// CK: Chuyển khoản
        /// TH: Thu hộ
        /// CTT&TH: Hỗ hợp 
        /// </summary>
        public string PAYMENT_TYPE { get; set; }
        public string IS_SMS { get; set; }
        public string IS_EMAIL { get; set; }
        /// <summary>
        /// Với trường hợp thanh toán hỗ hợp thì 
        /// </summary>
        public List<PaymentMix> PAYMENT_MIX { get; set; }
    }
    /// <summary>
    /// Thông tin đơn hàng của sàn
    /// </summary>
    public class ORD_SKU
    {
        public string OSKU_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string MAR_CODE { get; set; }
    }
    /// <summary>
    /// thông tin đơn hàng từ đối tác
    /// </summary>
    public class ORD_PART
    {
        public string TRANS_ID { get; set; }
        public string REF_ID { get; set; }
        public string ENV { get; set; }
    }

    public class ORD_BUSINESS
    {
        //public LO LOAN { get; set; } // sau bỏ
        public HealthCare HEALTH_CARE { get; set; }
        public LoanInsurance LO { get; set; }
        public OrderVJC FLIGHT { get; set; }
        public VehicleInsur VEHICLE { get; set; }
        public OrdLoInsur LO_INSUR { get; set; } // Lo ver 2
        public List<HouseInsur> HOUSE_INSUR { get; set; }
        //Start 2022-05-10 By ThienTVB
        public List<DeviceInsur> DEVICE_INSUR { get; set; }
        //End 2022-05-10 By ThienTVB
        //KhanhPT 2023-07-20
        public TravelCare TRAVEL_CARE { get; set; }
        //End 2023-07-20
    }
    #endregion

    #region Insurance LOAN
    /*
    public class LO
    {
        public List<LO_INSURED> INSURED { get; set; }
    }
    public class LO_INSURED : Customer
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public string REF_ID { get; set; }
        public string CONTRACT_NO { get; set; }
        public string SO_ID_CENT { get; set; }
        public string SO_ID_DT_CENT { get; set; }
        public string NV_CENT { get; set; }
        public string GOI_BH_CENT { get; set; }
        public string RELATIONSHIP { get; set; }
        public List<Beneficiary> BENEFICIARY { get; set; }
        public string PRODUCT_CODE { get; set; }
        public double PRODUCT_FEES { get; set; }
        public string REGION { get; set; }
        public string REGION_PERCENT { get; set; }
        //public string ADDTIONAL_COVERAGE { get; set; }
        public double ADDITIONAL_FEES { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public double AMOUNT { get; set; }
        public double DISCOUNT { get; set; }
        public string DISCOUNT_UNIT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public string STATUS { get; set; }
        public string CERTIFICATE_NO { get; set; }
        /// <summary>
        /// Số tiền muốn bảo hiểm
        /// </summary>
        public double INSUR_TOTAL_AMOUNT { get; set; }

        public string BANK_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        /// <summary>
        /// Mã hđ tín dụng
        /// </summary>
        public string LO_CONTRACT { get; set; }
        /// <summary>
        /// Loại HĐ tín dụng
        /// TRUC_TIEP: Hợp đồng trực tiếp 1-1. Thanh toán 1 lần duy nhất ứng với 1 HĐ bảo hiểm
        /// HD_BAO: Hợp đồng bao 1-n. 1 HĐ tín dụng với 1 HĐ bảo hiểm bao và n lần giải ngân ứng với n lần bảo hiểm
        /// LINH_DONG: Hợp đồng linh động n-n. 1 HĐ tín dụng ứng với n bảo hiểm, với mỗi lần giải ngân là 1 hđ bảo hiểm
        /// </summary>
        public string LO_TYPE { get; set; }
        public string TELLER_CODE { get; set; }
        public string TELLER_NAME { get; set; }
        public string TELLER_EMAIL { get; set; }
        public string TELLER_PHONE { get; set; }
        public string TELLER_GENDER { get; set; }
        public string CURRENCY { get; set; }
        public double LO_TOTAL_AMOUNT { get; set; }
        public string LO_EFFECTIVE_DATE { get; set; }
        public string LO_EXPIRATION_DATE { get; set; }
        /// <summary>
        /// ngày ký kết
        /// </summary>
        public string LO_DATE { get; set; }
        public string LO_STATUS { get; set; }
        public string INTEREST_RATE { get; set; }
        public double DURATION { get; set; }
        public string LO_UNIT { get; set; }
        public List<ADDTIONAL_COVERAGE> ADD_COVERAGE { get; set; }
        public LO_DISBUR DISBUR { get; set; }
        public List<PaymentPeriod> PERIOD { get; set; }
    }
    /// <summary>
    /// danh sách lần giải ngân
    /// </summary>
    public class LO_DISBUR
    {
        public string BRANCH_CODE { get; set; }
        public string DISBUR_NUM { get; set; }
        public string DISBUR_DATE { get; set; }
        public double DISBUR_AMOUNT { get; set; }
        public double INSUR_TOTAL_AMOUNT { get; set; }

    }
    /// <summary>
    /// Danh sách điều khoản bổ sung, phạm vi mở rộng
    /// </summary>
    public class ADDTIONAL_COVERAGE
    {
        public string CODE { get; set; }
        public string NAME_DISPLAYED { get; set; }
        public string NAME_PRINT { get; set; }
        public string NAME { get; set; }
        public string FEES { get; set; }
        public string PARENT_CODE { get; set; }
    }
    */
    #endregion
    #region Insurance Vietjet

    public class FLIGHT
    {
        public string ID_COMMON { get; set; } = Guid.NewGuid().ToString();
        public FLIGHT_INFO INFO { get; set; }
        public List<FLIGHT_INSURED> INSURED { get; set; }
    }
    public class FLIGHT_INFO
    {
        /// <summary>
        /// HÃNG BAY: VIETJECT, VN,..
        /// </summary>
        public string ORG_CODE { get; set; }
        /// <summary>
        /// ID CHUYẾN BAY
        /// </summary>
        public string FLI_NO { get; set; }
        /// <summary>
        /// SỐ HIỆU CHUYẾN BAY
        /// </summary>
        public string FLI_NUM { get; set; }
        /// <summary>
        /// NƠI BẮT ĐẦU
        /// </summary>
        public string FLI_F { get; set; }
        /// <summary>
        /// NƠI ĐẾN
        /// </summary>
        public string FLI_T { get; set; }
        /// <summary>
        /// NƠI BẮT ĐẦU - ĐẾN
        /// </summary>
        public string FLI_FT { get; set; }
        /// <summary>
        /// NGÀY BAY
        /// </summary>
        public string FLI_DATE { get; set; }
        /// <summary>
        /// GIỜ BAY
        /// </summary>
        public string FLI_TIME { get; set; }
        ///// <summary>
        ///// Ngày giờ hiệu lực bảo hiểm
        ///// </summary>
        //public string EFFECTIVE_DATE { get; set; }
        ///// <summary>
        ///// Ngày giờ hết hiệu lực bảo hiểm
        ///// </summary>
        //public string EXPIRATION_DATE { get; set; }
    }

    public class FLIGHT_INSURED
    {
        public string CONTRACT_CODE { get; set; }
        public string DETAIL_CODE { get; set; }
        public string CUS_CODE { get; set; }
        public string TYPE { get; set; }
        public string NAME { get; set; }
        public string DOB { get; set; }
        public string GENDER { get; set; }
        public string IDCARD { get; set; }
        public string IDCARD_D { get; set; }
        public string IDCARD_P { get; set; }
        public string PROVINCE { get; set; }
        public string DISTRICT { get; set; }
        public string WARDS { get; set; }
        public string ADDRESS { get; set; }
        public string EMAIL { get; set; }
        public string PHONE { get; set; }
        public string FAX { get; set; }
        public string TAXCODE { get; set; }
        public string RELATIONSHIP { get; set; }
        /// <summary>
        /// Mã đặt chỗ
        /// </summary>
        public string BOOKING_ID { get; set; }
        /// <summary>
        /// Số ghế
        /// </summary>
        public string SEAT { get; set; }
        /// <summary>
        /// sản phẩm mua: option 1, 2
        /// </summary>
        public string PRODUCT_CODE { get; set; }
        /// <summary>
        /// Ngày giờ hiệu lực bảo hiểm
        /// </summary>
        public string EFFECTIVE_DATE { get; set; }
        /// <summary>
        /// Ngày giờ hết hiệu lực bảo hiểm
        /// </summary>
        public string EXPIRATION_DATE { get; set; }
    }
    #endregion

}
