﻿using GO.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class OrderTransInfo
    {
        public string order_trans_code { get; set; }
        public string order_code { get; set; }
        public string period_id { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string org_code { get; set; }
        public string payer { get; set; }
        public string payer_phone { get; set; }
        public string payer_email { get; set; }
        public string description { get; set; }
        public string description_en { get; set; }
        public string content_pay { get; set; }
        public string status { get; set; }
        public string struct_code { get; set; }
        public string org_seller { get; set; }
        public string product_code { get; set; }
        public string channel { get; set; }
        //public string content_transfer { get; set; }
    }

    public class PaymentMix
    {
        public double AMOUNT { get; set; }
        public string PAYMENT_TYPE { get; set; }
    }

    public class OrdersTransferApprove
    {
        public string ORDER_TRANS_CODE { get; set; }
        public double TOTAL_PAY { get; set; }
    }

    public class OrdersInfo
    {
        public string ORDER_CODE { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public double POINT { get; set; }
    }

    public class OrderDetailInfo
    {
        public string ORDER_CODE { get; set; }
        public string DETAIL_ID { get; set; }
        public double AMOUNT { get; set; }
        public double TOTAL_DISCOUNT { get; set; }
        public double VAT { get; set; }
        public double TOTAL_AMOUNT { get; set; }
        public double POINT { get; set; }
        public string PRODUCT_TYPE { get; set; }
        public string PRODUCT_CODE { get; set; }
    }
}
