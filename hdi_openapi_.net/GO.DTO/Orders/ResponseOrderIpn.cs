﻿using GO.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class ResponseOrderIpn : BaseResponse
    {
        public dataRequestOrderIpn Data { get; set; }
    }
    public class dataRequestOrderIpn
    {
        public string code { get; set; }//mã lỗi
        public string messageCode { get; set; }//thông tin lỗi
        public paymentResult paymentResult { get; set; }
        public transCard card { get; set; }
        public string checksum { get; set; }//thông tin lỗi
    }
    public class transCard
    {
        /// <summary>
        /// The brand name used to describe the card that is recognized and accepted globally.
        /// </summary>
        public string brand { get; set; }
        /// <summary>
        /// Month, as shown on the card.
        /// </summary>
        public string expiryMonth { get; set; }
        /// <summary>
        /// Year, as shown on the card.
        /// </summary>
        public string expiryYear { get; set; }
        /// <summary>
        /// The issuer of the card, if known.
        /// </summary>
        public string issuer { get; set; }
        /// <summary>
        /// Issuer code of local bank.
        /// </summary>
        public string issuerCode { get; set; }
        /// <summary>
        /// The cardholder's name as printed on the card
        /// </summary>
        public string nameOnCard { get; set; }
        /// <summary>
        /// Card issue date, shown on the card. DD/MM/YYYY
        /// </summary>
        public string issueDate { get; set; }
        /// <summary>
        /// Credit card number as printed on the card, masked.
        /// </summary>
        public string number { get; set; }
        /// <summary>
        /// The organization that owns a card brand and defines operating regulations for its use.
        /// </summary>
        public string scheme { get; set; }
        /// <summary>
        /// Only returns if enable3DSecure is set to true in the request
        /// true: card is enrolled with 3DS
        /// false: card is not enrolled with 3DS
        /// </summary>
        public string status3ds { get; set; }
        /// <summary>
        /// Customer identifier
        /// </summary>
        public string deviceId { get; set; }
        /// <summary>
        /// type card: CARD
        /// </summary>
        public string type { get; set; }
    }
    public class paymentResult
    {
        public string merchantId { get; set; }//mã của đơn vị gọi thanh toán
        public string masterCode { get; set; }//mã của đơn vị được gọi thanh toán
        public string payMethod { get; set; }//Phươn thức thanh toán: Tại quầy:TQ, Tại nhà TN, NH: Ngân hàng...
        public string paymentType { get; set; }//Hình thức thanh toán, QR, web, trích nợ, CARD

        public orderIpn order { get; set; }
    }
    public class orderIpn
    {
        /// <summary>
        /// trans id của cổng thanh toán
        /// </summary>
        public string transactionNo { get; set; }
        /// <summary>
        /// trans order của HDI
        /// </summary>
        public string orderCode { get; set; }
        public string amount { get; set; }
        public string creationTime { get; set; }
        public string currency { get; set; }
        public string reference { get; set; }
        public string totalAuthorizedAmount { get; set; }
        public string totalCapturedAmount { get; set; }
        public string totalRefundedAmount { get; set; }
        public string payDate { get; set; }
        public string bankCode { get; set; }
        public string mobile { get; set; }
        public string accountNo { get; set; }
        
        public List<gifCode> gifCode { get; set; }
    }
    public class gifCode
    {
        public string code { get; set; }
        public string discount { get; set; }
        public string discountUnit { get; set; }
        public string totalDiscount { get; set; }
    }
}
