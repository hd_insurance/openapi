﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Orders
{
    public class OrderPaymentAction
    {
        public string CHANNEL { get; set; }
        public string USERNAME { get; set; }
        public string ORDER_CODE { get; set; }
        public string CONTRACT_CODE { get; set; }
        public string ORG_SELLER { get; set; }
        public string STR_PERIOD { get; set; }
        public ORD_PAYINFO PAY_INFO { get; set; }
    }
}
