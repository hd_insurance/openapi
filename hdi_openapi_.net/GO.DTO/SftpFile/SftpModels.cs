﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.SftpFile
{
    public class SftpModels
    {
        public string org_code { get; set; }
        public string type { get; set; }
        public List<fileModels> files { get; set; }
    }

    public class fileModels
    {
        public string nameFile { get; set; }
        public string FileData { get; set; }
        public string typeFile { get; set; }
    }

    public class pathFile
    {
        public string nameFileOld { get; set; }
        public string pathFileNew { get; set; }
    }

    public class ServerSftp
    {
        public string host { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string is_split_folder { get; set; }
    }
}
