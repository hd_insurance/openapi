﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Database
{
    public class DatabaseModel
    {
        public string DB_CODE { get; set; }
        public string DB_NAME { get; set; }
    }
}
