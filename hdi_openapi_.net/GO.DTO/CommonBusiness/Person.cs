﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CommonBusiness
{
    public class Person
    {
        //public string ID { get; set; }
        /// <summary>
        /// Quốc tịch
        /// </summary>
        public string NATIONALITY { get; set; }
        /// <summary>
        /// Tên
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// Ngày sinh
        /// </summary>
        public string DOB { get; set; }
        /// <summary>
        /// Giới tính (M: nam, F: nữ)
        /// </summary>
        public string GENDER { get; set; }
        /// <summary>
        /// Mã tỉnh
        /// </summary>
        public string PROV { get; set; }
        /// <summary>
        /// Mã huyện
        /// </summary>
        public string DIST { get; set; }
        /// <summary>
        /// mã xã
        /// </summary>
        public string WARDS { get; set; }
        //public string ADD { get; set; }
        /// <summary>
        /// địa chỉ
        /// </summary>
        public string ADDRESS { get; set; }
        /// <summary>
        /// CMND
        /// </summary>
        public string IDCARD { get; set; }
        /// <summary>
        /// Ngày cấp
        /// </summary>
        public string IDCARD_D { get; set; }
        /// <summary>
        /// Nơi cấp
        /// </summary>
        public string IDCARD_P { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string EMAIL { get; set; }
        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string PHONE { get; set; }
    }
}
