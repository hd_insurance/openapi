﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CommonBusiness
{
    public class Customer : Person
    {
        /// <summary>
        /// PER_ID: mã định danh
        /// </summary>
        public string CUS_ID { get; set; }
        /// <summary>
        /// Mã nhân viên
        /// </summary>
        public string STAFF_CODE { get; set; }
        /// <summary>
        /// Mã nhân viên ánh xạ
        /// </summary>
        public string STAFF_REF { get; set; }
        /// <summary>
        /// Loại: KH. (CQ: Cơ quan, CN: Cá nhân)
        /// </summary>
        public string TYPE { get; set; }
        /// <summary>
        /// Mã fax
        /// </summary>
        public string FAX { get; set; }
        /// <summary>
        /// Mã số thuế
        /// </summary>
        public string TAXCODE { get; set; }
        /// <summary>
        /// Mã đối tác Khách hàng
        /// </summary>
        public string ORG_CUS { get; set; }
        public string ADDRESS_FORM { get; set; }
    }
}
