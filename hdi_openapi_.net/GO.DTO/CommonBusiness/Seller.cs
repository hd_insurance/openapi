﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.CommonBusiness
{
    public class Seller
    {
        public string SELLER_CODE { get; set; }
        public string SELLER_NAME { get; set; }
        public string SELLER_EMAIL { get; set; }
        public string SELLER_PHONE { get; set; }
        public string SELLER_GENDER { get; set; }
        /// <summary>
        /// Phòng ban, đơn vị của seller
        /// </summary>
        public string STRUCT_CODE { get; set; }
        public string ORG_CODE { get; set; }
        public string BRANCH_CODE { get; set; }
        /// <summary>
        /// Đối tác dẫn link
        /// </summary>
        public string ORG_TRAFFIC { get; set; }
        /// <summary>
        /// Link dẫn
        /// </summary>
        public string TRAFFIC_LINK { get; set; }
        /// <summary>
        /// Môi trường tạo đơn (WEB, APP, WEB_B2B, APP_B2B,...)
        /// </summary>
        public string ENVIROMENT { get; set; }
    }
}
