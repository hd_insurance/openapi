﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Common
{
    public class ResponseConfig
    {
        public string env_code { get;set;}
        public bool isSign { get; set; } = false;
        public string client_id { get;set;}
        public string secret { get;set;}
        public string partnerCode { get;set;}
    }
}
