﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Common
{
    public class RequestBevavios
    {
        /// <summary>
        /// ip client
        /// </summary>
        public string ClientIP { get; set; }
        /// <summary>
        /// action code
        /// </summary>
        public string ActionCode { get; set; }
        /// <summary>
        /// store name
        /// </summary>
        public string ActionProCode { get; set; }
        /// <summary>
        /// start time request client dd/MM/yyyy hh24:mi:ss
        /// </summary>
        public DateTime CreateExecute { get; set; }
        /// <summary>
        /// last time request client
        /// </summary>
        public DateTime LastExecute { get; set; }
        /// <summary>
        /// count requset
        /// </summary>
        public int CountExecute { get; set; }
        /// <summary>
        /// param store 
        /// </summary>
        public object Pram { get; set; }
        /// <summary>
        /// is cahce data store
        /// </summary>
        public bool IsCache { get; set; }
    }
}
