﻿using GO.DTO.SystemModels;
using System;

namespace GO.DTO.Common
{
    public class DataExecuteInfo
    {
        public bool IsSuccess { get; set; }
        public string ResultMess { get; set; }
        public bool IsCache { get; set; }
        public bool IsData { get; set; }
        public Object Data { get; set; }
        public string storePro { get; set; }
        public string keyCache { get; set; }
        public ActionApiDatabase dbCache { get; set; }
        public ActionApiDatabase dbExecute { get; set; }
        public bool IsSignature { get; set; }
        public string Path_Cer { get; set; }
        public string ClientID { get; set; }
        public string secret { get; set; }
        public string privateKey { get; set; }
    }
}
