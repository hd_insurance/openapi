﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Common
{
    public class KeyValModel
    {
        public string Key { get; set; }
        public string Val { get; set; }
    }
}
