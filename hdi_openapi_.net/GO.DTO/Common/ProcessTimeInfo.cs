﻿using System;

namespace GO.DTO.Common
{
    public class ProcessTimeInfo
    {
        public DateTime RequestTime { get; set; }
        public DateTime ExecuteTime { get; set; }
        public DateTime ResponseTime { get; set; }
    }
}
