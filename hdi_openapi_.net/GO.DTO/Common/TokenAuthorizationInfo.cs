﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Common
{
    public class TokenAuthorizationInfo
    {
        public bool Sucess { get; set; }
        public string Signature { get; set; }
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
    }
}
