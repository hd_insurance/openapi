﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;

namespace GO.DTO.InsurRegister
{
    public class StaffRegister : Customer
    {
        public string CHANNEL { get; set; }
        public string ORG_CODE { get; set; }
        public string STAFF_CODE { get; set; }
        public string CUS_CODE { get; set; }
        public string STAFF_NAME { get; set; }
        public string BANK_ACCOUNT_NUM { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string PAID_BY_SAL { get; set; }
        public string STATUS { get; set; }
        public string ACTIVATION_CODE { get; set; }
        public string FULL_FEES { get; set; }
        public string IS_REGISTERED { get; set; }
        public string CAMPAIGN_CODE { get; set; }
        public string STATUS_ORD { get; set; }
        public string CATEGORY { get; set; }
        public string EFF_DATE { get; set; }
        public string EXP_DATE { get; set; }
        public string ADDRESS { get; set; }
        public string PRODUCT_CODE { get; set; }
    }
}
