﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.DTO.CommonBusiness;

namespace GO.DTO.InsurRegister
{
    public class InsuredRegister : Customer
    {
        public string ORG_CODE { get; set; }
        public string STAFF_CODE { get; set; }
        public string CUS_CODE { get; set; }
        public string CUS_NAME { get; set; }
        public string RELATIONSHIP { get; set; }
        public double AMOUNT { get; set; }
        public string PACK_CODE { get; set; }
        public string EXCLUDE_COND1 { get; set; }
        public string EXCLUDE_COND2 { get; set; }
        public string IS_CONFIRM { get; set; }
        public string MONTH_ORDER { get; set; }
        public string YEAR_ORDER { get; set; }
        //public double FULL_FEES { get; set; }
        public string CAMPAIGN_CODE { get; set; }
        public string PRODUCT_CODE { get; set; }
        public string EFF_DATE { get; set; }
        public string EXP_DATE { get; set; }
        public string ADDRESS { get; set; }
    }
}
