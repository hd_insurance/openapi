﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.UseSystem
{
    public class SysUserSSO
    {
        public string USER_CODE { get; set; }
        public string USER_NAME { get; set; }
        public string PASSWORD { get; set; }
        public string ORG_CODE { get; set; }
        public string STRUCT_CODE { get; set; }
        public string FULL_NAME { get; set; }
        public string USER_TYPE { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRATION_DATE { get; set; }
        public string LAST_LOGIN { get; set; }
    }
}
