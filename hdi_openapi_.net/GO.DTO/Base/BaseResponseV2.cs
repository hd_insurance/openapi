﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Base
{
    public class BaseResponseV2 : BaseResponse
    {
        public string LogId { get; set; }
    }
}
