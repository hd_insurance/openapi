﻿namespace GO.DTO.Base
{
    public class BaseResponse
    {
        public bool Success { get; set; }
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
        public object Data { get; set; }
        public string Signature { get; set; }
    }
}
