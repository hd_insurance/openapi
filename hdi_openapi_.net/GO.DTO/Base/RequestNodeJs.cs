﻿using GO.DTO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Base
{
    public class RequestNodeJs
    {
        public ClientNodeInfo Device { get; set; }
        public ActionInfo Action { get; set; }
        public object Data { get; set; }
        public string Signature { get; set; }
    }

    public class ClientNodeInfo
    {
        public string DeviceId { get; set; } = "";
        public string DeviceCode { get; set; } = "";
        /// <summary>
        /// Dùng cho hoàng
        /// </summary>
        public string Device_name { get; set; } = "";
        public string IpPrivate { get; set; } = "";
        public string IpPublic { get; set; } = "";
        public string X { get; set; } = "";
        public string Y { get; set; } = "";
        public string province { get; set; } = "";
        public string district { get; set; } = "";
        public string wards { get; set; } = "";
        public string address { get; set; } = "";
        public string environment { get; set; } = "";
        public string browser { get; set; } = "";
        public string DeviceEnvironment { get; set; } = "";
    }
}
