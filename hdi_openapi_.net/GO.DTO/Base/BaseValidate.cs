﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Base
{
    public class BaseValidate
    {
        public bool Success { get; set; } = true;
        public string Error { get; set; }
        public string ErrorMessage { get; set; }
    }
}
