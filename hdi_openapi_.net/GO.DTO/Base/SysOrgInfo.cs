﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.DTO.Base
{
    public class SysOrgInfo
    {
        public string org_code { get; set; }
        public string parent_code { get; set; }
    }
}
