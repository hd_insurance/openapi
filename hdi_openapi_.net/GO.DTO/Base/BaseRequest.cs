﻿using GO.DTO.Common;
using System.ComponentModel;

namespace GO.DTO.Base
{
    public class BaseRequest
    {
        /// <summary>
        /// Device: Thông tin thiết bị
        /// </summary>
        [Description("Thông tin thiết bị")]
        public ClientInfo Device { get; set; }
        public ActionInfo Action { get; set; }
        public object Data { get; set; }
        public string Signature { get; set; }
        public ProcessTimeInfo Process { get; set; }
    }
}
