﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.CONSTANTS
{
    public class goConstantsError
    {
        #region Variable Error Method
        /// <summary>
        /// ERROR_2001: DDOS
        /// </summary>
        public string ERROR_2001 = "requests_exceeded ";
        /// <summary>
        /// ERROR_2002: Models not valid
        /// </summary>
        public string ERROR_2002 = "model_not_valid";
        /// <summary>
        /// ERROR_2003: please provide token
        /// </summary>
        public string ERROR_2003 = "please_provide_token";
        /// <summary>
        /// ERROR_2004: invalid token
        /// </summary>
        public string ERROR_2004 = "invalid_token";
        /// <summary>
        /// ERROR_2005: request not allow
        /// </summary>
        public string ERROR_2005 = "request_not_allow";
        /// <summary>
        /// ERROR_2006: bad request
        /// </summary>
        public string ERROR_2006 = "bad_request";
        /// <summary>
        /// ERROR_2007: Unregistered partner
        /// </summary>
        public string ERROR_2007 = "unregistered_partner";
        /// <summary>
        /// ERROR_2008: Unregistered action
        /// </summary>
        public string ERROR_2008 = "unregistered_action";
        /// <summary>
        /// ERROR_2009: exeception response action
        /// </summary>
        public string ERROR_2009 = "Hệ thống phát hiện có lỗi từ yêu cầu của bạn, vui lòng thử lại hoặc liên hệ kỹ thuật để được trợ giúp. Xin cảm ơn !";
        /// <summary>
        /// ERROR_2010: Tài khoản hoặc mật khẩu không đúng !
        /// </summary>
        public string ERROR_2010 = "Tài khoản hoặc mật khẩu không đúng !";
        /// <summary>
        /// ERROR_2011: Signature error
        /// </summary>
        public string ERROR_2011 = "signature_error";
        /// <summary>
        /// ERROR_2012: invalid signature
        /// </summary>
        public string ERROR_2012 = "invalid_signature";
        /// <summary>
        /// ERROR_2013: partner not authorization
        /// </summary>
        public string ERROR_2013 = "partner_not_authorization";
        /// <summary>
        /// ERROR_2014: invalid domain
        /// </summary>
        public string ERROR_2014 = "invalid_domain";
        /// <summary>
        /// ERROR_2012: invalid ip
        /// </summary>
        public string ERROR_2015 = "invalid_ip";
        /// <summary>
        /// ERROR_2016: blacklist ip
        /// </summary>
        public string ERROR_2016 = "blacklist_ip";
        /// <summary>
        /// Sai mã action lấy log audit
        /// </summary>
        public string ERROR_2017 = "action audit logs get error";
        #endregion

        #region Variables Error Method Service
        public string ERROR_3001 = "Mã ActionCode không đúng vui lòng thử lại, hoặc liên hệ để được trợ giúp !";
        public string ERROR_3002 = "Yêu cầu chưa đúng cấu trúc vui lòng thử lại, hoặc liên hệ để được trợ giúp !";
        public string ERROR_3003 = "Có lỗi xảy ra vui lòng thử lại, hoặc liên hệ để được trợ giúp !";
        public string ERROR_3004 = "Không tìm thấy dữ liệu cấu hình mẫu !";

        public string ERROR_3005 = "Không khởi tạo được file ký số !";
        public string ERROR_3006 = "Không tìm thấy dữ liệu để chuyển đổi !";
        public string ERROR_3007 = "Không upload được file lên server !";
        public string ERROR_3008 = "Có lỗi tạo QR, vui lòng thử lại hoặc liên hệ để được trợ giúp !";
        #endregion

        #region IPN
        public string ERROR_4001 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_4002 = "Chuỗi xác thực không đúng, vui lòng thử lại !";
        public string ERROR_4003 = "Có lỗi, vui lòng thử lại !";
        public string ERROR_4004 = "Có lỗi xảy ra, vui lòng thử lại !";
        public string ERROR_P01 = "Chưa phân quyền thanh toán cho đơn vị !";
        public string ERROR_P02 = "Dữ liệu thanh toán không đúng đặc tả !";
        public string ERROR_P03 = "Không khởi tạo được giao dịch !";
        public string ERROR_P04 = "Đơn hàng đã được thanh toán !";
        public string ERROR_P05 = "Không tìm thấy giao dịch thanh toán !";
        public string ERROR_P06 = "Không tìm thấy đơn vị yêu cầu thanh toán";

        #endregion

        #region SFTP
        public string ERROR_5001 = "Có lỗi xảy ra, vui lòng thử lại !";
        public string ERROR_5002 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_5003 = "Đối tác chưa được phân quyền !";
        #endregion

        #region Orders
        public string ERROR_6001 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        //public string ERROR_6002 = "unregistered_partner";
        public string ERROR_6003 = "Yêu cầu nghiệp vụ bảo hiểm vay vốn không hợp lệ, vui lòng thử lại !";
        public string ERROR_6004 = "Yêu cầu nghiệp vụ bảo hiểm vay vốn không đúng cấu trúc, vui lòng thử lại !";
        public string ERROR_6005 = "Đối tác chưa được phân quyền yêu cầu, vui lòng thử lại !";
        public string ERROR_6006 = "Không tìm thấy hợp đồng, vui lòng thử lại !";
        public string ERROR_6007 = "Không tìm thấy chương trình, vui lòng thử lại !";
        public string ERROR_6008 = "File dữ liệu không chuẩn, vui lòng thử lại !";
        public string ERROR_6009 = "Bạn chưa chọn hình thức thanh toán, vui lòng thử lại !";
        public string ERROR_6010 = "Số tiền của sản phẩm và số tiền gửi qua khác nhau !";
        #endregion

        #region Loan
        public string ERROR_LO_01 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_LO_02 = "Có lỗi xảy ra, vui lòng thử lại !";
        public string ERROR_LO_03 = "Lưu thông tin thất bại, vui lòng thử lại !";

        // 18/10/2021: bổ sung mã lỗi mới
        public string ERROR_LO_04 = "Bạn không được bỏ trống: SELLER_INFO, BRANCH_INFO, SELLER_CODE, BANK_CODE, ARR_PARENT !";
        public string ERROR_LO_05 = "Bạn không được bỏ trống: LO_INFO, NAME, LO_CONTRACT, LO_TYPE, LO_DATE, LO_TOTAL !";
        public string ERROR_LO_06 = "Bạn không được bỏ trống: USER_NAME, CHANNEL, PRODUCT_CODE !";

        public string ERROR_LO_07 = "Phiên làm việc của bạn đã hết, vui lòng đăng nhập lại !";
        public string ERROR_LO_08 = "Bạn không được bỏ trống LO_CONTRACT, DISBUR_CODE, BANK_CODE, BRANCH_CODE !";

        public string ERROR_LO_09 = "Bạn không được bỏ trống: DISBUR, DISBUR_CODE, DISBUR_NUM, DISBUR_DATE, DISBUR_AMOUNT !";
        public string ERROR_LO_10 = "Bạn không được bỏ trống ARR_PARENT: CODE, NAME, PARENT_CODE !";
        public string ERROR_LO_11 = "Bạn không được bỏ trống: SELLER_NAME, SELLER_EMAIL, SELLER_PHONE !";
        public string ERROR_LO_12 = "Không lớn hơn ngày hiện tại: ";
        #endregion

        #region Vietjet
        public string ERROR_VJC_01 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_VJC_02 = "Có lỗi xảy ra, vui lòng thử lại !";
        #endregion

        #region Voucher
        public string ERROR_VOUCHER_01 = "Có lỗi xảy ra, vui lòng thử lại !";
        public string ERROR_VOUCHER_02 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_VOUCHER_03 = "Mã OTP của bạn không đúng hoặc đã hết hạn, vui lòng thử lại !";
        public string ERROR_VOUCHER_04 = "Dữ liệu không hợp lệ, vui lòng thử lại !";
        public string ERROR_VOUCHER_05 = "Độ tuổi tham gia không phù hợp !";
        #endregion

        #region OTP
        public string ERROR_OTP_01 = "Yêu cầu gửi OTP của bạn bị từ chối do gửi quá nhiều lần. Vui lòng thử lại sau 60 phút !";
        #endregion

        #region Variable Error Model

        #region Request
        public string NULL_REQUEST = "Request cannot null.";
        public string NULL_DEVICE = "Device cannot null.";
        public string NULL_DEVICE_ID = "Device id cannot null.";
        public string NULL_DEVICE_CODE = "Device code cannot null.";
        public string NULL_DEVICE_IP_PRIVATE = "Device ip private cannot null.";
        public string NULL_ACTION = "Action cannot null.";
        public string NULL_ACTION_CODE = "Action code cannot null.";
        public string NULL_ACTION_USER_NAME = "Username cannot null.";
        public string NULL_ACTION_PARTNER_CODE = "Partner code cannot null.";
        public string NULL_ACTION_SECRET = "Secret cannot null.";
        public string NOT_IN_ENVIRONMENT_CODE = "Environment not in WEB, APP.";
        #endregion

        //public string EMPTY_TOKEN = "Token cannot be empty.";
        #endregion

        #region Dynamic Fees
        public string ERROR_FEES_01 = "Yêu cầu không hợp lệ, vui lòng thử lại !";
        public string ERROR_FEES_02 = "Sản phẩm chưa được thêm tính phí, vui lòng thử lại hoặc liên hệ kỹ thuật để được trợ giúp !";
        #endregion

        #region Ecommerce
        public string ERROR_ECM_01 = "Không tìm thấy thông tin, vui lòng thử lại !";
        #endregion

        #region Contructor
        private static readonly Lazy<goConstantsError> InitInstance = new Lazy<goConstantsError>(() => new goConstantsError());
        public static goConstantsError Instance => InitInstance.Value;
        #endregion
    }
}
