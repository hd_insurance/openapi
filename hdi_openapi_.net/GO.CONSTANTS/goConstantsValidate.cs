﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.CONSTANTS
{
    public class goConstantsValidate
    {
        #region Variable
        public static string ERR_CHK_01 = "Danh mục phí chưa được khai báo !";
        public static string ERR_CHK_02 = "Nhóm xe chưa được khai báo !";
        public static string ERR_CHK_03 = "Số tiền và mã truyền lên khác nhau !";
        public static string ERR_CHK_04 = "Năm sản xuất lơn hơn năm bảo hiểm !";
        public static string ERR_CHK_05 = "Năm sử dụng khác mã truyền lên !";
        public static string ERR_CHK_06 = "Số chỗ khác mã truyền lên !";
        public static string ERR_CHK_07 = "Mã mục đích kinh doanh không đúng !";
        public static string ERR_CHK_08 = "Trọng tải khác mã truyền lên !";
        public static string ERR_CHK_09 = "Giờ phút hiệu lực khác giờ phút hết hiệu lực !";
        public static string ERR_CHK_10 = "Thiếu file giám định !";
        public static string ERR_CHK_11 = "Thiếu file chứng từ hóa đơn với xe không biển số !";
        public static string ERR_CHK_12 = "Thiếu file Giấy đăng ký xe(Mặt sau) !";
        public static string ERR_CHK_13 = "Thiếu file Sổ đăng kiểm xe(mặt trong) !";
        public static string ERR_CHK_14 = "Thiếu file Góc đầu phải và biển !";
        public static string ERR_CHK_15 = "Thiếu file Góc đầu trái và biển !";
        public static string ERR_CHK_16 = "Thiếu file Góc đuôi phải và biển !";
        public static string ERR_CHK_17 = "Thiếu file Góc đuôi trái và biển !";
        public static string ERR_CHK_18 = "Thiếu Biển số xe !";
        public static string ERR_CHK_19 = "Xe không được mua VCX !";
        public static string ERR_CHK_20 = "Xe không được mua TNDS TN !";
        public static string ERR_CHK_21 = "Xe không được mua TNDS TN (Vượt quá mức) !";
        public static string ERR_CHK_22 = "Xe không được mua TNDS TN (Thân thể người t3)  !";
        public static string ERR_CHK_23 = "Xe không được mua TNDS TN (Hàng hóa người t3)  !";
        public static string ERR_CHK_24 = "Xe không được mua TNDS TN (Hành khách)  !";
        public static string ERR_CHK_25 = "Xe không được mua TNDS TN (LPX, NNTX)  !";
        public static string ERR_CHK_26 = "Xe không được mua TNDS TN (Hàng hóa)  !";
        public static string ERR_CHK_27 = "Xe không được mua TNDS BB  !";

        public static string ERR_CHK_28 = "Không tìm thấy sản phẩm đóng gói yêu cầu hoặc sản phẩm chưa được phân quyền bán !";

        public static string ERR_CHK_29 = "Giá trị xây dựng căn nhà vượt quá cho phép !";
        public static string ERR_CHK_30 = "Giá trị tài sản bên trong vượt quá cho phép !";
        public static string ERR_CHK_HOU_03 = "Mã năm sử dụng sai !";
        public static string ERR_CHK_HOU_04 = "Mã năm sử dụng sai hoặc năm sử dụng vượt quá cho phép bán !";

        //Fees input validate 
        public static string ERR_CHK_31 = "Lỗi xác minh phí, vui lòng kiểm tra lại !";
        public static string ERR_CHK_32 = "Lỗi xác minh phí vật chất xe, vui lòng kiểm tra lại !";
        public static string ERR_CHK_33 = "Lỗi phí PHYSICAL_DAMAGE.FEES, vui lòng kiểm tra lại !";
        public static string ERR_CHK_34 = "Lỗi phí PHYSICAL_DAMAGE.AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_35 = "Lỗi phí PHYSICAL_DAMAGE.TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_36 = "Lỗi phí PHYSICAL_DAMAGE.VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_37 = "Lỗi phí PHYSICAL_DAMAGE.TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_38 = "Lỗi xác minh phí gốc vật chất xe, vui lòng kiểm tra lại !";
        public static string ERR_CHK_39 = "Lỗi phí PHYSICAL_DAMAGE.PHYSICAL_FEES, vui lòng kiểm tra lại !";
        public static string ERR_CHK_40 = "Lỗi phí PHYSICAL_DAMAGE.PHYSICAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_41 = "Lỗi phí PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_42 = "Lỗi phí PHYSICAL_DAMAGE.PHYSICAL_VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_43 = "Lỗi phí PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_44 = "Lỗi phí bổ sung vật chất xe, vui lòng kiểm tra lại !";
        public static string ERR_CHK_45 = "Lỗi phí bổ PHYSICAL_DAMAGE.TOTAL_ADDITIONAL, vui lòng kiểm tra lại !";
        public static string ERR_CHK_46 = "Lỗi phí bổ PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES, vui lòng kiểm tra lại !";
        public static string ERR_CHK_47 = "Lỗi phí bổ PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_48 = "Lỗi phí bổ PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_49 = "Lỗi phí bổ PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_50 = "Lỗi phí bổ PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_51 = "Lỗi xác minh phí tnds bb, vui lòng kiểm tra lại !";
        public static string ERR_CHK_52 = "Lỗi phí bổ COMPULSORY_CIVIL.FEES, vui lòng kiểm tra lại !";
        public static string ERR_CHK_53 = "Lỗi phí bổ COMPULSORY_CIVIL.AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_54 = "Lỗi phí bổ COMPULSORY_CIVIL.VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_55 = "Lỗi phí bổ COMPULSORY_CIVIL.TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_56 = "Lỗi phí bổ COMPULSORY_CIVIL.TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_57 = "Lỗi xác minh phí tnds tn, vui lòng kiểm tra lại !";
        public static string ERR_CHK_58 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES, vui lòng kiểm tra lại !";
        public static string ERR_CHK_59 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_60 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_61 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_62 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_63 = "Lỗi xác minh phí tnds tn(thân thể t3), vui lòng kiểm tra lại !";
        public static string ERR_CHK_64 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_3RD_INSUR, vui lòng kiểm tra lại !";
        public static string ERR_CHK_65 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR, vui lòng kiểm tra lại !";
        public static string ERR_CHK_66 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR, vui lòng kiểm tra lại !";
        public static string ERR_CHK_67 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_3RD_INSUR, vui lòng kiểm tra lại !";
        public static string ERR_CHK_68 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_3RD_INSUR, vui lòng kiểm tra lại !";
        public static string ERR_CHK_69 = "Lỗi xác minh phí tnds tn(tài sản t3), vui lòng kiểm tra lại !";
        public static string ERR_CHK_70 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_3RD_ASSETS, vui lòng kiểm tra lại !";
        public static string ERR_CHK_71 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS, vui lòng kiểm tra lại !";
        public static string ERR_CHK_72 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS, vui lòng kiểm tra lại !";
        public static string ERR_CHK_73 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_3RD_ASSETS, vui lòng kiểm tra lại !";
        public static string ERR_CHK_74 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS, vui lòng kiểm tra lại !";
        public static string ERR_CHK_75 = "Lỗi xác minh phí tnds tn(hành khách), vui lòng kiểm tra lại !";
        public static string ERR_CHK_76 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_PASSENGER, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_77 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_PASSENGER, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_78 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_79 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_PASSENGER, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_80 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_PASSENGER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_81 = "Lỗi xác minh phí tnds tn(vqm), vui lòng kiểm tra lại !";
        public static string ERR_CHK_82 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_83 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_84 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_85 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD, vui lòng kiểm tra lại !"; 
        public static string ERR_CHK_86 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD, vui lòng kiểm tra lại !";
        public static string ERR_CHK_87 = "Lỗi xác minh phí tnds tn(lpx,nntx), vui lòng kiểm tra lại !";
        public static string ERR_CHK_88 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_DRIVER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_89 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_DRIVER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_90 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_91 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_DRIVER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_92 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DRIVER, vui lòng kiểm tra lại !";
        public static string ERR_CHK_93 = "Lỗi xác minh phí tnds tn(hh), vui lòng kiểm tra lại !";
        public static string ERR_CHK_94 = "Lỗi phí bổ VOLUNTARY_CIVIL.FEES_CARGO, vui lòng kiểm tra lại !";
        public static string ERR_CHK_95 = "Lỗi phí bổ VOLUNTARY_CIVIL.AMOUNT_CARGO, vui lòng kiểm tra lại !";
        public static string ERR_CHK_96 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO, vui lòng kiểm tra lại !";
        public static string ERR_CHK_97 = "Lỗi phí bổ VOLUNTARY_CIVIL.VAT_CARGO, vui lòng kiểm tra lại !";
        public static string ERR_CHK_98 = "Lỗi phí bổ VOLUNTARY_CIVIL.TOTAL_CARGO, vui lòng kiểm tra lại !";
        public static string ERR_CHK_99 = "Lỗi phí bổ AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_100 = "Lỗi phí bổ TOTAL_DISCOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_101 = "Lỗi phí bổ TOTAL_VAT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_102 = "Lỗi phí bổ TOTAL_AMOUNT, vui lòng kiểm tra lại !";
        public static string ERR_CHK_103 = "Lỗi phí bổ TOTAL_ADDITIONAL, vui lòng kiểm tra lại !";
        #endregion

        #region Contructor
        private static readonly Lazy<goConstantsValidate> InitInstance = new Lazy<goConstantsValidate>(() => new goConstantsValidate());
        public static goConstantsValidate Instance => InitInstance.Value;
        #endregion
    }
}
