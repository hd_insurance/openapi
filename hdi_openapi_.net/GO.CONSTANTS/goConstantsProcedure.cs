﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.CONSTANTS
{
    public class goConstantsProcedure
    {
        #region Variable Procedure
        public static string ActionApis_Store = "DEVOPS.SYS_ACTION_API_GETALL";
        public static string ActionApiBehavios_Store = "DEVOPS.SYS_BEHAVIOS_CACHE_GETALL";
        public static string ActionApiDatabase_Store = "DEVOPS.SYS_API_DATABASE_GETALL";
        public static string User_Store = "SYS_USER_GETALL";
        //public static string AuditLog_Store = "SYS_ACTION_API_GETALL";
        public static string PartnerConfig_Store = "DEVOPS.SYS_PARTNER_CONFIG_GETALL";
        //public static string ApiWhiteList_Store = "SYS_API_WHITE_LIST_GETALL";
        public static string PartnerGroupApi_Store = "SYS_PARTNER_GROUP_API_GETALL";
        public static string Domain_Store = "SYS_DOMAIN_GETALL";
        public static string PartnerWhiteList_Store = "sys_partner_whitelist_GETALL";
        public static string Webconfig_Store = "GET_WEBCONFIG_API";
        public static string BlackList_Store = "SYS_BLACK_LIST_GET_ALL";

        #endregion

        #region Variables Action IPN
        public static string HDBank_Ipn = "HDI_PAY_01";
        #endregion

        #region Variables Action Orders
        //public static string Orders_SaveAll = "API_ORDERS_SAVE";
        //public static string Orders_Save = "API_ORDERS_SAVE";
        //public static string OrdersDetail_Save = "API_ORDERS_DETAIL_SAVE";
        //public static string Orders_SaveAll = "API_ORDERS_SAVE";

        #endregion

        #region Payment
        //lay payment config
        public static string Pay_assign_config = "API5ZTMMXU";
        public static string Pay_insert_trans = "APILKR5A1I";
        public static string Pay_get_info_getway = "API8Q9O7YC";
        //lấy all thông tin đối tác
        public static string Pay_All_Config = "APICU8M515";
        //lấy thông tin của các đơn vị kết nối thanh toán
        public static string Pay_Get_Config = "HDB_QR";
        //lấy thông tin url cần ridrect sang ứng với mã đối tác gọi thanh toán
        public static string Pay_Get_ORG_Config = "HDB_ORG_CALL";
        public static string Pay_create_transaction = "HDI_CREATE_PAY";
        //lấy thông tin giao dịch thanh toán bằng id_trans
        public static string Pay_get_transaction = "HDI_GET_TRANS";
        //lấy thông tin giấy chứng nhận bằng hợp đồng số id
        public static string Pay_get_contract = "HDI_GET_CONTRACT";
        //lấy thông tin giấy chứng nhận bằng hợp đồng số id
        public static string Pay_get_gui = "ORDER_GUI";
        //lấy thông tin giao dịch băng gui
        public static string Pay_get_trans_byGui = "HDI_ORDER_TRANS";

        public static string Pay_Update_Trans = "HDI_UPDATE_TRANS";
        //cập nhật chuyển khoản
        public static string Pay_TransFer = "IPN_TRANSFER";
        //insert thông tin chuyển khoản đẩy file
        public static string Pay_TransFer_info = "IPN_TRANSFER_INFO";
        //insert dữ liệu khi gọi IPN về cho đối tác bị lỗi
        public static string Pay_Insert_IPNError = "INSERT_PM_IPN_ERROR";

        public static string Insu_get_infor = "HDI_SEARCH_INSUR";

        public static string Pay_Insert_DetailTrans = "API1WQHCRD";

        public static string Pay_Update_DetailTrans = "APIFE9DNY3";

        public static string Pay_Update_QR = "APIJQREJ7U";
        #endregion

        #region CallBack
        public static string Get_Callback_Data = "APIK5M91PZ";
        public static string Get_TransID_ByTranscode = "APIH5KZGUS";
        public static string Update_Trans_Status = "APIH6TM5S2";
        #endregion

        #region SIGN
        public static string Sign_Insert_Batch = "HDI_INSERT_BATCH";
        public static string Sign_Insert_File = "HDI_INSERT_FILE";
        public static string Sign_Edit_Batch = "HDI_UPDATE_BATCH";
        public static string Sign_Edit_File = "HDI_UPDATE_FILE";
        public static string Sign_Get_Batch = "HDI_GET_SIGN";
		public static string Sign_Get_Config = "HDI_GET_SIGN_CONFIG";
        public static string Sign_Sign_File = "HDI_SIGN_FILE";
        public static string sign_InsBatch_Json = "APIYCFKBB7";
        public static string sign_dataExt = "APIJPTTSML";
        public static string sign_veryfi = "APIBJTXHNE";
        #endregion

        #region c_media
        //LẤY DỮ LIỆU VỀ TEMPLACE 
        public static string ser_get_temp = "HDI_CER_FILE";
        public static string Get_Info_VJ = "HDI_INFOR_VJ";
        public static string MD_Insert_Campaign = "HDI_CAMPAIGN";
        public static string MD_Update_Campaign = "HDI_UPDATE_CAM";
        public static string MD_Update_Cam_Email = "HDI_UPDATE_MAIL";
        public static string MD_Update_Cam_SMS = "HDI_UPDATE_SMS";
        public static string MD_Get_Temp_Value = "HDI_GET_TEMP_VALUE";
        public static string MD_Log_Email = "APIBP54GKA";
        public static string MD_LOG_MAIL_JSON = "API7JNN28H";
        public static string MD_TEMPLATE_SMS = "APIVHSSTN8";
        public static string MD_LOG_SMS = "APIARHB2VX";
        public static string GET_ALL_TEMP = "APIGZM17ZJ";
        public static string GET_ID_TEMP = "API9M2U8RA";
        public static string INSERT_TEMPLATE = "APIZ4S9514";

        public static string TEMP_CODE_TBAO_BVIET = "BV_TBAO";
        public static string TEMP_CODE_CODL_BVIET = "BV_TBAO_CO";
        public static string TEMP_CODE_KODL_BVIET = "BV_TBAO_KOCO";

        #endregion

        #region Order
        public static string SaveOrderObject = "API_SAVE_O_ORDER";
        public static string SaveOrderObject_Preview = "API5QCM9DL";
        public static string CREATE_ORDER_TRANS = "HDI_CREATE_ORDER_TRANS";
        public static string ORDER_IPN = "ORDER_IPN";
        public static string ORDER_CUS_INFO = "GET_TEMP_CUSINFO";
        /// <summary>
        /// Cập nhật trạng thái tạo đơn bảo hiểm
        /// </summary>
        public static string UDP_STAFF_ORDER_CREATE ="APIF0TEZR5";
        public static string CREAT_ORDER_TRANS_V2 = "APIHENXZAB";

        public static string GET_DATA_NOTIFI = "APIC7OHBJK";
        public static string INSERT_DATA_NOTIFI = "APIIEJ7AFA";
        public static string GET_NOTIFI_CONFIG = "APIOL2GXMH";
        public static string INSERT_NOTIFI_LOG = "API4E9SQCV";
        

        public static string GET_PRODUCT_RECORD = "API73BHAIB";
        public static string STREAM_UPLOAD = "UPLOAD_GIAMDINH";
        public static string STREAM_COMMIT = "STREAM_UPLOAD";
        public static string INSERT_CUS_INFO = "API2H9TOHX";
        public static string UPDATE_CUS_INFO = "API5YFYS2Z";
        public static string CREATE_SHORT_LINK = "APIIJZ26FY";
        public static string GET_MAR_BY_ORG = "APIWPZ36Z3";
        public static string GET_MAR_BY_ORG_V2 = "API2FY2R0B";
        public static string CANCEL_SKU_ORG = "APIF285KFB";
        public static string CANCEL_SKU_TOOL = "API5SBR5H1";
        public static string GET_SKU_CONTRACT = "APIWOEYLTA";
        public static string GET_SKU_ORG = "APINWO1DXN";
        public static string INS_DETAIL_CER = "APITC63Y9N";

        public static string GET_ORDER_BY_TRANSPARTNER = "APILDFQ0FR";

        /// <summary>
        /// Yêu cầu thanh toán -- thu hộ, CTT, CK
        /// </summary>
        public static string ACTION_ORDER_PAYMENT = "APIAHBD9ZU"; // data trả ra của hàm CREAT_ORDER_TRANS_V2
        /// <summary>
        /// Kiểm tra người mua đã mua BH và còn HL không
        /// </summary>
        public static string INSUR_EXISTS = "APIG05WJJW";
        /// <summary>
        /// Thêm data vào order sku
        /// </summary>
        public static string INS_ORDER_SKU = "API4YG4T45";
        public static string GET_LANDING_MASTER = "APIIKCA6QA";
        #endregion

        #region Otp
        public static string INS_SEND_OTP = "APIYAGYPGB";
        public static string SEND_SMS_HDI = "HDI_SMS_01";

        #endregion

        #region Voucher
        public static string UPD_VOUCHER_USED = "APIP1C4KRA";
        #endregion

        #region ATTD
        public static string UPD_DISBUR_STATUS = "APIDGQ5C3L";

        public static string CHANGE_USER_LO = "APINA9ZTEN";
        public static string SEAR_STATUS_BY_LO = "APIE9ZMV8C";
        #endregion

        #region Insurance
        public static string UPD_AFTER_SIGN = "APIE3A34XI";
        public static string CREATE_VEHICLE = "API4RMM4KA";
        /// <summary>
        /// Lấy danh sách đơn BH: pkg_insur_selling.get_insur_by_seach
        /// </summary>
        public static string SEAR_INSUR = "APIL7F9SVG";
        #endregion

        #region Dynamic Fees
        public static string GET_FEES_INIT_CACHE = "APIJCM85SH";
        #endregion

        #region Import
        /// <summary>
        /// Lấy thông tin chương trình pkg_define_bussiness.get_program_by_id
        /// </summary>
        public static string IMP_GET_PROGRAM = "APIFQJLJL4";
        public static string IMP_UDP_PROGRAM = "APIX0M98GA";
        #endregion

        #region VietJet
        //Lay Data tao Xls
        //
        public static string BV_EXPORT_DATA = "API5VTJXSH";
        public static string BV_SYN_DATA = "APIA8PSGIT";

        #endregion

        #region Vikki Notify
        //Lay Data tao Xls
        //
        public static string VIKKI_EXPORT_DATA = "API2FSX1Z6";

        #endregion

        #region Import Notify
        //Lay Data tao Xls
        //
        public static string IMPORT_EXPORT_DATA = "API2FSX1Z6";

        #endregion

        #region FEE
        //Lay Data tao Xls
        //
        public static string HEALTH_FEE = "APIXE6WGR9";        

        #endregion

        #region Search Landing
        public static string SEARCH_LANDING = "API8ORT2VM";
        #endregion

        #region Contructor
        private static readonly Lazy<goConstantsProcedure> InitInstance = new Lazy<goConstantsProcedure>(() => new goConstantsProcedure());
        public static goConstantsProcedure Instance => InitInstance.Value;
        #endregion
    }
}
