﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.CONSTANTS
{
    public class goConstants
    {
        #region Enum
        /// <summary>
        /// Đơn vị thời gian 
        /// </summary>
        public enum TimeUnit
        {
            Min = 60,
            Hous = 3600,
            Day = 86400
        }

        /// <summary>
        /// Đếm số thời gian
        /// </summary>
        public enum CountRequest
        {
            Min = 20000,//50,
            Hous = 300000,//300,
            Day = 700000//7200
        }
        /// <summary>
        /// Các type của Action
        /// </summary>
        public enum ActionType
        {
            EXECUTE,
            FORWARD
        }

        public enum DeviceEnvironment
        {
            APP,
            WEB
        }

        public enum ServiceType
        {
            SMS,
            EMAIL
        }

        public enum Currency
        {
            VND,
            USD
        }
        public enum DataMode
        {
            PUBLIC,
            PRIVATE,
            TEMP
        }

        /// <summary>
        /// P: percent %
        /// M: Money VND
        /// </summary>
        public enum UnitMode
        {
            P,
            M
        }

        public enum Env_Code
        {
            DEV,
            TEST,
            UAT,
            LIVE
        }

        public enum DanhXung
        {
            ONG,
            BA
        }

        public enum TypeCache
        {
            REDIS,
            AWS
        }
        #endregion

        #region File
        /// <summary>
        /// Kiểu file EXCEL, DOCX,...
        /// </summary>
        public enum FileFormat
        {
            TXT,
            XLSX,
            XLS,
            DOC,
            DOCX,
            PDF,
            PNG,
            JPG,
            JPEG,

        }
        public enum FileDataType
        {
            BASE64,
            PATH
        }

        /// <summary>
        /// Loại file: GCN, KHAC, HDON,...
        /// </summary>
        public enum FileType
        {
            GDKX_MS, // XE
            HDON_XE, // XE
            XE_GHIHINH, // XE
            SDKX_MT, // XE
            GDAUP_B, // XE
            GDAUT_B, // XE
            GDUOIP_B, // XE
            GDUOIT_B, // XE
            KHAC,
            CMND,
            GCN,
            GYC,
            QUYEN_LOI
        }
        #endregion

        #region Database
        public static string KeyConnectionDB = "Connection";
        public static string KeyModeDB = "ModeDB";

        public static string KeySql = "SQLSERVER";
        public static string KeyOracle = "ORACLE";
        public static string KeyMySql = "MYSQL";
        public static string KeyMongoDb = "MONGODB";

        public static string KeyConnectMongo = "ConnectionMongo";
        //public static string KeyDatabaseMongo = "DatabaseMongo";
        //public static string KeyTableLogMongo = "TableLogMongo";
        //public static string KeyTableLogServiceMongo = "TableLogServiceMongo";
        #endregion

        #region DB mode config
        public static string ModeServerCache = "SERVERCACHE";
        #endregion

        #region key web config
        public static string timeCache = "TimeCache";
        public static string timeCacheToken = "Timeout_Token";
        public static string pathDeploy = "pathDeploy";
        //public static string urlResource = "urlResource";
        #endregion
        #region key web config Db
        public static string ENVIRONMENT = "ENVIRONMENT";
        public static string URL_SERVER_FILE = "URL_SERVER_FILE";
        public static string URL_SERVER_FILE_IP = "URL_SERVER_FILE_IP";
        public static string URL_WEB_PAY = "URL_WEB_PAY";
        public static string URL_WEB_PAY_NEW = "URL_WEB_PAY_NEW";
        public static string FPT_SMS_URL = "FPT_SMS_URL";
        public static string FPT_SMS_PATH = "FPT_SMS_PATH";
        public static string FPT_SMS_CLIENT_ID = "FPT_SMS_CLIENT_ID";
        public static string FPT_SMS_SECRET = "FPT_SMS_SECRET";
        public static string FPT_SMS_SESSION_ID = "FPT_SMS_SESSION_ID";
        public static string FPT_SMS_BRAND_NAME = "FPT_SMS_BRAND_NAME";
        public static string EMAIL_CMS = "EMAIL_CMS";
        public static string EMAIL_HOST = "EMAIL_HOST";
        public static string EMAIL_PORT = "EMAIL_PORT";
        public static string EMAIL_USER = "EMAIL_USER";
        public static string EMAIL_PASSWORD = "EMAIL_PASSWORD";
        public static string HDI_MAIL_ERROR = "HDI_MAIL_ERROR";
        public static string HDI_PASS_ERROR = "HDI_PASS_ERROR";
        public static string NAMEMAIL = "NAMEMAIL";
        public static string EMAIL_ENABLESSL = "EMAIL_ENABLESSL";

        public static string CentUrl = "CENT_URL_API";
        public static string CentPathLogin = "CENT_PATH_LOGIN";
        public static string CentTraKq = "CENT_TRA_KQ";
        public static string CentUserName = "CENT_USERNAME";
        public static string CentPassWord = "CENT_PASSWORD";
        public static string CentChannel = "CENT_CHANNEL";
        public static string CentBranchUnit = "CENT_BRANCH_UNIT";
        public static string CentBlockCode = "CENT_BLOCK_CODE";
        public static string CentCodeSuccess = "CENT_CODE_SUCCESS";
        public static string CentPathAddContract = "CENT_PATH_ADD_CONTRACT";
        public static string CentPathGetContract = "CENT_PATH_GET_CONTRACT";
        public static string CentDvsl = "CENT_DV_SL";
        public static string CentKenh = "CENT_KENH";
        public static string CentMaCn = "CENT_MA_CN";
        public static string CentPathIpn = "CENT_PATH_IPN";
        public static string CentEmail = "CENT_EMAIL";
        public static string CentPass = "CENT_PASS";
        public static string CentSecret = "CENT_SECRET";

        public static string SftpHost = "SFTP_HOST";
        public static string SftpUser = "SFTP_USER";
        public static string SftpPass = "	SFTP_PASS";

        public static string DatabaseMongo = "DATABASE_MONGO";
        public static string TableLogAudit = "TABLE_LOG_AUDIT";
        public static string TableLogService = "TABLE_LOG_SERVICE";

        public static string HdiResouce = "HDI_RESOURCE";

        public static string timeRecordCache = "TIME_RECORD_CACHE";
        public static string short_link = "SHORT_LINK";
        public static string SMS_API = "SMS_API";
        public static string TEMP_HEADER = "TEMP_HEADER";
        public static string TEMP_FOOTER = "TEMP_FOOTER";

        public static string HDI_SELLING = "HDI_SELLING";

        public static string HDI_E_COM = "HDI_E_COM";
        #endregion

        #region Biến để ghép chuỗi
        public static string SPLIT_ROW = "@HDI_ROW@";
        public static string SPLIT_FIELD = "@HDI_FIELD@";
        public static string SPLIT_ROW_CHILD = "@HDI_ROW_CHILD@";
        public static string SPLIT_FIELD_CHILD = "@HDI_FIELD_CHILD@";
        #endregion

        #region Order
        public enum FieldOrder
        {
            BH,
            THOI_TRANG,
        }

        public enum PayMethod
        {
            ON_HDI_CHANNEL,
            ON_PARTNER_CHANNEL
        }

        public enum CategoryOrderDetail
        {
            BAO_HIEM_VAY,
            BH_VNAT

        }

        /// <summary>
        /// BH_M: Bảo hiểm mới
        /// BH_SDBS: Bảo hiểm sửa đổi bổ sung
        /// BH_BT: Bảo hiểm bồi thường
        /// </summary>
        public enum TypeOrder
        {
            BH_NHAP,
            BH_M,
            BH_S,
            BH_SDBS,
            BH_BT,

        }

        public enum PersonType
        {
            CN,
            CQ
        }

        public enum OrderStatus
        {
            PAID
        }

        /// <summary>
        /// Hình thức thanh toán
        /// </summary>
        public enum Payment_type
        {
            CTT, // Cổng thanh toán
            TH, // Thu hộ
            CK, // Chuyển khoản
            TM // Tiền mặt
        }

        public enum ActionCallback
        {
            APPROVE,
            REJECT,
            REJECT_APPROVE,
            CALENDAR_RESET,
            WAIT_CONFIRM
        }
        #endregion

        #region Insurance
        public enum Relationship
        {
            BAN_THAN,
            BO_ME,
            ANH_CHI,
            EM,
            VO_CHONG,
            CON_CAI,
            KHAC
        }
        /// <summary>
        /// Quyền sử dụng đất
        /// </summary>
        public enum LandUseRights
        {
            O, // chủ nhà
            L, // Quyền trông coi ngôi nhà
            U // Quyền sử dụng ngôi nhà
        }

        public enum ModeInsurance
        {
            TRUC_TIEP,
            HD_BAO,
            LINH_DONG
        }

        public enum Duration_Unit
        {
            Y,
            M,
            D
        }

        public enum Gender_Unit
        {
            M,
            F
        }

        public enum Org_Code
        {
            HDBANK_VN,
            HDI,
            CENTECH_VN,
            HDI_PRIVATE,
            VIETJET_VN,
            HDI_UPLOAD,
            ONPOINT,
            // Add 2022-11-29 By THienTVB
            GALAXY_CONNECT
            // End 2022-11-29 By THienTVB
        }

        public enum UserApi
        {
            HDI_UPLOAD
        }

        /// <summary>
        /// môi trường tạo đơn
        /// </summary>
        public enum EnviromentHDI
        {
            WEB_LANDING,
            SELLING
        }

        public enum ProducInsur
        {
            BAY_AT,
            LOST_BAGGAGE
        }

        public static string CATEGORY_ATTD = "CN.04";
        public static string CATEGORY_SK = "CN.02";
        public static string CATEGORY_XE = "XE";
        public static string CATEGORY_TAINAN = "CN.03";
        public static string CATEGORY_NHATN = "TS10";
        // Add 2022-04-07 By ThienTVB
        public static string CATEGORY_DEVICE = "TS16";
        //KhanhPT 2023-07-20
        public static string CATEGORY_TRAVEL = "CN.05";
        public static string CATEGORY_TRAVEL_INTER = "CN.06";
        public static string PACKCODE_DEVICE_GB_1M = "GB_1M";
        public static double PACKCODE_DEVICE_GB_1M_FEES = 8000;
        public static string PACKCODE_DEVICE_GB_6M = "GB_6M";
        public static double PACKCODE_DEVICE_GB_6M_FEES = 20000;
        public static string PACKCODE_DEVICE_GB_1Y = "GB_1Y";
        public static double PACKCODE_DEVICE_GB_1Y_FEES = 40000;
        public static string PACKCODE_DEVICE_GV_1M = "GV_1M";
        public static double PACKCODE_DEVICE_GV_1M_FEES = 15000;
        public static string PACKCODE_DEVICE_GV_6M = "GV_6M";
        public static double PACKCODE_DEVICE_GV_6M_FEES = 38000;
        public static string PACKCODE_DEVICE_GV_1Y = "GV_1Y";
        public static double PACKCODE_DEVICE_GV_1Y_FEES = 75000;
        public static double VAT_DEVICE = 10;
        // End 2022-04-07 By ThienTVB

        public enum Product_Xe
        {
            XCG_VCX_NEW,
            XCG_TNDSTN,
            XCG_TNDSBB,
            //XCG_TNDSBB_TN,
            //XCG_VCX_TN,
            XCG_HH
        }
        public enum Product_Attd
        {
            ATTD,
            ATTD_NH
        }

        public enum Pack_Attd
        {
            A,
            AB,
            AC,
            ABC
        }
        /// <summary>
        /// Người thanh toán:
        /// C: Customer
        /// B: Bank
        /// </summary>
        public enum Payer_Unit
        {
            C,
            B
        }

        public enum After_Signed
        {
            SIGNED,
            SEND_EMAIL,
            SEND_SMS,
            SEND_EMAIL_SMS
        }
        public enum SurveyorType
        {
            DLCA, /*đại lý chụp ảnh*/
            GDLKH,//Đến giám định theo lịch KH
            GDT//Gọi điện thoại thống nhất thời gian với KH
        }

        public enum KeyFeesInfo
        {
            XCGTN_BS, // phí tổng TN
            XCGTN_VQM, // phí vqm
            XCGTN_N3_F, // phí thân thể ng t3
            XCGTN_TSN3_F, // phí ts ng t3
            XCGTN_HK_F, // phí hành khách
            XCGTN_FNTX, // phí ntx, lpx
            XCGTN_FBHHH, // phí bh hàng hóa
            XCG_VCX_FEES, // phí VCX
            XCG_PL1, // phí gốc vcx
            XCG_VCX_BS, // tổng phí bổ sung VCX
            TNDSBB_F, // Phí TNDS BB
            NHAF1, // Tổng phí NHATN
            NHA_FVF, // Tổng phí giá trị tài sản trong nhà NHATN
            NHA_HVF, // Tổng phí giá trị nhà NHATN

        }

        #endregion

        #region Validate data Fees
        public enum KeyDefineFees
        {
            BO_SUNG,
            CAR_PRICE,
            VH_TYPE,
            YEAR_OF_USE,
            CAR_SEAT,
            CAR_WEIGHT,
            CAR_PURPOSE
        }
        #endregion

        #region Centech
        public enum Cent_nv
        {
            ATTD
        }

        public enum Cent_PersonType
        {
            C
        }

        public enum Cent_kieu_hd
        {
            G
        }
        #endregion

        #region HDI private
        public string secret_private_HDI_D = "B5F0A62DA1E8B5F3E0551F6E21B40329";
        public string secret_private_HDI_T = "B5F0A62DA1EAB5F3E0551F6E21B40329";
        public string secret_private_HDI_U = "B5F0A62DA1EAB5F3E0551F6E21B40329";
        public string secret_private_HDI_L = "B5F0A62DA1ECB5F3E0551F6E21B40329";
        #endregion

        #region HDI Upload
        public string secret_private_HDI_UP_D = "HDI_UPLOAD_198282911FASE1239212";
        public string secret_private_HDI_UP_T = "HDI_UPLOAD_198282911FASE1239212";
        public string secret_private_HDI_UP_U = "HDI_UPLOAD_198282911FASE1239212";
        public string secret_private_HDI_UP_L = "HDI_UPLOAD_198282911FASE1239212";
        #endregion

        #region OTP
        public enum OTP_TYPE
        {
            VOUCHER,

        }
        #endregion

        #region Dynamic fees
        public enum TypeValueDynamic
        {
            CONSTANT,
            CACHE,
            FORMULA
        }

        public enum TypeFeesDynamic
        {
            CHINH,
            BSUNG
        }

        public enum TypeEncryptFees
        {
            NONE,
            MD5
        }

        #endregion

        #region Callback
        public enum TypeApiCallBack
        {
            LOGIN, // hàm login
            APPROVE, // callback khi duyệt đơn

        }

        public static string E_COM_CALLBACK = "e_com_callback";
        public static string E_COM_HASHKEY = "e_com_hashkey";

        public static string VIKKI_LOGIN_URL = "vikki_auth";
        public static string VIKKI_LOGIN_CLIENTID = "vikki_clientid";
        public static string VIKKI_LOGIN_CLIENTSECRET = "vikki_clientsecret";
        public static string VIKKI_API_KEY = "vikki_apikey";
        public static string VIKKI_PATH_CALLBACK = "vikki_callback_path";
        public static string VIKKI_PATH_CALLBACKỦL = "vikki_callback_url";
        public static string VIKKI_HASHKEY = "vikki_hashkey";
        public static string VIKKI_CHARACTERS = "vikki_characters";
        public static string VIKKI_NOTI_BUFFER = "vikki_noti_buffer";
        public static string VIKKI_INDEX = "vikki_noti_index";
        public static string VIKKI_ENV = "vikki_env";
        #endregion

        #region Contructor
        private static readonly Lazy<goConstants> InitInstance = new Lazy<goConstants>(() => new goConstants());
        public static goConstants Instance => InitInstance.Value;
        #endregion
    }
}
