﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.CONSTANTS
{
    public class goConstantsRedis
    {
        #region Variables
        #region Variables Key Redis
        public static string ActionApis_Key = "ActionApis_Key";
        public static string ActionApiBehavios_Key = "ActionApiBehavios_Key";
        public static string ActionApiDatabase_Key = "ActionApiDatabase_Key";
        public static string User_Key = "User_Key";
        public static string PartnerConfig_Key = "PartnerConfig_Key";
        public static string PartnerGroupApi_Key = "PartnerGroupApi_Key";
        public static string Domain_Key = "SysDomain_Key";
        public static string WhiteList_Key = "SysPartnerWhiteList_Key";

        public static string BlackList_Key = "SysBlackList_Key";


        public static string PrefixSysUser = "SysUser_";
        public static string PrefixOtp = "Otp_";
        public static string PrefixWebconfig = "Wcf_";
        public static string ExitstWebconfig = "Wcf_ExitstWebconfig";

        public static string OrgKey = "ORG_INFO_DATA";
        #endregion

        #region Variables Redis Appconfig
        public static string KeyConnectionRedis = "ConnectionRedis"; //localhost,allowAdmin=true
        public static string KeyDBRedisIndex = "DBRedisIndex";
        public static string KeyDBRedisAsync = "DBRedisAsync";

        public static string KeyTypeCache = "TypeCache";
        #endregion

        #region Variables Dynamic Fees
        public static string PrefixDynamicFess = "DynF_";
        public static string PrefixCriteria = "Criteria";
        public static string PrefixCriteriaDetail = "CriteriaDetail";
        public static string PrefixDefineFees = "DefineFees";
        public static string PrefixNotBuy = "NotBuy";
        public static string PrefixFeesMin = "FeesMin";
        #endregion

        #region Record
        public static string PrefixDataRecord = "RecordData_";
        #endregion

        #endregion

        #region Contructor
        private static readonly Lazy<goConstantsRedis> InitInstance = new Lazy<goConstantsRedis>(() => new goConstantsRedis());
        public static goConstantsRedis Instance => InitInstance.Value;
        #endregion
    }
}
