﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OPEN.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_BeginRequest()
        {
            //var a = Request.Headers.AllKeys.Contains("Origin");
            //GO.COMMON.Log.Logger.Instance.WriteLog(GO.COMMON.Log.Logger.TypeLog.DEGBUG, "1113 " + a.ToString(), "Application_BeginRequest");
            //GO.COMMON.Log.Logger.Instance.WriteLog(GO.COMMON.Log.Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(Request.Headers.AllKeys), "Application_BeginRequest");
            //if (Request.Headers.AllKeys.Contains("Origin"))
            //{
            //    //Request.Headers.Add("Access-Control-Allow-Origin", "*");
            //    //Request.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
            //    //Request.Headers.Add("Access-Control-Allow-Headers", "Content-Type");
            //    //Response.Headers.Add("Access-Control-Allow-Origin", Request.Headers["Origin"]);
            //    //Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, DELETE");
            //    //Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type");
            //    Response.Flush();
            //}
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
