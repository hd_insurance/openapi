﻿using Owin;
using Microsoft.Owin;

using GO.BUSINESS.CacheSystem;

[assembly: OwinStartupAttribute(typeof(OPEN.API.Startup))]

namespace OPEN.API
{
    //Microsoft.Owin.Host.SystemWeb
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Init caching when startup
            goInstanceCaching.Instance.InstanceCaching();

        }
    }
}
