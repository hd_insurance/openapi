﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GO.BUSINESS.Insurance;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;
//using static GO.HELPER.Utility.goUtility;

namespace OPEN.API.Controllers
{
    public class CalculateFeesController : ApiController
    {
        [AllowAnonymous]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/calculate/fees_close")]
        public async Task<IHttpActionResult> CalculateFees(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessCalculateFees.Instance.GetResDynamicFees(request);
            //goBusinessCalculateFees.Instance.GetFees();
            return Ok(response);
        }

        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/calculate/v2/fees")]
        public async Task<IHttpActionResult> CalculateFeesVer(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessCalculateFees.Instance.GetResDynamicFeesVer2(request);
            
            //goBusinessCalculateFees.Instance.GetFees();
            return Ok(response);
        }

        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/calculate/v3/fees")]
        public async Task<IHttpActionResult> CalculateFeesVer3(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessCalculateFees.Instance.GetResDynamicFeesVer3(request);

            //goBusinessCalculateFees.Instance.GetFees();
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/calculate/recache")]
        public async Task<IHttpActionResult> RecacheCalculateFees(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessCalculateFees.Instance.GetRecache(request);
            //goBusinessCalculateFees.Instance.GetFees();
            return Ok(response);
        }

        #region Mask
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "mask_cal/v1/fees")]
        public async Task<IHttpActionResult> MaskCalFeesVer1(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessMaskFees.Instance.MaskFees(request);
            return Ok(response);
        }
        #endregion

        #region Suc khoe HDOSP
        [TokenAuthorization]
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/calculate/v4/fees")]
        public async Task<IHttpActionResult> CalculateFeesHealthCare(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessMaskFees.Instance.MaskFeesHealthCare(request);
            return Ok(response);
        }
        #endregion
    }
}
