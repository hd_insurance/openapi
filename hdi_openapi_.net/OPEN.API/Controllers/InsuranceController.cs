﻿using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

using GO.BUSINESS.Insurance;
using GO.SECURITY.Authorization;

using GO.HELPER.Utility;
using GO.BUSINESS.Action;
using System.Web;
using System.Collections.Specialized;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class InsuranceController : ApiController
    {
        //[HttpPost]
        //[Route(RouteRootAPICore.partialRootAPI + "bank/loan_add")]
        //public async Task<IHttpActionResult> AddContractLoans(BaseRequest request)
        //{
        //    BaseResponse response = new BaseResponse();
        //    //response = goBusinessInsurance.Instance.GetBaseResponse_InsurLoAdd(request);
        //    //goBusinessInsurance.Instance.GetInsurLoanCent(request);
        //    return Ok(response);
        //}
        //[AllowAnonymous]
        //[HttpPost]
        //[Route("hdi/selling/add")]
        //public async Task<IHttpActionResult> SaveInsuranceSelling(BaseRequest request)
        //{
        //    BaseResponse response = new BaseResponse();
        //    //response = goBusinessInsurance.Instance.GetBaseResponse_InsurLoInfo(request);
        //    //goBusinessInsurance.Instance.GetInsurLoanCent(request);
        //    return Ok(response);
        //}

        #region ATTD
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "lo/link")]
        public async Task<IHttpActionResult> CreateLinkSelling(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            response = goBusinessBankLo.Instance.CreateLinkSelling(request);
            //response = goBusinessInsurance.Instance.CreateLinkSelling(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "lo/status")]
        public async Task<IHttpActionResult> SearStatusInsur(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            response = goBusinessBankLo.Instance.SearStatusInsur(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "lo/status/loid")]
        public async Task<IHttpActionResult> SearStatusInsurByLoId(BaseRequest request)
        {
            BaseResponseV2 response = goBusinessBankLo.Instance.SearStatusInsurByLoId(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "lo/certificate")]
        public async Task<IHttpActionResult> SearCertificateInsur(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessBankLo.Instance.SearchCertificate(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        //[HttpPost]
        //[Route(RouteRootAPICore.partialRootAPI + "lo/disbursed")]
        //public async Task<IHttpActionResult> UpdateDisbursed(BaseRequest request)
        //{
        //    BaseResponse response = new BaseResponse();
        //    //response = goBusinessInsurance.Instance.UpdateDisbursed(request);
        //    return Ok(response);
        //}

        //[HttpPost]
        //[Route(RouteRootAPICore.partialRootAPI + "lo/decrease")]
        //public async Task<IHttpActionResult> LoDecrease(BaseRequest request)
        //{
        //    BaseResponse response = new BaseResponse();
        //    //response = goBusinessInsurance.Instance.LoDecrease(request);
        //    return Ok(response);
        //}

        #endregion

        #region Vietjet
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/safe_home/add")]
        public async Task<IHttpActionResult> SafeHomeAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResponseSafeHomeAdd(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/safe_home/confirm")]
        public async Task<IHttpActionResult> SafeHome_Confirm(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResponseSafeHomeConfirm(request);
            return Ok(response);
        }

        /// <summary>
        /// Bay an toàn
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v1/order")]
        public async Task<IHttpActionResult> OrderVJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResOrderVJC(request);
            return Ok(response);
        }

        /// <summary>
        /// Travel/SkyCare
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/travel/v1/order")]
        public async Task<IHttpActionResult> OrderTravelVJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResTravelOrderVJC(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v1/define/get")]
        public async Task<IHttpActionResult> GetDefineVJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            //response = goBusinessAction.Instance.GetBaseResponse(request);
            return Ok(response);
        }

        /// <summary>
        /// Cấp đơn chung cho vietjet (Mất hành lý)
        /// </summary>
        /// <returns></returns>
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v2/order/create")]
        public async Task<IHttpActionResult> OrderCreateV2VJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResOrderV2VJC(request);
            return Ok(response);
        }

        #region Vietjet opt-out - Old
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v1/product/get")]
        public async Task<IHttpActionResult> GetProdcutByOrg(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetProductByOrg(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v1/order/create")]
        public async Task<IHttpActionResult> CreateOrder(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateOrderByVietjet(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "VJC/v1/order/active")]
        public async Task<IHttpActionResult> ActiveOrder(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.ActiveOrderByVietjet(request);
            return Ok(response);
        }
        #endregion
        #endregion

        #region Tính phí

        #endregion

        #region Chiến sỹ vắc xin
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "csvx/orders/add")]
        public async Task<IHttpActionResult> CsvxOrdersAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CsvxOrdersAdd(request);
            return Ok(response);
        }
        #endregion

        #region Xe cơ giới

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/vehicle/create")]
        public async Task<IHttpActionResult> CreateInsurVehicle(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateInsurVehicle(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "insur/vehicle/create_pay")]
        public async Task<IHttpActionResult> CreateAndPayInsurVehicle(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateInsurVehicle(request, true);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "ver2/insur/vehicle/create")]
        public async Task<IHttpActionResult> CreateInsurVehicleVer2(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateInsurVehicleVer2(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "ver2/insur/vehicle/preview")]
        public async Task<IHttpActionResult> CreateInsurVehicleVer2_Preview(BaseRequest request)
        {            
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateInsurVehicleVer2_Preview(request, true);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "ver2/insur/vehicle/create_pay")]
        public async Task<IHttpActionResult> CreateAndPayInsurVehicleVer2(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CreateInsurVehicleVer2(request, true);
            return Ok(response);
        }

        #endregion

        #region Visa Master And Import All
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/order/common/add")]
        public async Task<IHttpActionResult> OrderProduct(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetResOrderCommon(request);
            return Ok(response);
        }
        #endregion

        #region Sản phẩm chung
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/product/get")]
        public async Task<IHttpActionResult> GetProdcutByAnci(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetProductByOrg(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/product/detail")]
        public async Task<IHttpActionResult> GetDetailInsur(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetDetailInsur(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/record/qr")]
        public async Task<IHttpActionResult> GetQrRecord(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetQrRecord(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/record/qr/data")]
        public async Task<IHttpActionResult> GetQrRecordData(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.GetDataRecordCache(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/record/commit")]
        public async Task<IHttpActionResult> CommitRecord(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.CommitRecord(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/insur/exists")]
        public async Task<IHttpActionResult> InsurExists(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.InsurExists(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/token/valid")]
        public async Task<IHttpActionResult> ValidToken(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessBankLo.Instance.ValidToken(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/v1/insur/search")]
        public async Task<IHttpActionResult> InsurSearch(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            response = goBusinessInsurance.Instance.SearInsur(request);
            return Ok(response);
        }
        #endregion

        #region Hàm chung mặt nạ cho các sản phẩm bảo hiểm
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/insur/create_pay")]
        public async Task<IHttpActionResult> MaskCreatePay(BaseRequest request)
        {            
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception)
            {
            }
            response = goBusinessInsurance.Instance.GetResMask(request, tracking, true);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/insur/preview")]
        public async Task<IHttpActionResult> MaskCreatePay_Preview(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception)
            {
            }
            response = goBusinessInsurance.Instance.GetResMask_Preview(request, tracking, true);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/insur/create")]
        public async Task<IHttpActionResult> MaskCreate(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }

            response = goBusinessInsurance.Instance.GetResMask(request, tracking, false);
            return Ok(response);
        }

        /// <summary>
        /// Hàm gọi lấy các thông tin duyệt, từ chối duyệt, từ chối bán BH
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/insur/status")]
        public async Task<IHttpActionResult> InsurStatus(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.UdpStatus(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/order/payment")]
        public async Task<IHttpActionResult> InsurPayment(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.ContractPaymentAction(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/order/search/trans_partner")]
        public async Task<IHttpActionResult> SearchTransPartner(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.SearchOrderByTrans(request);
            return Ok(response);
        }
         
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/order/valid_list")]
        public async Task<IHttpActionResult> OrderValidList(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.OrderValidList(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v2/mask/insur/create_pay")]
        public async Task<IHttpActionResult> MaskCreatePayV2(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2(request, tracking, true);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v2/mask/insur/create")]
        public async Task<IHttpActionResult> MaskCreateV2(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2(request, tracking, false);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/mask/insur/import")]
        public async Task<IHttpActionResult> MaskCreateV1_Import(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV1_Import(request, tracking, true);
            return Ok(response);
        }
        #endregion

        #region Movi
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "movi/bhsk/order/create")]
        public async Task<IHttpActionResult> OrderCreateMovi_BHSK(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_SK_Movi(request, tracking);
            return Ok(response);
        }
        #endregion Movi

        #region MFast
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "mfast/bhsk/order/create")]
        public async Task<IHttpActionResult> OrderCreateMFast_BHSK(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_SK_MFast(request, tracking);
            return Ok(response);
        }
        #endregion MFast

        #region Vikki
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "vikki/bhsk/order/create")]
        public async Task<IHttpActionResult> OrderCreateVikki_BHSK(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_SK_Vikki(request, tracking);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "vikki/bhsk/order/createandpay")]
        public async Task<IHttpActionResult> OrderCreateAndPayVikki_BHSK(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_SK_VikkiCreateAndPay(request, tracking);
            return Ok(response);
        }
        #endregion Vikki

        #region HDSaison
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdsaison/bhsk/order/create")]
        public async Task<IHttpActionResult> OrderCreateHDSaison_BHSK(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_SK_HDSaison(request, tracking);
            return Ok(response);
        }
        #endregion HDSaison

        #region KS/Nghi duong
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "ksnd/v1/order/create")]
        public async Task<IHttpActionResult> OrderCreateKSND(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.GetResMaskV2_KSND(request, tracking);
            return Ok(response);
        }
        #endregion KS/Nghi duong

        #region Search GCN Order Partner
        //[AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "order/search")]
        public async Task<IHttpActionResult> OrderPartner_Search(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.OrderPartner_Search(request, tracking);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "vikki/notify")]
        public async Task<IHttpActionResult> NotifyViKi(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string tracking = "";
            try
            {
                NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
                tracking = nvc["tracking"];
            }
            catch (Exception ex)
            {
            }
            response = goBusinessInsurance.Instance.VikiNotify(request, tracking);
            return Ok(response);
        }
        #endregion Movi
    }
}
