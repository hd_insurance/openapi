﻿using GO.BUSINESS.Service;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class CRMController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/crm/required")]
        public async Task<IHttpActionResult> SafeHomeAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessCRM.Instance.addRequired(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/crm/customerEmail")]
        public async Task<IHttpActionResult> customerEmail(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessCRM.Instance.getEmailCustomer(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/crm/requiredPH")]
        public async Task<IHttpActionResult> requiredPH(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.sendEmailPhaoHoa(request);
            return Ok(response);
        }
    }
}
