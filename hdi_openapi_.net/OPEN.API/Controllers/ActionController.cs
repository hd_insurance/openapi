﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

using GO.CONSTANTS;
using GO.SECURITY.Filter;
using GO.SECURITY.Authorization;
using GO.DTO.Base;
using Newtonsoft.Json;
using GO.DTO.Common;
using GO.BUSINESS.Action;
using GO.BUSINESS.SftpFile;
using GO.HELPER.Utility;
using GO.BUSINESS.Common;

namespace OPEN.API.Controllers
{
    public class ActionController : ApiController
    {
        /// <summary>
        /// Api post common 
        /// </summary>
        /// <param name="request">request client send</param>
        /// <returns></returns>
        [TokenAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route("OpenApi/Post")]
        public async Task<IHttpActionResult> BasePost(BaseRequest request)//object jsonPost)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetBaseResponse(request);

            return Ok(response);
        }

        [TokenAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        //[AllowAnonymous]
        [HttpPost]
        [Route("OpenApi/GetCerInfo")]
        public async Task<IHttpActionResult> GetCerInfo(BaseRequest request)//object jsonPost)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetInfoCer_SignBack(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("OpenApi/GetCerInfo_Url")]
        public async Task<IHttpActionResult> GetCerInfo_Url(String cert_no, String name, String hash)//object jsonPost)
        {
            BaseResponse response = new BaseResponse();
            String url_Redirect = "";
            url_Redirect = goBusinessAction.Instance.GetInfoCer_SignBack_Certificate(cert_no, name, hash);
            if (url_Redirect != "")
            {
                return Redirect(url_Redirect);
            }
            return Ok(response);
        }

        [TokenAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route("OpenApi/PostDev")]
        public async Task<IHttpActionResult> BasePostDev(BaseRequest request)//object jsonPost)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetBaseResponse(request);

            return Ok(response);
        }

        [HttpGet]
        [Route("OpenApi/RemoveAll")]
        public async Task<IHttpActionResult> ClearAllCache()
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetResponseByClearCache(null);
            return Ok();
        }

        [HttpGet]
        [Route("OpenApi/RemoveByKey")]
        public async Task<IHttpActionResult> ClearCache(string key)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetResponseByClearCache(key);
            return Ok();
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("OpenApi/RemoveByKeyPrefix")]
        public async Task<IHttpActionResult> ClearCacheKeyPrefix(string key)
        {
            goBusinessCache.Instance.RemoveAllPrefix(key);
            return Ok();
        }

        [HttpGet]
        [Route("OpenApi/RemoveParams")]
        public async Task<IHttpActionResult> RemoveParams()
        {
            goBusinessAction.Instance.RemoveParams("ORACLE");
            return Ok();
        }

        //[System.Web.Http.Cors.EnableCors(origins: "*", headers: "*", methods: "*")]
        [HttpPost]
        [Route("OpenApi/sample")]
        public async Task<IHttpActionResult> SamplePost()//object jsonPost)
        {
            goBusinessAction.Instance.Sample();

            //List<object> lst = goBusinessAction.Instance.Get<object>("mongodb://103.237.145.141:27017/?readPreference=primary&ssl=false", "HDI", "Test");
            return Ok();
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("OpenApi/sftp/file/upload")]
        public async Task<IHttpActionResult> sftpFile(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessSftp.Instance.GetResponse(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("OpenApi/get_sign")]
        public async Task<IHttpActionResult> getSign(BaseRequest request)
        {
            GO.BUSINESS.Common.goBusinessCommon.Instance.GetSignRequest(ref request);
            return Ok(request);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("OpenApi/info_sign")]
        public async Task<IHttpActionResult> infoSign(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = GO.BUSINESS.Common.goBusinessCommon.Instance.GetSignInfo(request);
            return Ok(response);
        }

        [TokenAuthorization]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "audit/logs/get")]
        public async Task<IHttpActionResult> getAuditLogs(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessAction.Instance.GetAuditLos(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("OpenApi/logs/get")]
        public async Task<IHttpActionResult> getLogs(string key, string nam, string thang, string ngay)
        {
            string pathRoot = goUtility.Instance.GetPathProject();
            if (key == DateTime.Now.ToString("ddMMyyyy") + "HDI-Logs")
            {
                string path = pathRoot + @"\logs\" + nam + @"\" + thang;
                string fileName = @"\" + ngay + "." + thang + "." + nam + ".log";
                string filePath = path + fileName;
                if (!System.IO.File.Exists(filePath))
                {
                    return Ok("Không tồn tại file!");
                }

                string resulfText;
                int count = 0;
                while (true)
                {
                    if (count == 20)
                    {
                        return Ok("File đang đươc mở hoặc lỗi!!!");
                    }
                    try
                    {
                        string filePathCopy = path + @"\" + ngay + "." + thang + "." + nam + "(copy).log";
                        if (!System.IO.File.Exists(filePathCopy))
                        {
                            System.IO.File.Copy(filePath, filePathCopy);
                        }
                        resulfText = System.IO.File.ReadAllText(filePathCopy);
                        System.IO.File.Delete(filePathCopy);
                        break;
                    }
                    catch
                    {
                        count++;
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                return Ok(resulfText);
            }
            return Ok("Thất bại!!!");
        }
    }
}
