﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Login;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.SingleSignOn;
using GO.HELPER.Config;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OPEN.API.Controllers
{
    public class AccountController : ApiController
    {
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;

        /// <summary>
        /// Api login api
        /// </summary>
        /// <param name="request">request client send (data: {username, password})</param>
        /// <returns></returns>
        [LoginAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "Login")]
        public async Task<IHttpActionResult> LoginAPI(object jsonPost)
        {
            ResponseLogin response = new ResponseLogin();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseRequest request = new BaseRequest();
                if (jsonPost == null)
                    throw new Exception("null request");
                request = JsonConvert.DeserializeObject<BaseRequest>(JsonConvert.SerializeObject(jsonPost));
                response = goBusinessLogin.Instance.getResponseLogin(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message + ". json " + jsonPost, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultLogin(false, "", 0, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2010), goConstantsError.Instance.ERROR_2010);
            }
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "Logout")]
        public async Task<IHttpActionResult> Logout(BaseRequest request)
        {
            BaseResponse baseResponse = new BaseResponse();
            baseResponse = goBusinessLogin.Instance.GetResponseLogout(request, false);
            return Ok(baseResponse);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "LogoutAll")]
        public async Task<IHttpActionResult> LogoutAll(BaseRequest request)
        {
            BaseResponse baseResponse = new BaseResponse();
            baseResponse = goBusinessLogin.Instance.GetResponseLogout(request, true);
            return Ok(baseResponse);
        }

        [TokenAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "sso")]
        public async Task<IHttpActionResult> LoginSSO(BaseRequest request)
        {
            ResponseLoginSSO response = new ResponseLoginSSO();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                response = goBusinessSingleSignOn.Instance.GetResponseLogin_SSO(request);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultSSO(null, false, "", 0, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2010), goConstantsError.Instance.ERROR_2010);
            }
            return Ok(response);
        }

        [TokenAuthorization]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "sso/user/add")]
        public async Task<IHttpActionResult> UserAdd(BaseRequest request)
        {
            return Ok();
        }
    }
}
