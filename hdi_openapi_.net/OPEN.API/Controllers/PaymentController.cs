﻿using GO.BUSINESS.BusPayment;
using GO.BUSINESS.PaymentCallBack;
using GO.BUSINESS.Service;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Payment;
using GO.DTO.Service;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace OPEN.API.Controllers
{
    //[TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class PaymentController : ApiController
    {
        private string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [AllowAnonymous]
        [Route("payment/createUrlPay")]
        public async Task<IHttpActionResult> createUrlPay(BaseRequest obj)
        {
            BaseResponse response = new BaseResponse();
            response = goBusPay.Instance.createUrlPay(obj);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("payment/ipnPaymentAPI")]
        public async Task<IHttpActionResult> ipnPaymentAPI()
        {
            BaseResponse response = new BaseResponse();
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
            SortedList<String, String> _requestData = new SortedList<String, String>();
            foreach (KeyValuePair<string, string> kv in allUrlKeyValues)
            {
                _requestData.Add(kv.Key, kv.Value);
            }
            response = goBusPay.Instance.ipnPaymentVNPAY(_requestData, "VnpayPayment");
            //response = goBussinessPayment.Instance.ipnPaymentApi(_requestData, "content");
            JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(objReturn);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("payment/checkStatusPay")]
        public async Task<IHttpActionResult> checkPayment()
        {
            BaseResponse response = new BaseResponse();
            var allUrlKeyValues = ControllerContext.Request.GetQueryNameValuePairs();
            SortedList<String, String> _requestData = new SortedList<String, String>();
            foreach (KeyValuePair<string, string> kv in allUrlKeyValues)
            {
                _requestData.Add(kv.Key, kv.Value);
            }
            response = goBussinessPayment.Instance.checkPayment(_requestData);
            //JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("payment/ipnPayHDI")]
        public async Task<IHttpActionResult> ipnPayHDI()
        {
            string input;
            //using (var reader = new StreamReader(HttpContext.Current.Request.InputStream))
            //{
            //    input = reader.ReadToEnd();
            //}
            Task<string> content = Request.Content.ReadAsStringAsync();
            input = content.Result;
            BaseResponse response = new BaseResponse();
            //response = goBussinessPayment.Instance.ipnHdbank(input);
            response = goBusPay.Instance.ipnQRHdbank(input);
            JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(objReturn);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("payment/ipnPayHDIQR")]
        public async Task<IHttpActionResult> ipnPayHDIVNPAY()
        {
            string input;
            Task<string> content = Request.Content.ReadAsStringAsync();
            input = content.Result;
            BaseResponse response = new BaseResponse();
            response = goBusPay.Instance.ipnQRVnpay(input);
            JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(objReturn);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("payment/ipnBankTransfer")]
        public async Task<IHttpActionResult> ipnBankTransfer(JObject input)
        {
            //BaseResponse response = new BaseResponse();
            JObject objReturn = goBussinessPayment.Instance.ipnBankTransfer(input);
            //JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(objReturn);
        }
        //[HttpPost]
        //[Route("payment/ipnHDITrans")]
        //public async Task<IHttpActionResult> ipnHDITrans(BaseRequest input)
        //{
        //    BaseResponse response = new BaseResponse();
        //    response = goBussinessPayment.Instance.ipnHDITrans(input);
        //    //JObject objReturn = JObject.Parse(response.Data.ToString());
        //    return Ok(response);
        //}
        [HttpPost]
        [Route("payment/ipnTransfer")]
        public async Task<IHttpActionResult> ipnTransfer(JObject input)
        {
            //Cập nhật dữ liệu chuyển khoản vào hệ thống
            JObject objReturn = goBussinessPayment.Instance.updateTransferPayment(input);
            //JObject objReturn = JObject.Parse(response.Data.ToString());
            return Ok(objReturn);
        }
        //tao giao dịch thanh toán
        [HttpPost]
        [AllowAnonymous]
        [Route("payment/paymentOrder")]
        public async Task<IHttpActionResult> paymentOrder(BaseRequest obj)
        {
            BaseResponse response = new BaseResponse();
            response = goBusPay.Instance.payment(obj);
            //response = goBussinessPayment.Instance.payment(obj);
            return Ok(response);
        }
        [HttpPost]
        //[AllowAnonymous]
        [Route("payment/transfer")]
        public async Task<IHttpActionResult> transfer(BaseRequest obj)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPayment.Instance.getSetTransFer(obj);
            return Ok(response);
        }
        //tra cứu thanh toán từ web
        [AllowAnonymous]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
        [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
        [HttpPost]
        [Route("payment/info")]
        public async Task<IHttpActionResult> getPaymentTransaction(ReqPayModel request)//object jsonPost)
        {
            string nameLog = Logger.ConcatName(LoggerName, System.Reflection.MethodBase.GetCurrentMethod().ToString());

            BaseResponse response = new BaseResponse();
            //GenPaymentModels info = new GenPaymentModels();
            response = goBusPay.Instance.getInfoPayGateway(request);

            //response.Success = true;
            //response.Data = info;
            //response.Signature = "";
            //response = goBusinessAction.Instance.GetBaseResponse(request);
            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), nameLog);
            return Ok(response);
        }
        //tra cứu thanh toán
        [HttpPost]
        [AllowAnonymous]
        [Route("payment/findPayment")]
        public async Task<IHttpActionResult> findPayment(BaseRequest request)//object jsonPost)
        {
            string nameLog = Logger.ConcatName(LoggerName, System.Reflection.MethodBase.GetCurrentMethod().ToString());

            BaseResponse response = new BaseResponse();
            GenPaymentModels info = new GenPaymentModels();
            info = goBussinessPayment.Instance.findPayment(request);

            response.Success = true;
            response.Data = info;
            response.Signature = "";
            //response = goBusinessAction.Instance.GetBaseResponse(request);
            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), nameLog);
            return Ok(response);
        }
    }
}
