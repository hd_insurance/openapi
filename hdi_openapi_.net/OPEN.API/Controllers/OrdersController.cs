﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

using GO.CONSTANTS;
using GO.SECURITY.Filter;
using GO.SECURITY.Authorization;
using GO.DTO.Base;
using GO.BUSINESS.Orders;
using GO.COMMON.Log;
using Newtonsoft.Json;

namespace OPEN.API.Controllers
{
    //[TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class OrdersController : ApiController
    {
        [TokenAuthorization]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "orders/create")]
        public async Task<IHttpActionResult> CreateOrders(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(request), Logger.ConcatName("OrdersController", "rders/create"));
            response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(request);
            return Ok(response);
        }
        [TokenAuthorization]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "orders/create_pay")]
        public async Task<IHttpActionResult> CreateAndPayOrders(BaseRequest request)
        {
            BaseResponse response = new BaseResponse(); 
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(request), Logger.ConcatName("OrdersController", "rders/create_pay"));
            response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route(RouteRootAPICore.partialRootAPI + "zalo/cb")]
        public async Task<IHttpActionResult> zaloCB()
        {
            try
            {
                string tracking = "";
                try
                {
                    System.Collections.Specialized.NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query);
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(nvc), Logger.ConcatName("zaloCB", "cb"));
                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception)
            {
            }

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "viber/set_webhook")]
        public async Task<IHttpActionResult> ViberCB(object jsonPost)
        {
            try
            {
                string tracking = "";
                try
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(jsonPost), Logger.ConcatName("viber", "cb"));
                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception)
            {
            }

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "zalo/set_webhook")]
        public async Task<IHttpActionResult> ZaloCBWh(object jsonPost)
        {
            try
            {
                string tracking = "";
                try
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(jsonPost), Logger.ConcatName("zalo", "set_webhook"));
                }
                catch (Exception ex)
                {
                }
            }
            catch (Exception)
            {
            }

            return Ok();
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "orders/bank_add")]
        public async Task<IHttpActionResult> BankAddOrders(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/orders/regisHG")]
        public async Task<IHttpActionResult> regisHG(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessOrders.Instance.regis_staff(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/orders/UpdateRegisHG")]
        public async Task<IHttpActionResult> UpdateRegisHG(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessOrders.Instance.update_regis_staff(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/orders/getStaffByid")]
        public async Task<IHttpActionResult> staff_byID(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessOrders.Instance.get_staff_byID(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/orders_regisHG/create")]
        public async Task<IHttpActionResult> OrdersRegisCreate(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessOrders.Instance.regis_staff_create_insur(request);
            return Ok(response);
        }

        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "orders/transfer/approve")]
        public async Task<IHttpActionResult> ApproveTransfer(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessOrders.Instance.ApproveOrdTransfer(request);
            return Ok(response);
        }


    }
}
