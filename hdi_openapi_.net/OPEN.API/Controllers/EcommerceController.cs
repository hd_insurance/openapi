﻿using GO.BUSINESS.Ecommerce;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static GO.CONSTANTS.goConstants;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class EcommerceController : ApiController
    {
        private string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/getProducts")]
        public async Task<IHttpActionResult> GetProducts(BaseRequest request)
        {
            BaseResponse response = goBusinessEcommerce.Instance.GetProducts(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/getSortLink")]
        public async Task<IHttpActionResult> getSortLink(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.OrgSkuCreateOrder(request);
            return Ok(response);
        }
        [HttpPost]
        [Route("hdi/ecommerce/createOrder")]
        public async Task<IHttpActionResult> createOrder(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.OrgSkuCreateOrder(request);
            return Ok(response);
        }

        #region Landing multi product
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/test")]
        public async Task<IHttpActionResult> INS_SKU(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.OrgSkuCreateOrder(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "hdi/landing/master")]
        public async Task<IHttpActionResult> LandingMaster(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.GetObjMaster(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/cancelSku")]
        public async Task<IHttpActionResult> cancelSku(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.cancelSKD(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/cancelSkuTool")]
        public async Task<IHttpActionResult> cancelSkuTool(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.cancelSKU_Tool(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/insert_detail_cer")]
        public async Task<IHttpActionResult> insert_detail_cer(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.insert_detail_cer(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/ecommerce/getSkuOrg")]
        public async Task<IHttpActionResult> getSkuOrg(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessEcommerce.Instance.getSkuByOrg(request);
            return Ok(response);
        }
        #endregion
    }
}