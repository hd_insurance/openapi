﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

using GO.CONSTANTS;
using GO.SECURITY.Filter;
using GO.SECURITY.Authorization;
using GO.DTO.Base;
using Newtonsoft.Json;
using GO.DTO.Common;
using GO.BUSINESS.Action;
using GO.DTO.Service;
using GO.BUSINESS.Service;
using GO.COMMON.Log;

using GO.HELPER.Config;
using GO.HELPER.Utility;
using System.Net.Http.Headers;
using System.Configuration;
using Newtonsoft.Json.Linq;
using GO.BUSINESS.Common;
using GO.BUSINESS.PartnerFunction;
using GO.BUSINESS.Insurance;
using System.Web;
using System.IO;

//using System.Web.Http.Cors;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class ServiceController : ApiController
    {
        private string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;

        [HttpPost]
        [Route("hdi/service/sms")]
        public async Task<IHttpActionResult> SmsPost(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessSMS.Instance.GetResponseSMS(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("hdi/service/email")]
        public async Task<IHttpActionResult> EmailPost(BaseRequest request)
        {
            
            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.GetResponseEmail(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendEmail")]
        public async Task<IHttpActionResult> sendEmail(BaseRequest request)
        {

            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.SendEmail_ORG(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("FTI/drl")]
        public async Task<IHttpActionResult> ipnHDISMS(object request)
        {
            BaseResponse response = new BaseResponse();
            HttpHeaders headers = Request.Headers;
            IEnumerable<string> values;
            JObject jOut = new JObject();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Data IPN SMS FPT header " + Request.Headers, Logger.ConcatName("servicecontroler", LoggerName));
                //if (headers.TryGetValues("Authorization", out values))
                //{
                headers.TryGetValues("Authorization", out values);
                string authen = values.First();
                string key_config = "Basic " + goBusinessCommon.Instance.GetConfigDB("FPT_SMS_KEY");
                //}
                if (authen.Equals(key_config))
                {
                    response = goBussinessSMS.Instance.ipnSMS(request);
                    jOut.Add("status", 1);
                }
                else
                {
                    jOut.Add("status", 0);
                    jOut.Add("desc", "Lỗi key md5");
                }
            }catch(Exception exx)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Data IPN SMS FPT header error" + exx.ToString(), Logger.ConcatName("servicecontroler", LoggerName));
                jOut.Add("status", 0);
                jOut.Add("desc", "Du lieu khong hop le");
            }
           
            
            return Ok(jOut);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sign")]
        public async Task<IHttpActionResult> SignFile(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessSign.Instance.GetBaseResponse(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/veryfiSign")]
        public async Task<IHttpActionResult> VeryfiFile(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessSign.Instance.veryfiSign(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sign_email")]
        public async Task<IHttpActionResult> SignFileEmail(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessSign.Instance.sign_mail_core(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendMailWithJson")]
        public async Task<IHttpActionResult> sendMailWithJson(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.sendMaiJson(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendMailAdmin")]
        public async Task<IHttpActionResult> sendMailAdmin(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.sendMailAdmin(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/impTemplate")]
        public async Task<IHttpActionResult> impTemplate(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.ImpTepmlate(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/addTextGCN")]
        public async Task<IHttpActionResult> addTextGCN(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.addTextToPdf(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/certificate")]
        public async Task<IHttpActionResult> convertHTMLToPDF(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.convertPDF(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/getPDFJson")]
        public async Task<IHttpActionResult> getPDFJson(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.getPDFJson(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/findCer")]
        public async Task<IHttpActionResult> findCer(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.findCer(request);
            return Ok(response);
        }
        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/viewCertificate")]
        public async Task<IHttpActionResult> viewCertificate(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.viewCertificate_Json(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/findCerVJ")]
        public async Task<IHttpActionResult> findCerVJ(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBussinessPDF.Instance.findCerVJ(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/signsign")]
        public async Task<IHttpActionResult> signsign(JObject request)
        {
            //là hàm ký lại khi có lỗi từ hàm createAndPay
            BaseResponse response = new BaseResponse();
            response = goBussinessSign.Instance.signsign(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/reSign")]
        public async Task<IHttpActionResult> reSign(JObject request)
        {
            //là hàm ký lại tạo giấy chứng nhận khi đã tồn tại dữ liệu
            BaseResponse response = new BaseResponse();
            response = goBussinessSign.Instance.reSign(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendSmsPrivate")]
        public async Task<IHttpActionResult> sendSmsPrivate(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.sendSMSPrivateHDI(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/emailNotifi")]
        public async Task<IHttpActionResult> emailNotifi(JObject request)
        {
            //Thông báo thu tiền hoặc các loại thông báo khác
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.emailNotifi(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendSmsContent")]
        public async Task<IHttpActionResult> sendSmsContent(SMS_CONTENT request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.sendSMSContenHDI(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/signApp")]
        public async Task<IHttpActionResult> signApp(JObject request)
        {
            //là hàm ký lại khi có lỗi từ hàm createAndPay
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.getQueueSign(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/reSendEmail")]
        public async Task<IHttpActionResult> reSendEmail(JObject request)
        {
            //là hàm ký lại khi có lỗi từ hàm createAndPay
            BaseResponse response = new BaseResponse();
            response = goBussinessEmail.Instance.reSendMailError(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/getTokenVerify")]
        public async Task<IHttpActionResult> getTokenVerify(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.getTokenVerify(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/verifyToken")]
        public async Task<IHttpActionResult> verifyToken(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessServices.Instance.verifyToken(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendObjectToPartner")]
        public async Task<IHttpActionResult> sendObjectToPartner(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.sendObjectToPartner(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/sendEmailtoBVietInsurance")]
        public async Task<IHttpActionResult> sendEmailtoBVietInsurance(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.sendEmailtoBVietInsurance(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/synDataltoBVietInsurance")]
        public async Task<IHttpActionResult> synDataltoBVietInsurance(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.synDataltoBVietInsurance(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/syn_data_core")]
        public async Task<IHttpActionResult> syn_data_core(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.syn_data_core(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/update_syn")]
        public async Task<IHttpActionResult> update_syn_data_core(JObject request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.update_syn_data_core(request);
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("hdi/service/getGCNBySoID")]
        public async Task<IHttpActionResult> get_gcn_core(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVietjet.Instance.get_gcn_core(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/getDocument")]
        public async Task<IHttpActionResult> getDocuments(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.getDocuments(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/searchPolicy")]
        public async Task<IHttpActionResult> searchPolicy(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.searchPolicy(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/submitClaim")]
        public async Task<IHttpActionResult> submitClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.submitClaim(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/getDetailClaim")]
        public async Task<IHttpActionResult> getDetailClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.getDetailClaim(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]        
        [Route("hdi/claim/uploadDocument")]
        public async Task<IHttpActionResult> uploadDocument()
        {
            BaseResponse response = new BaseResponse();
                                 
            var httpRequest = HttpContext.Current.Request;

            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.Contents)
            {
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                byte[] buffer = await file.ReadAsByteArrayAsync();
                response = goBusinessInsurance.Instance.uploadDocument(httpRequest, buffer);
                //Do whatever you want with filename and its binary data.
            }
            
            return Ok(response);
        }        

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/updateBeneInfoClaim")]
        public async Task<IHttpActionResult> updateBeneInfoClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.updateBeneInfoClaim(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/updateDocumentClaim")]
        public async Task<IHttpActionResult> updateDocumentClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.updateDocumentClaim(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/claim/updateStatusClaim")]
        public async Task<IHttpActionResult> updateStatusClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.updateStatusClaim(request);
            return Ok(response);
        }

        //[AllowAnonymous]
        [HttpPost]
        [Route("hdi/policy/changeRequest")]
        public async Task<IHttpActionResult> changeRequest(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessInsurance.Instance.changeRequest(request);
            return Ok(response);
        }
    }
}
