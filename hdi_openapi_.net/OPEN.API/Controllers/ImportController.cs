﻿using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.IO;
using System.Data;

using GO.HELPER.Utility;
using GO.BUSINESS.Insurance;
using GO.DTO.Base;
//using GO.DTO.Insurance.ImportFile;

namespace OPEN.API.Controllers
{
    public class ImportController : ApiController
    {
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "v1/import/care/add")]
        public async Task<IHttpActionResult> ImportCare()
        {

            var httpRq = HttpContext.Current.Request;
            int file = httpRq.Files.Count;
            BaseResponse response = new BaseResponse();
            try
            {
                var pathServer = HttpContext.Current.Server.MapPath("~/UploadImport");
                goUtility.Instance.CreateFolder(pathServer);

                string action = httpRq.Headers["ACTION"].ToString();
                string id = httpRq.Headers["ID"].ToString();
                string action_no = "";// httpRq.Headers["ACTION_NO"].ToString();
                //string contract_no = httpRq.Headers["CONTRACT_NO"].ToString();
                string isSMS = httpRq.Headers["IS_SMS"].ToString();
                string isEmail = httpRq.Headers["IS_EMAIL"].ToString();
                string env = httpRq.Headers["ENV"].ToString();
                string partner = httpRq.Headers["ParentCode"].ToString();
                string secret = httpRq.Headers["Secret"].ToString();
                string userName = httpRq.Headers["UserName"].ToString();

                string[] arrExtent = new[] { ".xlsx", ".xls", ".XLSX", ".XLS" };

                string pathFile = "";
                foreach (string item in httpRq.Files)
                {
                    var postedFile = httpRq.Files[item];
                    string fileExtension = Path.GetExtension(postedFile.FileName);
                    string nameFile = Path.GetFileName(postedFile.FileName);
                    string nameFileNew = nameFile + DateTime.Now.ToString("yyyyMMddHHmmss");
                    if (!Array.Exists(arrExtent, i => i.Equals(fileExtension)))
                        throw new Exception("File không phải excel. Ahihi A Quỳnh !");
                    pathFile = pathServer + "\\" + nameFileNew + fileExtension;
                    postedFile.SaveAs(pathFile);
                    
                    break;
                }

                DataTable dt = goUtility.Instance.ReadExcelToTable(pathFile);
                //response.Data = httpRq.Files.Count;

                response = goBusinessImpInsur.Instance.ImpOrder(partner, secret, userName, action, id, action_no, isSMS, isEmail, env, dt);
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.ErrorMessage = ex.Message;
                //throw ex;
            }

            return Ok(response);
        }
    }
}