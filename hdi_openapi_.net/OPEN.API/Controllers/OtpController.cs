﻿using System.Threading.Tasks;
using System.Web.Http;
using GO.BUSINESS.Otp;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class OptController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "otp/send")]
        public IHttpActionResult OtpSend(BaseRequest request)
        {
            BaseResponse response = goBusinessOtp.Instance.OtpSend(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "otp/confirm")]
        public IHttpActionResult OtpConfirm(BaseRequest request)
        {
            BaseResponse response = goBusinessOtp.Instance.OtpConfirm(request);
            return Ok(response);
        }
    }
}
