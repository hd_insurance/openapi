﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using GO.BUSINESS.Voucher;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.SECURITY.Authorization;
using GO.SECURITY.Filter;

namespace OPEN.API.Controllers
{
    [TokenAuthorization]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Min, CountRequest = (int)goConstants.CountRequest.Min)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Hous, CountRequest = (int)goConstants.CountRequest.Hous)]
    [HeaderFilter(TimeUnit = goConstants.TimeUnit.Day, CountRequest = (int)goConstants.CountRequest.Day)]
    public class VoucherController : ApiController
    {
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "voucher/otp/send")]
        public async Task<IHttpActionResult> VoucherOtp(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVoucher.Instance.VoucherOtpSend(request);
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route(RouteRootAPICore.partialRootAPI + "voucher/ord_insur/none")]
        public async Task<IHttpActionResult> VoucherInsurFree(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            response = goBusinessVoucher.Instance.VoucherInsurFree(request);
            return Ok(response);
        }
    }
}
