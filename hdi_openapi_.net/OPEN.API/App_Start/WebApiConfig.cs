﻿using GO.CONSTANTS;
using GO.HELPER.Config;
using System.Web.Http;
using System.Web.Http.Cors;

namespace OPEN.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // New code
            //config.EnableCors();
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);
            // Web API configuration and services
            //config.Filters.Add(new ValidatorModelsFilter());
            // Web API routes
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
    internal class RouteRootAPICore
    {
        public const string partialRootAPI = "OpenApi/";
        private static string pathDeploy = goConfig.Instance.GetAppConfig(goConstants.pathDeploy);
    }
}
