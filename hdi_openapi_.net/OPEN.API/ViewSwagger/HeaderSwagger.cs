﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;

namespace OPEN.API.ViewSwagger
{
    public class HeaderSwagger : IOperationFilter
    {
        #region Nuget
        // Swashbuckle 5.6.0
        #endregion

        #region Method
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                operation.parameters = new List<Parameter>();

            operation.parameters.Add(new Parameter
            {
                name = "authToken",
                @in = "header",
                type = "string",
                @default = "",
                description = "Add token login api.",
                required = true
            });
        }
        #endregion
    }
}