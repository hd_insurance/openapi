
function closeMenu(){
	if($('#bm-menu').attr("state") == "open"){
		$('#bm-menu').attr("state", "close")
		$('#bm-menu').css("opacity", "0");
    	$('#bm-menu').css("transform", "translate3d(100%, 0px, 0px)");
    	$('.bm-menu-wrap').css("transform", "translate3d(100%, 0px, 0px)");
    }
}

$( document ).ready(function() {
    

    $('#btn-tggle-menu').click((e)=>{
    	if($('#bm-menu').attr("state") == "close"){
    		$('#bm-menu').attr("state", "open")
    		$('#bm-menu').css("opacity", "1");
	    	$('#bm-menu').css("transform", "translate3d(0px, 0px, 0px)");
	    	$('.bm-menu-wrap').css("transform", "translate3d(0px, 0px, 0px)");
    	}else{
    		closeMenu()
    	}
    	
    })
    $('#bm-menu').click((e)=>{
    	if($('#bm-menu').attr("state") == "open"){
    		closeMenu()
    	}
    	
    })
    $('.btn-close-menu').click((e)=>{
    	closeMenu()
    })

});