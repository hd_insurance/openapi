﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace GO.ENCRYPTION
{
    public class goEncryptBase
    {
        #region Type Encrypt
        public enum EncryptType
        {
            SHA1,
            SHA2,
            SHA256
        }
        #endregion

        #region Variable
        // file cert default
        private readonly string certificatePublic = @"C:\Users\TuanNV\Downloads\TestSignText\myca.cer";
        private readonly string CertificatePfx = @"C:\Users\TuanNV\Downloads\TestSignText\myca.pfx";
        private readonly string CertificateP12 = @"C:\Users\TuanNV\Downloads\TestSignText\myca.p12";
        private readonly string CertificatePassword = "123456";

        // key 1024
        public readonly string keyPublic = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        private readonly string keyPrivate = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent><P>/aULPE6jd5IkwtWXmReyMUhmI/nfwfkQSyl7tsg2PKdpcxk4mpPZUdEQhHQLvE84w2DhTyYkPHCtq/mMKE3MHw==</P><Q>3WV46X9Arg2l9cxb67KVlNVXyCqc/w+LWt/tbhLJvV2xCF/0rWKPsBJ9MC6cquaqNPxWWEav8RAVbmmGrJt51Q==</Q><DP>8TuZFgBMpBoQcGUoS2goB4st6aVq1FcG0hVgHhUI0GMAfYFNPmbDV3cY2IBt8Oj/uYJYhyhlaj5YTqmGTYbATQ==</DP><DQ>FIoVbZQgrAUYIHWVEYi/187zFd7eMct/Yi7kGBImJStMATrluDAspGkStCWe4zwDDmdam1XzfKnBUzz3AYxrAQ==</DQ><InverseQ>QPU3Tmt8nznSgYZ+5jUo9E0SfjiTu435ihANiHqqjasaUNvOHKumqzuBZ8NRtkUhS6dsOEb8A2ODvy7KswUxyA==</InverseQ><D>cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=</D></RSAKeyValue>";

        StreamReader sr = null;
        private bool m_pemFormat = false;
        public X509Certificate2 cert = null;
        bool _UsingX509KeyStorageFlags = true;
        public static UnicodeEncoding ByteConverter = new UnicodeEncoding();
        X509KeyStorageFlags _X509KeyStorageFlags = X509KeyStorageFlags.MachineKeySet;
        #endregion

        #region Contructor
        private static readonly Lazy<goEncryptBase> InitInstance = new Lazy<goEncryptBase>(() => new goEncryptBase());
        public static goEncryptBase Instance => InitInstance.Value;
        #endregion

        #region Method

        #region Base64
        public string Base64Endcode(string plainText)
        {
            try
            {
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(plainText);
                // convert the byte array to a Base64 string
                return Convert.ToBase64String(byt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string Base64Decode(string cipherText)
        {
            try
            {
                byte[] b = Convert.FromBase64String(cipherText);
                return System.Text.Encoding.UTF8.GetString(b);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region MD5
        public string Md5ProviderEncode(string plainText)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(plainText));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString().ToUpper();
        }
        public string Md5Encode(string plainText)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.UTF8.GetBytes(plainText);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToUpper();
            }
        }

        #region Md5 by key (short input)
        public string Md5Encode(string plainText, string key)
        {
            try
            {
                byte[] data = UTF8Encoding.UTF8.GetBytes(plainText);
                byte[] results = Md5Encode(data, key);
                return Convert.ToBase64String(results, 0, results.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public byte[] Md5Encode(byte[] input, string key)
        {
            try
            {
                using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
                {
                    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                    {
                        ICryptoTransform transform = tripDes.CreateEncryptor();
                        return transform.TransformFinalBlock(input, 0, input.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string Md5Decode(string cipherText, string key)
        {
            try
            {
                byte[] data = Convert.FromBase64String(cipherText); // decrypt the incrypted text
                byte[] results = Md5Decode(data, key);
                return UTF8Encoding.UTF8.GetString(results);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public byte[] Md5Decode(byte[] input, string key)
        {
            try
            {
                using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
                {
                    byte[] keys = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    using (TripleDESCryptoServiceProvider tripDes = new TripleDESCryptoServiceProvider() { Key = keys, Mode = CipherMode.ECB, Padding = PaddingMode.PKCS7 })
                    {
                        ICryptoTransform transform = tripDes.CreateDecryptor();
                        return transform.TransformFinalBlock(input, 0, input.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region AES
        #region Key Random
        public class KeyIV
        {
            public byte[] IV { get; set; }
            public byte[] Key { get; set; }
        }
        public class KeyIVBase64
        {
            public string IV { get; set; }
            public string Key { get; set; }
        }
        public static KeyIV GenerateKeyIV()
        {
            KeyIV keyIV = new KeyIV();
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.GenerateIV();
                rijAlg.GenerateKey();
                keyIV.IV = rijAlg.IV;
                keyIV.Key = rijAlg.Key;
            }
            return keyIV;
        }

        public string AESEncode(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key,
                                             rijAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt,
                            encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }
        public string AESDecode(string cipherText, byte[] Key, byte[] IV)
        {
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            string plaintext = null;
            byte[] cipherBytes = Convert.FromBase64String(cipherText);

            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key,
                                             rijAlg.IV);
                using (MemoryStream msDecrypt = new MemoryStream(cipherBytes))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                           decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;
        }
        #endregion

        #region Key fixed
        public string AESEncode(string plainText, string passWord)
        {
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(plainText);
            byte[] bytePassWord = UTF8Encoding.UTF8.GetBytes(passWord);
            byte[] byteResult = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(bytePassWord, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(byteData, 0, byteData.Length);
                        cs.Close();
                    }
                    byteResult = ms.ToArray();
                }
            }

            return Encoding.UTF8.GetString(byteResult);
        }
        public string AESDecrypt(string cipherText, string passWord)
        {
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(cipherText);
            byte[] bytePassWord = UTF8Encoding.UTF8.GetBytes(passWord);
            byte[] byteResult = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(bytePassWord, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(byteData, 0, byteData.Length);
                        cs.Close();
                    }
                    saltBytes = ms.ToArray();
                }
            }
            return Encoding.UTF8.GetString(byteResult);
        }
        #endregion
        #endregion

        #region RSA
        public string RSAEncode(string plainText)
        {
            try
            {
                var byteData = Encoding.UTF8.GetBytes(plainText);
                using (var rsa = new RSACryptoServiceProvider(1024))
                {
                    try
                    {
                        // client encrypting data with public key issued by server                    
                        rsa.FromXmlString(keyPublic.ToString());

                        var encryptedData = rsa.Encrypt(byteData, true);

                        var base64Encrypted = Convert.ToBase64String(encryptedData);

                        return base64Encrypted;
                    }
                    finally
                    {
                        rsa.PersistKeyInCsp = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string RSAEncode(string plainText, int dwKeySize)
        {
            string xmlString = keyPublic;
            // TODO: Add Proper Exception Handlers
            RSACryptoServiceProvider rsaCryptoServiceProvider =
                                          new RSACryptoServiceProvider(dwKeySize);
            rsaCryptoServiceProvider.FromXmlString(xmlString);
            int keySize = dwKeySize / 8;
            byte[] byteData = Encoding.UTF32.GetBytes(plainText);
            // The hash function in use by the .NET RSACryptoServiceProvider here 
            // is SHA1
            // int maxLength = ( keySize ) - 2 - 
            //              ( 2 * SHA1.Create().ComputeHash( rawBytes ).Length );
            int maxLength = keySize - 42;
            int dataLength = byteData.Length;
            int iterations = dataLength / maxLength;
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i <= iterations; i++)
            {
                byte[] tempBytes = new byte[
                        (dataLength - maxLength * i > maxLength) ? maxLength :
                                                      dataLength - maxLength * i];
                Buffer.BlockCopy(byteData, maxLength * i, tempBytes, 0,
                                  tempBytes.Length);
                byte[] encryptedBytes = rsaCryptoServiceProvider.Encrypt(tempBytes,
                                                                          true);
                // Be aware the RSACryptoServiceProvider reverses the order of 
                // encrypted bytes. It does this after encryption and before 
                // decryption. If you do not require compatibility with Microsoft 
                // Cryptographic API (CAPI) and/or other vendors. Comment out the 
                // next line and the corresponding one in the DecryptString function.
                Array.Reverse(encryptedBytes);
                // Why convert to base 64?
                // Because it is the largest power-of-two base printable using only 
                // ASCII characters
                stringBuilder.Append(Convert.ToBase64String(encryptedBytes));
            }
            return stringBuilder.ToString();
        }
        public string RSADecode(string cipherText)
        {
            try
            {
                using (var rsa = new RSACryptoServiceProvider(1024))
                {
                    try
                    {
                        var base64Encrypted = cipherText;
                        // server decrypting data with private key                    
                        rsa.FromXmlString(keyPrivate);

                        var resultBytes = Convert.FromBase64String(base64Encrypted);
                        var decryptedBytes = rsa.Decrypt(resultBytes, true);
                        var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                        return decryptedData.ToString();
                    }
                    finally
                    {
                        rsa.PersistKeyInCsp = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string RSADecode(string cipherText, int dwKeySize)
        {
            string xmlString = keyPrivate;
            // TODO: Add Proper Exception Handlers
            RSACryptoServiceProvider rsaCryptoServiceProvider = new RSACryptoServiceProvider(dwKeySize);
            rsaCryptoServiceProvider.FromXmlString(xmlString);
            int base64BlockSize = ((dwKeySize / 8) % 3 != 0) ?
              (((dwKeySize / 8) / 3) * 4) + 4 : ((dwKeySize / 8) / 3) * 4;
            int iterations = cipherText.Length / base64BlockSize;
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < iterations; i++)
            {
                byte[] encryptedBytes = Convert.FromBase64String(cipherText.Substring(base64BlockSize * i, base64BlockSize));
                // Be aware the RSACryptoServiceProvider reverses the order of 
                // encrypted bytes after encryption and before decryption.
                // If you do not require compatibility with Microsoft Cryptographic 
                // API (CAPI) and/or other vendors.
                // Comment out the next line and the corresponding one in the 
                // EncryptString function.
                Array.Reverse(encryptedBytes);
                arrayList.AddRange(rsaCryptoServiceProvider.Decrypt(encryptedBytes, true));
            }
            return Encoding.UTF32.GetString(arrayList.ToArray(Type.GetType("System.Byte")) as byte[]);
        }
        #endregion

        #region RSA Certificates
        public string RSAEncodeCert(string plainText, string pathFile, string passWord = "")
        {
            cert = GetCertificate2FromFile(pathFile, passWord);
            var _publicKeyRsaProvider = GetPublicKey(cert);

            Byte[] PlaintextData = Encoding.UTF8.GetBytes(plainText);
            int MaxBlockSize = _publicKeyRsaProvider.KeySize / 8 - 11;

            if (PlaintextData.Length <= MaxBlockSize)
            {
                return Convert.ToBase64String(_publicKeyRsaProvider.Encrypt(PlaintextData, false));
            }
            else
            {
                using (MemoryStream PlaiStream = new MemoryStream(PlaintextData))
                using (MemoryStream CrypStream = new MemoryStream())
                {
                    Byte[] Buffer = new Byte[MaxBlockSize];
                    int BlockSize = PlaiStream.Read(Buffer, 0, MaxBlockSize);

                    while (BlockSize > 0)
                    {
                        Byte[] ToEncrypt = new Byte[BlockSize];
                        Array.Copy(Buffer, 0, ToEncrypt, 0, BlockSize);

                        Byte[] Cryptograph = _publicKeyRsaProvider.Encrypt(ToEncrypt, false);
                        CrypStream.Write(Cryptograph, 0, Cryptograph.Length);

                        BlockSize = PlaiStream.Read(Buffer, 0, MaxBlockSize);
                    }

                    return Convert.ToBase64String(CrypStream.ToArray(), Base64FormattingOptions.None);
                }
            }
        }
        public string RSADecodeCert(string ciphertext, string pathFile, string passWord)
        {
            cert = GetCertificate2FromFile(pathFile, passWord);
            var _privateKeyRsaProvider = GetPrivateKey(cert);

            Byte[] CiphertextData = Convert.FromBase64String(ciphertext);
            int MaxBlockSize = _privateKeyRsaProvider.KeySize / 8;

            if (CiphertextData.Length <= MaxBlockSize)
                return System.Text.Encoding.UTF8.GetString(_privateKeyRsaProvider.Decrypt(CiphertextData, false));

            using (MemoryStream CrypStream = new MemoryStream(CiphertextData))
            using (MemoryStream PlaiStream = new MemoryStream())
            {
                Byte[] Buffer = new Byte[MaxBlockSize];
                int BlockSize = CrypStream.Read(Buffer, 0, MaxBlockSize);

                while (BlockSize > 0)
                {
                    Byte[] ToDecrypt = new Byte[BlockSize];
                    Array.Copy(Buffer, 0, ToDecrypt, 0, BlockSize);

                    Byte[] Plaintext = _privateKeyRsaProvider.Decrypt(ToDecrypt, false);
                    PlaiStream.Write(Plaintext, 0, Plaintext.Length);

                    BlockSize = CrypStream.Read(Buffer, 0, MaxBlockSize);
                }

                return System.Text.Encoding.UTF8.GetString(PlaiStream.ToArray());
            }
        }
        private X509Certificate2 GetCertificate2FromFile(string filePath, string passWord)
        {
            try
            {
                X509Certificate2 cert = null;

                if (m_pemFormat)
                {
                    string sPublicKey = GetCertString(filePath);

                    string cf = sPublicKey.Replace("-----BEGIN CERTIFICATE-----", "");
                    cf = cf.Replace("-----END CERTIFICATE-----", "");
                    byte[] bInput;
                    bInput = Convert.FromBase64String(cf);
                    cert = new X509Certificate2();
                    if (string.IsNullOrEmpty(passWord))
                    {
                        cert.Import(bInput);
                    }
                    else
                    {
                        cert.Import(bInput);
                    }

                    return cert;
                }

                if (!string.IsNullOrEmpty(passWord))
                {
                    if (_UsingX509KeyStorageFlags == false)
                    {
                        cert = new X509Certificate2(filePath, passWord);
                    }
                    else
                    {
                        cert = new X509Certificate2(filePath, passWord, _X509KeyStorageFlags);//X509KeyStorageFlags.MachineKeySet
                    }
                }
                else
                {
                    cert = new X509Certificate2(filePath);
                }

                return cert;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string GetCertString(string filePath)
        {
            try
            {

                sr = new StreamReader(filePath);
                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                throw new Exception("GetCertString(): " + ex.Message);
            }
        }
        private RSACryptoServiceProvider GetPublicKey(X509Certificate2 cert)
        {
            try
            {
                PublicKey publickey = cert.PublicKey;
                return (RSACryptoServiceProvider)publickey.Key;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private RSACryptoServiceProvider GetPrivateKey(X509Certificate2 cert)
        {
            try
            {
                return (RSACryptoServiceProvider)cert.PrivateKey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Certificates new RSA - SHA
        byte[] Sign(string text, string p12Path, string p12Password, EncryptType type)
        {
            X509Certificate2 cert = new X509Certificate2(p12Path, p12Password, X509KeyStorageFlags.MachineKeySet);
            RSACryptoServiceProvider csp = null;
            var rsa = (RSACryptoServiceProvider)cert.PrivateKey;
            // Hash the data
            byte[] output = null;
            byte[] data = System.Text.Encoding.UTF8.GetBytes(text);
            switch (type)
            {
                case EncryptType.SHA1:
                    output = rsa.SignHash(data, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                    break;
                case EncryptType.SHA256:
                    output = rsa.SignData(data, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                    break;
            }
            return output;
        }
        bool Verify(string text, byte[] signature, string certPath, EncryptType type)
        {
            // Load the certificate we'll use to verify the signature from a file
            X509Certificate2 cert = new X509Certificate2(certPath);
            // Note:
            // If we want to use the client cert in an ASP.NET app, we may use something like this instead:
            // X509Certificate2 cert = new X509Certificate2(Request.ClientCertificate.Certificate);
            // Get its associated CSP and public key
            RSACryptoServiceProvider csp = (RSACryptoServiceProvider)cert.PublicKey.Key;
            // Hash the data
            bool checkVerify = false;
            byte[] data = System.Text.Encoding.UTF8.GetBytes(text);
            switch (type)
            {
                case EncryptType.SHA1:
                    checkVerify = csp.VerifyData(data, signature, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1);
                    break;
                case EncryptType.SHA256:
                    checkVerify = csp.VerifyData(data, signature, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
                    break;
            }
            return checkVerify;
        }
        public string SignCertificates(string text, string p12Path, string p12Password, EncryptType type, bool isPartner)
        {
            string ciphertext = "";
            try
            {
                byte[] signed = null;
                //UnicodeEncoding encoding = new UnicodeEncoding();
                if(!isPartner)
                    signed = Sign(text, CertificateP12, CertificatePassword, type);
                else
                    signed = Sign(text, p12Path, p12Password, type);
                ciphertext = Convert.ToBase64String(signed, 0, signed.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ciphertext;
        }

        public bool VerifyCertificates(string text, string sign,  string cerPath, EncryptType type, bool isPartner)
        {
            try
            {
                bool check = false;
                if(!isPartner)
                    check = Verify(text, Convert.FromBase64String(sign), certificatePublic, type);
                else
                    check = Verify(text, Convert.FromBase64String(sign), cerPath, type);
                return check;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public EncryptType GetEncryptType(string text)
        {
            EncryptType type = new EncryptType();
            try
            {
                switch (text)
                {
                    case ("SHA256"):
                        type = EncryptType.SHA256;
                        break;
                    case ("SHA1"):
                        type = EncryptType.SHA1;
                        break;
                    case ("SHA2"):
                        type = EncryptType.SHA2;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return type;
        }
        #endregion

        #region SHA
        public string SHA256Encode(string cipherText)
        {
            string output = string.Empty;
            SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
            byte[] data = sha256.ComputeHash(Encoding.UTF8.GetBytes(cipherText));
            StringBuilder sb = new StringBuilder();
            foreach (byte b in data)
            {
                sb.Append(b.ToString("x2"));
            }
            output = sb.ToString();
            return output;
        }
        public string SHA256HmacEncode(string raw, string key)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(key);
            byte[] messageBytes = encoding.GetBytes(raw);
            HMACSHA256 hash = new HMACSHA256(keyByte);
            byte[] hashmessage = hash.ComputeHash(messageBytes);
            string sbinary = "";
            for (int i = 0; i < hashmessage.Length; i++)
            {
                sbinary += hashmessage[i].ToString("X2"); // hex format
            }
            return sbinary;
        }
        public string SHA256HmacBase64(string raw, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);

            // Get bytes from the message
            byte[] messageBytes = Encoding.UTF8.GetBytes(raw);

            // Initialize the HMACSHA256 instance with the key
            using (var hmacsha256 = new HMACSHA256(keyBytes))
            {
                // Compute the hash
                byte[] hashBytes = hmacsha256.ComputeHash(messageBytes);

                // Convert the hash to base64
                string sbinary = Convert.ToBase64String(hashBytes);
                return sbinary;
            }
        }
        #endregion

        #endregion
    }
}
