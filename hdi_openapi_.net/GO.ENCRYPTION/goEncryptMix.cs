﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.ENCRYPTION.goEncryptBase;

namespace GO.ENCRYPTION
{
    public class goEncryptMix
    {
        #region Variable
        #endregion

        #region Contructor
        private static readonly Lazy<goEncryptMix> InitInstance = new Lazy<goEncryptMix>(() => new goEncryptMix());
        public static goEncryptMix Instance => InitInstance.Value;
        #endregion

        #region Method
        public string encryptAesMd5(string plainText)
        {
            try
            {
                KeyIV keyIV = goEncryptBase.GenerateKeyIV();
                KeyIVBase64 keyIVbase64 = new KeyIVBase64
                {
                    IV = Convert.ToBase64String(keyIV.IV),
                    Key = Convert.ToBase64String(keyIV.Key)
                };
                //encrypt by AES
                string encryptedAES = goEncryptBase.Instance.AESEncode(plainText, keyIV.Key, keyIV.IV);
                //continue hash by MD5
                string cipherText = goEncryptBase.Instance.Md5Encode(encryptedAES);
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string encryptMd5HmacSha256(string plainText, string password)
        {
            try
            {
                string encryptedMD5 = goEncryptBase.Instance.Md5Encode(plainText);
                string cipherText = goEncryptBase.Instance.SHA256HmacEncode(encryptedMD5, password);
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string encryptHmacSha256(string plainText, string password)
        {
            try
            {
                //plainText = plainText.Replace("\r\n", "");
                string cipherText = goEncryptBase.Instance.SHA256HmacEncode(plainText, password);
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string base64tHmacSha256(string plainText, string password)
        {
            try
            {
                //plainText = plainText.Replace("\r\n", "");
                string cipherText = goEncryptBase.Instance.SHA256HmacBase64(plainText, password);
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string encryptSha256(string plainText, string password)
        {
            try
            {
                //plainText = plainText.Replace("\r\n", "");
                string cipherText = goEncryptBase.Instance.SHA256Encode(plainText);
                return cipherText;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string encryptHmacSha256Md5(string plainText, string password)
        {
            try
            {
                string cipherText = goEncryptBase.Instance.SHA256HmacEncode(plainText, password);
                string encryptedMD5 = goEncryptBase.Instance.Md5Encode(cipherText);
                return encryptedMD5;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
