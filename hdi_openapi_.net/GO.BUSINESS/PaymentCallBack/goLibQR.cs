﻿using GO.DTO.Base;
using GO.ENCRYPTION;
using GO.SERVICE.CallApi;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using static GO.DTO.Payment.QRCodeModels;

namespace GO.BUSINESS.PaymentCallBack
{
    class goLibQR
    {
        [StringLength(100, MinimumLength = 1, ErrorMessage = " appId không vượt quá 100 ký tự, không được để trống")]
        public string _appId { get; set; }
        [StringLength(25, MinimumLength = 1, ErrorMessage = " merchantName không vượt quá 25 ký tự, không được để trống")]
        public string _merchantName { get; set; }
        [StringLength(20, MinimumLength = 1, ErrorMessage = " serviceCode không vượt quá 20 ký tự, không được để trống")]
        public string _serviceCode { get; set; }
        [StringLength(20, MinimumLength = 1, ErrorMessage = " merchantCode không vượt quá 20 ký tự, không được để trống")]
        public string _merchantCode { get; set; }
        [StringLength(100, MinimumLength = 1, ErrorMessage = " masterMerCode không vượt quá 100 ký tự, không được để trống")]
        public string _masterMerCode { get; set; }
        [StringLength(9, MinimumLength = 1, ErrorMessage = " merchantType không vượt quá 9 ký tự, không được để trống")]
        public string _merchantType { get; set; }
        [StringLength(2, MinimumLength = 1, ErrorMessage = " countryCode không vượt quá 2 ký tự, không được để trống")]
        public string _countryCode { get; set; }
        [StringLength(500, MinimumLength = 1, ErrorMessage = " _secretKey không vượt quá 500 ký tự, không được để trống")]
        public string _secretKey { get; set; }
        [StringLength(500, MinimumLength = 1, ErrorMessage = " url không vượt quá 500 ký tự, không được để trống")]
        public string _Url { get; set; }
        public string _terminalId { get; set; }

        public goLibQR(string appId, string merchantName, string serviceCode, string merchantCode, string masterMerCode,
            string merchantType, string countryCode, string terminalId, string secretKey)
        {
            this._appId = appId;
            this._merchantName = merchantName;
            this._serviceCode = serviceCode;
            this._merchantCode = merchantCode;
            this._masterMerCode = masterMerCode;
            this._merchantType = merchantType;
            this._countryCode = countryCode;
            this._secretKey = secretKey;
            this._terminalId = terminalId;
        }
        public goLibQR()
        {

        }
        /// <summary>
        /// Hàm gọi sang HDbank tạo QR code
        /// </summary>
        /// <param name="_jInput">Json object đầu vào tạo QR</param>
        /// <returns>Trả về danh sách thông tin gửi (MessageId,PartnerId,Telco)</returns>
        public RESULT_QR createQr(string orderId, string amount,string payDate,string desc,
            string tipAndFee, JObject jConfig, JObject jApiConfig)
        {
            try
            {
                CREATE_QR qr = new CREATE_QR();
                qr.appId = jConfig["CLIENT_ID"].ToString();// "HDI";
                qr.masterMerCode = jConfig["MASTER_CODE"].ToString();// "A000000775";
                qr.merchantName = jConfig["CLIENT_NAME"].ToString();// "BH_HDI";
                qr.merchantCode = jConfig["MERCHANT_ID"].ToString();// "HDI";
                qr.merchantType = "";// "";
                qr.terminalId = jConfig["TERMINAL_ID"].ToString();
                qr.serviceCode = jConfig["SERVICE_CODE"].ToString(); //"03";// "03";//mã dịch vụ cho QR               
                qr.countryCode = jConfig["COUNTRY_CODE"].ToString(); //"VN";
                this._secretKey = jConfig["SECRET"].ToString();

                qr.payType = jConfig["PAY_CODE"].ToString(); //"03";//QR sử dụng để thanh toán
                qr.ccy = jConfig["CCY"].ToString(); //"704";
                qr.productId = "";
                qr.txnId = orderId;
                qr.billNumber = orderId;
                qr.amount = amount;
                qr.expDate = payDate;
                qr.desc = desc;
                qr.tipAndFee = tipAndFee;

                qr.checksum = goEncryptBase.Instance.Md5Encode(qr.appId + "|" +
                        qr.merchantName + "|" + qr.serviceCode
                        + "|" + qr.countryCode + "|" +
                        qr.masterMerCode + "|" +
                        qr.merchantType + "|" +
                        qr.merchantCode + "|" + qr.terminalId
                        + "|" + qr.payType + "|" + qr.productId +
                        "|" + qr.txnId + "|" + qr.amount + "|" +
                        qr.tipAndFee + "|" + qr.ccy + "|" +
                        qr.expDate + "|" + this._secretKey);
                JObject oJsonInput = JObject.FromObject(qr);
                HttpResponseMessage rp = callApiQRCode(oJsonInput, jApiConfig);
                RESULT_QR rs = callApi(rp);
                if (rs.code == "00")
                {                    
                    JObject jsonOut = JObject.Parse(rs.message);
                    if (jsonOut["code"].ToString().Equals("00"))
                    {
                        //xử lý data qr trả về                   
                        rs.message = jsonOut["message"].ToString();
                        rs.data = jsonOut["data"].ToString();
                        rs.checkSum = jsonOut["checksum"].ToString();
                        rs.idQrcode = jsonOut["idQrcode"].ToString();
                        //kiểm tra md5
                        string strMD5 = goEncryptBase.Instance.Md5Encode(rs.code + "|" + rs.message + "|" +
                        rs.data + "|" + this._Url + qr.url + "|" + this._secretKey);
                        if (!strMD5.Equals(rs.checkSum))
                        {
                            rs.code = "15";
                            rs.message = "Lỗi check sum dữ liệu HD";
                        }
                    }else
                    {
                        rs.code = jsonOut["code"].ToString();
                    }    
                    
                }
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool callWebPayment(string orderId,string strUrlCallback, JObject jApiConfig)
        {
            try
            {
                
                JObject oJsonInput = new JObject();
                oJsonInput.Add("id", orderId);
                oJsonInput.Add("status", 1);
                oJsonInput.Add("url_callback", strUrlCallback);
                HttpResponseMessage rp = callApiQRCode(oJsonInput, jApiConfig);
                if (rp.IsSuccessStatusCode)
                {
                    string message = rp.Content.ReadAsStringAsync().Result;
                    if (message.IndexOf("true") > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }                
            }
            catch (Exception ex)
            {
                //return false;
                throw new Exception(ex.ToString());
            }
        }
        public static HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Content_type", "text/plain");
                client.DefaultRequestHeaders.Add("User-Agent", "HDI");
                client.DefaultRequestHeaders.Add("Accept", "*/*");
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                //string strConten = "{ \"appId\": \"QrcodeLocalSite\",  \"merchantName\": \"HDI\",  \"serviceCode\": \"03\",  \"countryCode\": \"VN\",  \"merchantCode\": \"0316314482\",  \"masterMerCode\": \"970437\",  \"merchantType\": \"\",  \"terminalId\": \"HDINS\",  \"payType\": \"01\",  \"productId\": \"\",  \"txnId\": \"GCN_0111\",  \"billNumber\": \"\",  \"amount\": \"200000\",  \"ccy\": \"704\",  \"expDate\": \"2011141054\",  \"desc\": \"Thanh toan bao hiem\",  \"tipAndFee\": \"0\",  \"consumerID\": \"\",  \"purpose\": \"\",  \"url\": \"CreateQrcodeApi/createQrcode\", \"checksum\": \"872A10B205446A245BFC0CC518F6FBCA\"}";
                var content = new StringContent(JInput.ToString(), Encoding.UTF8);
                HttpResponseMessage response = client.PostAsync(strUrl, content).Result;
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Service timeout " + ex.ToString());
            }
        }
        private RESULT_QR callApi(string strFunction, JObject JInput)
        {
            RESULT_QR result = new RESULT_QR();
            try
            {
                HttpResponseMessage response = postJsonAPI(JInput, this._Url + strFunction, null);
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (response.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    result.code = "00";
                    result.message = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    result.code = details["code"].ToString();
                    result.message = details["message"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private RESULT_QR callApi(HttpResponseMessage response)
        {
            RESULT_QR result = new RESULT_QR();
            try
            {                
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (response.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    result.code = "00";
                    result.message = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    result.code = details["code"].ToString();
                    result.message = details["message"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public HttpResponseMessage callApiQRCode(JObject jInput, JObject jApi)
        {            
            try
            {
                goServiceConstants.MethodApi method = goServiceConstants.MethodApi.POST;
                switch (jApi["METHOD"].ToString().ToUpper()) 
                {
                    case "GET":
                        {
                            method = goServiceConstants.MethodApi.GET;
                            break;
                        }
                    case "DELETE":
                        {
                            method = goServiceConstants.MethodApi.DELETE;
                            break;
                        }
                    case "PUT":
                        {
                            method = goServiceConstants.MethodApi.PUT;
                            break;
                        }
                }
                JArray jHeader = JArray.Parse(jApi["HEADER"].ToString());
                List<JToken> lsJToken = jHeader.ToList();
                List<goServiceConstants.KeyVal> ls = new List<goServiceConstants.KeyVal>();
                foreach (JToken jT in lsJToken)
                {
                    JObject job = JObject.FromObject(jT);
                    goServiceConstants.KeyVal key = new goServiceConstants.KeyVal();
                    key.key = job["key"].ToString();
                    key.val = job["value"].ToString();
                    ls.Add(key);
                }
                goServiceConstants.TypeBody typebody = goServiceConstants.TypeBody.raw;
                switch (jApi["TYPE_BODY"].ToString().ToUpper())
                {
                    case "FORM_DATA":
                        {
                            typebody = goServiceConstants.TypeBody.form_data;
                            break;
                        }
                    case "FORM_URLENDCODED":
                        {
                            typebody = goServiceConstants.TypeBody.form_urlendcoded;
                            break;
                        }
                    case "NONE":
                        {
                            typebody = goServiceConstants.TypeBody.none;
                            break;
                        }
                }
                goServiceConstants.TypeRaw text = goServiceConstants.TypeRaw.json;
                switch (jApi["TYPE_RAW"].ToString().ToUpper())
                {
                    case "HTML":
                        {
                            text = goServiceConstants.TypeRaw.html;
                            break;
                        }
                    case "JAVASCRIPT":
                        {
                            text = goServiceConstants.TypeRaw.javascript;
                            break;
                        }
                    case "NONE":
                        {
                            text = goServiceConstants.TypeRaw.none;
                            break;
                        }
                    case "TEXT":
                        {
                            text = goServiceConstants.TypeRaw.text;
                            break;
                        }
                    case "XML":
                        {
                            text = goServiceConstants.TypeRaw.xml;
                            break;
                        }
                }
                HttpResponseMessage rp = new goServiceInvoke().Invoke(jApi["URL"].ToString(), method, typebody, text, ls, string.Empty, jInput).Result;
                return rp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
