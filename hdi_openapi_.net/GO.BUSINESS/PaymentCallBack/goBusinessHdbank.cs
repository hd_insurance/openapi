﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DAO.Common;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Partner;
using GO.DTO.Payment;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using MessagingToolkit.QRCode.Codec;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.DTO.Payment.QRCodeModels;

namespace GO.BUSINESS.PaymentCallBack
{
    public class goBusinessHdbank
    {
        #region Variables
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessHdbank> _instance = new Lazy<goBusinessHdbank>(() =>
        {
            return new goBusinessHdbank();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessHdbank Instance { get => _instance.Value; }
        #endregion
        #region Method
        public BaseResponse ipnHdbank(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string secretKey = "";
            string merchantCode = "";
            string spl = "|";
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(obj), Logger.ConcatName(nameClass, LoggerName));
                IpnHdbankModels ipnHdbank = new IpnHdbankModels();
                ipnHdbank = JsonConvert.DeserializeObject<IpnHdbankModels>(obj?.ToString());

                if (ipnHdbank == null)// || ipnHdbank.addData == null
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4001), goConstantsError.Instance.ERROR_4001);
                    return response;
                }
                string raw = ipnHdbank.code + spl + ipnHdbank.msgType + spl + ipnHdbank.txnId + spl + ipnHdbank.qrTrace + spl + ipnHdbank.bankCode +
                        spl + ipnHdbank.mobile + spl + ipnHdbank.accountNo + spl + ipnHdbank.amount + spl + ipnHdbank.payDate + spl + merchantCode + secretKey;
                string checkSum = goEncryptBase.Instance.Md5Encode(raw);
                if (!checkSum.Equals(ipnHdbank.checksum))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4002), goConstantsError.Instance.ERROR_4002);
                    return response;
                }
                // thực thi
                if (UpdateIpnHdBank(ipnHdbank))
                    response.Success = true;
                else
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4003), goConstantsError.Instance.ERROR_4003);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnHdbank", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse createQRCode(BaseRequest request)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
                string environment = "";
                goBusinessCommon.Instance.GetEnvironmentConfig(ref environment);
                object[] Param = new object[] { "HDBANK", "QR", environment, "CREATE" };

                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.Pay_Get_Config, environment, Param);
                response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
                List<JObject> a = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                //JArray arr = JArray.FromObject(a);

                QRCodeModels.CREATE_QR cQR = new QRCodeModels.CREATE_QR();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    cQR = JsonConvert.DeserializeObject<QRCodeModels.CREATE_QR>(request.Data.ToString());

                if (cQR == null)// || ipnHdbank.addData == null
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_4001), goConstantsError.Instance.ERROR_4001);
                    return response;
                }

                //response = createQr(cQR);               

                response = goBusinessCommon.Instance.getResultApi(response.Success, response.Data, config, response.Error, response.ErrorMessage);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public JObject createQr(string orderId, string amount, string payDate, string desc,
            string tipAndFee, JObject jConfig, JObject jApiConfig)
        {
            try
            {
                goLibQR cls = new goLibQR();
                DateTime dt = Convert.ToDateTime(payDate);
                string strDatePay = dt.ToString("yyMMddHHmm");// "2011011000";//thời gian hết hạn thanh toán    yyMMddHHmm            
                QRCodeModels.RESULT_QR rs = new goLibQR().createQr(orderId, amount, strDatePay, desc, tipAndFee, jConfig, jApiConfig);
                if (rs.code == "00")
                {
                    string strQRBase64 = getQrCode(rs.data, "Q", 8, "");
                    JObject obj = new JObject();
                    obj.Add("imageQR", strQRBase64);
                    obj.Add("dataQR", rs.data);
                    obj.Add("txnId", rs.txnId);
                    return obj;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string getQrCode(string strQrData, string strErrorCorrect, int iSize,
            string strPathLogo)
        {
            try
            {
                QRCodeEncoder encoder = new QRCodeEncoder();
                encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                switch (strErrorCorrect)
                {
                    case "L":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                        break;
                    case "M":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                        break;
                    case "Q":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                        break;
                    case "H":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                        break;
                }
                encoder.QRCodeScale = iSize;
                Bitmap bmp = encoder.Encode(strQrData);
                //string strNameQR = "qr_code" + DateTime.Now.ToString("HHss") + ".png";
                //strPathFileImage = pathFile + strNameQR;
                if (!string.IsNullOrEmpty(strPathLogo))
                {
                    Graphics g = Graphics.FromImage(bmp);
                    Bitmap logo = new Bitmap(strPathLogo);
                    g.DrawImage(logo, new Point((bmp.Width - logo.Width) / 2, (bmp.Height - logo.Height) / 2));
                    g.Save();
                    g.Dispose();
                }
                //bmp.Save(strPathFileImage, ImageFormat.Png);
                System.IO.MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Jpeg);
                byte[] byteImage = ms.ToArray();
                return Convert.ToBase64String(byteImage);
                //bmp.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool UpdateIpnHdBank(IpnHdbankModels ipnHdbank)
        {
            try
            {
                string action = goConstantsProcedure.HDBank_Ipn;
                string[] goParam = { ipnHdbank.txnId, ipnHdbank.code, ipnHdbank.amount, ipnHdbank.msgType, "HDBANK" };
                string environment = "";
                string storeProcedure = "";
                goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
                ActionApi apiInfo = goBusinessCommon.Instance.GetActionApi(action);
                goBusinessCommon.Instance.GetEnvironmentConfig(ref environment);
                ActionApiDatabase apiDatabase = goBusinessAction.Instance.GetApiDatabase(action, environment, false);
                if (apiInfo != null)
                    storeProcedure = apiInfo.Pro_Code;
                if (apiDatabase != null && apiDatabase.Data != null)
                {
                    ModeDB = goVariables.Instance.GetDATABASE_TYPE(apiDatabase.Db_Mode);
                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && !string.IsNullOrEmpty(apiDatabase.Packages))
                        storeProcedure = apiDatabase.Schema + "." + apiDatabase.Packages + "." + storeProcedure;

                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && string.IsNullOrEmpty(apiDatabase.Packages))
                        storeProcedure = apiDatabase.Schema + "." + storeProcedure;
                    goExecute.Instance.ExecuteMultiple(ModeDB, apiDatabase.Data, storeProcedure, goParam);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
