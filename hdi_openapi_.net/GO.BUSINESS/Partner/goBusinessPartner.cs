﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.COMMON.Log;
using GO.DTO.Partner;
using GO.COMMON.Cache;
using GO.CONSTANTS;
using GO.BUSINESS.CacheSystem;
using GO.DTO.Common;
using GO.HELPER.Utility;
using GO.HELPER.Config;
using GO.BUSINESS.Common;

namespace GO.BUSINESS.Partner
{
    public class goBusinessPartner
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessPartner> _instance = new Lazy<goBusinessPartner>(() =>
        {
            return new goBusinessPartner();
        });

        public static goBusinessPartner Instance { get => _instance.Value; }
        #endregion

        #region Method
        public PartnerConfigModel GetPartnerConfig(string partnerCode, string secret)
        {
            try
            {
                goInstanceCaching.Instance.InstanceCaching();
                PartnerConfigModel info = new PartnerConfigModel();
                List<PartnerConfigModel> lstData = goBusinessCache.Instance.Get<List<PartnerConfigModel>>(goConstantsRedis.PartnerConfig_Key);

                if (lstData != null && lstData.Any())
                {
                    info = lstData.Where(i => i.Partner_Code == partnerCode && i.Secret == secret).ToList().FirstOrDefault();
                }
                return info;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetPartnerConfig ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                return default(PartnerConfigModel);
            }

        }

        public void ValidPartnerUseApi(ref TokenAuthorizationInfo result, string partnerCode, string apiCode)
        {
            try
            {
                if (string.IsNullOrEmpty(partnerCode) || string.IsNullOrEmpty(apiCode))
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2013);
                }
                goInstanceCaching.Instance.InstanceCaching();
                List<PartnerUseGroupApi> lstData = goBusinessCache.Instance.Get<List<PartnerUseGroupApi>>(goConstantsRedis.PartnerGroupApi_Key);
                List<PartnerUseGroupApi> lstUse = new List<PartnerUseGroupApi>();
                lstUse = lstData.Where(i => i.Api_Code.Equals(apiCode) && i.Partner_Code.Equals(partnerCode)).ToList();
                if (lstUse == null || !lstUse.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2013);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ValidPartnerUseApi ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2013);
            }
        }

        public void ValidPartnerWhitelist(ref TokenAuthorizationInfo result, string partnerCode, string ip, string env)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(partnerCode) || string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(env))
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                    return;
                }
                goInstanceCaching.Instance.InstanceCaching();
                List<PartnerWhiteListModel> lstData = goBusinessCache.Instance.Get<List<PartnerWhiteListModel>>(goConstantsRedis.WhiteList_Key);
                if (lstData == null || !lstData.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                    return;
                }
                List<PartnerWhiteListModel> lst = new List<PartnerWhiteListModel>();
                lst = lstData.Where(i => i.ORG_CODE.Equals(partnerCode) && i.IP.Equals(ip) && i.ENV_CODE.Equals(env)).ToList();
                if (lst == null || !lst.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                    return;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2015);
            }
        }

        public void ValidPartnerWhitelist(ref TokenAuthorizationInfo result, string ip, string env)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(ip) || string.IsNullOrEmpty(env))
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                    return;
                }
                goInstanceCaching.Instance.InstanceCaching();
                List<PartnerWhiteListModel> lstData = goBusinessCache.Instance.Get<List<PartnerWhiteListModel>>(goConstantsRedis.WhiteList_Key);
                if (lstData == null || !lstData.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                    return;
                }
                List<PartnerWhiteListModel> lst = new List<PartnerWhiteListModel>();
                lst = lstData.Where(i => i.IP.Equals(ip) && i.ENV_CODE.Equals(env)).ToList();
                if (lst == null || !lst.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2015);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2015);
            }
        }
        #endregion
    }
}
