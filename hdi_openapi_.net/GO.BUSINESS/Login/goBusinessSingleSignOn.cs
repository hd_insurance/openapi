﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.Token;
using GO.COMMON.Cache;
using GO.COMMON.ErrDB;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.SingleSignOn;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.Login
{
    public class goBusinessSingleSignOn
    {

        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessSingleSignOn> _instance = new Lazy<goBusinessSingleSignOn>(() =>
        {
            return new goBusinessSingleSignOn();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessSingleSignOn Instance { get => _instance.Value; }
        #endregion
        #region Method
        public ResponseLoginSSO GetResponseLogin_SSO(BaseRequest request)
        {
            ResponseLoginSSO response = new ResponseLoginSSO();
            UserSSO UserInfo = new UserSSO();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            string userName = "", passWord = "", channel = "";
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string deviceEnv = request?.Device?.DeviceEnvironment;
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultSSO(UserInfo, false, "", 0, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                request.Process = new ProcessTimeInfo();
                request.Process.RequestTime = DateTime.Now;
                #region 1. get user/pass in request
                getUserPasswordSSO(request, ref channel, ref userName, ref passWord);
                #endregion

                #region 2. check user/pass in enviroment
                // cần chỉnh lại thành check và get từ cache. có edit remove thì update cache

                request.Process.ExecuteTime = DateTime.Now;
                object param = new
                {
                    org_code = request.Action.ParentCode,
                    channel = channel,
                    userName = userName,
                    passWord = passWord
                };
                request.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                BaseResponse responseExe = new BaseResponse();
                responseExe = goBusinessAction.Instance.GetBaseResponse(request);

                if (responseExe.Success)
                {
                    UserInfo = goBusinessCommon.Instance.GetData<UserSSO>(responseExe, 0).FirstOrDefault();
                    response = getInfoTokenSSO(UserInfo, channel, userName, passWord, deviceEnv, config);
                }
                else
                    throw new Exception("fail");
                #endregion
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                string errCode = "";
                string errMess = "";
                goErrDB.Instance.ReadExcep(ex.Message, ref errCode, ref errMess);
                response.Success = false;
                if (!string.IsNullOrEmpty(errCode) && !string.IsNullOrEmpty(errMess))
                {
                    response.Error = errCode;
                    response.ErrorMessage = errMess;
                }
                else
                {
                    response.Error = nameof(goConstantsError.Instance.ERROR_2010);
                    response.ErrorMessage = goConstantsError.Instance.ERROR_2010;
                }
                return response;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
        }

        public void getUserPasswordSSO(BaseRequest request, ref string channel, ref string userName, ref string passWord)
        {
            object[] goParam = null;
            if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

            if (goParam == null && goParam.Length < 3)
            {
                throw new Exception("this parameter does not match");
            }
            channel = goParam[0]?.ToString();
            userName = goParam[1]?.ToString();
            passWord = goParam[2]?.ToString();
        }

        public ResponseLoginSSO getInfoTokenSSO(UserSSO UserInfo, string channel, string userName, string passWord, string DeviceEnvironment, ResponseConfig config)
        {
            ResponseLoginSSO response = new ResponseLoginSSO();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string key = "", token = "";
                int expries = 0;
                key = "SSO_" + channel + userName + config.secret + config.partnerCode + DeviceEnvironment;
                if (goBusinessCache.Instance.Exists(key))
                {
                    token = goBusinessCache.Instance.Get<string>(key);
                    TimeSpan? timeToLive = goBusinessCache.Instance.TimeToLive(key);
                    expries = goUtility.Instance.ConvertToInt32(Math.Floor(timeToLive.Value.TotalSeconds), 0);
                }
                else
                {
                    token = goBusinessToken.Instance.generateTokenSSO(channel, userName, passWord, config.secret);
                    expries = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCacheToken), 0);
                    goBusinessCache.Instance.Add<string>(key, token, expries);
                }
                response = goBusinessCommon.Instance.getResultSSO(UserInfo, true, token, expries, "", config.isSign, config.secret, config.client_id, "", "");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return response;
        }

        
        #endregion
    }
}
