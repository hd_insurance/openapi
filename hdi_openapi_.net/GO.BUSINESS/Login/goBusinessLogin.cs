﻿using GO.BUSINESS.Action;
using GO.BUSINESS.CacheSystem;
using GO.BUSINESS.Common;
using GO.BUSINESS.Partner;
using GO.BUSINESS.Token;
using GO.COMMON.Cache;
using GO.COMMON.ErrDB;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Partner;
using GO.DTO.SystemModels;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.Login
{
    public class goBusinessLogin
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessLogin> _instance = new Lazy<goBusinessLogin>(() =>
        {
            return new goBusinessLogin();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessLogin Instance { get => _instance.Value; }
        #endregion

        #region Method
        public ResponseLogin getResponseLogin(BaseRequest request)
        {
            // 1. get config partner
            // 2. get user/pass in request
            // 3. check user/pass in enviroment
            // 4. generate token + expired
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            ResponseLogin response = new ResponseLogin();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string userName = "", passWord = "";
                bool isSignature = false;
                PartnerConfigModel partnerConfig = new PartnerConfigModel();

                request.Process = new ProcessTimeInfo();
                request.Process.RequestTime = DateTime.Now;
                #region 1. get config partner
                try
                {
                    if (request != null && request.Action != null && request.Action.ParentCode != null && request.Action.Secret != null)
                    {
                        partnerConfig = goBusinessPartner.Instance.GetPartnerConfig(request.Action.ParentCode, request.Action.Secret);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
                #endregion
                #region 2. get user/pass in request
                getUserPassword(request, ref userName, ref passWord);
                #endregion
                #region 3. check user/pass in enviroment
                request.Process.ExecuteTime = DateTime.Now;
                isSignature = goUtility.Instance.CastToBoolean(partnerConfig?.isSignature_Response);
                if (validateUser(userName, passWord, partnerConfig.Enviroment_Code))
                {

                    response = getInfoToken(userName, passWord, partnerConfig.Secret, partnerConfig.Client_ID, isSignature, request?.Device?.DeviceEnvironment);
                }
                else
                {

                    response = goBusinessCommon.Instance.getResultLogin(false, "", 0, "", isSignature, "", "", nameof(goConstantsError.Instance.ERROR_2010), goConstantsError.Instance.ERROR_2010);
                }
                #endregion
                request.Process.ResponseTime = DateTime.Now;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
        }

        public void getUserPassword(BaseRequest request, ref string userName, ref string passWord)
        {
            object[] goParam = null;
            if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

            if (goParam == null && goParam.Length < 2)
            {
                throw new Exception("this parameter does not match");
            }
            userName = goParam[0]?.ToString();
            passWord = goParam[1]?.ToString();
        }

        public bool validateUser(string userName, string passWord, string enviroment)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                goInstanceCaching.Instance.InstanceCaching();
                List<UserInfo> lstData = new List<UserInfo>();
                lstData = goBusinessCache.Instance.Get<List<UserInfo>>(goConstantsRedis.User_Key);

                if (lstData != null && lstData.Any())
                {
                    UserInfo info = lstData.Where(i => i.UserName == userName && i.Password == passWord && i.Enviroment_Code == enviroment).FirstOrDefault();
                    if (info != null && !string.IsNullOrEmpty(info.UserName))
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
            return false;
        }

        public ResponseLogin getInfoToken(string userName, string passWord, string secret, string clientId, bool isSignature, string DeviceEnvironment)
        {
            ResponseLogin response = new ResponseLogin();
            try
            {
                string key = "", token = "";
                int expries = 0;
                key = userName + secret + DeviceEnvironment;
                if (goBusinessCache.Instance.Exists(key))
                {
                    token = goBusinessCache.Instance.Get<string>(key);
                    TimeSpan? timeToLive = goBusinessCache.Instance.TimeToLive(key);
                    expries = goUtility.Instance.ConvertToInt32(Math.Floor(timeToLive.Value.TotalSeconds), 0);
                }
                else
                {
                    token = goBusinessToken.Instance.generateToken(userName, passWord, secret);
                    expries = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCacheToken), 0);
                    goBusinessCache.Instance.Add<string>(key, token, expries);
                }
                response = goBusinessCommon.Instance.getResultLogin(true, token, expries, "", isSignature, secret, clientId, "", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        /// <summary>
        /// Logout api
        /// 1. key = userName + secret + environmentCode(APP/WEB)
        /// 2. if logout all -> key = userName + secret + APP and key = userName + secret + WEB
        /// </summary>
        /// <param name="request"></param>
        /// <param name="isLogoutAll"></param>
        /// <returns></returns>
        public BaseResponse GetResponseLogout(BaseRequest request, bool isLogoutAll)
        {
            BaseResponse baseResponse = new BaseResponse();
            string keyApp = "", keyWeb = "", key = "";
            if (isLogoutAll)
            {
                keyApp = request.Action.UserName + request.Action.Secret + goConstants.DeviceEnvironment.APP;
                keyWeb = request.Action.UserName + request.Action.Secret + goConstants.DeviceEnvironment.WEB;
                goBusinessCache.Instance.Remove(keyApp);
                goBusinessCache.Instance.Remove(keyWeb);
            }
            else
            {
                key = request.Action.UserName + request.Action.Secret + request.Device.DeviceEnvironment;
                goBusinessCache.Instance.Remove(key);
            }
            baseResponse = goBusinessCommon.Instance.getResultApi(true, "", false, "", "", null, null);
            return baseResponse;
        }

        //#region SSO
        //public ResponseLogin GetResponseLogin_SSO(BaseRequest request)
        //{
        //    ResponseLogin response = new ResponseLogin();
        //    AuditLogsInfo auditLogs = new AuditLogsInfo();
        //    ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
        //    string userName = "", passWord = "", channel = "";
        //    string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
        //    try
        //    {
        //        string deviceEnv = request?.Device?.DeviceEnvironment;
        //        if (config == null)
        //        {
        //            response = goBusinessCommon.Instance.getResultLogin(false, "", 0, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
        //            return response;
        //        }
        //        request.Process = new ProcessTimeInfo();
        //        request.Process.RequestTime = DateTime.Now;
        //        auditLogs.request = request;
        //        #region 1. get user/pass in request
        //        getUserPasswordSSO(request,ref channel, ref userName, ref passWord);
        //        #endregion

        //        #region 2. check user/pass in enviroment
        //        request.Process.ExecuteTime = DateTime.Now;
        //        object param = new
        //        {
        //            org_code = request.Action.ParentCode,
        //            channel = channel,
        //            userName = userName,
        //            passWord = passWord
        //        };
        //        request.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
        //        BaseResponse responseExe = new BaseResponse();
        //        responseExe = goBusinessAction.Instance.GetBaseResponse(request);

        //        if (responseExe.Success)
        //            response = getInfoTokenSSO(channel, userName, passWord, deviceEnv, config);
        //        else
        //            throw new Exception("fail");
        //        #endregion
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
        //        string errCode = "";
        //        string errMess = "";
        //        goErrDB.Instance.ReadExcep(ex.Message, ref errCode, ref errMess);
        //        response.Success = false;
        //        if (!string.IsNullOrEmpty(errCode) && !string.IsNullOrEmpty(errMess))
        //        {
        //            response.Error = errCode;
        //            response.ErrorMessage = errMess;
        //        }
        //        else
        //        {
        //            response.Error = nameof(goConstantsError.Instance.ERROR_2010);
        //            response.ErrorMessage = goConstantsError.Instance.ERROR_2010;
        //        }
        //        return response;
        //    }
        //    finally
        //    {
        //        try
        //        {
        //            auditLogs.request = request;
        //            auditLogs.response = response;
        //            new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
        //        }
        //    }
        //}

        //public void getUserPasswordSSO(BaseRequest request, ref string channel, ref string userName, ref string passWord)
        //{
        //    object[] goParam = null;
        //    if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
        //        goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

        //    if (goParam == null && goParam.Length < 3)
        //    {
        //        throw new Exception("this parameter does not match");
        //    }
        //    channel = goParam[0]?.ToString();
        //    userName = goParam[1]?.ToString();
        //    passWord = goParam[2]?.ToString();
        //}

        //public ResponseLogin getInfoTokenSSO(string channel, string userName, string passWord, string DeviceEnvironment, ResponseConfig config)
        //{
        //    ResponseLogin response = new ResponseLogin();
        //    string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
        //    try
        //    {
        //        string key = "", token = "";
        //        int expries = 0;
        //        key = "SSO_" + channel + userName + config.secret + config.partnerCode + DeviceEnvironment;
        //        if (goBussinessCache.Instance.Exists(key))
        //        {
        //            token = goBussinessCache.Instance.Get<string>(key);
        //            TimeSpan? timeToLive = goBussinessCache.Instance.TimeToLive(key);
        //            expries = goUtility.Instance.ConvertToInt32(Math.Floor(timeToLive.Value.TotalSeconds), 0);
        //        }
        //        else
        //        {
        //            token = goBusinessToken.Instance.generateTokenSSO(channel, userName, passWord, config.secret);
        //            expries = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCacheToken), 0);
        //            goBussinessCache.Instance.Add<string>(key, token, expries);
        //        }
        //        response = goBusinessCommon.Instance.getResultLogin(true, token, expries, "", config.isSign, config.secret, config.client_id, "", "");
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
        //        throw ex;
        //    }
        //    return response;
        //}
        //#endregion

        #endregion
    }
}
