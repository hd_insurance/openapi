﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Ecommerce;
using GO.BUSINESS.ModelBusiness.FileModels;
using GO.BUSINESS.Service;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Service;
using GO.DTO.SftpFile;
using GO.ENCRYPTION;
using GO.HELPER.Utility;
using GO.SERVICE.CallApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUglify.JavaScript;
using Org.BouncyCastle.Asn1.Ocsp;
using Renci.SshNet.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace GO.BUSINESS.PartnerFunction
{
    public class goBusinessVietjet
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessVietjet> _instance = new Lazy<goBusinessVietjet>(() =>
        {
            return new goBusinessVietjet();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessVietjet Instance { get => _instance.Value; }
        #endregion
        public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        {
            ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
            return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
        }
        public BaseResponse sendObjectToPartner(JObject request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //Kiểm tra checksum
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["ORG_SELLER"].ToString() + request["PRODUCT_CODE"].ToString() + request["TPA_CODE"].ToString()).ToLower();
                if (!strMD5.ToLower().Equals(request["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                //lấy thông tin json call
                response = callPackage("APIEAVSZ5X", strEvm, new object[] { "sys", "WEB", request["ORG_SELLER"].ToString(), request["PRODUCT_CODE"].ToString(), request["TPA_CODE"].ToString(), "CURRENT" });
                if (response.Success)
                {
                    // lấy dữ liệu cần gửi
                    JArray jarrKetQua = new JArray();
                    List<JObject> lsObjSend = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (lsObjSend != null && lsObjSend.Any())
                    {
                        JObject jRequestSend = new JObject();
                        JArray arrData = new JArray();
                        foreach (JObject objSend in lsObjSend)
                        {
                            //Tạo json theo yêu cầu của đối tác để send
                            JObject jboData = JObject.Parse(objSend["CONTRACT_DATA"].ToString());
                            arrData.Add(jboData);
                            JObject joKetQua = new JObject();
                            joKetQua.Add("ORG_CODE", objSend["ORG_CODE"].ToString());
                            joKetQua.Add("ORG_SELLER", objSend["ORG_SELLER"].ToString());
                            joKetQua.Add("PRODUCT_CODE", objSend["PRODUCT_CODE"].ToString());
                            joKetQua.Add("TPA_CODE", objSend["TPA_CODE"].ToString());
                            joKetQua.Add("CONTRACT_CODE", objSend["CONTRACT_CODE"].ToString());
                            jarrKetQua.Add(joKetQua);
                        }
                        jRequestSend.Add("policies", arrData);
                        //call api đối tác                        
                        switch (request["TPA_CODE"].ToString())
                        {
                            case "PVC":
                                {
                                    var timestampUpdate = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                                    string strUrlIpn = "";
                                    string strParam = goPaymentLib.Instance.getLink_CallBack(request["TPA_CODE"].ToString(), ref strUrlIpn);
                                    if (string.IsNullOrEmpty(strParam))
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lay link call back " + request["TPA_CODE"].ToString(), Logger.ConcatName(nameClass, nameMethod));
                                    }
                                    List<string> lsStrParam = strParam.Split(';').ToList();
                                    string strKey = lsStrParam[0].Replace("aaaA", ";");
                                    JObject jHeader = new JObject();
                                    jHeader.Add("Authorization", strKey);
                                    jHeader.Add("timestamp", timestampUpdate.ToString());
                                    //string strSign = goEncryptMix.Instance.encryptHmacSha256("." + strJson + "." + timestampUpdate.ToString(), strSecret);
                                    //jHeader.Add("x-signature", strSign.ToUpper());
                                    HttpResponseMessage response1 = postJsonAPI(jRequestSend, strUrlIpn, jHeader);
                                    string message = response1.Content.ReadAsStringAsync().Result;
                                    string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                                    JObject jobReturn = JObject.Parse(parsedString);
                                    if (jobReturn["code"].ToString().Equals("0"))
                                    {
                                        //thực hiện update dữ liệu
                                        response = callPackage("APIE8T5SSA", strEvm, new object[] { "sys", "WEB", request["ORG_SELLER"].ToString(), request["PRODUCT_CODE"].ToString(), request["TPA_CODE"].ToString(), "DONE", jarrKetQua });
                                        if (response.Success)
                                        {
                                            //goBussinessEmail.Instance.sendEmaiCMS("", "cap nhat pvc contract tpa thành công ", JsonConvert.SerializeObject(response).ToString());
                                        }
                                        else
                                        {
                                            goBussinessEmail.Instance.sendEmaiCMS("", "cap nhat pvc contract tpa lỗi ", JsonConvert.SerializeObject(response).ToString());
                                        }
                                    }
                                    else
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Loi gui contract tpa " + jobReturn["message"].ToString(), Logger.ConcatName(nameClass, nameMethod));
                                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi gui contract tpa ", parsedString);
                                        response.Success = false;
                                        response.ErrorMessage = parsedString;
                                    }
                                    break;
                                }
                            case "SAS":
                                {
                                    foreach (JObject objSend in lsObjSend)
                                    {
                                        jarrKetQua = new JArray();
                                        JObject joKetQua = new JObject();
                                        joKetQua.Add("ORG_CODE", objSend["ORG_CODE"].ToString());
                                        joKetQua.Add("ORG_SELLER", objSend["ORG_SELLER"].ToString());
                                        joKetQua.Add("PRODUCT_CODE", objSend["PRODUCT_CODE"].ToString());
                                        joKetQua.Add("TPA_CODE", objSend["TPA_CODE"].ToString());
                                        joKetQua.Add("CONTRACT_CODE", objSend["CONTRACT_CODE"].ToString());
                                        jarrKetQua.Add(joKetQua);
                                        //Tạo json theo yêu cầu của đối tác để send
                                        JObject jboData = JObject.Parse(objSend["CONTRACT_DATA"].ToString());
                                        string strUrlIpn = "";
                                        string strParam = goPaymentLib.Instance.getLink_CallBack(request["TPA_CODE"].ToString(), ref strUrlIpn);
                                        if (string.IsNullOrEmpty(strParam))
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lay link call back tpa" + request["TPA_CODE"].ToString(), Logger.ConcatName(nameClass, nameMethod));
                                        }
                                        List<string> lsStrParam = strParam.Split(';').ToList();
                                        string strUser = lsStrParam[0].Replace("aaaA", ";");
                                        string strPass = lsStrParam[1].Replace("aaaA", ";");
                                        string strAppID = lsStrParam[2].Replace("aaaA", ";");
                                        //login hệ thống SAS
                                        JObject jHeader = new JObject();
                                        jHeader.Add("X-Parse-Application-Id", strAppID);
                                        JObject jobjLogin = new JObject();
                                        jobjLogin.Add("username", strUser);
                                        jobjLogin.Add("password", strPass);
                                        HttpResponseMessage responseLogin = postJsonAPI(jobjLogin, strUrlIpn + "parse/login", jHeader);
                                        string strResLg = Regex.Unescape(responseLogin.Content.ReadAsStringAsync().Result.Replace("\\\"", ""));
                                        JObject jobLogin = JObject.Parse(strResLg);
                                        string strToken = jobLogin["sessionToken"]?.ToString();
                                        if (string.IsNullOrEmpty(strToken))
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Loi gui contract tpa " + strResLg, Logger.ConcatName(nameClass, nameMethod));
                                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi gui contract tpa ", strUser + strPass + strAppID + strResLg);
                                            response.Success = false;
                                            response.ErrorMessage = strResLg;
                                            return response;
                                        }
                                        //Thực hiện đẩy dữ liệu
                                        JObject jHeader2 = new JObject();
                                        jHeader2.Add("X-Parse-Application-Id", strAppID);
                                        jHeader2.Add("X-Parse-Session-Token", strToken);

                                        HttpResponseMessage response1 = postJsonAPI(jboData, strUrlIpn + "parse/functions/uploadDataHDI", jHeader2);
                                        string message = response1.Content.ReadAsStringAsync().Result;
                                        string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                                        JObject jobReturn = JObject.Parse(parsedString);
                                        string strKq = jobReturn["result"]?.ToString();
                                        if (string.IsNullOrEmpty(strKq) || strKq.IndexOf("true") < 1)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Loi gui contract tpa " + message, Logger.ConcatName(nameClass, nameMethod));
                                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi gui contract tpa ", strUser + strPass + strAppID + message);
                                            response.Success = false;
                                            response.ErrorMessage = message;
                                            return response;
                                        }
                                        else
                                        {
                                            response = callPackage("APIE8T5SSA", strEvm, new object[] { "sys", "WEB", request["ORG_SELLER"].ToString(), request["PRODUCT_CODE"].ToString(), request["TPA_CODE"].ToString(), "DONE", jarrKetQua });
                                            if (response.Success)
                                            {
                                                //goBussinessEmail.Instance.sendEmaiCMS("", "cap nhat pvc contract tpa thành công ", JsonConvert.SerializeObject(response).ToString());
                                            }
                                            else
                                            {
                                                goBussinessEmail.Instance.sendEmaiCMS("", "cap nhat pvc contract tpa lỗi ", JsonConvert.SerializeObject(response).ToString());
                                            }
                                        }
                                    }

                                    break;
                                }
                        }
                    }
                    return response;
                }
                else
                {
                    //response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                    response.ErrorMessage = "Lỗi PKG lấy dữ liệu";
                    goBussinessEmail.Instance.sendEmaiCMS("", "loi ", JsonConvert.SerializeObject(request).ToString());
                    return response;
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi cập nhật onpoint " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt movi ", ex.Message);
                return response;
                //throw;
            }
        }

        public BaseResponse sendEmailtoBVietInsurance(JObject request)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                /*
                 * request Format
                 * {"MD5":"MD5 data", "ORG_SELLER":"", "PRODUCT_CODE" : "", "EXPORT_DATE":""}
                 * */
                //Kiểm tra checksum
                String strOrgCode = request["ORG_SELLER"].ToString();
                String strProductCode = request["PRODUCT_CODE"].ToString();
                String strExportDate = request["EXPORT_DATE"].ToString();
                String strFlighID = request["FLIGHT_ID"].ToString();

                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + strOrgCode + strProductCode + strExportDate).ToLower();

                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet " + strMD5 + request.ToString(), Logger.ConcatName(nameClass, nameMethod));
                if (!strMD5.ToLower().Equals(request["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                JObject jRequestParam = new JObject();
                jRequestParam.Add("A7", strExportDate);
                jRequestParam.Add("A12", strFlighID);
                //lấy thông tin json call
                object[] param = new object[] { "sys", "WEB", strOrgCode, strProductCode, jRequestParam };
                response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.BV_EXPORT_DATA, strEvm, param);
               
                if (response.Success)
                {
                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data" + JsonConvert.SerializeObject(goBusinessCommon.Instance.GetData<JObject>(response, 0)).ToString(), Logger.ConcatName(nameClass, nameMethod));

                    JObject jHeader = new JObject();
                    JObject jRequestSend = new JObject();
                    JArray jarrRequestSend = new JArray();
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data 1" + JsonConvert.SerializeObject(jDataExport).ToString(), Logger.ConcatName(nameClass, nameMethod));

                    List<JObject> jReplace = goBusinessCommon.Instance.GetData<JObject>(response, 1);

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data 2" + JsonConvert.SerializeObject(jReplace).ToString(), Logger.ConcatName(nameClass, nameMethod));                   

                    

                    List<String> lstReplace = jReplace[0].GetValue("LST_REPLACE").ToString().Split(';').ToList();
                    String urlExcel = jReplace[0].GetValue("XLSX_URL").ToString();
                    String emailto = jReplace[0].GetValue("EMAIL_TO").ToString();
                    String emailcc = jReplace[0].GetValue("EMAIL_CC").ToString();
                    String emailbcc = jReplace[0].GetValue("EMAIL_BCC").ToString();
                    String fileTemplate = jReplace[0].GetValue("FILE_TEMPLATE").ToString();
                    foreach (JObject jsonData in jDataExport)
                    {
                        //JObject jRequestSendParamádsd = goBussinessPayment.Instance.mergJobject(jsonData, lstReplace);
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data 3" + jsonData.ToString(), Logger.ConcatName(nameClass, nameMethod));

                        JObject jRequestSendParam = new JObject();
                        foreach(String strReplace in  lstReplace)
                        {
                            jRequestSendParam.Add(strReplace, jsonData.GetValue(strReplace));
                        }                            

                        jarrRequestSend.Add(jRequestSendParam);                        
                    }

                    jRequestSend.Add("data", jarrRequestSend);

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet JsonData" + jRequestSend, Logger.ConcatName(nameClass, nameMethod));

                    HttpResponseMessage responseExport = postJsonAPI(jRequestSend, urlExcel, jHeader);
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Exxport" + responseExport.Content, Logger.ConcatName(nameClass, nameMethod));

                    if (responseExport.IsSuccessStatusCode)
                    {
                        var pathServer = HttpContext.Current.Server.MapPath("~/TempFile");
                        var fileName = fileTemplate + "_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + ".xlsx";
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet File" + pathServer + fileName, Logger.ConcatName(nameClass, nameMethod));

                        String pathFile = @pathServer + "/" + fileName;

                        goUtility.Instance.Base64ToFile(System.Convert.ToBase64String(responseExport.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult()), pathFile);

                        // lấy dữ liệu cần gửi
                        JArray jarrKetQua = new JArray();
                        List<JObject> lsObjSend = goBusinessCommon.Instance.GetData<JObject>(response, 0);

                        JArray files = new JArray();
                        JObject jsonFile = new JObject();
                        jsonFile.Add("name", fileName);
                        jsonFile.Add("path", System.Convert.ToBase64String(responseExport.Content.ReadAsByteArrayAsync().GetAwaiter().GetResult()));
                        files.Add(jsonFile);

                        JObject jsonDataSendEmail = new JObject();
                        jsonDataSendEmail.Add("ORG_CODE", strOrgCode);
                        jsonDataSendEmail.Add("TEMP_CODE", "");
                        jsonDataSendEmail.Add("PRODUCT_CODE", strProductCode);
                        jsonDataSendEmail.Add("TYPE", "");
                        jsonDataSendEmail.Add("TO", emailto);
                        jsonDataSendEmail.Add("CC", emailcc);
                        jsonDataSendEmail.Add("BCC", emailbcc);
                        jsonDataSendEmail.Add("file", files);
                        jsonDataSendEmail.Add("ACCOUNT", "");

                        JArray arrDataSendEmail = new JArray();
                        arrDataSendEmail.Add(jsonDataSendEmail);

                        JObject jsonSenđata = new JObject();
                        jsonSenđata.Add("TEMP", arrDataSendEmail);
                        foreach (String strReplace in lstReplace)
                        {
                            jsonSenđata.Add(strReplace, jReplace[0].GetValue(strReplace));
                        }

                        //TempConfigModel template = goBusinessServices.Instance.get_all_template(strOrgCode, "STRUCT_HDI", strProductCode, "null",
                        //    goConstantsProcedure.TEMP_CODE_TBAO_BVIET, "HDI_TB", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                        if (lsObjSend != null && lsObjSend.Any())
                        {
                            //TempEmailModel itemEmail = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL" && (m.TYPE_SEND == nameof(TYPE_SEND.ALLONE) || m.TYPE_SEND == nameof(TYPE_SEND.ALL))).FirstOrDefault();

                            //goBusinessServices.Instance.sendMailWithJson();
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Sendemail" + jsonSenđata.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            response = resLog = goBusinessServices.Instance.sendMailWithJson(jsonSenđata);
                            //call Send Email (Attach File) đối tác                        
                        }
                    }
                    
                    
                    return response;
                }
                else
                {
                    //response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                    response.ErrorMessage = "Lỗi PKG lấy dữ liệu";
                    goBussinessEmail.Instance.sendEmaiCMS("", "loi ", JsonConvert.SerializeObject(request).ToString());
                    return response;
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi cập nhật BaoViet " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt BaoViet ", ex.Message);
                return response;
                //throw;
            }
        }

        public BaseResponse synDataltoBVietInsurance(JObject request)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                /*
                 * request Format
                 * {"MD5":"MD5 data", "ORG_SELLER":"", "PRODUCT_CODE" : "", "EXPORT_DATE":""}
                 * */
                //Kiểm tra checksum                                
                String strExportDate = request["EXPORT_DATE"].ToString();
                String strFlighID = request["FLIGHT_ID"].ToString();
                String strProductCode = request["PRODUCT_CODE"].ToString();

                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + strProductCode + strExportDate + strFlighID).ToLower();

                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet " + strMD5 + request.ToString(), Logger.ConcatName(nameClass, nameMethod));
                if (!strMD5.ToLower().Equals(request["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                
                //lấy thông tin json call
                object[] param = new object[] { strFlighID, strProductCode, strExportDate };
                response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.BV_SYN_DATA, strEvm, param);

                if (response.Success)
                {
                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data" + JsonConvert.SerializeObject(goBusinessCommon.Instance.GetData<JObject>(response, 0)).ToString(), Logger.ConcatName(nameClass, nameMethod));

                    JObject jHeader = new JObject();
                    JObject jRequestSend = new JObject();
                    JArray jarrRequestSend = new JArray();
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);

                    List<JObject> jDataConfig = goBusinessCommon.Instance.GetData<JObject>(response, 1);

                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Bao viet Data 1" + JsonConvert.SerializeObject(jDataExport).ToString(), Logger.ConcatName(nameClass, nameMethod));                                        

                    String url_Login = "";
                    String User = "";
                    String Pass = "";

                    String url_Syn = "";

                    if (jDataConfig !=null && jDataConfig.Count > 0)
                    {
                        String strLogin = jDataConfig[0]["LOGIN"].ToString();
                        String[] arrLogin = strLogin.Split(';');
                        url_Login = arrLogin[0];
                        User = arrLogin[1].Replace("aaaA", "");
                        Pass = arrLogin[2].Replace("aaaA", "");

                        url_Syn = jDataConfig[0]["SYN"].ToString();
                       
                        //login hệ thống Bao Viet                        
                        JObject jobjLogin = new JObject();
                        jobjLogin.Add("username", User);
                        jobjLogin.Add("password", Pass);                                                
                        HttpResponseMessage responseLogin = postJsonAPI(jobjLogin, url_Login, jHeader);
                        string strResLg = Regex.Unescape(responseLogin.Content.ReadAsStringAsync().Result.Replace("\\\"", ""));
                        
                        JObject jobLogin = JObject.Parse(strResLg);
                        string strToken = jobLogin["id_token"]?.ToString();
                        bool bToken = false;
                        //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Dang nhap Bao Viet " + strResLg + "Token " + strToken, Logger.ConcatName(nameClass, nameMethod));
                        if (string.IsNullOrEmpty(strToken))
                        {
                            //Doi 30s recall lai login
                            for (int i = 0; i < 2; i++)
                            {
                                if (!bToken)
                                {
                                    Thread.Sleep(1800);
                                    responseLogin = postJsonAPI(jobjLogin, url_Login, jHeader);
                                    strResLg = Regex.Unescape(responseLogin.Content.ReadAsStringAsync().Result.Replace("\\\"", ""));

                                    jobLogin = JObject.Parse(strResLg);
                                    strToken = jobLogin["id_token"]?.ToString();
                                    if (!string.IsNullOrEmpty(strToken))
                                    {
                                        bToken = true;
                                        break;
                                    }                                       
                                }
                            }
                        }
                        if (string.IsNullOrEmpty(strToken))
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Lỗi không login được hệ thống " + strResLg, Logger.ConcatName(nameClass, nameMethod));                            
                            response.Success = false;
                            response.ErrorMessage = strResLg;
                            return response;
                        }
                        //Thực hiện đẩy dữ liệu                                                
                        JObject jHeaderData = new JObject();
                        jHeaderData.Add("Authorization", "Bearer " + strToken);                        

                        if(jDataExport != null && jDataExport.Count > 0)
                        {
                            foreach(JObject objData in jDataExport)
                            {                                                           
                                HttpResponseMessage response1 = postJsonAPI(JObject.Parse(objData["DATA"].ToString()), url_Syn, jHeaderData);
                                string message = response1.Content.ReadAsStringAsync().Result;
                                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                                JObject jobReturn = JObject.Parse(parsedString);
                                string strKq = jobReturn["result"]?.ToString();

                                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Đồng bộ chuyến bay " + objData["FLI_ORG_ID"].ToString() + " " + strKq + "content " + message, Logger.ConcatName(nameClass, nameMethod));
                            }    
                            
                        }    

                        
                    }     

                    return response;
                }
                else
                {
                    //response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                    response.ErrorMessage = "Lỗi PKG lấy dữ liệu BaoViet";
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, response.ErrorMessage, Logger.ConcatName(nameClass, nameMethod));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "loi ", JsonConvert.SerializeObject(request).ToString());
                    return response;
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi cập nhật BaoViet " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt BaoViet ", ex.Message);
                return response;
                //throw;
            }
        }       

        public BaseResponse syn_data_core(JObject request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //Kiểm tra checksum
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["COUNT"].ToString()).ToLower();
                if (!strMD5.ToLower().Equals(request["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                response = callPackage("API9CRZURS", strEvm, new object[] { "sys", "WEB", request["COUNT"].ToString() });
                response.Data = Base64Encode(response.Data);
                return response;

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lấy dữ liệu đồng bộ " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi lấy dữ liệu đồng bộ ", ex.Message);
                return response;
                //throw;
            }
        }
        public static string Base64Encode(object obj)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public BaseResponse update_syn_data_core(JObject request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //Kiểm tra checksum
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["CONTRACT_CODE"].ToString() + request["CONTRACT_CORE"].ToString()).ToLower();
                if (!strMD5.ToLower().Equals(request["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                response = callPackage("APIYAJAK1R", strEvm, new object[] { "sys", "WEB", request["STATUS"].ToString(), request["CONTRACT_CODE"].ToString(), request["MESSAGE"].ToString(), request["CONTRACT_CORE"].ToString(), request["SO_ID"].ToString() });
                return response;

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi update đồng bộ " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update đồng bộ ", ex.Message);
                return response;
                //throw;
            }
        }

        public BaseResponse get_gcn_core(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                string OrgCode = config.partnerCode;
                string strContract = "";
                string strSoID = "";
                JObject jInput = JObject.Parse(request.Data.ToString());
                strContract = jInput["CONTRACT_CODE"].ToString();
                strSoID = jInput["SO_ID"].ToString();
                response = callPackage("APIHSN6X7O", strEvm, new object[] { "sys", "WEB", strSoID, strContract });
                return response;

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi update đồng bộ " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update đồng bộ ", ex.Message);
                return response;
                //throw;
            }
        }
        public HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PostAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API call pvc ", ex.Message + JInput.ToString() + strUrl + "Lỗi" + ex.Message);
                throw new Exception("Service timeout " + ex.ToString());
            }

        }

    }
}
