﻿using GO.HELPER.Config;
using GO.HELPER.Utility;
using GO.SERVICE.CallApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GO.DTO.Centech.Login;
using GO.DTO.Centech.Loans;
using GO.DTO.Insurance;
using GO.DTO.Common;
using GO.DTO.Centech;
using GO.COMMON.Log;
using GO.BUSINESS.Common;
using GO.CONSTANTS;

namespace GO.BUSINESS.PartnerFunction
{
    public class goBusinessCentech
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;

        private static string url_cent = string.Empty;
        private static string url_form_selling = string.Empty;
        private static string pathLogin_cent = string.Empty;
        private static string pathGetContract_cent = string.Empty;
        private static string pathAddContract_cent = string.Empty;
        private static string pathIpn_cent = string.Empty;
        private static string email_cent = string.Empty;
        private static string pass_cent = string.Empty;
        private static string tra_kq_cent = string.Empty;

        private static string userName_cent = string.Empty;
        private static string password_cent = string.Empty;
        private static string secretKey_cent = string.Empty;
        private static string channel_cent = string.Empty;
        private static string branchUnit_cent = string.Empty;
        private static string blockCode_cent = string.Empty;
        private static string code_success_cent = string.Empty;

        public static string dvi_sl_cent = string.Empty;
        public static string kenh_cent = string.Empty; // trường kenh luôn luôn là HDI
        public static string ma_cn_cent = string.Empty;
        #endregion

        #region priorities
        goBusinessCentech()
        {
            url_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUrl);
            url_form_selling = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUrl);//
            pathLogin_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathLogin);
            pathGetContract_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathGetContract);

            email_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentEmail);
            pass_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPass);
            tra_kq_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentTraKq);
            userName_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUserName);
            password_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPassWord);
            secretKey_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentSecret);
            channel_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentChannel);
            branchUnit_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentBranchUnit);
            blockCode_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentBlockCode);
            code_success_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentCodeSuccess);
            pathAddContract_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathAddContract);
            dvi_sl_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentDvsl);
            kenh_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentKenh);
            ma_cn_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentMaCn);
            pathIpn_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathIpn);
        }
        #endregion

        #region Contructor
        private static readonly Lazy<goBusinessCentech> _instance = new Lazy<goBusinessCentech>(() =>
        {
            return new goBusinessCentech();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessCentech Instance { get => _instance.Value; }
        #endregion

        #region Method
        public void configCentech()
        {
            url_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUrl);
            url_form_selling = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUrl);//
            pathLogin_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathLogin);
            pathGetContract_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathGetContract);

            email_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentEmail);
            pass_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPass);
            tra_kq_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentTraKq);
            userName_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentUserName);
            password_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPassWord);
            secretKey_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentSecret);
            channel_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentChannel);
            branchUnit_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentBranchUnit);
            blockCode_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentBlockCode);
            code_success_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentCodeSuccess);
            pathAddContract_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathAddContract);
            dvi_sl_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentDvsl);
            kenh_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentKenh);
            ma_cn_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentMaCn);
            pathIpn_cent = goBusinessCommon.Instance.GetConfigDB(goConstants.CentPathIpn);
        }
        public string GetTokenLogin()
        {
            try
            {
                string url = url_cent + pathLogin_cent;
                string token = "";
                object json = new
                {
                    data = "{\"email\":\"" + email_cent + "\",\"pas\":\"" + pass_cent + "\",\"tra_kq\":\"" + tra_kq_cent + "\"}"
                };
                var strJson = JsonConvert.SerializeObject(json);
                HttpResponseMessage response = new goServiceInvoke().Invoke(url, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw,
                        goServiceConstants.TypeRaw.json, null, string.Empty, strJson).Result;
                string rs = response.Content.ReadAsStringAsync().Result;
                CentResponseLogin resToken = JsonConvert.DeserializeObject<CentResponseLogin>(rs);
                if (resToken != null && resToken.code.Equals(code_success_cent))
                    token = resToken.data?.token;
                return token;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreateContract(ref ContractLoansResponse contractLoansResponse, ref ContractLoans contractLoans, object body, string token)
        {
            try
            {
                string url = url_cent + pathAddContract_cent;
                List<goServiceConstants.KeyVal> lstHeader = new List<goServiceConstants.KeyVal>();
                lstHeader.Add(new goServiceConstants.KeyVal { key = "Authorization", val = "Bearer " + token });
                var strJson = JsonConvert.SerializeObject(body);
                HttpResponseMessage response = new goServiceInvoke().Invoke(url, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw,
                        goServiceConstants.TypeRaw.json, lstHeader, string.Empty, strJson).Result;
                string rs = response.Content.ReadAsStringAsync().Result;

                CentLoansResponse centLoansResponse = new CentLoansResponse();
                if (!string.IsNullOrEmpty(rs))
                    centLoansResponse = JsonConvert.DeserializeObject<CentLoansResponse>(rs);

                if (centLoansResponse != null && centLoansResponse.code.Equals(code_success_cent))
                {
                    contractLoansResponse.url = url_form_selling + centLoansResponse.data?.iurl;
                    KeyValModel keyVal = new KeyValModel();
                    keyVal.Key = "Authorization";
                    keyVal.Val = "Bearer " + centLoansResponse.data?.token;
                    List<KeyValModel> lstKeyVal = new List<KeyValModel>();
                    lstKeyVal.Add(keyVal);
                    contractLoansResponse.header = lstKeyVal;
                    contractLoans.so_id = centLoansResponse.data.so_id;
                    contractLoans.so_hd = centLoansResponse.data.so_hd;
                }
                else
                {
                    contractLoansResponse = null;
                    contractLoans = null;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetContractById(object body, string token)
        {
            try
            {
                string url = url_cent + pathGetContract_cent;
                List<goServiceConstants.KeyVal> lstHeader = new List<goServiceConstants.KeyVal>();
                lstHeader.Add(new goServiceConstants.KeyVal { key = "Authorization", val = "Bearer " + token });
                var strJson = JsonConvert.SerializeObject(body);
                HttpResponseMessage response = new goServiceInvoke().Invoke(url, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw,
                            goServiceConstants.TypeRaw.json, lstHeader, string.Empty, strJson).Result;
                string rs = response.Content.ReadAsStringAsync().Result;
                return rs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Ipn(IpnCent ipnInfo)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string token = GetTokenLogin();
                string url = url_cent + pathIpn_cent;
                ipnInfo.dvi_sl = dvi_sl_cent;
                var strJson = JsonConvert.SerializeObject(paramIpn(ipnInfo));
                List<goServiceConstants.KeyVal> lstHeader = new List<goServiceConstants.KeyVal>();
                lstHeader.Add(new goServiceConstants.KeyVal { key = "Authorization", val = "Bearer " + token });
                HttpResponseMessage response = new goServiceInvoke().Invoke(url, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw,
                            goServiceConstants.TypeRaw.json, lstHeader, string.Empty, strJson).Result;
                string rs = response.Content.ReadAsStringAsync().Result;
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                if(ipnInfo != null)
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(ipnInfo), Logger.ConcatName(nameClass, nameMethod));
                else
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "ipnInfo null", Logger.ConcatName(nameClass, nameMethod));
                return false;
            }
            finally
            {
                // logs ipn
            }
        }

        public object paramIpn(IpnCent ipnInfo)
        {
            try
            {
                return new { data = "{\"dvi_sl\":\"" + ipnInfo.dvi_sl + "\",\"so_id\":\"" + ipnInfo.so_id + "\",\"tien\":\"" + ipnInfo.tien + "\"}" };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
