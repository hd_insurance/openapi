﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Partner;
using GO.DTO.Service;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using GO.DTO.SystemModels;
using Newtonsoft.Json.Linq;
using GO.DTO.Common;
using GO.BUSINESS.ServiceAccess;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.SERVICE.Sign;
using GO.DTO.Insurance;
using GO.BUSINESS.Action;
using System.Threading;

namespace GO.BUSINESS.Service
{
    public class goBussinessSign
    {
        #region Variables
        private string nameClass = nameof(goBussinessSign);
        public static string pathRoot = goUtility.Instance.GetPathProject();
        public string strPathSign = pathRoot + @"/UploadCA/";
        public string strPathTemplate = pathRoot + @"/Template/";

        private string pathFont = pathRoot + @"/SignCA/fonts/font.ttf";
        private static string pathCer = pathRoot + @"/SignCA/hdi.cer";
        private static string pathP12 = pathRoot + @"/SignCA/myca.p12";
        private static List<string> passP12 = new List<string> { "5004E3CDBB045FC6D4BF1698866D4B55", "2B9AF9C54FE1F462364FA6E72D5D07B8" };
        #endregion
        #region Priorities
        public goBussinessSign()
        {
            goUtility.Instance.CreateFolder(strPathSign);
            if (!goUtility.Instance.ExistsFile(pathFont))
            {
                throw new Exception("Chưa có font arial trong thư mục ");
            }
        }
        #endregion
        #region Contructor
        private static readonly Lazy<goBussinessSign> InitInstance = new Lazy<goBussinessSign>(() => new goBussinessSign());
        public static goBussinessSign Instance => InitInstance.Value;
        #endregion

        #region Method
        public List<SignFileReturnModel> signGCNDT(TempSignModel tempConfigSign, List<SignFileReturnModel> lsFilePDf, bool isUpload, bool isBase64, bool isBackDate, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                List<SignFileReturnModel> lsFilePDfReturn = new List<SignFileReturnModel>();
                byte[] p12Byte = Convert.FromBase64String(tempConfigSign.DATA_FILE);
                int iSangTraiX = Int16.Parse(tempConfigSign.X);
                int iXuongDuoiY = Int16.Parse(tempConfigSign.Y);
                string strTypeSign = "IMAGE";
                string image_sign = tempConfigSign.M_IMAGE;
                switch (tempConfigSign.IMAGE)
                {
                    case "S":
                        image_sign = tempConfigSign.S_IMAGE;
                        break;
                    case "F":
                        image_sign = tempConfigSign.F_IMAGE;
                        break;
                }
                string strCerBasse64 = tempConfigSign.CER_FILE;
                LibSign clsSign = new LibSign(pathRoot);
                JArray jarrOut = new JArray();
                foreach (SignFileReturnModel fileInput in lsFilePDf)
                {
                    SignFileReturnModel fileReturn = fileInput;
                    float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                    int ipage = 0;
                    bool check = false;
                    check = clsSign.findTextInPdfBatch(tempConfigSign.TEXT_CA, Convert.FromBase64String(fileInput.FILE_BASE64), ref x1, ref y1, ref width1, ref ipage);
                    SignFileModel infoSign = new SignFileModel();
                    infoSign.detail = new SignFileDetail();
                    infoSign.PROVIDER = "HDI";
                    infoSign.DEVICE = "HDI";
                    infoSign.CER = strCerBasse64;
                    infoSign.FILE_SIGN = fileInput.FILE_BASE64;
                    infoSign.F12_FILE = p12Byte;
                    infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                    infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                    infoSign.detail.WIDTH = ((Int16)width1 + 2 * iSangTraiX).ToString();
                    infoSign.detail.HEIGHT = (height1 + iSangTraiX).ToString();
                    infoSign.detail.CONTACT = "";
                    infoSign.detail.DISPLAY_TYPE = strTypeSign;
                    infoSign.detail.IMAGE = image_sign;
                    infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
                    infoSign.detail.MESSAGE = "";
                    infoSign.detail.PAGE = ipage.ToString();
                    infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                    infoSign.detail.FIELD_NAME = fileInput.FILE_REF;
                    JObject jOutput = new JObject();
                    if (check)
                    {
                        //thực hiện ký số
                        string strFileKy = "";
                        if (isBackDate && fileInput.dtSignBack != null)
                        {
                            DateTime dt = (DateTime) fileInput.dtSignBack;
                            strFileKy = clsSign.signPDFBack(infoSign, tempConfigSign.PASSWORD, "", dt);
                        }
                        else
                            strFileKy = clsSign.signPDF(infoSign, tempConfigSign.PASSWORD, "");
                        if (string.IsNullOrEmpty(strFileKy))
                        {
                            fileReturn.Success = false;
                            fileReturn.MESSAGE = "Không thực hiện được ký số";
                            return null;
                        }
                        string strNameFile = "";
                        string strUrl = "";
                        bool bUpload = false;
                        byte[] fileSigned = Convert.FromBase64String(strFileKy);
                        if (isUpload)
                        {
                            //thực hiện đẩy file ký lên server                            
                            BaseRequest request = new BaseRequest();
                            request.Action = new ActionInfo();
                            bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, fileInput.FILE_REF + ".pdf", "",
                                                                        request, ref strNameFile, ref strUrl, fileInput.FILE_ID);
                            for(int i = 0; i < 3; i++)
                            {
                                if (!bUpload)
                                {
                                    Thread.Sleep(5000);
                                    bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, fileInput.FILE_REF + ".pdf", "",
                                                                        request, ref strNameFile, ref strUrl, fileInput.FILE_ID);
                                    if (bUpload)
                                        break;
                                }
                                else
                                    break;                                
                            }
                            if (!bUpload)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                                return null;
                            }
                        }
                        if (isBase64)
                        {
                            fileReturn.FILE_BASE64 = strFileKy;
                            jOutput.Add("FILE_SIGN", "DATA_JSON");
                        }
                        else
                        {
                            fileReturn.FILE_BASE64 = "";
                            jOutput.Add("FILE_SIGN", "UPLOAD");
                        }
                        if(!string.IsNullOrEmpty(strUrl))
                            fileReturn.FILE_URL = strUrl;
                        else
                            fileReturn.FILE_BASE64 = strFileKy;
                        jOutput.Add("FILE_SIGNED", fileReturn.FILE_ID);
                        jOutput.Add("FILE_REFF", fileReturn.FILE_REF);
                        jOutput.Add("ID_KEY", tempConfigSign.ID_KEY);
                        jOutput.Add("DATA_JSON", JsonConvert.SerializeObject(fileInput));
                        jOutput.Add("TEMP_CODE", tempConfigSign.TEMP_CODE);
                        jOutput.Add("PRODUCT_ID", tempConfigSign.PRODUCT_CODE);
                        jOutput.Add("USER_CODE", fileInput.USER_NAME);
                        jOutput.Add("REF_TYPE", fileInput.REF_TYPE);
                        jOutput.Add("FILE_SIZE", fileSigned.Length.ToString());
                        jarrOut.Add(jOutput);

                    }
                    else
                    {
                        fileReturn.Success = false;
                        fileReturn.MESSAGE = "Không tìm được tọa độ ký";
                        return null;
                    }

                    lsFilePDfReturn.Add(fileReturn);
                }
                //đẩy dữ liệu vào DB
                object[] paramJson = new object[] { tempConfigSign.ORG_CODE, "HDI", jarrOut };//jInput['FILE'].ToString()
                response = callPackage(goConstantsProcedure.sign_InsBatch_Json, strEVM, paramJson);
                if (response == null)
                {
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao database sign 181", string.Join("-", paramJson));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so khong lay dc du lieu vao database do PK " + string.Join("-", paramJson), Logger.ConcatName(nameClass, nameMethod));
                }
                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objBatch == null || !objBatch.Any())
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao database sign 178", string.Join("-", paramJson));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so khong lay dc du lieu vao database " + string.Join("-", paramJson), Logger.ConcatName(nameClass, nameMethod));
                }
                return lsFilePDfReturn;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public List<SignFileReturnModel> previewGCNDT(TempSignModel tempConfigSign, List<SignFileReturnModel> lsFilePDf, bool isUpload, bool isBase64, bool isBackDate, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                List<SignFileReturnModel> lsFilePDfReturn = new List<SignFileReturnModel>();
                byte[] p12Byte = Convert.FromBase64String(tempConfigSign.DATA_FILE);
                int iSangTraiX = Int16.Parse(tempConfigSign.X);
                int iXuongDuoiY = Int16.Parse(tempConfigSign.Y);
                string strTypeSign = "IMAGE";
                string image_sign = tempConfigSign.M_IMAGE;
                switch (tempConfigSign.IMAGE)
                {
                    case "S":
                        image_sign = tempConfigSign.S_IMAGE;
                        break;
                    case "F":
                        image_sign = tempConfigSign.F_IMAGE;
                        break;
                }
                string strCerBasse64 = tempConfigSign.CER_FILE;
                LibSign clsSign = new LibSign(pathRoot);
                JArray jarrOut = new JArray();
                foreach (SignFileReturnModel fileInput in lsFilePDf)
                {
                    SignFileReturnModel fileReturn = fileInput;
                    float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                    int ipage = 0;
                    bool check = false;
                    check = clsSign.findTextInPdfBatch(tempConfigSign.TEXT_CA, Convert.FromBase64String(fileInput.FILE_BASE64), ref x1, ref y1, ref width1, ref ipage);
                    SignFileModel infoSign = new SignFileModel();
                    infoSign.detail = new SignFileDetail();
                    infoSign.PROVIDER = "HDI";
                    infoSign.DEVICE = "HDI";
                    infoSign.CER = strCerBasse64;
                    infoSign.FILE_SIGN = fileInput.FILE_BASE64;
                    infoSign.F12_FILE = p12Byte;
                    infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                    infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                    infoSign.detail.WIDTH = ((Int16)width1 + 2 * iSangTraiX).ToString();
                    infoSign.detail.HEIGHT = (height1 + iSangTraiX).ToString();
                    infoSign.detail.CONTACT = "";
                    infoSign.detail.DISPLAY_TYPE = strTypeSign;
                    infoSign.detail.IMAGE = image_sign;
                    infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
                    infoSign.detail.MESSAGE = "";
                    infoSign.detail.PAGE = ipage.ToString();
                    infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                    infoSign.detail.FIELD_NAME = fileInput.FILE_REF;
                    JObject jOutput = new JObject();
                    if (check)
                    {
                        string strNameFile = "";
                        string strUrl = "";
                        bool bUpload = false;
                        byte[] fileSigned = Convert.FromBase64String(fileInput.FILE_BASE64);
                        if (isUpload)
                        {
                            //thực hiện đẩy file ký lên server                            
                            BaseRequest request = new BaseRequest();
                            request.Action = new ActionInfo();
                            bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, fileInput.FILE_ID + ".pdf", "",
                                                                        request, ref strNameFile, ref strUrl, fileInput.FILE_ID);
                            for (int i = 0; i < 3; i++)
                            {
                                if (!bUpload)
                                {
                                    Thread.Sleep(5000);
                                    bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, fileInput.FILE_ID + ".pdf", "",
                                                                        request, ref strNameFile, ref strUrl, fileInput.FILE_ID);
                                    if (bUpload)
                                        break;
                                }
                                else
                                    break;
                            }
                            if (!bUpload)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                                return null;
                            }
                        }
                        if (isBase64)
                        {
                            fileReturn.FILE_BASE64 = fileInput.FILE_BASE64;
                            jOutput.Add("FILE_SIGN", "DATA_JSON");
                        }
                        else
                        {
                            fileReturn.FILE_BASE64 = "";
                            jOutput.Add("FILE_SIGN", "UPLOAD");
                        }
                        if (!string.IsNullOrEmpty(strUrl))
                            fileReturn.FILE_URL = strUrl;
                        else
                            fileReturn.FILE_BASE64 = fileInput.FILE_BASE64;
                        jOutput.Add("FILE_SIGNED", fileReturn.FILE_ID);
                        jOutput.Add("FILE_REFF", fileReturn.FILE_ID);
                        jOutput.Add("ID_KEY", tempConfigSign.ID_KEY);
                        jOutput.Add("DATA_JSON", JsonConvert.SerializeObject(fileInput));
                        jOutput.Add("TEMP_CODE", tempConfigSign.TEMP_CODE);
                        jOutput.Add("PRODUCT_ID", tempConfigSign.PRODUCT_CODE);
                        jOutput.Add("USER_CODE", fileInput.USER_NAME);
                        jOutput.Add("REF_TYPE", fileInput.REF_TYPE);
                        jOutput.Add("FILE_SIZE", fileSigned.Length.ToString());
                        jarrOut.Add(jOutput);

                    }
                    else
                    {
                        fileReturn.Success = false;
                        fileReturn.MESSAGE = "Không tìm được tọa độ ký";
                        return null;
                    }

                    lsFilePDfReturn.Add(fileReturn);
                }                
                return lsFilePDfReturn;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public BaseResponse signsign(JObject request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {   
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["CONTRACT_CODE"].ToString()).ToLower();
                //fb9f6ea41f46bd4a75b11fdfa37dbefe
                //17052021HDI@2022C1153F74B4AAA9E3E0551F6E21B40329       fb9f6ea41f46bd4a75b11fdfa37dbefe
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    InsurContract iContract = JsonConvert.DeserializeObject<InsurContract>(request.ToString());
                    List<InsurDetail> tempParam = JsonConvert.DeserializeObject< List<InsurDetail>>(request["CONTRACT"].ToString());
                    string strUSER = request["USER"].ToString();
                    string strType = request["TYPE_CALL"].ToString();
                    string strChannel = request["CHANNEL_CALL"].ToString();
                    response = goBusinessServices.Instance.signAndSendMail(iContract,tempParam,strUSER,strChannel,strType,false,true);
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "signsign", request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse reSign(JObject request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["CONTRACT_CODE"].ToString()).ToLower();
                //fb9f6ea41f46bd4a75b11fdfa37dbefe
                //17052021HDI@2022C1153F74B4AAA9E3E0551F6E21B40329       fb9f6ea41f46bd4a75b11fdfa37dbefe
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    if (request["EMAIL"].ToString().Equals("RE_SEND"))
                        response = goBusinessServices.Instance.reSendEmailContract(request);
                    else
                        response = goBusinessServices.Instance.reSignGCN(request);
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "reSign", request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public List<TempFileSign> signBatchPDF(List<TempFileSign> lsFilePDf, string strProductCode, string strOrgCode, string strPackCode)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strStruct = lsFilePDf[0].STRUCT_CODE;
                if (string.IsNullOrEmpty(lsFilePDf[0].STRUCT_CODE))
                    strStruct = "HDI";
                // LẤY THÔNG TIN CẤU HÌNH CHỨNG THƯ SỐ TỪ USER TRONG BẢNG CONFIG
                object[] paramSign;
                paramSign = new object[] { strOrgCode, "HDI", strProductCode + "_GCN", strProductCode, strPackCode, strStruct, "WEB" };
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute("HDI_SIGN", strEvm, paramSign);
                response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                List<JObject> objSignConfig = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objSignConfig == null || objSignConfig.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc cau hinh sign 293", strOrgCode + " : " + strProductCode + " moi truong " + strEvm);
                    return null;
                }
                if (!Directory.Exists(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd")))
                {
                    Directory.CreateDirectory(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd"));
                }
                //bắt đầu vòng tạo file ký số
                byte[] p12Byte = Convert.FromBase64String(objSignConfig[0]["DATA_FILE"].ToString());
                int iSangTraiX = Int16.Parse(objSignConfig[0]["X"].ToString());
                int iXuongDuoiY = Int16.Parse(objSignConfig[0]["Y"].ToString());
                string strTypeSign = "IMAGE";
                string image_sign = objSignConfig[0]["M_IMAGE"].ToString();
                switch (objSignConfig[0]["IMAGE"].ToString())
                {
                    case "S":
                        image_sign = objSignConfig[0]["S_IMAGE"].ToString();
                        break;
                    case "F":
                        image_sign = objSignConfig[0]["F_IMAGE"].ToString();
                        break;
                }
                string strCerBasse64 = objSignConfig[0]["CER_FILE"].ToString();
                JArray jarrOut = new JArray();
                foreach (TempFileSign filePdf in lsFilePDf)
                {                   
                        
                        //tìm tọa độ cần ký    
                        float x1 = 0, y1 = 0, width1 = 0, height1 = 80;                        
                        int ipage = 0;
                        bool check = false;
                        string strPathFileSign = filePdf.ID_FILE + ".pdf";
                        string strPathFileSave = strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd") + "/" + strPathFileSign;
                        //goUtility.Instance.Base64ToFile(strBase64, strPathFileSave);
                        LibSign clsSign = new LibSign(pathRoot);
                        check = clsSign.findTextInPdfBatch(objSignConfig[0]["TEXT_CA"]?.ToString(), Convert.FromBase64String(filePdf.FILE_BASE64), ref x1, ref y1, ref width1, ref ipage);
                        SignFileModel infoSign = new SignFileModel();
                        infoSign.detail = new SignFileDetail();
                        infoSign.PROVIDER = "HDI";
                        infoSign.DEVICE = "HDI";
                        infoSign.CER = strCerBasse64;
                        infoSign.FILE_SIGN = filePdf.FILE_BASE64;
                        infoSign.F12_FILE = p12Byte;
                        infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                        infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                        infoSign.detail.WIDTH = ((Int16)width1 + 2 * iSangTraiX).ToString();
                        infoSign.detail.HEIGHT = (height1 + iSangTraiX).ToString();
                        infoSign.detail.CONTACT = "";
                        infoSign.detail.DISPLAY_TYPE = strTypeSign;
                        infoSign.detail.IMAGE = image_sign;
                        infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
                        infoSign.detail.MESSAGE = "";
                        infoSign.detail.PAGE = ipage.ToString();
                        infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                    infoSign.detail.FIELD_NAME = filePdf.CERTIFICATE_NO;
                    JObject jOutput = new JObject();
                        if (check)
                        {
                            //thực hiện ký số
                            string strFileKy = clsSign.signPDF(infoSign, objSignConfig[0]["PASSWORD"].ToString(), "");
                            if (string.IsNullOrEmpty(strFileKy))
                            {
                                return null;
                            }
                            //thực hiện đẩy file ký lên server
                            string strNameFile = "";
                            string strUrl = "";
                            BaseRequest request = new BaseRequest();
                            request.Action = new ActionInfo();
                            bool bUpload = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strFileKy), strPathFileSign,
                                "SignPDF/" + strOrgCode + "/" + DateTime.Now.ToString("yyyyMMdd"),
                                request, ref strNameFile, ref strUrl, filePdf.ID_FILE);
                            if (!bUpload)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                                return null;
                            }
                            //thực hiện đẩy vào dữ liệu
                            object[] param1 = new string[] { strOrgCode, "HDI", "HDI_SIGN", "", objSignConfig[0]["ID_KEY"].ToString(),
                            filePdf.CERTIFICATE_NO, strProductCode+"_GCN", "HDI_SIGN","BASE64",strNameFile,"HDI_SIGN", filePdf.CERTIFICATE_NO };//jInput['FILE'].ToString()
                            response = callPackage(goConstantsProcedure.Sign_Insert_Batch, strEvm, param1);
                            List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                            if (objBatch == null || objBatch[0].Count == 0)
                            {
                                goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao batch sign 376", string.Join("-", param1));
                                return null;
                            }
                            string strID_BATch = objBatch[0]["BATCH_ID"].ToString();
                        filePdf.FILE_BASE64 = strFileKy;
                        filePdf.signed = true;
                        }
                        else
                        {
                        filePdf.signed = false;
                    }
                    }
                return lsFilePDf;
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public TempFileSign signBackDate(TempFileSign filePdf, string strProductCode, string strOrgCode, string strPackCode)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strStruct = filePdf.STRUCT_CODE;
                if (string.IsNullOrEmpty(filePdf.STRUCT_CODE))
                    strStruct = "HDI";
                // LẤY THÔNG TIN CẤU HÌNH CHỨNG THƯ SỐ TỪ USER TRONG BẢNG CONFIG
                object[] paramSign;
                paramSign = new object[] { strOrgCode, "HDI", strProductCode + "_GCN", strProductCode, strPackCode, strStruct, "WEB" };
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute("HDI_SIGN", strEvm, paramSign);
                response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                List<JObject> objSignConfig = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objSignConfig == null || objSignConfig.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc cau hinh sign 415", strOrgCode + " : " + strProductCode + " moi truong " + strEvm);
                    return null;
                }
                if (!Directory.Exists(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd")))
                {
                    Directory.CreateDirectory(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd"));
                }
                //bắt đầu vòng tạo file ký số
                byte[] p12Byte = Convert.FromBase64String(objSignConfig[0]["DATA_FILE"].ToString());
                int iSangTraiX = Int16.Parse(objSignConfig[0]["X"].ToString());
                int iXuongDuoiY = Int16.Parse(objSignConfig[0]["Y"].ToString());
                string strTypeSign = "IMAGE";
                string image_sign = objSignConfig[0]["M_IMAGE"].ToString();
                switch (objSignConfig[0]["IMAGE"].ToString())
                {
                    case "S":
                        image_sign = objSignConfig[0]["S_IMAGE"].ToString();
                        break;
                    case "F":
                        image_sign = objSignConfig[0]["F_IMAGE"].ToString();
                        break;
                }
                string strCerBasse64 = objSignConfig[0]["CER_FILE"].ToString();
                
                    //tìm tọa độ cần ký    
                    float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                    int ipage = 0;
                    bool check = false;
                    string strPathFileSign = filePdf.ID_FILE + ".pdf";
                    string strPathFileSave = strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd") + "/" + strPathFileSign;
                    //goUtility.Instance.Base64ToFile(strBase64, strPathFileSave);
                    LibSign clsSign = new LibSign(pathRoot);
                    check = clsSign.findTextInPdfBatch(objSignConfig[0]["TEXT_CA"]?.ToString(), Convert.FromBase64String(filePdf.FILE_BASE64), ref x1, ref y1, ref width1, ref ipage);
                    SignFileModel infoSign = new SignFileModel();
                    infoSign.detail = new SignFileDetail();
                    infoSign.PROVIDER = "HDI";
                    infoSign.DEVICE = "HDI";
                    infoSign.CER = strCerBasse64;
                    infoSign.FILE_SIGN = filePdf.FILE_BASE64;
                    infoSign.F12_FILE = p12Byte;
                    infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                    infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                    infoSign.detail.WIDTH = ((Int16)width1 + 2 * iSangTraiX).ToString();
                    infoSign.detail.HEIGHT = (height1 + iSangTraiX).ToString();
                    infoSign.detail.CONTACT = "";
                    infoSign.detail.DISPLAY_TYPE = strTypeSign;
                    infoSign.detail.IMAGE = image_sign;
                    infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
                    infoSign.detail.MESSAGE = "";
                    infoSign.detail.PAGE = ipage.ToString();
                    infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                infoSign.detail.FIELD_NAME = filePdf.CERTIFICATE_NO;
                    JObject jOutput = new JObject();
                    if (check)
                    {
                        //thực hiện ký số
                        string strFileKy = clsSign.signPDFBack(infoSign, objSignConfig[0]["PASSWORD"].ToString(), "",filePdf.dtSignBack);
                        if (string.IsNullOrEmpty(strFileKy))
                        {
                            return null;
                        }
                        //thực hiện đẩy file ký lên server
                        string strNameFile = "";
                        string strUrl = "";
                        BaseRequest request = new BaseRequest();
                        request.Action = new ActionInfo();
                        bool bUpload = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strFileKy), strPathFileSign,
                            "SignPDF/" + strOrgCode + "/" + DateTime.Now.ToString("yyyyMMdd"),
                            request, ref strNameFile, ref strUrl, filePdf.ID_FILE);
                        if (!bUpload)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                            return null;
                        }
                        //thực hiện đẩy vào dữ liệu
                        object[] param1 = new string[] { strOrgCode, "HDI", "HDI_SIGN", "", objSignConfig[0]["ID_KEY"].ToString(),
                            filePdf.CERTIFICATE_NO, strProductCode+"_GCN", "HDI_SIGN","BASE64",strNameFile,"HDI_SIGN", filePdf.CERTIFICATE_NO };//jInput['FILE'].ToString()
                        response = callPackage(goConstantsProcedure.Sign_Insert_Batch, strEvm, param1);
                        List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (objBatch == null || objBatch[0].Count == 0)
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao batch sign 495", string.Join("-", param1));
                            return null;
                        }
                        string strID_BATch = objBatch[0]["BATCH_ID"].ToString();
                        filePdf.FILE_BASE64 = strFileKy;
                        filePdf.signed = true;
                    }
                    else
                    {
                        filePdf.signed = false;
                    }
                
                return filePdf;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public BaseResponse GetBaseResponse(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    SignRequestModel signModel = JsonConvert.DeserializeObject<SignRequestModel>(request.Data.ToString());
                    return signHDI(request.Action.ParentCode, request.Action.ActionCode, "", signModel, config);
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong dung cau truc sign 535", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so sign 542", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse veryfiSign(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject signModel = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                    byte[] bFile = Convert.FromBase64String(signModel["FILE_BASE64"].ToString());
                    LibSign clsSign = new LibSign(pathRoot);
                    List<CerInforModel> lsCert = clsSign.getInforInFileSign(bFile);
                    if(lsCert !=null && lsCert.Count == 1)
                    {
                        //lấy dữ liệu từ DB
                        object[] param1 = new string[] { lsCert[0].FIELD_NAME, bFile.Length.ToString() };
                        response = callPackage(goConstantsProcedure.sign_veryfi, strEvm, param1);
                        if(response != null)
                        {
                            List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                            if (objBatch == null || objBatch[0].Count == 0)
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "File không được ký số bởi HDI - Không tìm thấy file 1");
                            }else
                            {
                                JObject jCerData = objBatch[0];
                                if(!jCerData["SERIAL"].ToString().ToUpper().Equals(lsCert[0].SERIAL))
                                    return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "File không được ký số bởi HDI - ID_KEY không tồn tại trong hệ thống");
                                if (!jCerData["NAME"].ToString().Equals(lsCert[0].NAME))
                                    return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "File không được ký số bởi HDI - Người ký không tồn tại trong hệ thống");
                                if (lsCert[0].DATE_SIGN < DateTime.ParseExact(jCerData["EFFECTIVE_DATE"].ToString(), "dd/MM/yyyy", null) || lsCert[0].DATE_SIGN > DateTime.ParseExact(jCerData["EXPIRATION_DATE"].ToString(), "dd/MM/yyyy", null))
                                {
                                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi veryfiSign - ký ngoài hiệu lực", "Ngoài hiệu lực số giấy chứng nhận " + lsCert[0].FIELD_NAME + "_sery " + lsCert[0].SERIAL);
                                }
                                response.Data = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                            }    
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "File không được ký số bởi HDI - Không tìm thấy file");
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "File không được ký số bởi HDI ");
                    }
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json veryfiSign " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi veryfiSign", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "loi veryfi", ex.ToString());
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so veryfiSign", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }


        public BaseResponse sign_mail_core(BaseRequest request)
        {
            //hàm ký sổ gửi mail cho centech
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    if (string.IsNullOrEmpty(jInput["MAIL_BUYER"]?.ToString()) && string.IsNullOrEmpty(jInput["MAIL_INSURED"]?.ToString()))
                    {
                        response.Success = false;
                        response.ErrorMessage = "Không tồn tại mail để gửi";
                    }
                    SignRequestModel signModel = JsonConvert.DeserializeObject<SignRequestModel>(request.Data.ToString());
                    //byte[] byteFile = Convert.FromBase64String(signModel.FILE);
                    //string strSig = byteFile.Length.ToString();
                    response = callPackage("HDI_GET_SIGN", "LIVE", new object[] { "X", signModel.FILE_REFF, request.Action.ParentCode, "INS" });
                    List<JObject> objFileSign = null;
                    if (response !=null)
                    objFileSign = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (response != null  && objFileSign != null && objFileSign.Any())
                    {
                        foreach(JObject jOutSign in objFileSign)
                        {
                            if (signModel.TEMP_CODE.Equals(jOutSign["TEMP_CODE"].ToString()))
                            {
                                JObject jOutput = new JObject();//TEMP_CODE
                                jOutput.Add("FILE_NAME", jOutSign["FILE_SIGNED"].ToString());
                                jOutput.Add("FILE_URL", goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + jOutSign["FILE_SIGNED"].ToString());
                                jOutput.Add("FILE_REFF", signModel.FILE_REFF);
                                jOutput.Add("FILE_BASE64", Convert.ToBase64String(goBusinessCommon.Instance.getFileServer(jOutSign["FILE_SIGNED"].ToString())));
                                response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                                return response;
                            }
                            
                        }                        
                    }
                    response = signHDI(request.Action.ParentCode, request.Action.ActionCode, "", signModel, config);
                    if (response.Success)
                    {
                        JObject jInputExt = new JObject();
                        try
                        {
                            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                            jInputExt = JObject.Parse(jInput["DATA_EXT"].ToString());
                            jInputExt = addValueJobject(jInputExt, "CERTIFICATE_NO", signModel.FILE_REFF);
                            jInputExt = addValueJobject(jInputExt, "CREATE_BY", signModel.USER_NAME);
                            jInputExt = addValueJobject(jInputExt, "PRODUCT_CODE", signModel.TEMP_CODE.Replace("_GCN", ""));
                            insertExtData(request.Action.ParentCode, jInputExt, strEvm);
                            //new Task(() => insertExtData(request.Action.ParentCode, jInputExt, strEvm)).Start();
                        }
                        catch { }
                        //xử lý gửi mail
                        JObject jDataSigned = JObject.Parse(response.Data.ToString());
                        bool b = goBusinessServices.Instance.sendEmailGCN_core(jDataSigned["FILE_NAME"].ToString(), jDataSigned["FILE_URL"].ToString(), jInput, jInputExt);
                        if (!b)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi gui mail gcn core do tat che do gui mail", Logger.ConcatName(nameClass, nameMethod));// + JsonConvert.SerializeObject(request).ToString()
                            //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gui mail gcn core sign 593", jDataSigned["FILE_NAME"].ToString() + jDataSigned["FILE_URL"].ToString() + JsonConvert.SerializeObject(request).ToString());
                        }
                        return response;
                    }

                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong dung cau truc sign 602", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so sign 609", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        {
            try
            {
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
                return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
            }
            catch (Exception)
            {

                return null;
            }
            
        }
        public JObject addValueJobject(JObject jInput, string strName, string strValue)
        {
            try
            {
                jInput.Add(strName, strValue);
            }
            catch (Exception ex)
            {   
            }
            return jInput;
        }
        public void insertExtData(string strOrgCode, JObject jInput, string strEVM) {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                
                object[] paramJson = new object[] { strOrgCode, jInput };//jInput['FILE'].ToString()
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.sign_dataExt, strEVM, paramJson);
                BaseResponse response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramJson), false, null, null, null, null);
                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objBatch == null || !objBatch.Any())
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi insert insertExtData vao database ", string.Join("-", paramJson));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi insert insertExtData vao database " + string.Join("-", paramJson), Logger.ConcatName(nameClass, nameMethod));
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi insert insertExtData vao database ", jInput.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi insert insertExtData vao database " + string.Join("-", jInput.ToString()), Logger.ConcatName(nameClass, nameMethod));
            }
        }
        public BaseResponse createAndSignBatch(string strContractCode, string strCategory, string strProductCode, string strOrgCode)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "lay mau, du lieu: ", Logger.ConcatName(nameClass, nameMethod));
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //lấy dữ liệu cần tạo giấy chứng nhận
                object[] paramFind = new object[] { "OPENAPI", "HDI", strContractCode, strProductCode, "NO", strProductCode + "_GCN", strOrgCode };
                BaseResponse responseFind = callPackage(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                List<JObject> objDetails = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);
                if (objContract.Count == 0 || objDetails.Count == 0)
                {
                    return response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3006), goConstantsError.Instance.ERROR_3006);
                }
                //thực hiện lấy mẫu 
                string strPackCode = objDetails[0]["PACK_CODE"].ToString();
                object[] param = new object[] { "HDI_PRIVATE", "PDF", strProductCode + "_GCN", strProductCode, strPackCode };
                BaseResponse responseTemplate = callPackage("GET_TEMPLATE", strEvm, param);
                List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
                if (arrTemplate == null || arrTemplate.Count == 0)
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không tìm thấy cấu hình mã mẫu " + strProductCode);
                }
                // LẤY THÔNG TIN CẤU HÌNH CHỨNG THƯ SỐ TỪ USER TRONG BẢNG CONFIG
                object[] paramSign;
                paramSign = new object[] { "HDI_PRIVATE", "HDI", strProductCode + "_GCN", strProductCode, strPackCode, objDetails[0]["STRUCT_CODE"].ToString(), "WEB" };
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute("HDI_SIGN", strEvm, paramSign);
                response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                List<JObject> objSignConfig = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objSignConfig == null || objSignConfig.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc cau hinh sign 670", strOrgCode + " : " + strProductCode + " moi truong " + strEvm);
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                }
                if (!Directory.Exists(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd")))
                {
                    Directory.CreateDirectory(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd"));
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ket thuc lay mau, du lieu: =====================", Logger.ConcatName(nameClass, nameMethod));
                //bắt đầu vòng tạo file ký số
                JArray jarrOut = new JArray();
                int i = 0;
                foreach (JObject jobCer in objDetails)
                {
                    if (0 == 1) {
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "foreach: " + jobCer.ToString(), Logger.ConcatName(nameClass, nameMethod));
                        //new Task(()=> Test(jobCer, arrTemplate, responseTemplate, objSignConfig, strContractCode, strCategory, strProductCode, strOrgCode, response, strEvm)).Start();
                    }
                    else
                    {
                        //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "===================================", Logger.ConcatName(nameClass, nameMethod));
                        //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "foreach: " + jobCer.ToString(), Logger.ConcatName(nameClass, nameMethod));
                        //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "===================================", Logger.ConcatName(nameClass, nameMethod));
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tao file pdf : =====================" + i.ToString(), Logger.ConcatName(nameClass, nameMethod));
                        List<JObject> lsCreate = new List<JObject>();
                        lsCreate.Add(jobCer);
                        string strBodyHtml = goBusinessTemplate.Instance.fillValueToHtmlBodyBatch(lsCreate, responseTemplate);
                        string strTempHeader = strPathTemplate + arrTemplate[0]["HEADER"].ToString();
                        string strHeader = "";
                        if (!string.IsNullOrEmpty(arrTemplate[0]["HEADER"].ToString()))
                        {
                            if (arrTemplate[0]["HEADER"].ToString().Length < 300)
                                strHeader = File.ReadAllText(strTempHeader);
                            else
                                strHeader = arrTemplate[0]["HEADER"].ToString();
                        }
                        string strTempPooter = strPathTemplate + arrTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                        string strFooter = "";
                        if (!string.IsNullOrEmpty(arrTemplate[0]["FOOTER"].ToString()))
                        {
                            if (arrTemplate[0]["FOOTER"].ToString().Length < 300)
                                strFooter = File.ReadAllText(strTempPooter);
                            else
                                strFooter = arrTemplate[0]["FOOTER"].ToString();
                        }
                        string strUrlAddPdf = "";
                        if ((arrTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!arrTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                            strUrlAddPdf = arrTemplate[0]["URL_PAGE_ADD"].ToString();
                        if (!string.IsNullOrEmpty(jobCer["QR_CODE"]?.ToString()))
                        {
                            string strBase64QR = goBussinessPDF.Instance.getQrCode(jobCer["QR_CODE"].ToString(), "Q", 8, "");
                            strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
                        }
                        string strGuiID_File = Guid.NewGuid().ToString("N");
                        string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
                        strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                        strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                        goBussinessPDF cls = new goBussinessPDF();
                        string strBase64 = cls.convertPageToPDFPersnal_batch(strHeader, strBodyHtml, strFooter, strUrlAddPdf);
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ket thuc tao file pdf : =====================" + i.ToString(), Logger.ConcatName(nameClass, nameMethod));
                        //bắt đầu thực hiện ký số
                        byte[] p12Byte = Convert.FromBase64String(objSignConfig[0]["DATA_FILE"].ToString());
                        //tìm tọa độ cần ký    
                        float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                        int iSangTraiX = Int16.Parse(objSignConfig[0]["X"].ToString());
                        int iXuongDuoiY = Int16.Parse(objSignConfig[0]["Y"].ToString());
                        int ipage = 0;
                        string strTypeSign = "IMAGE";
                        string image_sign = objSignConfig[0]["M_IMAGE"].ToString();
                        switch (objSignConfig[0]["IMAGE"].ToString())
                        {
                            case "S":
                                image_sign = objSignConfig[0]["S_IMAGE"].ToString();
                                break;
                            case "F":
                                image_sign = objSignConfig[0]["F_IMAGE"].ToString();
                                break;
                        }
                        string strCerBasse64 = objSignConfig[0]["CER_FILE"].ToString();
                        int page = 0;
                        bool check = false;
                        string strPathFileSign = strGuiID_File + ".pdf";
                        string strPathFileSave = strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd") + "/" + strPathFileSign;
                        //goUtility.Instance.Base64ToFile(strBase64, strPathFileSave);
                        LibSign clsSign = new LibSign(pathRoot);
                        check = clsSign.findTextInPdfBatch(objSignConfig[0]["TEXT_CA"]?.ToString(), Convert.FromBase64String(strBase64), ref x1, ref y1, ref width1, ref ipage);
                        SignFileModel infoSign = new SignFileModel();
                        infoSign.detail = new SignFileDetail();
                        infoSign.PROVIDER = "HDI";
                        infoSign.DEVICE = "HDI";
                        infoSign.CER = strCerBasse64;
                        infoSign.FILE_SIGN = strBase64;
                        infoSign.F12_FILE = p12Byte;
                        infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                        infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                        infoSign.detail.WIDTH = ((Int16)width1 + 2 * iSangTraiX).ToString();
                        infoSign.detail.HEIGHT = (height1 + iSangTraiX).ToString();
                        infoSign.detail.CONTACT = "";
                        infoSign.detail.DISPLAY_TYPE = strTypeSign;
                        infoSign.detail.IMAGE = image_sign;
                        infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
                        infoSign.detail.MESSAGE = "";
                        infoSign.detail.PAGE = ipage.ToString();
                        infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                        infoSign.detail.FIELD_NAME = jobCer["CERTIFICATE_NO"]?.ToString();
                        JObject jOutput = new JObject();
                        if (check)
                        {
                            //thực hiện ký số
                            string strFileKy = clsSign.signPDF(infoSign, objSignConfig[0]["PASSWORD"].ToString(), "");
                            if (string.IsNullOrEmpty(strFileKy))
                            {
                                return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3005), "Lỗi khi thực hiện ký số");
                            }
                            File.WriteAllBytes(strPathFileSave, Convert.FromBase64String(strFileKy));
                            //thực hiện đẩy file ký lên server
                            string strNameFile = "";
                            string strUrl = "";
                            BaseRequest request = new BaseRequest();
                            request.Action = new ActionInfo();
                            //bool bUpload = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strFileKy), strPathFileSign,
                            //    "SignPDF/" + strOrgCode + "/" + DateTime.Now.ToString("yyyyMMdd"),
                            //    request, ref strNameFile, ref strUrl, strGuiID_File);
                            //if (!bUpload)
                            //{
                            //    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                            //    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3007), "Lỗi khi thực hiện ký số");
                            //}
                            //thực hiện đẩy vào dữ liệu
                            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ky so xong : =====================" + i.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            
                            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ket thuc tien trinh : =====================" + i.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            i++;
                            //string strID_BATch = objBatch[0]["BATCH_ID"].ToString();

                            jOutput.Add("FILE_NAME", strGuiID_File);
                            jOutput.Add("FILE_URL", strUrl);
                            jOutput.Add("FILE_REFF", jobCer["CERTIFICATE_NO"].ToString());
                            jOutput.Add("FILE_BASE64", "strFileKy");
                            jOutput.Add("ID_KEY", objSignConfig[0]["ID_KEY"].ToString());
                            jOutput.Add("DATA_JSON", jobCer.ToString());
                            jOutput.Add("TEMP_CODE", strProductCode + "_GCN");
                            jOutput.Add("TEMP_SIGN", strPathFileSave);

                            jarrOut.Add(jOutput);
                            TempFileSign tempFile = new TempFileSign();
                            tempFile.FILE_URL = strUrl;
                            tempFile.CERTIFICATE_NO = jobCer["CERTIFICATE_NO"].ToString();
                            tempFile.FILE_BASE64 = strFileKy;
                            tempFile.ID_FILE = strGuiID_File;
                            tempFile.PRODUCT_CODE = strProductCode;
                            //new Task(() => goBusinessCommon.Instance.addFileSign(tempFile)).Start();
                        }
                        else
                        {
                            jOutput.Add("FILE_NAME", "Error");
                            jOutput.Add("FILE_URL", "Error");
                            jOutput.Add("FILE_REFF", jobCer["CERTIFICATE_NO"].ToString());
                            jOutput.Add("FILE_BASE64", "Không tìm được tọa độ");
                            jarrOut.Add(jOutput);
                        }
                    }
                }
                object[] param1 = new object[] { strOrgCode, "HDI", jarrOut };//jInput['FILE'].ToString()
                response = callPackage("APIYCFKBB7", strEvm, param1);
                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objBatch == null || objBatch[0].Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao batch sign 836", string.Join("-", param1));
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không đẩy được dữ liệu vào DB ký số");
                }
                response = goBusinessCommon.Instance.getResultApi(true, jarrOut, false, null, null, null, null);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return response;
        }

        public async void Test(JObject jobCer, List<JObject> arrTemplate, BaseResponse responseTemplate, List<JObject> objSignConfig, string strContractCode, string strCategory, string strProductCode, string strOrgCode, BaseResponse response, string strEvm)
        {
            List<JObject> lsCreate = new List<JObject>();
            lsCreate.Add(jobCer);
            string strBodyHtml = goBusinessTemplate.Instance.fillValueToHtmlBodyBatch(lsCreate, responseTemplate);
            string strTempHeader = strPathTemplate + arrTemplate[0]["HEADER"].ToString();
            string strHeader = "";
            if (!string.IsNullOrEmpty(arrTemplate[0]["HEADER"].ToString()))
            {
                if (arrTemplate[0]["HEADER"].ToString().Length < 300)
                    strHeader = File.ReadAllText(strTempHeader);
                else
                    strHeader = arrTemplate[0]["HEADER"].ToString();
            }
            string strTempPooter = strPathTemplate + arrTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
            string strFooter = "";
            if (!string.IsNullOrEmpty(arrTemplate[0]["FOOTER"].ToString()))
            {
                if (arrTemplate[0]["FOOTER"].ToString().Length < 300)
                    strFooter = File.ReadAllText(strTempPooter);
                else
                    strFooter = arrTemplate[0]["FOOTER"].ToString();
            }
            string strUrlAddPdf = "";
            if ((arrTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!arrTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                strUrlAddPdf = arrTemplate[0]["URL_PAGE_ADD"].ToString();
            if (!string.IsNullOrEmpty(jobCer["QR_CODE"]?.ToString()))
            {
                string strBase64QR = goBussinessPDF.Instance.getQrCode(jobCer["QR_CODE"].ToString(), "Q", 8, "");
                strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
            }
            string strGuiID_File = Guid.NewGuid().ToString("N");
            string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
            strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
            strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
            goBussinessPDF cls = new goBussinessPDF();
            string strBase64 = cls.convertPageToPDFPersnal(strHeader, strBodyHtml, strFooter, strUrlAddPdf, "", "", "", arrTemplate[0]["HEADER_HEIGHT"].ToString(), arrTemplate[0]["HEADER_HEIGHT"].ToString(), arrTemplate[0]["IS_ATTACH"].ToString());
            //bắt đầu thực hiện ký số
            byte[] p12Byte = Convert.FromBase64String(objSignConfig[0]["DATA_FILE"].ToString());
            //tìm tọa độ cần ký    
            float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
            int iSangTraiX = Int16.Parse(objSignConfig[0]["X"].ToString());
            int iXuongDuoiY = Int16.Parse(objSignConfig[0]["Y"].ToString());
            int ipage = 0;
            string strTypeSign = "IMAGE";
            string image_sign = objSignConfig[0]["M_IMAGE"].ToString();
            switch (objSignConfig[0]["IMAGE"].ToString())
            {
                case "S":
                    image_sign = objSignConfig[0]["S_IMAGE"].ToString();
                    break;
                case "F":
                    image_sign = objSignConfig[0]["F_IMAGE"].ToString();
                    break;
            }
            string strCerBasse64 = objSignConfig[0]["CER_FILE"].ToString();
            int page = 0;
            bool check = false;
            string strPathFileSign = strGuiID_File + ".pdf";
            string strPathFileSave = strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd") + "/" + strPathFileSign;
            //goUtility.Instance.Base64ToFile(strBase64, strPathFileSave);
            LibSign clsSign = new LibSign(pathRoot);
            check = clsSign.findTextInPdfBatch(objSignConfig[0]["TEXT_CA"]?.ToString(), Convert.FromBase64String(strBase64), ref x1, ref y1, ref width1, ref ipage);
            SignFileModel infoSign = new SignFileModel();
            infoSign.detail = new SignFileDetail();
            infoSign.PROVIDER = "HDI";
            infoSign.DEVICE = "HDI";
            infoSign.CER = strCerBasse64;
            infoSign.FILE_SIGN = strBase64;
            infoSign.F12_FILE = p12Byte;
            infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
            infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
            infoSign.detail.WIDTH = ((Int16)width1 + 2 * Math.Abs(iSangTraiX)).ToString();
            infoSign.detail.HEIGHT = (height1 + Math.Abs(iSangTraiX)).ToString();
            infoSign.detail.CONTACT = "";
            infoSign.detail.DISPLAY_TYPE = strTypeSign;
            infoSign.detail.IMAGE = image_sign;
            infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
            infoSign.detail.MESSAGE = "";
            infoSign.detail.PAGE = ipage.ToString();
            infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
            JObject jOutput = new JObject();
            if (check)
            {
                //thực hiện ký số
                string strFileKy = clsSign.signPDF(infoSign, objSignConfig[0]["PASSWORD"].ToString(), "");
                //if (string.IsNullOrEmpty(strFileKy))
                //{
                //    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3005), "Lỗi khi thực hiện ký số");
                //}
                //thực hiện đẩy file ký lên server
                string strNameFile = "";
                string strUrl = "";
                BaseRequest request = new BaseRequest();
                request.Action = new ActionInfo();
                bool bUpload = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strFileKy), strPathFileSign,
                    "SignPDF/" + strOrgCode + "/" + DateTime.Now.ToString("yyyyMMdd"),
                    request, ref strNameFile, ref strUrl, strGuiID_File);
                if (!bUpload)
                {
                    //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                    //return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3007), "Lỗi khi thực hiện ký số");
                }
                //thực hiện đẩy vào dữ liệu
                object[] param1 = new string[] { strOrgCode, "HDI", "HDI_SIGN", "", objSignConfig[0]["ID_KEY"].ToString(),
                            jobCer.ToString(), strProductCode+"_GCN", "HDI_SIGN","BASE64",strNameFile,"HDI_SIGN", jobCer["CERTIFICATE_NO"].ToString() };//jInput['FILE'].ToString()
                callPackage(goConstantsProcedure.Sign_Insert_Batch, strEvm, param1);
                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objBatch == null || objBatch[0].Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao batch sign 959", string.Join("-", param1));
                    //return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không đẩy được dữ liệu vào DB ký số");
                }
                string strID_BATch = objBatch[0]["BATCH_ID"].ToString();

                jOutput.Add("FILE_NAME", strNameFile);
                jOutput.Add("FILE_URL", strUrl);
                jOutput.Add("FILE_REFF", jobCer["CERTIFICATE_NO"].ToString());
                jOutput.Add("FILE_BASE64", strFileKy);
                //jarrOut.Add(jOutput);
            }
            else
            {
                jOutput.Add("FILE_NAME", "Error");
                jOutput.Add("FILE_URL", "Error");
                jOutput.Add("FILE_REFF", jobCer["CERTIFICATE_NO"].ToString());
                jOutput.Add("FILE_BASE64", "Không tìm được tọa độ");
                //jarrOut.Add(jOutput);
            }
        }

        public BaseResponse signHDI(string strOrgCode, string strActionCode, string strTypeSign, SignRequestModel signModel, ResponseConfig config)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                // LẤY THÔNG TIN CẤU HÌNH CHỨNG THƯ SỐ TỪ USER TRONG BẢNG CONFIG
                object[] paramSign;
                if (!string.IsNullOrEmpty(signModel.PRODUCT_CODE))
                {
                    if (string.IsNullOrEmpty(signModel.PACK_CODE))
                        signModel.PACK_CODE = "null";
                    paramSign = new object[] { strOrgCode, signModel.USER_NAME, signModel.TEMP_CODE, signModel.PRODUCT_CODE, signModel.PACK_CODE, signModel.STRUCT_CODE, signModel.CHANNEL };
                }
                else
                    paramSign = new object[] { strOrgCode, signModel.USER_NAME, signModel.TEMP_CODE, "null", "null", "null", "null" };
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strActionCode, strEvm, paramSign);
                response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                List<JObject> objSignConfig = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objSignConfig == null || objSignConfig.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc cau hinh sign 1003", strOrgCode + " : " + signModel.USER_NAME + " : " + signModel.TEMP_CODE + " moi truong " + strEvm);
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                }
                //gọi pkg để lấy dữ liệu fill vào các trường sau
                byte[] p12Byte = Convert.FromBase64String(objSignConfig[0]["DATA_FILE"].ToString());
                //tìm tọa độ cần ký    
                float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                int iSangTraiX = Int16.Parse(objSignConfig[0]["X"].ToString());
                int iXuongDuoiY = Int16.Parse(objSignConfig[0]["Y"].ToString());
                int ipage = 0;
                if (string.IsNullOrEmpty(strTypeSign))
                    strTypeSign = "IMAGE";
                string image_sign = objSignConfig[0]["M_IMAGE"].ToString();
                switch (objSignConfig[0]["IMAGE"].ToString())
                {
                    case "S":
                        image_sign = objSignConfig[0]["S_IMAGE"].ToString();
                        break;
                    case "F":
                        image_sign = objSignConfig[0]["F_IMAGE"].ToString();
                        break;
                }
                string strCerBasse64 = objSignConfig[0]["CER_FILE"].ToString();
                int page = 0;
                bool check = false;
                if (string.IsNullOrEmpty(signModel.EXT_FILE))
                    signModel.EXT_FILE = "HTMLHDI";
                //lưu file nếu base64 để tìm tọa độ
                string strPathFileSign = goEncryptBase.Instance.Md5Encode(JsonConvert.SerializeObject(signModel).ToString()) + "." + signModel.EXT_FILE;
                string strPathFileSave = strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd") + "/" + strPathFileSign;
                if (!Directory.Exists(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd")))
                {
                    Directory.CreateDirectory(strPathTemplate + "FileSign/" + (DateTime.Now).ToString("yyMMdd"));
                }
                switch (signModel.TYPE_FILE.ToUpper())
                {
                    case "FILE":
                        byte[] bFile = goBusinessCommon.Instance.getFileServer(signModel.FILE);
                        if (bFile != null)
                        {
                            try
                            {
                                File.WriteAllBytes(strPathFileSave, bFile);
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            }
                        }
                        else
                        {
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3005), goConstantsError.Instance.ERROR_3005);
                        }
                        break;
                    case "BASE64":
                        goUtility.Instance.Base64ToFile(signModel.FILE, strPathFileSave);
                        break;
                }
                LibSign clsSign = new LibSign(pathRoot);
                string strGuiID = signModel.FILE_CODE;
                if(string.IsNullOrEmpty(signModel.FILE_CODE))
                    strGuiID = Guid.NewGuid().ToString("N");
                switch (signModel.EXT_FILE.ToUpper())
                {
                    case "DOC":
                    case "DOCX":
                        byte[] bFilePdf = clsSign.convertDocToPDF(strPathFileSave);
                        File.WriteAllBytes(strPathFileSave +".pdf", bFilePdf);
                        string[] param = new string[] { "HDI_PRIVATE", "PDF", signModel.TEMP_CODE, signModel.TEMP_CODE, "null" };
                        BaseResponse responseTemp = new BaseResponse();
                        responseTemp = goBusinessCommon.Instance.executeProHDI("GET_TEMPLATE", strEvm, param);
                        List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                        List<JObject> objTempValue1 = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);
                        if (objTemp == null || objTemp.Count == 0)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail khong co template ky so doc " + signModel.TEMP_CODE, Logger.ConcatName(nameClass, nameMethod));
                        }
                        else
                        {
                            string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID;
                            string strHeader = objTemp[0]["HEADER"].ToString();
                            strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                            string strFOOTER = objTemp[0]["FOOTER"].ToString();
                            bFilePdf = goBussinessPDF.Instance.convertPageToPDFPersnal_doc(strHeader, strFOOTER, bFilePdf);
                        }
                        strPathFileSave = strPathFileSave.Replace("." + signModel.EXT_FILE, ".pdf");
                        File.WriteAllBytes(strPathFileSave, bFilePdf);
                        break;
                    case "HTML":
                        byte[] bFilePdf1 = clsSign.convertHtmlToPDF(strPathFileSave);
                        strPathFileSave = strPathFileSave.Replace("." + signModel.EXT_FILE, ".pdf");
                        File.WriteAllBytes(strPathFileSave, bFilePdf1);
                        break;
                    case "PDF":
                        byte[] bFilePdf2 = File.ReadAllBytes(strPathFileSave);
                        string[] param1 = new string[] { "HDI_PRIVATE", "PDF", signModel.TEMP_CODE, signModel.TEMP_CODE, "null" };
                        BaseResponse responseTemp1 = new BaseResponse();
                        responseTemp = goBusinessCommon.Instance.executeProHDI("GET_TEMPLATE", strEvm, param1);
                        List<JObject> objTemp1 = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                        if (objTemp1 == null || objTemp1.Count == 0)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail khong co template ky so doc " + signModel.TEMP_CODE, Logger.ConcatName(nameClass, nameMethod));
                        }
                        else
                        {
                            string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID;
                            string strHeader = objTemp1[0]["HEADER"].ToString();
                            strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                            string strFOOTER = objTemp1[0]["FOOTER"].ToString();
                            bFilePdf2 = goBussinessPDF.Instance.convertPageToPDFPersnal_doc(strHeader, strFOOTER, bFilePdf2);
                        }
                        strPathFileSave = strPathFileSave.Replace("." + signModel.EXT_FILE, ".pdf");
                        File.WriteAllBytes(strPathFileSave, bFilePdf2);
                        break;
                }
                strPathFileSign = strPathFileSign.Replace("." + signModel.EXT_FILE, ".pdf");
                check = clsSign.findTextInPdf(objSignConfig[0]["TEXT_CA"]?.ToString(), strPathFileSave, ref x1, ref y1, ref width1, ref ipage);
                SignFileModel infoSign = new SignFileModel();
                infoSign.detail = new SignFileDetail();
                infoSign.PROVIDER = "HDI";
                infoSign.DEVICE = "HDI";
                infoSign.CER = strCerBasse64;
                infoSign.FILE_SIGN = Convert.ToBase64String(File.ReadAllBytes(strPathFileSave));
                infoSign.F12_FILE = p12Byte;
                infoSign.detail.X = ((Int16)x1 - iSangTraiX).ToString();//sang bên trái x
                infoSign.detail.Y = (((Int16)y1) - iXuongDuoiY).ToString();//xuống dưới y
                width1 = (width1 < height1 || width1 < 1.5 * height1 || width1 < 1.75 * height1 ? 2 * height1 : width1);
                infoSign.detail.WIDTH = ((Int16)width1 + 2 * Math.Abs(iSangTraiX)).ToString();                
                infoSign.detail.HEIGHT = (height1 + Math.Abs(iSangTraiX)).ToString();
                infoSign.detail.CONTACT = "";
                infoSign.detail.DISPLAY_TYPE = strTypeSign;
                infoSign.detail.IMAGE = image_sign;
                infoSign.detail.LOCATION = signModel.LOCATION;
                infoSign.detail.MESSAGE = "";
                infoSign.detail.PAGE = ipage.ToString();
                infoSign.detail.REASON = signModel.REASON;
                infoSign.detail.FIELD_NAME = signModel.FILE_REFF;
                if (check)
                {
                    //thực hiện ký số
                    string strFileKy = clsSign.signPDF(infoSign, objSignConfig[0]["PASSWORD"].ToString(), "");
                    if (string.IsNullOrEmpty(strFileKy))
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3005), "Lỗi khi thực hiện ký số");
                    }
                    //thực hiện đẩy file ký lên server
                    string strNameFile = "";
                    string strUrl = "";
                    BaseRequest request = new BaseRequest();
                    request.Action = new ActionInfo();
                    bool bUpload = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strFileKy), strPathFileSign,
                        "SignPDF/" + strOrgCode + "/" + DateTime.Now.ToString("yyyyMMdd"),
                        request, ref strNameFile, ref strUrl, strGuiID);
                    if (!bUpload)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong upload file len server" + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3007), "Lỗi khi thực hiện ký số");
                    }
                    //thực hiện đẩy vào dữ liệu
                    if (signModel.FILE.Length > 200)
                        signModel.FILE = "BASE64";
                    object[] param1 = new string[] { strOrgCode, "HDI", signModel.USER_NAME, "", objSignConfig[0]["ID_KEY"].ToString(),
                            JsonConvert.SerializeObject(signModel).ToString(), signModel.TEMP_CODE, signModel.USER_NAME,signModel.FILE,strGuiID,Convert.FromBase64String(strFileKy).Length.ToString(), signModel.FILE_REFF };//jInput['FILE'].ToString()
                    response = callPackage(goConstantsProcedure.Sign_Insert_Batch, strEvm, param1);
                    List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (response == null || objBatch == null || objBatch[0].Count == 0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so khong lay dc du lieu vao batch sign 1168", string.Join("-", param1));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không đẩy được dữ liệu vào DB ký số");
                    }
                    string strID_BATch = objBatch[0]["BATCH_ID"].ToString();
                    JObject jOutput = new JObject();
                    jOutput.Add("FILE_NAME", strNameFile);
                    jOutput.Add("FILE_URL", strUrl);
                    jOutput.Add("FILE_REFF", signModel.FILE_REFF);
                    jOutput.Add("FILE_BASE64", strFileKy);
                    //if (objHDBQR[0]["IS_ATTACH"].ToString().Equals("1"))
                    //{
                    //    jOutput.Add("ATTACH", objHDBQR[0]["IS_ATTACH"].ToString());
                    //    jOutput.Add("FILE_ATTACH", objHDBQR[0]["URL_PAGE_ADD"].ToString());
                    //}
                    //jOutput
                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }
                else
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), "Kiểm tra lại tọa độ ký số");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            return response;
        }

        #endregion
    }
}
