﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.PaymentCallBack;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DAO.Common;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Partner;
using GO.DTO.Payment;
using GO.DTO.Service;
using GO.HELPER.Utility;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using MessagingToolkit.QRCode.Codec;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.DTO.Payment.QRCodeModels;
using static GO.HELPER.Utility.goUtility;
using GO.DTO.Orders;
using GO.COMMON.Cache;
using GO.BUSINESS.Orders;
using System.Globalization;
using GO.SERVICE.Sign;

namespace GO.BUSINESS.Service
{
    public class goBussinessPayment
    {
        #region Contructor
        private static readonly Lazy<goBussinessPayment> InitInstance = new Lazy<goBussinessPayment>(() => new goBussinessPayment());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public static goBussinessPayment Instance => InitInstance.Value;
        #endregion
        #region Method

        #region PaymentGateway
        
        public BaseResponse createUrlPay(BaseRequest request) 
        {
            //khi click vào đơn vị thanh toán sẽ chuyển sang web của đơn vị đó
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strID_Trans = jInput["ID"].ToString(); //LibSign.Decrypt(jInput["ID"].ToString());
                    if (string.IsNullOrEmpty(strID_Trans))
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                    }else
                    {
                        jInput["ID"] = strID_Trans;
                    }
                    JObject jParam = new JObject();
                    jParam.Add("ID", strID_Trans);
                    TRANSACTION trans = new TRANSACTION();
                    response = goPaymentLib.Instance.dbJsonObjectParam(jParam, goConstantsProcedure.Pay_get_transaction);
                    if (response.Success)
                    {
                        List<TRANSACTION> lsTran = goBusinessCommon.Instance.GetData<TRANSACTION>(response, 0);
                        if(lsTran !=null && lsTran.Count > 0)
                        {
                            if (lsTran[0].STATUS.Equals("DONE"))
                            {
                                resLog= goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Đơn hàng đã được thanh toán");
                                return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Đơn hàng đã được thanh toán");
                            }
                            else
                            {
                                trans = lsTran[0];
                            }

                        }
                        else
                        {
                            resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy giao dịch thanh toán");
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy giao dịch thanh toán");
                        }
                    }
                    else
                    {
                        resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy giao dịch thanh toán");
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy giao dịch thanh toán");
                    }
                    //Lấy thông tin cấu hình đơn vị thanh toán
                    ConfigOrgModels configOrg = goPaymentLib.Instance.getConfigPayByOrg(jInput["ORG_CODE"].ToString(), "");
                    if(configOrg !=null && configOrg.LS_ORG_PAY.Count > 0)
                    {
                        //lấy xong thông tin giao dịch tạo url thanh toán
                        string strUrlPay = goPaymentLib.Instance.getUrlPay(trans, jInput["ORG_CODE"].ToString(), jInput["LINK_CALLBACK"].ToString(), configOrg.LS_ORG_PAY[0]);
                        JObject jOutPut = new JObject();
                        jOutPut.Add("URL_REDIRECT", strUrlPay);
                        response.Success = true;
                        response.Data = jOutPut;
                        resLog = response;
                        return response;
                    }
                    else
                    {
                        resLog= goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy đơn vị yêu cầu thanh toán");
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), "Không tìm thấy đơn vị yêu cầu thanh toán");
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }

            return response;
        }
        public BaseResponse ipnPaymentApi(SortedList<String, String> obj, string abc)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            BaseResponse response = new BaseResponse();
            //AuditLogsInfo auditLogs = new AuditLogsInfo();
            JObject objReturn = new JObject();
            objReturn.Add("RspCode", "00");
            objReturn.Add("Message", "Confirm Success");
            JObject jInput = new JObject();
            bool bCheckSign = false;
            try
            {
                //JObject trans1 = new JObject();
                //trans1.Add("value", "300000");
                //trans1.Add("currency", "VND");
                //JObject vat = new JObject();
                //vat.Add("value", "20000");
                //vat.Add("currency", "VND");
                //goPaymentLib.Instance.checkCallIPNPartner("CREDIFYVN", "C60A93DC34B41CB2E0530100007F8C55", trans1, "3000000;3000000", vat);
               
                response.Success = false;
                jInput = JObject.Parse(JsonConvert.SerializeObject(obj));
                ORG_PAY org_pay = goPaymentLib.Instance.getOrg_Code(JsonConvert.SerializeObject(obj));
                if (org_pay == null)
                {
                    response.Data = getResponseVNPAY("01", "Order not found");
                    goBussinessEmail.Instance.sendEmaiCMS("", "pay controler ipnPaymentApi khong tim thay cau hinh thanh toan ", JsonConvert.SerializeObject(obj));
                    return response;
                }
                TRANSACTION trans = new TRANSACTION();
                string strOrderId = "";
                if (org_pay.ORG_PAY_CODE.Equals("VNPAY"))
                {
                    VnPayLibrary vnpay = goPaymentLib.Instance.checkIpnVNPay(obj, ref bCheckSign);
                    if (vnpay == null)
                    {
                        response.Data = getResponseVNPAY("99", "Input data required");
                        goBussinessEmail.Instance.sendEmaiCMS("", "Input data required 170 ", JsonConvert.SerializeObject(obj));
                        return response;
                    }
                    if (!bCheckSign)
                    {
                        response.Data = getResponseVNPAY("97", "Invalid signature");
                        goBussinessEmail.Instance.sendEmaiCMS("", "vnpay Invalid signature 170 ", JsonConvert.SerializeObject(obj));
                        return response;
                    }
                    strOrderId = vnpay.GetResponseData("vnp_TxnRef");
                    // lấy thông tin giao dịch
                    trans = goPaymentLib.Instance.getTransByID(strOrderId);
                    if (trans == null)
                    {
                        response.Data = getResponseVNPAY("01", "Order not found");
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Order not found 176", Logger.ConcatName(nameClass, LoggerName));
                        return response;
                    }
                    if (!trans.TOTAL_AMOUNT.Equals(vnpay.GetResponseData("vnp_Amount").Substring(0, vnpay.GetResponseData("vnp_Amount").Length - 2)))
                    {
                        response.Data = getResponseVNPAY("04", "Invalid amount");
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Invalid amount 189", Logger.ConcatName(nameClass, LoggerName));
                        return response;
                    }
                    jInput["vnp_Amount"] = trans.TOTAL_AMOUNT;
                    if (trans.STATUS.Equals("DONE"))
                    {
                        response.Data = getResponseVNPAY("02", "Order already confirmed");
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Order not found 182", Logger.ConcatName(nameClass, LoggerName));
                        return response;
                    }
                    
                }
                else
                {
                    response.Data = getResponseVNPAY("02", "Chưa cấu hình đơn vị thanh toán");
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Order not found 176", Logger.ConcatName(nameClass, LoggerName));
                    return response;
                }
                //map dữ liệu 
                dataRequestOrderIpn data = goPaymentLib.Instance.convertJsonIPN(org_pay, jInput);
                string[] paramUpdate = new string[] { data.code, data.messageCode, data.paymentResult.order.orderCode, org_pay.ORG_PAY_CODE, data.paymentResult.order.payDate,
                    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,org_pay.PAY_TYPE, data.paymentResult.order.bankCode,"" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_Trans, strEvm, paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate == null || !objHDBQRUpdate.Any())
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái thanh toán  payment 202", JsonConvert.SerializeObject(data));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai hdi: " + JsonConvert.SerializeObject(obj) + JsonConvert.SerializeObject(data), Logger.ConcatName(nameClass, LoggerName));
                    response.Data = objReturn;
                    return response;
                }
                
                //set lai tham so tra ve
                data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
                data.paymentResult.order.orderCode = trans.TRANSACTION_TRACE;
                data.paymentResult.order.amount = trans.TOTAL_AMOUNT;
                //thực thi gọi api tuấn để cập nhật order
                ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                returnIPN.Success = true;
                returnIPN.Data = data;
                returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
                returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
                DateTime dtPay = DateTime.ParseExact(data.paymentResult.order.payDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy hh:mm:ss", "");
                //ở đây phải kiểm tra thêm trường hợp 
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
                BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN, objHDBQRUpdate[0]["ORDER_CODE"].ToString(), trans.TOTAL_AMOUNT);
                if (!respIPN_HDI.Success)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 225", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + JsonConvert.SerializeObject(obj) + JsonConvert.SerializeObject(data), Logger.ConcatName(nameClass, LoggerName));
                    string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, strEvm, strparam);
                    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 466", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                    }
                }
                response.Success = true;
                response.Data = objReturn;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Data = objReturn;
            }


            
            return response;
        }

        public JObject getResponseVNPAY(string strValue1, string strValue2)
        {
            JObject objReturn = new JObject();
            objReturn["RspCode"] = strValue1;
            objReturn["Message"] = strValue2;
            return objReturn;
        }
        public BaseResponse checkPayment(SortedList<String, String> obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            
            BaseResponse response = new BaseResponse();
            //AuditLogsInfo auditLogs = new AuditLogsInfo();
            StatusCheck check = goPaymentLib.Instance.checkResponsePay(obj);
            if (check == null)
            {
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                check.STATUS = "099";
                check.MESSAGE = "Giao dịch có lỗi chờ xử lý";
                check.LINK_CALLBACK = strURLWEBPay;
                response.Success = false;
                response.Data = check;
                return response;
            }
            if (!check.STATUS.Equals("000"))
            {
                response.Success = false;
                response.ErrorMessage = "Giao dịch không thành công";
                response.Data = check;
                return response;
            }
            
            response.Success = true;
            response.Data = check;
            return response;
        }

        public GenPaymentModels GetPaymentGateway(RequestPaymentModel request)
        {
            GenPaymentModels models = new GenPaymentModels();
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                string[] paramQR = new string[] { request.Id };
                response = resLog = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_transaction, environment, paramQR);
                List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objTRANS != null && objTRANS.Count > 0)
                {
                    List<PaymentInfo> lstPayInfo = new List<PaymentInfo>();
                    PaymentInfo info = new PaymentInfo();

                    info.Total_Amount = objTRANS[0]["TOTAL_AMOUNT"]?.ToString();
                    if(request.lang.Equals("EN"))
                        info.Total_Amount_Text = new GO.HELPER.Utility.goUtility().ConvertMoneyToText(objTRANS[0]["TOTAL_AMOUNT"]?.ToString(), MoneyFomat.VIET_TO_ENGLISH);
                    else
                        info.Total_Amount_Text = new GO.HELPER.Utility.goUtility().ConvertMoneyToText(objTRANS[0]["TOTAL_AMOUNT"]?.ToString(), MoneyFomat.MONEY_TO_VIET);
                    info.Amount = objTRANS[0]["AMOUNT"]?.ToString();
                    info.VAT = "0";
                    info.Gif_Code = "";
                    info.Discount = "0";
                    info.Transaction_Id = request.Id;
                    info.Payer_Name = objTRANS[0]["PAYER_NAME"]?.ToString();
                    if (request.lang.Equals("EN"))
                    {
                        info.Description = objTRANS[0]["ORDER_SUMMARY"]?.ToString();
                        if (objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                            info.Status_Name = "Wait for pay";
                        else
                            info.Status_Name = "Customer paid";                        
                    }                        
                    else
                    {
                        info.Description = objTRANS[0]["ORDER_TITLE"]?.ToString();
                        if (objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                            info.Status_Name = "Chờ thanh toán";
                        else
                            info.Status_Name = "Khách hàng đã thanh toán";
                    }    
                        
                    info.Status = objTRANS[0]["STATUS"]?.ToString();                    
                    info.Date_Pay = objTRANS[0]["CREATE_DATE"]?.ToString();
                    info.Path_Logo = "";
                    info.Path_Banner = "";
                    //info.Path_Logo_Qr = "";
                    string strQRImage = getQrCode(objTRANS[0]["QR_DATA"]?.ToString(), "Q", 8, "");
                    info.Qr_Code = strQRImage;
                    info.Qr_Expire = objTRANS[0]["PAY_DATE"]?.ToString();
                    info.Min_Amount = "10000";
                    info.Max_Amount = "50000000";
                    info.Currency = "VNĐ";
                    info.Contract_Code = objTRANS[0]["TRANSACTION_REF"]?.ToString();
                    info.Description_Min = "Số tiền tối thiểu của thanh toán qua mã QR là 10.000 VNĐ. Bằng việc quét mã QR, bạn chấp nhận thanh toán theo số tiền tối thiểu.";
                    info.Description_Max = "Số tiền tối đa của thanh toán qua mã QR là 50.000.000 VNĐ.";
                    lstPayInfo.Add(info);
                    List<JObject> objTransDetail = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                    ConfigOrgModels config = goPaymentLib.Instance.getAllConfigPayOrg(true);
                    List<PaymentGateway> lstPayGateway = new List<PaymentGateway>();
                    if (config != null && config.LS_ORG_PAY != null)
                    {
                        foreach (ORG_PAY org_pay in config.LS_ORG_PAY)
                        {
                            if (org_pay.PAY_TYPE.Equals("PAY"))
                            {
                                PaymentGateway hdbank = new PaymentGateway();
                                hdbank.Org_Code = org_pay.ORG_PAY_CODE;
                                hdbank.Org_Name = org_pay.CLIENT_NAME;
                                hdbank.Path_Logo = org_pay.LINK_LOGO;
                                hdbank.Min_Amount = org_pay.MIN_AMOUNT;
                                hdbank.Max_Amount = org_pay.MAX_AMOUNT;
                                hdbank.Description_Min = "";
                                hdbank.Description_Max = "";
                                hdbank.Disable = true;
                                hdbank.LinkPay = "";
                                //lấy link thanh toán đã được khởi tạo
                                //if (objTransDetail != null && objTransDetail.Any())
                                //{
                                //    foreach (JObject job in objTransDetail)
                                //    {
                                //        if (job["ORG_PAY_CODE"].ToString().Equals(org_pay.ORG_PAY_CODE) &&
                                //            job["TRANSACTION_ID"].ToString().Equals(objTRANS[0]["TRANSACTION_ID"].ToString()))
                                //        {
                                //            hdbank.LinkPay = job["URL_PAY"].ToString();
                                //        }
                                //        else
                                //            hdbank.LinkPay = "";
                                //    }
                                //}

                                lstPayGateway.Add(hdbank);
                            }
                            
                        }
                    }
                    //    string[] Param = new string[] { "ALL", "PAY", environment, "CREATE" };
                    //response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_Config, environment, Param);
                    //List<JObject> objHDBQR = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    
                    //foreach (JObject obj in objHDBQR)
                    //{
                    //    PaymentGateway hdbank = new PaymentGateway();
                    //    hdbank.Org_Code = obj["ORG_PAY_CODE"]?.ToString();
                    //    hdbank.Org_Name = obj["CLIENT_NAME"]?.ToString();
                    //    hdbank.Path_Logo = "Path/Logo_MOMO.png";
                    //    hdbank.Min_Amount = obj["MIN_AMOUNT"]?.ToString();
                    //    hdbank.Max_Amount = obj["MAX_AMOUNT"]?.ToString();
                    //    hdbank.Description_Min = "";
                    //    hdbank.Description_Max = "";
                    //    hdbank.Disable = true;
                    //    //lấy link thanh toán đã được khởi tạo

                    //    hdbank.LinkPay = "https://sandbox.vnpayment.vn/paymentv2/Payment/Error.html?code=01";
                    //    lstPayGateway.Add(hdbank);
                    //}

                    models.lstPayInfo = lstPayInfo;
                    models.lstPayGateway = lstPayGateway;
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "lỗi ngoại lệ hàm trả về cho webpayment payment 118", ex.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "GetPaymentGateway", request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return models;
        }
        public string createUrlVnpay()
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try { 

                }
                catch (Exception ex)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "lỗi ngoại lệ hàm trả về cho webpayment payment 145", ex.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                    
                }
            return "";
            
        }
        public GenPaymentModels findPayment(BaseRequest request)
        {
            GenPaymentModels models = new GenPaymentModels();
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string strUrlWebPay = "";
            string environment = "";
            string strInsuContract = "";
            GetEnvironmentIpn(ref environment, ref strUrlWebPay);
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string[] paramQR;
                    string strTransaction = jInput["TRANSACTION_ID"]?.ToString();
                    if (strTransaction.Equals(""))
                    {
                        string strGCN = jInput["INS_CONTRACT"]?.ToString();//mã hợp đồng bảo hiểm                   

                        //gọi hàm anh Khánh lấy trans
                        string[] paramFind;
                        if (!string.IsNullOrEmpty(strGCN))
                        {
                            paramFind = new string[] { "1", "1", strGCN, "NO", "GCN" };
                        }
                        else
                        {
                            string strLO = jInput["LO_CONTRACT"]?.ToString();
                            if (!string.IsNullOrEmpty(strLO))
                            {
                                paramFind = new string[] { "1", "1", strLO, "LO", "GCN" };
                            }
                            else
                            {
                                string strHDTD = jInput["INS_ID"]?.ToString();
                                paramFind = new string[] { "1", "1", strHDTD, "ID", "GCN" };
                            }

                        }

                        BaseResponse responseFind = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_gui, environment, paramFind);
                        List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                        if (objcontract.Count > 0)
                        {
                            strInsuContract = objcontract[0]["INSR"].ToString();
                            paramFind = new string[] { objcontract[0]["GUI"].ToString() };
                            response= resLog = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_trans_byGui, environment, paramFind);
                        }
                    }
                    else
                    {
                        paramQR = new string[] { strTransaction };
                        response = resLog = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_transaction, environment, paramQR);
                    }

                    List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (objTRANS != null && objTRANS.Count > 0)
                    {
                        List<PaymentInfo> lstPayInfo = new List<PaymentInfo>();
                        PaymentInfo info = new PaymentInfo();
                        models.INS_CONTRACT = strInsuContract;
                        info.Total_Amount = objTRANS[0]["TOTAL_AMOUNT"]?.ToString();
                        info.Total_Amount_Text = new GO.HELPER.Utility.goUtility().ConvertMoneyToText(objTRANS[0]["TOTAL_AMOUNT"]?.ToString(), MoneyFomat.MONEY_TO_VIET);
                        //info.Amount = objTRANS[0]["AMOUNT"]?.ToString();
                        info.VAT = "0";
                        //info.Gif_Code = "";
                        info.Amount = objTRANS[0]["AMOUNT"]?.ToString();
                        info.Discount = string.IsNullOrEmpty(objTRANS[0]["DISCOUNT"]?.ToString()) ? "0" : objTRANS[0]["DISCOUNT"].ToString();
                        info.Transaction_Id = objTRANS[0]["TRANSACTION_ID"]?.ToString();
                        info.Payer_Name = objTRANS[0]["PAYER_NAME"]?.ToString();
                        info.Description = objTRANS[0]["ORDER_TITLE"]?.ToString();
                        info.Status = objTRANS[0]["STATUS"]?.ToString();
                        if (objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                            info.Status_Name = "Chờ thanh toán";
                        else if (objTRANS[0]["STATUS"].ToString().Equals("DONE"))
                            info.Status_Name = "Khách hàng đã thanh toán";
                        else
                            info.Status_Name = "Khách hàng đã thanh toán thành công";
                        info.Date_Pay = objTRANS[0]["CREATE_DATE"]?.ToString();
                        info.Path_Logo = "";
                        info.Path_Banner = "";
                        //info.Path_Logo_Qr = "";
                        string strQRImage = getQrCode(objTRANS[0]["QR_DATA"]?.ToString(), "Q", 8, "");
                        info.Qr_Code = strQRImage;
                        info.Qr_Expire = objTRANS[0]["PAY_DATE"]?.ToString();
                        //info.Min_Amount = "10000";
                        //info.Max_Amount = "50000000";
                        info.Currency = "VNĐ";
                        info.Contract_Code = objTRANS[0]["TRANSACTION_TRACE"]?.ToString();
                        //info.Description_Min = "Số tiền tối thiểu của thanh toán qua mã QR là 10.000 VNĐ. Bằng việc quét mã QR, bạn chấp nhận thanh toán theo số tiền tối thiểu.";
                        //info.Description_Max = "Số tiền tối đa của thanh toán qua mã QR là 50.000.000 VNĐ.";
                        lstPayInfo.Add(info);
                        models.lstPayInfo = lstPayInfo;
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }

            return models;
        }

        #endregion
        #region Method+

        public BaseResponse ipnHDITrans(IpnHDIModels obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();            
            try
            {                
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN HDI : " + JsonConvert.SerializeObject(obj), Logger.ConcatName(nameClass, LoggerName));
                //List<IpnHDIModels> obj1 = JsonConvert.DeserializeObject< List<IpnHDIModels>>(request.Data.ToString());
                //IpnHDIModels obj = obj1[0];
                dataRequestOrderIpn data = new dataRequestOrderIpn();                
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                var paramFind = new string[] { obj.order_trans_code };//trong bảng B_ORDER_TRANS
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_trans_byGui, environment, paramFind);
                List<JObject> objTransaction = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objTransaction.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không lấy được thông tin giao dịch HDI-ipn payment 296", JsonConvert.SerializeObject(obj));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không lấy được thông tin giao dịch HDI-ipn: " + JsonConvert.SerializeObject(obj), Logger.ConcatName(nameClass, LoggerName));
                    response.Success = false;
                    return response;
                }
                //set lai tham so tra ve
                data.paymentResult = new paymentResult();
                data.paymentResult.order = new orderIpn();
                data.paymentResult.order.transactionNo = objTransaction[0]["TRANSACTION_ID"].ToString(); //transid c_payment
                data.paymentResult.order.orderCode = objTransaction[0]["ORDER_CODE"].ToString(); //order_code c_payment
                data.paymentResult.order.amount = obj.amount;
                data.paymentResult.order.totalRefundedAmount = "0";

                
                //thực thi gọi api tuấn để cập nhật order
                ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                data.paymentResult.order.totalRefundedAmount = "0";
                data.paymentResult.order.bankCode = objTransaction[0]["ORG_PAY_CODE"].ToString();
                returnIPN.Success = true;
                returnIPN.Data = data;
                returnIPN.Data.paymentResult.paymentType = obj.pay_type;
                returnIPN.Data.paymentResult.payMethod = obj.pay_type;
                returnIPN.Data.paymentResult.order.totalCapturedAmount = obj.amount;
                returnIPN.Data.paymentResult.order.totalAuthorizedAmount = obj.amount;
                returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(goUtility.Instance.stringToDate(objTransaction[0]["PAY_DATE"].ToString(), "dd/MM/yyyy hh:mm:ss", DateTime.MinValue), "dd/MM/yyyy hh:mm:ss", "");
                //ở đây phải kiểm tra thêm trường hợp 
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
                BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN,obj.order_trans_code,obj.amount);
                if (!respIPN_HDI.Success)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 326", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    string[] strparam = new string[] { objTransaction[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
                    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 332", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                    }//kirmt ýt
                    response.Success = false;
                }
                else
                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 342", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);               
                response.Success = false;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnHDITrans", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        public BaseResponse ipnQRVnpay(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                JObject jConfig = new JObject();
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "IPN QR VNPAY", obj.ToString());
                dataRequestOrderIpn data = convertJsonIPN(JObject.Parse(obj.ToString()), ref jConfig);
                if (data == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán  payment 380", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    response.Data = objReturn;
                    return response;
                }
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                // thực thi cập nhật dữ liệu
                //object[] paramUpdate = new object[] { data.code, data.messageCode, data.paymentResult.order.orderCode, jConfig["ORG_PAY_CODE"].ToString(), data.paymentResult.order.amount,
                //    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,jConfig["PAY_TYPE"].ToString(),"X" };
                //response = callPackage(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
                //data.paymentResult.order.bankCode
                string[] paramUpdate = new string[] { data.code, data.messageCode, data.paymentResult.order.orderCode, jConfig["ORG_PAY_CODE"].ToString(), data.paymentResult.order.amount,
                    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,jConfig["PAY_TYPE"].ToString(), data.paymentResult.order.bankCode ,""};
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái thanh toán  payment 399", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    response.Data = objReturn;
                    return response;
                }
                //thực thi gọi api webpaymet để cập nhật trạng thái
                List<JObject> lsOrg = new List<JObject>();
                List<JObject> lsFun = new List<JObject>();
                if (getORG_PAY("HDI", "CALLBACKIN", "CALL", ref lsOrg, ref lsFun))
                {
                    //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong getORG_PAY: ", Logger.ConcatName(nameClass, LoggerName));
                    string[] Param = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), environment };

                    BaseResponse response1 = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_ORG_Config, environment, Param);
                    List<JObject> objorg = goBusinessCommon.Instance.GetData<JObject>(response1, 0);
                    if (lsFun.Count > 0)
                    {
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(objHDBQRUpdate), Logger.ConcatName(nameClass, LoggerName));
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(lsFun) + JsonConvert.SerializeObject(objorg) + Param.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        string strUrlCallBack = "";
                        if (!string.IsNullOrEmpty(objorg[0]["URL_CALLBACK"]?.ToString()))
                        {
                            //phải bổ xung thêm tham số vào urlcalback
                            strUrlCallBack = objorg[0]["URL_CALLBACK"]?.ToString();
                        }
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 1: " + JsonConvert.SerializeObject(lsFun[0]), Logger.ConcatName(nameClass, LoggerName));
                        bool b = new goLibQR().callWebPayment(objHDBQRUpdate[0]["DATA_CHECK"].ToString(), strUrlCallBack, lsFun[0]);
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 2: " + b.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        if (!b)
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật webpayment  payment 429", data.paymentResult.order.transactionNo + objorg[0]["URL_CALLBACK"].ToString());
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai webpayment : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        }
                    }

                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY  payment 437", "HDI-callback");
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY", Logger.ConcatName(nameClass, LoggerName));
                }
                objReturn["message"] = "Cập nhật thành công";
                objCT["txnId"] = data.paymentResult.order.orderCode;
                objReturn["data"] = objCT;
                //set lai tham so tra ve
                data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
                data.paymentResult.order.orderCode = objHDBQRUpdate[0]["ORDER_CODE"].ToString();
                data.paymentResult.order.amount = objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString();
                //thực thi gọi api tuấn để cập nhật order
                ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                returnIPN.Success = true;
                returnIPN.Data = data;
                returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
                returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
                DateTime dtPay = DateTime.ParseExact(data.paymentResult.order.payDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy hh:mm:ss", "");
                //ở đây phải kiểm tra thêm trường hợp 
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
                BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN, objHDBQRUpdate[0]["ORDER_CODE"].ToString(), objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString());
                if (!respIPN_HDI.Success)
                {
                    objReturn["code"] = "03";
                    objReturn["message"] = "Đơn hàng đã được thanh toán";
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 460", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
                    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 466", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                    }//kirmt ýt
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnQRVnpay", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = objReturn;
            return response;
        }

        public BaseResponse ipnHdbank(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                JObject jConfig = new JObject();
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                dataRequestOrderIpn data = convertJsonIPN(JObject.Parse(obj.ToString()), ref jConfig);
                if (data == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán  payment 380", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    response.Data = objReturn;
                    return response;
                }
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                // thực thi cập nhật dữ liệu
                //object[] paramUpdate = new object[] { data.code, data.messageCode, data.paymentResult.order.orderCode, jConfig["ORG_PAY_CODE"].ToString(), data.paymentResult.order.amount,
                //    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,jConfig["PAY_TYPE"].ToString(),"X" };
                //response = callPackage(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
                //data.paymentResult.order.bankCode
                string[] paramUpdate = new string[] { data.code, data.messageCode, data.paymentResult.order.orderCode, jConfig["ORG_PAY_CODE"].ToString(), data.paymentResult.order.amount,
                    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,jConfig["PAY_TYPE"].ToString(), data.paymentResult.order.bankCode,"" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái thanh toán  payment 399", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    response.Data = objReturn;
                    return response;
                }
                //thực thi gọi api webpaymet để cập nhật trạng thái
                List<JObject> lsOrg = new List<JObject>();
                List<JObject> lsFun = new List<JObject>();
                if (getORG_PAY("HDI", "CALLBACKIN", "CALL", ref lsOrg, ref lsFun))
                {
                    //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong getORG_PAY: ", Logger.ConcatName(nameClass, LoggerName));
                    string[] Param = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), environment };

                    BaseResponse response1 = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_ORG_Config, environment, Param);
                    List<JObject> objorg = goBusinessCommon.Instance.GetData<JObject>(response1, 0);
                    if (lsFun.Count > 0)
                    {
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(objHDBQRUpdate), Logger.ConcatName(nameClass, LoggerName));
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(lsFun) + JsonConvert.SerializeObject(objorg) + Param.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        string strUrlCallBack = "";
                        if (!string.IsNullOrEmpty(objorg[0]["URL_CALLBACK"]?.ToString()))
                        {
                            //phải bổ xung thêm tham số vào urlcalback
                            strUrlCallBack = objorg[0]["URL_CALLBACK"]?.ToString();
                        }
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 1: " + JsonConvert.SerializeObject(lsFun[0]), Logger.ConcatName(nameClass, LoggerName));
                        bool b = new goLibQR().callWebPayment(objHDBQRUpdate[0]["DATA_CHECK"].ToString(), strUrlCallBack, lsFun[0]);
                        //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 2: " + b.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        if (!b)
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật webpayment  payment 429", data.paymentResult.order.transactionNo + objorg[0]["URL_CALLBACK"].ToString());
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai webpayment : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        }
                    }

                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY  payment 437", "HDI-callback");
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY", Logger.ConcatName(nameClass, LoggerName));
                }
                objReturn["message"] = "Cập nhật thành công";
                objCT["txnId"] = data.paymentResult.order.orderCode;
                objReturn["data"] = objCT;
                //set lai tham so tra ve
                data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
                data.paymentResult.order.orderCode = objHDBQRUpdate[0]["ORDER_CODE"].ToString();
                data.paymentResult.order.amount = objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString();
                //thực thi gọi api tuấn để cập nhật order
                ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                returnIPN.Success = true;
                returnIPN.Data = data;
                returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
                returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
                DateTime dtPay = DateTime.ParseExact(data.paymentResult.order.payDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy hh:mm:ss", "");
                //ở đây phải kiểm tra thêm trường hợp 
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
                BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN, objHDBQRUpdate[0]["ORDER_CODE"].ToString(), objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString());
                if (!respIPN_HDI.Success)
                {
                    objReturn["code"] = "03";
                    objReturn["message"] = "Đơn hàng đã được thanh toán";
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 460", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                   string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
                    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count ==0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 466", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                    }//kirmt ýt
                }
                  
                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnHdbank", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = objReturn;
            return response;
        }
        public BaseResponse getSetTransFer(BaseRequest request)
        {   
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);            
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response= resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    request.Data = jInput;
                    goBusinessCommon.Instance.GetSignRequest(ref request, config);
                    response= resLog = goBusinessAction.Instance.GetBaseResponse(request);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }

            return response;
        }

        public JObject updateTransferPayment(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                IpnBankTransfer tempParam = new IpnBankTransfer();
                tempParam = JsonConvert.DeserializeObject<IpnBankTransfer>(obj.ToString());
                if (tempParam == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán payment 559", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi cấu trúc JSON";
                    //response.Data = objReturn;
                    return objReturn;
                }
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                //CHECK MD5 DỮ LIỆU accountNumber|accountHolderName|bankHolderName| totalAmount |secretKey
                string strMD5 = goEncryptBase.Instance.Md5Encode(tempParam.accountNumber + "|" + tempParam.accountHolderName + "|" + tempParam.bankHolderName + "|" + tempParam.totalAmount + "|B229FBB474507F46E0551F6E21B40329");
                if (!strMD5.Equals(tempParam.checksum.ToUpper()))
                {
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi check sum";
                    return objReturn;
                }
                List<TransferDetailInsert> lsOutput = convertInsertTransfer(tempParam);
                
                // thực thi cập nhật dữ liệu   
                // "a,b,c" -> 1 param
                string[] paramUpdate = new string[] { tempParam.orgCode, "NULL", "0", JsonConvert.SerializeObject(lsOutput) };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_TransFer, environment, paramUpdate);
                List<JObject> lsUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (lsUpdate.Count > 0)
                {
                    string strOrderList = "";
                    foreach(JObject obj1 in lsUpdate)
                    {
                        strOrderList = strOrderList + obj1["ORDER_CODE"].ToString() + ";";
                    }
                    BaseRequest req = goBusinessCommon.Instance.GetBaseRequestHDI(environment);                    
                    req.Action.ActionCode = "APIWMPKDJV";
                    var param = new
                    {
                        CHANNEL = "WEB",
                        USERNAME = "PAYMENT",
                        DATA = strOrderList
                    };
                    req.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                    BaseResponse resApprove = goBusinessAction.Instance.GetBaseResponse(req);
                    if (resApprove.Success)
                    {
                        List<OrdersTransferApprove> lstApprove = goBusinessCommon.Instance.GetData<OrdersTransferApprove>(resApprove, 0);
                        //JArray array = new JArray();
                        foreach (var item in lstApprove)
                        {
                            IpnHDIModels obj2 = new IpnHDIModels();
                            obj2.amount = item.TOTAL_PAY.ToString();
                            obj2.pay_type = "CK";
                            obj2.order_trans_code = item.ORDER_TRANS_CODE;
                            obj2.message = "Thành công";
                            obj2.code = "00";
                            obj2.trans_id = "-1";
                            BaseResponse res = goBussinessPayment.Instance.ipnHDITrans(obj2);
                            if (!res.Success)
                            {
                                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cấp đơn tự động chuyển khoản ", JsonConvert.SerializeObject(req));
                            }
                        }
                        response.Success = true;
                    }

                }
                if (response.Success)
                {
                    objReturn["message"] = "Cập nhật thành công";
                    objReturn["data"] = response.Data.ToString();
                }
                else
                {
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi cập nhật dữ liệu 1";
                    objReturn["data"] = response.ErrorMessage;
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán payment 609 ", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "updateTransferPayment", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            //response.Data = objReturn;
            return objReturn;
        }

        public JObject ipnBankTransfer(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                IpnBankTransfer tempParam = new IpnBankTransfer();
                tempParam = JsonConvert.DeserializeObject<IpnBankTransfer>(obj.ToString());
                
                //dataRequestOrderIpn data = convertJsonIPN(JObject.Parse(obj.ToString()), ref jConfig);
                if (tempParam == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán  payment 651", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi cấu trúc JSON";
                    //response.Data = objReturn;
                    return objReturn;
                }
                
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                //CHECK MD5 DỮ LIỆU accountNumber|accountHolderName|bankHolderName| totalAmount |secretKey
                string strMD5 = goEncryptBase.Instance.Md5Encode(tempParam.accountNumber+"|" + tempParam.accountHolderName+"|"+tempParam.bankHolderName+"|" + tempParam.totalAmount + "|B229FBB474507F46E0551F6E21B40329");
                if (!strMD5.Equals(tempParam.checksum.ToUpper()))
                {
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi check sum";
                    //response.Data = objReturn;
                    return objReturn;
                }

                List<TransferDetailInsert> lsOutput = convertInsertTransfer(tempParam);
                string strParamIn = "";
                foreach(TransferDetailInsert tmp in lsOutput)
                {
                    strParamIn = strParamIn + tmp.orderCode + ",";
                }
                // thực thi cập nhật dữ liệu   
                // "a,b,c" -> 1 param
                string[] paramUpdate = new string[] { tempParam.orgCode, strParamIn, lsOutput.Count.ToString(), JsonConvert.SerializeObject(lsOutput)};
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_TransFer, environment, paramUpdate);
                List<JObject> lsUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (lsUpdate.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái thanh toán payment 685", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    objReturn["code"] = "01";
                    objReturn["message"] = "Lỗi cập nhật trạng thái, không tìm thấy giao dịch " + response.ErrorMessage;
                    //response.Data = objReturn;
                    return objReturn;
                }
                
                objReturn["message"] = "Cập nhật thành công";
                JArray arrOrderUpdate = new JArray();
                foreach(JObject objCTCN in lsUpdate)
                {
                    objCT["txnId"] = objCTCN["ORDER_CODE"].ToString();
                    arrOrderUpdate.Add(objCT);
                }                
                objReturn["data"] = arrOrderUpdate;
                //set lai tham so tra ve
                //data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
                //data.paymentResult.order.orderCode = objHDBQRUpdate[0]["ORDER_CODE"].ToString();
                //data.paymentResult.order.amount = objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString();
                ////thực thi gọi api tuấn để cập nhật order
                //ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                //returnIPN.Success = true;
                //returnIPN.Data = data;
                //returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
                //returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
                //returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(goUtility.Instance.stringToDate(data.paymentResult.order.payDate, "yyyyMMddhhmmss", DateTime.MinValue), "dd/MM/yyyy hh:mm:ss", "");
                ////ở đây phải kiểm tra thêm trường hợp 
                //BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN);
                //if (!respIPN_HDI.Success)
                //{
                //    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                //    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                //    string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                //    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
                //    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
                //    {
                //        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error ", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                //    }//kirmt ýt
                //}
                ////thực thi gọi api webpaymet để cập nhật trạng thái
                //List<JObject> lsOrg = new List<JObject>();
                //List<JObject> lsFun = new List<JObject>();
                //if (getORG_PAY("HDI", "CALLBACKIN", "CALL", ref lsOrg, ref lsFun))
                //{
                //    string[] Param = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), environment };
                //    BaseResponse response1 = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_ORG_Config, environment, Param);
                //    List<JObject> objorg = goBusinessCommon.Instance.GetData<JObject>(response1, 0);
                //    if (objorg.Count > 0)
                //    {
                //        bool b = new goLibQR().callWebPayment(objHDBQRUpdate[0]["DATA_CHECK"].ToString(), objorg[0]["URL_CALLBACK"].ToString(), lsFun[0]);
                //        if (!b)
                //        {
                //            goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật webpayment ", data.paymentResult.order.transactionNo + objorg[0]["URL_CALLBACK"].ToString());
                //            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai webpayment : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                //        }
                //    }

                //}
                //else
                //{
                //    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY", "HDI-callback");
                //    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY", Logger.ConcatName(nameClass, LoggerName));
                //}
                //response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 754", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnBankTransfer", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            //response.Data = objReturn;
            return objReturn;
        }
        public List<TransferDetailInsert> convertInsertTransfer(IpnBankTransfer param)
        {
            List<TransferDetailInsert> lsOutput = new List<TransferDetailInsert>();
            
            foreach(TransferDetail detail in param.data)
            {
                TransferDetailInsert outPut = new TransferDetailInsert();
                outPut.accountHolderName = param.accountHolderName;
                outPut.accountNumber = param.accountNumber;
                outPut.amount = detail.amount;
                outPut.bankHolderName = param.bankHolderName;
                outPut.bankName = detail.bankName;
                outPut.cardAddress = detail.cardAddress;
                outPut.cardName = detail.cardName;
                outPut.cardNumber = detail.cardNumber;
                outPut.code = detail.code;
                outPut.currency = detail.currency;
                outPut.detailsOfPayment = detail.detailsOfPayment;
                string strOrder = "NULL";
                List<string> lsContent = detail.detailsOfPayment.Trim().Split(' ').ToList();
                for (int i = 0 ;i<lsContent.Count;i++)
                {
                    if (lsContent[i].ToUpper().StartsWith("HDI") && lsContent[i].Trim().Length >6)
                    {
                        strOrder = lsContent[i].Trim().Substring(6);
                        break;
                    }
                    if(lsContent[i].ToUpper().StartsWith("HDI") && lsContent[i].Trim().Length < 7 && lsContent.Count> i)
                    {
                        strOrder = lsContent[i+1].Trim();
                        break;
                    }
                }
                outPut.orderCode = strOrder;// detail.detailsOfPayment; //xử lý để lấy order code
                outPut.effectiveDate = detail.effectiveDate;
                outPut.message = detail.message;
                outPut.orderNumber = detail.orderNumber;
                outPut.orgCode = param.orgCode;
                outPut.terminalId = detail.terminalId;
                outPut.totalAmount = param.totalAmount;
                outPut.transactionDate = detail.transactionDate;
                outPut.transactionID = detail.transactionID;
                outPut.transactionType = detail.transactionType;
                lsOutput.Add(outPut);
            }
            return lsOutput;
        }
        //public BaseResponse paymentnew(BaseRequest request, goConstants.Payment_type type = goConstants.Payment_type.CTT)
        //{
        //    string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    BaseResponse response = new BaseResponse();
        //    AuditLogsInfo auditLogs = new AuditLogsInfo();
        //    ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
        //    BaseRequest reGoc = request;
        //    try
        //    {
        //        if (config == null)
        //        {
        //            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
        //            return response;
        //        }
                
        //        string strUrlWebPay = "", environment = "";
        //        string action = request.Action.ActionCode; /*goConstantsProcedure.Pay_Get_Config----->HDB_QR*/
        //        GetEnvironmentIpn(ref environment, ref strUrlWebPay);
        //        if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
        //        {
        //            Order_Create orderCreate = JsonConvert.DeserializeObject<Order_Create>(request.Data.ToString());
        //            if (orderCreate == null)
        //            {
        //                Logger.Instance.WriteLog(Logger.TypeLog.ERROR,"Loi tao transaction pay" + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
        //                return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
        //            }                        
        //            if (string.IsNullOrEmpty(orderCreate.tipAndFee))
        //                orderCreate.tipAndFee = "0";
        //            JObject jInput = JObject.Parse(JsonConvert.SerializeObject(request.Data));
        //            string str_orderId = jInput["orderId"].ToString();
        //            string str_amount = jInput["amount"].ToString();
        //            string str_payDate = jInput["payDate"].ToString();
        //            string str_desc = jInput["desc"].ToString();
        //            string str_desc_en = jInput["desc_en"].ToString();
        //            string str_tipAndFee = string.IsNullOrEmpty(jInput["tipAndFee"]?.ToString()) ? "0" : jInput["tipAndFee"].ToString();
        //            string str_terminalId = jInput["terminalId"]?.ToString();
        //            string str_Name = jInput["payer_name"].ToString();
        //            string str_Phone = jInput["payer_mobile"]?.ToString();
        //            string str_Email = jInput["payer_email"]?.ToString();
        //            string str_StructCode = jInput["struct_code"]?.ToString();
        //            string str_contentPay = jInput["contentPay"]?.ToString();
        //            //khởi tạo QR code nếu giao dịch phục vụ cổng thanh toán
        //            string strTransactionID = Guid.NewGuid().ToString("N").Substring(0, 8) + DateTime.Now.ToString("MMHHmmss");// DateTime.Now.ToString("ddMMyyyy");
        //            string strQRData = "CK";
        //            string strTaxID = "-1";
        //            string strImageData = "";
        //            if (type.Equals(goConstants.Payment_type.CTT))
        //            {
        //                ConfigOrgModels configPay = goPaymentLib.Instance.getConfigCreatePay("", str_StructCode, "QR");
        //                if (configPay != null)
        //                {
        //                    PAY_FUNTION payFuntion = configPay.LS_ORG_PAY[0].LS_FUNTION.Where(m => m.TYPE_METHOD == "CREATE").FirstOrDefault();
        //                    if (configPay.LS_ORG_PAY[0].ORG_PAY_CODE.Equals("VNPAY1") && configPay.LS_ORG_PAY[0].PAY_TYPE.Equals("QR"))
        //                    {
        //                        //sử dụng QR của VNPAY
        //                        try
        //                        {
        //                            goPayQrcode qrVNAPY = new goPayQrcode();
        //                            JObject QRResult = qrVNAPY.createQr(strTransactionID,str_amount,str_payDate,str_desc,str_tipAndFee,configPay.LS_ORG_PAY[0]);
        //                            if (!QRResult["code"].ToString().Equals("000"))
        //                            {
        //                                //return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", QRResult["code"].ToString(), QRResult["message"].ToString());
        //                                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được QR cho giao dịch payment 869", QRResult["code"].ToString() + QRResult["message"].ToString());
        //                                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + request, Logger.ConcatName(nameClass, LoggerName));
        //                            }
        //                            strQRData = QRResult["dataQR"].ToString();
        //                            strTaxID = QRResult["txnId"].ToString();
        //                            strImageData = QRResult["imageQR"]?.ToString();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //                        }
        //                    }
        //                    if (configPay.LS_ORG_PAY[0].ORG_PAY_CODE.Equals("HDBANK") && configPay.LS_ORG_PAY[0].PAY_TYPE.Equals("QR"))
        //                    {
        //                        try
        //                        {
                                    
        //                            JObject QRResult = createQr(strTransactionID, str_amount, str_payDate, str_desc, str_tipAndFee, 
        //                             JObject.FromObject(configPay.LS_ORG_PAY[0]), JObject.FromObject(payFuntion));
        //                            if (!QRResult["code"].ToString().Equals("000"))
        //                            {
        //                                //return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", QRResult["code"].ToString(), QRResult["message"].ToString());
        //                                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được QR cho giao dịch payment 869", QRResult["code"].ToString() + QRResult["message"].ToString());
        //                                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + request, Logger.ConcatName(nameClass, LoggerName));
        //                            }
        //                            strQRData = QRResult["dataQR"].ToString();
        //                            strTaxID = QRResult["txnId"].ToString();
        //                            strImageData = QRResult["imageQR"]?.ToString();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //                        }
        //                    }
        //                }
        //            }
        //            //đẩy dữ liệu vào bảng giao dịch
        //            string strMD5 = goEncryptBase.Instance.Md5Encode(str_orderId + str_amount);
        //            string[] ParamInsert = new string[] { strTransactionID, "HDBANK", config.partnerCode, "CREATE", str_orderId, str_desc , str_desc_en , str_amount , str_amount ,
        //                                                    "VN", goUtility.Instance.ConvertToDateTime(str_payDate, DateTime.MinValue).ToString("yyyyMMdd"),strQRData,strTaxID,
        //                                        strTransactionID,str_terminalId,strMD5,str_Name,str_Phone,str_Email,str_tipAndFee, "FALSE","[]"};
        //            JObject JoutPut = new JObject();
        //            request.Data = goUtility.Instance.convertArrayToJsonParam(ParamInsert);
        //            request.Action.ActionCode = goConstantsProcedure.Pay_create_transaction;
        //            response = goBusinessAction.Instance.GetBaseResponse(request);
        //            //response = callPackage(goConstantsProcedure.Pay_create_transaction, environment, ParamInsert);
        //            if (response.Success)
        //            {
        //                //JoutPut.Add("code", "000");
        //                //JoutPut.Add("message", "Khoi tao giao dich thanh cong");
        //                List<JObject> objKQ = goBusinessCommon.Instance.GetData<JObject>(response, 0);
        //                if (objKQ[0]["TRANSACTION_ID"].ToString().Equals("X"))
        //                {
        //                    goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng payment 902", JsonConvert.SerializeObject(reGoc));
        //                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng: " + reGoc, Logger.ConcatName(nameClass, LoggerName));
        //                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "01", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng");
        //                }
        //                JoutPut.Add("transactionNo", strTransactionID);
        //                JoutPut.Add("payDate", str_payDate);
        //                JoutPut.Add("appId", config.partnerCode);
        //                JoutPut.Add("bankCode", "HDI_PAY");
        //                JoutPut.Add("amount", str_amount);
        //                JoutPut.Add("url_redirect", strUrlWebPay + "?id=" + strMD5);
        //                JoutPut.Add("imageQR", strImageData);
        //                JoutPut.Add("dataQR", strQRData);
        //                JoutPut.Add("orderId", str_orderId);
        //            }
        //            else
        //            {
        //                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 918", JsonConvert.SerializeObject(response));
        //                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
        //                return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", response.Error, response.ErrorMessage);
        //                //JoutPut.Add("code", response.Error);
        //                //JoutPut.Add("message", response.ErrorMessage);
        //            }

        //            return goBusinessCommon.Instance.getResultApi(response.Success, JoutPut, config, response.Error, response.ErrorMessage);
        //        }
        //        else
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 929", JsonConvert.SerializeObject(response));
        //            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
        //            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_4001), goConstantsError.Instance.ERROR_4001);
        //            return response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 937", JsonConvert.SerializeObject(request) + ex.ToString());
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
        //    }
        //    finally
        //    {
        //        try
        //        {
        //            auditLogs.request = request;
        //            auditLogs.response = response;
        //            new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        //        }
        //    }
        //    return response;
        //}
        public BaseResponse payment(BaseRequest request, goConstants.Payment_type type = goConstants.Payment_type.CTT)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            BaseRequest reGoc = request;
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
                string strUrlWebPay = "";
                string environment = "";
                string action = request.Action.ActionCode; /*goConstantsProcedure.Pay_Get_Config----->HDB_QR*/

                //ExecuteInfo GetInfoExecute 
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                //Lấy thông tin của đơn vị gọi thanh toán                
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    string[] Param = new string[] { "HDBANK", "QR", environment, "CREATE" };
                    response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_Config, environment, Param);
                    List<JObject> objHDBQR = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    List<JObject> objHDBQR_FUNC = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                    JObject jInput = JObject.Parse(JsonConvert.SerializeObject(request.Data));
                    string str_orderId = jInput["orderId"].ToString();
                    string str_amount = jInput["amount"].ToString();
                    string str_payDate = jInput["payDate"].ToString();
                    string str_desc = jInput["desc"].ToString();
                    string str_desc_en = jInput["desc_en"].ToString();
                    string str_tipAndFee = string.IsNullOrEmpty(jInput["tipAndFee"]?.ToString()) ? "0" : jInput["tipAndFee"].ToString();
                    string str_terminalId = jInput["terminalId"]?.ToString();
                    string str_Name = jInput["payer_name"].ToString();
                    string str_Phone = jInput["payer_mobile"]?.ToString();
                    string str_Email = jInput["payer_email"]?.ToString();
                    string str_StructCode = jInput["struct_code"]?.ToString();

                    string strTransactionID = Guid.NewGuid().ToString("N").Substring(0, 8) + DateTime.Now.ToString("MMHHmmss");// DateTime.Now.ToString("ddMMyyyy");
                    string strQRData = "CK";
                    string strTaxID = "-1";
                    string strImageData = "";
                    if (type.Equals(goConstants.Payment_type.CTT))
                    {
                        // goi hdb

                        try
                        {
                            JObject QRResult = createQr(strTransactionID, str_amount, str_payDate, str_desc, str_tipAndFee, objHDBQR[0], objHDBQR_FUNC[0]);
                            if (!QRResult["code"].ToString().Equals("000"))
                            {
                                //return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", QRResult["code"].ToString(), QRResult["message"].ToString());
                                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được QR cho giao dịch payment 869", QRResult["code"].ToString() + QRResult["message"].ToString());
                                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + request, Logger.ConcatName(nameClass, LoggerName));
                            }
                            strQRData = QRResult["dataQR"].ToString();
                            strTaxID = QRResult["txnId"].ToString();
                            strImageData = QRResult["imageQR"]?.ToString();
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được QR cho giao dịch: " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                        }
                        //tạo sẵn url cho đơn vị auto thanh toán
                        try
                        {
                            //Lấy thông tin cấu hình đơn vị thanh toán
                            TRANSACTION trans = new TRANSACTION();
                            trans.ORDER_TITLE = str_desc;
                            trans.TRANSACTION_ID = strTransactionID;
                            trans.TOTAL_AMOUNT = str_amount;
                            ConfigOrgModels configOrgAll = goPaymentLib.Instance.getAllConfigPayOrg(true);
                            if (configOrgAll != null && configOrgAll.LS_ORG_PAY != null)
                            {
                                foreach (ORG_PAY org_pay in configOrgAll.LS_ORG_PAY)
                                {
                                    ConfigOrgModels configOrg = goPaymentLib.Instance.getConfigPayByOrg(org_pay.ORG_PAY_CODE, "");
                                    if (configOrg != null && configOrg.LS_ORG_PAY.Count > 0)
                                    {
                                        if (configOrg.LS_ORG_PAY[0].AUTO_CREATE != null && configOrg.LS_ORG_PAY[0].AUTO_CREATE.Equals("1"))
                                        {
                                            string strUrlPay = goPaymentLib.Instance.getUrlPay(trans, org_pay.ORG_PAY_CODE, "", configOrg.LS_ORG_PAY[0]);
                                        }
                                    }
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán vnpay", ex.Message);
                        }
                    }


                    //đẩy dữ liệu vào bảng giao dịch
                    string strMD5 = goEncryptBase.Instance.Md5Encode(str_orderId + str_amount);
                    string[] ParamInsert = new string[] { strTransactionID, "HDBANK", config.partnerCode, "CREATE", str_orderId, str_desc , str_desc_en , str_amount , str_amount ,
                                                            "VN", goUtility.Instance.ConvertToDateTime(str_payDate, DateTime.MinValue).ToString("yyyyMMdd"),strQRData,strTaxID,
                                                strTransactionID,str_terminalId,strMD5,str_Name,str_Phone,str_Email,str_tipAndFee, "FALSE","[]"};
                    JObject JoutPut = new JObject();
                    request.Data = goUtility.Instance.convertArrayToJsonParam(ParamInsert);
                    request.Action.ActionCode = goConstantsProcedure.Pay_create_transaction;
                    response = goBusinessAction.Instance.GetBaseResponse(request);
                    //response = callPackage(goConstantsProcedure.Pay_create_transaction, environment, ParamInsert);
                    if (response.Success)
                    {
                        //JoutPut.Add("code", "000");
                        //JoutPut.Add("message", "Khoi tao giao dich thanh cong");
                        List<JObject> objKQ = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (objKQ[0]["TRANSACTION_ID"].ToString().Equals("X"))
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng payment 902", JsonConvert.SerializeObject(reGoc));
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng: " + reGoc, Logger.ConcatName(nameClass, LoggerName));
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "01", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng");
                        }
                        JoutPut.Add("transactionNo", strTransactionID);
                        JoutPut.Add("payDate", str_payDate);
                        JoutPut.Add("appId", config.partnerCode);
                        JoutPut.Add("bankCode", "HDI_PAY");
                        JoutPut.Add("amount", str_amount);
                        JoutPut.Add("url_redirect", strUrlWebPay + "?id=" + strMD5);
                        JoutPut.Add("imageQR", strImageData);
                        JoutPut.Add("dataQR", strQRData);
                        JoutPut.Add("orderId", str_orderId);
                    }
                    else
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 918", JsonConvert.SerializeObject(response));
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", response.Error, response.ErrorMessage);
                        //JoutPut.Add("code", response.Error);
                        //JoutPut.Add("message", response.ErrorMessage);
                    }

                    return goBusinessCommon.Instance.getResultApi(response.Success, JoutPut, config, response.Error, response.ErrorMessage);
                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 929", JsonConvert.SerializeObject(response));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_4001), goConstantsError.Instance.ERROR_4001);
                    return response;
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 937", JsonConvert.SerializeObject(request) + ex.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public JObject re_creatQRCode(string strOrderCode, DateTime dtPayDate)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                
                BaseResponse response = new BaseResponse();
                //AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                JObject jOutPut = new JObject();
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                //lấy thông tin giao dịch
                string[] paramFind = new string[] { strOrderCode };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_trans_byGui, environment, paramFind);
                List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objTRANS != null && objTRANS.Count > 0)
                {
                    //khởi tạo QR mới cho giao dịch
                    string[] Param = new string[] { "HDBANK", "QR", environment, "CREATE" };
                    response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_Config, environment, Param);
                    List<JObject> objHDBQR = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    List<JObject> objHDBQR_FUNC = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                    if(objHDBQR != null && objHDBQR.Any() && objHDBQR_FUNC !=null && objHDBQR_FUNC.Any())
                    {
                        string strTransactionID = Guid.NewGuid().ToString("N").Substring(0, 8) + DateTime.Now.ToString("MMHHmmss");
                        JObject QRResult = createQr(strTransactionID, objTRANS[0]["TOTAL_AMOUNT"].ToString(), dtPayDate.ToString("dd/MM/yyyy"), objTRANS[0]["ORDER_TITLE"].ToString(),
                            objTRANS[0]["DISCOUNT"].ToString(), objHDBQR[0], objHDBQR_FUNC[0]);
                        if (!QRResult["code"].ToString().Equals("000"))
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Tao lại QR cho giao dịch payment 1306 " + strOrderCode, QRResult["code"].ToString() + QRResult["message"].ToString());
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không tạo lại được QR cho giao dịch: " + strOrderCode, Logger.ConcatName(nameClass, LoggerName));
                            return null;
                        }
                        jOutPut.Add("ORDER_CODE", strOrderCode);
                        jOutPut.Add("QR_DATA", QRResult["dataQR"].ToString());
                        jOutPut.Add("QR_TRACE", QRResult["txnId"].ToString());
                        jOutPut.Add("QR_BASE64", QRResult["imageQR"]?.ToString());
                        jOutPut.Add("LINK_TT", strUrlWebPay + "?id=" + objTRANS[0]["DATA_CHECK"].ToString());
                        //UPDATE DỮ LIỆU QR 
                        string[] paramInsert = new string[] { "TBAO", objTRANS[0]["ORG_INIT_CODE"].ToString(), objTRANS[0]["TRANSACTION_ID"].ToString(), strOrderCode, strTransactionID, 
                            objTRANS[0]["QR_CODE"].ToString(), dtPayDate.ToString("dd/MM/yyyy HH:mm:ss"), QRResult["dataQR"].ToString(), QRResult["txnId"].ToString() };
                        response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_QR, environment, paramInsert);
                        if (response == null || response.Success==false)
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Tao lại QR cho giao dịch payment 1320 " + strOrderCode, jOutPut.ToString());
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không tạo lại được QR cho giao dịch: " + jOutPut.ToString(), Logger.ConcatName(nameClass, LoggerName));
                        }
                    }
                }

                    return jOutPut;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không tạo lại được QR cho giao dịch: " + strOrderCode, Logger.ConcatName(nameClass, LoggerName));
                return null;
            }
        }
        
        public JObject createQr(string orderId, string amount, string payDate, string desc,
            string tipAndFee, JObject jConfig, JObject jApiConfig)
        {
            try
            {
                goLibQR cls = new goLibQR();
                DateTime dt = goUtility.Instance.ConvertToDateTime(payDate, DateTime.MinValue);// Convert.ToDateTime(payDate); // 25/11/2020 -> MM/dd/yyyy
                string strDatePay = dt.ToString("yyMMddHHmm");// "2011011000";//thời gian hết hạn thanh toán    yyMMddHHmm            
                QRCodeModels.RESULT_QR rs = new goLibQR().createQr(orderId, amount, strDatePay, desc, tipAndFee, jConfig, jApiConfig);
                JObject obj = new JObject();
                if (rs.code == "00")
                {
                    string strQRBase64 = getQrCode(rs.data, "Q", 8, "");
                    obj.Add("code", "000");
                    obj.Add("message", rs.message);
                    obj.Add("imageQR", strQRBase64);
                    obj.Add("dataQR", rs.data);
                    obj.Add("txnId", rs.idQrcode);
                }
                else
                {
                    obj.Add("code", rs.code);
                    obj.Add("message", rs.message);
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string getQrCode(string strQrData, string strErrorCorrect, int iSize,
            string strPathLogo)
        {
            try
            {
                QRCodeEncoder encoder = new QRCodeEncoder();
                encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                switch (strErrorCorrect)
                {
                    case "L":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                        break;
                    case "M":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                        break;
                    case "Q":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                        break;
                    case "H":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                        break;
                }
                encoder.QRCodeScale = iSize;
                Bitmap bmp = encoder.Encode(strQrData);
                //string strNameQR = "qr_code" + DateTime.Now.ToString("HHss") + ".png";
                //strPathFileImage = pathFile + strNameQR;
                if (!string.IsNullOrEmpty(strPathLogo))
                {
                    Graphics g = Graphics.FromImage(bmp);
                    Bitmap logo = new Bitmap(strPathLogo);
                    g.DrawImage(logo, new Point((bmp.Width - logo.Width) / 2, (bmp.Height - logo.Height) / 2));
                    g.Save();
                    g.Dispose();
                }
                //bmp.Save(strPathFileImage, ImageFormat.Png);
                System.IO.MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Jpeg);
                byte[] byteImage = ms.ToArray();
                return Convert.ToBase64String(byteImage);
                //bmp.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool UpdateIpnHdBank(IpnHdbankModels ipnHdbank)
        {
            try
            {
                string action = goConstantsProcedure.HDBank_Ipn;
                string[] goParam = { ipnHdbank.txnId, ipnHdbank.code, ipnHdbank.amount, ipnHdbank.msgType, "HDBANK" };
                string environment = "";
                string storeProcedure = "";
                goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
                ActionApi apiInfo = goBusinessCommon.Instance.GetActionApi(action);
                GetEnvironmentIpn(ref environment, ref environment);
                ActionApiDatabase apiDatabase = goBusinessAction.Instance.GetApiDatabase(action, environment, false);
                if (apiInfo != null)
                    storeProcedure = apiInfo.Pro_Code;
                if (apiDatabase != null && apiDatabase.Data != null)
                {
                    ModeDB = goVariables.Instance.GetDATABASE_TYPE(apiDatabase.Db_Mode);
                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && !string.IsNullOrEmpty(apiDatabase.Packages))
                        storeProcedure = apiDatabase.Schema + "." + apiDatabase.Packages + "." + storeProcedure;

                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && string.IsNullOrEmpty(apiDatabase.Packages))
                        storeProcedure = apiDatabase.Schema + "." + storeProcedure;
                    goExecute.Instance.ExecuteMultiple(ModeDB, apiDatabase.Data, storeProcedure, goParam);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public dataRequestOrderIpn convertJsonIPN(JObject jInput, ref JObject jOrgConfig)
        {
            try
            {
                dataRequestOrderIpn jOutPut = new dataRequestOrderIpn();
                transCard trans = new transCard();
                paymentResult pay = new paymentResult();
                //Lấy chuỗi json replace                
                List<JObject> lsOrg = new List<JObject>();
                List<JObject> lsOrgFun = new List<JObject>();
                List<JObject> jobjOrg = new List<JObject>();
                if (getORG_PAY("ALL", "ALL", "CREATE", ref lsOrg, ref lsOrgFun))
                {
                    if (lsOrg.Count > 0)
                    {
                        foreach (JObject jo in lsOrg)
                        {
                            //nếu không có key để tìm kiếm thì chuyển thành dấu ****
                            string strKey = string.IsNullOrEmpty(jo["IPN_KEY"]?.ToString()) ? "*****|*****" : jo["IPN_KEY"]?.ToString();
                            if (jInput.ToString().IndexOf(strKey) > 0)
                            {
                                jobjOrg.Add(jo);
                            }
                        }
                    }
                }
                if (jobjOrg.Count != 1)
                {
                    throw new Exception("Sai cấu hình map replace IPN " + jobjOrg.Count + jInput);
                }
                jOrgConfig = jobjOrg[0];
                JObject jConfig = JObject.Parse(jobjOrg[0]["IPN_REPLACE"].ToString());//json cấu hình map các biến để lấy
                List<string> strListInput = goBusinessCommon.Instance.getDSKey(JObject.Parse(jInput.ToString()));
                var sorted = from s in strListInput
                             orderby s.Substring(0,s.IndexOf(";")).Length descending
                             select s;
                string strMD5 = jobjOrg[0]["CHECK_MD5"].ToString();
                foreach(string strR in sorted)
                {
                    strMD5 = strMD5.Replace(strR.Substring(0, strR.IndexOf(";")), strR.Substring(strR.IndexOf(";")+1));
                }
                strMD5 = strMD5 + jobjOrg[0]["SECRET"].ToString();
                JObject jIPNConvert = jConfig;
                var properties = jIPNConvert.Properties();
                foreach (var property in properties)
                {
                    if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                    {
                        JObject jConfigCT = JObject.Parse(property.Value.ToString());
                        var prt = jConfigCT.Properties();
                        foreach (var prtct in prt)
                        {
                            if (prtct.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 prtct.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                JObject ct3 = mergJobject(JObject.Parse(prtct.Value.ToString()), strListInput);
                                jConfigCT[prtct.Name] = ct3;
                            }
                            else
                                jConfigCT[prtct.Name] = goBusinessCommon.Instance.getValueJson(prtct.Value.ToString(), strListInput);
                        }
                        jIPNConvert[property.Name] = jConfigCT;
                    }
                    else
                        jIPNConvert[property.Name] = goBusinessCommon.Instance.getValueJson(property.Value.ToString(), strListInput);
                }
                jOutPut = JsonConvert.DeserializeObject<dataRequestOrderIpn>(jIPNConvert.ToString());
                //if (!jOutPut.checksum.Equals(goEncryptBase.Instance.Md5Encode(strMD5)))
                //{
                //    throw new Exception("Lỗi kiểm tra MD5 thông tin trả về " + strMD5 + jInput);
                //}
                return jOutPut;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JObject mergJobject(JObject jo, List<string> lsReplace)
        {
            //xử lý map dữ liệu vào json cố định
            try
            {
                JObject joReturn = new JObject();
                joReturn = jo;
                var properties = joReturn.Properties();
                foreach (var property in properties)
                {
                    if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                    {
                        JObject jConfigCT = JObject.Parse(property.Value.ToString());
                        var prt = jConfigCT.Properties();
                        foreach (var prtct in prt)
                        {
                            jConfigCT[prtct.Name] = goBusinessCommon.Instance.getValueJson(prtct.Value.ToString(), lsReplace);
                        }
                        joReturn[property.Name] = jConfigCT;
                    }
                    else
                        joReturn[property.Name] = goBusinessCommon.Instance.
                            getValueJson(property.Value.ToString(), lsReplace);
                }
                return joReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool getORG_PAY(string strOrgCode, string strType, string strMethod, ref List<JObject> lsOrg, ref List<JObject> lsFunc)
        {
            try
            {
                string strUrlWebPay = "";
                string environment = "";
                GetEnvironmentIpn(ref environment, ref strUrlWebPay);
                string[] Param = new string[] { "ALL", "ALL", environment, "ALL" };
                BaseResponse response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_Config, environment, Param);
                List<JObject> objorg = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objorg.Count == 0)
                {
                    throw new Exception("Không tìm thấy cấu hình đơn vị thanh toán: pkg=" + goConstantsProcedure.Pay_Get_Config + " tham số: " + Param.ToString() + response.ToString());
                }
                foreach (JObject jo in objorg)
                {
                    if ((jo["ORG_PAY_CODE"].ToString().Equals(strOrgCode) || strOrgCode.Equals("ALL"))
                        && (jo["PAY_TYPE"].ToString().Equals(strType) || strType.Equals("ALL")))
                    {
                        lsOrg.Add(jo);
                    }
                }
                List<JObject> objorgFun = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                foreach (JObject jof in objorgFun)
                {
                    if ((jof["ORG_PAY_CODE"].ToString().Equals(strOrgCode) || strOrgCode.Equals("ALL"))
                        && jof["TYPE_METHOD"].ToString().Equals(strMethod))
                    {
                        lsFunc.Add(jof);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void GetEnvironmentIpn(ref string environment, ref string strURLWEBPay)
        {
            //int isDebug = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig("isDebug"), 1);
            //if (isDebug == 0)
            //{
                environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
            //}
            //else
            //{
            //    environment = goConfig.Instance.GetAppConfig("environment_ipn");
            //    strURLWEBPay = goConfig.Instance.GetAppConfig("url_web_pay");
            //}

        }
        #endregion
        #endregion
    }
}
