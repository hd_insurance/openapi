﻿using GO.BUSINESS.PaymentCallBack;
using GO.DTO.Payment;
using GO.ENCRYPTION;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using static GO.DTO.Payment.QRCodeModels;

namespace GO.BUSINESS.Service
{
    public class goPaymentPartner
    {

    }
    public class VnPayLibrary
    {
        private SortedList<String, String> _requestData = new SortedList<String, String>(new VnPayCompare());
        private SortedList<String, String> _responseData = new SortedList<String, String>(new VnPayCompare());

        public void AddRequestData(string key, string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                _requestData.Add(key, value);
            }
        }

        public void AddResponseData(string key, string value)
        {
            if (!String.IsNullOrEmpty(value))
            {
                _responseData.Add(key, value);
            }
        }

        public string GetResponseData(string key)
        {
            string retValue;
            if (_responseData.TryGetValue(key, out retValue))
            {
                return retValue;
            }
            else
            {
                return string.Empty;
            }
        }

        #region Request

        public string CreateRequestUrl(string baseUrl, string vnp_HashSecret)
        {
            StringBuilder data = new StringBuilder();
            foreach (KeyValuePair<string, string> kv in _requestData)
            {
                if (!String.IsNullOrEmpty(kv.Value))
                {
                    data.Append(kv.Key + "=" + HttpUtility.UrlEncode(kv.Value) + "&");
                }
            }
            string queryString = data.ToString();
            string rawData = GetRequestRaw();
            baseUrl += "?" + queryString;
            string vnp_SecureHash = Utils.Sha256(vnp_HashSecret + rawData);
            baseUrl += "vnp_SecureHash=" + vnp_SecureHash;
            return baseUrl;
        }

        private string GetRequestRaw()
        {
            StringBuilder data = new StringBuilder();
            foreach (KeyValuePair<string, string> kv in _requestData)
            {
                if (!String.IsNullOrEmpty(kv.Value))
                {
                    data.Append(kv.Key + "=" + kv.Value + "&");
                }
            }
            //remove last '&'
            if (data.Length > 0)
            {
                data.Remove(data.Length - 1, 1);
            }
            return data.ToString();
        }

        #endregion

        #region Response process

        public bool ValidateSignature(string inputHash, string secretKey)
        {
            string rspRaw = GetResponseRaw();
            string myChecksum = Utils.Sha256(secretKey + rspRaw);
            return myChecksum.Equals(inputHash, StringComparison.InvariantCultureIgnoreCase);
        }
        private string GetResponseRaw()
        {

            StringBuilder data = new StringBuilder();
            if (_responseData.ContainsKey("vnp_SecureHashType"))
            {
                _responseData.Remove("vnp_SecureHashType");
            }
            if (_responseData.ContainsKey("vnp_SecureHash"))
            {
                _responseData.Remove("vnp_SecureHash");
            }
            foreach (KeyValuePair<string, string> kv in _responseData)
            {
                if (!String.IsNullOrEmpty(kv.Value))
                {
                    data.Append(kv.Key + "=" + kv.Value + "&");
                }
            }
            //remove last '&'
            if (data.Length > 0)
            {
                data.Remove(data.Length - 1, 1);
            }
            return data.ToString();
        }

        #endregion
    }

    public class Utils
    {
        public static string Md5(string sInput)
        {
            HashAlgorithm algorithmType = default(HashAlgorithm);
            ASCIIEncoding enCoder = new ASCIIEncoding();
            byte[] valueByteArr = enCoder.GetBytes(sInput);
            byte[] hashArray = null;
            // Encrypt Input string 
            algorithmType = new MD5CryptoServiceProvider();
            hashArray = algorithmType.ComputeHash(valueByteArr);
            //Convert byte hash to HEX
            StringBuilder sb = new StringBuilder();
            foreach (byte b in hashArray)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
        public static string Sha256(string data)
        {
            using (var sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                var bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(data));

                // Convert byte array to a string   
                var builder = new StringBuilder();
                foreach (var t in bytes)
                {
                    builder.Append(t.ToString("x2"));
                }
                return builder.ToString();
            }
        }
        public static string GetIpAddress()
        {
            string ipAddress;
            try
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (string.IsNullOrEmpty(ipAddress) || (ipAddress.ToLower() == "unknown"))
                    ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception ex)
            {
                ipAddress = "Invalid IP:" + ex.Message;
            }

            return ipAddress;
        }
    }

    public class VnPayCompare : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            if (x == y) return 0;
            if (x == null) return -1;
            if (y == null) return 1;
            var vnpCompare = CompareInfo.GetCompareInfo("en-US");
            return vnpCompare.Compare(x, y, CompareOptions.Ordinal);
        }
    }

    public class goPayQrcode
    {
        public JObject createQr(string orderId, string amount, string payDate, string desc,
           string tipAndFee, ORG_PAY jConfig)
        {
            try
            {
                QRCodeModels.CREATE_QR qr = new QRCodeModels.CREATE_QR();
                qr.appId = jConfig.CLIENT_ID;// jConfig["CLIENT_ID"].ToString();// "HDI";
                qr.masterMerCode = jConfig.MASTER_CODE;// jConfig["MASTER_CODE"].ToString();// "A000000775";
                qr.merchantName = jConfig.CLIENT_NAME;// jConfig["CLIENT_NAME"].ToString();// "BH_HDI";
                qr.merchantCode = jConfig.MERCHANT_ID;// jConfig["MERCHANT_ID"].ToString();// "HDI";
                qr.merchantType = jConfig.USERNAME;// "";
                qr.terminalId = jConfig.TERMINAL_ID;// jConfig["TERMINAL_ID"].ToString();
                qr.serviceCode = jConfig.SERVICE_CODE;// jConfig["SERVICE_CODE"].ToString(); //"03";// "03";//mã dịch vụ cho QR               
                qr.countryCode = jConfig.COUNTRY_CODE;// jConfig["COUNTRY_CODE"].ToString(); //"VN";
                string _secretKey = jConfig.SECRET;// jConfig["SECRET"].ToString();

                qr.payType = jConfig.PAY_CODE; //"03";//QR sử dụng để thanh toán
                qr.ccy = jConfig.CCY;// jConfig["CCY"].ToString(); //"704";
                qr.productId = "";
                qr.txnId = orderId;
                qr.billNumber = "";
                qr.amount = amount;
                qr.expDate = payDate;
                qr.desc = desc;
                qr.tipAndFee = tipAndFee;

                qr.checksum = goEncryptBase.Instance.Md5Encode(qr.appId + "|" +
                        qr.merchantName + "|" + qr.serviceCode
                        + "|" + qr.countryCode + "|" +
                        qr.masterMerCode + "|" +
                        qr.merchantType + "|" +
                        qr.merchantCode + "|" + qr.terminalId
                        + "|" + qr.payType + "|" + qr.productId +
                        "|" + qr.txnId + "|" + qr.amount + "|" +
                        qr.tipAndFee + "|" + qr.ccy + "|" +
                        qr.expDate + "|" + jConfig.SECRET);
                JObject oJsonInput = JObject.FromObject(qr);
                goLibQR libqr = new goLibQR();
                HttpResponseMessage rp = libqr.callApiQRCode(oJsonInput, JObject.FromObject(jConfig.LS_FUNTION[0]));
                QRCodeModels.RESULT_QR rs = new QRCodeModels.RESULT_QR();
                string message = rp.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (rp.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    rs.code = "00";
                    rs.message = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    rs.code = details["code"].ToString();
                    rs.message = details["message"].ToString();
                }
                if (rs.code == "00")
                {
                    JObject jsonOut = JObject.Parse(rs.message);
                    if (jsonOut["code"].ToString().Equals("00"))
                    {
                        //xử lý data qr trả về                   
                        rs.message = jsonOut["message"].ToString();
                        rs.data = jsonOut["data"].ToString();
                        rs.checkSum = jsonOut["checksum"].ToString();
                        rs.idQrcode = jsonOut["idQrcode"].ToString();
                        //kiểm tra md5
                        string strMD5 = goEncryptBase.Instance.Md5Encode(rs.code + "|" + rs.message + "|" +
                        rs.data + "|null|" + jConfig.SECRET);
                        if (!strMD5.Equals(rs.checkSum))
                        {
                            rs.code = "15";
                            rs.message = "Lỗi check sum dữ liệu HD";
                        }
                    }
                    else
                    {
                        rs.code = jsonOut["code"].ToString();
                    }

                }
                JObject obj = new JObject();
                if (rs.code == "00")
                {
                    string strQRBase64 = goBussinessPayment.Instance.getQrCode(rs.data, "Q", 8, "");
                    obj.Add("code", "000");
                    obj.Add("message", rs.message);
                    obj.Add("imageQR", strQRBase64);
                    obj.Add("dataQR", rs.data);
                    obj.Add("txnId", rs.idQrcode);
                }
                else
                {
                    obj.Add("code", rs.code);
                    obj.Add("message", rs.message);
                }
                return obj;

                //return JObject.FromObject(rs);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }    
}
