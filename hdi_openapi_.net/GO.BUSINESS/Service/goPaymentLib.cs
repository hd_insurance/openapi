﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Orders;
using GO.DTO.Payment;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;

namespace GO.BUSINESS.Service
{
    public class goPaymentLib
    {

        #region Contructor
        private static readonly Lazy<goPaymentLib> InitInstance = new Lazy<goPaymentLib>(() => new goPaymentLib());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        public string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
        public static goPaymentLib Instance => InitInstance.Value;
        #endregion


        #region database


        public ConfigOrgModels getConfigPayByOrg(string strOrgCode, string strUrl)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            ConfigOrgModels config = new ConfigOrgModels();
            try
            {
                ConfigOrgModels configReturn = new ConfigOrgModels();
                config = getAllConfigPayOrg(true);
                List<ORG_PAY> lsPay = config.LS_ORG_PAY.Where(m => m.ORG_PAY_CODE == strOrgCode).ToList();
                if (lsPay == null || !lsPay.Any())
                {
                    config = getAllConfigPayOrg(false);
                    lsPay = config.LS_ORG_PAY.Where(m => m.ORG_PAY_CODE == strOrgCode).ToList();
                }
                configReturn.LS_ORG_PAY = lsPay;
                if (!string.IsNullOrEmpty(strUrl))
                {
                    List<SYS_CALLBACK> lsCB = config.CALLBACK.Where(m => m.URL_CALL.StartsWith(strUrl) && m.ORG_CODE == strOrgCode).ToList();
                    configReturn.CALLBACK = lsCB;
                    return configReturn;
                }
                else
                {
                    configReturn.CALLBACK = config.CALLBACK.Where(m => m.ORG_CODE == strOrgCode).ToList();
                    return configReturn;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message + " With data: " + strOrgCode, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 94 ", ex.Message);
                return null;
            }
        }
        public ConfigOrgModels getConfigCreatePay(string strOrgPayCode, string strOrgCode, string strType)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            ConfigOrgModels config = new ConfigOrgModels();
            try
            {
                ConfigOrgModels configReturn = new ConfigOrgModels();
                config = getAllConfigPayOrg(true);
                List<ORG_PAY> lsPay = new List<ORG_PAY>();
                if (strType.Equals("QR"))
                {
                    lsPay = config.LS_ORG_PAY.Where(m => m.ORG_AGENCY_CODE == strOrgCode && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE.Equals("QR")).ToList();
                    if (lsPay == null || !lsPay.Any())
                    {
                        config = getAllConfigPayOrg(false);
                        lsPay = config.LS_ORG_PAY.Where(m => m.ORG_AGENCY_CODE == strOrgCode && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE.Equals("QR")).ToList();
                        if (lsPay == null || !lsPay.Any())
                        {
                            lsPay = config.LS_ORG_PAY.Where(m => m.ORG_AGENCY_CODE == "HDI" && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE.Equals("QR")).ToList();
                        }
                    }
                }
                else
                {
                    lsPay = config.LS_ORG_PAY.Where(m => m.ORG_PAY_CODE == strOrgPayCode && m.ORG_AGENCY_CODE == strOrgCode && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE != "QR").ToList();
                    if (lsPay == null || !lsPay.Any())
                    {
                        config = getAllConfigPayOrg(false);
                        lsPay = config.LS_ORG_PAY.Where(m => m.ORG_PAY_CODE == strOrgPayCode && m.ORG_AGENCY_CODE == strOrgCode && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE != "QR").ToList();
                        if (lsPay == null || !lsPay.Any())
                        {
                            config = getAllConfigPayOrg(false);
                            lsPay = config.LS_ORG_PAY.Where(m => m.ORG_PAY_CODE == strOrgPayCode && m.ORG_AGENCY_CODE == "HDI" && m.IS_ACTIVE.Equals("1") && m.PAY_TYPE != "QR").ToList();
                        }
                    }
                }
                configReturn.LS_ORG_PAY = lsPay;
                configReturn.CALLBACK = config.CALLBACK.Where(m => m.ORG_CODE == strOrgCode).ToList();
                return configReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message + " With data: " + strOrgCode, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 94 ", ex.Message);
                return null;
            }
        }

        public ConfigOrgModels getAllConfigPayOrg(bool bUpdateCache = true)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            //AuditLogsInfo auditLogs = new AuditLogsInfo();
            ConfigOrgModels config = new ConfigOrgModels();

            try
            {
                string strKeyRedis = "GET_KEY_PAY";
                try
                {
                    config = goBusinessCache.Instance.Get<ConfigOrgModels>(strKeyRedis);
                    if (config != null && config.LS_ORG_PAY.Count > 0)
                    {
                        if (bUpdateCache)
                            return config;
                        else
                            goBusinessCache.Instance.Remove(strKeyRedis);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                    config = null;
                }
                string[] param = new string[] { strEvm };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_All_Config, strEvm, param);
                if (response.Success)
                {
                    List<ORG_PAY> lsOrgPay = goBusinessCommon.Instance.GetData<ORG_PAY>(response, 0);
                    List<PAY_FUNTION> lsFuntion = goBusinessCommon.Instance.GetData<PAY_FUNTION>(response, 1);
                    List<SYS_CALLBACK> lsCallback = goBusinessCommon.Instance.GetData<SYS_CALLBACK>(response, 2);
                    if (lsOrgPay != null && lsOrgPay.Any() && lsFuntion != null && lsFuntion.Any())
                    {
                        List<ORG_PAY> lsOrgReturn = new List<ORG_PAY>();
                        config = new ConfigOrgModels();
                        config.CALLBACK = new List<SYS_CALLBACK>();
                        config.LS_ORG_PAY = new List<ORG_PAY>();
                        config.CALLBACK = lsCallback;
                        foreach (ORG_PAY org in lsOrgPay)
                        {
                            org.LS_FUNTION = new List<PAY_FUNTION>();
                            foreach (PAY_FUNTION pay in lsFuntion)
                            {
                                if (org.ORG_PAY_CODE.Equals(pay.ORG_PAY_CODE))
                                {
                                    org.LS_FUNTION.Add(pay);
                                }
                            }
                            lsOrgReturn.Add(org);
                        }
                        config.LS_ORG_PAY = lsOrgReturn;
                    }
                    bool b = goBusinessCache.Instance.Add(strKeyRedis, config);
                    if (!b)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong day duoc cau hinh don vi thanh toan vào redis", Logger.ConcatName(nameClass, LoggerName));
                        goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lib pay redis 80 ", "Khong day duoc cau hinh don vi thanh toan vào redis");
                    }
                    return config;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lib pay khong tim thay cau hinh thanh toan 141", Logger.ConcatName(nameClass, LoggerName));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 87 ", "pkg C_OPENAPI.PKG_C_OPENAPI.get_ALL_ORG_PAY no data found");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 94 ", ex.Message);
                return null;
            }
        }

        public BaseResponse dbJsonObjectParam(JObject jInput, string strAction)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            try
            {
                List<string> lsParam = new List<string>();
                var properties = jInput.Properties();
                foreach (var property in properties)
                {
                    lsParam.Add(property.Value.ToString());
                }
                response = goBusinessCommon.Instance.executeProHDI(strAction, strEvm, lsParam.ToArray());
                if (response.Success)
                {
                    return response;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong insert được dữ liệu" + jInput.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Khong insert được dữ liệu 157 ", "action strAction " + strAction + jInput.ToString());
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong insert được dữ liệu " + ex.Message + jInput.ToString(), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Khong insert được dữ liệu 164 ", "action strAction " + strAction + jInput.ToString() + ex.Message);
                return null;
            }
        }
        #endregion

        #region funtion

        public string getUrlPay(TRANSACTION trans, string strOrgPay, string strUrlCallBack, ORG_PAY orgModels)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strUrlPay = "";
                List<TRANSACTION_DETAIL> lsTransD = new List<TRANSACTION_DETAIL>();
                switch (strOrgPay)
                {
                    case "VNPAY":
                        {
                            strUrlPay = vnpay_CreateUrlPay(trans, orgModels);
                            TRANSACTION_DETAIL transDT = new TRANSACTION_DETAIL();
                            transDT.ORG_PAY_CODE = strOrgPay;
                            transDT.STATUS = "CREATE";
                            transDT.TRANSACTION_ID = trans.TRANSACTION_ID;
                            transDT.URL_PAY = strUrlPay;
                            transDT.URL_CALLBACK = strUrlCallBack;
                            lsTransD.Add(transDT);
                            break;
                        }
                }
                //insert detail transaction

                JObject jInputDb = new JObject();
                jInputDb.Add("transaction_id", trans.TRANSACTION_ID);
                jInputDb.Add("p_PM_TRANSFER_INFOR", JsonConvert.SerializeObject(lsTransD));
                BaseResponse response = dbJsonObjectParam(jInputDb, goConstantsProcedure.Pay_Insert_DetailTrans);
                if (response == null || !response.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "khong insert duoc pay trans detail" + jInputDb.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao url pay vnpay 205", jInputDb.ToString());
                }
                return strUrlPay;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public TRANSACTION getTransByID(string strID)
        {
            try
            {
                JObject jParam = new JObject();
                jParam.Add("ID", strID);
                BaseResponse response = new BaseResponse();
                response = goPaymentLib.Instance.dbJsonObjectParam(jParam, goConstantsProcedure.Pay_get_transaction);
                if (response.Success)
                {
                    List<TRANSACTION> lsTran = goBusinessCommon.Instance.GetData<TRANSACTION>(response, 0);
                    if (lsTran != null && lsTran.Count > 0)
                    {
                        return lsTran[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                return null;
            }
        }
        public string vnpay_CreateUrlPay(TRANSACTION trans, ORG_PAY orgModels)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                List<PAY_FUNTION> pay_fun = orgModels.LS_FUNTION.Where(m => m.TYPE_METHOD == "CREATE").ToList();
                string vnp_Url = pay_fun[0].URL;
                string vnp_HashSecret = orgModels.SECRET;
                string vnp_Returnurl = orgModels.LS_FUNTION.Where(m => m.TYPE_METHOD == "REDIRECT").ToList()[0].URL;
                string strUrlPay = "";
                OrderInfo order = new OrderInfo();
                order.OrderId = trans.TRANSACTION_ID;
                order.Amount = Convert.ToInt64(trans.TOTAL_AMOUNT);
                order.OrderDesc = goBussinessSMS.Instance.removeMark(trans.ORDER_TITLE);
                order.CreatedDate = DateTime.Now;
                VnPayLibrary vnpay = new VnPayLibrary();
                vnpay.AddRequestData("vnp_Version", "2.0.0");
                vnpay.AddRequestData("vnp_Command", "pay");//pay  genqr
                vnpay.AddRequestData("vnp_TmnCode", orgModels.CLIENT_ID);
                vnpay.AddRequestData("vnp_Amount", (order.Amount * 100).ToString());
                vnpay.AddRequestData("vnp_CreateDate", order.CreatedDate.ToString("yyyyMMddHHmmss"));
                vnpay.AddRequestData("vnp_CurrCode", "VND");
                vnpay.AddRequestData("vnp_IpAddr", Utils.GetIpAddress());
                vnpay.AddRequestData("vnp_Locale", "vn");
                vnpay.AddRequestData("vnp_OrderInfo", order.OrderDesc);
                vnpay.AddRequestData("vnp_OrderType", "250001");
                vnpay.AddRequestData("vnp_ReturnUrl", vnp_Returnurl);
                vnpay.AddRequestData("vnp_TxnRef", order.OrderId.ToString());
                strUrlPay = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
                return strUrlPay;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao url pay vnpay" + JsonConvert.SerializeObject(orgModels), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao url pay vnpay 228", JsonConvert.SerializeObject(trans) + " --- " + JsonConvert.SerializeObject(orgModels));
                return "";
            }
        }
        public PAY_FUNTION getKeyFromList(List<PAY_FUNTION> pay_fun, string strKey)
        {
            try
            {
                PAY_FUNTION funtionKey = pay_fun.Where(m => m.TYPE_METHOD == strKey).ToList()[0];
                return funtionKey;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public VnPayLibrary checkIpnVNPay(SortedList<String, String> inputData, ref bool bCheckSign)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strInput = JsonConvert.SerializeObject(inputData);
                ORG_PAY org_code = getOrg_Code(strInput);
                VnPayLibrary vnpay = new VnPayLibrary();
                foreach (KeyValuePair<string, string> s in inputData)
                {
                    if (!string.IsNullOrEmpty(s.Key) && s.Key.StartsWith("vnp_"))
                    {
                        vnpay.AddResponseData(s.Key, s.Value);
                    }
                }
                String vnp_SecureHash = vnpay.GetResponseData("vnp_SecureHash");
                bCheckSign = vnpay.ValidateSignature(vnp_SecureHash, org_code.SECRET);
                return vnpay;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message + " With data: " + JsonConvert.SerializeObject(inputData), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi checkResponsePay 279 ", ex.Message);
                return null;
            }
        }
        public StatusCheck checkResponsePay(SortedList<String, String> inputData)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                BaseResponse response = new BaseResponse();
                StatusCheck check = new StatusCheck();
                string strInput = JsonConvert.SerializeObject(inputData);
                ORG_PAY org_code = getOrg_Code(strInput);
                string strUrlWebPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                switch (org_code.ORG_PAY_CODE)
                {
                    case "VNPAY":
                        {
                            VnPayLibrary vnpay = new VnPayLibrary();
                            foreach (KeyValuePair<string, string> s in inputData)
                            {
                                if (!string.IsNullOrEmpty(s.Key) && s.Key.StartsWith("vnp_"))
                                {
                                    vnpay.AddResponseData(s.Key, s.Value);
                                }
                            }
                            TRANSACTION trans = new TRANSACTION();
                            string orderId = vnpay.GetResponseData("vnp_TxnRef");
                            string vnpayTranId = vnpay.GetResponseData("vnp_TransactionNo");
                            string vnp_ResponseCode = vnpay.GetResponseData("vnp_ResponseCode");
                            String vnp_SecureHash = vnpay.GetResponseData("vnp_SecureHash");
                            bool checkSignature = vnpay.ValidateSignature(vnp_SecureHash, org_code.SECRET);
                            if (checkSignature)
                            {

                                if (vnp_ResponseCode == "00")
                                {
                                    check.AMOUNT = vnpay.GetResponseData("vnp_Amount").Substring(0, vnpay.GetResponseData("vnp_Amount").Length - 2);
                                    check.BANK_CODE = vnpay.GetResponseData("vnp_BankCode");
                                    check.ORG_PAY_CODE = org_code.ORG_PAY_CODE;
                                    check.PAY_DATE = DateTime.ParseExact(vnpay.GetResponseData("vnp_PayDate"), "yyyyMMddHHmmss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy HH:mm:ss");
                                    check.STATUS = "000";
                                    check.SUMMARY = vnpay.GetResponseData("vnp_OrderInfo");
                                    check.TRACE = vnpayTranId;
                                    check.TRANSACTION = orderId;
                                    check.MESSAGE = "Giao dịch thành công";
                                    //cập nhật dữ liệu về trạng thái đã báo thành công ngoài internet
                                    JObject jObjUpdate = new JObject();
                                    jObjUpdate.Add("p_ORG_PAY_CODE", check.ORG_PAY_CODE);
                                    jObjUpdate.Add("p_TRANSACTION_ID", check.TRANSACTION);
                                    jObjUpdate.Add("p_STATUS", "DONE_WEB");
                                    jObjUpdate.Add("p_PAY_DATE", vnpay.GetResponseData("vnp_PayDate"));
                                    jObjUpdate.Add("p_CARD_TYPE", vnpay.GetResponseData("vnp_CardType"));
                                    jObjUpdate.Add("p_BANK_CODE", vnpay.GetResponseData("vnp_BankCode"));
                                    jObjUpdate.Add("p_BANK_TRANS", vnpay.GetResponseData("vnp_BankTranNo"));
                                    response = dbJsonObjectParam(jObjUpdate, goConstantsProcedure.Pay_Update_DetailTrans);
                                    TRANSACTION_DETAIL detail = goBusinessCommon.Instance.GetData<TRANSACTION_DETAIL>(response, 0).FirstOrDefault();
                                    if (detail != null && detail.URL_CALLBACK != null)
                                    {
                                        check.LINK_CALLBACK = detail.URL_CALLBACK;
                                    }
                                    else
                                    {
                                        string strIPN = "";
                                        check.LINK_CALLBACK = getLink_CallBack("VNPAY", ref strIPN);
                                    }
                                }
                                else
                                {
                                    check.STATUS = "001";
                                    check.MESSAGE = "Giao dịch thất bại";
                                    trans = goPaymentLib.Instance.getTransByID(orderId);
                                    check.LINK_CALLBACK = strUrlWebPay + "?id=" + trans.DATA_CHECK;
                                }
                            }
                            else
                            {
                                check.STATUS = "097";
                                check.MESSAGE = "Invalid signature";
                                trans = goPaymentLib.Instance.getTransByID(orderId);
                                check.LINK_CALLBACK = strUrlWebPay + "?id=" + trans.DATA_CHECK;
                            }
                            break;
                        }
                }


                return check;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message + " With data: " + JsonConvert.SerializeObject(inputData) + ". ORG_CODE_Pay: " + getOrg_Code(JsonConvert.SerializeObject(inputData)).ORG_PAY_CODE, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi checkResponsePay 471 ", ex.Message);
                return null;
            }
        }

        public ORG_PAY getOrg_Code(string strResponse)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            ConfigOrgModels config = new ConfigOrgModels();
            try
            {
                ConfigOrgModels configReturn = new ConfigOrgModels();
                config = getAllConfigPayOrg(true);
                foreach (ORG_PAY orgpay in config.LS_ORG_PAY)
                {
                    if (orgpay.IPN_KEY != null && strResponse.IndexOf(orgpay.IPN_KEY) > 10)
                    {
                        return orgpay;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message + " With data: " + strResponse, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 94 ", ex.Message);
                return null;
            }
        }
        public string getLink_CallBack(string strOrgCode, ref string strUrlIPN)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            ConfigOrgModels config = new ConfigOrgModels();
            try
            {
                strUrlIPN = "";
                ConfigOrgModels configReturn = new ConfigOrgModels();
                config = getAllConfigPayOrg(true);
                foreach (SYS_CALLBACK orgpay in config.CALLBACK)
                {
                    if (orgpay.URL_CALLBACK != null && orgpay.ORG_CODE.Equals(strOrgCode))
                    {
                        strUrlIPN = orgpay.URL_IPN;
                        return orgpay.URL_CALLBACK;
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message + " With data: " + strOrgCode, Logger.ConcatName(nameClass, LoggerName));
                return "";
            }
        }
        public dataRequestOrderIpn convertJsonIPN(ORG_PAY orgPay, JObject jInput)
        {
            try
            {
                dataRequestOrderIpn jOutPut = new dataRequestOrderIpn();
                transCard trans = new transCard();
                paymentResult pay = new paymentResult();
                JObject jConfig = JObject.Parse(orgPay.IPN_REPLACE);//json cấu hình map các biến để lấy
                List<string> strListInput = goBusinessCommon.Instance.getDSKey(JObject.Parse(jInput.ToString()));
                var sorted = from s in strListInput
                             orderby s.Substring(0, s.IndexOf(";")).Length descending
                             select s;
                string strMD5 = orgPay.CHECK_MD5;
                if (!string.IsNullOrEmpty(strMD5))
                {
                    foreach (string strR in sorted)
                    {
                        strMD5 = strMD5.Replace(strR.Substring(0, strR.IndexOf(";")), strR.Substring(strR.IndexOf(";") + 1));
                    }
                }
                strMD5 = strMD5 + orgPay.SECRET;
                JObject jIPNConvert = jConfig;
                var properties = jIPNConvert.Properties();
                foreach (var property in properties)
                {
                    if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                    {
                        JObject jConfigCT = JObject.Parse(property.Value.ToString());
                        var prt = jConfigCT.Properties();
                        foreach (var prtct in prt)
                        {
                            if (prtct.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 prtct.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                JObject ct3 = mergJobject(JObject.Parse(prtct.Value.ToString()), strListInput);
                                jConfigCT[prtct.Name] = ct3;
                            }
                            else
                                jConfigCT[prtct.Name] = goBusinessCommon.Instance.getValueJson(prtct.Value.ToString(), strListInput);
                        }
                        jIPNConvert[property.Name] = jConfigCT;
                    }
                    else
                        jIPNConvert[property.Name] = goBusinessCommon.Instance.getValueJson(property.Value.ToString(), strListInput);
                }
                jOutPut = JsonConvert.DeserializeObject<dataRequestOrderIpn>(jIPNConvert.ToString());
                return jOutPut;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JObject mergJobject(JObject jo, List<string> lsReplace)
        {
            //xử lý map dữ liệu vào json cố định
            try
            {
                JObject joReturn = new JObject();
                joReturn = jo;
                var properties = joReturn.Properties();
                foreach (var property in properties)
                {
                    if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                    {
                        JObject jConfigCT = JObject.Parse(property.Value.ToString());
                        var prt = jConfigCT.Properties();
                        foreach (var prtct in prt)
                        {
                            jConfigCT[prtct.Name] = goBusinessCommon.Instance.getValueJson(prtct.Value.ToString(), lsReplace);
                        }
                        joReturn[property.Name] = jConfigCT;
                    }
                    else
                        joReturn[property.Name] = goBusinessCommon.Instance.getValueJson(property.Value.ToString(), lsReplace);
                }
                return joReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public void checkCallIPNPartner(string strOrg, string strOderid, string reference_id, string transaction_value, string transaction_currency, string vat_value, string vat_currency)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strUrlIpn = "";
                string strSecret = getLink_CallBack(strOrg, ref strUrlIpn);
                string strToken = "";
                //strToken = GenerateJWToken(jInput.ToString(), "aGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIx");
                //strToken = goEncryptMix.Instance.encryptMd5HmacSha256(jInput.ToString(), strSecret);
                if (!strOrg.Equals("GALAXY_CONNECT"))
                {
                    strToken = GenerateJWToken(strOderid, reference_id, strSecret, transaction_value, transaction_currency, vat_value, vat_currency);
                }
                JObject jobjIpn = new JObject();
                jobjIpn.Add("order_id", strOderid);
                jobjIpn.Add("transaction_value", transaction_value);
                jobjIpn.Add("transaction_currency", transaction_currency);
                jobjIpn.Add("reference_id", reference_id);
                jobjIpn.Add("vat_value", vat_value);
                jobjIpn.Add("vat_currency", vat_currency);
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "DL day Hoang " + jobjIpn.ToString() + "   " + strToken, Logger.ConcatName(nameClass, LoggerName));
                HttpResponseMessage response = postJsonAPI(jobjIpn, strUrlIpn + strToken, null);
                string message = response.Content.ReadAsStringAsync().Result;
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn creadify " + message, Logger.ConcatName(nameClass, LoggerName));
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (!(parsedString.IndexOf("success") > 1))
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt Hoang ", parsedString + "-----------" + jobjIpn.ToString());
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, parsedString, Logger.ConcatName(nameClass, LoggerName));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "loi ipn creadify " + ex.Message, Logger.ConcatName(nameClass, LoggerName));

            }
        }

        public void checkCallIPNGalaxyConnect(string strOrg, string transId, List<JObject> lstDataCallback, bool status)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strUrlIpn = "";
                string strSecret = getLink_CallBack(strOrg, ref strUrlIpn);
                string strToken = "";
                //strToken = GenerateJWToken(jInput.ToString(), "aGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIx");
                //strToken = goEncryptMix.Instance.encryptMd5HmacSha256(jInput.ToString(), strSecret);
                if (!strOrg.Equals("GALAXY_CONNECT"))
                {
                    //strToken = GenerateJWToken(strOderid, reference_id, strSecret, transaction_value, transaction_currency, vat_value, vat_currency);
                }
                //KhanhPT callback to GalaxyConnect 2022-12-13 cho phần xe
                JObject jobjIpn = new JObject();               
                JArray arr = new JArray();
                jobjIpn.Add("transId", transId);
                if (status)
                {
                    jobjIpn.Add("status", "success");
                }
                else
                {
                    jobjIpn.Add("status", "failed");
                }
                if (lstDataCallback == null || lstDataCallback.Count == 0)
                {
                    jobjIpn.Add("DATA_CALLBACK", arr);
                }
                else
                {
                    foreach (JObject obj in lstDataCallback)
                    {
                        arr.Add(obj);
                    }
                    
                    jobjIpn.Add("DATA_CALLBACK", arr);
                }
                                                
                //End


                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "GalaxyConnect " + jobjIpn.ToString() + "   " + strToken, Logger.ConcatName(nameClass, LoggerName));
                HttpResponseMessage response = postJsonAPI(jobjIpn, strUrlIpn + strToken, null);
                string message = response.Content.ReadAsStringAsync().Result;
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn galaxyconnect " + message, Logger.ConcatName(nameClass, LoggerName));
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (!(parsedString.Contains("success")))
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhật ", parsedString + "-----------" + jobjIpn.ToString());
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, parsedString, Logger.ConcatName(nameClass, LoggerName));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "loi ipn galaxyconnect " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
            }
        }

        public HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PostAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Service timeout " + ex.ToString());
            }

        }
        public static HttpResponseMessage postJsonAPI_VNPAY_QR(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Content_type", "text/plain");
                client.DefaultRequestHeaders.Add("User-Agent", "HDI");
                client.DefaultRequestHeaders.Add("Accept", "*/*");
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                var content = new StringContent(JInput.ToString(), Encoding.UTF8);
                HttpResponseMessage response = client.PostAsync(strUrl, content).Result;
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Service timeout " + ex.ToString());
            }
        }
        public static string GenerateJWToken(string order_id, string reference_id, string Secret, string transaction_value, string transaction_currency, string vat_value, string vat_currency,
            int expireMinutes = 20)
        {
            var symmetricKey = Encoding.ASCII.GetBytes(Secret);// Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("order_id", order_id), new Claim("reference_id", reference_id),
                    new Claim("transaction_value", transaction_value), new Claim("transaction_currency", transaction_currency),
                new Claim("vat_value", vat_value), new Claim("vat_currency", vat_currency)}),

                Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),

                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(symmetricKey),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);

            return token;
        }

    }

}
