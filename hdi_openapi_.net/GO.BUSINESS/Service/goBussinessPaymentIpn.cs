﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.Service
{
    public class goBussinessPaymentIpn
    {
        #region Contructor
        private static readonly Lazy<goBussinessPaymentIpn> InitInstance = new Lazy<goBussinessPaymentIpn>(() => new goBussinessPaymentIpn());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public static goBussinessPaymentIpn Instance => InitInstance.Value;
        #endregion

        //public BaseResponse ipnQRVnpay(object obj)
        //{
        //    //test git tesst master
        //    string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    BaseResponse response = new BaseResponse();
        //    JObject objReturn = new JObject();
        //    objReturn.Add("code", "00");
        //    objReturn.Add("message", "HDI da nhan duoc thong tin");
        //    JObject objCT = new JObject();
        //    objCT.Add("txnId", "");
        //    objReturn.Add("data", objCT);
        //    try
        //    {
        //        JObject jConfig = new JObject();
        //        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN QR VNPAY: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //        JObject jObjIpn = JObject.Parse(obj.ToString());
        //        if (jObjIpn == null || !jObjIpn["code"].ToString().Equals("00"))
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Không đúng json ipn qr vnpay", obj.ToString());
        //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không đúng json ipn qr vnpay : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //            response.Data = objReturn;
        //            return response;
        //        }
        //        string strUrlWebPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
        //        string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        //        //Lấy thông tin cấu hình đơn vị tạo QR


        //        string[] paramUpdate = new string[] { data.code, data.messageCode, data.paymentResult.order.orderCode, jConfig["ORG_PAY_CODE"].ToString(), data.paymentResult.order.amount,
        //            data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,jConfig["PAY_TYPE"].ToString(), data.paymentResult.order.bankCode };
        //        response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
        //        List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
        //        if (objHDBQRUpdate.Count == 0)
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái thanh toán  payment 399", obj.ToString());
        //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //            response.Data = objReturn;
        //            return response;
        //        }
        //        //thực thi gọi api webpaymet để cập nhật trạng thái
        //        List<JObject> lsOrg = new List<JObject>();
        //        List<JObject> lsFun = new List<JObject>();
        //        if (getORG_PAY("HDI", "CALLBACKIN", "CALL", ref lsOrg, ref lsFun))
        //        {
        //            //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong getORG_PAY: ", Logger.ConcatName(nameClass, LoggerName));
        //            string[] Param = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), environment };

        //            BaseResponse response1 = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Get_ORG_Config, environment, Param);
        //            List<JObject> objorg = goBusinessCommon.Instance.GetData<JObject>(response1, 0);
        //            if (lsFun.Count > 0)
        //            {
        //                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(objHDBQRUpdate), Logger.ConcatName(nameClass, LoggerName));
        //                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay xong Pay_Get_ORG_Config: " + JsonConvert.SerializeObject(lsFun) + JsonConvert.SerializeObject(objorg) + Param.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //                string strUrlCallBack = "";
        //                if (!string.IsNullOrEmpty(objorg[0]["URL_CALLBACK"]?.ToString()))
        //                {
        //                    //phải bổ xung thêm tham số vào urlcalback
        //                    strUrlCallBack = objorg[0]["URL_CALLBACK"]?.ToString();
        //                }
        //                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 1: " + JsonConvert.SerializeObject(lsFun[0]), Logger.ConcatName(nameClass, LoggerName));
        //                bool b = new goLibQR().callWebPayment(objHDBQRUpdate[0]["DATA_CHECK"].ToString(), strUrlCallBack, lsFun[0]);
        //                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lay 2: " + b.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //                if (!b)
        //                {
        //                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật webpayment  payment 429", data.paymentResult.order.transactionNo + objorg[0]["URL_CALLBACK"].ToString());
        //                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai webpayment : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //                }
        //            }

        //        }
        //        else
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY  payment 437", "HDI-callback");
        //            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Lỗi chưa cấu hình trang webpayment hdi SYS_ORG_PAY", Logger.ConcatName(nameClass, LoggerName));
        //        }
        //        objReturn["message"] = "Cập nhật thành công";
        //        objCT["txnId"] = data.paymentResult.order.orderCode;
        //        objReturn["data"] = objCT;
        //        //set lai tham so tra ve
        //        data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
        //        data.paymentResult.order.orderCode = objHDBQRUpdate[0]["ORDER_CODE"].ToString();
        //        data.paymentResult.order.amount = objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString();
        //        //thực thi gọi api tuấn để cập nhật order
        //        ResponseOrderIpn returnIPN = new ResponseOrderIpn();
        //        returnIPN.Success = true;
        //        returnIPN.Data = data;
        //        returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
        //        returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
        //        DateTime dtPay = DateTime.ParseExact(data.paymentResult.order.payDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
        //        returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy hh:mm:ss", "");
        //        //ở đây phải kiểm tra thêm trường hợp 
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
        //        BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN, objHDBQRUpdate[0]["ORDER_CODE"].ToString(), objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString());
        //        if (!respIPN_HDI.Success)
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 460", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
        //            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai hdi: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
        //            string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
        //            respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
        //            if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
        //            {
        //                goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 466", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
        //            }//kirmt ýt
        //        }

        //        response.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //        goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
        //        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
        //        objReturn["message"] = "Lỗi cập nhật đơn hàng";
        //        response.Data = objReturn;
        //    }
        //    response.Data = objReturn;
        //    return response;
        //}
    }
}
