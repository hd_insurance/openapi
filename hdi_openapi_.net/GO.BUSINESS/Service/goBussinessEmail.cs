﻿using GO.BUSINESS.Common;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Service;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GO.BUSINESS.Service
{
    public class goBussinessEmail
    {
        public string UrlResource = goBusinessCommon.Instance.GetConfigDB(goConstants.HdiResouce);
        private string nameClass = nameof(goBussinessEmail);
        #region Contructor
        private static readonly Lazy<goBussinessEmail> InitInstance = new Lazy<goBussinessEmail>(() => new goBussinessEmail());
        public static goBussinessEmail Instance => InitInstance.Value;
        #endregion
        #region Method
        public BaseResponse sendEmailPhaoHoa(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), "goBussinessEmail - sendEmailPhaoHoa");
                string[] arrAction = { "HDI_EMAIL_PH" };
                if (!Array.Exists(arrAction, i => i == request.Action.ActionCode))
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                    return response;
                }
                JObject jInput = JObject.Parse(request.Data.ToString());
                string strName = jInput["REQUESTER_NAME"]?.ToString();
                string strEmail = jInput["EMAIL"]?.ToString();
                string strPhone = jInput["PHONE"]?.ToString();
                string strNoiDung = jInput["CONTENT"]?.ToString();
                string host = "smtp.gmail.com", user = "Hotrophaohoaviet@gmail.com",//"Hotrophaohoaviet @gmail.com", 
                    pass = goConfig.Instance.GetAppConfig("passPhaoHoa"),
                    nameEmail = goConfig.Instance.GetAppConfig("nameEmail"),
                    strTieuDe = goConfig.Instance.GetAppConfig("TieuDe"),
                    strBCC = goConfig.Instance.GetAppConfig("maiBcc");
                bool enableSsl = true;
                int port = 587;
                var base64EncodedBytes = System.Convert.FromBase64String(goConfig.Instance.GetAppConfig("noidung"));
                string strContent = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                strContent = strContent.Replace("@NAME@", strName);
                //strContent = strContent.Replace("", strEmail);
                strContent = strContent.Replace("@PHONE@", strPhone);
                strContent = strContent.Replace("@NOI_DUNG@", strNoiDung);
                SendEmailPH(host, port, enableSsl, user, pass, nameEmail, strContent,
                    strEmail, strBCC, strTieuDe);//nguyennh@gmail.com
                response.Success = resLog.Success = true;
            }
            catch (Exception ex)
            {
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), ex.Message);
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - sendEmailPhaoHoa");
            }
            finally
            {
                LogServiceModel logService = new LogServiceModel();
                logService.typeService = nameof(goConstants.ServiceType.EMAIL);
                logService.request = request;
                logService.response = response;
                new Task(() => goBusinessCommon.Instance.LogsService(logService)).Start();

                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public async void SendEmailPH(string host, int port, bool enableSsl, string user, string pass,
            string nameEmail, string strContent, string strTo, string strBCC, string strTieuDe)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(user);
                    mail.To.Add(strTo);
                    mail.Bcc.Add(strBCC);
                    mail.Subject = strTieuDe;
                    mail.Body = strContent;
                    mail.IsBodyHtml = true;

                    using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                    {
                        smtp.Credentials = new NetworkCredential(user, pass);
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }
                }
                // MailMessage mail = new MailMessage();
                // SmtpClient client = new SmtpClient();

                // client.Host = host;
                // client.Port = port;
                // client.EnableSsl = enableSsl;
                // client.Credentials = new NetworkCredential(user, pass);
                // client.DeliveryMethod = SmtpDeliveryMethod.Network;
                // client.UseDefaultCredentials = true;
                // //client.Credentials = new NetworkCredential(user, pass, host);
                // //client.Host = "smtp.gmail.com";
                // //client.Port = 587;
                // //client.DeliveryMethod = SmtpDeliveryMethod.Network;
                // //client.EnableSsl = true;
                // //client.UseDefaultCredentials = true;
                // MailAddress from = new MailAddress(user, nameEmail);
                // mail.From = from;
                // mail.IsBodyHtml = true;
                // mail.Subject = strTieuDe;
                // mail.Body = strContent;
                // //test add ảnh
                // //AlternateView alterView = ContentToAlternateView(emailModel.content);
                // //mail.AlternateViews.Add(alterView);
                // mail.To.Add(strTo);
                ////         mail.CC.Add(item.email);
                // mail.Bcc.Add(strBCC);
                // mail.Priority = MailPriority.High;

                // client.Send(mail);
                // mail.Dispose();
                // client.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - SendEmail");
            }
        }

        public async void createLogSendMailJson(List<logEmailModel> lsEmailSend, string strOrgCode, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                string strEmailModel = JsonConvert.SerializeObject(lsEmailSend).ToString();
                JArray arrEmail = JArray.Parse(strEmailModel);
                object[] paramJson = new object[] { strOrgCode, arrEmail };//jInput['FILE'].ToString()
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.MD_LOG_MAIL_JSON, strEVM, paramJson);
                BaseResponse response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramJson), false, null, null, null, null);
                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objBatch == null || !objBatch.Any())
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi insert log sendmail vao database ", string.Join("-", paramJson));
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi insert log sendmail vao database " + string.Join("-", paramJson), Logger.ConcatName(nameClass, nameMethod));
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi insert log sendmail vao database ", JsonConvert.SerializeObject(lsEmailSend));
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi insert log sendmail vao database " + string.Join("-", JsonConvert.SerializeObject(lsEmailSend)), Logger.ConcatName(nameClass, nameMethod));
            }

        }

        public BaseResponse sendMaiJson(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject job = JObject.Parse(request.Data.ToString());
                    response = resLog = goBusinessServices.Instance.sendMailWithJson(job);
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi json khong dung cau truc", JsonConvert.SerializeObject(request).ToString());
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail json ", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), ex.Message);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse sendMailAdmin(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response= resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JArray job = JArray.Parse(request.Data.ToString());
                    response= resLog = goBusinessServices.Instance.sendMailAdmin(job);
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json aquynh " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi json khong dung cau truc  aquynh", JsonConvert.SerializeObject(request).ToString());
                    response= resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail json  aquynh", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), ex.Message);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse GetResponseEmail(BaseRequest request, string strUser = null)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            try
            {
                //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), "goBussinessEmail - GetResponseEmail");
                string[] arrAction = { "HDI_EMAIL_01" };
                if (!Array.Exists(arrAction, i => i == request.Action.ActionCode))
                {
                    response =resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                    return response;
                }
                ServiceEmailModel emailModel = new ServiceEmailModel();
                emailModel = JsonConvert.DeserializeObject<ServiceEmailModel>(request.Data.ToString());

                if (emailModel == null || string.IsNullOrEmpty(emailModel.subject) || string.IsNullOrEmpty(emailModel.content) || emailModel.to == null || !emailModel.to.Any())
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3002), "Email không chứa nội dung");
                    return response;
                }
                string host = "", user = "", pass = "", nameEmail = "";
                bool enableSsl = false;
                int port = 0;


                GetConfigEmail(ref host, ref port, ref enableSsl, ref user, ref pass, ref nameEmail);
                if (!string.IsNullOrEmpty(strUser))
                {
                    user = goBusinessCommon.Instance.GetConfigDB(goConstants.HDI_MAIL_ERROR);
                    pass = goBusinessCommon.Instance.GetConfigDB(goConstants.HDI_PASS_ERROR);
                }
                //new Task(() => SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel)).Start();
                SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel);
                response.Success = resLog.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), "goBussinessEmail - GetResponseEmail");
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), ex.Message);
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - GetResponseSMS");
            }
            finally
            {
                LogServiceModel logService = new LogServiceModel();
                logService.typeService = nameof(goConstants.ServiceType.EMAIL);
                logService.request = request;
                logService.response = response;
                new Task(() => goBusinessCommon.Instance.LogsService(logService)).Start();

                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse SendEmail_ORG(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), "goBussinessEmail - GetResponseEmail");
                string[] arrAction = { "HDI_EMAIL_01" };
                if (!Array.Exists(arrAction, i => i == request.Action.ActionCode))
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                    return response;
                }
                ServiceEmailModel emailModel = new ServiceEmailModel();
                emailModel = JsonConvert.DeserializeObject<ServiceEmailModel>(request.Data.ToString());
                var base64Content = System.Convert.FromBase64String(emailModel.content);
                string strContent = System.Text.Encoding.UTF8.GetString(base64Content);
                emailModel.content = strContent;
                if (emailModel == null || string.IsNullOrEmpty(emailModel.subject) || string.IsNullOrEmpty(emailModel.content) || emailModel.to == null || !emailModel.to.Any())
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3002), "Email không chứa nội dung");
                    return response;
                }
                string host = "", user = "", pass = "", nameEmail = "";
                bool enableSsl = false;
                int port = 0;


                GetConfigEmail(ref host, ref port, ref enableSsl, ref user, ref pass, ref nameEmail);
                new Task(() => SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel)).Start();
                response.Success= resLog.Success = true;
            }
            catch (Exception ex)
            {
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), ex.Message);
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - GetResponseSMS");
            }
            finally
            {
                LogServiceModel logService = new LogServiceModel();
                logService.typeService = nameof(goConstants.ServiceType.EMAIL);
                logService.request = request;
                logService.response = response;
                new Task(() => goBusinessCommon.Instance.LogsService(logService)).Start();

                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public void sendEmaiCMS(string strEmail, string strSubject, string strContent)
        {
            try
            {
                string host = "", user = "", pass = "", nameEmail = "";
                bool enableSsl = false;
                int port = 0;
                ServiceEmailModel emailModel = new ServiceEmailModel();
                if (string.IsNullOrEmpty(strEmail))
                    strEmail = pass = goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_CMS);
                string[] strEmailTo = strEmail.Split(';');
                List<EmailModel> lsEmail = new List<EmailModel>();
                foreach (string str in strEmailTo)
                {
                    EmailModel eMail = new EmailModel();
                    eMail.email = str;
                    lsEmail.Add(eMail);
                }
                emailModel.to = lsEmail;
                emailModel.content = strContent;
                emailModel.subject = strSubject;
                GetConfigEmail(ref host, ref port, ref enableSsl, ref user, ref pass, ref nameEmail);
                user = goBusinessCommon.Instance.GetConfigDB(goConstants.HDI_MAIL_ERROR);
                pass = goBusinessCommon.Instance.GetConfigDB(goConstants.HDI_PASS_ERROR);
                new Task(() => SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel)).Start();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - sendEmaiCMS");
            }
        }
        public async void createDBSendMail(string strOrgCode, string strCamCode, string strCamContent, string strCamMode, string strCamName, string strCamType, string strDataType,
            string strEmail, string strEmailCC,
            string strEmailBCC, string strEmailContent, string strPhone, string strPriority, string strSendType, string strSMSContent, string strSubject, string strUserName)
        {
            try
            {
                ServiceCampainModel camModel = new ServiceCampainModel();
                camModel.activeDate = DateTime.Now.ToString("dd/MM/yyyy");
                camModel.camCode = strCamCode;
                camModel.camContent = strCamContent;
                camModel.camMode = strCamMode;
                camModel.camName = strCamName;
                camModel.camType = strCamType;//ON_DEMAND
                camModel.configCode = "Config";
                camModel.dataType = strDataType;//"INTERNAL"
                camModel.email = strEmail;
                camModel.emailCC = strEmailCC;
                camModel.emailBCC = strEmailBCC;
                camModel.emailContent = strEmailContent;
                camModel.orgCode = strOrgCode;
                camModel.phone = strPhone;
                camModel.priority = strPriority;
                camModel.sendType = "ALL";
                camModel.smsContent = strSMSContent;
                camModel.smsID = "0";
                camModel.subject = strSubject;
                camModel.total = "0";
                int countSMS = 0;
                string strSMS = "";
                if (!strPhone.Equals("X"))
                {
                    strSMS = goBussinessSMS.Instance.checkMessage(strSMSContent, strPhone, ref countSMS);
                    camModel.smsContent = strSMS;
                    camModel.total = countSMS.ToString();
                }
                camModel.userName = strUserName;
                //           strOrgCode, strCamCode  , strCamName, strCamType, strCamMode, camModel.sendType,
                //             strDataType, camModel.activeDate, strPriority, strCamContent, strEmail, strSubject,
                //             strEmailCC , strEmailBCC, strOrgContent, strPhone, strSMS, camModel.smsID, camModel.configCode, countSMS, strUserName  ,
                string[] paramUpdate = new string[] { strOrgCode, strCamCode, strCamName, strCamType, strCamMode, camModel.sendType, strDataType, camModel.activeDate,
                strPriority, strCamContent, strEmail, strSubject, strEmailCC , strEmailBCC, strEmailContent, strPhone, strSMS, camModel.smsID, camModel.configCode, countSMS.ToString(), strUserName};
                BaseResponse response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Insert_Campaign, "LIVE", paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không insert được mail cam", strOrgCode + ":" + strUserName);
                }
            }
            catch (Exception ex)
            {

            }

        }

        public async void createLogSendMail(string strOrgCode, string strCamCode, string strProductCode, string strPackCode, string strTypeEmai, string strDate,
            string strEmail, string strSubject, string strEmailCC, string strEmailBCC, string strEmailContent, string strUserName, string strReffID)
        {
            try
            {

                string[] paramUpdate = new string[] { strOrgCode, strCamCode,strProductCode , strPackCode, strTypeEmai, strDate, strEmail, strSubject,
                strEmailCC, strEmailBCC, strEmail, "HDI_SEND", strReffID};
                BaseResponse response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Log_Email, "LIVE", paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không insert được mail cam", strOrgCode + ":" + strUserName);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessEmail 444 - sendEmaiCMS" + ex.Message);
            }

        }
        public async void SendEmail(string host, int port, bool enableSsl, string user, string pass, string nameEmail, ServiceEmailModel emailModel)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient();

                client.Host = host;
                client.Port = port;
                client.EnableSsl = enableSsl;
                client.Credentials = new NetworkCredential(user, pass);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                MailAddress from = new MailAddress(user, nameEmail);
                mail.From = from;
                mail.IsBodyHtml = true;
                mail.Subject = emailModel.subject;
                mail.Body = emailModel.content;
                //test add ảnh
                AlternateView alterView = ContentToAlternateView(emailModel.content);
                mail.AlternateViews.Add(alterView);

                foreach (var item in emailModel.to)
                {
                    mail.To.Add(item.email);
                }
                if (emailModel.cc != null)
                {
                    foreach (var item in emailModel.cc)
                    {
                        mail.CC.Add(item.email);
                    }
                }
                if (emailModel.bcc != null)
                {
                    foreach (var item in emailModel.bcc)
                    {
                        mail.Bcc.Add(item.email);
                    }
                }     
                if(emailModel.attach == "YES" || String.IsNullOrEmpty(emailModel.attach))
                {
                    if (emailModel.file != null && emailModel.file.Any())
                    {
                        Attachment at;
                        foreach (var item in emailModel.file)
                        {
                            if (!item.path.Equals(""))
                            {
                                //string urlFile = UrlResource + "/" + item.path;
                                string name = item.path + ".pdf";
                                //WebClient web = new WebClient();
                                //byte[] data = web.DownloadData(urlFile);
                                byte[] data;
                                if (item.path.Length > 300)
                                {
                                    if (string.IsNullOrEmpty(item.name))
                                        name = Guid.NewGuid().ToString("N").Substring(0, 8) + ".pdf";
                                    else
                                        name = item.name;
                                    data = System.Convert.FromBase64String(item.path);
                                }
                                else
                                    data = goBusinessCommon.Instance.getFileServer(item.path);
                                if (data != null)
                                {
                                    MemoryStream ms = new MemoryStream(data);
                                    if (item.path.Length < 300 && item.path.Replace(@"\", "/").Contains("/"))
                                    {
                                        string[] splArr = item.path.Split('/');
                                        name = splArr[splArr.Length - 1];
                                    }
                                    at = new Attachment(ms, name);
                                    mail.Attachments.Add(at);
                                }
                                else
                                {
                                    //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - SendEmail");
                                }
                            }

                        }
                    }
                }    
                
                client.Send(mail);
                mail.Dispose();
                client.Dispose();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message + JsonConvert.SerializeObject(emailModel).ToString(), "goBussinessEmail - SendEmail ");
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail của hệ thống", JsonConvert.SerializeObject(emailModel).ToString());
            }
        }
        private static AlternateView ContentToAlternateView(string content)
        {
            var imgCount = 0;
            List<LinkedResource> resourceCollection = new List<LinkedResource>();
            foreach (Match m in Regex.Matches(content, "<img(?<value>.*?)>"))
            {
                imgCount++;
                var imgContent = m.Groups["value"].Value;
                string type = Regex.Match(imgContent, ":(?<type>.*?);base64,").Groups["type"].Value;
                string base64 = Regex.Match(imgContent, "base64,(?<base64>.*?)\"").Groups["base64"].Value;
                if (String.IsNullOrEmpty(type) || String.IsNullOrEmpty(base64))
                {
                    //ignore replacement when match normal <img> tag
                    continue;
                }
                var replacement = " src=\"cid:" + imgCount + "\"";
                content = content.Replace(imgContent, replacement);
                var tempResource = new LinkedResource(Base64ToImageStream(base64), new ContentType(type))
                {
                    ContentId = imgCount.ToString()
                };
                resourceCollection.Add(tempResource);
            }

            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(content, null, MediaTypeNames.Text.Html);
            foreach (var item in resourceCollection)
            {
                alternateView.LinkedResources.Add(item);
            }

            return alternateView;
        }
        public BaseResponse reSendMailError(JObject request)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["EMAIL_TO"].ToString()).ToLower();
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    ServiceEmailModel emailModel = JsonConvert.DeserializeObject<ServiceEmailModel>(request["EMAIL_MODEL"].ToString());
                    if (emailModel == null || string.IsNullOrEmpty(emailModel.subject) || string.IsNullOrEmpty(emailModel.content) || emailModel.to == null || !emailModel.to.Any())
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3002), "Email không chứa nội dung");
                        return response;
                    }
                    string host = "", user = "", pass = "", nameEmail = "";
                    bool enableSsl = false;
                    int port = 0;
                    GetConfigEmail(ref host, ref port, ref enableSsl, ref user, ref pass, ref nameEmail);
                    SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel);
                    response.Success = true;
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

            }
            catch (Exception ex)
            {
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - GetResponseSMS");
            }
            return response;
        }

        public static Stream Base64ToImageStream(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);
            return ms;
        }
        public void GetConfigEmail(ref string host, ref int port, ref bool enableSsl, ref string user, ref string pass, ref string nameEmail)
        {
            //int isDebug = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig("isDebug"), 1);
            //if (isDebug == 0)
            //{
            host = goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_HOST);
            port = goUtility.Instance.ConvertToInt32(goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_PORT), 0);
            user = goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_USER);
            pass = goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_PASSWORD);
            nameEmail = goBusinessCommon.Instance.GetConfigDB(goConstants.NAMEMAIL);
            enableSsl = goBusinessCommon.Instance.GetConfigDB(goConstants.EMAIL_ENABLESSL) == "0" ? false : true;
            //}
            //else
            //{
            //    host = goConfig.Instance.GetAppConfig("email_host");
            //    port = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig("email_port"), 0);
            //    user = goConfig.Instance.GetAppConfig("email_user");
            //    pass = goConfig.Instance.GetAppConfig("email_password");
            //    nameEmail = goConfig.Instance.GetAppConfig("nameMail");
            //    enableSsl = goConfig.Instance.GetAppConfig("email_enableSsl") == "0" ? false : true;
            //}
        }
        #endregion
    }
}
