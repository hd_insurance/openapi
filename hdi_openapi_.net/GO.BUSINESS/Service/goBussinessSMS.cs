﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Ecommerce;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Service;
using GO.DTO.SystemModels;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GO.BUSINESS.Service
{
    public class goBussinessSMS
    {
        #region Variables
        public string _client_id { get; set; }
        public string _client_secret { get; set; }
        public string _url { get; set; }
        public string _session_id { get; set; }
        public string _access_token { get; set; }
        public string _brand_name { get; set; }
        #endregion
        #region Contructor
        private static readonly Lazy<goBussinessSMS> InitInstance = new Lazy<goBussinessSMS>(() => new goBussinessSMS());
        public static goBussinessSMS Instance => InitInstance.Value;
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Method
        public bool sendSMSSystem(TempConfigModel template, string strPhone, string strContent, string strUser, string strRefID)
        {
            BaseResponse response = new BaseResponse();
            bool returnKQ = true;
            try
            {   
                string url = "";
                string path = "";
                string client_id = "";
                string client_secret = "";
                string session_id = "";
                string brand_name = "";
                if (string.IsNullOrEmpty(strPhone) || string.IsNullOrEmpty(strContent))
                {
                    return false;
                }
                GetConfigFpt(ref url, ref path, ref client_id, ref client_secret, ref session_id, ref brand_name);
                //Lấy các thông tin dủa fpt lên để khởi tạo
                goBussinessSMS.Instance.SetConfig(url, client_id, client_secret, session_id, brand_name);
                int iCount = 0;
                string strSMS = goBussinessSMS.Instance.checkMessage(strContent, strPhone, ref iCount);
                if (iCount == 99)
                {
                    //MessageBox.Show("Số lượng tin nhắn vượt quá 3 tin");                    
                }
                if (iCount == 0)
                {
                    //MessageBox.Show("Lỗi khi chuyển base 64 tin nhắn " + strSMS);                    
                }
                
                string[] paramUpdate = new string[] {  };
                if (string.IsNullOrEmpty(strSMS))
                {
                    paramUpdate = new string[] { template.ORG_CODE, "ERROR", "Nội dung không đúng", "-1", "KXD", "ERROR", template.TEMP_CODE, strPhone, strContent, iCount.ToString(), strRefID, template.PRODUCT_CODE, template.PACK_CODE, "SMS", strUser,template.TEMP_SMS[0].ISCALLBACK };

                    returnKQ= false;
                }else
                {
                    SEND_SMS sms = new SEND_SMS(path, "send_brandname_otp", strPhone, strSMS);
                    RESULT_SMS rs = goBussinessSMS.Instance.sendSMS(sms);
                    if (rs.error==1)
                    {
                        paramUpdate = new string[] { template.ORG_CODE, rs.error.ToString(), JsonConvert.SerializeObject(rs), rs.MessageId, rs.Telco, "DONE", template.TEMP_CODE, strPhone, strContent, iCount.ToString(), strRefID, template.PRODUCT_CODE, template.PACK_CODE, "SMS", strUser, template.TEMP_SMS[0].ISCALLBACK };                        
                    }
                    else
                    {
                        returnKQ = false;
                        paramUpdate = new string[] { template.ORG_CODE, rs.error.ToString(), JsonConvert.SerializeObject(rs), "-1", "FPT_RES", "ERROR", template.TEMP_CODE, strPhone, strContent, iCount.ToString(), strRefID, template.PRODUCT_CODE, template.PACK_CODE, "SMS", strUser, template.TEMP_SMS[0].ISCALLBACK };
                    }
                }                
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_LOG_SMS, "LIVE", paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate == null || !objHDBQRUpdate.Any())
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi insert trạng thái sms", strPhone + strUser + strRefID);
                }
            }
            catch (Exception ex)
            {
                returnKQ = false;
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi tin nhắn SMS", strPhone + strUser + strRefID + ex.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - GetResponseSMS");
            }
            return returnKQ;
        }



        public void SetConfig(string strUrl, string client_id, string client_secret, string session_id, string brand_name)
        {
            this._client_id = client_id;
            this._client_secret = client_secret;
            this._url = strUrl;
            this._session_id = session_id;
            this._brand_name = brand_name;
        }
        public BaseResponse ipnSMS(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Data IPN SMS FPT: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
            
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            BaseResponse response = new BaseResponse();
            try
            {
                JObject jConfig = JObject.Parse(obj.ToString());
                string strID_SMS = jConfig["smsid"].ToString();
                string strStatus = jConfig["status"].ToString();
                //p_CAME_ID,p_CAMPAIGN_CODE, p_PHONE, p_SMS_ID,p_TELCO, p_STATUS,p_ERROR_CODE ,p_ERROR_MESSAGE , p_USER_NAME
                if (strStatus.Equals("1"))
                {
                    response = updateStatusSMS("X", strID_SMS, "X", "INVESTED", "000", "Thành công","","");
                    //ipn thành công thực hiện call back
                    
                    if (response.Success)
                    {
                        List<JObject> lsObjUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (lsObjUpdate[0]["ISCALLBACK"].ToString().Equals("1"))
                        {
                            //goBusinessEcommerce.Instance.callBackSMS(lsObjUpdate[0]["ORG_CODE"].ToString(), lsObjUpdate[0]["REF_ID"].ToString());
                            new Task(() => goBusinessEcommerce.Instance.callBackSMS(lsObjUpdate[0]["ORG_CODE"].ToString(), lsObjUpdate[0]["REF_ID"].ToString())).Start();
                        }
                    }
                }
                else
                {
                    response = updateStatusSMS("X", strID_SMS, "X", "INVESTED", strStatus, "Lỗi tra soát","","" );
                    
                }
                if (!response.Success)
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update ipn tin nhắn SMS", JsonConvert.SerializeObject(response));
                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật trạng thái thanh toán từ telco", ex.Message);               
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnSMS", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        //public BaseResponse updateSMSDB(string strCamID, string strCamCode, string strPhone, string strSMS_ID, string strTelco, string strStatus, string strErrorCode,
        //    string strMessage, string strUser)
        //{
        //    //test git tesst master
        //    string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    AuditLogsInfo auditLogs = new AuditLogsInfo();
        //    BaseResponse response = new BaseResponse();
        //    try
        //    {
        //        //p_CAME_ID,p_CAMPAIGN_CODE, p_PHONE, p_SMS_ID,p_TELCO, p_STATUS,p_ERROR_CODE ,p_ERROR_MESSAGE , p_USER_NAME
        //        //{ "X", "X", "X", strID_SMS, "X", "INVESTED", "000", "Thành công", "FPT" }
        //        string[] paramUpdate = new string[] { strCamID, strCamCode, strPhone, strSMS_ID, strTelco, strStatus, strErrorCode, strMessage, "FPT" };                
        //        response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Update_Cam_SMS, "LIVE", paramUpdate);
        //        List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
        //        if (objHDBQRUpdate.Count == 0)
        //        {
        //            response.Success = false;
        //        }
        //        response.Success = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //        response.Success = false;
        //    }
        //    return response;
        //}
        /// <summary>
        /// Hàm tạo token key để gọi các hàm sang FPT
        /// </summary>
        /// <param name="session_id">Tham số khởi tạo token dùng để truyền cho các hàm tiếp theo.</param>
        /// <returns>Trả về token để truyền trong các giao dịch khác nhau</returns>
        public string createToken()
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            JObject oJson = new JObject();
            try
            {                                
                
                oJson.Add("grant_type", "client_credentials");
                oJson.Add("client_id", this._client_id);
                oJson.Add("client_secret", this._client_secret);
                oJson.Add("scope", "send_brandname_otp");
                oJson.Add("session_id", this._session_id);
                HttpResponseMessage response = postJsonAPI(oJson, this._url + "/oauth2/token", null);
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "sms respos create token : " + parsedString, Logger.ConcatName(nameClass, LoggerName));
                JObject oJsonOutput = JObject.Parse(parsedString);
                if (response.IsSuccessStatusCode)
                {
                    this._access_token = oJsonOutput["access_token"].ToString();
                    return oJsonOutput["access_token"].ToString();
                }
                else
                {
                    throw new Exception("get token error: " + oJsonOutput["error_description"].ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Config: " + oJson.ToString() + this._url, Logger.ConcatName(nameClass, LoggerName));
                throw new Exception("get token error: " + ex.ToString() + oJson.ToString() + this._url);
            }
        }
        /// <summary>
        /// Hàm gửi 1 SMS 
        /// </summary>
        /// <param name="SEND_SMS">Các tham số phải có dữ liệu (access_token,BrandName,Message,Phone,session_id)</param>
        /// <returns>Trả về danh sách thông tin gửi (MessageId,PartnerId,Telco)</returns>
        public RESULT_SMS sendSMS(SEND_SMS _jInput)
        {
            try
            {
                RESULT_SMS result = new RESULT_SMS();
                if (this._brand_name.Equals("") || _jInput.Message.Equals("")
                    || _jInput.Phone.Equals("") || _session_id.Equals(""))
                {
                    result.error = 2;
                    result.error_description = "Tham số đầu vào không được để trống";
                    return result;
                }
                if (string.IsNullOrEmpty(_access_token))
                {
                    createToken();
                }
                _jInput.access_token = this._access_token;
                _jInput.session_id = this._session_id;
                _jInput.BrandName = this._brand_name;
                JObject oJsonInput = JObject.FromObject(_jInput);
                //oJsonInput.Add("scope", "send_brandname_otp");
                result = reCreateToken(_jInput.funtion, oJsonInput);//"/api/push-brandname-otp"
                if (result.error.Equals(1))
                {
                    JObject jsonOut = JObject.Parse(result.error_description);
                    result.MessageId = jsonOut["MessageId"].ToString();
                    result.PartnerId = jsonOut["PartnerId"].ToString();
                    result.Telco = jsonOut["Telco"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, "goBussinessSMS - sendSMS");
                throw new Exception(ex.ToString());
            }
        }
        private RESULT_SMS reCreateToken(string strFunction, JObject oJsonInput)
        {
            RESULT_SMS result = new RESULT_SMS();
            try
            {
                //if (oJsonInput["access_token"].ToString() == "")
                //{
                //    string strAccessTK = createToken();
                //    oJsonInput["access_token"] = strAccessTK;
                //}
                result = callApiFpt(strFunction, oJsonInput);
                if (!result.error.Equals(1))
                {
                    if (result.error.Equals(1011) || result.error.Equals(1012) || result.error.Equals(1013) 
                        || result.MessageId.Equals(null) || string.IsNullOrEmpty(result.MessageId))
                    {
                        createToken();
                        result = callApiFpt(strFunction, oJsonInput);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public bool sendSMS_API(SMS_CONTENT sms, string strUrl)
        {
            try
            {
                HttpResponseMessage response = postJsonAPI(JObject.FromObject(sms), strUrl, null);
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (response.IsSuccessStatusCode)
                {
                    BaseResponse bs = JsonConvert.DeserializeObject<BaseResponse>(parsedString);
                    if (bs.Success)
                    {
                        return false;
                    }
                    else
                        return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG,"Send sms content " + ex.Message, "goBussinessSMS - sendSMS");
                return false;
            }
        }
        private RESULT_SMS callApiFpt(string strFunction, JObject JInput)
        {
            RESULT_SMS result = new RESULT_SMS();
            try
            {
                HttpResponseMessage response = postJsonAPI(JInput, this._url + strFunction, null);
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (response.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    result.error = 1;
                    result.error_description = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    result.error = Int16.Parse(details["error"].ToString());
                    result.error_description = details["error_description"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        /// <summary>
        /// Hàm kiểm tra và chuyển đổi message thành không dấu -->base 64
        /// </summary>
        /// <param name="iError">Mã lỗi nhận được khi trả về</param>
        /// <returns>Chuỗi tin nhắn chuyển sang định dạng base64</returns>
        public string getErrorCode(int iError)
        {
            switch (iError)
            {
                case 1:
                    return "Gửi thành công";
                case 2:
                    return "Tham số đầu vào không được để trống";
                case 1001:
                    return "Request không hợp lệ";
                case 1002:
                    return "Client không được cấp phép truy cập";
                case 1003:
                    return "Truy cập bị từ chối";
                case 1004:
                    return "Loại response yêu cầu không được hỗ trợ";
                case 1005:
                    return "Các scope không hợp lệ";
                case 1006:
                    return "Lỗi server";
                case 1007:
                    return "Server tạm thời không thể xử lý request từ client";
                case 1008:
                    return "Thông tin client không đúng";
                case 1009:
                    return "Loại hình cấp quyền không hợp lệ";
                case 1010:
                    return "Scope không đủ để truy cập API";
                case 1011:
                    return "Access token không hợp lệ";
                case 1012:
                    return "Access token đã bị thay đổi";
                case 1013:
                    return "Access token hết hạn";
                case 1014:
                    return "Các tham số truyền vào bị lỗi";
                case 1015:
                    return "Không hỗ trợ kiêu loại hình cấp quyền này";
                case 1016:
                    return "Số lượng tin nhắn gửi đã vượt hạn mức";
                default:
                    return "Không xác định";
            }
            return "";
        }
        /// <summary>
        /// Hàm kiểm tra và chuyển đổi message thành không dấu -->base 64
        /// </summary>
        /// <param name="strMessage">message cần gửi</param>
        /// <param name="strPhone">số điện thoại nhận tin nhắn</param>
        /// <param name="iCount">Số lương tien nhắn của message</param>
        /// <returns>Chuỗi tin nhắn chuyển sang định dạng base64</returns>
        public string checkMessage(string strMessage, string strPhone, ref int iCount)
        {
            iCount = 0;
            try
            {
                //chuyển đổi sang tiếng việt không dấu
                string strSMS = removeMark(strMessage);
                //kiểm tra xem có phải sim vietnam mobile không
                bool checkSim = checkPhone(strPhone);

                if (checkSim)
                {
                    if (strSMS.Length < 127)
                        iCount = 1;
                    if (strSMS.Length > 126 && strSMS.Length < 273)
                        iCount = 2;
                    if (strSMS.Length > 272 && strSMS.Length < 426)
                        iCount = 3;
                    if (strSMS.Length > 425)
                        iCount = 99;
                }
                else
                {
                    if (strSMS.Length < 161)
                        iCount = 1;
                    if (strSMS.Length > 160 && strSMS.Length < 307)
                        iCount = 2;
                    if (strSMS.Length > 306 && strSMS.Length < 460)
                        iCount = 3;
                    if (strSMS.Length > 459)
                        iCount = 99;
                }
                if (iCount == 99)
                {
                    return "";
                }
                return Base64Encode(strSMS);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        private static bool checkPhone(string strPhone)
        {
            //kiểm tra số vietnam mobile: true --> vietnam mobile
            if (strPhone.Substring(0, 4).Equals("8492") || strPhone.Substring(0, 4).Equals("8456") || strPhone.Substring(0, 4).Equals("8458"))
                return true;
            if (strPhone.Substring(0, 3).Equals("092") || strPhone.Substring(0, 3).Equals("056") || strPhone.Substring(0, 3).Equals("058"))
                return true;
            if (strPhone.Substring(0, 2).Equals("92") || strPhone.Substring(0, 2).Equals("56") || strPhone.Substring(0, 2).Equals("58"))
                return true;
            return false;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public string removeMark(string str)
        {
            string[] a = { "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ" };
            string[] aUpper = { "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ" };
            string[] e = { "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ" };
            string[] eUpper = { "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ" };
            string[] i = { "ì", "í", "ị", "ỉ", "ĩ" };
            string[] iUpper = { "Ì", "Í", "Ị", "Ỉ", "Ĩ" };
            string[] o = { "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ" };
            string[] oUpper = { "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ" };
            string[] u = { "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ" };
            string[] uUpper = { "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ" };
            string[] y = { "ỳ", "ý", "ỵ", "ỷ", "ỹ" };
            string[] yUpper = { "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ" };
            str = str.Replace("đ", "d");
            str = str.Replace("Đ", "D");
            foreach (string a1 in a)
            {
                str = str.Replace(a1, "a");
            }
            foreach (string a1 in aUpper)
            {
                str = str.Replace(a1, "A");
            }
            foreach (string e1 in e)
            {
                str = str.Replace(e1, "e");
            }
            foreach (string e1 in eUpper)
            {
                str = str.Replace(e1, "E");
            }
            foreach (string i1 in i)
            {
                str = str.Replace(i1, "i");
            }
            foreach (string i1 in iUpper)
            {
                str = str.Replace(i1, "I");
            }
            foreach (string o1 in o)
            {
                str = str.Replace(o1, "o");
            }
            foreach (string o1 in oUpper)
            {
                str = str.Replace(o1, "O");
            }
            foreach (string u1 in u)
            {
                str = str.Replace(u1, "u");
            }
            foreach (string u1 in uUpper)
            {
                str = str.Replace(u1, "U");
            }
            foreach (string y1 in y)
            {
                str = str.Replace(y1, "y");
            }
            foreach (string y1 in yUpper)
            {
                str = str.Replace(y1, "Y");
            }
            return str;
        }
        public static HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PostAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Service timeout " + ex.ToString());
            }

        }

        public BaseResponse GetResponseSMS(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, JsonConvert.SerializeObject(request), "goBussinessSMS - GetResponseSMS");
                string[] arrAction = { "HDI_SMS_01" };

                if (!Array.Exists(arrAction, i => i == request.Action.ActionCode))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3001), goConstantsError.Instance.ERROR_3001);
                    return response;
                }
                object[] goParam = null;
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);
                string url = "";
                string path = "";
                string client_id = "";
                string client_secret = "";
                string session_id = "";
                string brand_name = "";
                string phone = "";
                string content = "";
                string strSendID = "";
                if (goParam == null || goParam.Length < 1 || string.IsNullOrEmpty(goParam[0].ToString()) || string.IsNullOrEmpty(goParam[1].ToString()))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                    return response;
                }

                if (goParam != null && goParam.Length > 1)
                {
                    phone = goParam[0].ToString();
                    content = goParam[1].ToString();
                    strSendID= goParam[2].ToString();
                }

                GetConfigFpt(ref url, ref path, ref client_id, ref client_secret, ref session_id, ref brand_name);
                //Lấy các thông tin dủa fpt lên để khởi tạo
                goBussinessSMS.Instance.SetConfig(url, client_id, client_secret, session_id, brand_name);
                int iCount = 0;
                string strSMS = goBussinessSMS.Instance.checkMessage(content, phone, ref iCount);
                if (iCount == 99)
                {
                    //MessageBox.Show("Số lượng tin nhắn vượt quá 3 tin");                    
                }
                if (iCount == 0)
                {
                    //MessageBox.Show("Lỗi khi chuyển base 64 tin nhắn " + strSMS);                    
                }

                SEND_SMS sms = new SEND_SMS(path, "send_brandname_otp", phone, strSMS);
                RESULT_SMS rs = goBussinessSMS.Instance.sendSMS(sms);
                if (rs.error != 1)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG,"fpt RESPONSE: " + JsonConvert.SerializeObject(rs), "goBussinessSMS - GetResponseSMS");
                    response = updateStatusSMS(strSendID, rs.MessageId, rs.Telco , "ERROR", rs.error.ToString(), rs.error_description,phone, strSMS);
                    if(!response.Success)
                        goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update tin nhắn SMS", JsonConvert.SerializeObject(response));
                    //thông báo lỗi
                    //rs.error_description;
                    response.Success = false;
                    response.Error = "HDI_SMS_" + rs.error;
                    response.ErrorMessage = getErrorCode(rs.error);                    
                }
                else
                {
                    response = updateStatusSMS(strSendID, rs.MessageId, rs.Telco, "DONE", rs.error.ToString(), rs.error_description, phone, strSMS);
                    if (!response.Success)
                        goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update tin nhắn SMS", JsonConvert.SerializeObject(response));
                    response.Success = true;

                    //cập nhật dữ liệu DB
                    //rs.MessageId;
                    //rs.PartnerId;
                    //rs.Telco
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi tin nhắn SMS", JsonConvert.SerializeObject(request) + ex.ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBussinessSMS - GetResponseSMS");
            }
            finally
            {
                LogServiceModel logService = new LogServiceModel();
                logService.typeService = nameof(goConstants.ServiceType.SMS);
                logService.request = request;
                logService.response = response;
                new Task(() => goBusinessCommon.Instance.LogsService(logService)).Start();

                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse updateStatusSMS(string p_CAME_ID, string p_SMS_ID, string p_TELCO, string p_STATUS, string p_ERROR_CODE, string p_ERROR_MESSAGE,
            string p_Phone, string strContent)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                string[] paramUpdate = new string[] { p_CAME_ID, p_SMS_ID, p_TELCO, p_STATUS, p_ERROR_CODE, p_ERROR_MESSAGE,"Service SMS", p_Phone, strContent };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Update_Cam_SMS, "LIVE", paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    response.Success = false;
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update trạng thái sms", p_CAME_ID + p_SMS_ID + p_TELCO + p_ERROR_MESSAGE);
                }
                else
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi update trạng thái sms ex", p_CAME_ID + p_SMS_ID + p_TELCO + ex.ToString());
            }
            return response;

        }
        public void GetConfigFpt(ref string url, ref string path, ref string client_id, ref string client_secret, ref string session_id, ref string brand_name)
        {
            //int isDebug = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig("isDebug"), 1);
            //if (isDebug == 0)
            //{
                url = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_URL);
                path = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_PATH);
                client_id = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_CLIENT_ID);
                client_secret = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_SECRET);
                session_id = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_SESSION_ID);
                brand_name = goBusinessCommon.Instance.GetConfigDB(goConstants.FPT_SMS_BRAND_NAME);
            //}
            //else
            //{
            //    url = goConfig.Instance.GetAppConfig("fpt_sms_url");
            //    path = goConfig.Instance.GetAppConfig("fpt_sms_path");
            //    client_id = goConfig.Instance.GetAppConfig("fpt_sms_client_id");
            //    client_secret = goConfig.Instance.GetAppConfig("fpt_sms_client_secret");
            //    session_id = goConfig.Instance.GetAppConfig("fpt_sms_session_id");
            //    brand_name = goConfig.Instance.GetAppConfig("fpt_sms_brand_name");
            //}
        }

        #endregion
    }
}
