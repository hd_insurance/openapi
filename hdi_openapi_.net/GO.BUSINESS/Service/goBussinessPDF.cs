﻿using EvoPdf;
using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.PaymentCallBack;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DAO.Common;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Insurance;
using GO.DTO.Service;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using GO.SERVICE.CallApi;
using GO.SERVICE.Sign;
using MessagingToolkit.QRCode.Codec;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUglify;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static GO.HELPER.Utility.goUtility;

namespace GO.BUSINESS.Service
{
    public class goBussinessPDF
    {
        #region Variables
        private string nameClass = nameof(goBussinessPDF);
        public static string pathRoot = goUtility.Instance.GetPathProject();
        public string strPathTemplate = pathRoot + @"/Template/";
        public string strWatermark = "";

        #endregion
        #region Priorities
        public goBussinessPDF()
        {
            goUtility.Instance.CreateFolder(strPathTemplate);
        }
        #endregion
        #region Contructor
        private static readonly Lazy<goBussinessPDF> InitInstance = new Lazy<goBussinessPDF>(() => new goBussinessPDF());
        public static goBussinessPDF Instance => InitInstance.Value;
        #endregion

        #region Method
        public SignFileReturnModel createPDF_GCN(TempEmailModel tempConfigModel, JObject jValueInsuran,string strGuiID_File)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {
                //xử lý các thông tin mẫu replaceHtmlValueJson
                string strHeader = tempConfigModel.HEADER; 
                string strUrlAddPdf = "";
                if (tempConfigModel.IS_ATTACH.ToString().Equals("0") && !string.IsNullOrEmpty(tempConfigModel.URL_PAGE_ADD))
                    strUrlAddPdf = tempConfigModel.URL_PAGE_ADD.ToString();
                string strCer = jValueInsuran["CERTIFICATE_NO"]?.ToString();
                if (string.IsNullOrEmpty(strCer))
                {
                    strCer = jValueInsuran["CONTRACT_NO"]?.ToString();
                    fileReturn.REF_TYPE = "CONTRACT";
                }
                else
                {
                    fileReturn.REF_TYPE = "CER";
                }
                List<TempFileSign> lsFileConvert = new List<TempFileSign>();
                int iCount = 1;
                TempFileSign fileTemp = new TempFileSign();
                string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlValueJson(jValueInsuran, tempConfigModel);
                string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
                strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                goBussinessPDF cls = new goBussinessPDF();
                string strBase64 = "";
                strWatermark = tempConfigModel.WATERMARK;
                if (tempConfigModel.PAGE_SIZE != null && tempConfigModel.PAGE_SIZE.IndexOf(",") >0)
                {                    
                    strBase64 = cls.convertPageToPDFPersnalA5(strHeader, strBodyHtml, tempConfigModel.FOOTER, strUrlAddPdf, tempConfigModel.LIST_REPLACE, strCer,tempConfigModel.PAGE_SIZE, tempConfigModel.HEADER_HEIGHT, tempConfigModel.FOOTER_HEIGHT, tempConfigModel.WATERMARK);
                }
                else
                {
                    strBase64 = cls.convertPageToPDFPersnal(strHeader, strBodyHtml, tempConfigModel.FOOTER, strUrlAddPdf, tempConfigModel.LIST_REPLACE, strCer,tempConfigModel.ORIEN, tempConfigModel.HEADER_HEIGHT, tempConfigModel.FOOTER_HEIGHT, tempConfigModel.WATERMARK);
                }
                //LibSign lib = new LibSign("");
                //strBase64 = Convert.ToBase64String(LibSign.resizePdf(Convert.FromBase64String(strBase64)));

                //string strCer = jValueInsuran["CONTRACT_NO"] == null ? jValueInsuran["CERTIFICATE_NO"]?.ToString() : jValueInsuran["CONTRACT_NO"]?.ToString();
                fileReturn.FILE_ID = strGuiID_File;
                fileReturn.FILE_BASE64 = strBase64;
                fileReturn.FILE_REF = strCer;
                fileReturn.FILE_URL = "";
                fileReturn.MESSAGE = "OK";
                fileReturn.Success = true;
                iCount++;
                return fileReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi khởi tạo pdf " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu  ", "Lỗi " + strOrgCode + " sản phẩm " + strProductCode + " # " + strTempCode + strUser + strChannel + strStructCode + ex.ToString());
                return null;
            }
        }

        public SignFileReturnModel createPDF_GCN_Preview(TempEmailModel tempConfigModel, JObject jValueInsuran, string strGuiID_File)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {
                //xử lý các thông tin mẫu replaceHtmlValueJson
                string strHeader = tempConfigModel.HEADER;
                string strUrlAddPdf = "";
                if (tempConfigModel.IS_ATTACH.ToString().Equals("0") && !string.IsNullOrEmpty(tempConfigModel.URL_PAGE_ADD))
                    strUrlAddPdf = tempConfigModel.URL_PAGE_ADD.ToString();
                string strCer = jValueInsuran["CERTIFICATE_NO"]?.ToString();
                if (string.IsNullOrEmpty(strCer))
                {
                    strCer = jValueInsuran["CONTRACT_NO"]?.ToString();
                    fileReturn.REF_TYPE = "CONTRACT";
                }
                else
                {
                    fileReturn.REF_TYPE = "CER";
                }
                List<TempFileSign> lsFileConvert = new List<TempFileSign>();
                int iCount = 1;
                TempFileSign fileTemp = new TempFileSign();
                string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlValueJson(jValueInsuran, tempConfigModel);
                string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
                //strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                goBussinessPDF cls = new goBussinessPDF();
                string strBase64 = "";
                strWatermark = tempConfigModel.WATERMARK;
                if (tempConfigModel.PAGE_SIZE != null && tempConfigModel.PAGE_SIZE.IndexOf(",") > 0)
                {
                    strBase64 = cls.convertPageToPDFPersnalA5(strHeader, strBodyHtml, tempConfigModel.FOOTER, strUrlAddPdf, tempConfigModel.LIST_REPLACE, strCer, tempConfigModel.PAGE_SIZE, tempConfigModel.HEADER_HEIGHT, tempConfigModel.FOOTER_HEIGHT, tempConfigModel.WATERMARK);
                }
                else
                {
                    strBase64 = cls.convertPageToPDFPersnal(strHeader, strBodyHtml, tempConfigModel.FOOTER, strUrlAddPdf, tempConfigModel.LIST_REPLACE, strCer, tempConfigModel.ORIEN, tempConfigModel.HEADER_HEIGHT, tempConfigModel.FOOTER_HEIGHT, tempConfigModel.WATERMARK);
                }
                //LibSign lib = new LibSign("");
                //strBase64 = Convert.ToBase64String(LibSign.resizePdf(Convert.FromBase64String(strBase64)));

                //string strCer = jValueInsuran["CONTRACT_NO"] == null ? jValueInsuran["CERTIFICATE_NO"]?.ToString() : jValueInsuran["CONTRACT_NO"]?.ToString();
                fileReturn.FILE_ID = strGuiID_File;
                fileReturn.FILE_BASE64 = strBase64;
                fileReturn.FILE_REF = strCer;
                fileReturn.FILE_URL = "";
                fileReturn.MESSAGE = "OK";
                fileReturn.Success = true;
                iCount++;
                return fileReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi khởi tạo pdf " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu  ", "Lỗi " + strOrgCode + " sản phẩm " + strProductCode + " # " + strTempCode + strUser + strChannel + strStructCode + ex.ToString());
                return null;
            }
        }

        public BaseResponse addTextToPdf(BaseRequest request)
        {
            //lấy file đã ký chèn đoạn text trước khi in cho tất cả giấy chứng nhận khi bấm qua nút print
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, request.Data.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strORG_CODE = request.Action.ParentCode;
                    string strAction = request.Action.ActionCode;
                    string strID_FILE = jInput["FILE_CODE"]?.ToString();
                    string strCerID = jInput["CER_ID"]?.ToString();
                    string strBase64 = addTextToPdf("GCN " + strCerID + " được in từ Hệ thống của Công ty Bảo hiểm HD tại thời điểm " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), strID_FILE);
                    JObject jOutput = new JObject();
                    jOutput.Add("CER_ID", strCerID);
                    if (string.IsNullOrEmpty(strBase64))
                    {
                        jOutput.Add("FILE_BASE64", "");
                        response = goBusinessCommon.Instance.getResultApi(false, jOutput, false, null, null, null, null);
                    }
                    else
                    {
                        jOutput.Add("FILE_BASE64", strBase64);
                        response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }


        public BaseResponse findCerVJ(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;
                    string strUser = jInput["USER_NAME"].ToString();//người tạo
                    string strNAME = jInput["NAME"].ToString();//"tên hành khách";
                    string strSTART_DATE = jInput["START_DATE"].ToString();//ngày khởi hành 29/11/2020 -->
                    string strRESERVE_CODE = jInput["RESERVE_CODE"].ToString();//mã đặt chỗ
                    string strFLI_NO = jInput["FLI_NO"].ToString();//số hiệu chuyến bay,
                    string strSTART_PLACE = jInput["START_PLACE"].ToString();//điểm khởi hành
                    string strDESTINATION = jInput["DESTINATION"].ToString();//điểm kết thúc
                    string strSEAT_NO = jInput["SEAT_NO"].ToString();//số ghế
                    DateTime dt = goUtility.Instance.ConvertToDateTime(strSTART_DATE, DateTime.MinValue);
                    string strPathFilePDF = strPathTemplate + "File/";
                    //lấy dữ liệu từ DB
                    BaseRequest req = request;
                    req.Action.ActionCode = strAction;//HDI_INFOR_VJ
                    req.Data = goUtility.Instance.convertArrayToJsonParam(new string[] { "1", strUser, strNAME, dt.ToString("yyyyMMdd"), strRESERVE_CODE, strFLI_NO, strSTART_PLACE, strDESTINATION, strSEAT_NO });
                    response = goBusinessAction.Instance.GetBaseResponse(req);
                    //lấy dữ file bảo hiểm đã ký số
                    JArray jarrOut = new JArray();
                    List<JObject> objcontractDetail = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    req.Action.ActionCode = goConstantsProcedure.Sign_Get_Batch;
                    foreach (JObject obj in objcontractDetail)
                    {
                        JObject jobj = new JObject();
                        jobj = obj;
                        req.Data = goUtility.Instance.convertArrayToJsonParam(new string[] { "X", obj["CERTIFICATE_NO"].ToString(), strORG_CODE });
                        response = goBusinessAction.Instance.GetBaseResponse(req);
                        List<JObject> objFileSign = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        jobj.Add("FILE_SIGNED", objFileSign[0]["FILE_SIGNED"].ToString()); //lấy key file đã ký trả về
                        jarrOut.Add(jobj);
                    }
                    response = goBusinessCommon.Instance.getResultApi(true, jarrOut, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        
        public BaseResponse findCer(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, request.Data.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strORG_CODE = request.Action.ParentCode;
                    string strAction = request.Action.ActionCode;
                    string strInsr = jInput["INS_CONTRACT"]?.ToString();
                    string strTypr_File = jInput["FILE_TYPE"].ToString();
                    string strID_FILE = jInput["FILE_CODE"]?.ToString();
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    string strType = "INS";
                    if (string.IsNullOrEmpty(strInsr))
                    {
                        strType = "ID";
                        strInsr = strID_FILE;
                    }
                    //lấy dữ liệu từ DB
                    BaseRequest req = request;
                    req.Action.ActionCode = strAction;//HDI_GET_SIGN

                    req.Data = goUtility.Instance.convertArrayToJsonParam(new string[] { "X", strInsr, strORG_CODE, strType }); //X batchID
                    response = goBusinessAction.Instance.GetBaseResponse(req);
                    List<JObject> objFileSign = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    JObject jOutput = new JObject();
                    string url = "";
                    if (objFileSign !=null && objFileSign.Any())
                    {
                        jOutput.Add("FILE_URL", goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + objFileSign[0]["FILE_SIGNED"].ToString());
                        jOutput.Add("FILE_REFF", strInsr);
                        jOutput.Add("INS_CONTRACT", objFileSign[0]["FILE_CODE"].ToString());
                        strInsr = objFileSign[0]["FILE_CODE"].ToString();
                        if (strTypr_File.Equals("BASE64"))
                            jOutput.Add("FILE_BASE64", Convert.ToBase64String(goBusinessCommon.Instance.getFileServer(objFileSign[0]["FILE_SIGNED"].ToString())));
                        else
                            jOutput.Add("FILE_BASE64", "");
                        url = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + objFileSign[0]["FILE_SIGNED"].ToString();
                    }
                    else
                    {
                        //kích hoạt hàm ký số 
                        //TempFileSign temp = goBusinessServices.Instance.fileCerBackDate("BAY_AT", strInsr,"ALL");
                        //if (temp != null && temp.signed)
                        //{
                        //    jOutput.Add("FILE_URL", temp.FILE_URL);
                        //    jOutput.Add("FILE_REFF", strInsr);
                        //    jOutput.Add("INS_CONTRACT", temp.CERTIFICATE_NO);
                        //    strInsr = temp.CERTIFICATE_NO;
                        //    if (strTypr_File.Equals("BASE64"))
                        //        jOutput.Add("FILE_BASE64", Convert.ToBase64String(goBusinessCommon.Instance.getFileServer(temp.ID_FILE)));
                        //    else
                        //        jOutput.Add("FILE_BASE64", "");
                        //    url = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + temp.ID_FILE;
                        //}
                    }
                   
                    
                    jOutput.Add("QR_BASE64", getQrCode(url, "Q", 8, ""));
                    //LẤY THÔNG TIN ĐƠN BẢO HIỂM
                    JObject jobjParam = new JObject();
                    jobjParam.Add("A1", "");
                    jobjParam.Add("A2", strInsr);
                    jobjParam.Add("A3", "");
                    jobjParam.Add("A4", "");
                    jobjParam.Add("A5", "");
                    jobjParam.Add("A6", "");
                    jobjParam.Add("A7", "");
                    jobjParam.Add("A8", "");
                    jobjParam.Add("A9", "");
                    jobjParam.Add("A10", "");
                    jobjParam.Add("A11", "");
                    jobjParam.Add("A12", "");
                    jobjParam.Add("A13", "");
                    jobjParam.Add("A14", "");
                    jobjParam.Add("A15", "");
                    object[] lsPr = new object[] { "OPENAPI", "HDI_PRIVATE", "HDI_PRIVATE", "DETAIL", jobjParam };
                    BaseResponse responseFind = callPackage(goConstantsProcedure.Insu_get_infor, strEvm, lsPr);
                    List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                    if (objcontract == null || objcontract.Count == 0)
                    {
                        jOutput.Add("DETAIL", new JObject());
                    }
                    else
                    {
                        jOutput.Add("DETAIL", objcontract[0]);
                    }
                    JArray arr = new JArray();
                    List<JObject> lsFile = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);

                    if (lsFile == null || lsFile.Count == 0)
                    {
                        jOutput.Add("FILE", arr);
                    }
                    else
                    {
                        foreach (JObject obj in lsFile)
                        {
                            arr.Add(obj);
                        }
                        jOutput.Add("FILE", arr);
                    }

                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public List<TempFileSign> createAllPdf(List<JObject> lsTemplate, List<JObject> lsValueContract, string strProductCode, string strOrgCode)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strHeader = "";
                string strTempHeader = strPathTemplate + lsTemplate[0]["HEADER"].ToString();
                if (!string.IsNullOrEmpty(lsTemplate[0]["HEADER"].ToString()))
                {
                    if (lsTemplate[0]["HEADER"].ToString().Length < 300)
                        strHeader = File.ReadAllText(strTempHeader);
                    else
                        strHeader = lsTemplate[0]["HEADER"].ToString();
                }
                string strTempPooter = strPathTemplate + lsTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                string strFooter = "";
                if (!string.IsNullOrEmpty(lsTemplate[0]["FOOTER"].ToString()))
                {
                    if (lsTemplate[0]["FOOTER"].ToString().Length < 300)
                        strFooter = File.ReadAllText(strTempPooter);
                    else
                        strFooter = lsTemplate[0]["FOOTER"].ToString();
                }
                string strUrlAddPdf = "";
                if ((lsTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!lsTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                    strUrlAddPdf = lsTemplate[0]["URL_PAGE_ADD"].ToString();
                List<TempFileSign> lsFileConvert = new List<TempFileSign>();
                int iCount = 1;
                foreach (JObject jobValue in lsValueContract)
                {
                    TempFileSign fileTemp = new TempFileSign();
                        string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlBody(jobValue, lsTemplate);                        
                        if (!string.IsNullOrEmpty(jobValue["QR_CODE"]?.ToString()))
                        {
                            string strBase64QR = goBussinessPDF.Instance.getQrCode(jobValue["QR_CODE"].ToString(), "Q", 8, "");
                            strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
                        }
                        string strGuiID_File = Guid.NewGuid().ToString("N");
                        string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
                        strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                        strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                        goBussinessPDF cls = new goBussinessPDF();
                        string strBase64 = cls.convertPageToPDFPersnal(strHeader, strBodyHtml, strFooter, strUrlAddPdf, "", "", lsTemplate[0]["ORIEN"].ToString(), lsTemplate[0]["HEADER_HEIGHT"].ToString(), lsTemplate[0]["FOOTER_HEIGHT"].ToString(), lsTemplate[0]["WATERMARK"].ToString());
                    string strCer = jobValue["CONTRACT_NO"] == null ? jobValue["CERTIFICATE_NO"]?.ToString() : jobValue["CONTRACT_NO"]?.ToString();
                    fileTemp.CERTIFICATE_NO = strCer;
                    fileTemp.FILE_BASE64 = strBase64;
                    fileTemp.FILE_URL = strQrHeader;
                    fileTemp.ID_FILE = strGuiID_File;
                    fileTemp.PRODUCT_CODE = strProductCode;
                    fileTemp.STT = iCount.ToString();
                    fileTemp.STRUCT_CODE = jobValue["STRUCT_CODE"]?.ToString();
                    //fileTemp.dtSignBack = 
                    lsFileConvert.Add(fileTemp);
                    iCount++;
                }
                return lsFileConvert;
                //response = goBusinessCommon.Instance.getResultApi(true, jarrOut, false, null, null, null, null);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public List<TempFileSign> createPdfBackDate(List<JObject> lsTemplate, JObject jobValue, string strProductCode, string strOrgCode, string strGuiID_File)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strHeader = "";
                string strTempHeader = strPathTemplate + lsTemplate[0]["HEADER"].ToString();
                if (!string.IsNullOrEmpty(lsTemplate[0]["HEADER"].ToString()))
                {
                    if (lsTemplate[0]["HEADER"].ToString().Length < 300)
                        strHeader = File.ReadAllText(strTempHeader);
                    else
                        strHeader = lsTemplate[0]["HEADER"].ToString();
                }
                string strTempPooter = strPathTemplate + lsTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                string strFooter = "";
                if (!string.IsNullOrEmpty(lsTemplate[0]["FOOTER"].ToString()))
                {
                    if (lsTemplate[0]["FOOTER"].ToString().Length < 300)
                        strFooter = File.ReadAllText(strTempPooter);
                    else
                        strFooter = lsTemplate[0]["FOOTER"].ToString();
                }
                string strUrlAddPdf = "";
                if ((lsTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!lsTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                    strUrlAddPdf = lsTemplate[0]["URL_PAGE_ADD"].ToString();
                List<TempFileSign> lsFileConvert = new List<TempFileSign>();
                int iCount = 1;
                TempFileSign fileTemp = new TempFileSign();
                    string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlBody(jobValue, lsTemplate);
                    if (!string.IsNullOrEmpty(jobValue["QR_CODE"]?.ToString()))
                    {
                        string strBase64QR = goBussinessPDF.Instance.getQrCode(jobValue["QR_CODE"].ToString(), "Q", 8, "");
                        strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
                    }
                    
                    string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
                    strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                    strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                    goBussinessPDF cls = new goBussinessPDF();
                    string strBase64 = cls.convertPageToPDFPersnal_batch(strHeader, strBodyHtml, strFooter, strUrlAddPdf);
                    string strCer = jobValue["CONTRACT_NO"] == null ? jobValue["CERTIFICATE_NO"]?.ToString() : jobValue["CONTRACT_NO"]?.ToString();
                    fileTemp.CERTIFICATE_NO = strCer;
                    fileTemp.FILE_BASE64 = strBase64;
                    fileTemp.FILE_URL = strQrHeader;
                    fileTemp.ID_FILE = strGuiID_File;
                    fileTemp.PRODUCT_CODE = strProductCode;
                    fileTemp.STT = iCount.ToString();
                    fileTemp.STRUCT_CODE = jobValue["STRUCT_CODE"]?.ToString();
                    //fileTemp.dtSignBack = 
                    lsFileConvert.Add(fileTemp);
                    iCount++;
                
                return lsFileConvert;
                //response = goBusinessCommon.Instance.getResultApi(true, jarrOut, false, null, null, null, null);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public BaseResponse getPDFJson(BaseRequest request)
        {
            //CONVERT GIẤY YÊU CẦU AN TÂM TỪ JSON
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    //JObject jInput = JObject.Parse(request.Data.ToString());
                    //p_ORG_CODE ,p_TEMP_TYPE, p_TEMP_CODE,
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    //string strTemp_type = "PDF";
                    string strPathFilePDF = strPathTemplate + "File/" + DateTime.Now.ToString("yyMMdd") + "/";
                    if (Directory.Exists(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd")))
                    {
                        Directory.Delete(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd"));
                    }
                    if (!Directory.Exists(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd")))
                    {
                        Directory.CreateDirectory(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd"));
                    }

                    string strFilePDF = goEncryptBase.Instance.Md5Encode(request.Data.ToString());
                    List<List<JObject>> objcontract = new List<List<JObject>>();
                    objcontract.Add(new List<JObject>() { JObject.Parse(jInput["MASTER"]?.ToString()) });
                    //JArray jarr = JArray.Parse(jInput["DETAIL"]?.ToString());
                    //List<JObject> lsJobj = new List<JObject>();
                    //foreach(JToken jtk in jarr)
                    //{
                    //    lsJobj.Add((JObject)jtk);
                    //}
                    //objcontract.Add(lsJobj);
                    if (objcontract.Count == 0)
                    {
                        return response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3006), goConstantsError.Instance.ERROR_3006);
                    }
                    if (string.IsNullOrEmpty(jInput["PACK_CODE"]?.ToString()))
                        jInput["PACK_CODE"] = "null";
                    object[] param = new object[] { strORG_CODE, "PDF", jInput["TEMP_CODE"]?.ToString(), jInput["PRODUCT_CODE"]?.ToString(), jInput["PACK_CODE"]?.ToString() };
                    //response = callPackage(strAction, "LIVE", param);
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strAction, strEvm, param);//GET_TEMPLATE
                    BaseResponse responseTemplate = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, param), false, null, null, null, null);
                    List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
                    if (arrTemplate == null || arrTemplate.Count == 0)
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không tìm thấy cấu hình mã mẫu " + jInput["PACK_CODE"]?.ToString() + strORG_CODE);
                    }
                    string strBodyHtml = goBusinessTemplate.Instance.fillValueToHtmlGYC(objcontract, responseTemplate);
                    string strTempHeader = strPathTemplate + arrTemplate[0]["HEADER"].ToString();// "HDI-GCNBH_Header.html";
                    string strHeader = "";
                    if (!string.IsNullOrEmpty(arrTemplate[0]["HEADER"].ToString()))
                    {
                        if (arrTemplate[0]["HEADER"].ToString().Length < 300)
                            strHeader = File.ReadAllText(strTempHeader);
                        else
                            strHeader = arrTemplate[0]["HEADER"].ToString();
                    }
                    string strTempPooter = strPathTemplate + arrTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                    string strFooter = "";
                    if (!string.IsNullOrEmpty(arrTemplate[0]["FOOTER"].ToString()))
                    {
                        if (arrTemplate[0]["FOOTER"].ToString().Length < 300)
                            strFooter = File.ReadAllText(strTempPooter);
                        else
                            strFooter = arrTemplate[0]["FOOTER"].ToString();
                    }
                    string strUrlAddPdf = "";
                    if ((arrTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!arrTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                        strUrlAddPdf = arrTemplate[0]["URL_PAGE_ADD"].ToString();
                    //string strQR = objcontract[0]["QR_CODE"]?.ToString();
                    //if (!string.IsNullOrEmpty(objcontract[0]["QR_CODE"]?.ToString()))
                    //{
                    //    string strBase64QR = getQrCode(objcontract[0]["QR_CODE"].ToString(), "Q", 8, "");
                    //    //strHeader = strHeader.Replace("@QR_CODE@",strBase64QR);
                    //    strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
                    //    //strFooter = strFooter.Replace("@QR_CODE@", strBase64QR);
                    //}
                    //string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID;
                    //strHeader = strHeader.Replace("@QR_CODE@", getQrCode(strQrHeader, "Q", 8, ""));
                    strBodyHtml = clearParram(strBodyHtml);
                    string strBase64 = convertPageToPDFPersnal(strHeader, strBodyHtml, strFooter, strUrlAddPdf, "", "", "", arrTemplate[0]["HEADER_HEIGHT"].ToString(), arrTemplate[0]["FOOTER_HEIGHT"].ToString(), arrTemplate[0]["WATERMARK"].ToString());

                    File.WriteAllBytes(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd") + "/" + strFilePDF + ".pdf", Convert.FromBase64String(strBase64));

                    JObject jOutput = new JObject();
                    jOutput.Add("FILE_NAME", strFilePDF + ".pdf");
                    //jOutput.Add("FILE_URL", strUrl);
                    jOutput.Add("FILE_BASE64", strBase64);
                    //jOutput.Add("FILE_CODE", strGuiID);
                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse convertPDF(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    //JObject jInput = JObject.Parse(request.Data.ToString());
                    //p_ORG_CODE ,p_TEMP_TYPE, p_TEMP_CODE,
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;
                    TemplaceModel tempParam = new TemplaceModel();
                    tempParam = JsonConvert.DeserializeObject<TemplaceModel>(request.Data.ToString());
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    //string strTemp_type = "PDF";
                    string strPathFilePDF = strPathTemplate + "File/" + DateTime.Now.ToString("yyMMdd") + "/";
                    if (Directory.Exists(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd")))
                    {
                        Directory.Delete(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd"));
                    }
                    if (!Directory.Exists(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd")))
                    {
                        Directory.CreateDirectory(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd"));
                    }
                    string strGuiID = Guid.NewGuid().ToString("N");
                    string strMD5 = goEncryptBase.Instance.Md5Encode(request.Data.ToString());
                    string strFilePDF = strMD5 + "_" + strGuiID;
                    DirectoryInfo d = new DirectoryInfo(strPathFilePDF);
                    FileInfo[] Files = d.GetFiles("*.pdf");
                    foreach (FileInfo file in Files)
                    {
                        if (file.Name.StartsWith(strMD5))
                        {
                            byte[] bf = File.ReadAllBytes(file.FullName);
                            JObject jOutput1 = new JObject();
                            jOutput1.Add("FILE_NAME", strFilePDF + ".pdf");
                            jOutput1.Add("FILE_BASE64", Convert.ToBase64String(bf));
                            jOutput1.Add("FILE_CODE", file.Name.Substring(33, 32));
                            return goBusinessCommon.Instance.getResultApi(true, jOutput1, false, null, null, null, null);
                        }
                    }
                    //Lấy dữ liệu của giấy chứng nhận
                    object[] paramFind = new object[] { "OPENAPI", tempParam.USER_NAME, tempParam.PARAM_VALUE, tempParam.PRODUCT_CODE, tempParam.PARAM_CODE, tempParam.TEMP_CODE, strORG_CODE };
                    BaseResponse responseFind = callPackage(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                    List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                    if (objcontract.Count == 0)
                    {
                        return response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3006), goConstantsError.Instance.ERROR_3006);
                    }
                    if (string.IsNullOrEmpty(tempParam.PACK_CODE))
                        tempParam.PACK_CODE = "null";
                    object[] param = new object[] { strORG_CODE, "PDF", tempParam.TEMP_CODE, tempParam.PRODUCT_CODE, tempParam.PACK_CODE };
                    //response = callPackage(strAction, "LIVE", param);
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strAction, strEvm, param);//GET_TEMPLATE
                    BaseResponse responseTemplate = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, param), false, null, null, null, null);
                    List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
                    if (arrTemplate == null || arrTemplate.Count == 0)
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không tìm thấy cấu hình mã mẫu " + tempParam.PACK_CODE + strORG_CODE);
                    }
                    string strBodyHtml = goBusinessTemplate.Instance.fillValueToHtmlBody(responseFind, responseTemplate);
                    string strTempHeader = strPathTemplate + arrTemplate[0]["HEADER"].ToString();// "HDI-GCNBH_Header.html";
                    string strHeader = "";
                    if (!string.IsNullOrEmpty(arrTemplate[0]["HEADER"].ToString()))
                    {
                        if (arrTemplate[0]["HEADER"].ToString().Length < 300)
                            strHeader = File.ReadAllText(strTempHeader);
                        else
                            strHeader = arrTemplate[0]["HEADER"].ToString();
                    }
                    string strTempPooter = strPathTemplate + arrTemplate[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                    string strFooter = "";
                    if (!string.IsNullOrEmpty(arrTemplate[0]["FOOTER"].ToString()))
                    {
                        if (arrTemplate[0]["FOOTER"].ToString().Length < 300)
                            strFooter = File.ReadAllText(strTempPooter);
                        else
                            strFooter = arrTemplate[0]["FOOTER"].ToString();
                    }
                    string strUrlAddPdf = "";
                    if ((arrTemplate[0]["IS_ATTACH"].ToString().Equals("0")) && (!arrTemplate[0]["URL_PAGE_ADD"].ToString().Equals("")))
                        strUrlAddPdf = arrTemplate[0]["URL_PAGE_ADD"].ToString();
                    //string strQR = objcontract[0]["QR_CODE"]?.ToString();
                    if (!string.IsNullOrEmpty(objcontract[0]["QR_CODE"]?.ToString()))
                    {
                        string strBase64QR = getQrCode(objcontract[0]["QR_CODE"].ToString(), "Q", 8, "");
                        //strHeader = strHeader.Replace("@QR_CODE@",strBase64QR);
                        strBodyHtml = strBodyHtml.Replace("@QR_CODE@", strBase64QR);
                        //strFooter = strFooter.Replace("@QR_CODE@", strBase64QR);
                    }
                    string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID;
                    strHeader = strHeader.Replace("@QR_CODE@", getQrCode(strQrHeader, "Q", 8, ""));
                    strBodyHtml = clearParram(strBodyHtml);
                    string strBase64 = convertPageToPDFPersnal(strHeader, strBodyHtml, strFooter, strUrlAddPdf,"","", "", arrTemplate[0]["HEADER_HEIGHT"].ToString(), arrTemplate[0]["FOOTER_HEIGHT"].ToString(), arrTemplate[0]["WATERMARK"].ToString());

                    //File.WriteAllBytes(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd") + "/" + strFilePDF + ".pdf", Convert.FromBase64String(strBase64));
                    //string strNameFile = "";
                    //string strUrl = "";
                    //bool b = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strBase64), strFilePDF + ".pdf",
                    //    "ViewPDF/" + strORG_CODE + "/" + DateTime.Now.ToString("yyyyMMdd"), request, ref strNameFile, ref strUrl);
                    //if (!b)
                    //{

                    //    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "day file convert len " + strUrl + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                    //}
                    JObject jOutput = new JObject();
                    jOutput.Add("FILE_NAME", strFilePDF + ".pdf");
                    //jOutput.Add("FILE_URL", strUrl);
                    jOutput.Add("FILE_BASE64", strBase64);
                    jOutput.Add("FILE_CODE", strGuiID);
                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse viewCertificate_Json(BaseRequest request)
        {
            //DÀNH CHO ATTD VIEW
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    //p_ORG_CODE ,p_TEMP_TYPE, p_TEMP_CODE,
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;
                    string strProductCode = jInput["PRODUCT_CODE"].ToString(); //PRODUCT_CODE
                    string strTempCode = jInput["TEMP_CODE"].ToString();//"ATTD_VIEW";--> 
                    string strPackCode = jInput["PACK_CODE"]?.ToString();//giá trị tra cứu
                    string strParamCode = jInput["JSON_DATA"].ToString();//mã sử dụng tra cứu ID-, NO, CODE
                    string strTypeData = jInput["DATA_TYPE"]?.ToString();//dữ liệu sử dụng convert DATA,
                    string strCertificate = jInput["CERTIFICATE_NO"]?.ToString();
                    response = goBusinessServices.Instance.viewCertificateJson(strORG_CODE, strProductCode, strPackCode, JObject.Parse(strParamCode));
                    if (response == null)
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse viewCertificate(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    //p_ORG_CODE ,p_TEMP_TYPE, p_TEMP_CODE,
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;

                    string strTemp_type = "PDF";
                    string strUser = jInput["USER_NAME"].ToString();//người tạo
                    string strTemp_code = jInput["TEMP_CODE"].ToString();//"ATTD_VIEW";--> PRODUCT_CODE
                    string strParamValue = jInput["PARAM_VALUE"].ToString();//giá trị tra cứu
                    string strParamCode = jInput["PARAM_CODE"].ToString();//mã sử dụng tra cứu ID-, NO, CODE
                    string strTypeData = jInput["DATA_TYPE"].ToString();//dữ liệu sử dụng convert DATA,
                    string strPathFilePDF = strPathTemplate + "File/" + DateTime.Now.ToString("yyMMdd") + "/";
                    if (Directory.Exists(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd")))
                    {
                        Directory.Delete(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd"));
                    }
                    string strFilePDF = strTemp_code + goEncryptBase.Instance.Md5Encode(jInput.ToString());                    
                    JObject jOutput = new JObject();
                    string strJsonConvert = request.Data.ToString();
                    JObject jobjGetData = new JObject();
                    JArray jarrBody = new JArray();
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    if (strTypeData.Equals("DATA"))
                    {
                        jarrBody = createJsonConvertTemp(strUser, "HDI", strParamCode, strParamValue, strTemp_code, strTemp_code + "_VIEW", strORG_CODE);                             
                        if (jarrBody == null || jarrBody.Count == 0)
                        {
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không lấy được thông tin " + strParamValue);
                        }
                        //lấy dữ dữ liệu để tạo jarrheader json
                        JArray jarrHeader = new JArray();
                        JObject jboAdd = new JObject();
                        jboAdd.Add("key", "QR_CODE");
                        if (strTemp_code.Equals("ATTD_TP"))
                            jboAdd.Add("value", "QR_CODE_VALUE");
                        else
                            jboAdd.Add("value", getQrCode(strParamValue, "Q", 8, ""));
                        jboAdd.Add("type", "string");
                        jarrHeader.Add(jboAdd);
                        JArray jarrFooter = new JArray();
                        jobjGetData.Add("HEADER", jarrHeader);
                        jobjGetData.Add("BODY", jarrBody);
                        jobjGetData.Add("FOOTER", jarrFooter);
                        strJsonConvert = jobjGetData.ToString();
                    }
                    //LẤY FILE TEMPLATE TRONG DATADASE
                    object[] param = new object[] { strORG_CODE, strTemp_type, strTemp_code+ "_VIEW", strTemp_code ,"null"};
                    //response = callPackage(strAction, "LIVE", param);
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strAction, "LIVE", param);
                    response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, param), false, null, null, null, null);
                    List<JObject> objHDBQR = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (objHDBQR == null || objHDBQR[0].Count == 0)
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không tìm thấy cấu hình mã mẫu " + strTemp_code);
                    }
                    //lấy được hết các thông tin về temp rồi thì đọc data để conver
                    string strTempHeader = strPathTemplate + objHDBQR[0]["HEADER"].ToString();// "HDI-GCNBH_Header.html";
                    string strHeader = "";
                    if (!string.IsNullOrEmpty(objHDBQR[0]["HEADER"].ToString()))
                    {
                        strHeader = File.ReadAllText(strTempHeader);
                    }
                    string strTempBody = strPathTemplate;
                    string strBody = "";
                    if (objHDBQR[0]["DATA_TYPE"].ToString().Equals("PATH"))
                    {
                        strTempBody = strPathTemplate + objHDBQR[0]["DATA"].ToString();//"HDI-GCNBH.html";
                        strBody = File.ReadAllText(strTempBody);
                    }
                    else
                    {
                        strBody = objHDBQR[0]["DATA"].ToString();
                    }
                    string strTempPooter = strPathTemplate + objHDBQR[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                    string strFooter = "";
                    if (!string.IsNullOrEmpty(objHDBQR[0]["FOOTER"].ToString()))
                    {
                        strFooter = File.ReadAllText(strTempPooter);
                    }
                    List<string> lsReplace = objHDBQR[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    if (strTemp_code.Equals("ATTD_TP") || strJsonConvert.IndexOf("QR_CODE_VALUE") > 100)
                    {
                        object[] paramQR = new object[] { jInput["TRANSACTION_ID"].ToString() };
                        response = callPackage(goConstantsProcedure.Pay_get_transaction, "LIVE", paramQR);
                        List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        string strQRImage = getQrCode(objTRANS[0]["QR_DATA"].ToString(), "H", 8, "");
                        strJsonConvert = strJsonConvert.Replace("QR_CODE_VALUE", strQRImage);
                    }
                    else
                    {
                        //strJsonConvert = strJsonConvert.Replace("QR_CODE_VALUE", );
                    }
                    string strUrlAddPdf = "";
                    if ((objHDBQR[0]["IS_ATTACH"].ToString().Equals("0")) && (!objHDBQR[0]["URL_PAGE_ADD"].ToString().Equals("")))
                        strUrlAddPdf = objHDBQR[0]["URL_PAGE_ADD"]?.ToString();
                    string strBase64 = convertPageToPDFNew(strJsonConvert, strHeader, strBody, strFooter, lsReplace, strUrlAddPdf);
                    if (!Directory.Exists(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd")))
                    {
                        Directory.CreateDirectory(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd"));
                    }
                    File.WriteAllBytes(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd") + "/" + strFilePDF + ".pdf", Convert.FromBase64String(strBase64));
                    
                    jOutput.Add("FILE_NAME", strFilePDF + ".pdf");
                    jOutput.Add("FILE_BASE64", strBase64);
                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse GetBaseResponse(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    //p_ORG_CODE ,p_TEMP_TYPE, p_TEMP_CODE,
                    string strORG_CODE = request.Action.ParentCode;// "CENTECH";
                    string strAction = request.Action.ActionCode;

                    string strTemp_type = "PDF";
                    string strUser = jInput["USER_NAME"].ToString();//người tạo
                    string strTemp_code = jInput["TEMP_CODE"].ToString();//"GCN,GYCBH,";
                    string strParamValue = jInput["PARAM_VALUE"].ToString();//giá trị tra cứu
                    string strParamCode = jInput["PARAM_CODE"].ToString();//mã sử dụng tra cứu ID-, NO, TRACE_CODE
                    string strTypeData = jInput["DATA_TYPE"].ToString();//dữ liệu sử dụng convert DATA,JSON,
                    string strPathFilePDF = strPathTemplate + "File/" + DateTime.Now.ToString("yyMMdd") + "/";
                    if (Directory.Exists(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd")))
                    {
                        Directory.Delete(strPathTemplate + "File/" + (DateTime.Now.Day - 1).ToString("yyMMdd"));
                    }
                    string strFilePDF = strTemp_code + goEncryptBase.Instance.Md5Encode(jInput.ToString());
                    //if(File.Exists(strPathFilePDF + strFilePDF + ".pdf"))
                    //{                        
                    //    byte[] bf = File.ReadAllBytes(strPathFilePDF + strFilePDF + ".pdf");
                    //    JObject jOutput1 = new JObject();
                    //    jOutput1.Add("FILE_NAME", strFilePDF + ".pdf");
                    //    jOutput1.Add("FILE_BASE64", Convert.ToBase64String(bf));
                    //    jOutput1.Add("FILE_BASE64", Convert.ToBase64String(bf));
                    //    return goBusinessCommon.Instance.getResultApi(true, jOutput1, false, null, null, null, null);
                    //}
                    JObject jOutput = new JObject();
                    string strJsonConvert = request.Data.ToString();
                    JObject jobjGetData = new JObject();
                    JArray jarrBody = new JArray();
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    if (strTypeData.Equals("DATA"))
                    {
                        //tạo body json từ dữ liệu được lấy
                        switch (strTemp_code)
                        {
                            case "ATTD_GCN":
                                string strNo = "";
                                if (!strParamCode.Equals("NO"))
                                {
                                    object[] paramFind = new object[] { "1", strUser, strParamValue, "ATTD", strParamCode, "GCN", "CEN" };
                                    BaseResponse responseFind = callPackage(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                                    List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                                    if (objcontract.Count > 0 && objcontract[0]["STATUS"].ToString().Equals("APPLY"))
                                    {
                                        strNo = objcontract[0]["CERTIFICATE_NO"].ToString();
                                    }
                                }
                                else
                                {
                                    strNo = strParamValue;
                                }
                                object[] paramFind1 = new object[] { "X", strNo, strORG_CODE }; //X batchID
                                BaseResponse responseFind1 = callPackage("HDI_GET_SIGN", "LIVE", paramFind1);
                                List<JObject> objcontract1 = goBusinessCommon.Instance.GetData<JObject>(responseFind1, 0);
                                if (objcontract1.Count > 0)
                                {
                                    jOutput.Add("FILE_NAME", objcontract1[0]["FILE_SIGNED"].ToString());
                                    jOutput.Add("FILE_URL", goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + objcontract1[0]["FILE_SIGNED"].ToString());
                                    jOutput.Add("FILE_BASE64", Convert.ToBase64String(goBusinessCommon.Instance.getFileServer(objcontract1[0]["FILE_SIGNED"].ToString())));
                                    return goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                                }
                                jarrBody = createJsonConvertTemp(strUser, "11", strParamCode, strParamValue, "ATTD", "GCN", "CEN");
                                break;
                            case "VNAT_HUY":
                            case "VNAT":
                                jarrBody = createJsonConvertTemp(strUser, "11", strParamCode, strParamValue, "VNAT", "GCN", "CEN");
                                break;
                            case "ATTD_YCBH":
                            case "SK_GYC":
                                jarrBody = createJsonConvertTemp(strUser, "11", strParamCode, strParamValue, "ATTD", "GYCBH", "CEN");
                                break;
                            case "ATTD_TP":
                                jarrBody = createJsonConvertTemp(strUser, "11", strParamCode, strParamValue, "ATTD", "GYCBH", "CEN");
                                break;
                            case "CSSK_D":
                            case "CSSK_B":
                            case "CSSK_V":
                            case "CSSK_TT":
                            case "CSSK_KC":
                                jarrBody = createJsonConvertTemp(strUser, "11", strParamCode, strParamValue, "CSSK", "GCN", "CEN");
                                break;
                        }

                        if (jarrBody == null || jarrBody.Count == 0)
                        {
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không lấy được thông tin " + strParamValue);
                        }
                        //lấy dữ dữ liệu để tạo jarrheader json
                        JArray jarrHeader = new JArray();
                        JObject jboAdd = new JObject();
                        jboAdd.Add("key", "QR_CODE");
                        if (strTemp_code.Equals("ATTD_TP"))
                            jboAdd.Add("value", "QR_CODE_VALUE");
                        else
                            jboAdd.Add("value", getQrCode(strParamValue, "Q", 8, ""));
                        jboAdd.Add("type", "string");
                        jarrHeader.Add(jboAdd);
                        JArray jarrFooter = new JArray();
                        jobjGetData.Add("HEADER", jarrHeader);
                        jobjGetData.Add("BODY", jarrBody);
                        jobjGetData.Add("FOOTER", jarrFooter);
                        strJsonConvert = jobjGetData.ToString();
                    }
                    //LẤY FILE TEMPLATE TRONG DATADASE
                    object[] param = new object[] { strORG_CODE, strTemp_type, strTemp_code };
                    //response = callPackage(strAction, "LIVE", param);
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strAction, "LIVE", param);
                    response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, param), false, null, null, null, null);
                    List<JObject> objHDBQR = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (objHDBQR == null || objHDBQR[0].Count == 0)
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3004), "Không tìm thấy cấu hình mã mẫu " + strTemp_code);
                    }
                    //lấy được hết các thông tin về temp rồi thì đọc data để conver
                    string strTempHeader = strPathTemplate + objHDBQR[0]["HEADER"].ToString();// "HDI-GCNBH_Header.html";
                    string strHeader = "";
                    if (!string.IsNullOrEmpty(objHDBQR[0]["HEADER"].ToString()))
                    {
                        strHeader = File.ReadAllText(strTempHeader);
                    }
                    string strTempBody = strPathTemplate;
                    string strBody = "";
                    if (objHDBQR[0]["DATA_TYPE"].ToString().Equals("PATH"))
                    {
                        strTempBody = strPathTemplate + objHDBQR[0]["DATA"].ToString();//"HDI-GCNBH.html";
                        strBody = File.ReadAllText(strTempBody);
                    }
                    else
                    {
                        strBody = objHDBQR[0]["DATA"].ToString();
                    }
                    string strTempPooter = strPathTemplate + objHDBQR[0]["FOOTER"].ToString();//"HDI-GCNBH_Footer.html";  
                    string strFooter = "";
                    if (!string.IsNullOrEmpty(objHDBQR[0]["FOOTER"].ToString()))
                    {
                        strFooter = File.ReadAllText(strTempPooter);
                    }
                    List<string> lsReplace = objHDBQR[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    if (strTemp_code.Equals("ATTD_TP") || strJsonConvert.IndexOf("QR_CODE_VALUE") > 100)
                    {
                        object[] paramQR = new object[] { jInput["TRANSACTION_ID"].ToString() };
                        response = callPackage(goConstantsProcedure.Pay_get_transaction, "LIVE", paramQR);
                        List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        string strQRImage = getQrCode(objTRANS[0]["QR_DATA"].ToString(), "H", 8, "");
                        strJsonConvert = strJsonConvert.Replace("QR_CODE_VALUE", strQRImage);
                    }
                    else
                    {
                        //strJsonConvert = strJsonConvert.Replace("QR_CODE_VALUE", );
                    }
                    string strUrlAddPdf = "";
                    if ((objHDBQR[0]["IS_ATTACH"].ToString().Equals("0")) && (!objHDBQR[0]["URL_PAGE_ADD"].ToString().Equals("")))
                        strUrlAddPdf = objHDBQR[0]["URL_PAGE_ADD"].ToString();
                    string strBase64 = convertPageToPDFNew(strJsonConvert, strHeader, strBody, strFooter, lsReplace, strUrlAddPdf);
                    if (!Directory.Exists(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd")))
                    {
                        Directory.CreateDirectory(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd"));
                    }
                    File.WriteAllBytes(strPathTemplate + "File/" + (DateTime.Now).ToString("yyMMdd") + "/" + strFilePDF + ".pdf", Convert.FromBase64String(strBase64));
                    string strNameFile = "";
                    string strUrl = "";
                    request.Action.ParentCode = "HDI_UPLOAD";
                    request.Action.Secret = "HDI_UPLOAD_198282911FASE1239212";
                    request.Action.ActionCode = "UPLOAD_SIGN";
                    bool b = goBusinessCommon.Instance.uploadFileServer(Convert.FromBase64String(strBase64), strFilePDF + ".pdf",
                        "ViewPDF/" + strORG_CODE + "/" + DateTime.Now.ToString("yyyyMMdd"), request, ref strNameFile, ref strUrl, "");
                    if (!b)
                    {

                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "day file convert len " + strUrl + strNameFile, Logger.ConcatName(nameClass, nameMethod));
                    }

                    jOutput.Add("FILE_NAME", strNameFile);
                    jOutput.Add("FILE_URL", strUrl);
                    jOutput.Add("FILE_BASE64", strBase64);
                    response = goBusinessCommon.Instance.getResultApi(true, jOutput, false, null, null, null, null);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public JArray createJsonConvertTemp(string strProject, string strUser, string strParamCode, string strParamValue,
            string strInsuType, string strInsuCode, string strOrgCode)
        {
            JArray jOutput = new JArray();
            try
            {
                //Xử lý lấy dữ liệu từ order chuyển sang chuỗi JSON
                //gọi hàm anh Khánh lấy trans
                object[] paramFind = new object[] { strProject, strUser, strParamValue, strInsuType, strParamCode, strInsuCode, strOrgCode };
                BaseResponse responseFind = callPackage(goConstantsProcedure.Pay_get_contract, "LIVE", paramFind);
                List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                List<JObject> objcontractDetail = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);
                if (objcontract.Count == 0 || objcontract[0].Count == 0)
                {
                    return jOutput;
                }
                JArray jArr1 = createJsonKeyValue(objcontract[0]);

                JArray jArr2 = createJsonKeyValue(objcontractDetail);
                JObject jboAdd = new JObject();
                jboAdd.Add("key", "COVERAGE");
                jboAdd.Add("value", jArr2);
                jboAdd.Add("type", "list");
                jArr1.Add(jboAdd);
                return jArr1;

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
            }
            return jOutput;
        }
        public JArray createJsonKeyValue(JObject jobjectIn)
        {
            JArray jOutput = new JArray();
            try
            {
                var my_slist = jobjectIn.GetEnumerator();
                while (my_slist.MoveNext())
                {
                    //JTokenType 
                    var _jToken = my_slist.Current;
                    JObject jo = new JObject();
                    jo.Add("key", _jToken.Key.ToString());
                    jo.Add("value", _jToken.Value.ToString().Trim());
                    jo.Add("type", "string");
                    jOutput.Add(jo);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
            }
            return jOutput;
        }
        public JArray createJsonKeyValue(List<JObject> jobjectIn)
        {
            JArray jOutput = new JArray();
            try
            {
                foreach (JObject jo in jobjectIn)
                {
                    JArray jArrHeader = new JArray();
                    jArrHeader = createJsonKeyValue(jo);
                    jOutput.Add(jArrHeader);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
            }
            return jOutput;
        }
        public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        {
            ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
            return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
        }
        public string getQrCode(string strQrData, string strErrorCorrect, int iSize,
            string strPathLogo)
        {
            try
            {
                QRCodeEncoder encoder = new QRCodeEncoder();
                encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                switch (strErrorCorrect)
                {
                    case "L":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                        break;
                    case "M":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                        break;
                    case "Q":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                        break;
                    case "H":
                        encoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                        break;
                }
                encoder.QRCodeScale = iSize;
                Bitmap bmp = encoder.Encode(strQrData);
                //string strNameQR = "qr_code" + DateTime.Now.ToString("HHss") + ".png";
                //strPathFileImage = pathFile + strNameQR;
                if (!string.IsNullOrEmpty(strPathLogo))
                {
                    Graphics g = Graphics.FromImage(bmp);
                    Bitmap logo = new Bitmap(strPathLogo);
                    g.DrawImage(logo, new Point((bmp.Width - logo.Width) / 2, (bmp.Height - logo.Height) / 2));
                    g.Save();
                    g.Dispose();
                }
                //bmp.Save(strPathFileImage, ImageFormat.Png);
                System.IO.MemoryStream ms = new MemoryStream();
                bmp.Save(ms, ImageFormat.Jpeg);
                byte[] byteImage = ms.ToArray();
                return Convert.ToBase64String(byteImage);
                //bmp.Dispose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #region Lib a Khoa
        public void replaceHTML(JObject jObj, StringBuilder sb)
        {

            if (jObj["type"].ToString().Equals("string"))
            {
                sb.Replace("@" + jObj["key"].ToString() + "@", jObj["value"].ToString());
            }
            else if (jObj["type"].ToString().Equals("array"))
            {
                JArray arr = JArray.Parse(jObj["value"].ToString());
                List<string> lsItem = arr.Select(c => (string)c).ToList();
                string strRep = "";
                foreach (string strArr in lsItem)
                {
                    strRep = strRep + strArr + "</br>";
                }
                sb.Replace("@" + jObj["key"].ToString() + "@", strRep);
            }
        }
        public string convertPageToPDFNew(string strJSON, string strPathH, string strPathB, string strPathF, List<string> lsReplace, string strUrlPage)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                strJSON = Regex.Replace(strJSON, @"\t|\n|\r", "");
                JObject jInput = JObject.Parse(strJSON);
                //string strType = jInput["MA_TEMP"].ToString();
                JArray arrHeader = JArray.Parse(jInput["HEADER"].ToString());
                JArray arrbodyr = JArray.Parse(jInput["BODY"].ToString());
                //string readText = File.ReadAllText(strPathB);
                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);
                List<JToken> lsJToken = arrbodyr.ToList();
                foreach (string str in lsReplace)
                {
                    bool bCheck = false;
                    if (str.IndexOf(":") > 0)
                    {
                        //đây là trường hợp array COVERAGE:NAME_PRINT,CONTENT
                        string strKeyArray = str.Substring(0, str.IndexOf(":"));
                        List<string> strBienRep = str.Substring(str.IndexOf(":") + 1, str.Length - strKeyArray.Length - 1).Split(',').ToList();
                        foreach (JToken jT in lsJToken)
                        {
                            if (jT["type"].ToString().Equals("list"))
                            {
                                if (jT["key"].ToString().Equals(strKeyArray))
                                {
                                    string strAll = "";
                                    JArray arr = JArray.Parse(jT["value"].ToString());
                                    List<JToken> lsJTokenDetail = arr.ToList();
                                    foreach (JArray jT12 in lsJTokenDetail)
                                    {
                                        List<JToken> lsJTl = jT12.ToList();
                                        foreach (JObject jT1 in lsJTl)
                                        {
                                            bCheck = false;
                                            foreach (string strD in strBienRep)
                                            {
                                                if (jT1["type"].ToString().Equals("string"))
                                                {
                                                    if (jT1["key"].ToString().Equals(strD))
                                                    {
                                                        strAll = strAll + jT1["value"].ToString();
                                                        bCheck = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (bCheck)
                                                strAll = strAll + "</br>";
                                        }
                                        //strAll = strAll + "</br>";
                                    }
                                    sb.Replace("@" + strKeyArray + "@", strAll);
                                    bCheck = true;
                                }
                            }
                        }
                    }
                    else if (str.IndexOf("=") > 0)
                    {
                        //đây là trường hợp replace 2 giá trị trong 2 trường trong ngoặc vuông COVERAGE=[COVERAGE_CODE,STATUS]
                        string strKeyArray = str.Substring(0, str.IndexOf("="));
                        List<string> strBienRep = str.Substring(str.IndexOf("=") + 1, str.Length - strKeyArray.Length - 1).Split(',').ToList();
                        foreach (JToken jT in lsJToken)
                        {
                            if (jT["type"].ToString().Equals("list"))
                            {
                                if (jT["key"].ToString().Equals(strKeyArray))
                                {
                                    JArray arr = JArray.Parse(jT["value"].ToString());
                                    List<JToken> lsJTokenDetail = arr.ToList();
                                    foreach (JArray jT12 in lsJTokenDetail)
                                    {
                                        List<JToken> lsJTl = jT12.ToList();
                                        string str1 = "";
                                        string str2 = "";
                                        foreach (JObject jT1 in lsJTl)
                                        {
                                            bCheck = false;
                                            if (jT1["type"].ToString().Equals("string"))
                                            {
                                                if (jT1["key"].ToString().Equals(strBienRep[0].ToString().Substring(1, strBienRep[0].ToString().Length - 1)))
                                                {
                                                    str1 = jT1["value"].ToString();
                                                }
                                            }
                                            if (jT1["type"].ToString().Equals("string"))
                                            {
                                                if (jT1["key"].ToString().Equals(strBienRep[1].ToString().Substring(0, strBienRep[1].ToString().Length - 1)))
                                                {
                                                    str2 = jT1["value"].ToString();

                                                }
                                            }

                                        }
                                        sb.Replace("@" + str1 + "@", str2);
                                    }
                                    bCheck = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        //trường hợp string bình thường
                        foreach (JToken jT in lsJToken)
                        {
                            if (jT["type"].ToString().Equals("string"))
                            {
                                if (str.IndexOf("(") > 0)
                                {
                                    string strKeyOne = str.Substring(0, str.IndexOf("("));
                                    string strValueRep = str.Substring(str.IndexOf("(") + 1, str.Length - strKeyOne.Length - 2);
                                    if (jT["key"].ToString().Equals(strKeyOne))
                                    {
                                        sb.Replace("@" + jT["value"].ToString() + "@", strValueRep);
                                        bCheck = true;
                                        break;
                                    }
                                }
                                else
                                {
                                    if (jT["key"].ToString().Equals(str))
                                    {
                                        if (str.Equals("MONEY"))
                                        {
                                            if (!string.IsNullOrEmpty(jT["value"]?.ToString()))
                                            {
                                                sb.Replace("@" + jT["key"].ToString() + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(jT["value"].ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                            }
                                            else
                                            {
                                                sb.Replace("@" + jT["key"].ToString() + "@", "");
                                            }
                                        }
                                        else
                                        {
                                            sb.Replace("@" + jT["key"].ToString() + "@", jT["value"].ToString());
                                        }

                                        bCheck = true;
                                        break;
                                    }

                                }
                            }
                        }
                    }
                    if (!bCheck)
                    {
                        sb.Replace("@" + str + "@", "");
                    }

                }
                Document pdfDocument = new Document();
                pdfDocument.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                PdfPage pdfPage = pdfDocument.AddPage();
                //pdfDocument.Fonts.Add(@"D:\Test\sign\font.ttf");
                //pdfPage.Document.Fonts.Add(@"D:\Test\sign\font.ttf");
                //Cursor = Cursors.WaitCursor;
                try
                {
                    // Add a default document header
                    if (!strPathH.Equals(""))
                        AddHeader(pdfDocument, true, arrHeader, strPathH);

                    // Add a default document footer
                    if (!strPathF.Equals(""))
                        AddFooter(pdfDocument, true, true, strPathF);

                    // Create a HTML to PDF element to add to document
                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, sb.ToString(), "");
                    htmlToPdfElement.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_and_after_div , #page_break_before_div" };
                    htmlToPdfElement.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR" };
                    htmlToPdfElement.AvoidImageBreak = true;
                    htmlToPdfElement.AvoidTextBreak = true;
                    // Optionally set a delay before conversion to allow asynchonous scripts to finish
                    htmlToPdfElement.ConversionDelay = 1;

                    // Add HTML to PDF element to document
                    pdfPage.AddElement(htmlToPdfElement);

                    // Automatically close the external PDF documents after the final document is saved
                    pdfDocument.AutoCloseAppendedDocs = true;
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            pdfDocument.AppendDocument(docAdd);
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }

                    }
                    // Save the PDF document in a memory buffer
                    byte[] outPdfBuffer = pdfDocument.Save();

                    // Write the memory buffer in a PDF file
                    //System.IO.File.WriteAllBytes(outPdfFile, outPdfBuffer);
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    //MessageBox.Show(String.Format("Create PDF Document Error. {0}", ex.Message));
                    return "";
                }
                finally
                {
                    // Close the PDF document
                    pdfDocument.Close();

                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string replaceHTML_JSON(JArray strJSON, string strHTML, List<string> lsReplace)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(strHTML);
                List<JToken> lsJToken = strJSON[0].ToList();
                foreach (string str in lsReplace)
                {
                    bool bCheck = false;
                    //trường hợp string bình thường
                    foreach (JToken jT in lsJToken)
                    {
                        if (jT["type"].ToString().Equals("string"))
                        {
                            if (jT["key"].ToString().Equals(str))
                            {
                                sb.Replace("@" + jT["key"].ToString() + "@", jT["value"].ToString());
                                bCheck = true;
                                break;
                            }
                        }
                    }
                    if (!bCheck)
                    {
                        sb.Replace("@" + str + "@", "");
                    }

                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private void AddHeader(Document pdfDocument, bool drawHeaderLine, JArray arrHeader, string headerHtmlUrl)
        {
            //string headerHtmlUrl = System.IO.Path.Combine(Application.StartupPath,
            //            @"DemoAppFiles\Input\HTML_Files\Header_HTML.html");
            //string headerHtmlUrl = @"E:\HDI\Cong_Viec\KHOADD\AKhanh\HDI-GCNBH_Header.html";

            //string readText = File.ReadAllText(headerHtmlUrl);
            StringBuilder sb = new StringBuilder();
            sb.Append(headerHtmlUrl);
            if (arrHeader != null)
            {
                List<JToken> lsJToken = arrHeader.ToList();
                foreach (JToken jT in lsJToken)
                {
                    replaceHTML((JObject)jT, sb);
                }
            }

            //File.WriteAllText(headerHtmlUrl, sb.ToString(), Encoding.UTF8);
            // Create the document footer template
            pdfDocument.AddHeaderTemplate(70);

            // Create a HTML element to be added in header
            HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, sb.ToString(), "");

            // Set the HTML element to fit the container height
            headerHtml.FitHeight = true;

            // Add HTML element to header
            pdfDocument.Header.AddElement(headerHtml);

            //if (drawHeaderLine)
            //{
            //    float headerWidth = pdfDocument.Header.Width;
            //    float headerHeight = pdfDocument.Header.Height;

            //    // Create a line element for the bottom of the header
            //    LineElement headerLine = new LineElement(0, headerHeight - 1, headerWidth, headerHeight - 1);

            //    // Set line color
            //    headerLine.ForeColor = Color.Gray;

            //    // Add line element to the bottom of the header
            //    pdfDocument.Header.AddElement(headerLine);
            //}
        }

        /// <summary>
        /// Add a footer to document
        /// </summary>
        /// <param name="pdfDocument">The PDF document object</param>
        /// <param name="addPageNumbers">A flag indicating if the page numbering is present in footer</param>
        /// <param name="drawFooterLine">A flag indicating if a line should be drawn at the top of the footer</param>
        private void AddFooter(Document pdfDocument, bool addPageNumbers, bool drawFooterLine, string footerHtmlUrl)
        {
            pdfDocument.AddFooterTemplate(25);
            RectangleElement backColorRectangle = new RectangleElement(0, 0, pdfDocument.Footer.Width, pdfDocument.Footer.Height);
            //backColorRectangle.BackColor = Color.FromArgb(1, 0, 109, 49);
            backColorRectangle.BackColor = Color.White;
            pdfDocument.Footer.Anchoring = TemplateAnchoring.BottomRight;
            pdfDocument.Footer.AddElement(backColorRectangle);
            
            // Create a HTML element to be added in footer
            HtmlToPdfElement footerHtml = new HtmlToPdfElement(0,2, pdfDocument.Footer.Width,25, footerHtmlUrl, "");
            footerHtml.FitHeight = true;
            footerHtml.FitWidth = true;
            pdfDocument.Footer.AddElement(footerHtml);
        }
        public string addTextToPdf(string strText, string strID_FILE)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Document pdfDocument = new Document();
                pdfDocument.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                
                try
                {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strID_FILE);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            pdfDocument.InsertDocument(0,docAdd);
                        PdfPage pdfPage = pdfDocument.Pages[0];
                        Font systemFontNormal = new Font("Times New Roman", 10, GraphicsUnit.Point);
                        PdfFont embeddedSystemFontNormal = pdfDocument.AddFont(systemFontNormal);                        
                        float topBottomTextWidth = embeddedSystemFontNormal.GetTextWidth(strText);

                        TextElement topBottomVerticalTextElement = new TextElement(0, 0, strText, embeddedSystemFontNormal);
                        topBottomVerticalTextElement.Translate(90, 5);
                        pdfPage.AddElement(topBottomVerticalTextElement);
                    }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan" + strID_FILE, Logger.ConcatName(nameClass, nameMethod));
                        }
                    byte[] outPdfBuffer = pdfDocument.Save();
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tai file " + strID_FILE, Logger.ConcatName(nameClass, nameMethod));
                    return "";
                }
                finally
                {
                    pdfDocument.Close();
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan" + strID_FILE + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                throw new Exception(ex.ToString());
            }
        }
        public string convertPageToPDFNew(string strPathH, string strPathB, string strPathF, string strUrlPage)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);

                Document pdfDocument = new Document();
                pdfDocument.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                PdfPage pdfPage = pdfDocument.AddPage();
                try
                {
                    // Add a default document header
                    if (!strPathH.Equals(""))
                        AddHeader(pdfDocument, true, null, strPathH);

                    // Add a default document footer
                    if (!strPathF.Equals(""))
                        AddFooter(pdfDocument, true, true, strPathF);

                    // Create a HTML to PDF element to add to document
                    HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, sb.ToString(), "");
                    htmlToPdfElement.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_and_after_div , #page_break_before_div" };
                    htmlToPdfElement.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR" };
                    htmlToPdfElement.AvoidImageBreak = true;
                    htmlToPdfElement.AvoidTextBreak = true;
                    // Optionally set a delay before conversion to allow asynchonous scripts to finish
                    htmlToPdfElement.ConversionDelay = 1;

                    pdfPage.AddElement(htmlToPdfElement);
                    
                    // Automatically close the external PDF documents after the final document is saved
                    pdfDocument.AutoCloseAppendedDocs = true;
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            pdfDocument.AppendDocument(docAdd);
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }

                    }

                    // Save the PDF document in a memory buffer
                    byte[] outPdfBuffer = pdfDocument.Save();

                    // Write the memory buffer in a PDF file
                    //System.IO.File.WriteAllBytes(outPdfFile, outPdfBuffer);
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    //MessageBox.Show(String.Format("Create PDF Document Error. {0}", ex.Message));
                    return "";
                }
                finally
                {
                    // Close the PDF document
                    pdfDocument.Close();

                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        
        //HtmlToPdfConverter htmlToPdfConverter1;
        public string convertPageToPDFPersnal(string strPathH, string strPathB, string strPathF, string strUrlPage, string strTextFind, string strValueAdd, string strOrien ="", string strHeaderHeight = "", string strFooterHeight ="", string strWatermark="")
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                float header_Height = 65f;
                float footer_portrait_Height = 25f;
                float footer_landscape_Height = 35f;
                if(!string.IsNullOrEmpty(strHeaderHeight))
                {
                    header_Height = (float)Convert.ToDouble(strHeaderHeight);
                }
                if (!string.IsNullOrEmpty(strFooterHeight))
                {
                    if(string.IsNullOrEmpty(strOrien))
                    {
                        footer_portrait_Height = (float)Convert.ToDouble(strFooterHeight);
                    }    
                    else
                    {
                        footer_landscape_Height = (float)Convert.ToDouble(strFooterHeight);
                    }                        
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "W/H", strHeaderHeight + strFooterHeight);

                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);
                Document documentObject = null;
                HtmlToPdfConverter htmlToPdfConverter1 = new HtmlToPdfConverter();
                htmlToPdfConverter1.BeforeRenderPdfPageEvent += new BeforeRenderPdfPageDelegate(htmlToPdfConverter_BeforeRenderPdfPageEvent);
                htmlToPdfConverter1.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                try
                {
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    if (strPathB.IndexOf("<script>") > 100)
                        htmlToPdfConverter1.ConversionDelay = 1;
                    else
                        htmlToPdfConverter1.ConversionDelay = 0;

                    htmlToPdfConverter1.JavaScriptEnabled = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR", "#page_break_inside_avoid_table tr" };
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidImageBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidTextBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_div" };
                    
                    HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 2, PdfPageSize.A4.Width, footer_portrait_Height, strPathF, "");
                    if (string.IsNullOrEmpty(strOrien))
                    {
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
                        htmlToPdfConverter1.PdfFooterOptions.FooterHeight = footer_portrait_Height;
                    }                            
                    else
                    {
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;
                        htmlToPdfConverter1.PdfDocumentOptions.RightMargin = 0;                        
                        htmlToPdfConverter1.PdfFooterOptions.FooterHeight = footer_landscape_Height;
                        //htmlToPdfConverter1.PdfFooterOptions.FooterBackColor = Color.CadetBlue;
                        footerHtml = new HtmlToPdfElement(0, 5, htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize.Height +200, footer_landscape_Height, strPathF, "");
                    }    
                    
                    htmlToPdfConverter1.JavaScriptEnabled = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR", "#page_break_inside_avoid_table tr" };
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidImageBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidTextBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.PageBreakBeforeHtmlElementsSelectors = new string[] {"#page_break_before_div" };
                    htmlToPdfConverter1.PdfDocumentOptions.ShowHeader = true;
                    htmlToPdfConverter1.PdfDocumentOptions.ShowFooter = true;
                    htmlToPdfConverter1.PdfHeaderOptions.HeaderHeight = header_Height;                    
                    HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, strPathH, "");
                    //headerHtml.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                    htmlToPdfConverter1.PdfHeaderOptions.AddElement(headerHtml);
                    htmlToPdfConverter1.PdfDocumentOptions.JpegCompressionLevel = 2;
                    htmlToPdfConverter1.PdfDocumentOptions.ImagesScalingEnabled = true;

                    footerHtml.FitHeight = true;
                    footerHtml.FitWidth = true;
                    htmlToPdfConverter1.PdfFooterOptions.AddElement(footerHtml);
                    
                    documentObject = htmlToPdfConverter1.ConvertHtmlToPdfDocumentObject(sb.ToString(),"");                    
                    documentObject.AutoCloseAppendedDocs = true;
                    
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                            int ipage = 0;
                            LibSign clsSign = new LibSign(pathRoot);
                            bool check = clsSign.findTextInPdfBatch(strTextFind, bPDF, ref x1, ref y1, ref width1, ref ipage);
                            if (check && ipage >0)
                            {
                                PdfPage pdfPage = docAdd.Pages[ipage - 1];
                                y1 = pdfPage.PageSize.Height - y1 - 3;
                                x1 = x1 + width1 + 7;
                                AddElementResult addTextResult = null;
                                // Start 2022-12-22 By ThienTVB Add Font Nunito
                                //PdfFont titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                PdfFont titleFont;
                                if (!strPathF.Contains("Nunito"))
                                {
                                    titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                else
                                {
                                    titleFont = docAdd.AddFont(new Font("Nunito", 8, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                // End 2022-12-22 By ThienTVB Add Font Nunito
                                //titleFont.IsUnderline = true;
                                //strValueAdd = "Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman";
                                TextElement titleTextElement = new TextElement(x1, y1, strValueAdd, titleFont);
                                addTextResult = pdfPage.AddElement(titleTextElement);
                                pdfPage = addTextResult.EndPdfPage;
                                docAdd.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                                //pdfPage.Margins.Right = -30;
                                //docAdd.Margins.Right = -30;
                                bPDF = docAdd.Save();
                                Stream stream1 = new MemoryStream(bPDF);
                                Document docAdd1 = new Document(stream1);
                                //docAdd1.Margins.Right = -30;
                                //documentObject.Margins.Right = -30;
                                                                
                                documentObject.AppendDocument(docAdd1, true, true, true);                                
                            }
                            else
                            {
                                documentObject.AppendDocument(docAdd, true, true, true);
                            }
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }
                    }                    

                    byte[] outPdfBuffer = documentObject.Save();
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi tạo file" + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    documentObject.Close();
                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        // Start 2022-08-18 By ThienTVB
        public byte[] convertPageToPDFPersnalByte64(string strPathH, string strPathB, string strPathF, string strUrlPage, string strTextFind, string strValueAdd, string strOrien = "")
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);
                Document documentObject = null;
                HtmlToPdfConverter htmlToPdfConverter1 = new HtmlToPdfConverter();
                htmlToPdfConverter1.BeforeRenderPdfPageEvent += new BeforeRenderPdfPageDelegate(htmlToPdfConverter_BeforeRenderPdfPageEvent);
                htmlToPdfConverter1.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                try
                {
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    if (strPathB.IndexOf("<script>") > 100)
                        htmlToPdfConverter1.ConversionDelay = 1;
                    else
                        htmlToPdfConverter1.ConversionDelay = 0;
                    HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 2, PdfPageSize.A4.Width, 26, strPathF, "");
                    if (string.IsNullOrEmpty(strOrien))
                    {
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
                        htmlToPdfConverter1.PdfFooterOptions.FooterHeight = 25f;
                    }
                    else
                    {
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;
                        htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;
                        htmlToPdfConverter1.PdfDocumentOptions.RightMargin = 0;
                        htmlToPdfConverter1.PdfFooterOptions.FooterHeight = 35f;
                        //htmlToPdfConverter1.PdfFooterOptions.FooterBackColor = Color.CadetBlue;
                        footerHtml = new HtmlToPdfElement(0, 5, htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize.Height + 200, 36, strPathF, "");
                    }

                    htmlToPdfConverter1.JavaScriptEnabled = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR", "#page_break_inside_avoid_table tr" };
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidImageBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidTextBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_div" };
                    htmlToPdfConverter1.PdfDocumentOptions.ShowHeader = true;
                    htmlToPdfConverter1.PdfDocumentOptions.ShowFooter = true;
                    htmlToPdfConverter1.PdfHeaderOptions.HeaderHeight = 65;
                    HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, strPathH, "");
                    //headerHtml.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                    htmlToPdfConverter1.PdfHeaderOptions.AddElement(headerHtml);
                    htmlToPdfConverter1.PdfDocumentOptions.JpegCompressionLevel = 2;
                    htmlToPdfConverter1.PdfDocumentOptions.ImagesScalingEnabled = true;

                    footerHtml.FitHeight = true;
                    footerHtml.FitWidth = true;
                    htmlToPdfConverter1.PdfFooterOptions.AddElement(footerHtml);
                    // Test time run Function "ConvertHtmlToPdfDocumentObject"
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    documentObject = htmlToPdfConverter1.ConvertHtmlToPdfDocumentObject(sb.ToString(), "");
                    documentObject.AutoCloseAppendedDocs = true;
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                            int ipage = 0;
                            LibSign clsSign = new LibSign(pathRoot);
                            bool check = clsSign.findTextInPdfBatch(strTextFind, bPDF, ref x1, ref y1, ref width1, ref ipage);
                            if (check && ipage > 0)
                            {
                                PdfPage pdfPage = docAdd.Pages[ipage - 1];
                                y1 = pdfPage.PageSize.Height - y1 - 3;
                                x1 = x1 + width1 + 7;
                                AddElementResult addTextResult = null;
                                // Start 2022-12-22 By ThienTVB Add Font Nunito
                                //PdfFont titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                PdfFont titleFont;
                                if (!strPathF.Contains("Nunito"))
                                {
                                    titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                else
                                {
                                    titleFont = docAdd.AddFont(new Font("Nunito", 8, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                // End 2022-12-22 By ThienTVB Add Font Nunito
                                //titleFont.IsUnderline = true;
                                //strValueAdd = "Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman";
                                TextElement titleTextElement = new TextElement(x1, y1, strValueAdd, titleFont);
                                addTextResult = pdfPage.AddElement(titleTextElement);
                                pdfPage = addTextResult.EndPdfPage;
                                docAdd.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                                //pdfPage.Margins.Right = -30;
                                //docAdd.Margins.Right = -30;
                                bPDF = docAdd.Save();
                                Stream stream1 = new MemoryStream(bPDF);
                                Document docAdd1 = new Document(stream1);
                                //docAdd1.Margins.Right = -30;
                                //documentObject.Margins.Right = -30;
                                documentObject.AppendDocument(docAdd1, true, true, true);
                            }
                            else
                            {
                                documentObject.AppendDocument(docAdd, true, true, true);
                            }
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }
                    }
                    sw.Stop();
                    double timeExcute = sw.Elapsed.TotalSeconds;
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Time Run create PDF: " + timeExcute.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    return documentObject.Save();
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi tạo file" + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    documentObject.Close();
                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        // End 2022-08-18 By ThienTVB
        public string convertPageToPDFPersnalA5(string strPathH, string strPathB, string strPathF, string strUrlPage, string strTextFind, string strValueAdd, string strPageSize, string strHeaderHeight, string strFooterHeight, string strWatermark)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                float header_Height = 90f;
                float footer_portrait_Height = 70f;                
                if (!string.IsNullOrEmpty(strHeaderHeight))
                {
                    header_Height = (float)Convert.ToDouble(strHeaderHeight);
                }
                if (!string.IsNullOrEmpty(strFooterHeight))
                {
                    footer_portrait_Height = (float)Convert.ToDouble(strFooterHeight);
                }
                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);
                Document documentObject = null;
                HtmlToPdfConverter htmlToPdfConverter1 = new HtmlToPdfConverter();
                //htmlToPdfConverter1.BeforeRenderPdfPageEvent += new BeforeRenderPdfPageDelegate(htmlToPdfConverter_BeforeRenderPdfPageEvent);
                htmlToPdfConverter1.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                try
                {
                    List<string> strSize = strPageSize.Split(',').ToList();
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = new PdfPageSize(Convert.ToInt16(strSize[0]), Convert.ToInt16(strSize[1]));
                    htmlToPdfConverter1.PdfDocumentOptions.BottomMargin = 0;
                    htmlToPdfConverter1.PdfDocumentOptions.TopMargin = 0;
                    htmlToPdfConverter1.PdfDocumentOptions.LeftMargin = 0;
                    htmlToPdfConverter1.PdfDocumentOptions.RightMargin = 0;
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
                    htmlToPdfConverter1.ConversionDelay = 2;
                    htmlToPdfConverter1.JavaScriptEnabled = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR", "#page_break_inside_avoid_table tr" };
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidImageBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidTextBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_div" };
                    htmlToPdfConverter1.PdfDocumentOptions.ShowHeader = true;
                    htmlToPdfConverter1.PdfDocumentOptions.ShowFooter = true;
                    htmlToPdfConverter1.PdfHeaderOptions.HeaderHeight = header_Height;
                    //htmlToPdfConverter1.PdfHeaderOptions.HeaderBackColor = Color.CadetBlue;
                    HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, strPathH, "");
                    //headerHtml.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                    htmlToPdfConverter1.PdfHeaderOptions.AddElement(headerHtml);
                    htmlToPdfConverter1.PdfFooterOptions.FooterHeight = footer_portrait_Height;
                    HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 2, PdfPageSize.Letter.Width, footer_portrait_Height, strPathF, "");
                    footerHtml.FitHeight = true;
                    footerHtml.FitWidth = true;
                    htmlToPdfConverter1.PdfFooterOptions.AddElement(footerHtml);
                    documentObject = htmlToPdfConverter1.ConvertHtmlToPdfDocumentObject(sb.ToString(), "");
                    documentObject.AutoCloseAppendedDocs = true;
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
                            int ipage = 0;
                            LibSign clsSign = new LibSign(pathRoot);
                            bool check = clsSign.findTextInPdfBatch(strTextFind, bPDF, ref x1, ref y1, ref width1, ref ipage);
                            if (check)
                            {
                                PdfPage pdfPage = docAdd.Pages[ipage - 1];
                                y1 = pdfPage.PageSize.Height - y1 - 3;
                                x1 = x1 + width1 + 7;
                                AddElementResult addTextResult = null;
                                // Start 2022-12-22 By ThienTVB Add Font Nunito
                                //PdfFont titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                PdfFont titleFont;
                                if (!strPathF.Contains("Nunito"))
                                {
                                    titleFont = docAdd.AddFont(new Font("Times New Roman", 10, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                else
                                {
                                    titleFont = docAdd.AddFont(new Font("Nunito", 8, FontStyle.Bold, GraphicsUnit.Point));
                                }
                                // End 2022-12-22 By ThienTVB Add Font Nunito
                                //titleFont.IsUnderline = true;
                                //strValueAdd = "Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman Times New Roman";
                                TextElement titleTextElement = new TextElement(x1, y1, strValueAdd, titleFont);
                                addTextResult = pdfPage.AddElement(titleTextElement);
                                pdfPage = addTextResult.EndPdfPage;
                                docAdd.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                                //pdfPage.Margins.Right = -30;
                                //docAdd.Margins.Right = -30;
                                bPDF = docAdd.Save();
                                Stream stream1 = new MemoryStream(bPDF);
                                Document docAdd1 = new Document(stream1);
                                //docAdd1.Margins.Right = -30;
                                //documentObject.Margins.Right = -30;
                                documentObject.AppendDocument(docAdd1, true, true, true);
                            }
                            else
                            {
                                documentObject.AppendDocument(docAdd, true, true, true);
                            }
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }
                    }
                    byte[] outPdfBuffer = documentObject.Save();
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi tạo file" + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    throw new Exception(ex.ToString());
                }
                finally
                {
                    documentObject.Close();
                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string convertPageToPDFPersnal_batch(string strPathH, string strPathB, string strPathF, string strUrlPage)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                StringBuilder sb = new StringBuilder();
                // Start 11-08-2022 By ThienTVB // Giam dung luong dau vao html
                strPathB = Uglify.Html(strPathB).Code;
                //strPathH = Uglify.Html(strPathH).Code;
                strPathF = Uglify.Html(strPathF).Code;
                // End 11-08-2022 By ThienTVB
                sb.Append(strPathB);
                Document documentObject = null;
                HtmlToPdfConverter htmlToPdfConverter1 = new HtmlToPdfConverter();
                htmlToPdfConverter1.BeforeRenderPdfPageEvent += new BeforeRenderPdfPageDelegate(htmlToPdfConverter_BeforeRenderPdfPageEvent);
                htmlToPdfConverter1.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                try
                {
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    htmlToPdfConverter1.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait;
                    //htmlToPdfConverter1.NavigationTimeout = 60;
                    htmlToPdfConverter1.ConversionDelay = 0;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "#page_break_inside_avoid_table TR", "#page_break_inside_avoid_table tr" };
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidImageBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.AvoidTextBreak = true;
                    htmlToPdfConverter1.PdfDocumentOptions.PageBreakBeforeHtmlElementsSelectors = new string[] { "#page_break_before_div" };
                    htmlToPdfConverter1.PdfDocumentOptions.ShowHeader = true;
                    htmlToPdfConverter1.PdfDocumentOptions.ShowFooter = true;
                    htmlToPdfConverter1.PdfHeaderOptions.HeaderHeight = 65;
                    HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, strPathH, "");
                    //headerHtml.NavigationCompletedEvent += new NavigationCompletedDelegate(headerHtml_NavigationCompletedEvent);
                    htmlToPdfConverter1.PdfHeaderOptions.AddElement(headerHtml);
                    htmlToPdfConverter1.PdfFooterOptions.FooterHeight = 25f;
                    HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 2, PdfPageSize.A4.Width, 26, strPathF, "");
                    footerHtml.FitHeight = true;
                    footerHtml.FitWidth = true;
                    htmlToPdfConverter1.PdfFooterOptions.AddElement(footerHtml);
                    documentObject = htmlToPdfConverter1.ConvertHtmlToPdfDocumentObject(sb.ToString(), "");
                    documentObject.AutoCloseAppendedDocs = true;
                    if (!string.IsNullOrEmpty(strUrlPage))
                    {
                        byte[] bPDF = goBusinessCommon.Instance.getFileServer(strUrlPage);
                        if (bPDF != null)
                        {
                            Stream stream = new MemoryStream(bPDF);
                            Document docAdd = new Document(stream);
                            documentObject.AppendDocument(docAdd, true, true, true);
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi duong dan cau hinh den file gan kem", Logger.ConcatName(nameClass, nameMethod));
                        }
                    }

                    // Save the PDF document in a memory buffer
                    byte[] outPdfBuffer = documentObject.Save();

                    // Write the memory buffer in a PDF file
                    //System.IO.File.WriteAllBytes(outPdfFile, outPdfBuffer);
                    return Convert.ToBase64String(outPdfBuffer);
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi tạo file" + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    return "";
                }
                finally
                {
                    // Close the PDF document
                    //pdfDocument.Close();
                    documentObject.Close();
                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public byte[] convertPageToPDFPersnal_doc(string strPathH, string strPathF, byte[] bPDF)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                Stream stream = new MemoryStream(bPDF);
                Document documentObject = new Document();
                documentObject.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                //PdfPage pdfPage = documentObject.AddPage();
                try
                {
                    PdfPage pdfPage = documentObject.AddPage();
                    Document docAdd = new Document(stream);
                    documentObject.InsertDocument(0, docAdd, true, true, true);
                    documentObject.AddFooterTemplate(23f);
                    //documentObject.InsertPage()

                    pdfPage.Margins.Right = 0;
                    documentObject.Margins.Right = 0;
                    documentObject.AddHeaderTemplate(65);
                    //documentObject.Header.Width = 400;
                    //RectangleElement backColorRectangle1 = new RectangleElement(0, 0, documentObject.Header.Width , 60);
                    
                    //backColorRectangle1.BackColor = Color.Red;
                    //documentObject.Header.AddElement(backColorRectangle1);
                    HtmlToPdfElement headerHtml = new HtmlToPdfElement(0, 0, documentObject.Header.Width, strPathH, ""); //450
                    headerHtml.FitHeight = true;
                    //headerHtml.FitWidth = true;
                    documentObject.Header.AddElement(headerHtml);
                    //RectangleElement backColorRectangle = new RectangleElement(0, 0, documentObject.Footer.Width - 100, 70);
                    //documentObject.Footer.Anchoring = TemplateAnchoring.BottomRight;
                    //documentObject.Footer.AddElement(backColorRectangle);
                    HtmlToPdfElement footerHtml = new HtmlToPdfElement(0, 2, documentObject.Footer.Width, 26f, strPathF, ""); //420
                    footerHtml.FitHeight = true;
                    footerHtml.FitWidth = true;
                    documentObject.Footer.AddElement(footerHtml);

                    //documentObject.Margins.Right = 0;
                    //documentObject.Margins.Left = 100;
                    //if (!strPathH.Equals(""))
                    //    AddHeader(documentObject, true, null, strPathH);

                    //Add a default document footer
                    //if (!strPathF.Equals(""))
                    //    AddFooter(documentObject, true, true, strPathF);
                    //pdfPage.AddElement(documentObject.);

                    documentObject.RemovePage(documentObject.Pages.Count-1);
                    documentObject.AutoCloseAppendedDocs = true;
                    
                    //documentObject.AppendDocument(docAdd,true, true, true);
                    
                    //PdfPage pdfPage1 = documentObject.AddPage();
                    //pdfPage1.Document.InsertDocument(0, docAdd, true, true, true);
                    //documentObject.InsertPage(0, pdfPage1);
                    //byte[] outPdfBuffer1 = docAdd.Save();
                    // Write the memory buffer in a PDF file
                    //System.IO.File.WriteAllBytes(@"D:/test.pdf", outPdfBuffer1);
                    byte[] outPdfBuffer = documentObject.Save();
                    return outPdfBuffer;
                }
                catch (Exception ex)
                {
                    // The PDF creation failed
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi tạo file" + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    return null;
                }
                finally
                {
                    // Close the PDF document
                    //pdfDocument.Close();
                    documentObject.Close();
                    //Cursor = Cursors.Arrow;
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public EvoPdf.ImageElement convertImage(string strPathH)
        {
            //string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                HtmlToImageConverter htmlToImageConverter = new HtmlToImageConverter();
                htmlToImageConverter.LicenseKey = "Z+n66P346Pv+8Pjo8Ob46Pv55vn65vHx8fHo+A==";
                try
                {
                    System.Drawing.Image imageTiles = null;
                    imageTiles = htmlToImageConverter.ConvertHtmlToImageObject(strPathH,"");
                    EvoPdf.ImageElement headerImage = new EvoPdf.ImageElement(5, 5, 100, 50, imageTiles);
                    return headerImage;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        //void headerHtml_NavigationCompletedEvent(NavigationCompletedParams eventParams)
        //{
        //    // Get the header HTML width and height from event parameters
        //    float headerHtmlWidth = eventParams.HtmlContentWidthPt;
        //    float headerHtmlHeight = eventParams.HtmlContentHeightPt;

        //    // Calculate the header width from coverter settings
        //    float headerWidth = htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize.Width - htmlToPdfConverter1.PdfDocumentOptions.LeftMargin -
        //                htmlToPdfConverter1.PdfDocumentOptions.RightMargin;

        //    // Calculate a resize factor to fit the header width
        //    float resizeFactor = 1;
        //    if (headerHtmlWidth > headerWidth)
        //        resizeFactor = headerWidth / headerHtmlWidth;

        //    // Calculate the header height to preserve the HTML aspect ratio
        //    float headerHeight = headerHtmlHeight * resizeFactor;

        //    if (!(headerHeight < htmlToPdfConverter1.PdfDocumentOptions.PdfPageSize.Height - htmlToPdfConverter1.PdfDocumentOptions.TopMargin -
        //                htmlToPdfConverter1.PdfDocumentOptions.BottomMargin))
        //    {
        //        throw new Exception("The header height cannot be bigger than PDF page height");
        //    }

        //    // Set the calculated header height
        //    htmlToPdfConverter1.PdfDocumentOptions.DocumentObject.Header.Height = headerHeight;
        //}

        void htmlToPdfConverter_BeforeRenderPdfPageEvent(BeforeRenderPdfPageParams eventParams)
        {
            
            // Get the PDF page being rendered
            if(!String.IsNullOrEmpty(this.strWatermark))
            {
                PdfPage pdfPage = eventParams.Page;

                float pdfPageWidth = pdfPage.ClientRectangle.Width;
                float pdfPageHeight = pdfPage.ClientRectangle.Height;
                // The image to be added as background
                string backgroundImagePath = strPathTemplate + "Artboard302.png";
                if (pdfPageWidth > 700)
                {
                    backgroundImagePath = strPathTemplate + "WaterMark.png";
                }
                // The image element to add in background
                ImageElement backgroundImageElement = new ImageElement(0, 0, pdfPageWidth, pdfPageHeight, backgroundImagePath);
                backgroundImageElement.KeepAspectRatio = true;
                backgroundImageElement.EnlargeEnabled = true;

                // Add the background image element to PDF page before the main content is rendered
                pdfPage.AddElement(backgroundImageElement);
            }                
        }
        public string clearParram(string strBody)
        {
            bool bcheck = true;
            string strtemp = "";
            int i1 = strBody.IndexOf("@");
            while (i1 > 1)
            {
                bcheck = false;
                i1 = strBody.IndexOf("@");
                if (i1 > 0)
                {
                    strtemp = strtemp + strBody.Substring(0, i1);
                    strBody = strBody.Substring(i1 + 1);
                    int k = strBody.IndexOf("@");
                    if (k > 20)
                    {
                        strtemp = strtemp + "@";
                    }
                    else if (k > 0)
                    {
                        strBody = strBody.Substring(k + 1);
                    }
                    else
                    {
                        strtemp = strtemp + "@" + strBody;
                        break;
                    }
                }
                else
                {
                    strtemp = strtemp + strBody;
                }
            }
            if (bcheck)
                strtemp = strBody;
            return strtemp;


        }
        #endregion
        //#region Lib create file PDF with resquest API 2022-08-18 by ThienTVB
        //public DataActionPDF TestCreatePDF(BaseRequest request)//object jsonPost)
        //{
        //    try
        //    {
        //        DataActionPDF dataActionPDF = JsonConvert.DeserializeObject<DataActionPDF>(JsonConvert.SerializeObject(request.Data));
        //        DataPDF dataPDF = JsonConvert.DeserializeObject<DataPDF>(JsonConvert.SerializeObject(dataActionPDF.DataPDF));
        //        string strPathH = dataPDF.Header ?? "";//@"<?xml version='1.0' encoding='utf-16'?><html><head><style>body{background: rgb(255,255,255);}page{background: white; display: block; margin: 0 auto;}page[size='A4']{width: 21cm; height: 29.7cm; min-width: 21cm; min-height: 29.7cm; max-width: 21cm; max-height: 29.7cm; font-size: 0.32cm;}page[size='A4'][layout='portrait']{width: 29.7cm; height: 21cm;}@media print{body, page{margin: 0; box-shadow: 0; padding: 0;}}.page-content{padding: 0cm 0cm;position: relative;height: 100%;background-repeat: no-repeat; background-attachment: local; background-position: bottom; background-size: contain; background-image: url('');}.logo svg{width: 2cm;height: 2cm;}.footer{position: absolute; bottom: 1.5cm; left: 0; margin: 0 auto; padding-left: 0cm;}h1,p,span{}p{margin:0px; line-height: 0.55cm; margin-top: 0.2cm;}.qr-code svg{width: 2cm;height: 2cm;}.qr-code{text-align: center;}.qr-code p{font-style: italic; display: inline-block; margin: 0; font-size: 0.3cm;}.header{display: flex; padding-top: 0.5cm;padding-right: 1.5cm;}.header .logo{width: 100%;}.subl1{text-align: center;font-size: 0.5cm; font-weight: 400;}.subl1 span{color: #329945;}.xtitle{font-weight: 700;}.mkx3ds{color: #329945; font-size: 0.4cm; margin: 0.1cm 0px;}p svg{width: 0.4cm; height: 0.3cm;}tr td{padding-top: 0.2cm;}span.bold{font-weight: bold;}.ajx92j{font-weight: 700;}.opiXs2{font-style: italic;}</style></head><body style='margin:0px'><page size=''><div class='page-content'><div class='header'><div style='margin-left:50px;' class='logo'><svg width='131' height='91' viewBox='0 0 131 91' fill='none' xmlns='http://www.w3.org/2000/svg'><path d='M130.872 19.6029C130.401 17.3208 127.578 16.0168 125.182 16.8318C118.08 19.2769 110.935 21.722 103.534 23.2705C99.8116 24.0448 91.5118 25.6341 82.2708 24.0855C79.9605 23.678 77.2652 24.6968 76.709 27.1011C76.1956 29.2202 77.3936 31.9505 79.875 32.3987C84.8377 33.2953 89.7577 33.4583 94.6349 33.0915C94.5494 33.173 90.8701 35.5366 87.1908 43.8906C85.0944 48.6177 84.3243 53.7523 83.811 58.8462C83.597 61.0875 86.0356 63.2473 88.3459 63.1658C90.9984 63.0436 92.6242 61.2505 92.8809 58.8462C93.0948 56.5234 93.3942 54.8526 93.9504 52.3668C95.02 47.6804 96.9452 44.1351 97.2447 43.6053C98.0576 42.2198 99.0416 40.9157 100.239 39.8155C102.806 37.5334 104.304 38.3077 104.774 38.5929C106.828 39.8562 108.967 45.1946 110.122 49.2697C110.807 51.674 111.705 54.0376 112.39 56.4419C112.989 58.6425 115.641 60.1503 117.951 59.4575C120.305 58.724 121.759 56.4827 121.117 54.1598C119.877 49.6772 118.337 45.3169 116.454 41.038C114.7 37.1259 112.775 32.9693 108.796 30.8095C115.171 29.3017 120.561 27.9569 126.337 25.4303C128.476 24.4523 131.428 22.048 130.872 19.6029Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M0 62.2676V25.8768C0 24.695 0.427826 23.6762 1.28348 22.8612C2.13913 22.0462 3.16592 21.6387 4.40662 21.6387H8.68488V36.7573H25.969V21.6387H34.7395V62.2676H25.969V44.2148H8.64209V62.2676H0Z' fill='#DA2128'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M39.0178 62.2676V25.8768C39.0178 24.695 39.4456 23.6762 40.3013 22.8612C41.157 22.0462 42.2265 21.6387 43.4672 21.6387H54.2484C60.7514 21.6387 65.6286 23.3095 68.8801 26.6103C72.0888 30.0742 73.7145 35.2088 73.7145 41.9735C73.7145 45.6003 73.1156 48.8604 71.9604 51.7945C70.6342 54.7693 68.9229 57.0921 66.7409 58.6407C64.9013 59.9855 62.9761 60.9228 60.9225 61.4933C58.9545 62.023 55.9169 62.3083 51.8526 62.3083H39.0178V62.2676ZM47.5743 54.9323H53.1789C57.2432 54.9323 60.1952 53.9136 62.1204 51.876C63.9173 50.0015 64.8585 46.7006 64.8585 41.9735C64.8585 37.4094 64.0029 34.0678 62.2488 31.9487C60.623 29.8296 57.9705 28.7701 54.334 28.7701H47.5743V54.9323Z' fill='#DA2128'></path><path d='M99.5552 18.827C105.013 18.827 109.438 14.6125 109.438 9.41351C109.438 4.21457 105.013 0 99.5552 0C94.0971 0 89.6724 4.21457 89.6724 9.41351C89.6724 14.6125 94.0971 18.827 99.5552 18.827Z' fill='url(#paint0_radial)'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M0 72.6615V90.7143H4.66331V70.4609H2.18192C1.41183 70.4609 0 71.0315 0 72.6615Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M35.2952 81.6262C34.0545 81.1371 32.3859 80.6481 30.8458 80.2406C28.4927 79.6293 27.5515 79.3441 27.4232 78.5698C27.2092 77.2658 29.0917 77.062 29.9901 77.062C33.7978 77.062 33.4983 79.1811 34.3539 79.1811H37.4343C37.4343 76.247 35.1668 74.943 34.6962 74.6985C33.4127 74.0057 31.8298 73.6797 29.9473 73.6797C27.851 73.6797 26.1397 74.1687 24.8134 75.106C23.5299 76.1655 22.8882 77.4695 22.8882 79.0588C22.8882 82.6449 26.5247 83.46 28.8778 83.9897C31.8298 84.6417 33.4127 85.0085 33.4127 86.068C33.4127 86.557 33.156 86.9646 32.6426 87.2498C32.0865 87.5351 31.4019 87.6981 30.5891 87.6981C29.4767 87.6981 28.6211 87.4943 27.9793 87.0868C27.3804 86.6793 27.0381 86.0273 26.9953 85.2123H22.5459C22.5459 85.2123 22.2465 87.7796 24.4712 89.5726C25.6691 90.5507 27.5515 90.9989 30.0329 90.9989C32.6426 90.9989 34.6106 90.5506 35.9369 89.6134C37.3487 88.5946 38.0332 87.2091 38.0332 85.3753V85.3345C37.9905 83.7045 36.8781 82.2374 35.2952 81.6262Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M49.9705 76.2067V84.6829C49.9705 85.7017 49.671 86.3945 49.0721 86.8835C48.4731 87.3317 47.7458 87.5762 46.8474 87.5762C45.9489 87.5762 45.2216 87.3317 44.6227 86.8835C44.0237 86.4352 43.7242 85.7017 43.7242 84.6829V74.0469H39.1465V84.7237C39.1465 87.617 40.9861 90.9993 46.8901 90.9993C52.7941 90.9993 54.6338 87.5762 54.6338 84.7237V74.0469H52.3235C50.9973 74.0469 49.9705 75.0249 49.9705 76.2067Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M58.5263 75.8805C57.6706 76.6141 55.7026 78.3664 55.7026 82.6452V90.7139H60.2804V82.5637C60.2804 78.2034 65.5426 78.5294 65.5426 78.5294V74.0875C65.5854 74.0467 60.8793 73.843 58.5263 75.8805Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M109.951 85.8637C109.694 86.0675 108.539 87.3715 106.785 87.3715C105.63 87.3715 104.774 87.0455 104.218 86.3528C103.448 85.5785 103.02 84.1929 103.02 82.2776C103.02 78.2025 105.245 77.4283 106.357 77.2653C108.368 77.0208 109.48 78.2433 109.651 79.0583H114.315C114.315 77.9173 113.074 73.5977 106.485 73.5977C104.004 73.5977 102.036 74.3719 100.624 75.9205C99.2552 77.4283 98.5707 79.5066 98.5707 82.1554C98.5707 84.9672 99.2552 87.0863 100.667 88.5941C102.079 90.1426 104.004 90.9169 106.528 90.9169C108.582 90.9169 110.293 90.4279 111.662 89.4906C113.031 88.5941 113.93 87.2493 114.4 85.497H111.106C110.635 85.5785 110.25 85.66 109.951 85.8637Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M131 82.3597C131 79.5886 130.315 77.4695 128.904 75.921C127.449 74.4132 125.438 73.6797 122.786 73.6797C120.304 73.6797 118.336 74.454 116.924 76.0025C115.555 77.5103 114.871 79.5886 114.871 82.2374C114.871 85.0493 115.555 87.1683 116.967 88.6761C118.379 90.2246 120.304 90.9989 122.828 90.9989C124.882 90.9989 126.593 90.5099 127.962 89.5726C129.331 88.6761 130.23 87.3313 130.7 85.579H127.406C126.893 85.579 126.465 85.7013 126.208 85.905C125.909 86.1088 124.839 87.4128 123.085 87.4128C121.93 87.4128 121.074 87.0868 120.518 86.394C119.919 85.7828 119.577 84.8047 119.534 83.5007H130.914C130.914 83.5007 131 82.5634 131 82.3597ZM119.577 80.6889C119.62 79.5886 119.962 78.7736 120.561 78.2031C121.16 77.6325 121.973 77.3473 122.914 77.3473C123.984 77.3473 124.754 77.6325 125.353 78.2031C125.866 78.7328 126.165 79.5886 126.251 80.6889H119.577Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M13.9475 73.6797C8.04351 73.6797 6.20386 77.1435 6.20386 80.0369V90.7137H10.7816V80.0776C10.7816 79.0588 11.0811 78.3661 11.68 77.877C12.279 77.4288 13.0063 77.1843 13.9047 77.1843C14.8032 77.1843 15.5305 77.4288 16.1294 77.877C16.7284 78.3253 17.0279 79.0588 17.0279 80.0776V88.5538C17.0279 89.7356 18.0546 90.7137 19.2953 90.7137H21.6056V80.0369C21.6912 77.1435 19.8087 73.6797 13.9475 73.6797Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M90.0144 73.6797C84.1104 73.6797 82.2708 77.1435 82.2708 80.0369V90.7137H86.8485V80.0776C86.8485 79.0588 87.148 78.3661 87.7469 77.877C88.3459 77.4288 89.0732 77.1843 89.9716 77.1843C90.8701 77.1843 91.5974 77.4288 92.1963 77.877C92.7953 78.3253 93.0947 79.0588 93.0947 80.0776V88.5538C93.0947 89.7356 94.1215 90.7137 95.3622 90.7137H97.6725V80.0369C97.7581 77.1435 95.9184 73.6797 90.0144 73.6797Z' fill='#1E552A'></path><path fill-rule='evenodd' clip-rule='evenodd' d='M78.9769 74.8615C77.6935 74.0872 75.7682 73.6797 73.2441 73.6797C70.9766 73.6797 69.2225 74.1687 67.9818 75.0652C66.9123 75.8802 66.1849 76.9805 66.0138 78.5291H70.6343C70.8055 76.5323 76.4956 76.5323 76.4528 78.5291C76.4528 79.3441 75.426 79.8739 73.458 80.1184C66.3561 80.8926 65.6288 83.297 65.586 85.8643C65.5432 87.6981 66.1849 89.0429 67.4256 89.8171C68.6663 90.5914 70.1209 90.9989 71.7467 90.9989C73.6719 90.9989 75.0409 90.4691 76.6239 89.4911C77.0517 90.4691 77.8218 90.7137 78.4636 90.7137H80.9449V78.2846C80.9877 76.8175 80.3032 75.6765 78.9769 74.8615ZM76.5383 84.0305C76.5383 85.1715 76.1961 86.068 75.426 86.72C74.6987 87.3721 73.7147 87.6981 72.4312 87.6981C71.6611 87.6981 71.0622 87.5351 70.6343 87.2091C70.2493 86.8423 70.0354 86.394 70.0354 85.742C70.0354 85.2937 70.1637 84.0305 72.3029 83.623L73.8858 83.2969C75.0409 83.0524 75.8966 82.8079 76.5811 82.5227V84.0305H76.5383Z' fill='#1E552A'></path><defs><radialGradient id='paint0_radial' cx='0' cy='0' r='1' gradientUnits='userSpaceOnUse' gradientTransform='translate(97.1373 7.47035) scale(11.5749 11.0253)'><stop stop-color='#F2E3C5'></stop><stop offset='0.0999043' stop-color='#F0DFBC'></stop><stop offset='0.2629' stop-color='#E8D3A5'></stop><stop offset='0.4673' stop-color='#DCC081'></stop><stop offset='0.6' stop-color='#D3B369'></stop><stop offset='1' stop-color='#BB8A0B'></stop></radialGradient></defs></svg></div><div class='qr-code'><svg width='120' height='120' viewBox='0 0 120 120' fill='none' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><rect width='120' height='120' fill='url(#pattern0)'></rect><defs><pattern id='pattern0' patternContentUnits='objectBoundingBox' width='1' height='1'><use xlink:href='#image0' transform='scale(0.000468604)'></use></pattern><image id='image0' width='2134' height='2134' xlink:href='data:image/png;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAFpAWkDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD5/oor7/oA+AKK+/6KAPgCivv+igD4Ar7/AK+AK+/6APgCvv8AoooA+AKK+/6KACvgCvv+vgCgD7/oor4AoA+/6+AKKKAPv+iiigD4Ar7/AKK+AKACvv8Ar4AooAK+/wCvgCigAoor7/oA+AKKKKACvv8Ar4Ar7/oAKKKKACiivgCgAr7/AKKKAPgCiivv+gD4Aor7/ooA+AKK+/6KAPgCvv8Ar4Ar7/oA+AK+/wCvgCvv+gD4AooooAK+/wCvgCvv+gD4Ar7/AK+AK+/6APgCiiigAoor7/oA+AK+/wCivgCgD7/or4AooAKKK+/6ACivgCigD7/r4Aor7/oA+AK+/wCvgCvv+gD4AooooAKKKKAPv+vgCvv+igD4Ar7/AKKKACivgCigD7/r4Ar7/r4AoA+/6K+AK+/6APgCvv8Ar4Ar7/oA+AKKKKACvv8Ar4Ar7/oA+AK+/wCvgCvv+gD4Ar7/AK+AK+/6APgCivv+igD4Ar7/AKKKAPgCvv8Ar4Ar7/oAK+AKKKACvv8Ar4AooAKK+/6KACivgCigAr7/AKKKACvgCvv+igD4Aoor7/oA+AKK+/6KAPgCvv8Ar4AooA+/6K+AKKACivv+vgCgAoor7/oAK+AK+/6KAPgCivv+igD4Ar7/AKKKAPgCvv8Ar4Ar7/oA+AKK+/6KAPgCvv8AoooA+AK+/wCvgCvv+gD4Aor7/ooA+AKK+/6KAPgCivv+igD4Ar7/AKKKAPgCvv8Ar4Ar7/oA+AK+/wCvgCigAr7/AKK+AKACvv8Aor4AoAK+/wCvgCvv+gD4Ar7/AKKKAPgCvv8Ar4Ar7/oA+AK+/wCiigD4Ar7/AK+AKKACvv8Ar4AooAK+/wCiigD4Ar7/AKK+AKACvv8Ar4Ar7/oA+AK+/wCvgCvv+gD4Ar7/AK+AK+/6APgCivv+igD4Aor7/ooA+AKK+/6KAPgCvv8AoooAK+AKKKAPv+vgCvv+igAr4Ar7/ooA+AKK+/6KAPgCiivv+gD4Ar7/AK+AK+/6ACivgCigD7/r4Ar7/r4AoAK+/wCvgCvv+gD4Ar7/AK+AKKACivv+igD4Aor7/r4AoAKKKKAPv+vgCvv+igD4Ar7/AK+AKKACvv8AoooAK+AK+/6+AKACivv+igAr4Ar7/ooA+AKK+/6+AKAPv+vgCvv+igD4Ar7/AKK+AKACvv8Ar4Ar7/oAK+AKKKACvv8Ar4AooA+/6+AK+/6KAPgCivv+igD4Aor7/r4AoA+/6K+AKKAPv+vgCvv+igAr4Ar7/r4AoAK+/wCiigAr4Ar7/ooAK+AKKKAPv+ivgCigAoor7/oAK+AKKKACvv8Ar4AooA+/6KKKAPgCivv+vgCgAr7/AKKKACiivgCgD7/r4AoooA+/6K+AKKAPv+iivgCgAr7/AK+AK+/6APgCvv8Ar4Ar7/oA+AKK+/6+AKACvv8AoooA+AKK+/6KAPgCvv8Aor4AoA+/6+AK+/6KACivgCigD7/r4Ar7/ooAK+AKK+/6ACivgCvv+gD4Aor7/ooA+AKK+/6KACivgCigAor7/r4AoAK+/wCiigD4Ar7/AKK+AKACvv8Aor4AoA+/6K+AK+/6ACiivgCgD7/r4Ar7/ooA+AK+/wCiigAr4Ar7/ooA+AK+/wCvgCvv+gD4Ar7/AKKKAPgCvv8Ar4Ar7/oA+AKKK+/6APgCvv8Aor4AoA+/6KKKACvgCvv+vgCgAr7/AKK+AKAPv+ivgCigD7/oor4AoAK+/wCvgCigAooooA+/6+AKK+/6APgCvv8Aor4AoAKK+/6+AKAPv+vgCiigD7/r4Ar7/r4AoA+/6+AKKKAPv+ivgCigAooooAKK+/6KAPgCvv8Ar4Ar7/oA+AKK+/6KACiiigAooooAK+AK+/6KAPgCvv8AoooA+AK+/wCiigAr4Aor7/oA+AKK+/6KAPgCvv8Ar4Ar7/oAK+AKK+/6APgCiiigD7/r4Aor7/oA+AKK+/6KACvgCvv+vgCgD7/r4Ar7/ooA+AK+/wCvgCvv+gAr4Ar7/r4AoA+/6KKKAPgCvv8Aor4AoAKK+/6KACiiigAor4AooA+/6KK+AKAPv+iivgCgAr7/AKKKACivgCvv+gAoor4AoA+/6K+AK+/6APgCvv8Aor4AoAKK+/6KACiivgCgAor7/r4AoA+/6KK+AKAPv+iivgCgD7/oor4AoA+/6K+AKKACvv8Ar4AooA+/6KKKACiiigD4Ar7/AK+AKKAPv+iiigD4Ar7/AK+AK+/6ACvgCiigD7/r4Ar7/ooAKK+AKKACivv+igD4Aor7/ooA+AK+/wCiigAr4Aor7/oA+AKKKKACiivv+gD4Aor7/ooA+AKKKKACivv+vgCgAr7/AK+AKKACvv8Aor4AoAK+/wCvgCvv+gD4Ar7/AKKKAPgCiivv+gD4Aor7/ooA+AKKKKAPv+iiigD4Ar7/AKK+AKACivv+igAor4Ar7/oAK+AK+/6KAPgCiivv+gD4Aor7/ooA+AKKKKAPv+ivgCvv+gAor4Ar7/oAK+AKKKACivv+igD4Ar7/AK+AK+/6APgCvv8AoooA+AKKKKACivv+igAr4Aor7/oA+AK+/wCivgCgAr7/AKK+AKACivv+igD4Ar7/AK+AK+/6APgCvv8AoooA+AK+/wCvgCigAr7/AK+AK+/6ACvgCiigAor7/r4AoAK+/wCivgCgAor7/ooA+AK+/wCvgCvv+gD4Ar7/AKKKAPgCvv8Ar4AooAKK+/6KAPgCvv8Ar4Ar7/oA+AKK+/6+AKAPv+iivgCgAor7/ooA+AKK+/6KAPgCvv8Ar4Ar7/oA+AKKK+/6ACiivgCgD7/oor4AoAK+/wCiigD4Aor7/r4AoAKK+/6KAPgCiivv+gD4Ar7/AK+AK+/6APgCvv8Ar4Ar7/oA+AKKK+/6APgCivv+vgCgAoor7/oA+AK+/wCvgCigAooooAKK+/6KAPgCiiigD7/r4Ar7/ooA+AK+/wCvgCvv+gAr4Ar7/r4AoAKK+/6KAPgCvv8Ar4Ar7/oA+AKK+/6KAPgCivv+igAr4Ar7/ooA+AK+/wCvgCigD7/or4Ar7/oAK+AKKKACivv+vgCgD7/r4AoooAKKK+/6ACvgCiigAooooAK+/wCvgCvv+gAr4Ar7/r4AoAK+/wCivgCgD7/r4Ar7/ooAK+AK+/6+AKAPv+vgCivv+gAor4Ar7/oAK+AK+/6KACivgCvv+gD4Aoor7/oA+AK+/wCvgCigAr7/AK+AKKACvv8Aor4AoA+/6+AK+/6KACvgCiigD7/ooooA+AKK+/6+AKACivv+vgCgD7/ooooA+AKKKKAPv+ivgCvv+gAr4Ar7/r4AoAKKKKAPv+iivgCgAor7/r4AoAKKK+/6ACiivgCgAr7/AKK+AKACiivv+gD4Ar7/AKKKACvgCivv+gD4Ar7/AKKKACvgCiigAr7/AK+AK+/6ACvgCiigAr7/AKKKACvgCiigAoor7/oAK+AKK+/6APgCivv+vgCgD7/ooooAK+AK+/6+AKACvv8Ar4Ar7/oAK+AKKKAPv+iiigAr4AoooAK+/wCiigAor4Ar7/oA+AK+/wCvgCvv+gAor4Ar7/oAKKK+AKACvv8Ar4Ar7/oAK+AK+/6KAPgCivv+igD4Ar7/AK+AK+/6ACvgCvv+vgCgD7/r4Ar7/ooA+AK+/wCvgCigAor7/r4AoA+/6K+AKKAPv+iiigD4AooooAK+/wCvgCvv+gD4Aoor7/oA+AKK+/6+AKAPv+vgCivv+gAooooA+AKKKKACiivv+gD4Aoor7/oAK+AK+/6+AKAPv+vgCivv+gD4Aor7/r4AoAKKK+/6ACvgCvv+igAr4Ar7/ooAK+AKKKACvv8Ar4Ar7/oAKKK+AKAPv+ivgCigAr7/AK+AK+/6APgCvv8AoooAK+AKK+/6APgCiivv+gD4Aor7/ooA+AKKK+/6APgCvv8AoooAK+AK+/6KACivgCigAooooA+/6KKKAPgCvv8Aor4AoA+/6K+AK+/6ACvgCvv+vgCgAr7/AKK+AKACvv8Ar4AooA+/6K+AKKACivv+igAooooAKKK+AKACvv8Ar4Ar7/oA+AK+/wCiigAr4Ar7/ooA+AK+/wCiigD4Ar7/AKKKACvgCivv+gD4Ar7/AKK+AKAPv+vgCvv+igD4AooooAK+/wCiigD4Aor7/r4AoAKKK+/6APgCivv+igD4Aoor7/oA+AK+/wCvgCvv+gD4Aoor7/oA+AKK+/6KAPgCiiigAr7/AK+AK+/6APgCiivv+gD4Aor7/ooA+AKKK+/6ACiiigAor4Ar7/oA+AKKK+/6APgCivv+igD4Aor7/ooA+AK+/wCvgCvv+gD4Ar7/AK+AK+/6ACiivgCgAoor7/oAK+AKKKAPv+vgCvv+vgCgD7/r4Aor7/oA+AK+/wCvgCvv+gD4Ar7/AKKKAPgCiivv+gD4Aor7/r4AoAKKKKAPv+iivgCgD7/r4Aor7/oAK+AKK+/6APgCvv8Ar4Ar7/oA+AK+/wCiigD4AooooAKK+/6KACiiigD4Ar7/AK+AK+/6APgCiiigAr7/AK+AK+/6APgCvv8Ar4Ar7/oAKK+AKKACivv+vgCgAr7/AKKKACiivgCgD7/oor4AoA+/6+AK+/6KAPgCivv+igD4AooooAKK+/6KACvgCvv+vgCgD7/r4Ar7/ooAKKK+AKACivv+vgCgAr7/AKKKAPgCiiigAoor7/oA+AKKK+/6ACiiigAor4Ar7/oA+AK+/wCvgCvv+gD4Aor7/ooA+AK+/wCiigD4Ar7/AK+AK+/6ACvgCiigD7/r4AoooAK+/wCivgCgD7/oor4AoA+/6+AKKKAPv+iivgCgAr7/AKK+AKACvv8Ar4Ar7/oA+AK+/wCvgCigD7/r4AoooAKKK+/6ACvgCivv+gAor4AooA+/6+AK+/6+AKACvv8Aor4AoAKK+/6+AKACiiigD7/r4Ar7/ooA+AK+/wCiigD4Aor7/ooA+AKK+/6KAPgCivv+igD4Ar7/AKKKACiiigAooooAKKKKAPgCvv8AoooA+AKK+/6KAPgCvv8AoooAK+AK+/6KAPgCvv8AoooAK+AK+/6KAPgCivv+igAr4Ar7/ooAK+AK+/6KACvgCvv+igD4Ar7/AKKKAPgCivv+igD4Aor7/ooA+AK+/wCiigAr4Ar7/ooAKKKKAP/Z'></image></defs></svg><p>(Mã QR GCN)</p></div></div></div></page></body></html>";
        //        string strPathF = dataPDF.Footer ?? "";//@"<html><head><meta http-equiv='”Content-Type”' content='”text/html;' charset='utf-16″'><meta name='viewport' content='width=device-width'><style>body{background: rgb(255,255,255);}page{background: white; display: block; margin: 0 auto;}page[size='A4']{width: 21cm; height: 29.7cm; min-width: 21cm; min-height: 29.7cm; max-width: 21cm; max-height: 29.7cm; font-size: 0.32cm;}page[size='A4'][layout='portrait']{width: 29.7cm; height: 23cm;}@media print{body, page{margin: 0; box-shadow: 0; padding: 0;}}.page-content{padding: 0cm 0cm;position: relative;height: 100%;background-repeat: no-repeat; background-attachment: local; background-position: bottom; background-size: contain; background-image: url('');}.logo svg{width: 2cm;height: 2cm;}.footer{position: absolute; left: 0; margin-top: 0px; padding-left: 0cm;}h1,p,span{}p{margin:0px; line-height: 0.55cm; margin-top: 0.2cm;}.gradient-footer{background: rgb(187,215,83);background: linear-gradient(293deg, rgba(187,215,83,1) 0%, rgba(0,109,49,1) 100%);-webkit-print-color-adjust: exact; color: #ffffff;padding: 13px;width: 100%;font-size: 13px;display: flex;margin-bottom: 0px;}.gradient-footer .diva{width: 50%;display: flex;}.gradient-footer .divb{width: 30%;}.gradient-footer .divc{width: 33%;text-align: center;}.noted{margin: 0 auto;width: 21cm;padding: 0cm 0cm;}@media screen{div.divFooter{display: block;}}@media print{div.divFooter{width: 100%; position: fixed; bottom: 0; left: 0;}}</style></head><body style='margin:0px'><div class='divFooter'><div class='gradient-footer'><div class='diva'>Tòa nhà Abacus, 58 Nguyễn Đình Chiểu, Quận 1, Tp. Hồ Chí Minh</div><div class='diva'><div class='divb'>T (+84) 28 3528 2999</div><div class='divb'>F (+84) 28 3910 2888</div><div class='divc'>www.hdinsurance.com.vn</div></div></div></div></body></html>";
        //        string strOrien = "";
        //        //string outputFile = @"new222222212";
        //        string strPathB = dataPDF.Body;
        //        string strGuiID_File = Guid.NewGuid().ToString("N");
        //        string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=" + strGuiID_File;
        //        strOrien = Uglify.Html(strOrien).Code;
        //        strPathB = Uglify.Html(strPathB).Code;
        //        strPathF = Uglify.Html(strPathF).Code;
        //        strPathH = Uglify.Html(strPathH).Code.Replace("@QR_CODE@", getQrCode(strQrHeader, "Q", 8, ""));
        //        goBussinessPDF cls = new goBussinessPDF();
        //        byte[] dataByte64 = cls.convertPageToPDFPersnalByte64(strPathH, strPathB, strPathF, "", "", "", strOrien);
        //        //System.IO.File.WriteAllBytes(GoUtility.Instance.GetPathProject() + @"/StorePDF/" + outputFile + ".pdf", dataByte64);
        //        //WebClient client = new WebClient();
        //        //client.DownloadString(@"file:///C:/Users/tvbth/OneDrive/Desktop/new2.html");
        //        //HtmlMinifier htmlMinifier = new();
        //        string strNameFile = strGuiID_File;
        //        string strUrl = "";
        //        DataSign dataSign = JsonConvert.DeserializeObject<DataSign>(JsonConvert.SerializeObject(dataActionPDF.DataSign));
        //        if (dataSign.IsSign)
        //        {
        //            SignFilePDF(dataSign.SignatureInfo, dataByte64, ref strUrl, ref strNameFile);
        //        }
        //        else
        //        {
        //            bool bUpload = goBusinessCommon.Instance.uploadFileServer(dataByte64, strGuiID_File + ".pdf", "",
        //                                                        null, ref strNameFile, ref strUrl, strGuiID_File);
        //            for (int i = 0; i < 5; i++)
        //            {
        //                if (!bUpload)
        //                {
        //                    Thread.Sleep(2000);
        //                    bUpload = goBusinessCommon.Instance.uploadFileServer(dataByte64, strGuiID_File + ".pdf", "",
        //                                                        null, ref strNameFile, ref strUrl, strGuiID_File);
        //                    if (bUpload)
        //                    {
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    break;
        //                }
        //            }
        //            if (!bUpload)
        //            {
        //                return null;
        //            }
        //        }
        //        DataEmail dataEmail = JsonConvert.DeserializeObject<DataEmail>(JsonConvert.SerializeObject(dataActionPDF.DataEmail));
        //        if (dataEmail != null && dataEmail.IsSendEmail)
        //        {
        //            new Task(() => {
        //                SendMail(dataEmail, strNameFile, strNameFile);
        //            }).Start();
        //        }
        //        DataActionPDF a = new DataActionPDF
        //        {
        //            LinkPDF = strUrl,
        //            Key = (strUrl != "") ? "Success" : "Error"
        //        };
        //        return a;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}
        //void SendMail(DataEmail dataEmail, string filePath, string fileName)
        //{
        //    try
        //    {
        //        string host = "", user = "", pass = "", nameEmail = "";
        //        bool enableSsl = false;
        //        int port = 0;
        //        ServiceEmailModel emailModel = new ServiceEmailModel();
        //        List<EmailModel> lsEmail = new List<EmailModel>();
        //        List<EmailModel> lsEmailCC = new List<EmailModel>();
        //        List<EmailModel> lsEmailBBC = new List<EmailModel>();
        //        foreach (string str in dataEmail.MailInfo.SendTo)
        //        {
        //            EmailModel eMail = new EmailModel();
        //            eMail.email = str;
        //            lsEmail.Add(eMail);
        //        }
        //        if (dataEmail.MailInfo.CC != null && dataEmail.MailInfo.CC.Count > 0)
        //        {
        //            foreach (string str in dataEmail.MailInfo.CC)
        //            {
        //                EmailModel eMail = new EmailModel();
        //                eMail.email = str;
        //                lsEmailCC.Add(eMail);
        //            }
        //        }
        //        if (dataEmail.MailInfo.BBC != null && dataEmail.MailInfo.BBC.Count > 0)
        //        {
        //            foreach (string str in dataEmail.MailInfo.BBC)
        //            {
        //                EmailModel eMail = new EmailModel();
        //                eMail.email = str;
        //                lsEmailBBC.Add(eMail);
        //            }
        //        }
        //        emailModel.to = lsEmail;
        //        emailModel.cc = lsEmailCC;
        //        emailModel.bcc = lsEmailBBC;
        //        emailModel.content = dataEmail.MailInfo.Content;
        //        emailModel.subject = dataEmail.MailInfo.Subject;
        //        FileModel file = new FileModel();
        //        emailModel.file = new List<FileModel>();
        //        file.path = filePath;
        //        file.name = fileName;
        //        emailModel.file.Add(file);
        //        goBussinessEmail.Instance.GetConfigEmail(ref host, ref port, ref enableSsl, ref user, ref pass, ref nameEmail);
        //        goBussinessEmail.Instance.SendEmail(host, port, enableSsl, user, pass, nameEmail, emailModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //void SignFilePDF(SignatureInfo dataSign, byte[] dataPDF, ref string strUrl, ref string nameFile)
        //{
        //    float x1 = 0, y1 = 0, width1 = 0, height1 = 80;
        //    int ipage = 0;
        //    LibSign clsSign = new LibSign(goUtility.Instance.GetPathProject());
        //    TempConfigModel template = goBusinessServices.Instance.get_all_template(dataSign.ORG_CODE, "", dataSign.PRODUCT_CODE, dataSign.PACK_CODE, "GCN", "CreateAndPay", "WEB", "", "");
        //    string image_sign = "";
        //    switch (dataSign.Image)
        //    {
        //        case "M":
        //            image_sign = template.TEMP_SIGN.M_IMAGE;
        //            break;
        //        case "S":
        //            image_sign = template.TEMP_SIGN.S_IMAGE;
        //            break;
        //        case "F":
        //            image_sign = template.TEMP_SIGN.F_IMAGE;
        //            break;
        //    }
        //    bool check = clsSign.findTextInPdfBatch(dataSign.FindTextInPdf, dataPDF, ref x1, ref y1, ref width1, ref ipage);
        //    byte[] p12Byte = Convert.FromBase64String(template.TEMP_SIGN.DATA_FILE);
        //    string strCerBasse64 = template.TEMP_SIGN.CER_FILE;
        //    SignFileModel infoSign = new SignFileModel();
        //    infoSign.detail = new SignFileDetail();
        //    infoSign.PROVIDER = "HDI";
        //    infoSign.DEVICE = "HDI";
        //    infoSign.CER = strCerBasse64;
        //    infoSign.FILE_SIGN = Convert.ToBase64String(dataPDF);
        //    infoSign.F12_FILE = p12Byte;
        //    infoSign.detail.X = ((short)x1 - dataSign.SangTraiX).ToString();//sang bên trái x
        //    infoSign.detail.Y = (((short)y1) - dataSign.XuongDuoiY).ToString();//xuống dưới y
        //    infoSign.detail.WIDTH = ((short)width1 + 2 * dataSign.SangTraiX).ToString();
        //    infoSign.detail.HEIGHT = (height1 + dataSign.SangTraiX).ToString();
        //    infoSign.detail.CONTACT = "";
        //    infoSign.detail.DISPLAY_TYPE = "IMAGE";
        //    infoSign.detail.IMAGE = image_sign;
        //    infoSign.detail.LOCATION = "Thành phố Hồ Chí Minh";
        //    infoSign.detail.MESSAGE = "";
        //    infoSign.detail.PAGE = ipage.ToString();
        //    infoSign.detail.REASON = "Cấp giấy chứng nhận Bảo hiểm";
        //    infoSign.detail.FIELD_NAME = nameFile;
        //    if (check)
        //    {
        //        //thực hiện ký số
        //        string strFileKy = clsSign.signPDF(infoSign, template.TEMP_SIGN.PASSWORD, "");
        //        bool bUpload;
        //        byte[] fileSigned = Convert.FromBase64String(strFileKy);
        //        //thực hiện đẩy file ký lên server
        //        bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, nameFile + ".pdf", "",
        //                                                null, ref nameFile, ref strUrl, nameFile);
        //        for (int i = 0; i < 5; i++)
        //        {
        //            if (!bUpload)
        //            {
        //                Thread.Sleep(2000);
        //                bUpload = goBusinessCommon.Instance.uploadFileServer(fileSigned, nameFile + ".pdf", "",
        //                                                null, ref nameFile, ref strUrl, nameFile);
        //                if (bUpload)
        //                {
        //                    break;
        //                }
        //            }
        //            else
        //            {
        //                break;
        //            }
        //        }
        //    }
        //}
        //#endregion
        #endregion
    }
}
