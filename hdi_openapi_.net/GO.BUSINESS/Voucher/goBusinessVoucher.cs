﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Service;
using GO.DTO.Voucher;
using GO.HELPER.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using GO.BUSINESS.Action;
using GO.DTO.Orders;
using GO.BUSINESS.Orders;
using GO.DTO.Insurance;
using Newtonsoft.Json.Linq;
using GO.BUSINESS.Service;
using System.Net.NetworkInformation;

namespace GO.BUSINESS.Voucher
{
    public class goBusinessVoucher
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessVoucher> _instance = new Lazy<goBusinessVoucher>(() =>
        {
            return new goBusinessVoucher();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessVoucher Instance { get => _instance.Value; }
        #endregion
        #region Method
        #region Voucher Discount 100% Insur
        public BaseResponse VoucherOtpSend(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                object[] goParam = null;
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

                SendOtpModel info = new SendOtpModel();
                info.PHONE = goParam[1].ToString();
                info.CAMPAIGN_CODE = goParam[0].ToString();
                info.OTP_TYPE = goConstants.OTP_TYPE.VOUCHER.ToString();
                info.OTP_SIZE = 4;
                info.EXP_SECONDS = 300;
                info.ENV_CODE = config.env_code;
                info.USER_CREATE = goParam[1].ToString();
                if (!goBusinessCommon.Instance.DDOS_OTP(info, 3, 3600))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_OTP_01), goConstantsError.Instance.ERROR_OTP_01);
                }
                else
                {
                    response = goBusinessCommon.Instance.SendOtp(info);
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_01), goConstantsError.Instance.ERROR_VOUCHER_01);
            }
            return response;
        }

        public BaseResponse VoucherInsurFree(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "request order voucher: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                // Kiểm tra request
                VoucherInsurFree voucherInsur = new VoucherInsurFree();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    voucherInsur = JsonConvert.DeserializeObject<VoucherInsurFree>(request.Data.ToString());
                if (voucherInsur == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_02), goConstantsError.Instance.ERROR_VOUCHER_02);
                    return response;
                }
                // Xác thực OTP
                int otpStatus = goBusinessCommon.Instance.VerifyOtp(voucherInsur.phone, voucherInsur.otp);
                if (otpStatus == 1)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "OTP Không chính xác", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }
                if (otpStatus == 2)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "OTP hết hiệu lực", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }
                if (otpStatus == -1)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "Lỗi xác thực OTP", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }

                // Lấy thông tin gán bảo hiểm
                BaseRequest req_InfoVoucher = request;
                object param = GetParamsInfoVoucher(voucherInsur);
                req_InfoVoucher.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                BaseResponse res_InfoVoucher = goBusinessAction.Instance.GetBaseResponse(req_InfoVoucher);
                VoucherInfo voucherInfo = goBusinessCommon.Instance.GetData<VoucherInfo>(res_InfoVoucher, 0).FirstOrDefault();

                if (voucherInfo == null || !voucherInfo.discount.Equals(100) || !voucherInfo.discount_unit.Equals(goConstants.UnitMode.P.ToString()))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_04), goConstantsError.Instance.ERROR_VOUCHER_04);
                    return response;
                }
                if (voucherInfo.age_insured < voucherInfo.age_from || voucherInfo.age_insured > voucherInfo.age_to)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_05), goConstantsError.Instance.ERROR_VOUCHER_05);
                    return response;
                }
                voucherInfo.channel = voucherInsur.channel;

                // Get Orders
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrdersByVoucherFree(voucherInsur, voucherInfo);
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "save order voucher: " + JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
                if (response.Success)
                {
                    try
                    {
                        OrderResInfo resInfoOrder = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                        // update user used voucher
                        try
                        {
                            BaseRequest request_used = request;
                            request_used.Action.ActionCode = goConstantsProcedure.UPD_VOUCHER_USED;
                            object Param = new
                            {
                                channel = voucherInsur.channel,
                                username = "",
                                activation_code = voucherInfo.activation_code,
                                activate_by = voucherInsur.phone,
                                address = voucherInsur.address,
                                email = voucherInsur.email,
                                phone = voucherInsur.phone,
                                order_code = resInfoOrder.order_code
                            };
                            request_used.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(Param));
                            BaseResponse re = goBusinessAction.Instance.GetBaseResponse(request_used);
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Update voucher used: " + JsonConvert.SerializeObject(re), Logger.ConcatName(nameClass, nameMethod));
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Update voucher used: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                        }

                        // send mail
                        try
                        {
                            if (resInfoOrder != null && resInfoOrder.status.Equals(nameof(goConstants.OrderStatus.PAID)))
                            {
                                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);
                                //send mail GCN theo insurDetails                            
                                BaseResponse res = goBusinessServices.Instance.signEmail(insurDetails[0]);
                                if (res.Success)
                                {
                                    response.Data = res.Data;
                                }
                                else
                                {
                                    //response.Success = false;
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));
                                    goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails[0]));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "send mail voucher: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_01), goConstantsError.Instance.ERROR_VOUCHER_01);
            }
            return response;
        }
        public BaseResponse signEmail(string transactionNo, string strIDCerCen, string strEmailTo, string strconttractNo, string strTemp_code)
        {
            //thực hiện ký số
            BaseRequest requestSign = new BaseRequest();
            requestSign.Action = new ActionInfo();
            requestSign.Action.ActionCode = "HDI_CER_FILE";
            requestSign.Action.ParentCode = "CENTECH_VN";
            JObject jobSign = new JObject();
            jobSign.Add("USER_NAME", "CENT");
            switch (strTemp_code)
            {
                case "TRAU_DONG":
                    jobSign.Add("TEMP_CODE", "CSSK_D");
                    break;
                case "TRAU_BAC":
                    jobSign.Add("TEMP_CODE", "CSSK_B");
                    break;
                case "TRAU_VANG":
                    jobSign.Add("TEMP_CODE", "CSSK_V");
                    break;
                case "TRAU_BACK_KIM":
                    jobSign.Add("TEMP_CODE", "CSSK_TT");
                    break;
                case "TRAU_KIM_CUONG":
                    jobSign.Add("TEMP_CODE", "CSSK_KC");
                    break;
            }
            jobSign.Add("TYPE_RES", "BASE64");
            jobSign.Add("PARAM_CODE", "NO");
            jobSign.Add("PARAM_VALUE", strconttractNo);
            jobSign.Add("TRANSACTION_ID", transactionNo);
            jobSign.Add("DATA_TYPE", "DATA");
            requestSign.Data = jobSign;
            BaseResponse basePDF = goBussinessPDF.Instance.GetBaseResponse(requestSign);
            if (basePDF.Success)
            {
                JObject jData = JObject.Parse(basePDF.Data.ToString());
                requestSign.Action.ActionCode = "HDI_SIGN";
                requestSign.Action.UserName = "CENTECH_VN";
                requestSign.Action.ParentCode = "CENTECH_VN";
                requestSign.Action.Secret = "B1B12FDEB22977ABE0551F6E21B40329";
                jobSign["USER_NAME"] = "TUANNV";
                jobSign.Add("LOCATION", "Hà Nội");
                jobSign.Add("REASON", "Cấp giấy chứng nhận Bảo hiểm");
                jobSign.Add("TypeResponse", "BASE64");
                jobSign.Add("TextNote", "");
                jobSign.Add("Alert", "");
                jobSign.Add("FILE_REFF", strconttractNo);
                jobSign.Add("TYPE_FILE", "FILE");
                jobSign.Add("FILE", jData["FILE_NAME"].ToString());
                requestSign.Data = jobSign;
                basePDF = goBussinessSign.Instance.GetBaseResponse(requestSign);
                string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
                
                //thực hiện gửi mail
                requestSign.Action.ActionCode = "HDI_EMAIL_01";
                requestSign.Action.UserName = "CENTECH_VN";
                jobSign = new JObject();
                jobSign.Add("subject", "Giấy chứng nhận bảo hiểm HDI cấp");
                JObject jEmailTo = JObject.Parse(" {\"email\":\"" + strEmailTo + "\"}");
                JArray jArrMailTo = new JArray();
                jArrMailTo.Add(jEmailTo);
                jobSign.Add("to", jArrMailTo);
                JObject jData1 = JObject.Parse(basePDF.Data.ToString());
                //thực hiện lấy tạm 1 html cho vào body mail -->phải lấy trong templace

                BaseResponse responseTemp = new BaseResponse();
                responseTemp = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Get_Temp_Value, "LIVE", new string[] { "CENTECH_VN", "EMAIL", "CSSK_TRAU", "GCN", strconttractNo });
                List<JObject> objTempValue = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);
                string strBody = "";
                List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                if (objTempValue == null || objTempValue.Count == 0)
                {
                    strBody = objTemp[0]["DATA"].ToString();
                }
                else
                {
                    strBody = goBussinessPDF.Instance.replaceHTML_JSON(goBussinessPDF.Instance.createJsonKeyValue(objTempValue), objTemp[0]["DATA"].ToString(), lsReplace);
                }
                jobSign.Add("content", strBody);
                jobSign.Add("file", JArray.Parse("[{\"path\" : \"" + jData1["FILE_NAME"].ToString() + "\"}" + ",{\"path\" : \"" + jData1["FILE_ATTACH"]?.ToString() + "\"}" + "]"));
                requestSign.Data = jobSign;
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, basePDF + " Dia chi mail " + strEmailTo, Logger.ConcatName(nameClass, nameMethod));
                new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSign.Action.ParentCode, "CSSK", "Gửi giấy chứng nhận cho khách hàng", "EMAIL", "Chăm sóc sức khỏe",
                    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), strEmailTo, "", "", strBody, "X", "HIGH", "ALL", "abc", "Giấy chứng nhận bảo hiểm HDI cấp", requestSign.Action.UserName)).Start();
                goBussinessEmail.Instance.GetResponseEmail(requestSign);
            }
            return basePDF;
        }
        public object GetParamsInfoVoucher(VoucherInsurFree info)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    channel = info.channel,
                    userName = info.userName,
                    giftCode = info.giftCode,
                    dob = info.dob
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        #endregion
        #endregion
    }
}
