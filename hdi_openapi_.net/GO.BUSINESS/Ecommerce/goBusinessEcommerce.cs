﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.Service;
using GO.BUSINESS.ServiceAccess;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Insurance.Lading.MultiProduct;
using GO.DTO.Insurance.Landing.MultiProduct;
using GO.DTO.Orders;
using GO.DTO.OrderSku;
using GO.DTO.Service;
using GO.ENCRYPTION;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GO.BUSINESS.Ecommerce
{
    public class goBusinessEcommerce
    {

        #region Variables
        private string nameClass = nameof(goBusinessEcommerce);
        #endregion
        #region Priorities
        public goBusinessEcommerce()
        {
        }
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessEcommerce> InitInstance = new Lazy<goBusinessEcommerce>(() => new goBusinessEcommerce());
        public static goBusinessEcommerce Instance => InitInstance.Value;
        #endregion

        #region Method
        // Start 2022-08-22 By ThienTVB
        private class ProductModel
        {
            public string Project { get; set; } = "";
            public string Username { get; set; } = "";

            public object Org_code { get; set; }
        }
        public BaseResponse GetProducts(BaseRequest request)
        {
            BaseResponse response;
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                ProductModel dataReq = JsonConvert.DeserializeObject<ProductModel>(JsonConvert.SerializeObject(request.Data));
                var param = new
                {
                    project = dataReq.Project,
                    username = dataReq.Username,
                    org_code = dataReq.Org_code
                };
                BaseRequest reqUser = request;
                reqUser.Action.ActionCode = "APIW7GQDHZ"; // ActionCode getProducts
                reqUser.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(param));

                response = goBusinessAction.Instance.GetBaseResponse(reqUser);
                IList collection = (IList)response.Data;
                if (collection.Count > 0)
                {
                    response.Data = collection[0];
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        // End 2022-08-22 By ThienTVB
        public BaseResponse getSortLink(BaseRequest request)
        {
            //Truyền thông tin khách hàng, lưu lại gửi sort link sms, email 
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //ĐẨY DỮ LIỆU KHÁCH HÀNG VÀO BẢNG LƯU TRỮ
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jIinput = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                    string strOrgCode = jIinput["ORG_CODE"].ToString();
                    string strOrderCode = jIinput["ORDER_CODE"].ToString();
                    string strEXCHANGE_CODE = jIinput["EXCHANGE_CODE"]?.ToString();
                    string strName = jIinput["BUYER_NAME"].ToString();
                    string strPhone = jIinput["PHONE"].ToString();
                    string strAmount = jIinput["TOTAL_AMOUNT"].ToString();
                    string strEmail = jIinput["EMAIL"]?.ToString();
                    string strContent = jIinput["CONTENT"]?.ToString();
                    string strAddress = jIinput["ADDRESS"]?.ToString();
                    string strCusCode = jIinput["CUS_CODE"]?.ToString();
                    var strDetail = jIinput["DETAIL"];
                    //LẤY THÔNG TIN VỀ SẢN PHẨM TẠO OBJECT SẢN PHẨM LOAD LÊN FORM
                    JArray jArrDetail = JArray.Parse(strDetail.ToString());
                    string strMarCode = "";
                    for (int i = 0; i < jArrDetail.Count; i++)
                    {
                        JObject jDetail = (JObject)jArrDetail[i];
                        strMarCode = jDetail["MAR_CODE"].ToString() + ";";
                    }
                    string strMarCode1 = jArrDetail[0]["MAR_CODE"].ToString();
                    List<JObject> lsShortLinkOrg = new List<JObject>();
                    JObject jObjInsert = new JObject();
                    response = callPackage(goConstantsProcedure.GET_MAR_BY_ORG, strEvm, new object[] { "project", "user_name", strOrgCode, strMarCode });
                    if (response != null)
                    {
                        List<JObject> lsSanPham = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        lsShortLinkOrg = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                        if (lsSanPham != null && lsSanPham.Any() && jArrDetail.Count == lsSanPham.Count)
                        {
                            //Thực hiện khởi tạo object cho hoàng
                            jObjInsert.Add("ORG_CODE", strOrgCode);
                            jObjInsert.Add("CHANNEL", "TMDT");
                            jObjInsert.Add("USERNAME", strOrgCode);
                            JArray jArrData = new JArray();
                            int iTongTien = 0;
                            for (int i = 0; i < jArrDetail.Count; i++)
                            {
                                JObject jObjINSUR = new JObject();
                                JObject jDetail1 = (JObject)jArrDetail[i];
                                foreach (JObject jmarketCT in lsSanPham)
                                {
                                    if (jmarketCT["MAR_CODE"].ToString().Equals(jDetail1["MAR_CODE"].ToString()))
                                    {
                                        iTongTien = iTongTien + Convert.ToInt32(jmarketCT["TOTAL_AMOUNT"].ToString()) * Convert.ToInt32(jDetail1["TOTAL"].ToString());
                                        JObject jObjCTOut = createProperties(jmarketCT["PRODUCT_CODE"].ToString(), Convert.ToInt32(jDetail1["TOTAL"].ToString()), JObject.Parse(jmarketCT["OBJ"].ToString()), jmarketCT["OBJ_KEY"].ToString());
                                        jArrData.Add(jObjCTOut);
                                    }
                                }
                            }
                            //kiểm tra số tiền xem chuẩn chưa
                            if (!strAmount.Equals(iTongTien.ToString()))
                            {
                                goBussinessEmail.Instance.sendEmaiCMS("", "Số tiền không đúng với số lượng sản phẩm đã mua", JsonConvert.SerializeObject(request).ToString());
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                                return response;
                            }
                            jObjInsert.Add("INSUR", jArrData);
                            JObject jBuyer = new JObject();
                            jBuyer.Add("TYPE", "");
                            jBuyer.Add("NAME", strName);
                            jBuyer.Add("DOB", "");
                            jBuyer.Add("GENDER", "");
                            jBuyer.Add("ADDRESS", strAddress);
                            jBuyer.Add("IDCARD", "");
                            jBuyer.Add("EMAIL", strEmail);
                            jBuyer.Add("PHONE", strPhone);
                            jBuyer.Add("PROV", "");
                            jBuyer.Add("DIST", "");
                            jBuyer.Add("WARDS", "");
                            jBuyer.Add("IDCARD_D", "");
                            jBuyer.Add("IDCARD_P", "");
                            jBuyer.Add("FAX", "");
                            jBuyer.Add("TAXCODE", "");
                            jObjInsert.Add("BUYER", jBuyer);
                            JObject jPAY_INFO = new JObject();
                            jBuyer.Add("PAYMENT_TYPE", "TH");
                            jObjInsert.Add("PAY_INFO", jPAY_INFO);
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tìm thấy sản phẩm được phân quyền bán");
                            goBussinessEmail.Instance.sendEmaiCMS("", "Không tìm thấy sản phẩm được phân quyền bán market", JsonConvert.SerializeObject(request).ToString());
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tìm thấy sản phẩm được phân quyền bán");
                        goBussinessEmail.Instance.sendEmaiCMS("", "Không tìm thấy sản phẩm được phân quyền bán market", JsonConvert.SerializeObject(request).ToString());
                        return response;
                    }

                    //đẩy dữ liệu vào bảng khách hàng
                    object[] oInput = new object[] { };
                    response = callPackage(goConstantsProcedure.INSERT_CUS_INFO, strEvm, new object[] { strOrgCode, strCusCode,strName, strPhone,strEmail,
                        strAddress,    strEXCHANGE_CODE,      "CREATE",    "",     strOrderCode,      strAmount,       strContent,      jObjInsert.ToString(),    strDetail });
                    string strRf = "ref_id=";
                    if (response != null)
                    {
                        List<JObject> lsCus = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (lsCus != null && lsCus.Any())
                        {
                            strRf = strRf + lsCus[0]["REF_ID"].ToString() + "&org=" + strOrgCode;
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không đẩy được dữ liệu khách hàng");
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không đẩy được dữ liệu khách hàng", JsonConvert.SerializeObject(request).ToString());
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không đẩy được dữ liệu khách hàng");
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không đẩy được dữ liệu khách hàng", JsonConvert.SerializeObject(request).ToString());
                        return response;
                    }
                    string strLink = goBusinessCommon.Instance.GetConfigDB(goConstants.short_link);
                    //khởi tạo sort link 
                    string strShortCode = "";
                    if (lsShortLinkOrg.Count > 0)
                    {
                        foreach (JObject job in lsShortLinkOrg)
                        {
                            if (job["MAR_CODE"].ToString().Equals(strMarCode1))
                            {
                                strShortCode = job["SHORT_KEY"].ToString();
                            }
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Chưa cấu hình đối tác với link");
                        goBussinessEmail.Instance.sendEmaiCMS("", "Chưa cấu hình đối tác với link", JsonConvert.SerializeObject(request).ToString());
                        return response;
                    }
                    response = callPackage(goConstantsProcedure.CREATE_SHORT_LINK, strEvm, new object[] { "sys", "WEB", strShortCode, strRf, 1, "10/06/2022", "15/06/2022" });
                    if (response != null)
                    {
                        List<JObject> lsLink = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (lsLink != null && lsLink.Any())
                        {
                            strLink = strLink + lsLink[0]["SHORT_CODE"].ToString();
                            JObject jOut = new JObject();
                            jOut.Add("LINK", strLink);
                            response.Data = jOut;
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tạo được short link");
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                        response.ErrorMessage = "Không tạo được short link";
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                        return response;
                    }

                    //send sms
                    TempConfigModel template = goBusinessServices.Instance.get_all_template(strOrgCode, "STRUCT_HDI", "ONPOINT", "ONPOINT",
                    "SMS", "ONPOINT", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                    if (template != null && template.TEMP_SMS != null)
                    {
                        template.PRODUCT_CODE = "ONPOINT";
                        template.ORG_CODE = "ONPOINT";
                        template.PACK_CODE = "ONPOINT";
                        template.TEMP_CODE = "ONPOINT";
                        string strContentSMS = template.TEMP_SMS[0].DATA;
                        strContentSMS = strContentSMS.Replace("LINK", strLink);
                        strContentSMS = goBussinessPDF.Instance.clearParram(strContentSMS);
                        bool b = goBussinessSMS.Instance.sendSMSSystem(template, strPhone, strContentSMS, "ONPOINT", strOrderCode);
                        if (b)
                        {
                            response.Success = true;
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                            response.ErrorMessage = "Không gửi được sms cho khách hàng";
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không gửi được sms cho khách hàng", JsonConvert.SerializeObject(request).ToString());
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                        response.ErrorMessage = "Không lấy được mẫu sms";
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không lấy được mẫu sms", JsonConvert.SerializeObject(request).ToString());
                        return response;
                    }
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce 119", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }


        public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        {
            try
            {
                ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
                return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
            }
            catch (Exception)
            {

                return null;
            }

        }
        public JObject createCusInfo(JObject jInput)
        {
            JObject jOutPut = new JObject();
            try
            {
                return jOutPut;
            }
            catch (Exception ex)
            {

                return null;
            }
        }
        public JObject createProperties(string strProduct, int iSoLuong, JObject jMarObject, string strKeyObj)
        {
            try
            {
                JArray jarrObj = JArray.Parse(jMarObject[strKeyObj].ToString());
                JObject jOutPut = new JObject();
                jOutPut.Add("PRODUCT_CODE", strProduct);
                jOutPut.Add("ACTION", jMarObject["ACTION"].ToString());
                JArray jarr_INSUR = new JArray();
                for (int i = 0; i < iSoLuong; i++)
                {
                    jarr_INSUR.Add(jarrObj[0]);
                }
                jOutPut.Add("DATA", jarr_INSUR);
                return jOutPut;
            }
            catch (Exception ex)
            {

                return null;
            }


        }

        public BaseResponse OrgSkuCreateOrder(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                LadingMasterInfo ladingMaster = new LadingMasterInfo();
                List<LandingDetailInfo> landingDetails = new List<LandingDetailInfo>();

                BaseResponse resSave = new BaseResponse();
                List<ProductMarket> lstProdMarket = new List<ProductMarket>();
                OrgShortLink shortlink = new OrgShortLink();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string OrgCode = "";
                string strPhone = "";
                string strOrderCode = "";
                string strEmail = "";
                switch (config.partnerCode)
                {
                    case nameof(goConstants.Org_Code.ONPOINT):
                        InputOnpoint inputOnpoint = JsonConvert.DeserializeObject<InputOnpoint>(JsonConvert.SerializeObject(request.Data));
                        string strMarCode = "";
                        OrgCode = inputOnpoint.ORG_CODE;
                        strPhone = inputOnpoint.PHONE;
                        strOrderCode = inputOnpoint.ORDER_CODE;
                        strEmail = inputOnpoint.EMAIL;
                        if (inputOnpoint.DETAIL == null || !inputOnpoint.DETAIL.Any())
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                            return response;
                        }

                        foreach (var item in inputOnpoint.DETAIL)
                        {
                            if (string.IsNullOrEmpty(strMarCode))
                                strMarCode = item.MAR_CODE;
                            else
                                strMarCode += ";" + item.MAR_CODE;
                        }
                        var param = new
                        {
                            channel = "",
                            user = "",
                            org = OrgCode,
                            strMark = strMarCode
                        };
                        BaseRequest reqMark = request;
                        reqMark.Action.ActionCode = goConstantsProcedure.GET_MAR_BY_ORG_V2;
                        reqMark.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                        BaseResponse res = goBusinessAction.Instance.GetBaseResponse(reqMark);
                        if (res.Success)
                        {
                            lstProdMarket = goBusinessCommon.Instance.GetData<ProductMarket>(res, 0);
                            shortlink = goBusinessCommon.Instance.GetData<OrgShortLink>(res, 1).FirstOrDefault();

                            double amountSv = 0;
                            BaseValidate validate = MappDataOnpoint(ref ladingMaster, ref landingDetails, ref amountSv, inputOnpoint, lstProdMarket);
                            if (!validate.Success)
                            {
                                goBussinessEmail.Instance.sendEmaiCMS("", "Sản phẩm truyền sang không đúng !", JsonConvert.SerializeObject(request).ToString());
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", validate.Error, validate.ErrorMessage);
                                return response;
                            }
                            if (!inputOnpoint.TOTAL_AMOUNT.Equals(amountSv))
                            {
                                goBussinessEmail.Instance.sendEmaiCMS("", "Số tiền không đúng với số lượng sản phẩm đã mua", JsonConvert.SerializeObject(request).ToString());
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_6010), goConstantsError.Instance.ERROR_6010);
                                return response;
                            }

                            resSave = SaveObjLanding(ladingMaster, landingDetails, config, request);
                        }
                        else
                        {
                            response = res;
                            return response;
                        }
                        // tạo shortlink khi lưu đơn thành công
                        if (resSave.Success)
                        {
                            JObject jObj = goBusinessCommon.Instance.GetData<JObject>(resSave, 0).FirstOrDefault();
                            string strRf = "ref_id=" + jObj["OSKU_CODE"] + "&org=" + OrgCode;
                            var paramSL = new
                            {
                                channel = "sys",
                                username = "WEB",
                                short_key = shortlink.SHORT_KEY,
                                param = strRf,
                                is_limit = 1,
                                eff = DateTime.Now.ToString("dd/MM/yyyy"),
                                exp = DateTime.Now.AddDays(60).ToString("dd/MM/yyyy")
                            };
                            BaseRequest reqSL = request;
                            reqSL.Action.ActionCode = goConstantsProcedure.CREATE_SHORT_LINK;
                            reqSL.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(paramSL));
                            string strLink = goBusinessCommon.Instance.GetConfigDB(goConstants.short_link);
                            //BaseResponse resShort = goBusinessAction.Instance.GetBaseResponse(reqSL);
                            BaseResponse resShort = callPackage(goConstantsProcedure.CREATE_SHORT_LINK, strEvm, new object[] { "sys", "WEB", shortlink.SHORT_KEY, strRf, 1, DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.AddDays(60).ToString("dd/MM/yyyy") });
                            if (resShort.Success)
                            {
                                // tạo shortlink xong
                                List<JObject> lsLink = goBusinessCommon.Instance.GetData<JObject>(resShort, 0);
                                if (lsLink != null && lsLink.Any())
                                {
                                    strLink = strLink + lsLink[0]["SHORT_CODE"].ToString();
                                    JObject jOut = new JObject();
                                    //jOut.Add("LINK", strLink);
                                    jOut.Add("LINK", "");
                                    response.Data = jOut;
                                }
                                else
                                {
                                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tạo được short link");
                                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                                    return response;
                                }
                            }
                            else
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                                response.ErrorMessage = "Không tạo được short link";
                                goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                                return response;
                            }

                            //send sms
                            TempConfigModel template = goBusinessServices.Instance.get_all_template(OrgCode, "STRUCT_HDI", "ONPOINT", "ONPOINT",
                            "SMS", "ONPOINT", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                            if (template != null && template.TEMP_SMS != null)
                            {
                                template.PRODUCT_CODE = "ONPOINT";
                                template.ORG_CODE = "ONPOINT";
                                template.PACK_CODE = "ONPOINT";
                                template.TEMP_CODE = "ONPOINT";
                                string strContentSMS = template.TEMP_SMS[0].DATA;
                                strContentSMS = strContentSMS.Replace("@LINK@", strLink);
                                strContentSMS = goBussinessPDF.Instance.clearParram(strContentSMS);
                                bool b = goBussinessSMS.Instance.sendSMSSystem(template, strPhone, strContentSMS, "ONPOINT", strOrderCode);
                                if (b)
                                {
                                    response.Success = true;
                                    //lên live phải bỏ đi
                                    //if (!strEvm.Equals("LIVE"))
                                    //{
                                    //if (!string.IsNullOrEmpty(strEmail))
                                    //    goBussinessEmail.Instance.sendEmaiCMS(strEmail, "Thông tin sms gửi cho khách hàng live ", strContentSMS);
                                    //new Task(() => callBackSMS(OrgCode, strOrderCode)).Start();
                                    //}

                                }
                                else
                                {
                                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                                    response.ErrorMessage = "Không gửi được sms cho khách hàng";
                                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không gửi được sms cho khách hàng", JsonConvert.SerializeObject(request).ToString());
                                    return response;
                                }
                            }
                            else
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                                response.ErrorMessage = "Không lấy được mẫu sms";
                                goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không lấy được mẫu sms", JsonConvert.SerializeObject(request).ToString());
                                return response;
                            }
                        }
                        break;
                    case nameof(goConstants.Org_Code.GALAXY_CONNECT):
                        // Galaxy connect
                        InputGalaxyConnect inputGalaxyPay = JsonConvert.DeserializeObject<InputGalaxyConnect>(JsonConvert.SerializeObject(request.Data));
                        var paramMar = new
                        {
                            p_offer_code = inputGalaxyPay.offerCode,
                            p_provider_code = ""
                        };
                        BaseRequest reqMar = request;
                        //reqMar.Action.ActionCode = "GET_CREDIFY_CONFIG";
                        reqMar.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(paramMar));
                        BaseResponse resMar = goBusinessAction.Instance.GetBaseResponse(reqMar);
                        if (resMar.Success)
                        {
                            List<MarProduct> lstMar = goBusinessCommon.Instance.GetData<MarProduct>(resMar, 0);
                            MarProduct mar = new MarProduct();
                            if (lstMar != null && lstMar.Count > 0)
                            {
                                mar = lstMar[0];
                                OrgCode = config.partnerCode;
                                // INSERT_CUSINFO
                                string p_lname = "";
                                string p_fname = "";
                                try
                                {
                                    p_lname = inputGalaxyPay.customerInfo?.name.Substring(inputGalaxyPay.customerInfo.name.IndexOf(" ") + 1);
                                }
                                catch (Exception)
                                {
                                    p_lname = "";
                                }
                                try
                                {
                                    p_fname = inputGalaxyPay.customerInfo?.name.Substring(0, inputGalaxyPay.customerInfo.name.IndexOf(" "));
                                }
                                catch (Exception)
                                {
                                    p_fname = "";
                                }
                                var paramCusInfo = new
                                {
                                    p_openid = inputGalaxyPay.transactionId,
                                    p_orgcode = OrgCode,
                                    p_fname,
                                    p_lname,
                                    p_phone = inputGalaxyPay.customerInfo?.phone,
                                    p_email = inputGalaxyPay.customerInfo?.email,
                                    p_address = inputGalaxyPay.customerInfo?.address,
                                    p_private_key = "",
                                    p_transaction_id = inputGalaxyPay.transactionId,
                                    p_exchange_code = "",
                                    p_obj_product = "",
                                    p_product_code = mar.PRODUCT_CODE
                                };
                                BaseRequest reqCusInfo = request;
                                reqMar.Action.ActionCode = "INSERT_CUSINFO";
                                reqMar.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(paramCusInfo));
                                BaseResponse resCusInfo = goBusinessAction.Instance.GetBaseResponse(reqCusInfo);
                                if (resCusInfo.Success)
                                {
                                    // var lstRef_id = GoBusinessCommon.Instance.GetData<object>(resCusInfo, 0);
                                    JObject jObj = goBusinessCommon.Instance.GetData<JObject>(resCusInfo, 0).FirstOrDefault();
                                    int ref_id = (int)jObj["REFID"];
                                    shortlink = goBusinessCommon.Instance.GetData<OrgShortLink>(resCusInfo, 1).FirstOrDefault();
                                    // tạo shortlink khi lưu đơn thành công
                                    if (ref_id > 0)
                                    {
                                        string strRf = "ref_id=" + ref_id + "&org=" + OrgCode;
                                        var paramSL = new
                                        {
                                            channel = "sys",
                                            username = "WEB",
                                            short_key = shortlink.SHORT_KEY,
                                            param = strRf,
                                            is_limit = 1,
                                            eff = DateTime.Now.ToString("dd/MM/yyyy"),
                                            exp = DateTime.Now.AddDays(60).ToString("dd/MM/yyyy")
                                        };
                                        BaseRequest reqSL = request;
                                        reqSL.Action.ActionCode = goConstantsProcedure.CREATE_SHORT_LINK;
                                        reqSL.Data = JsonConvert.DeserializeObject<object>(JsonConvert.SerializeObject(paramSL));
                                        string strLink = goBusinessCommon.Instance.GetConfigDB(goConstants.short_link);
                                        //BaseResponse resShort = goBusinessAction.Instance.GetBaseResponse(reqSL);
                                        BaseResponse resShort = callPackage(goConstantsProcedure.CREATE_SHORT_LINK, strEvm, new object[] { "sys", "WEB", shortlink.SHORT_KEY, strRf, 1, DateTime.Now.ToString("dd/MM/yyyy"), DateTime.Now.AddDays(60).ToString("dd/MM/yyyy") });
                                        if (resShort.Success)
                                        {
                                            // tạo shortlink xong
                                            List<JObject> lsLink = goBusinessCommon.Instance.GetData<JObject>(resShort, 0);
                                            if (lsLink != null && lsLink.Any())
                                            {
                                                strLink += lsLink[0]["SHORT_CODE"].ToString();
                                                JObject jOut = new JObject();
                                                jOut.Add("ShortLink", strLink);
                                                jOut.Add("TransactionId ", inputGalaxyPay.transactionId);
                                                //jOut.Add("LINK", "");
                                                response.Data = jOut;
                                                response.Success = true;
                                            }
                                            else
                                            {
                                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tạo được short link");
                                                goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                                                return response;
                                            }
                                        }
                                        else
                                        {
                                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Số tiền không đúng với số lượng sản phẩm đã mua");
                                            response.ErrorMessage = "Không tạo được short link";
                                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", JsonConvert.SerializeObject(request).ToString());
                                            return response;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Không tạo được short link");
                                goBussinessEmail.Instance.sendEmaiCMS("", "Không get được list MarProduct" + inputGalaxyPay.offerCode, JsonConvert.SerializeObject(request).ToString());
                                return response;
                            }
                        }
                        break;
                    // Galaxy connect
                    default:
                        // code block
                        response.Success = false;
                        response.Data = null;
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "OrgCode không tồn tại: " + config.partnerCode);
                        response.ErrorMessage = "OrgCode không tồn tại: " + config.partnerCode;
                        break;
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }

        public void callBackSMS(string strOrgCode, string strJson, string strFunc= "confirm")
        {
            
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                //Thread.Sleep(30 * 1000);
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn movi sms " + strOrgCode, Logger.ConcatName(nameClass, nameMethod));
                switch (strOrgCode)
                {
                    case ("ONPOINT"):
                        //JObject jInput = JObject.Parse("{\"order_no\":\"" + strOderCode + "\"}");
                        //string strJson = "{\"order_no\":\"" + strOderCode + "\"}";
                        //jInput.Add("order_no", strOderCode);
                        switch (strFunc)
                        {
                            case "confirm":
                                strJson = "{\"order_no\":\"" + strJson + "\"}";
                                break;
                            case "item_expired":                                
                                break;
                            case "expired":
                                strJson = "{\"order_no\":\"" + strJson + "\"}";
                                break;
                            case "ready_to_ship":
                                strJson = "{\"order_no\":\"" + strJson + "\"}";
                                break;
                                
                        }


                        var timestampUpdate = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
                        string strUrlIpn = "";
                        string strParam = goPaymentLib.Instance.getLink_CallBack(strOrgCode, ref strUrlIpn);
                        if (string.IsNullOrEmpty(strParam))
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lay ipn onpoint " + strOrgCode + " - " + strJson, Logger.ConcatName(nameClass, nameMethod));
                        }
                        strUrlIpn = strUrlIpn + strFunc;
                        List<string> lsStrParam = strParam.Split(';').ToList();
                        string strKey = lsStrParam[0].Replace("aaaA", ";");
                        string strSecret = lsStrParam[1].Replace("aaaA", ";");
                        JObject jHeader = new JObject();
                        jHeader.Add("x-api-key", strKey);
                        jHeader.Add("x-timestamp", timestampUpdate.ToString());
                        string strSign = goEncryptMix.Instance.encryptHmacSha256("."+ strJson + "." + timestampUpdate.ToString(), strSecret);
                        jHeader.Add("x-signature", strSign.ToUpper());
                        HttpResponseMessage response = postJsonAPI(strJson, strUrlIpn, jHeader);
                        string message = response.Content.ReadAsStringAsync().Result;                        
                        string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                        if (!(parsedString.IndexOf("SUCCESS") > 1))
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn movi sms " + message, Logger.ConcatName(nameClass, nameMethod));
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt movi ", parsedString + "-----------" + strJson);
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn movi sms sucssecc " + message, Logger.ConcatName(nameClass, nameMethod));
                        }
                        //gửi thông tin call back cho Thu sau zem lại                        
                        //if(!goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT).Equals("LIVE"))
                        //    goBussinessEmail.Instance.sendEmaiCMS("ithdi2021@gmail.com", "Đã callback về cho onpint kết quả onpoint trả về là: ", parsedString + " ----------- dữ liệu truyền sang onpoint là" + jInput.ToString());
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR,"Lỗi cập nhật onpoint "+ ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt movi ", ex.Message);
                //throw;
            }
        }
        public HttpResponseMessage postJsonAPI(string JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, strUrl);
                request.Content = new StringContent(JInput,
                                                    Encoding.UTF8,
                                                    "application/json");
                HttpResponseMessage response = client.SendAsync(request).Result;
                //HttpResponseMessage response = client.PostAsJsonAsync(strUrl, request.Content).Result;
                return response;
            }
            catch (Exception ex)
            {
                throw new Exception("Service timeout " + ex.ToString());
            }

        }
        #region Các đối tác
        private BaseValidate MappDataOnpoint(ref LadingMasterInfo ladingMaster, ref List<LandingDetailInfo> landingDetails, ref double totalAmount, InputOnpoint inputOnpoint, List<ProductMarket> lstProdMarket)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            BaseValidate baseValidate = new BaseValidate();
            baseValidate.Success = true;
            try
            {
                string ORG_SELLER = inputOnpoint.ORG_CODE, CHANNEL = inputOnpoint.EXCHANGE_CODE;

                MasterOrders masterOrders = new MasterOrders();
                masterOrders.ORDER_ORG = inputOnpoint.ORDER_CODE;
                masterOrders.ORG_SELLER = ORG_SELLER;
                masterOrders.CHANNEL = CHANNEL;
                masterOrders.COUNT_PRODUCT = inputOnpoint.DETAIL.Count;
                masterOrders.TOTAL_AMOUNT = inputOnpoint.TOTAL_AMOUNT;
                masterOrders.DESCRIPTION = inputOnpoint.CONTENT;
                masterOrders.DATE_PAY = inputOnpoint.DATE_PAY;
                masterOrders.STATUS = goConstants.OrderStatus.PAID.ToString();

                ORD_BUYER buyer = new ORD_BUYER();
                buyer.CUS_ID = inputOnpoint.CUS_CODE;
                buyer.NAME = inputOnpoint.BUYER_NAME;
                buyer.PHONE = inputOnpoint.PHONE;
                buyer.EMAIL = inputOnpoint.EMAIL;
                buyer.ADDRESS = inputOnpoint.ADDRESS;

                List<LadingProduct> lstProduct = new List<LadingProduct>();
                List<LandingDetailInfo> lstDetails = new List<LandingDetailInfo>();

                foreach (var item in inputOnpoint.DETAIL)
                {
                    ProductMarket prodMarket = lstProdMarket.Where(i => i.MAR_CODE.Equals(item.MAR_CODE)).FirstOrDefault();
                    if (prodMarket == null)
                    {
                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_28), goConstantsValidate.ERR_CHK_28);
                    }
                    string category = prodMarket.CATEGORY, productCode = prodMarket.PRODUCT_CODE, productName = prodMarket.MAR_NAME;

                    LadingProduct productInfo = new LadingProduct();
                    productInfo.MAR_CODE = prodMarket.MAR_CODE;
                    productInfo.CATEGORY = category;
                    productInfo.PRODUCT_CODE = productCode;
                    productInfo.PRODUCT_NAME = productName;
                    //productInfo.PRODUCT_NAME = prodMarket.PRODUCT_NAME;
                    productInfo.PACK_CODE = prodMarket.PACK_CODE;
                    productInfo.COUNT = item.TOTAL;
                    productInfo.AMOUNT = item.PRICE;
                    productInfo.TOTAL_DISCOUNT = item.DISCOUNT;
                    //productInfo.TOTAL_VAT = item.DISCOUNT;
                    productInfo.TOTAL_AMOUNT = item.TOTAL_AMOUNT;

                    lstProduct.Add(productInfo);

                    LandingDetailInfo detailInfo = new LandingDetailInfo();
                    detailInfo.MAR_CODE = prodMarket.MAR_CODE;
                    detailInfo.CATEGORY = category;
                    detailInfo.PRODUCT_CODE = productCode;
                    detailInfo.PRODUCT_NAME = productName;
                    //productInfo.PRODUCT_NAME = prodMarket.PRODUCT_NAME;
                    detailInfo.PACK_CODE = prodMarket.PACK_CODE;
                    detailInfo.COUNT = item.TOTAL;
                    if(item.PRICE != null && item.PRICE != 0)
                    {
                        detailInfo.AMOUNT = item.PRICE;
                        detailInfo.TOTAL_DISCOUNT = item.DISCOUNT;
                        detailInfo.TOTAL_VAT = item.VAT;
                        detailInfo.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                    }

                    detailInfo.REF_VAL = prodMarket.REF_VAL;

                    // 11/02/2022 tuannv thêm thời gian BH
                    detailInfo.DURATION = prodMarket.DURATION;
                    detailInfo.DURATION_UNIT = prodMarket.DURATION_UNIT;

                    detailInfo.FORM_VALID = prodMarket.OBJ_VALIDATE;

                    int count = item.TOTAL;
                    OutProductInit objInit = new OutProductInit();
                    switch (category)
                    {
                        case var s when category.Equals(goConstants.CATEGORY_XE):
                            List<object> lstVehicle = new List<object>();
                            double amount = 0, total_vat = 0, total_discount = 0, total_amount = 0;
                            for (var i = 0; i < count; i++)
                            {
                                OutProductInit objData = JsonConvert.DeserializeObject<OutProductInit>(prodMarket.OBJ);

                                OutVehicleLanding vehicleInfo = JsonConvert.DeserializeObject<OutVehicleLanding>(JsonConvert.SerializeObject(objData.LIST_INSURED[0]));
                                lstVehicle.Add(vehicleInfo);

                                amount += vehicleInfo.AMOUNT;
                                total_discount += vehicleInfo.TOTAL_DISCOUNT;
                                total_vat += vehicleInfo.VAT;
                                total_amount += vehicleInfo.TOTAL_AMOUNT;
                            }
                            objInit.ORG_SELLER = ORG_SELLER;
                            objInit.CHANNEL = CHANNEL;
                            objInit.AMOUNT = amount;
                            objInit.TOTAL_DISCOUNT = total_discount;
                            objInit.TOTAL_VAT = total_vat;
                            objInit.TOTAL_AMOUNT = total_amount;
                            objInit.LIST_INSURED = lstVehicle;

                            // 11/02/2022 tuannv thêm thời gian BH
                            objInit.DURATION = prodMarket.DURATION;
                            objInit.DURATION_UNIT = prodMarket.DURATION_UNIT;

                            //objInit.FORM_VALID = prodMarket.OBJ_VALIDATE; // Bỏ vì a tú để ngoài 15/02/2022

                            totalAmount += total_amount;

                            if (item.PRICE == null || item.PRICE == 0)
                            {
                                detailInfo.AMOUNT = amount;
                                detailInfo.TOTAL_DISCOUNT = total_discount;
                                detailInfo.TOTAL_VAT = total_vat;
                                detailInfo.TOTAL_AMOUNT = total_amount;
                            }

                            break;
                    }
                    detailInfo.OBJ_INIT = JsonConvert.SerializeObject(objInit);

                    lstDetails.Add(detailInfo);
                }

                masterOrders.BUYER = buyer;
                masterOrders.LIST_PRODUCT = lstProduct;

                ladingMaster.ORDER = masterOrders;
                landingDetails = lstDetails;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return baseValidate;
        }
        #endregion

        #region Landing multi product
        public BaseResponse MaskLadingNew(BaseRequest request)
        {
            //Truyền thông tin khách hàng, lưu lại gửi sort link sms, email 
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                switch (config.partnerCode)
                {
                    case var s when config.partnerCode.Equals("ONPOINT"):
                    case var s1 when config.partnerCode.Equals("HDI_WEB"):
                        JObject jIinput = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                        string strOrgCode = jIinput["ORG_CODE"].ToString();
                        string strOrderCode = jIinput["ORDER_CODE"].ToString();
                        string strEXCHANGE_CODE = jIinput["EXCHANGE_CODE"]?.ToString();
                        string strName = jIinput["BUYER_NAME"].ToString();
                        string strPhone = jIinput["PHONE"].ToString();
                        string strAmount = jIinput["TOTAL_AMOUNT"].ToString();
                        string strEmail = jIinput["EMAIL"]?.ToString();
                        string strContent = jIinput["CONTENT"]?.ToString();
                        string strAddress = jIinput["ADDRESS"]?.ToString();
                        string strCusCode = jIinput["CUS_CODE"]?.ToString();
                        var strDetail = jIinput["DETAIL"];

                        LadingMasterInfo ladingMaster = new LadingMasterInfo();
                        ladingMaster.ORDER = new MasterOrders();
                        ladingMaster.ORDER.BUYER = new DTO.Orders.ORD_BUYER();
                        ladingMaster.ORDER.ORDER_ORG = strOrderCode;
                        ladingMaster.ORDER.ORG_SELLER = strOrgCode;
                        ladingMaster.ORDER.CHANNEL = strEXCHANGE_CODE;
                        ladingMaster.ORDER.TOTAL_AMOUNT = Convert.ToInt32(strAmount);
                        ladingMaster.ORDER.BUYER.NAME = strName;
                        ladingMaster.ORDER.BUYER.PHONE = strPhone;
                        ladingMaster.ORDER.BUYER.EMAIL = strEmail;
                        ladingMaster.ORDER.BUYER.ADDRESS = strAddress;
                        ladingMaster.ORDER.DATE_PAY = "06/08/2021";
                        ladingMaster.ORDER.STATUS = "PAID";
                        ladingMaster.ORDER.COUNT_PRODUCT = 1;
                        ladingMaster.ORDER.LIST_PRODUCT = new List<LadingProduct>();

                        LadingProduct product = new LadingProduct();
                        product.MAR_CODE = "MR_01";
                        product.CATEGORY = "XE";
                        product.PRODUCT_CODE = "XCG_TNDS_BB";
                        product.PRODUCT_NAME = "Trách nhiệm DS BB";
                        product.PACK_CODE = "TNDSBB";
                        product.COUNT = 1;
                        product.AMOUNT = 30000;
                        product.TOTAL_DISCOUNT = 0;
                        product.TOTAL_VAT = 0;
                        product.TOTAL_AMOUNT = 30000;
                        product.STATUS = "NONE_INFO";
                        ladingMaster.ORDER.LIST_PRODUCT.Add(product);

                        List<LandingDetailInfo> landingDetails = new List<LandingDetailInfo>();
                        LandingDetailInfo landingDetailInfo = new LandingDetailInfo();
                        landingDetailInfo.MAR_CODE = "MR_01";
                        landingDetailInfo.CATEGORY = "XE";
                        landingDetailInfo.PRODUCT_CODE = "XCG_TNDS_BB";
                        landingDetailInfo.PACK_CODE = "TNDSBB";
                        landingDetailInfo.COUNT = 1;
                        landingDetailInfo.AMOUNT = 30000;
                        landingDetailInfo.TOTAL_DISCOUNT = 0;
                        landingDetailInfo.TOTAL_VAT = 0;
                        landingDetailInfo.TOTAL_AMOUNT = 30000;
                        landingDetailInfo.STATUS = "NONE_INFO";
                        landingDetailInfo.OBJ_INIT = "abc";
                        landingDetails.Add(landingDetailInfo);

                        response = SaveObjLanding(ladingMaster, landingDetails, config, request);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse SaveObjLanding(LadingMasterInfo ladingMaster, List<LandingDetailInfo> landingDetails, ResponseConfig config, BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                var param = new
                {
                    username = request.Action.UserName,
                    obj_master = JsonConvert.SerializeObject(ladingMaster),
                    obj_detail = JsonConvert.SerializeObject(landingDetails)
                };
                BaseRequest req = request;
                req.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                req.Action.ActionCode = goConstantsProcedure.INS_ORDER_SKU;
                response = goBusinessAction.Instance.GetBaseResponse(req);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        #region Master
        public BaseResponse GetObjMaster(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                BaseResponse res = goBusinessAction.Instance.GetBaseResponse(request);
                if (res.Success)
                {
                    MasterInfo masterInfo = goBusinessCommon.Instance.GetData<MasterInfo>(res, 0).FirstOrDefault();
                    List<LadingProduct> lstProduct = goBusinessCommon.Instance.GetData<LadingProduct>(res, 1);
                    if (masterInfo != null)
                    {
                        LadingMasterInfo lading = new LadingMasterInfo();
                        lading.ORDER = new MasterOrders();
                        MasterOrders masterOrders = new MasterOrders();
                        masterOrders = JsonConvert.DeserializeObject<MasterOrders>(JsonConvert.SerializeObject(masterInfo));
                        ORD_BUYER buyer = JsonConvert.DeserializeObject<ORD_BUYER>(JsonConvert.SerializeObject(masterInfo));
                        masterOrders.BUYER = buyer;
                        masterOrders.LIST_PRODUCT = lstProduct;

                        lading.ORDER = masterOrders;
                        response = goBusinessCommon.Instance.getResultApi(true, lading, config);
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_ECM_01), goConstantsError.Instance.ERROR_ECM_01);
                    }
                }
                else
                {
                    response = res;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }
        #endregion
        #endregion

        public BaseResponse cancelSKD(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string OrgCode = config.partnerCode;
                string strOrderCode = "";
                switch (config.partnerCode)
                {
                    case nameof(goConstants.Org_Code.ONPOINT):
                        JObject jInput = JObject.Parse(request.Data.ToString());
                        strOrderCode = jInput["ORDER_CODE"].ToString();
                        var param = new
                        {
                            channel = "",
                            user = "",
                            org = OrgCode,
                            OrderCode = strOrderCode
                        };
                        BaseRequest reqMark = request;
                        reqMark.Action.ActionCode = goConstantsProcedure.CANCEL_SKU_ORG;
                        reqMark.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                        BaseResponse res = goBusinessAction.Instance.GetBaseResponse(reqMark);
                        if (res.Success)
                        {
                            List<JObject> resuft = goBusinessCommon.Instance.GetData<JObject>(res, 0);
                            if (resuft[0]["STATUS"].ToString().Equals("00"))
                            {
                                response.Success = true;
                                //nếu tồn tại email thì gửi mail thông báo cho khách hàng
                                JObject jThongBao = new JObject();
                                jThongBao.Add("ORG_CODE", OrgCode);
                                jThongBao.Add("PRODUCT_CODE", "TNDSBB");
                                jThongBao.Add("NOTIFY_TYPE", "TB_HUY");
                                jThongBao.Add("TYPE_DATA", "SKU");
                                jThongBao.Add("VALUE_DATA", strOrderCode);
                                jThongBao.Add("NAME", resuft[0]["NAME"].ToString());
                                jThongBao.Add("EMAIL", resuft[0]["EMAIL"].ToString());
                                jThongBao.Add("ORDER_CODE", strOrderCode);
                                jThongBao.Add("VALUE1", resuft[0]["VALUE1"].ToString());
                                new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "ALL")).Start();
                            }
                            else
                            {
                                response.Success = false;
                            }
                            response.Data = resuft[0];
                        }
                        else
                        {
                            response = res;
                            return response;
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        public BaseResponse getSkuByOrg(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                string OrgCode = config.partnerCode;
                string strOrderCode = "";
                JObject jInput = JObject.Parse(request.Data.ToString());
                strOrderCode = jInput["ORDER_CODE"].ToString();
                string strphone = jInput["PHONE"].ToString();
                string strDateF = jInput["DATE_FROM"].ToString();
                string strDateT = jInput["DATE_TO"].ToString();
                JObject jRes = new JObject();
                if (!(string.IsNullOrEmpty(strDateF) && string.IsNullOrEmpty(strDateT)))
                {
                    DateTime dtF = DateTime.ParseExact(strDateF, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime dtT = DateTime.ParseExact(strDateT, "dd/MM/yyyy", CultureInfo.InvariantCulture);                    
                    if ((dtT - dtF).Days > 31)
                    {
                        jRes.Add("MESSAGE", "Khoảng tìm kiếm quá dài");
                        response.Data = jRes;
                        return response;
                    }
                }                 
                if(string.IsNullOrEmpty(strOrderCode) && string.IsNullOrEmpty(strphone) && string.IsNullOrEmpty(strDateT))
                {
                    jRes.Add("MESSAGE", "Phải truyền sang 1 trong các tiêu chí tìm kiếm");
                    response.Data = jRes;
                    return response;
                }
                response = callPackage(goConstantsProcedure.GET_SKU_ORG, strEvm, new object[] { OrgCode, OrgCode,OrgCode, strOrderCode, strphone,strDateF, strDateT });
                List<JObject> resuft = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                JArray arr = new JArray();
                foreach (JObject job in resuft)
                {
                    JArray jobj = JArray.Parse(job["DATA_JSON"].ToString().Replace("\\", ""));
                    arr.Add(jobj);
                }
                   
                response.Data = arr;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        public BaseResponse cancelSKU_Tool(JObject request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["ORG_SELLER"].ToString() + request["TIME"].ToString()).ToLower();
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    string OrgCode = request["ORG_SELLER"].ToString();
                    string sTime = request["TIME"].ToString();
                    response = callPackage(goConstantsProcedure.CANCEL_SKU_TOOL, strEvm, new object[] { OrgCode, OrgCode, OrgCode, sTime });
                    List<JObject> resuft = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    foreach (JObject job in resuft)
                    {
                        // gửi mail thông báo cho khách hàng
                        JArray jobj = JArray.Parse(job["DATA_JSON"].ToString().Replace("\\", ""));
                        foreach (JToken token in jobj)
                        {
                            JObject jThongBao = new JObject();
                            jThongBao.Add("ORG_CODE", OrgCode);
                            jThongBao.Add("PRODUCT_CODE", "TNDSBB");
                            jThongBao.Add("NOTIFY_TYPE", "TB_HUY");
                            jThongBao.Add("TYPE_DATA", "SKU");
                            jThongBao.Add("VALUE_DATA", token["ORDER_ORG"].ToString());
                            jThongBao.Add("NAME", token["NAME"].ToString());
                            jThongBao.Add("EMAIL", token["EMAIL"].ToString());
                            jThongBao.Add("ORDER_CODE", token["ORDER_ORG"].ToString());
                            jThongBao.Add("VALUE1", token["VALUE1"].ToString());
                            new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "ALL")).Start();
                            JObject jInput = new JObject();
                            jInput.Add("order_no", token["ORDER_ORG"].ToString());
                            JArray jDetail = JArray.Parse(token["DETAIL"].ToString());
                            JArray jItems = new JArray();
                            foreach (JToken jtk in jDetail)
                            {
                                JObject item = new JObject();
                                item.Add("product_sku", jtk["MAR_CODE"].ToString());
                                item.Add("count", jtk["COUNT"].ToString());
                                item.Add("amount", jtk["TOTAL_AMOUNT"].ToString());
                                jItems.Add(item);
                            }
                            jInput.Add("items", jItems);
                            callBackSMS(OrgCode, jInput.ToString(), "item_expired");
                        }

                    }
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

                // Call API của onpoint để thông báo đã hủy tự động
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        public BaseResponse insert_detail_cer(JObject request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["ORG_SELLER"].ToString() + request["TIME"].ToString()).ToLower();
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    string OrgCode = request["ORG_SELLER"].ToString();
                    string sTime = request["TIME_L1"].ToString();
                    string sTime2 = request["TIME_L2"].ToString();
                    string sTimes = request["LAN"].ToString(); //L1,L2
                    response = callPackage(goConstantsProcedure.INS_DETAIL_CER, strEvm, new object[] { OrgCode, OrgCode, OrgCode, sTime, sTime2, sTimes });
                    List<JObject> resuft = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (sTimes.Equals("L1"))
                    {
                        foreach (JObject token in resuft)
                        {
                            // gửi mail thông báo cho khách hàng
                            JObject jThongBao = new JObject();
                            jThongBao.Add("ORG_CODE", OrgCode);
                            jThongBao.Add("PRODUCT_CODE", "TNDSBB");
                            jThongBao.Add("NOTIFY_TYPE", "TB_KHL1");
                            jThongBao.Add("TYPE_DATA", "SKU");
                            jThongBao.Add("VALUE_DATA", token["ORDER_ORG"].ToString());
                            jThongBao.Add("NAME", token["NAME"].ToString());
                            jThongBao.Add("EMAIL", token["EMAIL"].ToString());
                            jThongBao.Add("ORDER_CODE", token["ORDER_ORG"].ToString());
                            jThongBao.Add("VALUE1", token["VALUE1"].ToString());
                            jThongBao.Add("VALUE2", token["VALUE2"].ToString());
                            jThongBao.Add("VALUE3", token["VALUE3"].ToString());
                            jThongBao.Add("VALUE4", token["VALUE4"].ToString());
                            jThongBao.Add("VALUE5", token["VALUE5"].ToString());
                            jThongBao.Add("LINK", token["LINK"].ToString());
                            new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "ALL")).Start();

                            //goBusinessServices.Instance.callNotify(jThongBao, "ALL");
                        }
                    }
                    else
                    {
                        foreach (JObject token in resuft)
                        {

                            JObject jThongBao = new JObject();
                            jThongBao.Add("ORG_CODE", OrgCode);
                            jThongBao.Add("PRODUCT_CODE", "TNDSBB");
                            jThongBao.Add("NOTIFY_TYPE", "TB_KHL2");
                            jThongBao.Add("TYPE_DATA", "SKU");
                            jThongBao.Add("VALUE_DATA", token["ORDER_ORG"].ToString());
                            jThongBao.Add("NAME", token["NAME"].ToString());
                            jThongBao.Add("EMAIL", token["EMAIL"].ToString());
                            jThongBao.Add("ORDER_CODE", token["ORDER_ORG"].ToString());
                            jThongBao.Add("VALUE1", token["VALUE1"].ToString());
                            jThongBao.Add("VALUE2", token["VALUE2"].ToString());
                            jThongBao.Add("VALUE3", token["VALUE3"].ToString());
                            jThongBao.Add("VALUE4", token["VALUE4"].ToString());
                            jThongBao.Add("VALUE5", token["VALUE5"].ToString());
                            jThongBao.Add("LINK", token["LINK"].ToString());
                            new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "ALL")).Start();


                        }
                    }

                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

                // Call API của onpoint để thông báo đã hủy tự động
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        public BaseResponse update_status_GCN(string OrgCode, string strContractCode)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse resSave = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                response = callPackage(goConstantsProcedure.GET_SKU_CONTRACT, strEvm, new object[] { OrgCode, OrgCode, OrgCode, strContractCode });
                List<JObject> resuft = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                foreach (JObject job in resuft)
                {
                    JArray jobj = JArray.Parse(job["DATA_JSON"].ToString().Replace("\\", ""));
                    // Gọi API cập nhật trạng thái đơn
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Cap nhat thong tin onpoint " + jobj.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    callBackSMS(OrgCode, jobj[0]["ORDER_ORG"].ToString(), "ready_to_ship");
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        #endregion
    }
}
