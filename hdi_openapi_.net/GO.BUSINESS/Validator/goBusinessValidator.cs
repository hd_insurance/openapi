﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.DTO.Common;
using GO.HELPER.Utility;
using GO.VALIDATION.FluentValidator;

namespace GO.BUSINESS.Validator
{
    public class goBusinessValidator
    {
        #region Variable
        #endregion

        #region Contructor
        private static readonly Lazy<goBusinessValidator> InitInstance = new Lazy<goBusinessValidator>(() => new goBusinessValidator());
        public static goBusinessValidator Instance => InitInstance.Value;
        #endregion

        #region Method

        public void ValidatorRequest(ref TokenAuthorizationInfo result, BaseRequest request)
        {
            try
            {
                TokenAuthorizationInfo validationModel = FluentValidatorModels.Instance.ValidatorRequest(request);
                if (!validationModel.Sucess)
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2002);
                    result.ErrorMessage = validationModel.ErrorMessage;
                }
            }
            catch (Exception ex)
            {
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2002);
            }
        }
        #endregion
    }
}
