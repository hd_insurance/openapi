﻿using com.itextpdf.text.pdf.security;
using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Partner;
using GO.DTO.Service;
using GO.HELPER.Utility;
using iTextSharp.text;
using iTextSharp.text.io;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using Newtonsoft.Json;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using EvoPdf;
using EvoPdf.PdfToText;
using GO.DTO.SystemModels;
using Newtonsoft.Json.Linq;
using GO.DAO.Common;
using GO.DTO.Common;
using GO.BUSINESS.ServiceAccess;
using GO.ENCRYPTION;
using GO.BUSINESS.ModelBusiness.FileModels;
using GO.BUSINESS.SftpFile;
using GO.BUSINESS.Action;
using GO.DTO.CRM;

namespace GO.BUSINESS.Service
{
    public class goBussinessCRM
    {
        #region Variables
        private string nameClass = nameof(goBussinessCRM);
        public static string pathRoot = goUtility.Instance.GetPathProject();        
        #endregion
        #region Priorities
        public goBussinessCRM()
        {
            
        }
        #endregion
        #region Contructor
        private static readonly Lazy<goBussinessCRM> InitInstance = new Lazy<goBussinessCRM>(() => new goBussinessCRM());
        public static goBussinessCRM Instance => InitInstance.Value;
        #endregion

        #region Method
        public BaseResponse addRequired(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    RequiredModel requireModel = new RequiredModel();
                    requireModel = JsonConvert.DeserializeObject<RequiredModel>(request.Data.ToString());
                
                    if (requireModel == null || string.IsNullOrEmpty(requireModel.DOMAIN) || string.IsNullOrEmpty(requireModel.EMAIL) || requireModel.CONTENT == null )
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3002), "Chưa đủ nội dung yêu cầu");
                        return response;
                    }
                    // DOMAIN, REQUIRED_TYPE, REQUESTER_NAME, GENDER, EMAIL, PHONE, CITY,
                    //ENTERPRISE_NAME, CAREER, CONTENT, LOCATION, FIELD,   WARDS_CODE,DISTRICT_CODE,CITY_CODE,ADDRESS

                    string strFIELD = "";
                    if(string.IsNullOrEmpty(requireModel.FIELD))
                        strFIELD=getField(requireModel.CONTENT);
                        object[] paramSign = new object[] { requireModel.DOMAIN, requireModel.REQUIRED_TYPE, requireModel.REQUESTER_NAME , requireModel.GENDER , requireModel.EMAIL, requireModel.PHONE, 
                            requireModel.CITY, requireModel.ENTERPRISE_NAME,requireModel.CAREER,requireModel.CONTENT,requireModel.LOCATION,strFIELD,requireModel.WARDS_CODE,requireModel.DISTRICT_CODE,
                            requireModel.CITY_CODE, requireModel.ADDRESS};
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(request.Action.ActionCode, goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT), paramSign);
                    response= goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                    List<JObject> objRe = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (objRe == null || objRe[0].Count == 0)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                        return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3004), "Không ghi nhận được dữ liệu truyền vào!");
                    }
                    response = goBusinessCommon.Instance.getResultApi(true, objRe, config);
                }
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse getEmailCustomer(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    object[] paramSign;
                    if (request.Action.ActionCode.Equals("UPDATE_CRM_CUSTOMER_SENDMAIL"))
                    {
                        paramSign = new object[] { jInput["EMAIL_ID"].ToString(), jInput["STATUS"].ToString() , jInput["ERROR"].ToString() };
                    }    else
                    {
                        paramSign = new object[] { jInput["ORG_CODE"].ToString(), jInput["STATUS"].ToString() };
                    }    
                    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(request.Action.ActionCode, goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT), paramSign);
                    response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                    List<JObject> objRe = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (objRe == null || objRe[0].Count == 0)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "", Logger.ConcatName(nameClass, nameMethod));
                        return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3004), "Không lấy được dữ liệu!");
                    }
                    response = goBusinessCommon.Instance.getResultApi(true, objRe, config);
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        {
            ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
            return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
        }
        public string getField(string strContent)
        {
            string strReturn = "";
            string strContentKD = goBussinessSMS.Instance.removeMark(strContent).ToUpper();
            if (strContentKD.IndexOf("BOI THUONG") > 0)
                strReturn = strReturn + "BOI_THUONG;";
            if (strContentKD.IndexOf("XE") > 0)
                strReturn = strReturn + "XE;";
            if (strContentKD.IndexOf("SUC KHOE") > 0)
                strReturn = strReturn + "SUC_KHOE;";
            if (strContentKD.IndexOf("ATTD") > 0 || strContentKD.IndexOf("AN TAM") > 0 || strContentKD.IndexOf("TIN DUNG") > 0)
                strReturn = strReturn + "ATTD;";
            return strReturn;
        }
         #endregion
    }
}
