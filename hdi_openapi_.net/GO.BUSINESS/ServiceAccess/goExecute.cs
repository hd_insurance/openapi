﻿using GO.BUSINESS.Common;
using GO.DAO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.ServiceAccess
{
    public class goExecute
    {
        #region Contructor
        private static readonly Lazy<goExecute> _instance = new Lazy<goExecute>(() =>
        {
            return new goExecute();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goExecute Instance { get => _instance.Value; }
        #endregion

        #region Method
        public object ExecuteNoneQuery(string goSPName, object[] objParam)
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteNoneQuery(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteNoneQuery(ModeDB, connection, goSPName, objParam);
            }
        }

        public object ExecuteNoneQuery(goVariables.DATABASE_TYPE ModeDB, string connection, string goSPName, object[] objParam)
        {
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteNoneQuery(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteNoneQuery(ModeDB, connection, goSPName, objParam);
            }

        }

        public List<IEnumerable<dynamic>> ExecuteMultiple(string goSPName, object[] objParam)
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteQueryMultiple(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteQueryMultiple(ModeDB, connection, goSPName, objParam);
            }
        }

        public List<IEnumerable<dynamic>> ExecuteMultiple(goVariables.DATABASE_TYPE ModeDB, string connection, string goSPName, object[] objParam)
        {
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteQueryMultiple(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteQueryMultiple(ModeDB, connection, goSPName, objParam);
            }
        }

        public IEnumerable<dynamic> ExecuteSingle(string goSPName, object[] objParam)
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteQuery<dynamic>(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteQuery<dynamic>(ModeDB, connection, goSPName, objParam);
            }
        }

        public IEnumerable<dynamic> ExecuteSingle(goVariables.DATABASE_TYPE ModeDB, string connection, string goSPName, object[] objParam)
        {
            if (objParam == null)
            {
                return goDataAccess.Instance.ExecuteQuery<dynamic>(ModeDB, connection, goSPName);
            }
            else
            {
                return goDataAccess.Instance.ExecuteQuery<dynamic>(ModeDB, connection, goSPName, objParam);
            }
        }
        #endregion
    }
}
