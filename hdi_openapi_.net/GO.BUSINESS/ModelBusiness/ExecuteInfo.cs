﻿using GO.DAO.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.ModelBusiness
{
    public class ExecuteInfo
    {
        public string store { get; set; }
        public string connectString { get; set; }
        public object[] param { get; set; }
        public goVariables.DATABASE_TYPE ModeDB { get; set; }
    }
}
