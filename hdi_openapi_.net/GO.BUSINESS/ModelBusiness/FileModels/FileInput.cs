﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.CONSTANTS.goConstants;

namespace GO.BUSINESS.ModelBusiness.FileModels
{
    public class FileInput
    {
        public string name { get; set; }
        public FileDataType fileDataType { get; set; }
        public string fileData { get; set; }
        public string partner { get; set; }
        public string contractNo { get; set; }
        public FileFormat fileFormat { get; set; }
        public DataMode dataMode { get; set; }
    }

    public class FileResponse
    {
        public string nameOld { get; set; }
        public string pathFile { get; set; }
    }
}
