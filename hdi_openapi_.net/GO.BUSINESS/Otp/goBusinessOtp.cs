﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Service;
using GO.DTO.Voucher;
using GO.HELPER.Utility;
using System;
using Newtonsoft.Json;

namespace GO.BUSINESS.Otp
{
    public class goBusinessOtp
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessOtp> _instance = new Lazy<goBusinessOtp>(() =>
        {
            return new goBusinessOtp();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessOtp Instance { get => _instance.Value; }
        #endregion
        #region Method
        public BaseResponse OtpSend(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                object[] goParam = null;
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

                SendOtpModel info = new SendOtpModel();
                info.PHONE = goParam[1].ToString();
                info.CAMPAIGN_CODE = goParam[0].ToString();
                info.OTP_TYPE = goConstants.OTP_TYPE.VOUCHER.ToString();
                info.OTP_SIZE = 6;
                info.EXP_SECONDS = 60;
                info.ENV_CODE = config.env_code;
                info.USER_CREATE = goParam[1].ToString();
                if (goParam[2] != null)
                {
                    info.EMAIL = goParam[2].ToString();
                }
                if (!goBusinessCommon.Instance.DDOS_OTP(info, 3, 3600))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "Vui lòng thực hiện gửi lại OTP sau 60p", config, nameof(goConstantsError.Instance.ERROR_OTP_01), goConstantsError.Instance.ERROR_OTP_01);
                }
                else
                {
                    response = goBusinessCommon.Instance.SendOtp(info);
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_01), goConstantsError.Instance.ERROR_VOUCHER_01);
            }
            return response;
        }

        public BaseResponse OtpConfirm(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "request order voucher: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                // Kiểm tra request
                VoucherInsurFree voucherInsur = new VoucherInsurFree();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    voucherInsur = JsonConvert.DeserializeObject<VoucherInsurFree>(request.Data.ToString());
                if (voucherInsur == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_02), goConstantsError.Instance.ERROR_VOUCHER_02);
                    return response;
                }
                // Xác thực OTP
                int otpStatus = goBusinessCommon.Instance.VerifyOtp(voucherInsur.phone, voucherInsur.otp);
                if(otpStatus == 1)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "OTP Không chính xác", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }
                if (otpStatus == 2)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "OTP hết hiệu lực", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }
                if (otpStatus == -1)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "Lỗi xác thực OTP", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_03), goConstantsError.Instance.ERROR_VOUCHER_03);
                    return response;
                }
                if (otpStatus == 0)
                {
                    response.Success = true;
                    response.Data = new
                    {
                        CONFIRM = true,
                        MESSAGE = "Xác thực thành công",
                        PHONE = voucherInsur.phone,
                        OTP = voucherInsur.otp
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "Lỗi xác thực OTP", config, nameof(goConstantsError.Instance.ERROR_VOUCHER_01), goConstantsError.Instance.ERROR_VOUCHER_01);
            }
            return response;
        }
        #endregion
    }
}
