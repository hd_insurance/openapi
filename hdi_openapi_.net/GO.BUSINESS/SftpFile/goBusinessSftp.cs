﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.ModelBusiness.FileModels;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.SftpFile;
using GO.DTO.SystemModels;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.CONSTANTS.goConstants;

namespace GO.BUSINESS.SftpFile
{
    public class goBusinessSftp
    {
        #region Variables
        // Enter your host name or IP here
        private static string host = "";
        private static int port = 0;
        // Enter your sftp username here
        private static string username = "";
        // Enter your sftp password here
        private static string password = "";
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessSftp> _instance = new Lazy<goBusinessSftp>(() =>
        {
            return new goBusinessSftp();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessSftp Instance { get => _instance.Value; }
        #endregion
        #region Method
        public void GetConfig()
        {
            host = goBusinessCommon.Instance.GetConfigDB(goConstants.SftpHost);
            username = goBusinessCommon.Instance.GetConfigDB(goConstants.SftpUser);
            password = goBusinessCommon.Instance.GetConfigDB(goConstants.SftpPass);
        }
        public string Upload(ServerSftp serverSftp, string base64, string nameFile, string folderPath)
        {
            try
            {
                host = serverSftp.host;
                port = serverSftp.port;
                username = serverSftp.username;
                password = serverSftp.password;

                //GetConfig();
                string pathNew = "/" + folderPath + "/" + nameFile;
                var connectionInfo = new ConnectionInfo(host, port, username, new PasswordAuthenticationMethod(username, password));
                // Upload File
                using (var sftp = new SftpClient(connectionInfo))
                {

                    sftp.Connect();
                    if (!sftp.Exists(folderPath))
                    {
                        sftp.CreateDirectory(folderPath);
                    }
                    if (!sftp.Exists(pathNew))
                    {
                        //Convert Base64 Encoded string to Byte Array.
                        byte[] fileBytes = Convert.FromBase64String(base64);
                        Stream stream = new MemoryStream(fileBytes);
                        {
                            sftp.UploadFile(stream, pathNew, true);
                        }
                    }
                    sftp.Disconnect();
                }
                return pathNew;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, "goBusinessSftp - Upload");
                throw ex;
            }
        }

        public BaseResponse GetResponse(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_5002), goConstantsError.Instance.ERROR_5002);
                    return response;
                }
                SftpModels sftpRequest = new SftpModels();
                List<pathFile> lstPathNew = new List<pathFile>();
                sftpRequest = JsonConvert.DeserializeObject<SftpModels>(request.Data.ToString());
                if (sftpRequest != null && sftpRequest.files != null && sftpRequest.files.Any())
                {
                    BaseRequest req_info = request;
                    request.Action.ActionCode = "API754QXBX";
                    var param = new
                    {
                        org_code = sftpRequest.org_code,
                        env_code = config.env_code,
                        type = sftpRequest.type
                    };
                    request.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                    BaseResponse res_info = goBusinessAction.Instance.GetBaseResponse(request);
                    ServerSftp serverSftp = new ServerSftp();
                    if (res_info.Success)
                    {
                        serverSftp = goBusinessCommon.Instance.GetData<ServerSftp>(res_info, 0).FirstOrDefault();
                    }
                    
                    if (!res_info.Success || serverSftp == null || string.IsNullOrEmpty(serverSftp.host))
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_5002), goConstantsError.Instance.ERROR_5002);
                        return response;
                    }

                    foreach (var item in sftpRequest.files)
                    {
                        pathFile pathFileNew = new pathFile();
                        pathFileNew.nameFileOld = item.nameFile;
                        string folderPath = string.Empty;
                        string nameFileNew = string.Empty;
                        string[] arrExtension = item.nameFile.Split('.');
                        if (arrExtension == null || arrExtension.Length < 2)
                        {

                        }
                        else
                        {
                            for (var i = 0; i < arrExtension.Length - 1; i++)
                            {
                                nameFileNew = nameFileNew + arrExtension[i];
                            }
                            nameFileNew = nameFileNew + "_" + DateTime.Now.ToString("yyMMddhhmmss") + "." + arrExtension[arrExtension.Length - 1];
                            switch (item.typeFile)
                            {
                                case nameof(goConstants.FileFormat.XLS):
                                case nameof(goConstants.FileFormat.XLSX):
                                    folderPath = "EXCEL";
                                    break;
                                case nameof(goConstants.FileFormat.DOC):
                                case nameof(goConstants.FileFormat.DOCX):
                                    folderPath = "WORD";
                                    break;
                                case nameof(goConstants.FileFormat.TXT):
                                    folderPath = "TXT/A";
                                    break;
                                case nameof(goConstants.FileFormat.PNG):
                                case nameof(goConstants.FileFormat.JPG):
                                    folderPath = "IMAGE";
                                    break;
                                default:
                                    folderPath = "";
                                    break;
                            }
                            if (sftpRequest.type.Equals("FTP"))
                            {
                                pathFileNew.pathFileNew = Upload(serverSftp, item.FileData, nameFileNew, folderPath);
                            }
                            else
                            {
                                pathFileNew.pathFileNew = Upload(serverSftp, item.FileData, nameFileNew, folderPath);
                            }
                        }
                        lstPathNew.Add(pathFileNew);
                    }
                }
                response.Data = lstPathNew;
            }
            catch (Exception ex)
            {
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_5001), goConstantsError.Instance.ERROR_5001);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        public List<FileResponse> Upload(List<FileInput> files)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                GetConfig();
                if (string.IsNullOrEmpty(host) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                    throw new Exception("server is not empty");
                ConnectionInfo connect;
                connect = new ConnectionInfo(host, username, new PasswordAuthenticationMethod(username, password));
                List<FileInput> fileError = files.Where(i => string.IsNullOrEmpty(i.partner) || string.IsNullOrEmpty(i.contractNo)).ToList();
                if (fileError != null && fileError.Any())
                    throw new Exception("File error: " + JsonConvert.SerializeObject(fileError));

                List<FileResponse> responses = new List<FileResponse>();
                // Upload File
                using (var sftp = new SftpClient(connect))
                {
                    sftp.Connect();
                    if (files != null && files.Any())
                    {
                        FileResponse file = new FileResponse();
                        foreach (var item in files)
                        {
                            string pathFolder = "";
                            string nameFileNew = "";
                            string extension = "";
                            switch (item.fileFormat)
                            {
                                case goConstants.FileFormat.TXT:
                                    extension = "txt";
                                    break;
                                case goConstants.FileFormat.XLSX:
                                    extension = "xlsx";
                                    break;
                                case goConstants.FileFormat.XLS:
                                    extension = "xls";
                                    break;
                                case goConstants.FileFormat.DOC:
                                    extension = "doc";
                                    break;
                                case goConstants.FileFormat.DOCX:
                                    extension = "docx";
                                    break;
                                case goConstants.FileFormat.PDF:
                                    extension = "pdf";
                                    break;
                                case goConstants.FileFormat.PNG:
                                    extension = "png";
                                    break;
                                case goConstants.FileFormat.JPG:
                                    extension = "jpg";
                                    break;
                                case goConstants.FileFormat.JPEG:
                                    extension = "jpeg";
                                    break;
                            }
                            switch (item.dataMode)
                            {
                                case goConstants.DataMode.PRIVATE:
                                case goConstants.DataMode.PUBLIC:
                                case goConstants.DataMode.TEMP:
                                    pathFolder = item.partner + "/" + DateTime.Now.ToString("yy") + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + item.contractNo;
                                    nameFileNew = item.partner + "_" + item.contractNo + "_" + DateTime.Now.ToString("yyMMddhhmmss") + "." + extension;
                                    break;
                            }
                            if (string.IsNullOrEmpty(pathFolder) || string.IsNullOrEmpty(nameFileNew))
                                throw new Exception("type or mode not support");

                            string pathNew = "/" + pathFolder + "/" + nameFileNew;
                            if (!sftp.Exists(pathFolder))
                                sftp.CreateDirectory(pathFolder);

                            if (!sftp.Exists(pathNew))
                            {
                                switch (item.fileDataType)
                                {
                                    case goConstants.FileDataType.BASE64:
                                        //Convert Base64 Encoded string to Byte Array.
                                        byte[] fileBytes = Convert.FromBase64String(item.fileData);
                                        Stream stream = new MemoryStream(fileBytes);
                                        sftp.UploadFile(stream, pathNew, false);
                                        break;
                                    default:
                                        throw new Exception("type data not support");
                                        break;
                                }
                            }
                            file.nameOld = item.name;
                            file.pathFile = pathNew;
                            responses.Add(file);
                        }
                    }
                    sftp.Disconnect();
                }

                return responses;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public List<FileResponse> Upload(List<FileInput> files, string host, int port, string username, string password)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if(string.IsNullOrEmpty(host) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                    throw new Exception("server is not empty");
                ConnectionInfo connect;
                if (port == null || port == 0)
                    connect = new ConnectionInfo(host, username, new PasswordAuthenticationMethod(username, password));
                else
                    connect = new ConnectionInfo(host, port, username, new PasswordAuthenticationMethod(username, password));
                List<FileInput> fileError = files.Where(i => string.IsNullOrEmpty(i.partner) || string.IsNullOrEmpty(i.contractNo)).ToList();
                if(fileError != null && fileError.Any())
                    throw new Exception("File error: " + JsonConvert.SerializeObject(fileError));

                List<FileResponse> responses = new List<FileResponse>();
                // Upload File
                using (var sftp = new SftpClient(connect))
                {
                    sftp.Connect();
                    if (files != null && files.Any())
                    {
                        FileResponse file = new FileResponse();
                        foreach (var item in files)
                        {
                            string pathFolder = "";
                            string nameFileNew = "";
                            string extension = "";
                            switch (item.fileFormat)
                            {
                                case goConstants.FileFormat.TXT:
                                    extension = "txt";
                                    break;
                                case goConstants.FileFormat.XLSX:
                                    extension = "xlsx";
                                    break;
                                case goConstants.FileFormat.XLS:
                                    extension = "xls";
                                    break;
                                case goConstants.FileFormat.DOC:
                                    extension = "doc";
                                    break;
                                case goConstants.FileFormat.DOCX:
                                    extension = "docx";
                                    break;
                                case goConstants.FileFormat.PDF:
                                    extension = "pdf";
                                    break;
                                case goConstants.FileFormat.PNG:
                                    extension = "png";
                                    break;
                                case goConstants.FileFormat.JPG:
                                    extension = "jpg";
                                    break;
                                case goConstants.FileFormat.JPEG:
                                    extension = "jpeg";
                                    break;
                            }
                            switch (item.dataMode)
                            {
                                case goConstants.DataMode.PRIVATE:
                                case goConstants.DataMode.PUBLIC:
                                case goConstants.DataMode.TEMP:
                                    pathFolder = item.partner + "/" + DateTime.Now.ToString("yy") + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/" + item.contractNo;
                                    nameFileNew = item.partner + "_" + item.contractNo + "_" + DateTime.Now.ToString("yyMMddhhmmss") + "." + extension;
                                    break;
                            }
                            if(string.IsNullOrEmpty(pathFolder) || string.IsNullOrEmpty(nameFileNew))
                                throw new Exception("type or mode not support");

                            string pathNew = "/" + pathFolder + "/" + nameFileNew;
                            if (!sftp.Exists(pathFolder))
                                sftp.CreateDirectory(pathFolder);

                            if (!sftp.Exists(pathNew))
                            {
                                switch (item.fileDataType)
                                {
                                    case goConstants.FileDataType.BASE64:
                                        //Convert Base64 Encoded string to Byte Array.
                                        byte[] fileBytes = Convert.FromBase64String(item.fileData);
                                        Stream stream = new MemoryStream(fileBytes);
                                        sftp.UploadFile(stream, pathNew, false);
                                        break;
                                    default:
                                        throw new Exception("type data not support");
                                        break;
                                }
                            }
                            file.nameOld = item.name;
                            file.pathFile = pathNew;
                            responses.Add(file);
                        }
                    }
                    sftp.Disconnect();
                }

                return responses;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        #endregion
    }
}
