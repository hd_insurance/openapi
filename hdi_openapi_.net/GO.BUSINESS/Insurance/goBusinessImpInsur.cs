﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.DTO.Base;
using GO.DTO.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using System.Data;
using GO.HELPER.Utility;
using GO.BUSINESS.Common;
using GO.DTO.Insurance.ImportFile;
using GO.BUSINESS.Action;
using GO.DTO.Insurance;
using GO.DTO.Orders;
using GO.DTO.Insurance.Common;
using Newtonsoft.Json;
using GO.BUSINESS.Orders;

namespace GO.BUSINESS.Insurance
{
    public class goBusinessImpInsur
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessImpInsur> _instance = new Lazy<goBusinessImpInsur>(() =>
        {
            return new goBusinessImpInsur();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessImpInsur Instance { get => _instance.Value; }
        #endregion
        #region Method
        public BaseResponse ImpOrder(string ParentCode, string Secret, string userName, string action, string id, string action_no, string isSMS, string isEmail, string env, DataTable dt)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(ParentCode, Secret);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                List<InputImport> lstInput = goUtility.Instance.DataTableToList<InputImport>(dt);
                //List<InputImport> lstInput = goUtility.Instance.DataTableToList<InputImport>(dt)?.Where(i => !string.IsNullOrEmpty(i.I_NAME)).ToList();
                List<InputImport> lstError = lstInput.Where(i => string.IsNullOrEmpty(i.I_TYPE) || string.IsNullOrEmpty(i.I_NAME)
                    || string.IsNullOrEmpty(i.P_EFF) || string.IsNullOrEmpty(i.P_EXP) || string.IsNullOrEmpty(i.P_PACK_CODE)
                    || i.P_FEES == null || i.P_AMOUNT == null || i.P_TOTAL_ADD == null
                    || i.P_TOTAL_DISCOUNT == null || i.P_VAT == null || i.P_TOTAL_AMOUNT == null).ToList();
                if (lstError != null && lstError.Any())
                {
                    string err = "";
                    foreach (var item in lstError)
                    {
                        err += err + item.STT + ";";
                    }
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "loi data input: " + lstError.Count() + " .stt: " + err, Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_6008), goConstantsError.Instance.ERROR_6008);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "action: " + action + ". id: " + id + ".env: " + env, Logger.ConcatName(nameClass, nameMethod));
                try
                {
                    new Task(() => ExeImp(config, userName, action, id, action_no, isSMS, isEmail, env, lstInput)).Start();
                }
                catch (Exception)
                {
                }
                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                string messErr = ex.Message; //goConstantsError.Instance.ERROR_2009
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), messErr);
            }
            return response;
        }

        public MaskInsur GetMaskInsur(ImpProgram impProgram, List<InputImport> lstInput, string userName, string action, string id, string action_no, string isSMS, string isEmail, string env)
        {
            MaskInsur maskInsur = new MaskInsur();
            try
            {
                string category = impProgram.CATEGORY;

                maskInsur.CHANNEL = "SELLING_IMP";
                maskInsur.USERNAME = userName;
                maskInsur.ACTION = action;

                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = impProgram.ORG_SELLER;
                seller.SELLER_CODE = impProgram.SELLER_CODE;
                seller.ENVIROMENT = "WEB";
                maskInsur.SELLER = seller;

                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = "CN";
                buyer.ORG_CUS = impProgram.ORG_BUYER;
                buyer.NAME = impProgram.ORG_BUYER_NAME;
                maskInsur.BUYER = buyer;

                maskInsur.CATEGORY = category;
                maskInsur.PRODUCT_CODE = impProgram.PRODUCT_CODE;
                maskInsur.PRODUCT_MODE = goConstants.ModeInsurance.HD_BAO.ToString();

                ORD_PAYINFO payInfo = new ORD_PAYINFO();
                payInfo.PAYMENT_TYPE = goConstants.Payment_type.TH.ToString();
                maskInsur.PAY_INFO = payInfo;

                switch (category)
                {
                    case var s when category.Equals(goConstants.CATEGORY_SK) || category.Equals(goConstants.CATEGORY_TAINAN):
                        MaskCare(ref maskInsur, impProgram, lstInput);
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return maskInsur;
        }

        public void MaskCare(ref MaskInsur maskInsur, ImpProgram impProgram, List<InputImport> lstInput)
        {
            try
            {
                List<InsuredPerson> lstInsur = new List<InsuredPerson>();
                foreach (var item in lstInput)
                {
                    InsuredPerson insur = new InsuredPerson();
                    #region NĐBH
                    insur.CUS_ID = item.I_STAFF_CODE;
                    insur.STAFF_CODE = item.I_STAFF_CODE;
                    insur.STAFF_REF = item.I_STAFF_REF;
                    insur.RELATIONSHIP_STAFF = item.I_RELATIONSHIP_STAFF;
                    insur.TYPE = item.I_TYPE;
                    insur.NAME = item.I_NAME;
                    insur.DOB = item.I_DOB;
                    insur.GENDER = item.I_GENDER;
                    insur.PROV = item.I_PROV;
                    insur.DIST = item.I_DIST;
                    insur.WARDS = item.I_WARDS;
                    insur.ADDRESS = item.I_ADDRESS;
                    insur.IDCARD = item.I_IDCARD;
                    insur.EMAIL = item.I_EMAIL;
                    insur.PHONE = item.I_PHONE;
                    insur.FAX = item.I_FAX;
                    insur.TAXCODE = item.I_TAXCODE;
                    #endregion

                    #region Product 
                    insur.CONTRACT_NO = impProgram.CONTRACT_NO;
                    insur.CERTIFICATE_NO = item.P_CERTIFICATE_NO;
                    insur.PRODUCT_CODE = impProgram.PRODUCT_CODE;
                    insur.PACK_CODE = item.P_PACK_CODE;
                    insur.REGION = item.P_REGION;
                    insur.EFFECTIVE_DATE = item.P_EFF;
                    insur.EXPIRATION_DATE = item.P_EXP;
                    insur.FEES = item.P_FEES;
                    insur.AMOUNT = item.P_AMOUNT;
                    insur.TOTAL_DISCOUNT = item.P_TOTAL_DISCOUNT;
                    insur.TOTAL_ADD = item.P_TOTAL_ADD;
                    insur.VAT = item.P_VAT;
                    insur.TOTAL_AMOUNT = item.P_TOTAL_AMOUNT;
                    insur.DATE_SIGN = item.P_DATE_SIGN;

                    // Additional
                    if (!string.IsNullOrEmpty(item.ADDITIONAL))
                    {
                        insur.ADDITIONAL = new List<Additional>();
                        insur.ADDITIONAL = JsonConvert.DeserializeObject<List<Additional>>(item.ADDITIONAL);
                    }

                    // File
                    if (!string.IsNullOrEmpty(item.FILE))
                    {
                        insur.FILE = new List<DetailFile>();
                        insur.FILE = JsonConvert.DeserializeObject<List<DetailFile>>(item.FILE);
                    }
                    #endregion

                    #region Thụ hưởng
                    Beneficiary ben = new Beneficiary();
                    ben.TYPE = item.BE_TYPE;
                    ben.NAME = item.BE_NAME;
                    ben.DOB = item.BE_DOB;
                    ben.GENDER = item.BE_GENDER;
                    ben.PROV = item.BE_PROV;
                    ben.DIST = item.BE_DIST;
                    ben.WARDS = item.BE_WARDS;
                    ben.ADDRESS = item.BE_ADDRESS;
                    ben.IDCARD = item.BE_IDCARD;
                    ben.EMAIL = item.BE_EMAIL;
                    ben.PHONE = item.BE_PHONE;
                    ben.FAX = item.BE_FAX;
                    ben.TAXCODE = item.BE_TAXCODE;
                    ben.ORG_CODE = item.BE_ORG;

                    insur.BENEFICIARY = new List<Beneficiary>();
                    insur.BENEFICIARY.Add(ben);
                    //ben.RELATIONSHIP = item.BE_RELATIONSHIP;
                    #endregion

                    lstInsur.Add(insur);
                }
                maskInsur.HEALTH_INSUR = new List<InsuredPerson>();
                maskInsur.HEALTH_INSUR = lstInsur;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<BaseResponse> ExeImp(ResponseConfig config, string userName, string action, string id, string action_no, string isSMS, string isEmail, string env, List<InputImport> lstInput)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                // Lấy thông tin chương trình
                var param = new
                {
                    project = "",
                    username = userName,
                    progid = id
                };

                BaseRequest reqProgram = new BaseRequest();
                reqProgram = goBusinessCommon.Instance.GetBaseRequestHDI(config.env_code);
                reqProgram.Action.ActionCode = goConstantsProcedure.IMP_GET_PROGRAM;
                reqProgram.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                BaseResponse resProgram = goBusinessAction.Instance.GetBaseResponse(reqProgram);
                if (resProgram.Success)
                {
                    try
                    {

                        ImpProgram impProgram = goBusinessCommon.Instance.GetData<ImpProgram>(resProgram, 0).FirstOrDefault();

                        if (impProgram == null)
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_6007), goConstantsError.Instance.ERROR_6007);
                            return response;
                        }

                        MaskInsur maskInsur = new MaskInsur();
                        maskInsur = GetMaskInsur(impProgram, lstInput, userName, action, id, action_no, isSMS, isEmail, env);

                        ORD_PAYINFO PAY_INFO = new ORD_PAYINFO();
                        PAY_INFO.PAYMENT_TYPE = goConstants.Payment_type.TH.ToString();
                        maskInsur.PAY_INFO = PAY_INFO;

                        //var a = JsonConvert.SerializeObject(maskInsur);
                        OrdersModel ordersModel = goBusinessOrders.Instance.GetOrdersByHeath(maskInsur, true);
                        bool iisSMS = (string.IsNullOrEmpty(isSMS) || isSMS == "0") ? false : true;
                        bool iisEmail = (string.IsNullOrEmpty(isEmail) || isEmail == "0") ? false : true;
                        BaseRequest reqOrd = reqProgram;
                        reqOrd.Data = ordersModel;
                        if (true)
                        {
                            // nếu là env = 0 => test truyền isTest = true
                            if (string.IsNullOrEmpty(env) || env == "0")
                                response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd, true, iisEmail, iisSMS);
                            else
                                response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd, false, iisEmail, iisSMS);

                            if (response.Success && env == "1")
                            {
                                BaseRequest reqImp = reqProgram;
                                reqImp.Action.ActionCode = goConstantsProcedure.IMP_UDP_PROGRAM;
                                reqImp.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                                BaseResponse reqUdpPro = goBusinessAction.Instance.GetBaseResponse(reqImp);
                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "req udp pro: " + JsonConvert.SerializeObject(reqImp), Logger.ConcatName(nameClass, nameMethod));
                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "res udp pro: " + JsonConvert.SerializeObject(reqUdpPro), Logger.ConcatName(nameClass, nameMethod));
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                        string messErr = ex.Message; //goConstantsError.Instance.ERROR_2009
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), messErr);
                    }
                }
                else
                {
                    response = resProgram;
                    return response;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return response;
        }

        #endregion
    }
}
