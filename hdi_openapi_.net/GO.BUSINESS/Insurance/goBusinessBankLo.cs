﻿using System;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.COMMON.Log;
using GO.DTO.Common;
using GO.DTO.Orders;
using GO.ENCRYPTION;
using GO.BUSINESS.Common;
using GO.BUSINESS.Orders;
using GO.BUSINESS.Service;
using GO.DTO.Insurance.BANK_LO;
using Newtonsoft.Json.Linq;
using GO.BUSINESS.Action;
using GO.HELPER.Utility;
using GO.DTO.SystemModels;


using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;

namespace GO.BUSINESS.Insurance
{
    public class goBusinessBankLo
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessBankLo> _instance = new Lazy<goBusinessBankLo>(() =>
        {
            return new goBusinessBankLo();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessBankLo Instance { get => _instance.Value; }
        #endregion

        #region Method
        #region ATTD HDBank
        public BaseResponseV2 CreateLinkSelling(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "rq: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                BankLoRequest bankLoRequest = new BankLoRequest();
                if (request.Data != null)
                    bankLoRequest = JsonConvert.DeserializeObject<BankLoRequest>(request.Data.ToString());

                ValidationContext valContext = new ValidationContext(bankLoRequest, null, null);
                var result = new List<ValidationResult>();
                if (bankLoRequest.PRODUCT_CODE == "NHA365")
                {
                    // Start 2022-06-24 By ThienTVB
                    bankLoRequest.LO_INFO.LO_CONTRACT = bankLoRequest.LO_INFO.LO_ID;
                    //if (string.IsNullOrEmpty(bankLoRequest.LO_INFO.LO_CONTRACT))
                    //{
                    //    bankLoRequest.LO_INFO.LO_CONTRACT = bankLoRequest.LO_INFO.LO_ID;
                    //}
                    // End 2022-06-24 By ThienTVB
                }

                Valid<BankLoRequest>(bankLoRequest, ref result);
                Valid<BankSellerInfo>(bankLoRequest.SELLER_INFO, ref result);
                Valid<BankBranchInfo>(bankLoRequest.SELLER_INFO.BRANCH_INFO, ref result);
                foreach(var item in bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT)
                {
                    Valid<BankBranchParent>(item, ref result);
                }
                Valid<BankLoContract>(bankLoRequest.LO_INFO, ref result);
                //Valid<BankLoDisbur>(bankLoRequest.LO_INFO.DISBUR, ref result);
                // Add 2022-05-27 By ThienTVB
                //Valid<BankLoDisbur>(bankLoRequest.LO_INFO.DISBUR, ref result);
                if (bankLoRequest.PRODUCT_CODE == "ATTD_HDB")
                {
                    Valid(bankLoRequest.LO_INFO.DISBUR, ref result);
                }
                else if (bankLoRequest.PRODUCT_CODE == "NHA365")
                {
                    Valid(bankLoRequest.LO_INFO.ASSET_CODE, ref result);
                    Valid(bankLoRequest.LO_INFO.HOUSE_INFO, ref result);
                }
                // End 2022-05-27 By ThienTVB
                if (result != null && result.Any())
                {
                    response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), JsonConvert.SerializeObject(result));
                    return response;
                }

                // validate
                BaseValidate validate = ValidateCreateLink(bankLoRequest);
                if (!validate.Success)
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Khởi tạo link lo lỗi !", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", validate.Error, validate.ErrorMessage);
                    return response;
                }

                // HDB: Kiểm tra chi nhánh tồn tại không ? => thêm mới
                // HDB: Kiểm tra user tồn tại không ? => thêm mới hoặc update chi nhánh nếu thay đổi
                var param = new
                {
                    org_code = bankLoRequest.SELLER_INFO.BRANCH_INFO.BANK_CODE,
                    o_business = JsonConvert.SerializeObject(bankLoRequest.SELLER_INFO)
                };

                BaseRequest reqUser = request;
                reqUser.Action.ActionCode = goConstantsProcedure.CHANGE_USER_LO;
                reqUser.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                BaseResponse resUser = goBusinessAction.Instance.GetBaseResponse(reqUser);
                LoUserChange infoUser = new LoUserChange();
                if (resUser.Success)
                {
                    infoUser = goBusinessCommon.Instance.GetData<LoUserChange>(resUser, 0).FirstOrDefault();
                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "user info: " + JsonConvert.SerializeObject(infoUser), Logger.ConcatName(nameClass, nameMethod));

                    if (goUtility.Instance.CastToBoolean(infoUser.IS_INS))
                    {
                        // gửi mail tài khoản đến KH
                        // infoUser.USER_NAME và infoUser.PASSWORD
                        var paramEmail = new
                        {
                            USER_NAME = infoUser.USER_NAME,
                            PASSWORD = infoUser.PASSWORD,
                            STAFF_CODE = bankLoRequest.SELLER_INFO.SELLER_CODE,
                            STAFF_NAME = bankLoRequest.SELLER_INFO.SELLER_NAME,
                            STAFF_GENDER = bankLoRequest.SELLER_INFO.SELLER_GENDER,
                            ORG_CODE = bankLoRequest.SELLER_INFO.BRANCH_INFO.BANK_CODE,
                            EMAIL = bankLoRequest.SELLER_INFO.SELLER_EMAIL
                        };
                        JObject jThongBao = JObject.FromObject(paramEmail);

                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "jThongBao info: " + JsonConvert.SerializeObject(jThongBao), Logger.ConcatName(nameClass, nameMethod));

                        new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "TTK_EMAIL")).Start();
                    }
                    if (goUtility.Instance.CastToBoolean(infoUser.IS_UPD))
                    {
                        // gửi mail cập nhật chi nhánh
                        //bankLoRequest.SELLER_INFO.BRANCH_INFO.BRANCH_CODE
                    }
                }
                else
                {
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Cập nhật thông tin user lỗi !", JsonConvert.SerializeObject(reqUser).ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "err user: " + JsonConvert.SerializeObject(resUser), Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.ConvertResponse(resUser, auditLogs.LogId);
                    return response;
                }

                // Kiểm tra đơn tồn tại chưa:
                // 1. Nếu tồn tại thì update lại chi nhánh, thông tin vay của đơn nếu có thay đổi => trả về link với order cũ
                // 2. Nếu chưa tồn tại thì thêm 1 đơn nháp => trả về link với orders mới

                //khởi tạo orders
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrdersByBankLo(bankLoRequest, infoUser);
                //var a = JsonConvert.SerializeObject(ordersModel);
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                BaseResponse resOrd = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                response = goBusinessCommon.Instance.ConvertResponse(resOrd, auditLogs.LogId);

                string vCategory = "", vContract_code = "", vDetail_code = "", vTilte = "", vAction = "", vProduct_code = "",
                    vOrg_code = "", vChannel = "", vUser_name = "";
                if (resOrd.Success)
                {
                    try
                    {
                        vOrg_code = bankLoRequest.SELLER_INFO.BRANCH_INFO.BANK_CODE;
                        vChannel = bankLoRequest.CHANNEL;
                        vUser_name = infoUser.USER_NAME;//request.Action.UserName;

                        JArray jArr = JArray.Parse(JsonConvert.SerializeObject(resOrd.Data));
                        var jData = jArr[0];
                        vContract_code = jData["CONTRACT_CODE"].ToString();
                        var strDetail = jData["INSURED"];
                        JArray jArrDetail = JArray.Parse(strDetail.ToString());
                        if (jArrDetail.Count > 0)
                        {
                            JObject jDetail = (JObject)jArrDetail[0];
                            vCategory = jDetail["CATEGORY"].ToString();
                            vProduct_code = jDetail["PRODUCT_CODE"].ToString();
                            vDetail_code = jDetail["DETAIL_CODE"].ToString();
                            vTilte = "Cấp đơn bảo hiểm của đối tác";
                            vAction = "EDIT";
                        }

                    }
                    catch (Exception ex)
                    {
                        //goBussinessEmail.Instance.sendEmaiCMS("", "Lo loi lay contract. res: ", JsonConvert.SerializeObject(resOrd).ToString());
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lo loi lay contract", Logger.ConcatName(nameClass, nameMethod));
                    }
                }
                else
                {
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Tạo đơn lỗi !", JsonConvert.SerializeObject(reqOrd).ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "err user: " + JsonConvert.SerializeObject(resOrd), Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.ConvertResponse(resOrd, auditLogs.LogId);
                    return response;
                }

                object objSell = new
                {
                    category = vCategory,
                    contract_code = vContract_code,
                    detail_code = vDetail_code,
                    tilte = vTilte,
                    action = vAction,
                    product_code = vProduct_code,
                    org_code = vOrg_code,
                    channel = vChannel,
                    user_name = vUser_name,
                    partner = "LO_HDB",
                    user_permission = new { APPROVE = 0, CREATE_ORDER = 1, INSPECTION = 0 }
                };

                string urlSell = goBusinessCommon.Instance.GetConfigDB(goConstants.HDI_SELLING);
                string urlLo = urlSell + "partner/indexCreaterOrder?pt=";
                // Start 2022-06-20 By ThienTVB
                string lo_id = "";
                if (bankLoRequest.PRODUCT_CODE == "NHA365")
                {
                    lo_id = bankLoRequest.LO_INFO.LO_ID + bankLoRequest.LO_INFO.ASSET_CODE;
                }
                // End 2022-06-20 By ThienTVB
                string t = goEncryptMix.Instance.encryptHmacSha256Md5(vUser_name + lo_id + DateTime.Now.ToString("yyyyMMdd"), "HDinsuranceIT");
                int timeout = (23 - DateTime.Now.Hour) * 3600 + (59 - DateTime.Now.Minute) * 60;
                if (timeout > 0)
                    goBusinessCache.Instance.Add<object>(t, objSell, timeout, true);

                object objData = new
                {
                    url = urlLo + t
                };
                response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, true, objData, config);
                resLog = response;
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        public void Valid<T>(T obj, ref List<ValidationResult> result)
        {
            ValidationContext valContext = new ValidationContext(obj, null, null);
            System.ComponentModel.DataAnnotations.Validator.TryValidateObject(obj, valContext, result, true);
        }
        public BaseValidate ValidateCreateLink(BankLoRequest bankLoRequest)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            BaseValidate baseValidate = new BaseValidate();
            baseValidate.Success = true;
            try
            {
                if (bankLoRequest == null)
                    throw new Exception("bankLoRequest null");
                //if (bankLoRequest.SELLER_INFO == null || bankLoRequest.SELLER_INFO.BRANCH_INFO == null ||
                //    string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.SELLER_CODE) || string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.BRANCH_INFO.BANK_CODE) ||
                //    string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.BRANCH_INFO.BRANCH_CODE) || bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT == null || !bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT.Any()
                //    )
                //{
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_04), goConstantsError.Instance.ERROR_LO_04);
                //}

                //if (bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT != null || bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT.Any())
                //{
                //    var lst = bankLoRequest.SELLER_INFO.BRANCH_INFO.ARR_PARENT.Where(x => string.IsNullOrEmpty(x.CODE) || string.IsNullOrEmpty(x.NAME) || string.IsNullOrEmpty(x.PARENT_CODE)).ToList();
                //    if (lst != null && lst.Any())
                //        baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_10), goConstantsError.Instance.ERROR_LO_10);
                //}


                //if (bankLoRequest.LO_INFO == null || string.IsNullOrEmpty(bankLoRequest.LO_INFO.NAME) || string.IsNullOrEmpty(bankLoRequest.LO_INFO.LO_CONTRACT)
                //    || string.IsNullOrEmpty(bankLoRequest.LO_INFO.LO_TYPE) || string.IsNullOrEmpty(bankLoRequest.LO_INFO.LO_DATE) || string.IsNullOrEmpty(bankLoRequest.LO_INFO.LO_DATE) || bankLoRequest.LO_INFO.LO_TOTAL == null || bankLoRequest.LO_INFO.LO_TOTAL == 0)
                //{
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_05), goConstantsError.Instance.ERROR_LO_05);
                //    return baseValidate;
                //}

                //if (string.IsNullOrEmpty(bankLoRequest.USER_NAME) || string.IsNullOrEmpty(bankLoRequest.CHANNEL) || string.IsNullOrEmpty(bankLoRequest.PRODUCT_CODE))
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_06), goConstantsError.Instance.ERROR_LO_06);

                //if (bankLoRequest.LO_INFO.DISBUR == null || string.IsNullOrEmpty(bankLoRequest.LO_INFO.DISBUR.DISBUR_CODE) || string.IsNullOrEmpty(bankLoRequest.LO_INFO.DISBUR.DISBUR_NUM) || string.IsNullOrEmpty(bankLoRequest.LO_INFO.DISBUR.DISBUR_DATE) || bankLoRequest.LO_INFO.DISBUR.DISBUR_AMOUNT == null || bankLoRequest.LO_INFO.DISBUR.DISBUR_AMOUNT == 0)
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_09), goConstantsError.Instance.ERROR_LO_09);

                //if (string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.SELLER_NAME) || string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.SELLER_EMAIL) || string.IsNullOrEmpty(bankLoRequest.SELLER_INFO.SELLER_PHONE))
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_11), goConstantsError.Instance.ERROR_LO_11);

                string idcard_d, dob, disbur_date, lo_date = "";
                idcard_d = bankLoRequest.LO_INFO.IDCARD_D;
                dob = bankLoRequest.LO_INFO.DOB;
                //disbur_date = bankLoRequest.LO_INFO.DISBUR.DISBUR_DATE;
                //lo_date = bankLoRequest.LO_INFO.LO_DATE;
                //if (goUtility.Instance.ConvertToDateTime(idcard_d, DateTime.MaxValue) > DateTime.Now.Date) // 28/12/2021 thunt bảo bỏ
                //{
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_12), goConstantsError.Instance.ERROR_LO_12 + "IDCARD_D");
                //}
                if (goUtility.Instance.ConvertToDateTime(dob, DateTime.MaxValue) > DateTime.Now.Date)
                {
                    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_12), goConstantsError.Instance.ERROR_LO_12 + "DOB");
                }
                //if (goUtility.Instance.ConvertToDateTime(disbur_date, DateTime.MaxValue) > DateTime.Now.Date) // 28/12/2021 thunt bảo bỏ
                //{
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_12), goConstantsError.Instance.ERROR_LO_12 + "DISBUR_DATE");
                //}
                //if (goUtility.Instance.ConvertToDateTime(lo_date, DateTime.MaxValue) > DateTime.Now.Date) // 28/12/2021 thunt bảo bỏ
                //{
                //    baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_12), goConstantsError.Instance.ERROR_LO_12 + "LO_DATE");
                //}
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                baseValidate = goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);
            }
            return baseValidate;
        }

        public BaseResponseV2 SearStatusInsur(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            BaseResponseV2 resLog = new BaseResponseV2();
            try
            {
                if (config == null)
                {
                    response = resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BankSearchStatus bankRequest = new BankSearchStatus();
                if (request.Data != null)
                    bankRequest = JsonConvert.DeserializeObject<BankSearchStatus>(request.Data.ToString());

                if (string.IsNullOrEmpty(bankRequest.LO_CONTRACT) || string.IsNullOrEmpty(bankRequest.DISBUR_CODE) || string.IsNullOrEmpty(bankRequest.BANK_CODE) ||
                    string.IsNullOrEmpty(bankRequest.BRANCH_CODE))
                {
                    response = resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_LO_08), goConstantsError.Instance.ERROR_LO_08);
                    return response;
                }
                var param = new
                {
                    lo_contract = bankRequest.LO_CONTRACT,
                    disbur_code = bankRequest.DISBUR_CODE,
                    bank_code = bankRequest.BANK_CODE,
                    branch_code = bankRequest.BRANCH_CODE
                };

                BaseRequest reqSearch = request;
                //reqSearch.Action.ActionCode = goConstantsProcedure.SEAR_STATUS_BY_LO;
                reqSearch.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                BaseResponse resSear = goBusinessAction.Instance.GetBaseResponse(reqSearch);
                response = goBusinessCommon.Instance.ConvertResponse(resSear, auditLogs.LogId);
                resLog = response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        // Start 2022-06-17 By ThienTVB
        public BaseResponseV2 SearStatusInsurByLoId(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            BaseResponseV2 resLog = new BaseResponseV2();
            try
            {
                if (config == null)
                {
                    response = resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BankSearchStatusByLoId bankRequest = new BankSearchStatusByLoId();
                if (request.Data != null)
                    bankRequest = JsonConvert.DeserializeObject<BankSearchStatusByLoId>(request.Data.ToString());

                if (string.IsNullOrEmpty(bankRequest.LO_ID))
                {
                    response = resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_LO_08), goConstantsError.Instance.ERROR_LO_08);
                    return response;
                }
                var param = new
                {
                    lo_id = bankRequest.LO_ID
                };

                BaseRequest reqSearch = request;
                //reqSearch.Action.ActionCode = "APIZACUUS4";
                //reqSearch.Action.ActionCode = goConstantsProcedure.SEAR_STATUS_BY_LO;
                reqSearch.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                BaseResponse resSear = goBusinessAction.Instance.GetBaseResponse(reqSearch);
                response = goBusinessCommon.Instance.ConvertResponse_v1(resSear, auditLogs.LogId);
                resLog = response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                resLog = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        // End 2022-06-17 By ThienTVB
        public BaseResponse SearchCertificate(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        #endregion

        #region Valid token
        public BaseResponse ValidToken(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string data = "";
                try
                {
                    data = JsonConvert.DeserializeObject<JObject>(request.Data.ToString())["token"].ToString();
                    if (string.IsNullOrEmpty(data))
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("lấy token lỗi: " + JsonConvert.SerializeObject(request));
                }
                if (goBusinessCache.Instance.Exists(data))
                {
                    var dataCache = goBusinessCache.Instance.Get<object>(data);
                    response = goBusinessCommon.Instance.getResultApi(true, dataCache, config);
                }
                else
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_07), goConstantsError.Instance.ERROR_LO_07);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        #endregion

        #endregion
    }
}
