﻿using GO.DTO.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

using GO.DTO.Centech;
using GO.DTO.Insurance;
using Newtonsoft.Json;
using GO.DTO.Centech.Loans;
using GO.COMMON.Log;
using GO.BUSINESS.PartnerFunction;
using GO.DTO.Centech.Login;
using GO.DTO.Orders;
using GO.BUSINESS.Orders;
using GO.CONSTANTS;
using GO.BUSINESS.Common;
using GO.DTO.Partner;
using GO.HELPER.Utility;
using GO.DTO.Common;
using GO.BUSINESS.Service;
using GO.BUSINESS.Action;
using GO.DTO.Org.Vietjet;
using System.Data;
using GO.DTO.Org.Hdi;
using GO.DTO.CalculateFees;

using GO.DTO.CommonBusiness;
using GO.DTO.Insurance.Common;
using System.Net.Http;
using GO.SERVICE.CallApi;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using GO.COMMON.Cache;
using GO.HELPER.Config;
using GO.DTO.Insurance.Vehicle;
using GO.DTO.Insurance.Callback;
using GO.ENCRYPTION;
using GO.DTO.Insurance.House;
using GO.DTO.Insurance.CheckExists;
using GO.DTO.Orders.SearTrans;
using GO.DTO.Org.Fpt;
using GO.DTO.SystemModels;
using GO.DTO.Insurance.Search;
using GO.DTO.Insurance.BANK_LO;
using System.Globalization;
using static iTextSharp.text.pdf.AcroFields;
using DnsClient.Protocol;
using GO.BUSINESS.ModelBusiness.FileModels;
using System.Diagnostics.Contracts;
using System.Web;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Net.Http.Headers;
using System.Security.Policy;
using static GO.CONSTANTS.goConstants;
using Renci.SshNet.Common;

namespace GO.BUSINESS.Insurance
{
    public class goBusinessInsurance
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessInsurance> _instance = new Lazy<goBusinessInsurance>(() =>
        {
            return new goBusinessInsurance();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessInsurance Instance { get => _instance.Value; }
        #endregion

        #region Method
        #region Bảo hiểm tín dụng - Old
        /*
        /// <summary>
        /// Bước 1: gọi qua cent login
        /// Bước 2: gửi thông tin qua cent cấp đơn
        /// Bước 3: Nhận response về tạo order phía HDI (nếu lỗi cũng tạo phía HDI)
        /// Bước 4: Trả về cho HDB url
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BaseResponse GetBaseResponse_InsurLoAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                ContractLoans contractLoans = new ContractLoans();
                if (request.Data != null)
                    contractLoans = JsonConvert.DeserializeObject<ContractLoans>(request.Data.ToString());
                if (contractLoans == null)
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);

                object loRequest = GetCentLoAdd(contractLoans);
                string branch = contractLoans.BRANCH_CODE;

                //Bước 1: get token cent
                string token = goBusinessCentech.Instance.GetTokenLogin();
                // Bước 2: gửi thông tin qua cent cấp đơn
                ContractLoansResponse contractLoansResponse = new ContractLoansResponse();
                goBusinessCentech.Instance.CreateContract(ref contractLoansResponse, ref contractLoans, loRequest, token);
                if (contractLoansResponse != null)
                {
                    response = goBusinessCommon.Instance.getResultApi(true, contractLoansResponse, config);
                    // Bước 3: Save all data bằng thread
                    try
                    {
                        OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderByLoContract_Old(contractLoans);
                        //response = 
                        goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    }
                }
                else
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_02), goConstantsError.Instance.ERROR_LO_02);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_02), goConstantsError.Instance.ERROR_LO_02);
            }
            // Bước 4
            return response;
        }

        public object GetCentLoAdd(ContractLoans contractLoans)
        {
            try
            {
                CentInsurLoAdd centLoData = new CentInsurLoAdd();
                goBusinessCentech.Instance.configCentech();
                centLoData.dvi_sl = goBusinessCentech.dvi_sl_cent;
                centLoData.SO_HD_KENH = contractLoans.LO_CONTRACT;
                centLoData.SALEID = contractLoans.TELLER_CODE;
                centLoData.TEN_SALE = contractLoans.TELLER_NAME;
                centLoData.EMAIL_NHANG = contractLoans.TELLER_EMAIL;
                centLoData.KENH = goBusinessCentech.kenh_cent;
                centLoData.MA_CN = goBusinessCentech.ma_cn_cent;

                centLoData.SO_CIF_KH = contractLoans.CIF;
                centLoData.TEN = contractLoans.NAME;

                if (contractLoans.GENDER == goConstants.Gender_Unit.M.ToString())
                    centLoData.GIOI = "0";
                else if (contractLoans.GENDER == goConstants.Gender_Unit.F.ToString())
                    centLoData.GIOI = "1";
                centLoData.SO_CMT = contractLoans.IDCARD;
                centLoData.NGAY_CMT = goUtility.Instance.ConvertToDateTime(contractLoans.IDCARD_D, DateTime.MinValue) == DateTime.MinValue ? string.Empty : goUtility.Instance.ConvertToDateTime(contractLoans.IDCARD_D, DateTime.MinValue).ToString("yyyyMMdd");
                centLoData.NOI_CMT = contractLoans.IDCARD_P;
                centLoData.NGAY_SINH = goUtility.Instance.stringToDate(contractLoans.DOB, "DD/MM/YYYY", DateTime.MinValue).ToString("yyyyMMdd");

                centLoData.PHONE = contractLoans.PHONE;
                centLoData.EMAIL = contractLoans.EMAIL;
                centLoData.DCHI = contractLoans.ADD;
                centLoData.NT_VAY = contractLoans.CURRENCY;
                centLoData.SUC_KHOE = "K";
                centLoData.SO_ID = "0";
                //centLoData.NGAY_HL = contractLoans.LO_EFF_DATE;
                //centLoData.NGAY_KT = contractLoans.LO_EXP_DATE;
                if (contractLoans.TYPE.Equals(goConstants.PersonType.CN.ToString()))
                    centLoData.NHOM_DN = goConstants.Cent_PersonType.C.ToString();
                if (contractLoans.INS_TYPE.Equals(goConstants.TypeOrder.BH_SDBS.ToString()))
                    centLoData.SO_HD_G = contractLoans.INS_CONTRACT;
                else
                    centLoData.KIEU_HD = goConstants.Cent_kieu_hd.G.ToString();
                centLoData.NV = goConstants.Cent_nv.ATTD.ToString();
                centLoData.TIEN_BH = contractLoans.LO_TOTAL;
                centLoData.TTRANG = "Đang trình";

                centLoData.TIEN_VAY = contractLoans.LO_TOTAL;
                centLoData.HAN_VAY = contractLoans.DURATION;
                centLoData.NGUOI_TH = contractLoans.BANK_CODE;
                centLoData.HAN_VAY_HL = contractLoans.LO_DATE;

                return new { data = JsonConvert.SerializeObject(centLoData) };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bước 1: convert data request
        /// Bước 2: gọi qua cent login
        /// Bước 3: gửi thông tin qua cent lấy thông tin đơn
        /// Bước 4: Update về HDI
        /// Bước 5: Lấy thông tin thanh toán
        /// Bước 6: Gọi qua thanh toán
        /// Bước 7: Trả về: order_trans, so_id_cent, url cổng tt, qr nếu có
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BaseResponse GetBaseResponse_InsurLoInfo(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                // Bước 1: convert data request
                CentIdInfo centInfo = new CentIdInfo();
                if (request.Data != null)
                    centInfo = JsonConvert.DeserializeObject<CentIdInfo>(request.Data.ToString());

                if (centInfo == null || string.IsNullOrEmpty(centInfo.so_id) || string.IsNullOrEmpty(centInfo.nv))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);
                }
                //Bước 2: get token cent
                string token = goBusinessCentech.Instance.GetTokenLogin();

                // Bước 3: gửi thông tin qua cent lấy thông tin đơn
                object body = GetObjInsurLoInfo(centInfo);
                string data = goBusinessCentech.Instance.GetContractById(body, token);
                CentInsurLoInfo insurLoInfo = JsonConvert.DeserializeObject<CentInsurLoInfo>(data);

                // Bước 4: Update về HDI
                BaseResponse responseOrd = new BaseResponse();
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderByCentInsurLo(insurLoInfo);
                responseOrd = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                if (responseOrd.Success)
                {
                    // Bước 5: Lấy thông tin thanh toán
                    OrderResInfo orderResInfo = goBusinessCommon.Instance.GetData<OrderResInfo>(responseOrd, 0).FirstOrDefault();
                    BaseRequest request_pay = goBusinessOrders.Instance.GetBaseRequest_OrderPay(request, orderResInfo.order_code, orderResInfo.str_period, orderResInfo.total_pay, null);
                    // Bước 6: Gọi qua thanh toán
                    response = goBussinessPayment.Instance.payment(request_pay);
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_03), goConstantsError.Instance.ERROR_LO_03);
                }
            }
            catch (Exception ex)
            {
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_03), goConstantsError.Instance.ERROR_LO_03);
            }
            return response;
        }

        public object GetObjInsurLoInfo(CentIdInfo centInfo)
        {
            try
            {
                return new { data = "{\"ma_dvi\":\"" + goBusinessCentech.dvi_sl_cent + "\",\"so_id\":\"" + centInfo.so_id + "\",\"kieu_hd\":\"" + centInfo.kieu_hd + "\",\"so_hd_g\":\"" + centInfo.so_hd_g + "\",\"sao\":\"" + centInfo.sao + "\"}" };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        #endregion

        #region Bảo hiểm tín dụng
        /*
        public BaseResponse CreateLinkSelling(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                LoContract loContract = new LoContract();
                if (request.Data != null)
                    loContract = JsonConvert.DeserializeObject<LoContract>(request.Data.ToString());
                if (loContract == null || string.IsNullOrEmpty(loContract.LO_CONTRACT) || string.IsNullOrEmpty(loContract.LO_TYPE) || string.IsNullOrEmpty(loContract.LO_MODE) || string.IsNullOrEmpty(loContract.BANK_CODE))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);
                    return response;
                }

                if (1 == 2)
                {
                    response = GetBaseResponse_InsurLoAdd(request);
                }
                else
                {
                    OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderByLoContract(loContract);
                    response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                    if (response.Success)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_02), goConstantsError.Instance.ERROR_LO_02);
            }
            // Bước 4
            return response;
        }
        */
        /// <summary>
        /// cập nhật trạng thái giải ngân, Thay đổi tiền bảo hiểm cho trường hợp dư nợ giảm dần ngân hàng tặng
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public BaseResponse UpdateDisbursed(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Lo_Disbur_Status lo = new Lo_Disbur_Status();
                if (request.Data != null)
                    lo = JsonConvert.DeserializeObject<Lo_Disbur_Status>(request.Data.ToString());
                if (lo == null || string.IsNullOrEmpty(lo.LO_CONTRACT))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);
                    return response;
                }
                object Param = new
                {
                    Channel = "",
                    UserName = request.Action.UserName,
                    LO_CONTRACT = lo.LO_CONTRACT,
                    DISBUR_CODE = lo.DISBUR_CODE,
                    DISBUR_DATE = lo.DISBUR_DATE
                };
                BaseRequest requestUpd = new BaseRequest();
                requestUpd.Device = request.Device;
                requestUpd.Action = request.Action;
                requestUpd.Action.ActionCode = goConstantsProcedure.UPD_DISBUR_STATUS;
                requestUpd.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(Param));
                goBusinessCommon.Instance.GetSignRequest(ref requestUpd, config);
                response = goBusinessAction.Instance.GetBaseResponse(requestUpd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_02), goConstantsError.Instance.ERROR_LO_02);
            }
            return response;
        }

        public BaseResponse LoDecrease(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Lo_Decrease lo = new Lo_Decrease();
                if (request.Data != null)
                    lo = JsonConvert.DeserializeObject<Lo_Decrease>(request.Data.ToString());
                if (lo == null || string.IsNullOrEmpty(lo.LO_CONTRACT))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_01), goConstantsError.Instance.ERROR_LO_01);
                    return response;
                }
                response.Success = true;
                //object Param = new
                //{
                //    Channel = "",
                //    UserName = request.Action.UserName,
                //    LO_CONTRACT = lo.LO_CONTRACT,
                //    DISBUR_CODE = lo.DISBUR_CODE,
                //    DISBUR_DATE = lo.DISBUR_DATE
                //};
                //BaseRequest requestUpd = new BaseRequest();
                //requestUpd.Device = request.Device;
                //requestUpd.Action = request.Action;
                //requestUpd.Action.ActionCode = goConstantsProcedure.UPD_DISBUR_STATUS;
                //requestUpd.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(Param));
                //goBusinessCommon.Instance.GetSignRequest(ref requestUpd, config);
                //response = goBusinessAction.Instance.GetBaseResponse(requestUpd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_LO_02), goConstantsError.Instance.ERROR_LO_02);
            }
            return response;
        }

        #endregion

        #region Safe Flight Vietjet
        public BaseResponse GetResponseSafeHomeAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                FLIGHT flight = new FLIGHT();
                if (request.Data != null)
                    flight = JsonConvert.DeserializeObject<FLIGHT>(request.Data.ToString());
                if (flight == null)
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_01), goConstantsError.Instance.ERROR_VJC_01);
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderBySafeFlight(flight);
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }

        public BaseResponse GetResponseSafeHomeConfirm(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                FLIGHT flight = new FLIGHT();
                if (request.Data != null)
                    flight = JsonConvert.DeserializeObject<FLIGHT>(request.Data.ToString());
                if (flight == null)
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_01), goConstantsError.Instance.ERROR_VJC_01);
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderBySafeFlight(flight);
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }

        public BaseResponse GetResOrderVJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);

            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrderVJC orderVJC = new OrderVJC();
                if (request.Data != null)
                    orderVJC = JsonConvert.DeserializeObject<OrderVJC>(request.Data.ToString());
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderByVJC(orderVJC);
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                if (response.Success)
                {
                    try
                    {
                        OrderResInfo resInfoOrder = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                        if (resInfoOrder != null && resInfoOrder.status.Equals(nameof(goConstants.OrderStatus.PAID)))
                        {
                            InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                            //response = goBusinessServices.Instance.signVietJetBatch(insurContract);
                            //List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);
                            //send mail GCN theo insurDetails  
                            //foreach (InsurDetail item in insurDetails)
                            //{
                            //    response = goBusinessServices.Instance.signVietJet(item.CERTIFICATE_NO);
                            //    if (response.Success)
                            //    {
                            //    }
                            //    else
                            //    {
                            //        //response.Success = false;
                            //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + item.CERTIFICATE_NO + JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
                            //        goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(item));
                            //    }
                            //}

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "send mail voucher: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }

        public BaseResponse GetResTravelOrderVJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);

            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrderVJC orderVJC = new OrderVJC();
                if (request.Data != null)
                    orderVJC = JsonConvert.DeserializeObject<OrderVJC>(request.Data.ToString());                

                if (orderVJC.FLIGHT_INFO == null || orderVJC.CUSTOMERS == null || !orderVJC.CUSTOMERS.Any() || orderVJC.PAY_INFO == null)
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_01), goConstantsError.Instance.ERROR_VJC_01);

                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderTravelByVJC(orderVJC);
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                response = goBusinessOrders.Instance.GetBaseResponse_Orders_Import(reqOrd);
                if (response.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Tạo đơn VJ thành công", Logger.ConcatName(nameClass, nameMethod));
                    String dateNow = DateTime.Now.ToString("ddMMyyyy");
                    for (int i = 0; i <= 1; i++)
                    {
                        JObject objectSyn = new JObject();
                        String exportDate = DateTime.Now.AddDays(i).ToString("dd/MM/yyyy");
                        String fliId = orderVJC.FLIGHT_INFO.FLI_ID;
                        String productCode = "ALL";
                        string strMD5 = goEncryptBase.Instance.Md5Encode(dateNow + "HDI@2022" + productCode + exportDate + orderVJC.FLIGHT_INFO.FLI_ID).ToLower();
                        objectSyn.Add("EXPORT_DATE", exportDate);
                        objectSyn.Add("FLIGHT_ID", fliId);
                        objectSyn.Add("PRODUCT_CODE", productCode);
                        objectSyn.Add("MD5", strMD5);

                        response = goBusinessVietjet.Instance.synDataltoBVietInsurance(objectSyn);
                    }                        

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Đồng bộ Bảo Việt Thành công", Logger.ConcatName(nameClass, nameMethod));
                }    
                goBusinessCommon.Instance.SignResponse(ref response, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }        

        public BaseResponse GetResOrderV2VJC(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrderVJC orderVJC = new OrderVJC();
                if (request.Data != null)
                    orderVJC = JsonConvert.DeserializeObject<OrderVJC>(request.Data.ToString());

                if (orderVJC.FLIGHT_INFO == null || orderVJC.CUSTOMERS == null || !orderVJC.CUSTOMERS.Any() || orderVJC.PAY_INFO == null)
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_01), goConstantsError.Instance.ERROR_VJC_01);

                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderByV2VJC(orderVJC);
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                goBusinessCommon.Instance.SignResponse(ref response, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }
        #endregion

        #region Vietjet - Opt-out
        public BaseResponse GetProductByOrg(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BaseResponse res_get = goBusinessAction.Instance.GetBaseResponse(request);
                if (res_get.Success)
                {
                    List<ProductHDI> lstProd = new List<ProductHDI>();
                    List<PackInfo> lstPack = new List<PackInfo>();
                    List<Benefits> lstBenef = new List<Benefits>();
                    List<Addit> lstAdd = new List<Addit>();

                    lstProd = goBusinessCommon.Instance.GetData<ProductHDI>(res_get, 0);
                    lstPack = goBusinessCommon.Instance.GetData<PackInfo>(res_get, 1);
                    lstBenef = goBusinessCommon.Instance.GetData<Benefits>(res_get, 2);
                    lstAdd = goBusinessCommon.Instance.GetData<Addit>(res_get, 3);

                    List<object> lstProdOut = new List<object>();
                    if (lstProd != null && lstProd.Any())
                    {
                        if (!lstPack.Any())
                            lstPack = new List<PackInfo>();
                        if (!lstBenef.Any())
                            lstBenef = new List<Benefits>();
                        if (!lstAdd.Any())
                            lstAdd = new List<Addit>();

                        foreach (var item in lstProd)
                        {
                            ProductHDI objProduct = item;
                            List<PackInfo> lstPackSear = lstPack.Where(i => i.CATEGORY.Equals(item.CATEGORY) && i.PRODUCT_CODE.Equals(item.PRODUCT_CODE)).ToList();
                            List<PackInfo> lstPackByProd = new List<PackInfo>();
                            if (lstPackSear != null)
                            {
                                foreach (var itemPack in lstPackSear)
                                {
                                    PackInfo packInfo = itemPack;
                                    ObjBenefits objBenefits = new ObjBenefits();

                                    List<Benefits> lstBenefSear = lstBenef.Where(i => i.CATEGORY.Equals(itemPack.CATEGORY) && i.PRODUCT_CODE.Equals(itemPack.PRODUCT_CODE) && i.PACK_CODE.Equals(itemPack.PACK_CODE)).ToList();
                                    List<Addit> lstAddSear = lstAdd.Where(i => i.CATEGORY.Equals(itemPack.CATEGORY) && i.PRODUCT_CODE.Equals(itemPack.PRODUCT_CODE) && i.PACK_CODE.Equals(itemPack.PACK_CODE)).ToList();
                                    objBenefits.BENEFITS = lstBenefSear.Select(x => new { x.BE_CODE, x.BE_NAME });

                                    packInfo.ADDITIONAL = lstAddSear.Select(x => new { x.ADD_CODE, x.ADD_NAME, x.ADD_FEES, x.DESCRIPTION });
                                    packInfo.BENEFITS_INFO = objBenefits;
                                    lstPackByProd.Add(packInfo);
                                }
                            }
                            objProduct.PACK_INFO = lstPackByProd.Select(i => new { i.PACK_CODE, i.PACK_NAME, i.FEES, i.FILE_URL, i.BENEFITS_INFO, i.ADDITIONAL });
                            lstProdOut.Add(objProduct);
                        }
                    }
                    response = goBusinessCommon.Instance.getResultApi(true, lstProdOut, config);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
            return response;
        }
        public BaseResponse CreateOrderByVietjet(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                ReqInsurAir reqAir = new ReqInsurAir();
                if (request.Data != null)
                    reqAir = JsonConvert.DeserializeObject<ReqInsurAir>(request.Data.ToString());

                if (reqAir.ORG_CODE == "VIETJET_VN")
                {
                    ResInsurAir resAir = new ResInsurAir();
                    resAir.ORDER = new OrdAir();
                    resAir.ORDER.ORD_CODE = "ORD-00000001";
                    resAir.ORDER.AMOUNT = "800000";
                    resAir.ORDER.TOTAL_DISCOUNT = "0";
                    resAir.ORDER.VAT = "0";
                    resAir.ORDER.TOTAL_AMOUNT = "800000";
                    resAir.ORDER.STATUS = "WAIT_PAY";
                    resAir.ORDER.STATUS_NAME = "Chờ thanh toán";
                    resAir.ORDER.SELLER_INFO = reqAir.SELLER_INFO;
                    resAir.ORDER.FLI_INFO = reqAir.FLI_INFO;
                    resAir.ORDER.BUYER_INFO = reqAir.BUYER_INFO;
                    List<OrdDetailAir> lstDetail = new List<OrdDetailAir>();
                    OrdDetailAir detailAir = new OrdDetailAir();
                    detailAir.DETAIL_CODE = "ODT-00000001";
                    detailAir.AMOUNT = "800000";
                    detailAir.TOTAL_DISCOUNT = "0";
                    detailAir.VAT = "0";
                    detailAir.TOTAL_AMOUNT = "800000";
                    detailAir.CUSTOMERS = reqAir.CUSTOMERS;
                    lstDetail.Add(detailAir);
                    resAir.ORDER.DETAIL = lstDetail;

                    response = goBusinessCommon.Instance.getResultApi(true, resAir, config);
                }
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }
        public BaseResponse ActiveOrderByVietjet(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                object[] param = null;
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    param = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);
                if (param != null && param.Length > 4)
                {
                    if (param[3].ToString() == "ORD-00000001")
                    {
                        response.Success = true;
                        if (param[4].ToString() == "CANCEL")
                            response.Data = "Hủy đơn thành công !";
                        else if (param[4].ToString() == "ACTIVE")
                            response.Data = "Kích hoạt đơn thành công. GCN sẽ được gửi đến cho khách hàng !";
                    }
                    else
                        throw new Exception();
                }
                else
                    throw new Exception();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        #endregion

        #region Chiến sỹ vắc xin
        public BaseResponse CsvxOrdersAdd(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                ReqCsvx reqCsvx = new ReqCsvx();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    reqCsvx = JsonConvert.DeserializeObject<ReqCsvx>(request.Data.ToString());
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrdersCSVX(reqCsvx);
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                if (response.Success)
                {
                    string payment_type = nameof(goConstants.Payment_type.CTT);
                    // Bước 5: Lấy thông tin thanh toán
                    OrderResInfo orderResInfo = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                    //BaseRequest request_pay = goBusinessOrders.Instance.GetBaseRequest_OrderPay(request, orderResInfo.order_code, orderResInfo.str_period, orderResInfo.total_pay, null);
                    // Bước 6: Gọi qua thanh toán
                    BaseResponse resOrdTrans = goBusinessOrders.Instance.GetOrderTrans(request, orderResInfo.order_code, orderResInfo.str_period, orderResInfo.total_pay, payment_type, ordersModel.COMMON?.PAY_INFO?.PAYMENT_MIX, null);
                    response = goBusinessOrders.Instance.GetResOrdTrans(resOrdTrans, request, config, payment_type);

                    //List<OrderTransInfo> lstOrdTrans = new List<OrderTransInfo>();
                    //List<OrdersInfo> lstOrd = new List<OrdersInfo>();
                    //List<OrderDetailInfo> lstOrdDetail = new List<OrderDetailInfo>();
                    //if (resOrdTrans.Success)
                    //{
                    //    lstOrdTrans = goBusinessCommon.Instance.GetData<OrderTransInfo>(resOrdTrans, 0);
                    //    lstOrd = goBusinessCommon.Instance.GetData<OrdersInfo>(resOrdTrans, 6);
                    //    lstOrdDetail = goBusinessCommon.Instance.GetData<OrderDetailInfo>(resOrdTrans, 7);
                    //    goBusinessOrders.Instance.GetResActionPayment(ref response, resOrdTrans, request, config, payment_type, lstOrdTrans, lstOrd, lstOrdDetail);
                    //}
                    //else
                    //    response = resOrdTrans;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }
        #endregion

        #region Xe cơ giới
        public BaseResponse CreateInsurVehicle(BaseRequest request, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                VehicleInsur vehicleInsur = new VehicleInsur();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    vehicleInsur = JsonConvert.DeserializeObject<VehicleInsur>(request.Data.ToString());

                // validate các define truyền lên
                BaseValidate baseValidate = ValidateDefineVehicle(request, vehicleInsur);
                if (!baseValidate.Success)
                    return goBusinessCommon.Instance.getResultApi(false, "", config, baseValidate.Error, baseValidate.ErrorMessage);

                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Json: " + JsonConvert.SerializeObject(vehicleInsur), Logger.ConcatName(nameClass, nameMethod));

                if (vehicleInsur != null && vehicleInsur.VEHICLE_INSUR != null && vehicleInsur.VEHICLE_INSUR.Any())
                {
                    string ORG_CODE = vehicleInsur.ORG_CODE, CHANNEL = vehicleInsur.CHANNEL, USERNAME = vehicleInsur.USERNAME, ACTION = vehicleInsur.ACTION;
                    if (vehicleInsur.VEHICLE_INSUR.Count == 1)
                    {
                        foreach (var item in vehicleInsur.VEHICLE_INSUR)
                        {
                            VEHICLE_PRODUCT vehicleProduct = item;

                            OutDynamicFees outDynamic = new OutDynamicFees();
                            CalInsurVehicle(request, ORG_CODE, CHANNEL, USERNAME, ref vehicleProduct, ref outDynamic);

                            //if(vehicleProduct == null || vehicleProduct.)

                            OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderVehicle(vehicleProduct, outDynamic, ORG_CODE, CHANNEL, USERNAME, ACTION, vehicleInsur.BUYER, vehicleInsur.BILL_INFO, vehicleInsur.ORD_SKU, vehicleInsur.ORD_PARTNER);


                            if (vehicleInsur.ACTION.Equals("BH_S"))
                            {
                                ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = item.CONTRACT_CODE;
                            }

                            if (pay)
                            {
                                ordersModel.COMMON.PAY_INFO = vehicleInsur.PAY_INFO;
                                if (vehicleInsur != null && vehicleInsur.PAY_INFO != null && ordersModel != null && ordersModel.COMMON != null && ordersModel.COMMON.ORDER != null && vehicleInsur.PAY_INFO.PAYMENT_TYPE.Equals(nameof(goConstants.Payment_type.TH)))
                                {
                                    ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();
                                }
                            }
                            BaseRequest reqOrd = request;
                            reqOrd.Data = ordersModel;
                            if (pay)
                                response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                            else
                                response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);

                        }
                    }
                    else
                    {
                        foreach (var item in vehicleInsur.VEHICLE_INSUR)
                        {
                            VEHICLE_PRODUCT vehicleProduct = item;

                            OutDynamicFees outDynamic = new OutDynamicFees();
                            CalInsurVehicle(request, ORG_CODE, CHANNEL, USERNAME, ref vehicleProduct, ref outDynamic);
                        }

                        OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderVehicle(vehicleInsur.VEHICLE_INSUR, ORG_CODE, CHANNEL, USERNAME, ACTION, vehicleInsur.BUYER, vehicleInsur.BILL_INFO, vehicleInsur.ORD_SKU, vehicleInsur.ORD_PARTNER);
                        if (vehicleInsur.ACTION.Equals("BH_S"))
                        {
                            ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = vehicleInsur.VEHICLE_INSUR[0].CONTRACT_CODE;
                        }

                        if (pay)
                        {
                            ordersModel.COMMON.PAY_INFO = vehicleInsur.PAY_INFO;
                            if (vehicleInsur != null && vehicleInsur.PAY_INFO != null && ordersModel != null && ordersModel.COMMON != null && ordersModel.COMMON.ORDER != null && vehicleInsur.PAY_INFO.PAYMENT_TYPE.Equals(nameof(goConstants.Payment_type.TH)))
                            {
                                ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();
                            }
                        }
                        BaseRequest reqOrd = request;
                        reqOrd.Data = ordersModel;
                        if (pay)
                            response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                        else
                            response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse CreateInsurVehicleVer2(BaseRequest request, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vehi Request Data: " + JsonConvert.SerializeObject(request.Data.ToString()), Logger.ConcatName(nameClass, nameMethod));
                VehicleInsur vehicleInsur = new VehicleInsur();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    vehicleInsur = JsonConvert.DeserializeObject<VehicleInsur>(request.Data.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vehi: " + JsonConvert.SerializeObject(vehicleInsur), Logger.ConcatName(nameClass, nameMethod));
                if (vehicleInsur != null && vehicleInsur.VEHICLE_INSUR != null && vehicleInsur.VEHICLE_INSUR.Any())
                {
                    // nếu là đại lý chụp ảnh thì phải validate file truyền lên
                    List<VEHICLE_PRODUCT> lstSur = vehicleInsur.VEHICLE_INSUR.Where(i => i.PHYSICAL_DAMAGE != null && i.IS_PHYSICAL == "1" && i.PHYSICAL_DAMAGE.SURVEYOR_TYPE.Equals(goConstants.SurveyorType.DLCA.ToString())).ToList();
                    if (lstSur != null && lstSur.Any())
                    {
                        List<VEHICLE_PRODUCT> lstFileSur = lstSur.Where(i => i.PHYSICAL_DAMAGE.SURVEYOR_FILES == null).ToList();
                        if (lstFileSur != null && lstFileSur.Any())
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                            return response;
                        }
                    }

                    // validate các define truyền lên
                    BaseValidate baseValidate = ValidateDefineVehicle(request, vehicleInsur);
                    if (!baseValidate.Success)
                        return goBusinessCommon.Instance.getResultApi(false, "", config, baseValidate.Error, baseValidate.ErrorMessage);

                    OutDynamicFeesVer3 outDynamic = new OutDynamicFeesVer3();
                    CalInsurVehicleVer3(ref outDynamic, ref vehicleInsur, request);


                    // 29/10/2021 bổ sung validate phí client truyền lên
                    if (outDynamic == null || outDynamic.PRODUCT_DETAIL == null && !outDynamic.PRODUCT_DETAIL.Any())
                        throw new Exception("Không tìm thấy sản phẩm để tính phí");
                    if (1 == 2)
                    {
                        BaseValidate validateFees = goBusinessValidateFees.Instance.ValidationFessVehicle(vehicleInsur, outDynamic);
                        if (!validateFees.Success)
                            return goBusinessCommon.Instance.getResultApi(false, "", config, validateFees.Error, validateFees.ErrorMessage);
                    }


                    //double amount = 0, total_discount = 0, vat = 0, total_amount = 0, total_add = 0; ;
                    string ORG_CODE = vehicleInsur.ORG_CODE, CHANNEL = vehicleInsur.CHANNEL, USERNAME = vehicleInsur.USERNAME, ACTION = vehicleInsur.ACTION;

                    if (outDynamic != null && outDynamic.PRODUCT_DETAIL != null && outDynamic.PRODUCT_DETAIL.Any())
                    {

                        for (var i = 0; i < vehicleInsur.VEHICLE_INSUR.Count(); i++)
                        {
                            double amount = 0, total_discount = 0, vat = 0, total_amount = 0, total_add = 0;

                            // sản phẩm VCX
                            #region Old
                            /*
                             
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                            {
                                OutDynamicProductVer3 productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.INX).FirstOrDefault();
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesVCX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).FirstOrDefault();

                                    if (feesVCX != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = feesVCX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = feesVCX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = feesVCX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = feesVCX.TOTAL_DISCOUNT;
                                        //item.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = productVer3.TOTAL_ADDITIONAL;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = feesVCX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = feesVCX.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = 0;
                                    }

                                    // thêm thông tin phí gốc (chưa gồm bổ sung)
                                    ADDITIONAL_FEES feesVCX_NoBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).FirstOrDefault();
                                    if (feesVCX_NoBS != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = feesVCX_NoBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = feesVCX_NoBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = feesVCX_NoBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = feesVCX_NoBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = feesVCX_NoBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = feesVCX_NoBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = 0;
                                    }
                                }
                                if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                    ADDITIONAL_FEES feesVcxBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).FirstOrDefault();
                                    if (feesVcxBS != null)
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = feesVcxBS.TOTAL_AMOUNT;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = feesVcxBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = feesVcxBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = feesVcxBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = feesVcxBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = feesVcxBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = feesVcxBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = 0;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = 0;
                                    }

                                }
                                // gán phí bs vào danh sách truyền lên
                                if (vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL != null && vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Any())
                                {
                                    if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                    {
                                        List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                        List<PHYSICAL_DAMAGE_ADDITIONAL> lstAddPhy = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                                        for (var k = 0; k < vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Count; k++)
                                        {
                                            PHYSICAL_DAMAGE_ADDITIONAL addPhy = new PHYSICAL_DAMAGE_ADDITIONAL();
                                            var item = vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL[k];
                                            ADDITIONAL_FEES existAddi = lstOut.Where(x => x.CODE.Equals(item.KEY))?.FirstOrDefault();
                                            if (existAddi != null)
                                            {
                                                addPhy = JsonConvert.DeserializeObject<PHYSICAL_DAMAGE_ADDITIONAL>(JsonConvert.SerializeObject(item));
                                                addPhy.FEES = existAddi.FEES;
                                                addPhy.AMOUNT = existAddi.AMOUNT;
                                                addPhy.TOTAL_DISCOUNT = existAddi.TOTAL_DISCOUNT;
                                                addPhy.VAT = existAddi.VAT;
                                                addPhy.TOTAL_AMOUNT = existAddi.TOTAL_AMOUNT;
                                                // bổ sung 12/05/2021
                                                addPhy.DISCOUNT_ADDITIONAL = existAddi.DISCOUNT_DETAIL;
                                                lstAddPhy.Add(addPhy);
                                            }
                                        }
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL = lstAddPhy;
                                    }
                                }

                                // gán phí TN
                                if ((vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == null || vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "0") && vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY_ALL == "1")
                                {
                                    if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                    {
                                        //bổ sung 12/05/2021
                                        List<DiscountInfo> lstDiscountVoluntary = new List<DiscountInfo>();

                                        List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                        ADDITIONAL_FEES feesTN = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).FirstOrDefault();
                                        if (feesTN != null)
                                        {
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = feesTN.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = feesTN.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = feesTN.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = feesTN.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = feesTN.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = feesTN.TOTAL_AMOUNT;
                                        }
                                        else
                                        {
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = 0;
                                        }

                                        // gán phí chi tiết
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_T3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = feesTN_T3.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_INSUR = feesTN_T3.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = feesTN_T3.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = feesTN_T3.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_INSUR = feesTN_T3.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = feesTN_T3.TOTAL_AMOUNT;
                                            if (feesTN_T3.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_T3.DISCOUNT_DETAIL);


                                            ADDITIONAL_FEES feesTN_TST3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = feesTN_TST3.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_ASSETS = feesTN_TST3.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = feesTN_TST3.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = feesTN_TST3.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_ASSETS = feesTN_TST3.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = feesTN_TST3.TOTAL_AMOUNT;
                                            if (feesTN_TST3.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_TST3.DISCOUNT_DETAIL);

                                            ADDITIONAL_FEES feesTN_HK = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = feesTN_HK.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_PASSENGER = feesTN_HK.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_PASSENGER = feesTN_HK.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = feesTN_HK.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_PASSENGER = feesTN_HK.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_PASSENGER = feesTN_HK.TOTAL_AMOUNT;
                                            if (feesTN_HK.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_HK.DISCOUNT_DETAIL);

                                            ADDITIONAL_FEES feesTN_VQM = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = feesTN_VQM.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = feesTN_VQM.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = feesTN_VQM.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = feesTN_VQM.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = feesTN_VQM.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = feesTN_VQM.TOTAL_AMOUNT;
                                            if (feesTN_VQM.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_VQM.DISCOUNT_DETAIL);
                                        }
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_DRIVER == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_LPX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_DRIVER = feesTN_LPX.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DRIVER = feesTN_LPX.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_DRIVER = feesTN_LPX.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = feesTN_LPX.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_DRIVER = feesTN_LPX.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DRIVER = feesTN_LPX.TOTAL_AMOUNT;
                                            if (feesTN_LPX.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_LPX.DISCOUNT_DETAIL);
                                        }
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_CARGO == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_HH = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_CARGO = feesTN_HH.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_CARGO = feesTN_HH.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_CARGO = feesTN_HH.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = feesTN_HH.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_CARGO = feesTN_HH.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_CARGO = feesTN_HH.TOTAL_AMOUNT;
                                            if (feesTN_HH.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_HH.DISCOUNT_DETAIL);
                                        }
                                        if (lstDiscountVoluntary != null && lstDiscountVoluntary.Any())
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.DISCOUNT_DETAIL = lstDiscountVoluntary;
                                    }
                                }

                                // gán phí giảm của VCX bổ sung 12/05/2021
                                vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.DISCOUNT_DETAIL = productVer3.DISCOUNT_DETAIL;

                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }
                             */
                            #endregion

                            OutDynamicProductVer3 productVer3 = new OutDynamicProductVer3();
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                                productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.INX).FirstOrDefault();

                            if (vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "1")
                                productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.INX).FirstOrDefault();

                            // VCX
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesVCX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).FirstOrDefault();

                                    if (feesVCX != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = feesVCX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = feesVCX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = feesVCX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = feesVCX.TOTAL_DISCOUNT;
                                        //item.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = productVer3.TOTAL_ADDITIONAL;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = feesVCX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = feesVCX.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = 0;
                                    }

                                    // thêm thông tin phí gốc (chưa gồm bổ sung)
                                    ADDITIONAL_FEES feesVCX_NoBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).FirstOrDefault();
                                    if (feesVCX_NoBS != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = feesVCX_NoBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = feesVCX_NoBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = feesVCX_NoBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = feesVCX_NoBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = feesVCX_NoBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = feesVCX_NoBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = 0;
                                    }
                                }
                                if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                    ADDITIONAL_FEES feesVcxBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).FirstOrDefault();
                                    if (feesVcxBS != null)
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = feesVcxBS.TOTAL_AMOUNT;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = feesVcxBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = feesVcxBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = feesVcxBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = feesVcxBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = feesVcxBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = feesVcxBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = 0;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = 0;
                                    }

                                }
                                // gán phí bs vào danh sách truyền lên
                                if (vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL != null && vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Any())
                                {
                                    if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                    {
                                        List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                        List<PHYSICAL_DAMAGE_ADDITIONAL> lstAddPhy = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                                        for (var k = 0; k < vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Count; k++)
                                        {
                                            PHYSICAL_DAMAGE_ADDITIONAL addPhy = new PHYSICAL_DAMAGE_ADDITIONAL();
                                            var item = vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL[k];
                                            ADDITIONAL_FEES existAddi = lstOut.Where(x => x.CODE.Equals(item.KEY))?.FirstOrDefault();
                                            if (existAddi != null)
                                            {
                                                addPhy = JsonConvert.DeserializeObject<PHYSICAL_DAMAGE_ADDITIONAL>(JsonConvert.SerializeObject(item));
                                                addPhy.FEES = existAddi.FEES;
                                                addPhy.AMOUNT = existAddi.AMOUNT;
                                                addPhy.TOTAL_DISCOUNT = existAddi.TOTAL_DISCOUNT;
                                                addPhy.VAT = existAddi.VAT;
                                                addPhy.TOTAL_AMOUNT = existAddi.TOTAL_AMOUNT;
                                                // bổ sung 12/05/2021
                                                addPhy.DISCOUNT_ADDITIONAL = existAddi.DISCOUNT_DETAIL;
                                                lstAddPhy.Add(addPhy);
                                            }
                                        }
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL = lstAddPhy;
                                    }
                                }

                                // gán phí giảm của VCX bổ sung 12/05/2021
                                vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.DISCOUNT_DETAIL = productVer3.DISCOUNT_DETAIL;

                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }

                            // TNDS BB
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    // Phí TNDS BB
                                    ADDITIONAL_FEES feesBB = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.TNDSBB_F.ToString())).FirstOrDefault();
                                    if (feesBB != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES_DATA = feesBB.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES = feesBB.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.AMOUNT = feesBB.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.VAT = feesBB.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_DISCOUNT = feesBB.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_AMOUNT = feesBB.TOTAL_AMOUNT;

                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_AMOUNT = 0;
                                    }
                                }
                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }

                            // TNDS TN
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY_ALL == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    //bổ sung 12/05/2021
                                    List<DiscountInfo> lstDiscountVoluntary = new List<DiscountInfo>();

                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesTN = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).FirstOrDefault();
                                    if (feesTN != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = feesTN.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = feesTN.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = feesTN.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = feesTN.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = feesTN.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = feesTN.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = 0;
                                    }

                                    // gán phí chi tiết
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_T3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = feesTN_T3.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_INSUR = feesTN_T3.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = feesTN_T3.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = feesTN_T3.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_INSUR = feesTN_T3.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = feesTN_T3.TOTAL_AMOUNT;
                                        if (feesTN_T3.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_T3.DISCOUNT_DETAIL);


                                        ADDITIONAL_FEES feesTN_TST3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = feesTN_TST3.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_ASSETS = feesTN_TST3.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = feesTN_TST3.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = feesTN_TST3.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_ASSETS = feesTN_TST3.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = feesTN_TST3.TOTAL_AMOUNT;
                                        if (feesTN_TST3.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_TST3.DISCOUNT_DETAIL);

                                        ADDITIONAL_FEES feesTN_HK = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = feesTN_HK.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_PASSENGER = feesTN_HK.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_PASSENGER = feesTN_HK.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = feesTN_HK.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_PASSENGER = feesTN_HK.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_PASSENGER = feesTN_HK.TOTAL_AMOUNT;
                                        if (feesTN_HK.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_HK.DISCOUNT_DETAIL);

                                        ADDITIONAL_FEES feesTN_VQM = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = feesTN_VQM.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = feesTN_VQM.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = feesTN_VQM.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = feesTN_VQM.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = feesTN_VQM.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = feesTN_VQM.TOTAL_AMOUNT;
                                        if (feesTN_VQM.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_VQM.DISCOUNT_DETAIL);
                                    }
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_DRIVER == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_LPX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_DRIVER = feesTN_LPX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DRIVER = feesTN_LPX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_DRIVER = feesTN_LPX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = feesTN_LPX.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_DRIVER = feesTN_LPX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DRIVER = feesTN_LPX.TOTAL_AMOUNT;
                                        if (feesTN_LPX.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_LPX.DISCOUNT_DETAIL);
                                    }
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_CARGO == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_HH = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_CARGO = feesTN_HH.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_CARGO = feesTN_HH.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_CARGO = feesTN_HH.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = feesTN_HH.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_CARGO = feesTN_HH.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_CARGO = feesTN_HH.TOTAL_AMOUNT;
                                        if (feesTN_HH.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_HH.DISCOUNT_DETAIL);
                                    }
                                    if (lstDiscountVoluntary != null && lstDiscountVoluntary.Any())
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.DISCOUNT_DETAIL = lstDiscountVoluntary;
                                }
                            }

                            // gán tổng tiền của từng xe và cấp đơn
                            vehicleInsur.VEHICLE_INSUR[i].AMOUNT = amount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_DISCOUNT = total_discount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_VAT = vat;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_AMOUNT = total_amount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_ADDITIONAL = total_add;
                        }
                    }
                    else throw new Exception("Không tìm thấy sản phẩm để tính phí");

                    // kiểm tra
                    // n xe check tách đơn hay gộp đơn
                    // kiểm tra tiếp trong 1 xe thì VCX và TNDS tách đơn hay gộp đơn


                    // không tách đơn TH n xe
                    if (vehicleInsur.IS_SPLIT_ORDER == null || vehicleInsur.IS_SPLIT_ORDER == "0")
                    {
                        int count = vehicleInsur.VEHICLE_INSUR.Where(x => x.IS_SPLIT_ORDER == "1").Count();

                        if (count > 0) // có tách đơn trên từng xe
                        {
                        }
                        else // không tách đơn trên 1 xe
                        {
                            //var b = JsonConvert.SerializeObject(vehicleInsur);
                            OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderVehicleVer3(vehicleInsur);
                            var a = JsonConvert.SerializeObject(ordersModel);
                            response = ExeOrdVehicle(request, ordersModel, vehicleInsur, pay);
                        }
                    }
                    else //tách đơn TH n xe
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse CreateInsurVehicleVer2_Preview(BaseRequest request, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vehi Request Data: " + JsonConvert.SerializeObject(request.Data.ToString()), Logger.ConcatName(nameClass, nameMethod));
                VehicleInsur vehicleInsur = new VehicleInsur();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    vehicleInsur = JsonConvert.DeserializeObject<VehicleInsur>(request.Data.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vehi: " + JsonConvert.SerializeObject(vehicleInsur), Logger.ConcatName(nameClass, nameMethod));
                if (vehicleInsur != null && vehicleInsur.VEHICLE_INSUR != null && vehicleInsur.VEHICLE_INSUR.Any())
                {
                    // nếu là đại lý chụp ảnh thì phải validate file truyền lên
                    List<VEHICLE_PRODUCT> lstSur = vehicleInsur.VEHICLE_INSUR.Where(i => i.PHYSICAL_DAMAGE != null && i.IS_PHYSICAL == "1" && i.PHYSICAL_DAMAGE.SURVEYOR_TYPE.Equals(goConstants.SurveyorType.DLCA.ToString())).ToList();
                    if (lstSur != null && lstSur.Any())
                    {
                        List<VEHICLE_PRODUCT> lstFileSur = lstSur.Where(i => i.PHYSICAL_DAMAGE.SURVEYOR_FILES == null).ToList();
                        if (lstFileSur != null && lstFileSur.Any())
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                            return response;
                        }
                    }

                    // validate các define truyền lên
                    BaseValidate baseValidate = ValidateDefineVehicle(request, vehicleInsur);
                    if (!baseValidate.Success)
                        return goBusinessCommon.Instance.getResultApi(false, "", config, baseValidate.Error, baseValidate.ErrorMessage);

                    OutDynamicFeesVer3 outDynamic = new OutDynamicFeesVer3();
                    CalInsurVehicleVer3(ref outDynamic, ref vehicleInsur, request);


                    // 29/10/2021 bổ sung validate phí client truyền lên
                    if (outDynamic == null || outDynamic.PRODUCT_DETAIL == null && !outDynamic.PRODUCT_DETAIL.Any())
                        throw new Exception("Không tìm thấy sản phẩm để tính phí");
                    if (1 == 2)
                    {
                        BaseValidate validateFees = goBusinessValidateFees.Instance.ValidationFessVehicle(vehicleInsur, outDynamic);
                        if (!validateFees.Success)
                            return goBusinessCommon.Instance.getResultApi(false, "", config, validateFees.Error, validateFees.ErrorMessage);
                    }


                    //double amount = 0, total_discount = 0, vat = 0, total_amount = 0, total_add = 0; ;
                    string ORG_CODE = vehicleInsur.ORG_CODE, CHANNEL = vehicleInsur.CHANNEL, USERNAME = vehicleInsur.USERNAME, ACTION = vehicleInsur.ACTION;

                    if (outDynamic != null && outDynamic.PRODUCT_DETAIL != null && outDynamic.PRODUCT_DETAIL.Any())
                    {

                        for (var i = 0; i < vehicleInsur.VEHICLE_INSUR.Count(); i++)
                        {
                            double amount = 0, total_discount = 0, vat = 0, total_amount = 0, total_add = 0;

                            // sản phẩm VCX
                            #region Old
                            /*
                             
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                            {
                                OutDynamicProductVer3 productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.INX).FirstOrDefault();
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesVCX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).FirstOrDefault();

                                    if (feesVCX != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = feesVCX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = feesVCX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = feesVCX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = feesVCX.TOTAL_DISCOUNT;
                                        //item.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = productVer3.TOTAL_ADDITIONAL;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = feesVCX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = feesVCX.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = 0;
                                    }

                                    // thêm thông tin phí gốc (chưa gồm bổ sung)
                                    ADDITIONAL_FEES feesVCX_NoBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).FirstOrDefault();
                                    if (feesVCX_NoBS != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = feesVCX_NoBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = feesVCX_NoBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = feesVCX_NoBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = feesVCX_NoBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = feesVCX_NoBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = feesVCX_NoBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = 0;
                                    }
                                }
                                if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                    ADDITIONAL_FEES feesVcxBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).FirstOrDefault();
                                    if (feesVcxBS != null)
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = feesVcxBS.TOTAL_AMOUNT;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = feesVcxBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = feesVcxBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = feesVcxBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = feesVcxBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = feesVcxBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = feesVcxBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = 0;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = 0;
                                    }

                                }
                                // gán phí bs vào danh sách truyền lên
                                if (vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL != null && vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Any())
                                {
                                    if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                    {
                                        List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                        List<PHYSICAL_DAMAGE_ADDITIONAL> lstAddPhy = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                                        for (var k = 0; k < vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Count; k++)
                                        {
                                            PHYSICAL_DAMAGE_ADDITIONAL addPhy = new PHYSICAL_DAMAGE_ADDITIONAL();
                                            var item = vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL[k];
                                            ADDITIONAL_FEES existAddi = lstOut.Where(x => x.CODE.Equals(item.KEY))?.FirstOrDefault();
                                            if (existAddi != null)
                                            {
                                                addPhy = JsonConvert.DeserializeObject<PHYSICAL_DAMAGE_ADDITIONAL>(JsonConvert.SerializeObject(item));
                                                addPhy.FEES = existAddi.FEES;
                                                addPhy.AMOUNT = existAddi.AMOUNT;
                                                addPhy.TOTAL_DISCOUNT = existAddi.TOTAL_DISCOUNT;
                                                addPhy.VAT = existAddi.VAT;
                                                addPhy.TOTAL_AMOUNT = existAddi.TOTAL_AMOUNT;
                                                // bổ sung 12/05/2021
                                                addPhy.DISCOUNT_ADDITIONAL = existAddi.DISCOUNT_DETAIL;
                                                lstAddPhy.Add(addPhy);
                                            }
                                        }
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL = lstAddPhy;
                                    }
                                }

                                // gán phí TN
                                if ((vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == null || vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "0") && vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY_ALL == "1")
                                {
                                    if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                    {
                                        //bổ sung 12/05/2021
                                        List<DiscountInfo> lstDiscountVoluntary = new List<DiscountInfo>();

                                        List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                        ADDITIONAL_FEES feesTN = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).FirstOrDefault();
                                        if (feesTN != null)
                                        {
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = feesTN.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = feesTN.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = feesTN.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = feesTN.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = feesTN.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = feesTN.TOTAL_AMOUNT;
                                        }
                                        else
                                        {
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = 0;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = 0;
                                        }

                                        // gán phí chi tiết
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_T3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = feesTN_T3.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_INSUR = feesTN_T3.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = feesTN_T3.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = feesTN_T3.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_INSUR = feesTN_T3.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = feesTN_T3.TOTAL_AMOUNT;
                                            if (feesTN_T3.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_T3.DISCOUNT_DETAIL);


                                            ADDITIONAL_FEES feesTN_TST3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = feesTN_TST3.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_ASSETS = feesTN_TST3.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = feesTN_TST3.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = feesTN_TST3.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_ASSETS = feesTN_TST3.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = feesTN_TST3.TOTAL_AMOUNT;
                                            if (feesTN_TST3.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_TST3.DISCOUNT_DETAIL);

                                            ADDITIONAL_FEES feesTN_HK = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = feesTN_HK.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_PASSENGER = feesTN_HK.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_PASSENGER = feesTN_HK.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = feesTN_HK.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_PASSENGER = feesTN_HK.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_PASSENGER = feesTN_HK.TOTAL_AMOUNT;
                                            if (feesTN_HK.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_HK.DISCOUNT_DETAIL);

                                            ADDITIONAL_FEES feesTN_VQM = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = feesTN_VQM.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = feesTN_VQM.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = feesTN_VQM.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = feesTN_VQM.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = feesTN_VQM.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = feesTN_VQM.TOTAL_AMOUNT;
                                            if (feesTN_VQM.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_VQM.DISCOUNT_DETAIL);
                                        }
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_DRIVER == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_LPX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_DRIVER = feesTN_LPX.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DRIVER = feesTN_LPX.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_DRIVER = feesTN_LPX.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = feesTN_LPX.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_DRIVER = feesTN_LPX.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DRIVER = feesTN_LPX.TOTAL_AMOUNT;
                                            if (feesTN_LPX.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_LPX.DISCOUNT_DETAIL);
                                        }
                                        if (vehicleInsur.VEHICLE_INSUR[i].IS_CARGO == "1")
                                        {
                                            ADDITIONAL_FEES feesTN_HH = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).FirstOrDefault();
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_CARGO = feesTN_HH.FEES_DATA;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_CARGO = feesTN_HH.FEES;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_CARGO = feesTN_HH.AMOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = feesTN_HH.TOTAL_DISCOUNT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_CARGO = feesTN_HH.VAT;
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_CARGO = feesTN_HH.TOTAL_AMOUNT;
                                            if (feesTN_HH.DISCOUNT_DETAIL != null)
                                                lstDiscountVoluntary.AddRange(feesTN_HH.DISCOUNT_DETAIL);
                                        }
                                        if (lstDiscountVoluntary != null && lstDiscountVoluntary.Any())
                                            vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.DISCOUNT_DETAIL = lstDiscountVoluntary;
                                    }
                                }

                                // gán phí giảm của VCX bổ sung 12/05/2021
                                vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.DISCOUNT_DETAIL = productVer3.DISCOUNT_DETAIL;

                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }
                             */
                            #endregion

                            OutDynamicProductVer3 productVer3 = new OutDynamicProductVer3();
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                                productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.INX).FirstOrDefault();

                            if (vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "1")
                                productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.INX).FirstOrDefault();

                            // VCX
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_PHYSICAL == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesVCX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).FirstOrDefault();

                                    if (feesVCX != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = feesVCX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = feesVCX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = feesVCX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = feesVCX.TOTAL_DISCOUNT;
                                        //item.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = productVer3.TOTAL_ADDITIONAL;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = feesVCX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = feesVCX.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_AMOUNT = 0;
                                    }

                                    // thêm thông tin phí gốc (chưa gồm bổ sung)
                                    ADDITIONAL_FEES feesVCX_NoBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).FirstOrDefault();
                                    if (feesVCX_NoBS != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = feesVCX_NoBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = feesVCX_NoBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = feesVCX_NoBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = feesVCX_NoBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = feesVCX_NoBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = feesVCX_NoBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = 0;
                                    }
                                }
                                if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                    ADDITIONAL_FEES feesVcxBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).FirstOrDefault();
                                    if (feesVcxBS != null)
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = feesVcxBS.TOTAL_AMOUNT;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = feesVcxBS.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = feesVcxBS.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = feesVcxBS.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = feesVcxBS.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = feesVcxBS.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = feesVcxBS.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        // thêm tổng bổ sung
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = 0;

                                        // thông tin chi tiết tổng BS
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = 0;
                                    }

                                }
                                // gán phí bs vào danh sách truyền lên
                                if (vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL != null && vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Any())
                                {
                                    if (productVer3 != null && productVer3.ADDITI_DETAIL != null)
                                    {
                                        List<ADDITIONAL_FEES> lstOut = productVer3.ADDITI_DETAIL;
                                        List<PHYSICAL_DAMAGE_ADDITIONAL> lstAddPhy = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                                        for (var k = 0; k < vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL.Count; k++)
                                        {
                                            PHYSICAL_DAMAGE_ADDITIONAL addPhy = new PHYSICAL_DAMAGE_ADDITIONAL();
                                            var item = vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL[k];
                                            ADDITIONAL_FEES existAddi = lstOut.Where(x => x.CODE.Equals(item.KEY))?.FirstOrDefault();
                                            if (existAddi != null)
                                            {
                                                addPhy = JsonConvert.DeserializeObject<PHYSICAL_DAMAGE_ADDITIONAL>(JsonConvert.SerializeObject(item));
                                                addPhy.FEES = existAddi.FEES;
                                                addPhy.AMOUNT = existAddi.AMOUNT;
                                                addPhy.TOTAL_DISCOUNT = existAddi.TOTAL_DISCOUNT;
                                                addPhy.VAT = existAddi.VAT;
                                                addPhy.TOTAL_AMOUNT = existAddi.TOTAL_AMOUNT;
                                                // bổ sung 12/05/2021
                                                addPhy.DISCOUNT_ADDITIONAL = existAddi.DISCOUNT_DETAIL;
                                                lstAddPhy.Add(addPhy);
                                            }
                                        }
                                        vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.ADDITIONAL = lstAddPhy;
                                    }
                                }

                                // gán phí giảm của VCX bổ sung 12/05/2021
                                vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.DISCOUNT_DETAIL = productVer3.DISCOUNT_DETAIL;

                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }

                            // TNDS BB
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_COMPULSORY == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    // Phí TNDS BB
                                    ADDITIONAL_FEES feesBB = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.TNDSBB_F.ToString())).FirstOrDefault();
                                    if (feesBB != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES_DATA = feesBB.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES = feesBB.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.AMOUNT = feesBB.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.VAT = feesBB.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_DISCOUNT = feesBB.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_AMOUNT = feesBB.TOTAL_AMOUNT;

                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.TOTAL_AMOUNT = 0;
                                    }
                                }
                                // gán tổng tiền của đơn
                                amount += productVer3.AMOUNT;
                                total_discount += productVer3.TOTAL_DISCOUNT;
                                vat += productVer3.VAT;
                                total_amount += productVer3.TOTAL_AMOUNT;
                                total_add += productVer3.TOTAL_ADD;
                            }

                            // TNDS TN
                            if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY_ALL == "1")
                            {
                                if (productVer3 != null && productVer3.OUT_DETAIL != null)
                                {
                                    //bổ sung 12/05/2021
                                    List<DiscountInfo> lstDiscountVoluntary = new List<DiscountInfo>();

                                    List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                                    ADDITIONAL_FEES feesTN = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).FirstOrDefault();
                                    if (feesTN != null)
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = feesTN.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = feesTN.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = feesTN.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = feesTN.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = feesTN.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = feesTN.TOTAL_AMOUNT;
                                    }
                                    else
                                    {
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT = 0;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_AMOUNT = 0;
                                    }

                                    // gán phí chi tiết
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_VOLUNTARY == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_T3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = feesTN_T3.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_INSUR = feesTN_T3.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = feesTN_T3.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = feesTN_T3.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_INSUR = feesTN_T3.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = feesTN_T3.TOTAL_AMOUNT;
                                        if (feesTN_T3.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_T3.DISCOUNT_DETAIL);


                                        ADDITIONAL_FEES feesTN_TST3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = feesTN_TST3.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_3RD_ASSETS = feesTN_TST3.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = feesTN_TST3.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = feesTN_TST3.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_3RD_ASSETS = feesTN_TST3.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = feesTN_TST3.TOTAL_AMOUNT;
                                        if (feesTN_TST3.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_TST3.DISCOUNT_DETAIL);

                                        ADDITIONAL_FEES feesTN_HK = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = feesTN_HK.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_PASSENGER = feesTN_HK.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_PASSENGER = feesTN_HK.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = feesTN_HK.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_PASSENGER = feesTN_HK.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_PASSENGER = feesTN_HK.TOTAL_AMOUNT;
                                        if (feesTN_HK.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_HK.DISCOUNT_DETAIL);

                                        ADDITIONAL_FEES feesTN_VQM = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = feesTN_VQM.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = feesTN_VQM.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = feesTN_VQM.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = feesTN_VQM.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = feesTN_VQM.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = feesTN_VQM.TOTAL_AMOUNT;
                                        if (feesTN_VQM.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_VQM.DISCOUNT_DETAIL);
                                    }
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_DRIVER == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_LPX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_DRIVER = feesTN_LPX.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DRIVER = feesTN_LPX.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_DRIVER = feesTN_LPX.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = feesTN_LPX.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_DRIVER = feesTN_LPX.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DRIVER = feesTN_LPX.TOTAL_AMOUNT;
                                        if (feesTN_LPX.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_LPX.DISCOUNT_DETAIL);
                                    }
                                    if (vehicleInsur.VEHICLE_INSUR[i].IS_CARGO == "1")
                                    {
                                        ADDITIONAL_FEES feesTN_HH = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).FirstOrDefault();
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_DATA_CARGO = feesTN_HH.FEES_DATA;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.FEES_CARGO = feesTN_HH.FEES;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.AMOUNT_CARGO = feesTN_HH.AMOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = feesTN_HH.TOTAL_DISCOUNT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.VAT_CARGO = feesTN_HH.VAT;
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.TOTAL_CARGO = feesTN_HH.TOTAL_AMOUNT;
                                        if (feesTN_HH.DISCOUNT_DETAIL != null)
                                            lstDiscountVoluntary.AddRange(feesTN_HH.DISCOUNT_DETAIL);
                                    }
                                    if (lstDiscountVoluntary != null && lstDiscountVoluntary.Any())
                                        vehicleInsur.VEHICLE_INSUR[i].VOLUNTARY_CIVIL.DISCOUNT_DETAIL = lstDiscountVoluntary;
                                }
                            }

                            // gán tổng tiền của từng xe và cấp đơn
                            vehicleInsur.VEHICLE_INSUR[i].AMOUNT = amount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_DISCOUNT = total_discount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_VAT = vat;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_AMOUNT = total_amount;
                            vehicleInsur.VEHICLE_INSUR[i].TOTAL_ADDITIONAL = total_add;
                        }
                    }
                    else throw new Exception("Không tìm thấy sản phẩm để tính phí");

                    // kiểm tra
                    // n xe check tách đơn hay gộp đơn
                    // kiểm tra tiếp trong 1 xe thì VCX và TNDS tách đơn hay gộp đơn


                    // không tách đơn TH n xe
                    if (vehicleInsur.IS_SPLIT_ORDER == null || vehicleInsur.IS_SPLIT_ORDER == "0")
                    {
                        int count = vehicleInsur.VEHICLE_INSUR.Where(x => x.IS_SPLIT_ORDER == "1").Count();

                        if (count > 0) // có tách đơn trên từng xe
                        {
                        }
                        else // không tách đơn trên 1 xe
                        {
                            //var b = JsonConvert.SerializeObject(vehicleInsur);
                            OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderVehicleVer3(vehicleInsur);
                            var a = JsonConvert.SerializeObject(ordersModel);
                            response = ExeOrdVehicle_Preview(request, ordersModel, vehicleInsur, pay);
                        }
                    }
                    else //tách đơn TH n xe
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse ExeOrdVehicle(BaseRequest request, OrdersModel ordersModel, VehicleInsur vehicleInsur, bool pay = false)
        {
            try
            {
                BaseResponse response = new BaseResponse();
                if (pay)
                {
                    ordersModel.COMMON.PAY_INFO = vehicleInsur.PAY_INFO;
                    if (vehicleInsur != null && vehicleInsur.PAY_INFO != null && ordersModel != null && ordersModel.COMMON != null && ordersModel.COMMON.ORDER != null && vehicleInsur.PAY_INFO.PAYMENT_TYPE.Equals(nameof(goConstants.Payment_type.TH)))
                    {
                        ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();
                    }
                }
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                if (ordersModel.COMMON.SELLER.ORG_CODE == goConstants.HDI_E_COM)
                {
                    new Task(() => {
                        if (pay)
                            goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                        else
                            goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                    }).Start();
                    response = goBusinessCommon.Instance.getResultApi(true, "", false, "", "");
                    return response;
                }
                else
                {
                    if (pay)
                        response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                    else
                        response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BaseResponse ExeOrdVehicle_Preview(BaseRequest request, OrdersModel ordersModel, VehicleInsur vehicleInsur, bool pay = false)
        {
            try
            {
                BaseResponse response = new BaseResponse();
                if (pay)
                {
                    ordersModel.COMMON.PAY_INFO = vehicleInsur.PAY_INFO;
                    if (vehicleInsur != null && vehicleInsur.PAY_INFO != null && ordersModel != null && ordersModel.COMMON != null && ordersModel.COMMON.ORDER != null && vehicleInsur.PAY_INFO.PAYMENT_TYPE.Equals(nameof(goConstants.Payment_type.TH)))
                    {
                        ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();
                    }
                }
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay_Preview(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay_Preview(reqOrd);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BaseValidate ValidateDefineVehicle(BaseRequest request, VehicleInsur vehicleInsur)
        {
            BaseValidate baseValidate = new BaseValidate();
            baseValidate.Success = true;
            try
            {
                //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "1", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                if (vehicleInsur != null && vehicleInsur.VEHICLE_INSUR != null && vehicleInsur.VEHICLE_INSUR.Any())
                {
                    foreach (var item in vehicleInsur.VEHICLE_INSUR)
                    {
                        string keyNotBuy = "";

                        // kiểm tra biến truyền lên theo từng giá trị

                        if (item.NONE_NUMBER_PLATE == null || !goUtility.Instance.CastToBoolean(item.NONE_NUMBER_PLATE))
                        {
                            if (string.IsNullOrEmpty(item.NUMBER_PLATE))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_18), goConstantsValidate.ERR_CHK_18);
                        }

                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "2", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                        if (item.IS_PHYSICAL == "1" && item.IS_COMPULSORY == "0") // với VCX
                        {
                            string productCode = item.PHYSICAL_DAMAGE.PRODUCT_CODE;
                            string category = goConstants.CATEGORY_XE;

                            string key_init_Crit = goConstantsRedis.PrefixDynamicFess + category;
                            string key_init_Product = key_init_Crit + productCode;
                            keyNotBuy = key_init_Product + goConstantsRedis.PrefixNotBuy;
                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "3", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            // kiểm tra cache
                            goBusinessCalculateFees.Instance.CheckInitCaching(key_init_Product, request, vehicleInsur.CHANNEL, vehicleInsur.USERNAME, vehicleInsur.ORG_CODE, category, productCode);
                            // danh sách define của sản phẩm
                            string keyDefine = key_init_Product + goConstantsRedis.PrefixDefineFees;
                            List<ProductDefineFees> lstDefine = goBusinessCache.Instance.Get<List<ProductDefineFees>>(keyDefine);
                            if (lstDefine == null || !lstDefine.Any())
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_01), goConstantsValidate.ERR_CHK_01);
                            // validate data
                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "4", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            #region validate data
                            // nhóm xe
                            if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.VH_TYPE.ToString(), item.VEHICLE_TYPE, null, false))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_02), goConstantsValidate.ERR_CHK_02);

                            //số tiền BH xe
                            if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_PRICE.ToString(), item.VEHICLE_VALUE_CODE, item.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT.ToString(), true, true))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_03), goConstantsValidate.ERR_CHK_03);

                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "5", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            //Mục đích KD
                            if (!string.IsNullOrEmpty(item.VEHICLE_USE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_PURPOSE.ToString(), item.VEHICLE_USE, null, false))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_07), goConstantsValidate.ERR_CHK_07);
                            }

                            // Số chỗ
                            if (!string.IsNullOrEmpty(item.SEAT_NO) || !string.IsNullOrEmpty(item.SEAT_CODE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_SEAT.ToString(), item.SEAT_CODE, item.SEAT_NO, true, false))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_06), goConstantsValidate.ERR_CHK_06);
                            }

                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "6", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            // trọng tải
                            if (!string.IsNullOrEmpty(item.WEIGH) || !string.IsNullOrEmpty(item.WEIGH_CODE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_WEIGHT.ToString(), item.WEIGH_CODE, item.WEIGH, true, true))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_08), goConstantsValidate.ERR_CHK_08);
                            }

                            // Năm sử dụng
                            int year_use = goUtility.Instance.ConvertToDateTime(item.PHYSICAL_DAMAGE.EFF, DateTime.MinValue).Year - goUtility.Instance.ConvertToInt32(item.MFG, 9999);
                            if (year_use < 0)
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_04), goConstantsValidate.ERR_CHK_04);
                            if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.YEAR_OF_USE.ToString(), item.VEHICLE_REGIS_CODE, year_use.ToString(), true, true))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_05), goConstantsValidate.ERR_CHK_05);

                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "7", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            // kiểm tra file nếu có gửi lên
                            if (item.PHYSICAL_DAMAGE.SURVEYOR_TYPE.Equals(goConstants.SurveyorType.DLCA) && (item.PHYSICAL_DAMAGE.SURVEYOR_FILES == null || !item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Any()))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_10), goConstantsValidate.ERR_CHK_10);

                            //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "8", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                            if (item.PHYSICAL_DAMAGE.SURVEYOR_FILES != null && item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Any())
                            {
                                // nếu có file giám định yêu cầu đủ
                                if (item.NONE_NUMBER_PLATE != null && goUtility.Instance.CastToBoolean(item.NONE_NUMBER_PLATE))
                                {
                                    var lstExit = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.HDON_XE.ToString()));
                                    if (lstExit == null || !lstExit.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_11), goConstantsValidate.ERR_CHK_11);
                                }
                                else
                                {
                                    var lstGDKX_MS = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.GDKX_MS.ToString()));
                                    if (lstGDKX_MS == null || !lstGDKX_MS.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_12), goConstantsValidate.ERR_CHK_12);

                                    var lstSDKX_MT = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.SDKX_MT.ToString()));
                                    if (lstSDKX_MT == null || !lstSDKX_MT.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_13), goConstantsValidate.ERR_CHK_13);

                                    var lstGDAUP_B = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.GDAUP_B.ToString()));
                                    if (lstGDAUP_B == null || !lstGDAUP_B.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_14), goConstantsValidate.ERR_CHK_14);

                                    var lstGDAUT_B = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.GDAUT_B.ToString()));
                                    if (lstGDAUT_B == null || !lstGDAUT_B.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_15), goConstantsValidate.ERR_CHK_15);

                                    var lstGDUOIP_B = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.GDUOIP_B.ToString()));
                                    if (lstGDUOIP_B == null || !lstGDUOIP_B.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_16), goConstantsValidate.ERR_CHK_16);

                                    var lstGDUOIT_B = item.PHYSICAL_DAMAGE.SURVEYOR_FILES.Where(x => x.FILE_TYPE.Equals(goConstants.FileType.GDUOIT_B.ToString()));
                                    if (lstGDUOIT_B == null || !lstGDUOIT_B.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_17), goConstantsValidate.ERR_CHK_17);
                                }
                            }
                            #endregion

                            // kiểm tra giờ phút hiệu lực phải trùng giờ phút hết hiệu lực
                            if (!item.PHYSICAL_DAMAGE.TIME_EFF.Equals(item.PHYSICAL_DAMAGE.TIME_EXP))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_09), goConstantsValidate.ERR_CHK_09);

                        }

                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "9", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                        if (item.IS_COMPULSORY == "1")
                        {
                            string productCode = item.COMPULSORY_CIVIL.PRODUCT_CODE;
                            string category = goConstants.CATEGORY_XE;

                            string key_init_Crit = goConstantsRedis.PrefixDynamicFess + category;
                            string key_init_Product = key_init_Crit + productCode;
                            keyNotBuy = key_init_Product + goConstantsRedis.PrefixNotBuy;

                            // kiểm tra cache
                            goBusinessCalculateFees.Instance.CheckInitCaching(key_init_Product, request, vehicleInsur.CHANNEL, vehicleInsur.USERNAME, vehicleInsur.ORG_CODE, category, productCode);
                            // danh sách define của sản phẩm
                            string keyDefine = key_init_Product + goConstantsRedis.PrefixDefineFees;
                            List<ProductDefineFees> lstDefine = goBusinessCache.Instance.Get<List<ProductDefineFees>>(keyDefine);
                            // validate data
                            #region validate data
                            // nhóm xe
                            if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.VH_TYPE.ToString(), item.VEHICLE_TYPE, null, false))
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_02), goConstantsValidate.ERR_CHK_02);

                            //số tiền BH xe
                            //if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_PRICE.ToString(), item.VEHICLE_VALUE_CODE, item.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT.ToString(), true, true))
                            //    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_03), goConstantsValidate.ERR_CHK_03);

                            //Mục đích KD
                            if (!string.IsNullOrEmpty(item.VEHICLE_USE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_PURPOSE.ToString(), item.VEHICLE_USE, null, false))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_07), goConstantsValidate.ERR_CHK_07);
                            }

                            // Số chỗ
                            if (!string.IsNullOrEmpty(item.SEAT_NO) || !string.IsNullOrEmpty(item.SEAT_CODE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_SEAT.ToString(), item.SEAT_CODE, item.SEAT_NO, true, false))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_06), goConstantsValidate.ERR_CHK_06);
                            }

                            // trọng tải
                            if (!string.IsNullOrEmpty(item.WEIGH) || !string.IsNullOrEmpty(item.WEIGH_CODE))
                            {
                                if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.CAR_WEIGHT.ToString(), item.WEIGH_CODE, item.WEIGH, true, true))
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_08), goConstantsValidate.ERR_CHK_08);
                            }

                            // Năm sử dụng
                            //int year_use = goUtility.Instance.ConvertToDateTime(item.PHYSICAL_DAMAGE.EFF, DateTime.MinValue).Year - goUtility.Instance.ConvertToInt32(item.MFG, 9999);
                            //if (year_use < 0)
                            //    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_04), goConstantsValidate.ERR_CHK_04);
                            //if (!CheckDefine(lstDefine, goConstants.KeyDefineFees.YEAR_OF_USE.ToString(), item.VEHICLE_REGIS_CODE, year_use.ToString(), true, true))
                            //    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_05), goConstantsValidate.ERR_CHK_05);
                            #endregion

                            // kiểm tra giờ phút hiệu lực phải trùng giờ phút hết hiệu lực
                            //KhanhPT bỏ valide Giờ kết thúc phải bằng giờ hiệu lực
                            //if (!item.COMPULSORY_CIVIL.TIME_EFF.Equals(item.COMPULSORY_CIVIL.TIME_EXP))
                            //    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_09), goConstantsValidate.ERR_CHK_09);
                        }

                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "10", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                        // Kiểm tra cho bán hay không
                        List<VehicleNotBuy> lstNotBuy = goBusinessCache.Instance.Get<List<VehicleNotBuy>>(keyNotBuy);
                        if (lstNotBuy != null && lstNotBuy.Any())
                        {
                            string IS_3RD = "0", IS_3RD_ASSETS = "0", IS_PASSENGER = "0";

                            if (item.VOLUNTARY_CIVIL != null)
                            {
                                if (!string.IsNullOrEmpty(item.VOLUNTARY_CIVIL.INSUR_3RD))
                                    IS_3RD = "1";
                                if (!string.IsNullOrEmpty(item.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS))
                                    IS_3RD_ASSETS = "1";
                                if (!string.IsNullOrEmpty(item.VOLUNTARY_CIVIL.INSUR_PASSENGER))
                                    IS_PASSENGER = "1";
                            }

                            List<VehicleNotBuy> lstExists = lstNotBuy.Where(x => !string.IsNullOrEmpty(x.VEHICLE_GROUP) && x.VEHICLE_GROUP.Equals(item.VEHICLE_GROUP) &&
                                  !string.IsNullOrEmpty(x.VEHICLE_TYPE) && x.VEHICLE_TYPE.Equals(item.VEHICLE_TYPE) && !string.IsNullOrEmpty(x.VEHICLE_USE) && x.VEHICLE_USE.Equals(item.VEHICLE_USE)).ToList();
                            if (lstExists != null && lstExists.Any())
                            {
                                // cho bán VCX không
                                if (item.IS_PHYSICAL == "1")
                                {
                                    List<VehicleNotBuy> lstExistsNotBuy = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_PHYSICAL) && x.IS_PHYSICAL.Equals(item.IS_PHYSICAL)).ToList();
                                    if (lstExistsNotBuy != null && lstExistsNotBuy.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_19), goConstantsValidate.ERR_CHK_19);
                                }

                                // cho bán TN không
                                if (item.IS_VOLUNTARY_ALL == "1")
                                {
                                    List<VehicleNotBuy> lstExistsNotBuy = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_VOLUNTARY_ALL) && x.IS_VOLUNTARY_ALL.Equals(item.IS_VOLUNTARY_ALL)).ToList();
                                    if (lstExistsNotBuy != null && lstExistsNotBuy.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_20), goConstantsValidate.ERR_CHK_20);

                                    // cho bán TN VQM không
                                    if (item.IS_VOLUNTARY == "1")
                                    {
                                        List<VehicleNotBuy> lstExistsNotBuyVqm = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_VOLUNTARY) && x.IS_VOLUNTARY.Equals(item.IS_VOLUNTARY)).ToList();
                                        if (lstExistsNotBuyVqm != null && lstExistsNotBuyVqm.Any())
                                            return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_21), goConstantsValidate.ERR_CHK_21);

                                        // cho bán TN VQM Thân thể t3 không
                                        if (IS_3RD == "1")
                                        {
                                            List<VehicleNotBuy> lstNotBuy3rd = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_3RD) && x.IS_3RD.Equals(IS_3RD)).ToList();
                                            if (lstNotBuy3rd != null && lstNotBuy3rd.Any())
                                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_22), goConstantsValidate.ERR_CHK_22);
                                        }
                                        // cho bán TN VQM hàng hóa t3 không
                                        if (IS_3RD_ASSETS == "1")
                                        {
                                            List<VehicleNotBuy> lstNotBuy3rdAsset = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_3RD_ASSETS) && x.IS_3RD_ASSETS.Equals(IS_3RD_ASSETS)).ToList();
                                            if (lstNotBuy3rdAsset != null && lstNotBuy3rdAsset.Any())
                                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_23), goConstantsValidate.ERR_CHK_23);
                                        }
                                        // cho bán TN VQM hành khách
                                        if (IS_PASSENGER == "1")
                                        {
                                            List<VehicleNotBuy> lstNotBuyPassenger = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_PASSENGER) && x.IS_PASSENGER.Equals(IS_PASSENGER)).ToList();
                                            if (lstNotBuyPassenger != null && lstNotBuyPassenger.Any())
                                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_24), goConstantsValidate.ERR_CHK_24);
                                        }
                                    }
                                    // cho bán TN LPX không
                                    if (item.IS_DRIVER == "1")
                                    {
                                        List<VehicleNotBuy> lstExistsNotBuyDriver = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_DRIVER) && x.IS_DRIVER.Equals(item.IS_DRIVER)).ToList();
                                        if (lstExistsNotBuyDriver != null && lstExistsNotBuyDriver.Any())
                                            return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_25), goConstantsValidate.ERR_CHK_25);
                                    }
                                    // cho bán TN Hàng hóa không
                                    if (item.IS_CARGO == "1")
                                    {
                                        List<VehicleNotBuy> lstExistsNotBuyCargo = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_CARGO) && x.IS_CARGO.Equals(item.IS_CARGO)).ToList();
                                        if (lstExistsNotBuyCargo != null && lstExistsNotBuyCargo.Any())
                                            return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_26), goConstantsValidate.ERR_CHK_26);
                                    }
                                }

                                // cho bán TNDS BB không
                                if (item.IS_COMPULSORY == "1")
                                {
                                    List<VehicleNotBuy> lstExistsNotBuy = lstExists.Where(x => !string.IsNullOrEmpty(x.IS_COMPULSORY) && x.IS_COMPULSORY.Equals(item.IS_COMPULSORY)).ToList();
                                    if (lstExistsNotBuy != null && lstExistsNotBuy.Any())
                                        return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_27), goConstantsValidate.ERR_CHK_27);
                                }
                            }
                        }

                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "11", Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                    }
                }
                return baseValidate;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, "ValidateDefineVehicle"));
                throw ex;
            }
        }

        public bool CheckDefine(List<ProductDefineFees> lstDefine, string defineCode, string typeCode, string value, bool isValue, bool isRange = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                bool chk = true;
                if (lstDefine != null)
                {
                    List<ProductDefineFees> lstExists = lstDefine.Where(i => i.DEFINE_CODE.Equals(defineCode) && i.TYPE_CODE.Equals(typeCode)).ToList();
                    List<ProductDefineFees> lstExists2 = lstDefine.Where(i => i.DEFINE_CODE.Equals(defineCode)).ToList();
                    if (lstExists == null || !lstExists.Any())
                        return false;
                    if (!isValue)
                    {
                        return chk;
                    }
                    else
                    {
                        if (isRange)
                        {
                            chk = false;
                            // operator > < >= <= (nếu là = thì k rơi vào range)
                            foreach (var item in lstExists)
                            {
                                double dValue = goUtility.Instance.ConvertToDouble(value, -999);
                                double dMin = goUtility.Instance.ConvertToDouble(item.MIN_VALUE, -1);
                                double dMax = goUtility.Instance.ConvertToDouble(item.MAX_VALUE, -2);
                                bool chkRange = CheckRangeData(dValue, item.OPERATOR_MIN, dMin, item.OPERATOR_MAX, dMax);
                                if (chkRange)
                                {
                                    chk = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            List<ProductDefineFees> lstData = lstExists.Where(i => i.VALUE.Equals(value)).ToList();
                            if (lstData == null || !lstData.Any())
                                chk = false;
                        }
                    }
                }
                return chk;
            }
            catch (Exception ex)
            {
                return false;
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "d= " + defineCode + ". t= " + typeCode + ". v= " + value + ". ex= " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
        }

        public bool CheckRangeData(double value, string operator_min, double min, string operator_max, double max)
        {
            try
            {
                bool chk = false;

                if (operator_min.Equals(">") && operator_max.Equals("<"))
                {
                    if (value > min && value < max)
                        chk = true;
                }
                if (operator_min.Equals(">") && operator_max.Equals("<="))
                {
                    if (value > min && value <= max)
                        chk = true;
                }
                if (operator_min.Equals(">=") && operator_max.Equals("<"))
                {
                    if (value >= min && value < max)
                        chk = true;
                }
                if (operator_min.Equals(">=") && operator_max.Equals("<="))
                {
                    if (value >= min && value <= max)
                        chk = true;
                }
                return chk;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckDefineOrNull(List<ProductDefineFees> lstDefine, string defineCode, string typeCode, string value, bool isValue, bool isRange = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                bool chk = true;
                if (lstDefine != null)
                {
                    List<ProductDefineFees> lstExists = lstDefine.Where(i => i.DEFINE_CODE.Equals(defineCode) && i.TYPE_CODE.Equals(typeCode)).ToList();
                    List<ProductDefineFees> lstExists2 = lstDefine.Where(i => i.DEFINE_CODE.Equals(defineCode)).ToList();
                    if (lstExists == null || !lstExists.Any())
                        return true;
                    if (!isValue)
                    {
                        return chk;
                    }
                    else
                    {
                        if (isRange)
                        {
                            chk = false;
                            // operator > < >= <= (nếu là = thì k rơi vào range)
                            foreach (var item in lstExists)
                            {
                                double dValue = goUtility.Instance.ConvertToDouble(value, -999);
                                double dMin = goUtility.Instance.ConvertToDouble(item.MIN_VALUE, -1);
                                double dMax = goUtility.Instance.ConvertToDouble(item.MAX_VALUE, -2);
                                bool chkRange = CheckRangeData(dValue, item.OPERATOR_MIN, dMin, item.OPERATOR_MAX, dMax);
                                if (chkRange)
                                {
                                    chk = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            List<ProductDefineFees> lstData = lstExists.Where(i => i.VALUE.Equals(value)).ToList();
                            if (lstData == null || !lstData.Any())
                                chk = false;
                        }
                    }
                }
                return chk;
            }
            catch (Exception ex)
            {
                return false;
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "d= " + defineCode + ". t= " + typeCode + ". v= " + value + ". ex= " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
        }
        /// <summary>
        /// Tính lại phí bảo hiểm xe
        /// </summary>
        public void CalInsurVehicle(BaseRequest request, string org_code, string channel, string username, ref VEHICLE_PRODUCT vehicleProduct, ref OutDynamicFees feesProduct)
        {
            InputDynamicFeesVer2 inputFees = new InputDynamicFeesVer2();
            inputFees.ORG_CODE = org_code;
            inputFees.CHANNEL = channel;
            inputFees.USERNAME = channel;
            inputFees.GIFTS = vehicleProduct.GIFTS;

            List<DynamicProduct> lstProduct = new List<DynamicProduct>();

            if (vehicleProduct.COMPULSORY_CIVIL != null)
            {
                // BBPLA_CNGOI+BBPLA_TTAI+BBPLA_LXE
                DynamicProduct inputProduct = new DynamicProduct();
                inputProduct.CATEGORY = goConstants.CATEGORY_XE.ToString();
                inputProduct.PRODUCT_CODE = goConstants.Product_Xe.XCG_TNDSBB.ToString();
                inputProduct.STBH = "";
                //KhanhPT Add EXP, EFF
                inputProduct.EFF = vehicleProduct.COMPULSORY_CIVIL.EFF;
                inputProduct.EXP = vehicleProduct.COMPULSORY_CIVIL.EXP;
                //End

                List<DynamicData> lstDynamics = new List<DynamicData>();
                DynamicData dyn = new DynamicData();
                dyn.KEY_CODE = "BBPLA_LXE";
                dyn.VAL_CODE = vehicleProduct.VEHICLE_TYPE;
                lstDynamics.Add(dyn);

                DynamicData dynTtai = new DynamicData();
                dynTtai.KEY_CODE = "BBPLA_TTAI";
                dynTtai.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.WEIGH_CODE;
                lstDynamics.Add(dynTtai);

                DynamicData dynCnguoi = new DynamicData();
                dynCnguoi.KEY_CODE = "BBPLA_CNGOI";
                dynCnguoi.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.SEAT_CODE;
                lstDynamics.Add(dynCnguoi);

                DynamicData dynMdkd = new DynamicData();
                dynMdkd.KEY_CODE = "BBPLA_MDKD";
                dynMdkd.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.VEHICLE_USE;
                lstDynamics.Add(dynMdkd);

                DynamicData dynCnguoiMdkd = new DynamicData();
                dynCnguoiMdkd.KEY_CODE = "BBPLA_MDKD_CNGOI";
                dynCnguoiMdkd.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.VEHICLE_USE + "@" + vehicleProduct.SEAT_CODE;
                lstDynamics.Add(dynCnguoiMdkd);

                //TimeSpan timeSpan = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EXP, DateTime.MinValue) - goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EFF, DateTime.MinValue);
                //int day = timeSpan.Days;
                //2022-12-18 KhanhPT hiệu chỉnh tính theo nguyên tắc 365 ngày áp dụng cho cả Bắt buộc, tự nguyện, VCX
                DateTime dExp = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EXP, DateTime.MinValue);
                DateTime dEff = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EFF, DateTime.MinValue);
                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);
                //End

                DynamicData dynDay = new DynamicData();
                if (day < 30)
                {
                    dynDay.KEY_CODE = "HS_D30D";
                    dynDay.VAL_CODE = "1";
                    dynDay.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                }
                else
                {
                    dynDay.KEY_CODE = "HS_T30D";
                    dynDay.VAL_CODE = day.ToString();
                    dynDay.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                }
                lstDynamics.Add(dynDay);

                inputProduct.DYNAMIC_FEES = lstDynamics;
                lstProduct.Add(inputProduct);
                //OutDynamicFees outFees = goBusinessCalculateFees.Instance.GetOutCalFees(inputDynamic, request);

                //double fees = goBusinessCalculateFees.Instance.CalculateFeesInCache(inputDynamic, request);
                //double vat = fees * 10 / 100;
                //vehicleProduct.COMPULSORY_CIVIL.AMOUNT = fees - vat;
                //vehicleProduct.COMPULSORY_CIVIL.VAT = vat;
                //vehicleProduct.COMPULSORY_CIVIL.DISCOUNT = 0;
                //vehicleProduct.COMPULSORY_CIVIL.TOTAL_DISCOUNT = 0;
                //vehicleProduct.COMPULSORY_CIVIL.TOTAL_AMOUNT = fees;
            }

            if (vehicleProduct.VOLUNTARY_CIVIL != null && vehicleProduct.IS_VOLUNTARY_ALL == "1")
            {
                DynamicProduct inputProduct = new DynamicProduct();
                inputProduct.CATEGORY = goConstants.CATEGORY_XE.ToString();
                inputProduct.PRODUCT_CODE = vehicleProduct.VOLUNTARY_CIVIL.PRODUCT_CODE == null ? goConstants.Product_Xe.XCG_TNDSTN.ToString() : vehicleProduct.VOLUNTARY_CIVIL.PRODUCT_CODE;
                inputProduct.STBH = "";
                List<DynamicData> lstDynamics = new List<DynamicData>();

                DynamicData dynCngoi = new DynamicData();
                dynCngoi.KEY_CODE = "TN_CNGOI";
                dynCngoi.VAL_CODE = vehicleProduct.SEAT_CODE;
                lstDynamics.Add(dynCngoi);

                if (vehicleProduct.IS_VOLUNTARY != null && vehicleProduct.IS_VOLUNTARY == "1")
                {
                    DynamicData dynT3Lxe = new DynamicData();
                    dynT3Lxe.KEY_CODE = "TN_N3_LXE";
                    dynT3Lxe.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD;
                    lstDynamics.Add(dynT3Lxe);

                    DynamicData dynT3Ttai = new DynamicData();
                    dynT3Ttai.KEY_CODE = "TN_N3_TTAI";
                    dynT3Ttai.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.WEIGH_CODE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD;
                    lstDynamics.Add(dynT3Ttai);

                    DynamicData dynT3Cnguoi = new DynamicData();
                    dynT3Cnguoi.KEY_CODE = "TN_N3_CNGOI";
                    dynT3Cnguoi.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.SEAT_CODE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD;
                    lstDynamics.Add(dynT3Cnguoi);

                    DynamicData dynT3Hk = new DynamicData();
                    dynT3Hk.KEY_CODE = "TN_TH_HK";
                    dynT3Hk.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.VEHICLE_USE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_PASSENGER;
                    lstDynamics.Add(dynT3Hk);

                    DynamicData dynTsLxe = new DynamicData();
                    dynTsLxe.KEY_CODE = "TN_TSN3_LXE";
                    dynTsLxe.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    lstDynamics.Add(dynTsLxe);

                    DynamicData dynTsTtai = new DynamicData();
                    dynTsTtai.KEY_CODE = "TN_TSN3_TTAI";
                    dynTsTtai.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.WEIGH_CODE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    lstDynamics.Add(dynTsTtai);

                    DynamicData dynTsCnguoi = new DynamicData();
                    dynTsCnguoi.KEY_CODE = "TN_TSN3_CNGOI";
                    dynTsCnguoi.VAL_CODE = vehicleProduct.VEHICLE_TYPE + "@" + vehicleProduct.SEAT_CODE + "@" + vehicleProduct.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    lstDynamics.Add(dynTsCnguoi);
                }

                if (vehicleProduct.IS_DRIVER != null && vehicleProduct.IS_DRIVER == "1")
                {
                    DynamicData dynNxt = new DynamicData();
                    dynNxt.KEY_CODE = "TN_NTX";
                    dynNxt.VAL_CODE = vehicleProduct.VOLUNTARY_CIVIL.DRIVER_INSUR;//vehicleProduct.VEHICLE_TYPE + "@" + 
                    lstDynamics.Add(dynNxt);
                }

                if (vehicleProduct.IS_CARGO != null && vehicleProduct.IS_CARGO == "1")
                {
                    DynamicData dynTtai_Hh = new DynamicData();
                    dynTtai_Hh.KEY_CODE = "TN_TTAI_HH";
                    dynTtai_Hh.VAL_CODE = vehicleProduct.VOLUNTARY_CIVIL.WEIGHT_CARGO;
                    dynTtai_Hh.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                    lstDynamics.Add(dynTtai_Hh);

                    DynamicData dynHh = new DynamicData();
                    dynHh.KEY_CODE = "TN_BH_HH";
                    dynHh.VAL_CODE = vehicleProduct.VOLUNTARY_CIVIL.CARGO_INSUR;
                    lstDynamics.Add(dynHh);
                }

                if (vehicleProduct.COMPULSORY_CIVIL != null)
                {
                    //TimeSpan timeSpan = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EXP, DateTime.MinValue) - goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EFF, DateTime.MinValue);
                    //int day = timeSpan.Days;
                    //KhanhPT Add EXP, EFF
                    inputProduct.EFF = vehicleProduct.COMPULSORY_CIVIL.EFF;
                    inputProduct.EXP = vehicleProduct.COMPULSORY_CIVIL.EXP;
                    //End
                    DateTime dExpTN = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EXP, DateTime.MinValue);
                    DateTime dEffTN = goUtility.Instance.ConvertToDateTime(vehicleProduct.COMPULSORY_CIVIL.EFF, DateTime.MinValue);
                    int day = goUtility.Instance.GetDayRange(dEffTN, dExpTN, false);


                    DynamicData dynTg = new DynamicData();
                    dynTg.KEY_CODE = "TN_TGBH";
                    dynTg.VAL_CODE = day.ToString();
                    dynTg.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                    lstDynamics.Add(dynTg);
                }

                inputProduct.DYNAMIC_FEES = lstDynamics;
                lstProduct.Add(inputProduct);
                //double fees = goBusinessCalculateFees.Instance.CalculateFeesInCache(inputDynamic, request);
                //double vat = fees * 10 / 100;
                //vehicleProduct.VOLUNTARY_CIVIL.AMOUNT = fees - vat;
                //vehicleProduct.VOLUNTARY_CIVIL.VAT = vat;
                //vehicleProduct.VOLUNTARY_CIVIL.DISCOUNT = 0;
                //vehicleProduct.VOLUNTARY_CIVIL.TOTAL_DISCOUNT = 0;
                //vehicleProduct.VOLUNTARY_CIVIL.TOTAL_AMOUNT = fees;
            }

            inputFees.PRODUCT_INFO = lstProduct;
            //var a = JsonConvert.SerializeObject(inputFees);
            OutDynamicFees outDynamic = goBusinessCalculateFees.Instance.GetOutCalFees(inputFees, request);
            feesProduct = outDynamic;
            if (outDynamic != null && outDynamic.PRODUCT_DETAIL != null && outDynamic.PRODUCT_DETAIL.Any())
            {
                foreach (var item in outDynamic.PRODUCT_DETAIL)
                {
                    if (vehicleProduct.COMPULSORY_CIVIL != null)
                    {
                        if (item.PRODUCT_CODE.Equals(vehicleProduct.COMPULSORY_CIVIL.PRODUCT_CODE))
                        {
                            vehicleProduct.COMPULSORY_CIVIL.FEES = item.FEES;
                            vehicleProduct.COMPULSORY_CIVIL.AMOUNT = item.AMOUNT;
                            vehicleProduct.COMPULSORY_CIVIL.VAT = item.VAT;
                            vehicleProduct.COMPULSORY_CIVIL.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT;
                            vehicleProduct.COMPULSORY_CIVIL.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                            vehicleProduct.COMPULSORY_CIVIL.DISCOUNT_DETAIL = item.DISCOUNT_DETAIL;
                        }
                    }

                    if (vehicleProduct.VOLUNTARY_CIVIL != null)
                    {
                        if (item.PRODUCT_CODE.Equals(vehicleProduct.VOLUNTARY_CIVIL.PRODUCT_CODE))
                        {
                            vehicleProduct.VOLUNTARY_CIVIL.FEES = item.FEES;
                            vehicleProduct.VOLUNTARY_CIVIL.AMOUNT = item.AMOUNT;
                            vehicleProduct.VOLUNTARY_CIVIL.VAT = item.VAT;
                            vehicleProduct.VOLUNTARY_CIVIL.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT;
                            vehicleProduct.VOLUNTARY_CIVIL.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                            vehicleProduct.VOLUNTARY_CIVIL.DISCOUNT_DETAIL = item.DISCOUNT_DETAIL;
                        }
                    }

                    if (vehicleProduct.PHYSICAL_DAMAGE != null)
                    {
                        if (item.PRODUCT_CODE.Equals(vehicleProduct.PHYSICAL_DAMAGE.PRODUCT_CODE))
                        {
                            vehicleProduct.PHYSICAL_DAMAGE.FEES = item.FEES;
                            vehicleProduct.PHYSICAL_DAMAGE.AMOUNT = item.AMOUNT;
                            vehicleProduct.PHYSICAL_DAMAGE.VAT = item.VAT;
                            vehicleProduct.PHYSICAL_DAMAGE.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT;
                            vehicleProduct.PHYSICAL_DAMAGE.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                            vehicleProduct.PHYSICAL_DAMAGE.DISCOUNT_DETAIL = item.DISCOUNT_DETAIL;
                        }
                    }
                }
            }
        }

        public void CalInsurVehicleVer3(ref OutDynamicFeesVer3 outDynamic, ref VehicleInsur vehicleInsur, BaseRequest request)
        {
            try
            {
                string org_code = vehicleInsur.ORG_CODE, channel = vehicleInsur.CHANNEL, username = vehicleInsur.USERNAME, action = vehicleInsur.ACTION;

                InputDynamicFeesVer3 inputFees = new InputDynamicFeesVer3();
                inputFees.ORG_CODE = org_code;
                inputFees.CHANNEL = channel;
                inputFees.USERNAME = username;
                List<DynamicProductVer3> lstProduct = new List<DynamicProductVer3>();
                if (vehicleInsur != null && vehicleInsur.VEHICLE_INSUR != null && vehicleInsur.VEHICLE_INSUR.Any())
                {
                    int index = 0;

                    for (var i = 0; i < vehicleInsur.VEHICLE_INSUR.Count(); i++)
                    {
                        // gán sản phẩm xe
                        VEHICLE_PRODUCT product = vehicleInsur.VEHICLE_INSUR[i];

                        // sản phẩm VCX
                        if (product.PHYSICAL_DAMAGE != null && product.PHYSICAL_DAMAGE.PRODUCT_CODE != null && product.PHYSICAL_DAMAGE.EFF != null)
                        {
                            //gán index
                            vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.INX = index; // vì 1 sản phẩm có thể bao gồm 2 3 sản phẩm con

                            // tạo json tính phí
                            DynamicProductVer3 productVer3 = new DynamicProductVer3();
                            productVer3.INX = index;
                            productVer3.CATEGORY = goConstants.CATEGORY_XE;
                            productVer3.PRODUCT_CODE = product.PHYSICAL_DAMAGE.PRODUCT_CODE;
                            productVer3.PACK_CODE = product.PHYSICAL_DAMAGE.PACK_CODE;
                            if (product.PHYSICAL_DAMAGE.IS_DISCOUNT != null && product.PHYSICAL_DAMAGE.IS_DISCOUNT != "0")
                            {
                                productVer3.DISCOUNT = product.PHYSICAL_DAMAGE.DISCOUNT;
                                productVer3.DISCOUNT_UNIT = product.PHYSICAL_DAMAGE.DISCOUNT_UNIT;
                                productVer3.TYPE_DISCOUNT = product.PHYSICAL_DAMAGE.TYPE_DISCOUNT;
                            }
                            List<DynamicData> dynamicDatas = new List<DynamicData>();

                            dynamicDatas = GetDynamicPhysicalDamage(product);
                            productVer3.DYNAMIC_FEES = dynamicDatas;
                            // gán sp vào tính phí
                            lstProduct.Add(productVer3);
                        }

                        // sản phẩm TNDS
                        if (product.COMPULSORY_CIVIL != null && product.COMPULSORY_CIVIL.PRODUCT_CODE != null && product.IS_COMPULSORY != null && product.IS_COMPULSORY == "1")
                        {
                            //gán index
                            vehicleInsur.VEHICLE_INSUR[i].COMPULSORY_CIVIL.INX = index; // vì 1 sản phẩm có thể bao gồm 2 3 sản phẩm con

                            // tạo json tính phí
                            DynamicProductVer3 productVer3 = new DynamicProductVer3();
                            productVer3.INX = index;
                            productVer3.CATEGORY = goConstants.CATEGORY_XE;
                            productVer3.PRODUCT_CODE = product.COMPULSORY_CIVIL.PRODUCT_CODE;
                            productVer3.PACK_CODE = product.COMPULSORY_CIVIL.PACK_CODE;
                            //if (product.COMPULSORY_CIVIL.IS_DISCOUNT != null && product.COMPULSORY_CIVIL.IS_DISCOUNT != "0")
                            //{
                            //    productVer3.DISCOUNT = product.COMPULSORY_CIVIL.DISCOUNT;
                            //    productVer3.DISCOUNT_UNIT = product.COMPULSORY_CIVIL.DISCOUNT_UNIT;
                            //    productVer3.TYPE_DISCOUNT = product.COMPULSORY_CIVIL.TYPE_DISCOUNT;
                            //}
                            List<DynamicData> dynamicDatas = new List<DynamicData>();

                            dynamicDatas = GetDynamicCompulsoryCivil(product);
                            productVer3.DYNAMIC_FEES = dynamicDatas;
                            // gán sp vào tính phí
                            lstProduct.Add(productVer3);
                        }
                        index++;
                    }
                }
                inputFees.PRODUCT_INFO = lstProduct;

                var a = JsonConvert.SerializeObject(inputFees);

                outDynamic = goBusinessCalculateFees.Instance.GetOutCalFeesVer3(inputFees, request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DynamicData> GetDynamicPhysicalDamage(VEHICLE_PRODUCT product)
        {
            List<DynamicData> dynamicDatas = new List<DynamicData>();
            try
            {
                #region Physical Damage
                // STBH
                DynamicData dyn_stbh = new DynamicData();
                dyn_stbh.KEY_CODE = "STBH";
                //dyn_stbh.VAL_CODE = product.VEHICLE_VALUE;
                dyn_stbh.VAL_CODE = product.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT == null ? "0" : product.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT.ToString();
                dyn_stbh.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_stbh);

                // XCG_PL1_1 = VH_TYPE@CAR_WEIGHT@CAR_PRICE@YEAR_OF_USE
                DynamicData dyn_pl1_1 = new DynamicData();
                dyn_pl1_1.KEY_CODE = "XCG_PL1_1";
                dyn_pl1_1.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE + "@" + product.VEHICLE_VALUE_CODE + "@" + product.VEHICLE_REGIS_CODE;
                dyn_pl1_1.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dyn_pl1_1);

                //XCG_PL1_2 = VH_TYPE@CAR_PRICE@YEAR_OF_USE
                DynamicData dyn_pl1_2 = new DynamicData();
                dyn_pl1_2.KEY_CODE = "XCG_PL1_2";
                dyn_pl1_2.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VEHICLE_VALUE_CODE + "@" + product.VEHICLE_REGIS_CODE;
                dyn_pl1_2.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dyn_pl1_2);

                // XCG_VCX_BS
                string bs = "";
                if (product.PHYSICAL_DAMAGE.ADDITIONAL != null && product.PHYSICAL_DAMAGE.ADDITIONAL.Any())
                {
                    foreach (var item in product.PHYSICAL_DAMAGE.ADDITIONAL)
                    {
                        if (string.IsNullOrEmpty(bs))
                            bs = item.KEY;
                        else
                            bs += "@" + item.KEY;
                    }
                }
                DynamicData dynBs = new DynamicData();
                dynBs.KEY_CODE = "XCG_VCX_BS";
                dynBs.VAL_CODE = bs;
                dynBs.TYPE_CODE = goConstants.TypeValueDynamic.FORMULA.ToString();
                dynamicDatas.Add(dynBs);

                // với trường hợp hợp 
                DynamicData dynBs01 = new DynamicData();
                dynBs01.KEY_CODE = "BS01_YEAR_OF_USE";
                dynBs01.VAL_CODE = "BS01/HDI-XCG@" + product.VEHICLE_REGIS_CODE;
                dynBs01.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dynBs01);

                DynamicData dynBs02 = new DynamicData();
                dynBs02.KEY_CODE = "BS02_YEAR_OF_USE";
                dynBs02.VAL_CODE = "BS02/HDI-XCG@" + product.VEHICLE_REGIS_CODE;
                dynBs02.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dynBs02);

                #endregion

                #region TNDS TN
                // TNDS TN: (người và hàng hóa người t3, hành khách)
                if (product.IS_VOLUNTARY != null && product.IS_VOLUNTARY == "1")
                {
                    #region Thân thể người t3
                    //XCGTN_N3_CNGOI = VH_TYPE@CAR_SEAT@TH_N3
                    DynamicData dynTnN3_1 = new DynamicData();
                    dynTnN3_1.KEY_CODE = "XCGTN_N3_CNGOI"; // thân thể ng t3
                    dynTnN3_1.VAL_CODE = product.VEHICLE_TYPE + "@" + product.SEAT_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_1.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_1);

                    //XCGTN_N3_LXE = VH_TYPE@TH_N3
                    DynamicData dynTnN3_2 = new DynamicData();
                    dynTnN3_2.KEY_CODE = "XCGTN_N3_LXE"; // thân thể ng t3
                    dynTnN3_2.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_2.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_2);

                    // XCGTN_N3_TTAI = VH_TYPE@CAR_WEIGHT@TH_N3
                    DynamicData dynTnN3_3 = new DynamicData();
                    dynTnN3_3.KEY_CODE = "XCGTN_N3_TTAI"; // thân thể ng t3
                    dynTnN3_3.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_3.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_3);
                    #endregion

                    #region Tài sản ng t3
                    // XCGTN_TSN3_LXE = VH_TYPE@TH_TSN3
                    DynamicData dynTnN3_TS_1 = new DynamicData();
                    dynTnN3_TS_1.KEY_CODE = "XCGTN_TSN3_LXE";
                    dynTnN3_TS_1.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_1.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_1);

                    // XCGTN_TSN3_TTAI = VH_TYPE@CAR_WEIGHT@TH_TSN3
                    DynamicData dynTnN3_TS_2 = new DynamicData();
                    dynTnN3_TS_2.KEY_CODE = "XCGTN_TSN3_TTAI";
                    dynTnN3_TS_2.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_2.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_2);

                    //XCGTN_TSN3_CNGOI = VH_TYPE@CAR_SEAT@TH_TSN3
                    DynamicData dynTnN3_TS_3 = new DynamicData();
                    dynTnN3_TS_3.KEY_CODE = "XCGTN_TSN3_CNGOI";
                    dynTnN3_TS_3.VAL_CODE = product.VEHICLE_TYPE + "@" + product.SEAT_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_3.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_3);
                    #endregion

                    #region Thiệt hại hành khách
                    //XCGTN_TH_HK = VH_TYPE@CAR_PURPOSE@TH_HK
                    DynamicData dynTnN3_HK = new DynamicData();
                    dynTnN3_HK.KEY_CODE = "XCGTN_TH_HK";
                    dynTnN3_HK.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VEHICLE_USE + "@" + product.VOLUNTARY_CIVIL.INSUR_PASSENGER;
                    dynTnN3_HK.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_HK);
                    #endregion
                }

                // TNDS TN: LPX, NNTX
                if (product.IS_DRIVER != null && product.IS_DRIVER == "1")
                {
                    //XCGTN_NTX = BH_NTX
                    DynamicData dynTnNTX = new DynamicData();
                    dynTnNTX.KEY_CODE = "XCGTN_NTX";
                    dynTnNTX.VAL_CODE = product.VOLUNTARY_CIVIL.DRIVER_INSUR;
                    dynTnNTX.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnNTX);
                }

                // TNDS TN: Hàng hóa
                if (product.IS_CARGO != null && product.IS_CARGO == "1")
                {
                    // XCGTN_BH_HH = BH_HH
                    DynamicData dynTnHH = new DynamicData();
                    dynTnHH.KEY_CODE = "XCGTN_BH_HH";
                    dynTnHH.VAL_CODE = product.VOLUNTARY_CIVIL.CARGO_INSUR;
                    dynTnHH.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnHH);

                    //XCGTN_TTAI_HH
                    DynamicData dynTnHH_TT = new DynamicData();
                    dynTnHH_TT.KEY_CODE = "XCGTN_TTAI_HH";
                    dynTnHH_TT.VAL_CODE = product.VOLUNTARY_CIVIL.WEIGHT_CARGO;
                    dynTnHH_TT.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                    dynamicDatas.Add(dynTnHH_TT);
                }
                #endregion

                #region Common
                // XCG_TGBH

                //int day = 0;
                DateTime dEff = goUtility.Instance.ConvertToDateTime(product.PHYSICAL_DAMAGE.EFF, DateTime.MinValue);
                DateTime dExp = goUtility.Instance.ConvertToDateTime(product.PHYSICAL_DAMAGE.EXP, DateTime.MinValue);
                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);

                DynamicData dyn_tgbh = new DynamicData();
                dyn_tgbh.KEY_CODE = "XCG_TGBH";
                dyn_tgbh.VAL_CODE = day.ToString();
                dyn_tgbh.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_tgbh);

                //XCGTN_CNGOI = CAR_SEAT
                DynamicData dyn_cNgoi = new DynamicData();
                dyn_cNgoi.KEY_CODE = "XCGTN_CNGOI";
                dyn_cNgoi.VAL_CODE = product.SEAT_CODE;
                dyn_cNgoi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dyn_cNgoi);
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dynamicDatas;
        }

        public List<DynamicData> GetDynamicCompulsoryCivil(VEHICLE_PRODUCT product)
        {

            List<DynamicData> dynamicDatas = new List<DynamicData>();
            try
            {

                DateTime dEff = goUtility.Instance.ConvertToDateTime(product.COMPULSORY_CIVIL.EFF, DateTime.MinValue);
                DateTime dExp = goUtility.Instance.ConvertToDateTime(product.COMPULSORY_CIVIL.EXP, DateTime.MinValue);
                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);

                #region Common
                DynamicData dyn_tgbh = new DynamicData();
                dyn_tgbh.KEY_CODE = "XCG_TGBH";
                dyn_tgbh.VAL_CODE = day.ToString();
                dyn_tgbh.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_tgbh);

                //XCGTN_CNGOI = CAR_SEAT
                if (product.IS_DRIVER != null && product.IS_DRIVER == "1")
                {
                    if(product.VOLUNTARY_CIVIL.DRIVER_SEAT_CODE != null && !String.IsNullOrEmpty(product.VOLUNTARY_CIVIL.DRIVER_SEAT_CODE))
                    {
                        DynamicData dyn_cNgoi = new DynamicData();
                        dyn_cNgoi.KEY_CODE = "XCGTN_CNGOI";
                        dyn_cNgoi.VAL_CODE = product.VOLUNTARY_CIVIL.DRIVER_SEAT_CODE;
                        dyn_cNgoi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                        dynamicDatas.Add(dyn_cNgoi);
                    }
                    else
                    {
                        DynamicData dyn_cNgoi = new DynamicData();
                        dyn_cNgoi.KEY_CODE = "XCGTN_CNGOI";
                        dyn_cNgoi.VAL_CODE = product.SEAT_CODE;
                        dyn_cNgoi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                        dynamicDatas.Add(dyn_cNgoi);
                    }
                }
                else
                {
                    DynamicData dyn_cNgoi = new DynamicData();
                    dyn_cNgoi.KEY_CODE = "XCGTN_CNGOI";
                    dyn_cNgoi.VAL_CODE = product.SEAT_CODE;
                    dyn_cNgoi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dyn_cNgoi);
                }
                #endregion

                #region TNDS TN
                // TNDS TN: (người và hàng hóa người t3, hành khách)
                if (product.IS_VOLUNTARY != null && product.IS_VOLUNTARY == "1")
                {
                    #region Thân thể người t3
                    //XCGTN_N3_CNGOI = VH_TYPE@CAR_SEAT@TH_N3
                    DynamicData dynTnN3_1 = new DynamicData();
                    dynTnN3_1.KEY_CODE = "XCGTN_N3_CNGOI"; // thân thể ng t3
                    dynTnN3_1.VAL_CODE = product.VEHICLE_TYPE + "@" + product.SEAT_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_1.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_1);

                    //XCGTN_N3_LXE = VH_TYPE@TH_N3
                    DynamicData dynTnN3_2 = new DynamicData();
                    dynTnN3_2.KEY_CODE = "XCGTN_N3_LXE"; // thân thể ng t3
                    dynTnN3_2.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_2.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_2);

                    // XCGTN_N3_TTAI = VH_TYPE@CAR_WEIGHT@TH_N3
                    DynamicData dynTnN3_3 = new DynamicData();
                    dynTnN3_3.KEY_CODE = "XCGTN_N3_TTAI"; // thân thể ng t3
                    dynTnN3_3.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD;
                    dynTnN3_3.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_3);
                    #endregion

                    #region Tài sản ng t3
                    // XCGTN_TSN3_LXE = VH_TYPE@TH_TSN3
                    DynamicData dynTnN3_TS_1 = new DynamicData();
                    dynTnN3_TS_1.KEY_CODE = "XCGTN_TSN3_LXE";
                    dynTnN3_TS_1.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_1.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_1);

                    // XCGTN_TSN3_TTAI = VH_TYPE@CAR_WEIGHT@TH_TSN3
                    DynamicData dynTnN3_TS_2 = new DynamicData();
                    dynTnN3_TS_2.KEY_CODE = "XCGTN_TSN3_TTAI";
                    dynTnN3_TS_2.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_2.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_2);

                    //XCGTN_TSN3_CNGOI = VH_TYPE@CAR_SEAT@TH_TSN3
                    DynamicData dynTnN3_TS_3 = new DynamicData();
                    dynTnN3_TS_3.KEY_CODE = "XCGTN_TSN3_CNGOI";
                    dynTnN3_TS_3.VAL_CODE = product.VEHICLE_TYPE + "@" + product.SEAT_CODE + "@" + product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS;
                    dynTnN3_TS_3.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_TS_3);
                    #endregion

                    #region Thiệt hại hành khách
                    //XCGTN_TH_HK = VH_TYPE@CAR_PURPOSE@TH_HK
                    DynamicData dynTnN3_HK = new DynamicData();
                    dynTnN3_HK.KEY_CODE = "XCGTN_TH_HK";
                    dynTnN3_HK.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VEHICLE_USE + "@" + product.VOLUNTARY_CIVIL.INSUR_PASSENGER;
                    dynTnN3_HK.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnN3_HK);
                    #endregion
                }

                // TNDS TN: LPX, NNTX
                if (product.IS_DRIVER != null && product.IS_DRIVER == "1")
                {
                    //XCGTN_NTX = BH_NTX
                    DynamicData dynTnNTX = new DynamicData();
                    dynTnNTX.KEY_CODE = "XCGTN_NTX";
                    dynTnNTX.VAL_CODE = product.VOLUNTARY_CIVIL.DRIVER_INSUR;
                    dynTnNTX.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnNTX);
                }

                // TNDS TN: Hàng hóa
                if (product.IS_CARGO != null && product.IS_CARGO == "1")
                {
                    // XCGTN_BH_HH = BH_HH
                    DynamicData dynTnHH = new DynamicData();
                    dynTnHH.KEY_CODE = "XCGTN_BH_HH";
                    dynTnHH.VAL_CODE = product.VOLUNTARY_CIVIL.CARGO_INSUR;
                    dynTnHH.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                    dynamicDatas.Add(dynTnHH);

                    //XCGTN_TTAI_HH
                    DynamicData dynTnHH_TT = new DynamicData();
                    dynTnHH_TT.KEY_CODE = "XCGTN_TTAI_HH";
                    dynTnHH_TT.VAL_CODE = product.VOLUNTARY_CIVIL.WEIGHT_CARGO;
                    dynTnHH_TT.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                    dynamicDatas.Add(dynTnHH_TT);
                }
                #endregion

                #region TNDS BB
                //BBPLA_CNGOI = VH_TYPE@CAR_SEAT
                DynamicData bbCngoi = new DynamicData();
                bbCngoi.KEY_CODE = "BBPLA_CNGOI"; // thân thể ng t3
                bbCngoi.VAL_CODE = product.VEHICLE_TYPE + "@" + product.SEAT_CODE;
                bbCngoi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(bbCngoi);

                //BBPLA_LXE = VH_TYPE
                DynamicData bbLxe = new DynamicData();
                bbLxe.KEY_CODE = "BBPLA_LXE"; // thân thể ng t3
                bbLxe.VAL_CODE = product.VEHICLE_TYPE;
                bbLxe.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(bbLxe);

                //BBPLA_TTAI = VH_TYPE@CAR_WEIGHT
                DynamicData bbTtai = new DynamicData();
                bbTtai.KEY_CODE = "BBPLA_TTAI"; // thân thể ng t3
                bbTtai.VAL_CODE = product.VEHICLE_TYPE + "@" + product.WEIGH_CODE;
                bbTtai.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(bbTtai);

                //BBPLA_MDKD = VH_TYPE@CAR_PURPOSE
                DynamicData bbMdkd = new DynamicData();
                bbMdkd.KEY_CODE = "BBPLA_MDKD"; // thân thể ng t3
                bbMdkd.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VEHICLE_USE;
                bbMdkd.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(bbMdkd);

                //BBPLA_MDKD_CNGOI = VH_TYPE@CAR_PURPOSE@CAR_SEAT
                DynamicData bbMdkdCn = new DynamicData();
                bbMdkdCn.KEY_CODE = "BBPLA_MDKD_CNGOI"; // thân thể ng t3
                bbMdkdCn.VAL_CODE = product.VEHICLE_TYPE + "@" + product.VEHICLE_USE + "@" + product.SEAT_CODE;
                bbMdkdCn.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(bbMdkdCn);


                DynamicData dynDay = new DynamicData();
                if (day < 30)
                {
                    dynDay.KEY_CODE = "HS_D30D";
                    dynDay.VAL_CODE = "1";
                    dynDay.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                }
                else
                {
                    dynDay.KEY_CODE = "HS_T30D";
                    dynDay.VAL_CODE = day.ToString();
                    dynDay.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                }
                dynamicDatas.Add(dynDay);
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dynamicDatas;
        }

        #endregion

        #region Cấp đơn lô - import
        public BaseResponse GetResOrderCommon(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);

            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                ReqOrdersCommon reqOrder = new ReqOrdersCommon();
                if (request.Data != null)
                    reqOrder = JsonConvert.DeserializeObject<ReqOrdersCommon>(request.Data.ToString());
                OrdersModel ordersModel = goBusinessOrders.Instance.GetOrderCommonByReq(reqOrder);
                BaseRequest req = request;
                req.Data = ordersModel;
                response = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                //response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(req);
                if (response.Success)
                {
                    try
                    {
                        OrderResInfo resInfoOrder = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                        if (resInfoOrder != null && resInfoOrder.status.Equals(nameof(goConstants.OrderStatus.PAID)))
                        {
                            InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                            //response = goBusinessServices.Instance.signVietJetBatch(insurContract);
                            List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);
                            //send mail GCN theo insurDetails  
                            //foreach (InsurDetail item in insurDetails)
                            //{
                            response = goBusinessServices.Instance.signAndSendMail(insurContract, insurDetails, request.Action.UserName, request.Device.DeviceEnvironment, "NO");
                            //response = goBusinessServices.Instance.signVietJet(item);
                            //new Task(() => goBusinessServices.Instance.signVietJet(item)).Start();
                            if (response.Success)
                            {
                            }
                            else
                            {
                                //response.Success = false;
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + insurDetails[0].CERTIFICATE_NO + JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
                                goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails));
                            }
                            //}

                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "send mail voucher: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_VJC_02), goConstantsError.Instance.ERROR_VJC_02);
            }
            return response;
        }

        #endregion

        #region Hàm chung mặt nạ cho các sản phẩm bảo hiểm
        public BaseResponse GetResMask(BaseRequest request, string tracking, bool pay = false)
        {            
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {                
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                if(ordersModel.COMMON.SELLER.ORG_CODE == goConstants.HDI_E_COM)
                {
                    new Task(() => {
                        if (pay)
                            goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                        else
                            goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                    }).Start();
                    response = goBusinessCommon.Instance.getResultApi(true, "", config, "", "");
                    return response;
                }
                else
                {
                    if (pay)
                        response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                    else
                        response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                }    
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        private MaskValidate ValidateReq(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask == null || string.IsNullOrEmpty(mask.CATEGORY))
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                    return validate;
                }
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        validate = ValidateLo(mask);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        validate = ValidateHealth(mask);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        validate = ValidateHouse(mask);
                        break;
                    // Start 2022-04-12 By ThienTVB
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_DEVICE):
                        validate = ValidateDevice(mask);
                        break;
                    // End 2022-04-12 By ThienTVB
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate ValidateReqDefine(MaskInsur mask, BaseRequest request)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask == null || string.IsNullOrEmpty(mask.CATEGORY))
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                    return validate;
                }
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        validate = ValidateHouseDefine(mask, request);
                        break;
                    // Start 2022-04-12 By ThienTVB
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_DEVICE):
                        validate = ValidateDeviceDefine(mask, request);
                        break;
                    // End 2022-04-12 By ThienTVB
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate CalFeesMask(BaseRequest request, ref MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        validate = CalFeesHouse(request, ref mask);
                        break;
                    // Start 2022-04-12 By ThienTVB
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_DEVICE):
                        validate = CalFeesDevice(request, ref mask);
                        break;
                    // End 2022-04-12 By ThienTVB
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private OrdersModel GetOrdersByMask(MaskInsur mask, bool pay = false)
        {
            OrdersModel ordersModel = new OrdersModel();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByLoVer2(mask, pay);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByHeath(mask, pay);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByHouse(mask, pay);
                        break;
                    // Add 2022-04-07 By ThienTVB
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_DEVICE):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByDevice(mask, pay);
                        break;
                    // Add 2023-07-20 By KhanhPT
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_TRAVEL):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByTravel(mask, pay);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_TRAVEL_INTER):
                        ordersModel = goBusinessOrders.Instance.GetOrdersByTravel(mask, pay);
                        break;
                    default:
                        ordersModel = null;
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ordersModel is NULL Row 2883 CATEGORY: (" + mask.CATEGORY + ")", Logger.ConcatName(nameClass, nameMethod));
                        break;
                    // END 2022-04-07 By ThienTVB
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return ordersModel;
        }

        private OrdersModel GetOrdersByMaskV2(MaskInsur mask, bool pay = false)
        {
            OrdersModel ordersModel = new OrdersModel();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        //ordersModel = goBusinessOrders.Instance.GetOrdersByLoVer2(mask, pay);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        if (mask.BUYER.BUYER_MODE.Equals("GD"))
                        {
                            ordersModel = goBusinessOrders.Instance.GetOrdersByHeath(mask, pay);
                        }

                        if (mask.BUYER.BUYER_MODE.Equals("CN"))
                        {
                            ordersModel = goBusinessOrders.Instance.GetOrdersByHeathV2(mask, pay);
                        }
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        //ordersModel = goBusinessOrders.Instance.GetOrdersByHouse(mask, pay);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return ordersModel;
        }

        #region ATTD NEW
        private MaskValidate ValidateLo(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask.SELLER == null || string.IsNullOrEmpty(mask.SELLER.ORG_CODE) || //string.IsNullOrEmpty(mask.SELLER.SELLER_CODE) ||
                    mask.BUYER == null || string.IsNullOrEmpty(mask.BUYER.NAME) || string.IsNullOrEmpty(mask.BUYER.TYPE) ||
                    mask.LO_INSUR == null || !mask.LO_INSUR.Any())
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }
        #endregion

        #region Sức khỏe - Tai nạn
        private MaskValidate ValidateHealth(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask.SELLER == null || mask.BUYER == null || string.IsNullOrEmpty(mask.BUYER.NAME) || string.IsNullOrEmpty(mask.BUYER.TYPE) ||
                    mask.HEALTH_INSUR == null || !mask.HEALTH_INSUR.Any() ||
                    (string.IsNullOrEmpty(mask.SELLER.ORG_CODE) && string.IsNullOrEmpty(mask.BUYER.SHARE_CODE)) // || string.IsNullOrEmpty(mask.SELLER.SELLER_CODE))
                    )
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate ValidateHealthV2(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask.SELLER == null || mask.BUYER == null || string.IsNullOrEmpty(mask.BUYER.NAME) || string.IsNullOrEmpty(mask.BUYER.TYPE) ||
                    mask.HEALTH_INSUR == null || !mask.HEALTH_INSUR.Any() || string.IsNullOrEmpty(mask.BUYER.BUYER_MODE) // || string.IsNullOrEmpty(mask.SELLER.SELLER_CODE))
                    )
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }
        #endregion

        #region Nhà tư nhân
        private MaskValidate ValidateHouse(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask.SELLER == null || string.IsNullOrEmpty(mask.SELLER.ORG_CODE) || //string.IsNullOrEmpty(mask.SELLER.SELLER_CODE) ||
                    mask.BUYER == null || string.IsNullOrEmpty(mask.BUYER.NAME) || string.IsNullOrEmpty(mask.BUYER.TYPE) ||
                    mask.HOUSE_INSUR == null || !mask.HOUSE_INSUR.Any())
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                    return validate;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate ValidateHouseDefine(MaskInsur mask, BaseRequest request)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                // check trường hợp không cho bán VD: đất xanh

                // danh sách define của sản phẩm
                if (mask.HOUSE_INSUR.Count == 1)
                {
                    string productCode = mask.HOUSE_INSUR[0].HOUSE_PRODUCT.PRODUCT_CODE;
                    string channel = mask.CHANNEL;
                    string user = mask.USERNAME;
                    string org_code = mask.SELLER.ORG_CODE;
                    string category = goConstants.CATEGORY_NHATN;
                    string key_init_Crit = goConstantsRedis.PrefixDynamicFess + category;
                    string key_init_Product = key_init_Crit + productCode;
                    string keyDefine = key_init_Product + goConstantsRedis.PrefixDefineFees;
                    List<ProductDefineFees> lstDefine = goBusinessCache.Instance.Get<List<ProductDefineFees>>(keyDefine);

                    // kiểm tra cache
                    goBusinessCalculateFees.Instance.CheckInitCaching(key_init_Product, request, channel, user, org_code, category, productCode);

                    #region validate data
                    // Kiểm tra gói phí
                    double houseValue = mask.HOUSE_INSUR[0].HOUSE_PRODUCT.INSUR_HOUSE_VALUE;
                    double houseAssets = mask.HOUSE_INSUR[0].HOUSE_PRODUCT.INSUR_HOUSE_ASSETS;
                    string packCode = mask.HOUSE_INSUR[0].HOUSE_PRODUCT.PACK_CODE;
                    if (!CheckDefineOrNull(lstDefine, packCode, goConstants.KeyFeesInfo.NHA_HVF.ToString(), houseValue.ToString(), true, true))
                        return goBusinessCommon.Instance.ResValidateMask(true, nameof(goConstantsValidate.ERR_CHK_29), goConstantsValidate.ERR_CHK_29);
                    if (!CheckDefineOrNull(lstDefine, packCode, goConstants.KeyFeesInfo.NHA_FVF.ToString(), houseAssets.ToString(), true, true))
                        return goBusinessCommon.Instance.ResValidateMask(true, nameof(goConstantsValidate.ERR_CHK_30), goConstantsValidate.ERR_CHK_30);
                    if (string.IsNullOrEmpty(mask.HOUSE_INSUR[0].HOUSE_PRODUCT.HOUSE_USE_CODE) || string.IsNullOrEmpty(mask.HOUSE_INSUR[0].HOUSE_INFO.YEAR_COMPLETE))
                        if(mask.SELLER.ORG_CODE != goConstants.HDI_E_COM)
                            return goBusinessCommon.Instance.ResValidateMask(true, nameof(goConstantsValidate.ERR_CHK_HOU_04), goConstantsValidate.ERR_CHK_HOU_04);

                    if (mask.SELLER.ORG_CODE != goConstants.HDI_E_COM)
                    {
                        DateTime dEff = goUtility.Instance.ConvertToDateTime(mask.HOUSE_INSUR[0].HOUSE_PRODUCT.EFF, DateTime.MinValue);
                        int year = dEff.Year - goUtility.Instance.ConvertToInt32(mask.HOUSE_INSUR[0].HOUSE_INFO.YEAR_COMPLETE, 999999);
                        if (!CheckDefineOrNull(lstDefine, packCode, goConstants.KeyDefineFees.YEAR_OF_USE.ToString(), year.ToString(), false, true))
                            return goBusinessCommon.Instance.ResValidateMask(true, nameof(goConstantsValidate.ERR_CHK_HOU_04), goConstantsValidate.ERR_CHK_HOU_04);
                    }                           

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate CalFeesHouse(BaseRequest request, ref MaskInsur mask)
        {
            OutDynamicFeesVer3 outDynamic = new OutDynamicFeesVer3();
            MaskValidate validate = new MaskValidate();
            try
            {
                string org_code = mask.SELLER.ORG_CODE, channel = mask.CHANNEL, username = mask.USERNAME, action = mask.ACTION, category = mask.CATEGORY;

                InputDynamicFeesVer3 inputFees = new InputDynamicFeesVer3();
                inputFees.ORG_CODE = org_code;
                inputFees.CHANNEL = channel;
                inputFees.USERNAME = username;
                List<DynamicProductVer3> lstProduct = new List<DynamicProductVer3>();
                int index = 0;
                foreach (var item in mask.HOUSE_INSUR)
                {
                    index++;
                    item.INX = index;

                    // tạo json tính phí
                    DynamicProductVer3 productVer3 = new DynamicProductVer3();
                    productVer3.INX = index;
                    productVer3.CATEGORY = category;
                    productVer3.PRODUCT_CODE = item.HOUSE_PRODUCT.PRODUCT_CODE;
                    productVer3.PACK_CODE = item.HOUSE_PRODUCT.PACK_CODE;
                    productVer3.DISCOUNT = 0;
                    productVer3.DISCOUNT_UNIT = "P";
                    List<DynamicData> dynamicDatas = new List<DynamicData>();
                    dynamicDatas = GetDynamicHouse(item);
                    productVer3.DYNAMIC_FEES = dynamicDatas;
                    // gán sp vào tính phí
                    lstProduct.Add(productVer3);
                }
                inputFees.PRODUCT_INFO = lstProduct;
                //var a = JsonConvert.SerializeObject(inputFees);

                outDynamic = goBusinessCalculateFees.Instance.GetOutCalFeesVer3(inputFees, request);

                if (outDynamic != null && outDynamic.PRODUCT_DETAIL != null && outDynamic.PRODUCT_DETAIL.Any())
                {
                    foreach (var item in mask.HOUSE_INSUR)
                    {
                        // Thêm phí vào Json
                        OutDynamicProductVer3 productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == item.INX).FirstOrDefault();
                        if (productVer3 != null && productVer3.OUT_DETAIL != null)
                        {
                            List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;

                            // Tổng phí
                            ADDITIONAL_FEES fees = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.NHAF1.ToString())).FirstOrDefault();
                            if (fees != null)
                            {
                                item.HOUSE_PRODUCT.FEES_DATA = fees.FEES_DATA;
                                item.HOUSE_PRODUCT.FEES = fees.FEES;
                                item.HOUSE_PRODUCT.AMOUNT = fees.AMOUNT;
                                item.HOUSE_PRODUCT.TOTAL_DISCOUNT = fees.TOTAL_DISCOUNT;
                                item.HOUSE_PRODUCT.VAT = fees.VAT;
                                item.HOUSE_PRODUCT.TOTAL_AMOUNT = fees.TOTAL_AMOUNT;
                                item.HOUSE_PRODUCT.DISCOUNT_DETAIL = fees.DISCOUNT_DETAIL;
                            }
                            else
                            {
                                validate.Err = true;
                                validate.ErrCode = nameof(goConstantsError.Instance.ERROR_FEES_02);
                                validate.ErrMess = goConstantsError.Instance.ERROR_FEES_02;
                                return validate;
                            }
                            // phí giá trị nhà
                            ADDITIONAL_FEES feesHouse = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.NHA_HVF.ToString())).FirstOrDefault();
                            if (feesHouse != null)
                            {
                                item.HOUSE_PRODUCT.HOUSE_VAL_FEES_DATA = feesHouse.FEES_DATA;
                                item.HOUSE_PRODUCT.HOUSE_VAL_FEES = feesHouse.FEES;
                                item.HOUSE_PRODUCT.HOUSE_VAL_AMOUNT = feesHouse.AMOUNT;
                                item.HOUSE_PRODUCT.HOUSE_VAL_TOTAL_DISCOUNT = feesHouse.TOTAL_DISCOUNT;
                                item.HOUSE_PRODUCT.HOUSE_VAL_VAT = feesHouse.VAT;
                                item.HOUSE_PRODUCT.HOUSE_VAL_TOTAL_AMOUNT = feesHouse.TOTAL_AMOUNT;
                            }
                            // phí giá trị tài sản trong nhà
                            ADDITIONAL_FEES feesAssets = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.NHA_FVF.ToString())).FirstOrDefault();
                            if (feesHouse != null)
                            {
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_FEES_DATA = feesAssets.FEES_DATA;
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_FEES = feesAssets.FEES;
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_AMOUNT = feesAssets.AMOUNT;
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_TOTAL_DISCOUNT = feesAssets.TOTAL_DISCOUNT;
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_VAT = feesAssets.VAT;
                                item.HOUSE_PRODUCT.HOUSE_ASSETS_TOTAL_AMOUNT = feesAssets.TOTAL_AMOUNT;
                            }
                        }

                        // Tự vá thêm data vào 
                        // thêm tổng BH
                        item.HOUSE_PRODUCT.INSUR_TOTAL_AMOUNT = item.HOUSE_PRODUCT.INSUR_HOUSE_VALUE + item.HOUSE_PRODUCT.INSUR_HOUSE_ASSETS;
                        // thêm thông tin nếu là chủ nhà
                        if (item.LAND_USE_RIGHTS.Equals(goConstants.LandUseRights.O.ToString()))
                        {
                            item.HOUSE_INFO.NAME = item.NAME;
                            item.HOUSE_INFO.DOB = item.DOB;
                            item.HOUSE_INFO.GENDER = item.GENDER;
                            item.HOUSE_INFO.IDCARD = item.IDCARD;
                            item.HOUSE_INFO.IDCARD_D = item.IDCARD_D;
                            item.HOUSE_INFO.IDCARD_P = item.IDCARD_P;
                            item.HOUSE_INFO.EMAIL = item.EMAIL;
                            item.HOUSE_INFO.PHONE = item.PHONE;
                        }
                    }
                }
                else throw new Exception("Không tìm thấy sản phẩm để tính phí");
            }
            catch (Exception ex)
            {
                validate.Err = true;
                validate.ErrCode = nameof(goConstantsError.Instance.ERROR_FEES_02);
                validate.ErrMess = goConstantsError.Instance.ERROR_FEES_02;
            }
            return validate;
        }
        public List<DynamicData> GetDynamicHouse(HouseInsur item)
        {
            List<DynamicData> dynamicDatas = new List<DynamicData>();
            try
            {
                //Gói BH
                DynamicData dyn_goi = new DynamicData();
                dyn_goi.KEY_CODE = "NHA_GOITG";
                dyn_goi.VAL_CODE = item.HOUSE_PRODUCT.PACK_CODE + "@" + item.HOUSE_PRODUCT.HOUSE_USE_CODE;
                dyn_goi.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dyn_goi);

                //Gói TS BH
                DynamicData dyn_goits = new DynamicData();
                dyn_goits.KEY_CODE = "TS_GOITG";
                dyn_goits.VAL_CODE = item.HOUSE_PRODUCT.PACK_CODE + "@" + item.HOUSE_PRODUCT.ASSETS_USE_CODE;
                dyn_goits.TYPE_CODE = goConstants.TypeValueDynamic.CACHE.ToString();
                dynamicDatas.Add(dyn_goits);

                // Thời gian BH
                DateTime dEff = goUtility.Instance.ConvertToDateTime(item.HOUSE_PRODUCT.EFF, DateTime.MinValue);
                DateTime dExp = goUtility.Instance.ConvertToDateTime(item.HOUSE_PRODUCT.EXP, DateTime.MinValue);
                // Start 2022-07-22 BY ThienTVB Bỏ tính phí theo ngày (OLD)
                //int day = GoUtility.Instance.GetDayRange(dEff, dExp, false);

                //DynamicData dyn_tgbh = new();
                //dyn_tgbh.KEY_CODE = "NHA_TGBH";
                //dyn_tgbh.VAL_CODE = day.ToString();
                //dyn_tgbh.TYPE_CODE = GoConstants.TypeValueDynamic.CONSTANT.ToString();
                //dynamicDatas.Add(dyn_tgbh);
                // End 2022-07-22 BY ThienTVB Bỏ tính phí theo ngày (OLD)

                // Start 2022-07-22 BY ThienTVB Tính phí theo tháng (NEW)
                int month = Math.Abs((dExp.Year * 12 + (dExp.Month - 1)) - (dEff.Year * 12 + (dEff.Month - 1)));

                DynamicData dyn_tgbh = new DynamicData();
                dyn_tgbh.KEY_CODE = "NHA_TGBH";
                dyn_tgbh.VAL_CODE = month.ToString();
                dyn_tgbh.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_tgbh);
                // End 2022-07-22 BY ThienTVB Tính phí theo tháng (NEW)

                // Giá trị căn nhà
                DynamicData dyn_nhaGT = new DynamicData();
                dyn_nhaGT.KEY_CODE = "NHA_HV1";
                dyn_nhaGT.VAL_CODE = item.HOUSE_PRODUCT.INSUR_HOUSE_VALUE.ToString();
                dyn_nhaGT.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_nhaGT);

                // Giá trị tài sản trong nhà
                DynamicData dyn_nhaTS = new DynamicData();
                dyn_nhaTS.KEY_CODE = "NHA_FV1";
                dyn_nhaTS.VAL_CODE = item.HOUSE_PRODUCT.INSUR_HOUSE_ASSETS.ToString();
                dyn_nhaTS.TYPE_CODE = goConstants.TypeValueDynamic.CONSTANT.ToString();
                dynamicDatas.Add(dyn_nhaTS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dynamicDatas;
        }

        #endregion

        // Start 2022-04-13 By ThienTVB
        #region Thiết bị màn hình
        private MaskValidate ValidateDevice(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask.SELLER == null || string.IsNullOrEmpty(mask.SELLER.ORG_CODE) || //string.IsNullOrEmpty(mask.SELLER.SELLER_CODE) ||
                    mask.BUYER == null || string.IsNullOrEmpty(mask.BUYER.NAME) || string.IsNullOrEmpty(mask.BUYER.TYPE) ||
                    mask.DEVICE_INSUR == null || !mask.DEVICE_INSUR.Any())
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                    return validate;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        private MaskValidate ValidateDeviceDefine(MaskInsur mask, BaseRequest request)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(mask.BUYER.NAME))
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = "Tên người mua trống!";
                    return validate;
                }
                if (string.IsNullOrEmpty(mask.BUYER.PHONE))
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = "Số điện thoại người mua trống!";
                    return validate;
                }
                foreach (DeviceInsur item in mask.DEVICE_INSUR)
                {
                    if (item.DEVICE_INFO.URL == null || item.DEVICE_INFO.URL.Count == 0)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "Link URL hình ảnh thiết bị trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.NAME))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "Tên thiết bị trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.PHONE))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "Tên điện thoại trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.EMAIL))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "Email trống!";
                        return validate;
                    }
                    //if (string.IsNullOrEmpty(item.DEVICE_INFO.DEVICE_TYPE))
                    //{
                    //    validate.Err = true;
                    //    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    //    validate.ErrMess = "Loại thiết bị trống!";
                    //    return validate;
                    //}
                    //if (string.IsNullOrEmpty(item.DEVICE_INFO.BRAND))
                    //{
                    //    validate.Err = true;
                    //    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    //    validate.ErrMess = "Hãng thiết bị trống!";
                    //    return validate;
                    //}
                    //if (string.IsNullOrEmpty(item.DEVICE_INFO.MODEL))
                    //{
                    //    validate.Err = true;
                    //    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    //    validate.ErrMess = "Thương hiệu thiết bị trống!";
                    //    return validate;
                    //}
                    if (string.IsNullOrEmpty(item.DEVICE_INFO.IMEI))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "IMEI thiết bị trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.PACK_CODE))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "PACK_CODE thiết bị trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.REGION) && item.REGION != "VN")
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "Mã vùng không hợp lệ!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.RELATIONSHIP))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "RELATIONSHIP trống!";
                        return validate;
                    }
                    if (string.IsNullOrEmpty(item.PRODUCT_CODE))
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "PRODUCT_CODE trống!";
                        return validate;
                    }
                    if (item.FEES == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "FEES trống!";
                        return validate;
                    }
                    if (item.AMOUNT == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "AMOUNT trống!";
                        return validate;
                    }
                    if (item.TOTAL_DISCOUNT == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "TOTAL_DISCOUNT trống!";
                        return validate;
                    }
                    if (item.TOTAL_AMOUNT == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "TOTAL_AMOUNT trống!";
                        return validate;
                    }
                    if (item.TOTAL_ADD == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "TOTAL_ADD trống!";
                        return validate;
                    }
                    if (item.VAT == null)
                    {
                        validate.Err = true;
                        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                        validate.ErrMess = "VAT trống!";
                        return validate;
                    }
                    if (!string.IsNullOrEmpty(item.PRODUCT_CODE) && item.TYPE == "CQ")
                    {
                        if (string.IsNullOrEmpty(item.TAXCODE))
                        {
                            validate.Err = true;
                            validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                            validate.ErrMess = "TAXCODE trống! Khi Type là CQ";
                            return validate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }
        private MaskValidate CalFeesDevice(BaseRequest request, ref MaskInsur mask)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            MaskValidate validate = new MaskValidate();
            try
            {
                //foreach (var item in mask.DEVICE_INSUR)
                //{
                //    if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GB_1M)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GB_1M_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GB_1M_FEES;
                //    }
                //    else if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GB_6M)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GB_6M_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GB_6M_FEES;
                //    }
                //    else if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GB_1Y)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GB_1Y_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GB_1Y_FEES;
                //    }
                //    else if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GV_1M)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GV_1M_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GV_1M_FEES;
                //    }
                //    else if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GV_6M)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GV_6M_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GV_6M_FEES;
                //    }
                //    else if (item.PACK_CODE == goConstants.PACKCODE_DEVICE_GV_1Y)
                //    {
                //        item.VAT = goConstants.PACKCODE_DEVICE_GV_1Y_FEES * goConstants.VAT_DEVICE / 100;
                //        item.FEES = goConstants.PACKCODE_DEVICE_GV_1Y_FEES;
                //    }
                //    else
                //    {
                //        item.VAT = 0;
                //        item.FEES = 0;
                //    }
                //    item.AMOUNT = (double)(item.FEES + item.VAT);
                //    item.TOTAL_AMOUNT = (double)(item.AMOUNT + item.TOTAL_ADD - item.TOTAL_DISCOUNT);
                //    if (item.AMOUNT == 0)
                //    {
                //        validate.Err = true;
                //        validate.ErrCode = nameof(goConstantsError.Instance.ERROR_FEES_02);
                //        validate.ErrMess = goConstantsError.Instance.ERROR_FEES_02;
                //    }
                //}
            }
            catch (Exception ex)
            {
                validate.Err = true;
                validate.ErrCode = nameof(goConstantsError.Instance.ERROR_FEES_02);
                validate.ErrMess = goConstantsError.Instance.ERROR_FEES_02;
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
            return validate;
        }
        #endregion
        // End 2022-04-13 By ThienTVB

        #region Update status contract
        public BaseResponse UdpStatus(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "rq json: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                response = goBusinessAction.Instance.GetBaseResponse(request);
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "res json: " + JsonConvert.SerializeObject(response), Logger.ConcatName(nameClass, nameMethod));
                if (response.Success)
                {
                    try
                    {
                        // thông tin org callback
                        OrgDataCBConfig org = new OrgDataCBConfig();
                        org = goBusinessCommon.Instance.GetData<OrgDataCBConfig>(response, 0).FirstOrDefault();

                        if (org != null)
                        {
                            // Object push
                            InsurStatusCallback statusCallback = GetObjStatusCallback(org, config);
                            if (statusCallback == null)
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "UdpStatus Callback null js: " + JsonConvert.SerializeObject(org), Logger.ConcatName(nameClass, nameMethod));

                            // không cần login
                            if (!goUtility.Instance.CastToBoolean(org.IS_LOGIN))
                            {
                                List<OrgDataCB> lstApiOrg = new List<OrgDataCB>();
                                lstApiOrg = goBusinessCommon.Instance.GetData<OrgDataCB>(response, 1);
                                OrgDataCB ApiApprovedOrg = new OrgDataCB();
                                ApiApprovedOrg = lstApiOrg.Where(x => x.TYPE_CB.Equals(goConstants.TypeApiCallBack.APPROVE.ToString())).FirstOrDefault();
                                if (ApiApprovedOrg != null && !string.IsNullOrEmpty(ApiApprovedOrg.HEADER))
                                {
                                    List<goServiceConstants.KeyVal> lstHeader = new List<goServiceConstants.KeyVal>();
                                    lstHeader = JsonConvert.DeserializeObject<List<goServiceConstants.KeyVal>>(ApiApprovedOrg.HEADER);

                                    string url = org.URL_CB + ApiApprovedOrg.PATH_CB;
                                    var strJson = JsonConvert.SerializeObject(statusCallback);
                                    goServiceConstants.MethodApi methodApi = goServiceConstants.MethodApi.POST;
                                    goServiceConstants.TypeBody typeBody = goServiceConstants.TypeBody.none;
                                    goServiceConstants.TypeRaw typeRaw = goServiceConstants.TypeRaw.none;
                                    switch (ApiApprovedOrg.METHOD)
                                    {
                                        case var s when ApiApprovedOrg.METHOD.Equals(goServiceConstants.MethodApi.POST):
                                            methodApi = goServiceConstants.MethodApi.POST;
                                            break;
                                    }

                                    switch (ApiApprovedOrg.TYPE_BODY)
                                    {
                                        case var s when ApiApprovedOrg.TYPE_BODY.Equals(goServiceConstants.TypeBody.raw.ToString().ToUpper()):
                                            typeBody = goServiceConstants.TypeBody.raw;
                                            if (ApiApprovedOrg.TYPE_RAW.Equals(goServiceConstants.TypeRaw.json.ToString().ToUpper()))
                                            {
                                                typeRaw = goServiceConstants.TypeRaw.json;
                                            }
                                            break;
                                    }

                                    HttpResponseMessage rp = goServiceInvoke.Instance.Invoke(url, methodApi, typeBody, typeRaw, lstHeader, new List<goServiceConstants.KeyVal>(), strJson).Result;
                                    if (rp.StatusCode == HttpStatusCode.OK)
                                    {
                                        string message = rp.Content.ReadAsStringAsync().Result;
                                        try
                                        {
                                            string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                                            var jMees = JObject.Parse(parsedString);
                                            var status = jMees["Success"].ToString();
                                            if (goUtility.Instance.CastToBoolean(status))
                                            {
                                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "push org ok: " + message, Logger.ConcatName(nameClass, nameMethod));
                                                //goBussinessEmail.Instance.sendEmaiCMS("", "push org ok error", string.Join("-", message));
                                            }
                                            else
                                            {
                                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "push org error: " + message + ". Json: " + strJson, Logger.ConcatName(nameClass, nameMethod));
                                                goBussinessEmail.Instance.sendEmaiCMS("", "push org error", string.Join("-", message, strJson));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "push org error: " + message + ". Json: " + strJson + ". " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                                            goBussinessEmail.Instance.sendEmaiCMS("", "push org error", string.Join("-", message));
                                        }
                                    }
                                    else
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "push org error not OK: " + strJson, Logger.ConcatName(nameClass, nameMethod));
                                        goBussinessEmail.Instance.sendEmaiCMS("", "push org error not OK ", string.Join("-", strJson));
                                    }
                                }

                            }
                        }
                    }
                    catch (Exception ex1)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "callback loi" + ex1.Message, Logger.ConcatName(nameClass, nameMethod)); goBussinessEmail.Instance.sendEmaiCMS("", "Số tiền không đúng với số lượng sản phẩm đã mua", JsonConvert.SerializeObject(request).ToString());
                        goBussinessEmail.Instance.sendEmaiCMS("", "callback loi", JsonConvert.SerializeObject(request).ToString());
                    }


                    //OrderDataCB ord = goBusinessCommon.Instance.GetData<OrderDataCB>(response, 1).FirstOrDefault();
                    //List<OrderDetailDataCB> ordDt = goBusinessCommon.Instance.GetData<OrderDetailDataCB>(response, 2);
                    //List<InsurCallback> insur = goBusinessCommon.Instance.GetData<InsurCallback>(response, 3);
                    //List<InsurDetailCallback> insurDetail = goBusinessCommon.Instance.GetData<InsurDetailCallback>(response, 4);
                    //List<AdditionalCallback> detailAdd = goBusinessCommon.Instance.GetData<AdditionalCallback>(response, 5);
                    //List<BeneficiaryCallback> beneficiary = goBusinessCommon.Instance.GetData<BeneficiaryCallback>(response, 6);

                    //// lst theo sản phẩm
                    //List<object> lstObj1 = goBusinessCommon.Instance.GetData<object>(response, 7);
                    //List<object> lstObj2 = goBusinessCommon.Instance.GetData<object>(response, 8);
                    //List<object> lstObj3 = goBusinessCommon.Instance.GetData<object>(response, 9);

                    //OrderCallback ordCB = GetOrdCallback(org, ord, ordDt, insur, insurDetail, detailAdd, beneficiary, lstObj1, lstObj2, lstObj3);
                }

                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }
        #endregion

        #region Search trans id partner
        public BaseResponse SearchOrderByTrans(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                object param = null;
                if (request.Data != null)
                {
                    JObject jIinput = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                    param = new
                    {
                        channel = jIinput["CHANNEL"].ToString(),
                        userName = jIinput["USERNAME"].ToString(),
                        org_seller = config.partnerCode,
                        trans_id = jIinput["TRANS_ID"].ToString()
                    };
                }
                if (param == null)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "error: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                request.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                BaseResponse res_data = goBusinessAction.Instance.GetBaseResponse(request);
                if (res_data.Success)
                {
                    OutSearTrans outSear = new OutSearTrans();

                    OutSearOrder ord = goBusinessCommon.Instance.GetData<OutSearOrder>(res_data, 0).FirstOrDefault();
                    if (ord == null)
                    {
                        outSear.MESS = "Không tìm thấy id giao dịch, Đơn hàng chưa tồn tại !";
                        outSear.STATUS = "NOT_FOUND";
                        response = goBusinessCommon.Instance.getResultApi(true, outSear, config);
                        return response;
                    }
                    else
                    {
                        outSear.MESS = "Tìm thấy thông tin đơn hàng !";
                        outSear.STATUS = "EXISTS";

                        List<InsurDetail> contractDetails = goBusinessCommon.Instance.GetData<InsurDetail>(res_data, 3);
                        List<OutSearTransDetail> outSearTransDetails = new List<OutSearTransDetail>();
                        foreach (var item in contractDetails)
                        {
                            OutSearTransDetail info = new OutSearTransDetail();
                            info.NAME = item.NAME;
                            info.CERTIFICATE_NO = item.CERTIFICATE_NO;
                            if (item.STATUS.Equals(nameof(goConstants.OrderStatus.PAID)))
                            {
                                if (goUtility.Instance.CastToBoolean(item.IS_CER))
                                {
                                    if (item.TYPE_SEND.Equals(nameof(goConstants.After_Signed.SEND_EMAIL)) || item.TYPE_SEND.Equals(nameof(goConstants.After_Signed.SEND_EMAIL_SMS)))
                                    {
                                        info.STATUS = "SIGN_SEND";
                                        info.STATUS_NAME = "Đã ký số và gửi mail !";
                                    }
                                }
                                else
                                {
                                    info.STATUS = "SIGN_FAIL";
                                    info.STATUS_NAME = "Ký số thất bại, HDI sẽ xử lý nếu đơn đã thanh toán !";
                                }
                                // SEND_FAIL
                                // SEND_FAIL

                            }
                            else
                            {
                                info.STATUS = "DRAFT";
                                info.STATUS_NAME = "Đơn chưa thanh toán !";
                            }

                            outSearTransDetails.Add(info);
                        }

                        outSear.DETAILS = outSearTransDetails;
                        response = goBusinessCommon.Instance.getResultApi(true, outSear, config);
                        return response;
                    }
                }
                else
                {
                    response = res_data;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }
        #endregion

        public BaseResponse GetResMaskV2(BaseRequest request, string tracking, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReqV2(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                //validate v2
                validate = ValidateAge(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // mapping đơn theo cấu trúc mới
                OrdersModel ordersModel = GetOrdersByMaskV2(mask, pay);
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV1_Import(BaseRequest request, string tracking, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                String dateNow = DateTime.Now.ToString("ddMMyyyy");
                String productCode = jData["PRODUCT_CODE"].ToString();
                string strMD5 = goEncryptBase.Instance.Md5Encode(dateNow + "HDI@2024IMPORT" + productCode).ToLower();

                if (!strMD5.ToLower().Equals(jData["MD5"].ToString().ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }

                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReqV2(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }                

                // mapping đơn theo cấu trúc mới
                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                //Tao sản phẩm tai nạn Vikki về luồng Import                
                response = goBusinessOrders.Instance.GetBaseResponse_Orders_Import(reqOrd);
                if (response.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Tạo đơn Import thành công", Logger.ConcatName(nameClass, nameMethod));                   
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV2_SK_Movi(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());

                
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];

                    #endregion General

                    if (jData["ACTION"].ToString() == "BH_S")
                    {
                        //Get Info Buyer, Insured People
                        BaseResponse responseOrder = new BaseResponse();
                        object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                        responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);
                        if (responseOrder.Success)
                        {
                            List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);//Contract
                            List<JObject> objDetail = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 3);//Insured
                            if (objContract != null && objContract.Count > 0)
                            {
                                JObject jContract = new JObject();
                                jContract.Add("NAME", objContract[0]["NAME"]);
                                jContract.Add("DOB", objContract[0]["DOB_F"]);
                                jContract.Add("IDCARD", objContract[0]["IDCARD"]);
                                jContract.Add("EMAIL", objContract[0]["EMAIL"]);
                                jContract.Add("PHONE", objContract[0]["PHONE"]);
                                jContract.Add("ADDRESS", objContract[0]["ADDRESS"]);
                                jContract.Add("TYPE", objContract[0]["TYPE"]);
                                jContract.Add("GENDER", objContract[0]["GENDER"]);
                                jData.Add("BUYER", jContract);
                            }
                            else
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, "ORD_PARTER_DATA", "Không có thông tin Partner TransID");
                                return response;
                            }

                            if (objDetail != null && objDetail.Count > 0)
                            {
                                JArray jArrHealthInsur = new JArray();
                                foreach (JObject jDetail in objDetail)
                                {
                                    JObject jInsured = new JObject();
                                    jInsured.Add("CONTRACT_CODE", jDetail["CONTRACT_CODE"]);
                                    jInsured.Add("PACK_CODE", jDetail["PACK_CODE"]);
                                    jInsured.Add("NAME", jDetail["NAME"]);
                                    jInsured.Add("DOB", jDetail["DOB_F"]);
                                    jInsured.Add("IDCARD", jDetail["IDCARD"]);
                                    jInsured.Add("EMAIL", jDetail["EMAIL"]);
                                    jInsured.Add("PHONE", jDetail["PHONE"]);
                                    jInsured.Add("ADDRESS", jDetail["ADDRESS"]);                                    
                                    jInsured.Add("GENDER", jDetail["GENDER"]);
                                    jInsured.Add("RELATIONSHIP", jDetail["RELATIONSHIP"]);
                                    jInsured.Add("EFFECTIVE_DATE", jDetail["EFFECTIVE_DATE_F"]);
                                    jInsured.Add("EXPIRATION_DATE", jDetail["EXPIRATION_DATE_F"]);
                                    jArrHealthInsur.Add(jInsured);
                                }
                                
                                jData.Add("CUS_INFO", jArrHealthInsur);
                            }
                            else
                            {
                                response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, "ORD_PARTER_DATA", "Không có thông tin Partner TransID");
                                return response;
                            }
                        }

                        //Set info Order in jData BUyer
                    }

                    #region Buyer

                    JObject jBuyer = JObject.Parse(jData["BUYER"].ToString());
                    #region Buyer

                    jsonHealth["BUYER"]["NAME"] = jBuyer["NAME"];
                    jsonHealth["BUYER"]["DOB"] = jBuyer["DOB"];
                    jsonHealth["BUYER"]["IDCARD"] = jBuyer["IDCARD"];
                    jsonHealth["BUYER"]["EMAIL"] = jBuyer["EMAIL"];
                    jsonHealth["BUYER"]["PHONE"] = jBuyer["PHONE"];
                    jsonHealth["BUYER"]["ADDRESS"] = jBuyer["ADDRESS"];
                    jsonHealth["BUYER"]["TYPE"] = jBuyer["TYPE"];
                    jsonHealth["BUYER"]["GENDER"] = jBuyer["GENDER"];

                    #endregion Buyer

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);                    
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jsonCusInfo in lsJTK_DataDetail)
                        {
                            JObject jHealth = JObject.Parse(lstHealthInsur[0].ToString());
                            jHealth["CONTRACT_CODE"] = jsonCusInfo["CONTRACT_CODE"];
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];
                            jHealth["REGION"] = "VN";

                            if(jData["ACTION"].ToString() == "BH_M")
                            {
                                DateTime dtEFF = new DateTime();
                                DateTime dtEXP = new DateTime();
                                String dtNow = DateTime.Now.ToString("dd/MM/yyyy");

                                dtNow = dtNow + " 00:00";

                                //String effDate = jData["REQUEST_DATE"].ToString();

                                dtEFF = DateTime.ParseExact(dtNow, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture).AddDays(1);

                                dtEXP = dtEFF.AddYears(1);

                                jHealth["EFFECTIVE_DATE"] = dtEFF.ToString("dd/MM/yyyy HH:mm");
                                jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy HH:mm");
                            }
                            else
                            {
                                jHealth["EFFECTIVE_DATE"] = jsonCusInfo["EFFECTIVE_DATE"];
                                jHealth["EXPIRATION_DATE"] = jsonCusInfo["EXPIRATION_DATE"];
                            }
                            
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            #region FileAttach
                            if (jsonCusInfo.ContainsKey("FILE_ATTACH"))
                            {
                                JArray arrFile = JArray.FromObject(jsonCusInfo["FILE_ATTACH"]);
                                jHealth.Add("FILE", arrFile);
                            }

                            #endregion FileAttach

                            jArrHealthInsur_Replace.Add(jHealth);
                        }                                                
                        #region Health Insurers

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    if (jData["STATUS"] != null && jData["STATUS"].ToString() == "PAID")
                    {
                        pay = true;
                        jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                        jsonHealth["PAY_INFO"]["IS_SMS"] = "1";
                    }

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());               

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                /*
                if (mask.ACTION.Equals("BH_S"))
                {
                    //lấy thông tin json call
                    BaseResponse responseOrder = new BaseResponse();
                    object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                    responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);
                    if (responseOrder.Success)
                    {
                        List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);
                        if(objContract != null && objContract.Count > 0)
                        {
                            ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = objContract[0]["CONTRACT_CODE"].ToString();
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                            return response;
                        }    
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                        return response;
                    }
                   
                }
                */
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;

                

                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse OrderPartner_Search(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner

                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());

                if (jData != null)
                {
                    #region Lấy thông tin đơn bảo hiểm từ đối tác

                    //Get Info Buyer, Insured People
                    BaseResponse responseOrder = new BaseResponse();
                    object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                    responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);

                    if (responseOrder.Success)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ID: " + JsonConvert.SerializeObject(responseOrder), Logger.ConcatName(nameClass, nameMethod));
                        //Lay thong tin hop dong
                        List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);//Contract
                        List<JObject> objDetail = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 3);//Insured

                        String cert_no = objDetail[0]["CERTIFICATE_NO"].ToString();
                        String name = objDetail[0]["NAME"].ToString();

                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Cert: " + cert_no, Logger.ConcatName(nameClass, nameMethod));

                        JObject jRequestParam = new JObject();
                        jRequestParam.Add("A1", cert_no);
                        jRequestParam.Add("A2", name);
                        jRequestParam.Add("A3", "GENERAL");
                        //lấy thông tin json call
                        object[] paramContract = new object[] { "sys", "WEB", jRequestParam };
                        BaseResponse responseContract = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.SEARCH_LANDING, strEvm, paramContract);
                        if (responseContract.Success)
                        {
                            List<JObject> objSignInfo = goBusinessCommon.Instance.GetData<JObject>(responseContract, 0);
                            if (objSignInfo != null && objSignInfo.Any())
                            {
                                List<List<JObject>> lstData = new List<List<JObject>>();
                                lstData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(responseContract.Data));
                                response.Data = objSignInfo;
                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV2_SK_MFast(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string[] pack_1M = { "MFAST_GOI1", "MFAST_GOI2", "MFAST_GOI3" };
            string[] pack_3M = { "MFAST_GOI4" };
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    if (jData.ContainsKey("SELLER"))
                    {
                        jsonHealth.Remove("SELLER");
                        jsonHealth.Add("SELLER", jData["SELLER"]);                        
                    }
                    else
                    {
                        jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];
                        jsonHealth["SELLER"]["SELLER_CODE"] = jData["USERNAME"];                        
                    }

                    #endregion General

                    #region Buyer

                    JObject jBuyer = JObject.Parse(jData["BUYER"].ToString());
                    #region Buyer

                    jsonHealth["BUYER"]["NAME"] = jBuyer["NAME"];
                    jsonHealth["BUYER"]["DOB"] = jBuyer["DOB"];
                    jsonHealth["BUYER"]["IDCARD"] = jBuyer["IDCARD"];
                    jsonHealth["BUYER"]["EMAIL"] = jBuyer["EMAIL"];
                    jsonHealth["BUYER"]["PHONE"] = jBuyer["PHONE"];
                    jsonHealth["BUYER"]["ADDRESS"] = jBuyer["ADDRESS"];
                    jsonHealth["BUYER"]["TYPE"] = jBuyer["TYPE"];
                    jsonHealth["BUYER"]["GENDER"] = jBuyer["GENDER"];

                    #endregion Buyer

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jsonCusInfo in lsJTK_DataDetail)
                        {
                            JObject jHealth = JObject.Parse(lstHealthInsur[0].ToString());
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];
                            
                            jHealth["REGION"] = "VN";
                            String strEff = jsonCusInfo["EFFECTIVE_DATE"].ToString();
                            DateTime dtEFF = new DateTime();
                            DateTime dtEXP = new DateTime();
                            //String dtNow = DateTime.Now.ToString("dd/MM/yyyy");

                            //dtNow = dtNow + " 00:00";

                            //String effDate = jData["REQUEST_DATE"].ToString();

                            dtEFF = DateTime.ParseExact(strEff, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                            if(pack_1M.ToList().Contains(jsonCusInfo["PACK_CODE"].ToString()))
                            {
                                dtEXP = dtEFF.AddDays(30);
                            } 
                            else
                            {
                                //Goi 3 thang: Cong 90 ngay
                                dtEXP = dtEFF.AddDays(90);
                            }    
                            

                            jHealth["EFFECTIVE_DATE"] = dtEFF.ToString("dd/MM/yyyy HH:mm");
                            jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy HH:mm");
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            #region FileAttach
                            if (jsonCusInfo.ContainsKey("FILE_ATTACH"))
                            {
                                JArray arrFile = JArray.FromObject(jsonCusInfo["FILE_ATTACH"]);
                                jHealth.Add("FILE", arrFile);
                            }

                            #endregion FileAttach

                            jArrHealthInsur_Replace.Add(jHealth);
                        }
                        #region Health Insurers

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    if (jData["STATUS"] != null && jData["STATUS"].ToString() == "PAID")
                    {
                        pay = true;
                        jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                        jsonHealth["PAY_INFO"]["IS_SMS"] = "1";
                    }

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                if (mask.ACTION.Equals("BH_S"))
                {
                    //lấy thông tin json call
                    BaseResponse responseOrder = new BaseResponse();
                    object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                    responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);
                    if (responseOrder.Success)
                    {
                        List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);
                        if (objContract != null && objContract.Count > 0)
                        {
                            ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = objContract[0]["CONTRACT_CODE"].ToString();
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                        return response;
                    }

                }
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;



                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV2_SK_Vikki(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string[] pack_1M = { "" };
            string[] pack_3M = { "VIKKI_01" };
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {                
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    String dateNow = DateTime.Now.ToString("ddMMyyyy");                                      
                    String productCode = jData["PRODUCT_CODE"].ToString();
                    string strMD5 = goEncryptBase.Instance.Md5Encode(dateNow + "HDI@2024VIKKI" + productCode).ToLower();

                    if (!strMD5.ToLower().Equals(jData["MD5"].ToString().ToLower()))
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                    }

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    if (jData.ContainsKey("SELLER"))
                    {
                        jsonHealth.Remove("SELLER");
                        jsonHealth.Add("SELLER", jData["SELLER"]);
                    }
                    else
                    {
                        jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];
                        jsonHealth["SELLER"]["SELLER_CODE"] = jData["USERNAME"];
                    }

                    #endregion General

                    #region Buyer

                    JObject jBuyer = JObject.Parse(jData["BUYER"].ToString());
                    #region Buyer

                    jsonHealth["BUYER"]["NAME"] = jBuyer["NAME"];
                    jsonHealth["BUYER"]["DOB"] = jBuyer["DOB"];
                    jsonHealth["BUYER"]["IDCARD"] = jBuyer["IDCARD"];
                    jsonHealth["BUYER"]["EMAIL"] = jBuyer["EMAIL"];
                    jsonHealth["BUYER"]["PHONE"] = jBuyer["PHONE"];
                    jsonHealth["BUYER"]["ADDRESS"] = jBuyer["ADDRESS"];
                    jsonHealth["BUYER"]["TYPE"] = jBuyer["TYPE"];
                    jsonHealth["BUYER"]["GENDER"] = jBuyer["GENDER"];
                    jsonHealth["BUYER"]["TAXCODE"] = jBuyer["TAXCODE"];

                    #endregion Buyer

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jsonCusInfo in lsJTK_DataDetail)
                        {
                            JObject jHealth = JObject.Parse(lstHealthInsur[0].ToString());
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["CUS_ID"] = jsonCusInfo["CUS_ID"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];

                            jHealth["REGION"] = "VN";
                            String strEff = jsonCusInfo["EFFECTIVE_DATE"].ToString();
                            DateTime dtEFF = new DateTime();
                            DateTime dtEXP = new DateTime();
                            //String dtNow = DateTime.Now.ToString("dd/MM/yyyy");

                            //dtNow = dtNow + " 00:00";

                            //String effDate = jData["REQUEST_DATE"].ToString();

                            dtEFF = DateTime.ParseExact(strEff, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                            if (pack_1M.ToList().Contains(jsonCusInfo["PACK_CODE"].ToString()))
                            {
                                dtEXP = dtEFF.AddDays(30);
                            }
                            else
                            {
                                //Goi 3 thang: Cong 90 ngay
                                dtEXP = dtEFF.AddDays(90);
                            }


                            jHealth["EFFECTIVE_DATE"] = dtEFF.ToString("dd/MM/yyyy HH:mm");
                            jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy HH:mm");
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            #region FileAttach
                            if (jsonCusInfo.ContainsKey("FILE_ATTACH"))
                            {
                                JArray arrFile = JArray.FromObject(jsonCusInfo["FILE_ATTACH"]);
                                jHealth.Add("FILE", arrFile);
                            }

                            #endregion FileAttach

                            jArrHealthInsur_Replace.Add(jHealth);
                        }
                        #region Health Insurers

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    if (jData["STATUS"] != null && jData["STATUS"].ToString() == "PAID")
                    {
                        pay = true;
                        jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                        jsonHealth["PAY_INFO"]["IS_SMS"] = "1";
                    }

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                if (mask.ACTION.Equals("BH_S"))
                {
                    //lấy thông tin json call
                    BaseResponse responseOrder = new BaseResponse();
                    object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                    responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);
                    if (responseOrder.Success)
                    {
                        List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);
                        if (objContract != null && objContract.Count > 0)
                        {
                            ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = objContract[0]["CONTRACT_CODE"].ToString();
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                        return response;
                    }

                }
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;

                //Tao sản phẩm tai nạn Vikki về luồng Import                
                response = goBusinessOrders.Instance.GetBaseResponse_Orders_Import(reqOrd);
                if (response.Success)
                {                    
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Tạo đơn Vikki thành công", Logger.ConcatName(nameClass, nameMethod));
                    
                    //Tao dư liệu Syn sang Tikki, notity
                    OrderResInfo orderResInfo = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info resoponse" + JsonConvert.SerializeObject(orderResInfo), Logger.ConcatName(nameClass, nameMethod));
                    JObject objectSyn = new JObject();
                    objectSyn.Add("CONTRACT_CODE", orderResInfo.contract_code);                    

                    //lấy thông tin json call
                    object[] param = new object[] { orderResInfo.contract_code, "CONTRACT" };
                    response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.VIKKI_EXPORT_DATA, strEvm, param);
                    

                    if (response.Success)
                    {                        
                        List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Syndata" + JsonConvert.SerializeObject(jDataExport), Logger.ConcatName(nameClass, nameMethod));
                        String env_vikki = goConfig.Instance.GetAppConfig(goConstants.VIKKI_ENV);
                        new Task(() =>
                        {
                            this.createVikki_Notity(jDataExport, env_vikki);
                        }).Start();
                    }                    

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Đồng bộ Vikki Thành công", Logger.ConcatName(nameClass, nameMethod));
                }

                /*Sử dụng luồng Import
                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                */
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV2_SK_VikkiCreateAndPay(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string[] pack_1M = { "" };
            string[] pack_3M = { "VIKKI_01" };
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    if (jData.ContainsKey("SELLER"))
                    {
                        jsonHealth.Remove("SELLER");
                        jsonHealth.Add("SELLER", jData["SELLER"]);
                    }
                    else
                    {
                        jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];
                        jsonHealth["SELLER"]["SELLER_CODE"] = jData["USERNAME"];
                    }

                    #endregion General

                    #region Buyer

                    JObject jBuyer = JObject.Parse(jData["BUYER"].ToString());
                    #region Buyer

                    jsonHealth["BUYER"]["NAME"] = jBuyer["NAME"];
                    jsonHealth["BUYER"]["DOB"] = jBuyer["DOB"];
                    jsonHealth["BUYER"]["IDCARD"] = jBuyer["IDCARD"];
                    jsonHealth["BUYER"]["EMAIL"] = jBuyer["EMAIL"];
                    jsonHealth["BUYER"]["PHONE"] = jBuyer["PHONE"];
                    jsonHealth["BUYER"]["ADDRESS"] = jBuyer["ADDRESS"];
                    jsonHealth["BUYER"]["TYPE"] = jBuyer["TYPE"];
                    jsonHealth["BUYER"]["GENDER"] = jBuyer["GENDER"];
                    jsonHealth["BUYER"]["TAXCODE"] = jBuyer["TAXCODE"];

                    #endregion Buyer

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jsonCusInfo in lsJTK_DataDetail)
                        {
                            JObject jHealth = JObject.Parse(lstHealthInsur[0].ToString());
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["CUS_ID"] = jsonCusInfo["CUS_ID"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];

                            jHealth["REGION"] = "VN";
                            String strEff = jsonCusInfo["EFFECTIVE_DATE"].ToString();
                            DateTime dtEFF = new DateTime();
                            DateTime dtEXP = new DateTime();

                            dtEFF = DateTime.ParseExact(strEff, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                            dtEXP = dtEFF.AddMonths(12);


                            jHealth["EFFECTIVE_DATE"] = dtEFF.ToString("dd/MM/yyyy HH:mm");
                            jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy HH:mm");
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            #region FileAttach
                            if (jsonCusInfo.ContainsKey("FILE_ATTACH"))
                            {
                                JArray arrFile = JArray.FromObject(jsonCusInfo["FILE_ATTACH"]);
                                jHealth.Add("FILE", arrFile);
                            }

                            #endregion FileAttach

                            jArrHealthInsur_Replace.Add(jHealth);
                        }
                        #region Health Insurers

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    if (jData["STATUS"] != null && jData["STATUS"].ToString() == "PAID")
                    {
                        pay = true;
                        jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                        jsonHealth["PAY_INFO"]["IS_SMS"] = "1";
                    }

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                if (mask.ACTION.Equals("BH_S"))
                {
                    //lấy thông tin json call
                    BaseResponse responseOrder = new BaseResponse();
                    object[] param = new object[] { "sys", "WEB", jData["ORG_CODE"], jData["ORD_PARTNER"]["TRANS_ID"] };
                    responseOrder = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ORDER_BY_TRANSPARTNER, strEvm, param);
                    if (responseOrder.Success)
                    {
                        List<JObject> objContract = goBusinessCommon.Instance.GetData<JObject>(responseOrder, 2);
                        if (objContract != null && objContract.Count > 0)
                        {
                            ordersModel.COMMON.ORDER_DETAIL[0].REF_ID = objContract[0]["CONTRACT_CODE"].ToString();
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                            return response;
                        }
                    }
                    else
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "Lỗi không thấy được thông tin Order Partner " + jData["ORD_PARTNER"]["TRANS_ID"].ToString(), config, validate.ErrCode, validate.ErrMess);
                        return response;
                    }

                }
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;

                //Tao sản phẩm tai nạn Vikki về luồng Import                
                response = goBusinessOrders.Instance.GetBaseResponse_Orders_Import(reqOrd);
                if (response.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Tạo đơn Vikki thành công", Logger.ConcatName(nameClass, nameMethod));

                    //Tao dư liệu Syn sang Tikki, notity
                    OrderResInfo orderResInfo = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info resoponse" + JsonConvert.SerializeObject(orderResInfo), Logger.ConcatName(nameClass, nameMethod));
                    JObject objectSyn = new JObject();
                    objectSyn.Add("CONTRACT_CODE", orderResInfo.contract_code);

                    //lấy thông tin json call
                    object[] param = new object[] { orderResInfo.contract_code, "CONTRACT" };
                    response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.VIKKI_EXPORT_DATA, strEvm, param);


                    if (response.Success)
                    {
                        List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Syndata" + JsonConvert.SerializeObject(jDataExport), Logger.ConcatName(nameClass, nameMethod));
                        String env_vikki = goConfig.Instance.GetAppConfig(goConstants.VIKKI_ENV);
                        new Task(() =>
                        {
                            this.createVikki_Notity(jDataExport, env_vikki);
                        }).Start();
                    }

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Đồng bộ Vikki Thành công", Logger.ConcatName(nameClass, nameMethod));
                }

                /*Sử dụng luồng Import
                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
                */
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse VikiNotify(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string[] pack_1M = { "" };
            string[] pack_3M = { "VIKKI_01" };
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                String dateNow = DateTime.Now.ToString("ddMMyyyy");
                JObject jDataExport = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                String MD5 = jDataExport["MD5"].ToString();
                String productCode = jDataExport["PRODUCT_CODE"].ToString();
                string strMD5 = goEncryptBase.Instance.Md5Encode(dateNow + "HDI@2024VIKKI" + productCode).ToLower();

                if (!strMD5.ToLower().Equals(MD5.ToLower()))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }

                JObject jNotify = JObject.Parse(jDataExport["VIKKI_NOTI"].ToString());

                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Syndata" + JsonConvert.SerializeObject(jDataExport), Logger.ConcatName(nameClass, nameMethod));
                String env_vikki = goConfig.Instance.GetAppConfig(goConstants.VIKKI_ENV);
                new Task(() =>
                {
                    this.sendVikki_Notity(jNotify, env_vikki);
                }).Start();
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        private void createVikki_Notity(List<JObject> jDataExport, String env)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            JArray jArrData_Notify = new JArray();
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, goConfig.Instance.GetAppConfig(goConstants.VIKKI_NOTI_BUFFER + "_" + env), Logger.ConcatName(nameClass, nameMethod));
            int buffer_noti = int.Parse(goConfig.Instance.GetAppConfig(goConstants.VIKKI_NOTI_BUFFER + "_" + env));
            int batchId = 0;
            if(jDataExport.Count > 0)
            {
                JObject objData = jDataExport[0];
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Data 1" + JsonConvert.SerializeObject(objData), Logger.ConcatName(nameClass, nameMethod));
                JArray jArrData = JArray.Parse(objData["DATA"].ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Data 1" + JsonConvert.SerializeObject(jArrData), Logger.ConcatName(nameClass, nameMethod));
                if(jArrData.Count <= buffer_noti)
                {
                    batchId = batchId + 1;
                    JObject jDataNotify = new JObject();
                    jDataNotify.Add("batchId", batchId.ToString());
                    jDataNotify.Add("resultItems", jArrData);
                    this.sendVikki_Notity(jDataNotify, env);
                }
                else
                {
                    foreach (JObject jsonNotfy in jArrData)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Info Notify" + JsonConvert.SerializeObject(jsonNotfy), Logger.ConcatName(nameClass, nameMethod));
                        jArrData_Notify.Add(jsonNotfy);
                        if (jArrData_Notify.Count == buffer_noti)
                        {
                            batchId = batchId + 1;
                            JObject jDataNotify = new JObject();
                            jDataNotify.Add("batchId", batchId.ToString());
                            jDataNotify.Add("resultItems", jArrData_Notify);
                            this.sendVikki_Notity(jDataNotify, env);

                            jArrData_Notify = new JArray();
                        }
                    }
                }
                
            }
        }

        private void sendVikki_Notity(JObject jDataNotify, String env)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //String urlToken = "https://open-banking.sit.galaxyfinx.cloud/api/v1/partners/auth";
            String urlToken = goConfig.Instance.GetAppConfig(goConstants.VIKKI_LOGIN_URL + "_" + env);
            JObject jHeader = new JObject();

            JObject jobjLogin = new JObject();
            //jobjLogin.Add("clientId", "5om33e13oodhb527mim3au06o");
            //jobjLogin.Add("clientSecret", "kXdXNhlNh695fEeksac0SYkp4qkZ/r0S/EA1hWbfOw0=");
            jobjLogin.Add("clientId", goConfig.Instance.GetAppConfig(goConstants.VIKKI_LOGIN_CLIENTID + "_" + env));
            jobjLogin.Add("clientSecret", goConfig.Instance.GetAppConfig(goConstants.VIKKI_LOGIN_CLIENTSECRET + "_" + env));

            HttpResponseMessage responseLogin = postJsonAPI(jobjLogin, urlToken, jHeader);
            string strResLg = Regex.Unescape(responseLogin.Content.ReadAsStringAsync().Result.Replace("\\\"", ""));

            JObject jobLogin = JObject.Parse(strResLg);
            JObject jData = JObject.Parse(jobLogin["payload"]?.ToString());

            String strToken = jData["accessToken"]?.ToString();

            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Token:" + strToken, Logger.ConcatName(nameClass, nameMethod));

            JObject jHeaderData = new JObject();
            String strTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            jHeaderData.Add("Authorization", "Bearer " + strToken);
            jHeaderData.Add("X-Request-Timestamp", strTimestamp);
            //jHeaderData.Add("X-Api-Key", "eEHuW7PBiI4CtG2eS01Rk8PZUx4eKvaD5H7QWGcU");
            jHeaderData.Add("X-Api-Key", goConfig.Instance.GetAppConfig(goConstants.VIKKI_API_KEY + "_" + env));
            //jHeaderData.Add("X-Request-ID", makeid(26, env));
            int indexChar = int.Parse(goConfig.Instance.GetAppConfig(goConstants.VIKKI_INDEX + "_" + env));
            jHeaderData.Add("X-Request-ID", makeid(indexChar, env));

            //String messageHmac = strTimestamp + "POST" + "/api/open-banking/v1/hdi/result/callback" + JsonConvert.SerializeObject(jDataExport);
            
            String messageHmac = strTimestamp + "POST" + goConfig.Instance.GetAppConfig(goConstants.VIKKI_PATH_CALLBACK + "_" + env) + JsonConvert.SerializeObject(jDataNotify);
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Notify Send:" + messageHmac, Logger.ConcatName(nameClass, nameMethod));
            //String haskingKey = "tGJmAubXDLNRDRvkpaWNvh/+BiytdEVyfg==";
            String haskingKey = goConfig.Instance.GetAppConfig(goConstants.VIKKI_HASHKEY + "_" + env);

            String hmac = goEncryptMix.Instance.base64tHmacSha256(messageHmac, haskingKey);

            jHeaderData.Add("X-Payload-Signature", hmac);

            //String urlData = "https://open-banking.sit.galaxyfinx.cloud/api/open-banking/v1/hdi/result/callback";
            String urlData = goConfig.Instance.GetAppConfig(goConstants.VIKKI_PATH_CALLBACKỦL + "_" + env);

            HttpResponseMessage responseNotify = postJsonAPI(jDataNotify, urlData, jHeaderData);
            string strResNotiLg = Regex.Unescape(responseNotify.Content.ReadAsStringAsync().Result.Replace("\\\"", ""));
            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Vikki Notify:" + strResNotiLg, Logger.ConcatName(nameClass, nameMethod));
            //String urlCallBack = goConfig.Instance.GetAppConfig(goConstants.E_COM_CALLBACK + "_" + env);
            //String hashkey = goConfig.Instance.GetAppConfig(goConstants.E_COM_HASHKEY + "_" + env);
        }

        String makeid(int length, string env)
        {
            var result = "";
            //var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var characters = goConfig.Instance.GetAppConfig(goConstants.VIKKI_CHARACTERS + "_" + env);
            int charactersLength = characters.Length;
            for (var i = 0; i < length; i++)
            {
                Random random = new Random();
                Double val = random.NextDouble();
                Double index = val * charactersLength;
                String strIndex = Math.Floor(index).ToString();
                result += characters.ElementAt(Int32.Parse(strIndex)); //(Math.Floor(val * Decimal.Parse(charactersLength.ToString())));
            }
            return result;
        }

        public HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PostAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API call pvc ", ex.Message + JInput.ToString() + strUrl + "Lỗi" + ex.Message);
                throw new Exception("Service timeout " + ex.ToString());
            }

        }

        public BaseResponse GetResMaskV2_SK_HDSaison(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];

                    #endregion General

                    #region Buyer

                    jsonHealth.Remove("BUYER");
                    jsonHealth.Add("BUYER", jData["BUYER"]);

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);
                    JToken jsonCusInfo = null;
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        jsonCusInfo = lsJTK_DataDetail[0];

                        #region Health Insurers

                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jHealth in lstHealthInsur)
                        {
                            jHealth["CERTIFICATE_NO"] = jsonCusInfo["CERTIFICATE_NO"];
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];
                            jHealth["REGION"] = "VN";
                            DateTime dtEFF = new DateTime();
                            DateTime dtEXP = new DateTime();

                            String effDate = jsonCusInfo["EFFECTIVE_DATE"].ToString();

                            dtEFF = DateTime.ParseExact(effDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

                            dtEXP = dtEFF.AddYears(1);

                            jHealth["EFFECTIVE_DATE"] = effDate;
                            jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy HH:mm");
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            jArrHealthInsur_Replace.Add(jHealth);
                        }

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    pay = true;
                    jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                    jsonHealth["PAY_INFO"]["IS_SMS"] = "1";

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());


                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;

                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse GetResMaskV2_KSND(BaseRequest request, string tracking)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                //Create Json Order Model Partner
                JObject jsonHealth = createJsonMask_Health();
                JObject jData = JsonConvert.DeserializeObject<JObject>(request.Data.ToString());
                bool pay = false;
                if (jData != null)
                {
                    if (jData.ContainsKey("ORD_PARTNER"))
                    {
                        jsonHealth.Add("ORD_PARTNER", jData["ORD_PARTNER"]);
                    }
                    #region General

                    jsonHealth["USERNAME"] = jData["USERNAME"];
                    jsonHealth["ORG_CODE"] = jData["ORG_CODE"];
                    jsonHealth["ACTION"] = jData["ACTION"];
                    jsonHealth["CATEGORY"] = jData["CATEGORY"];
                    jsonHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];

                    jsonHealth["SELLER"]["ORG_CODE"] = jData["ORG_CODE"];

                    #endregion General

                    #region Buyer

                    jsonHealth.Remove("BUYER");
                    jsonHealth.Add("BUYER", jData["BUYER"]);

                    #endregion Buyer

                    JArray jArrCusInfo = JArray.FromObject(jData["CUS_INFO"]);
                    JToken jsonCusInfo = null;
                    if (jArrCusInfo != null && jArrCusInfo.Count > 0)
                    {
                        List<JToken> lsJTK_DataDetail = jArrCusInfo.ToList();
                        jsonCusInfo = lsJTK_DataDetail[0];

                        #region Health Insurers

                        JArray jArrHealthInsur = JArray.FromObject(jsonHealth["HEALTH_INSUR"]);
                        List<JToken> lstHealthInsur = jArrHealthInsur.ToList();
                        JArray jArrHealthInsur_Replace = new JArray();
                        foreach (JObject jHealth in lstHealthInsur)
                        {                            
                            jHealth["TYPE"] = "CN";
                            jHealth["NATIONALITY"] = "VN";
                            jHealth["NAME"] = jsonCusInfo["NAME"];
                            jHealth["DOB"] = jsonCusInfo["DOB"];
                            jHealth["IDCARD"] = jsonCusInfo["IDCARD"];
                            jHealth["EMAIL"] = jsonCusInfo["EMAIL"];
                            jHealth["PHONE"] = jsonCusInfo["PHONE"];
                            jHealth["RELATIONSHIP"] = jsonCusInfo["RELATIONSHIP"];
                            jHealth["REGION"] = "VN";
                            DateTime dtEFF = new DateTime();
                            DateTime dtEXP = new DateTime();

                            String effDate = jsonCusInfo["EFFECTIVE_DATE"].ToString();
                            String expDate = jsonCusInfo["EXPIRATION_DATE"].ToString();

                            dtEFF = DateTime.ParseExact(effDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            dtEXP = DateTime.ParseExact(expDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                            jHealth["EFFECTIVE_DATE"] = effDate;
                            jHealth["EXPIRATION_DATE"] = dtEXP.ToString("dd/MM/yyyy");
                            jHealth["PRODUCT_CODE"] = jData["PRODUCT_CODE"];
                            jHealth["PACK_CODE"] = jsonCusInfo["PACK_CODE"];

                            jArrHealthInsur_Replace.Add(jHealth);
                        }

                        jsonHealth.Remove("HEALTH_INSUR");
                        jsonHealth.Add("HEALTH_INSUR", jArrHealthInsur_Replace);

                        #endregion Health Insurers
                    }

                    #region Pay

                    //Neu xac nhan da thu phi se chuyen trang thai Tho ho, tu sinh GCN, gui GCN qua Email, SMS

                    pay = true;
                    jsonHealth["PAY_INFO"]["PAYMENT_TYPE"] = "TH";
                    jsonHealth["PAY_INFO"]["IS_SMS"] = "0";

                    #endregion Pay
                }
                //End

                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    //mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());
                    mask = JsonConvert.DeserializeObject<MaskInsur>(jsonHealth.ToString());


                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);

                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;

                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_Create(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public MaskValidate ValidateAge(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            switch (mask.CATEGORY)
            {
                case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                    //validate = ValidateLo(mask);
                    break;
                case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                    foreach (var item in mask.HEALTH_INSUR)
                    {
                        try
                        {
                            DateTime dob = goUtility.Instance.ConvertToDateTime(item.DOB, DateTime.MinValue);
                            DateTime eff = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue);

                            //goUtility.Instance.getAge(dob, eff, ref item.age, ref item.age_y, ref item.age_m, ref item.age_d);
                            item.age = goUtility.Instance.getAge(dob, eff);
                            item.age_y = goUtility.Instance.getAgeYear(dob, eff);
                            item.age_m = goUtility.Instance.getAgeMonth(dob, eff);
                            item.age_d = goUtility.Instance.getAgeDay(dob, eff);

                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    List<InsuredPerson> lstInsurD18 = mask.HEALTH_INSUR.Where(i => i.age < 18).ToList();
                    List<InsuredPerson> lstInsurT18 = mask.HEALTH_INSUR.Where(i => i.age >= 18).ToList();
                    if (mask.BUYER.BUYER_MODE.Equals("GD"))
                    {
                        //TH: có trẻ dưới 18
                        if (lstInsurD18 != null && lstInsurD18.Any())
                        {
                            //nếu không có ng lớn >= 18 mua kèm => lỗi
                            if (lstInsurT18 == null || !lstInsurT18.Any())
                            {
                                validate.Err = true;
                                validate.ErrCode = "ERR_AGE";
                                validate.ErrMess = "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ";
                            }
                            else //nếu có người lớn => đạt yêu cầu
                            {
                            }
                        }
                        else
                        {
                        }
                    }

                    if (mask.BUYER.BUYER_MODE.Equals("CN"))
                    {
                        //TH: có trẻ dưới 18
                        if (lstInsurD18 != null && lstInsurD18.Any())
                        {
                            //nếu không có ng lớn >= 18 mua kèm => lỗi
                            if (true)//(lstInsurT18 == null || !lstInsurT18.Any())
                            {
                                validate.Err = true;
                                validate.ErrCode = "ERR_AGE";
                                validate.ErrMess = "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ";
                            }
                            else //nếu có người lớn => đạt yêu cầu
                            {
                            }
                        }
                        else
                        {
                        }
                    }
                    break;
                case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                    break;
                case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                    //validate = ValidateHouse(mask);
                    break;
            }
            return validate;
        }

        private MaskValidate ValidateReqV2(MaskInsur mask)
        {
            MaskValidate validate = new MaskValidate();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (mask == null || string.IsNullOrEmpty(mask.CATEGORY))
                {
                    validate.Err = true;
                    validate.ErrCode = nameof(goConstantsError.Instance.ERROR_6001);
                    validate.ErrMess = goConstantsError.Instance.ERROR_6001;
                    return validate;
                }
                switch (mask.CATEGORY)
                {
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                        //validate = ValidateLo(mask);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_SK) || mask.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                        validate = ValidateHealth(mask);
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        break;
                    case var s when mask.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                        //validate = ValidateHouse(mask);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return validate;
        }

        #endregion

        #region Preview GCN
        public BaseResponse GetResMask_Preview(BaseRequest request, string tracking, bool pay = false)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "tracking: " + tracking, Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json1: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                MaskInsur mask = new MaskInsur();
                if (request.Data != null)
                    mask = JsonConvert.DeserializeObject<MaskInsur>(request.Data.ToString());

                if (mask.ToString().Length < 10000)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(mask), Logger.ConcatName(nameClass, nameMethod));
                }

                MaskValidate validate = ValidateReq(mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // Validate define
                MaskValidate validateV2 = ValidateReqDefine(mask, request);
                if (validateV2.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validateV2.ErrCode, validateV2.ErrMess);
                    return response;
                }

                // Validate tính phí
                validate = CalFeesMask(request, ref mask);
                if (validate.Err)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, validate.ErrCode, validate.ErrMess);
                    return response;
                }

                // gán tracking code vào buyer
                if (!string.IsNullOrEmpty(tracking))
                {
                    mask.BUYER.SHARE_CODE = tracking;
                }

                OrdersModel ordersModel = GetOrdersByMask(mask, pay);
                // Add 2022-04-07 By ThienTVB
                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                // End 2022-04-07 By ThienTVB
                BaseRequest reqOrd = request;
                reqOrd.Data = ordersModel;
                if (pay)
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay_Preview(reqOrd);
                else
                    response = goBusinessOrders.Instance.GetBaseResponse_Orders_CreateAndPay_Preview(reqOrd);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }
        #endregion Preview GCN

        #region Yêu cầu thanh toán
        public BaseResponse ContractPaymentAction(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrderPaymentAction orderPayment = new OrderPaymentAction();
                if (request.Data != null)
                    orderPayment = JsonConvert.DeserializeObject<OrderPaymentAction>(JsonConvert.SerializeObject(request.Data));

                if (orderPayment.PAY_INFO == null || string.IsNullOrEmpty(orderPayment.CONTRACT_CODE))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                }
                string payment_type = orderPayment.PAY_INFO.PAYMENT_TYPE;
                bool isSms = goUtility.Instance.CastToBoolean(orderPayment.PAY_INFO?.IS_SMS);
                bool isEmail = goUtility.Instance.CastToBoolean(orderPayment.PAY_INFO?.IS_EMAIL);

                var param = new
                {
                    CHANNEL = orderPayment.CHANNEL,
                    USERNAME = orderPayment.USERNAME,
                    ORG_SELLER = orderPayment.ORG_SELLER,
                    CONTRACT_CODE = orderPayment.CONTRACT_CODE,
                    STR_PERIOD = orderPayment.STR_PERIOD,
                    payment_type = payment_type,
                    payment_mix = JsonConvert.SerializeObject(orderPayment.PAY_INFO.PAYMENT_MIX)
                };
                BaseRequest reqInfo = request;
                reqInfo.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Send create payment: " + JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param)), Logger.ConcatName(nameClass, nameMethod));
                BaseResponse resPay = goBusinessAction.Instance.GetBaseResponse(reqInfo);

                List<OrderTransInfo> lstOrdTrans = new List<OrderTransInfo>();
                List<OrdersInfo> lstOrd = new List<OrdersInfo>();
                List<OrderDetailInfo> lstOrdDetail = new List<OrderDetailInfo>();
                if (resPay.Success)
                {
                    response.Success = true;
                    lstOrdTrans = goBusinessCommon.Instance.GetData<OrderTransInfo>(resPay, 0);

                    List<InsurContract> lstContract = goBusinessCommon.Instance.GetData<InsurContract>(resPay, 8);
                    List<InsurDetail> lstDetails = goBusinessCommon.Instance.GetData<InsurDetail>(resPay, 9);
                    if (lstOrdTrans == null && !lstOrdTrans.Any())
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6006), goConstantsError.Instance.ERROR_6006);
                    }
                    if (lstOrdTrans[0].status != null && lstOrdTrans[0].status.Equals(nameof(goConstants.OrderStatus.PAID)))
                    {
                        goBusinessOrders.Instance.OrderCreateAndPay(ref response, lstContract, lstDetails);
                    }
                    else
                    {
                        response = goBusinessOrders.Instance.GetResOrdTrans(resPay, request, config, payment_type, isSms, isEmail);
                        //lstOrd = goBusinessCommon.Instance.GetData<OrdersInfo>(resPay, 6);
                        //lstOrdDetail = goBusinessCommon.Instance.GetData<OrderDetailInfo>(resPay, 7);
                        //goBusinessOrders.Instance.GetResActionPayment(ref response, resPay, request, config, payment_type, lstOrdTrans, lstOrd, lstOrdDetail, isSms, isEmail);
                    }
                    // Chuẩn hóa data trả về
                    //List<ResInsurData> dataOut = goBusinessOrders.Instance.ResGetData_V2(lstContract, lstDetails);
                    //response.Data = dataOut;
                }
                else
                    response = resPay;
                goBusinessCommon.Instance.SignResponse(ref response, config);
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }
        #endregion

        #region Chung
        public BaseResponse GetDetailInsur(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                BaseResponse res_dt = goBusinessAction.Instance.GetBaseResponse(request);
                if (res_dt.Success)
                {
                    object[] goParam = null;
                    if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                        goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);
                    object data = null;
                    if (goParam.Count() > 4)
                    {
                        string category = goParam[3]?.ToString();
                        switch (category)
                        {
                            case var s when category.Equals(goConstants.CATEGORY_XE):
                                data = GetDetailVehicle(res_dt);
                                response = goBusinessCommon.Instance.getResultApi(true, data, config);
                                break;
                            case var s when category.Equals(goConstants.CATEGORY_NHATN):
                                data = GetDetailHouse(res_dt);
                                response = goBusinessCommon.Instance.getResultApi(true, data, config);
                                break;
                            case var s when category.Equals(goConstants.CATEGORY_SK) || category.Equals(goConstants.CATEGORY_TAINAN):
                                data = GetDetailHealth(res_dt);
                                response = goBusinessCommon.Instance.getResultApi(true, data, config);
                                break;
                        }

                    }
                }
                else
                    response = res_dt;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        private object GetDetailVehicle(BaseResponse res_dt)
        {
            try
            {
                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(res_dt, 0).FirstOrDefault();
                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(res_dt, 1);
                List<VehicleInsurDetail> lstVehicleExt = goBusinessCommon.Instance.GetData<VehicleInsurDetail>(res_dt, 2);
                VehicleInfo vehicleInfo = goBusinessCommon.Instance.GetData<VehicleInfo>(res_dt, 3).FirstOrDefault();

                // file GCN + file thông tin liên quan hợp đồng
                List<DetailFile> files = goBusinessCommon.Instance.GetData<DetailFile>(res_dt, 4);

                // Thông tin giám định với VCX
                List<DetailCalendar> lstDtCalen = goBusinessCommon.Instance.GetData<DetailCalendar>(res_dt, 5);

                // thông tin thụ hưởng
                List<Beneficiary> lstBennefi = goBusinessCommon.Instance.GetData<Beneficiary>(res_dt, 6);

                // thông tin bổ sung
                List<PHYSICAL_DAMAGE_ADDITIONAL> lstAddi = goBusinessCommon.Instance.GetData<PHYSICAL_DAMAGE_ADDITIONAL>(res_dt, 7);

                // file giám định
                List<DetailFile> filesSurveyor = goBusinessCommon.Instance.GetData<DetailFile>(res_dt, 8);

                // thông tin chi tiết phí
                List<VehicleInsurFees> lstFeesVeh = goBusinessCommon.Instance.GetData<VehicleInsurFees>(res_dt, 9);

                //thông tin bổ sung của hợp đồng
                ContractExtend contractExtend = goBusinessCommon.Instance.GetData<ContractExtend>(res_dt, 10).FirstOrDefault();

                VehicleInsur vehicle = new VehicleInsur();
                vehicle.VEHICLE_INSUR = new List<VEHICLE_PRODUCT>();
                if (insurContract != null)
                {
                    vehicle.ORG_CODE = insurContract.ORG_SELLER;
                    vehicle.CHANNEL = insurContract.CHANEL;
                    vehicle.USERNAME = insurContract.SELLER_CODE;
                    vehicle.BUYER = new ORD_BUYER();
                    vehicle.BUYER.CUS_ID = insurContract.CUS_CODE;
                    vehicle.BUYER.TYPE = insurContract.TYPE;
                    vehicle.BUYER.FAX = insurContract.FAX;
                    vehicle.BUYER.TAXCODE = insurContract.TAXCODE;

                    vehicle.BUYER.NATIONALITY = insurContract.NATIONALITY;
                    vehicle.BUYER.NAME = insurContract.NAME;
                    vehicle.BUYER.DOB = insurContract.DOB;
                    vehicle.BUYER.GENDER = insurContract.GENDER;
                    vehicle.BUYER.PROV = insurContract.PROVINCE;
                    vehicle.BUYER.DIST = insurContract.DISTRICT;
                    vehicle.BUYER.WARDS = insurContract.WARDS;
                    vehicle.BUYER.ADDRESS = insurContract.ADDRESS;
                    vehicle.BUYER.IDCARD = insurContract.IDCARD;
                    vehicle.BUYER.IDCARD_D = insurContract.IDCARD_D;
                    vehicle.BUYER.IDCARD_P = insurContract.IDCARD_P;
                    vehicle.BUYER.EMAIL = insurContract.EMAIL;
                    vehicle.BUYER.PHONE = insurContract.PHONE;
                    vehicle.BUYER.ADDRESS_FORM = insurContract.ADDRESS_FORM;
                }
                if (insurDetails != null && insurDetails.Any())
                {
                    VEHICLE_PRODUCT product = new VEHICLE_PRODUCT();

                    // thông tin xe
                    product.VEHICLE_GROUP = vehicleInfo.VEHICLE_GROUP;
                    product.VEHICLE_TYPE = vehicleInfo.VEHICLE_TYPE;
                    product.VEHICLE_USE = vehicleInfo.VEHICLE_USE;
                    product.NONE_NUMBER_PLATE = vehicleInfo.NONE_NUMBER_PLATE;
                    product.NUMBER_PLATE = vehicleInfo.NUMBER_PLATE;
                    product.CHASSIS_NO = vehicleInfo.CHASSIS_NO;
                    product.ENGINE_NO = vehicleInfo.ENGINE_NO;
                    product.SEAT_NO = vehicleInfo.SEAT_NO;
                    product.WEIGH = vehicleInfo.WEIGH;
                    product.BRAND = vehicleInfo.BRAND;
                    product.MODEL = vehicleInfo.MODEL;
                    product.MFG = vehicleInfo.MFG;
                    product.CAPACITY = vehicleInfo.CAPACITY;
                    product.REF_VEHICLE_VALUE = vehicleInfo.REF_VEHICLE_VALUE;
                    product.VEHICLE_REGIS = vehicleInfo.VEHICLE_REGIS;
                    product.VEHICLE_VALUE = vehicleInfo.VEHICLE_VALUE;

                    if (insurDetails.Count == 1) // chỉ mua VCX hoặc chỉ mua BB
                    {
                        var item = insurDetails[0];
                        product.CUS_ID = item.CUS_CODE;
                        product.TYPE = item.TYPE;
                        product.FAX = item.FAX;
                        product.TAXCODE = item.TAXCODE;

                        product.NATIONALITY = item.NATIONALITY;
                        product.NAME = item.NAME;
                        product.DOB = item.DOB;
                        product.GENDER = item.GENDER;
                        product.PROV = item.PROVINCE;
                        product.DIST = item.DISTRICT;
                        product.WARDS = item.WARDS;
                        product.ADDRESS = item.ADDRESS;
                        product.ADDRESS_FORM = item.ADDRESS_FORM;
                        product.IDCARD = item.IDCARD;
                        product.IDCARD_D = item.IDCARD_D;
                        product.IDCARD_P = item.IDCARD_P;
                        product.EMAIL = item.EMAIL;
                        product.PHONE = item.PHONE;
                        product.RELATIONSHIP = item.RELATIONSHIP;

                        product.CONTRACT_CODE = insurContract.CONTRACT_CODE;
                        product.AMOUNT = item.AMOUNT;
                        product.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT;
                        product.TOTAL_VAT = item.VAT;
                        product.TOTAL_AMOUNT = item.TOTAL_AMOUNT;

                        VehicleInsurDetail vehicleDt = lstVehicleExt.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList().FirstOrDefault();
                        // thông tin ext
                        product.IS_SPLIT_ORDER = vehicleDt.IS_SPLIT_ORDER;
                        product.IS_SEND_MAIL = vehicleDt.IS_SEND_MAIL;
                        product.SEAT_CODE = vehicleDt.SEAT_CODE;
                        product.WEIGH_CODE = vehicleDt.WEIGH_CODE;

                        product.IS_VOLUNTARY = vehicleDt.IS_VOLUNTARY;
                        product.IS_DRIVER = vehicleDt.IS_DRIVER;
                        product.IS_CARGO = vehicleDt.IS_CARGO;
                        product.IS_COMPULSORY = vehicleDt.IS_COMPULSORY;
                        product.IS_VOLUNTARY_ALL = vehicleDt.IS_VOLUNTARY_ALL;
                        product.IS_PHYSICAL = vehicleDt.IS_PHYSICAL;
                        product.VEHICLE_REGIS_CODE = vehicleDt.VEHICLE_REGIS_CODE;
                        product.VEHICLE_VALUE_CODE = vehicleDt.VEHICLE_VALUE_CODE;

                        #region TNDS BB

                        #region Old

                        if (vehicleDt.IS_COMPULSORY == "1" && item.PRODUCT_CODE == "XCG_TNDSBB")
                        {
                            // thông tin BB
                            product.COMPULSORY_CIVIL = new COMPULSORY_CIVIL_LIABILITY();
                            product.COMPULSORY_CIVIL.PRODUCT_CODE = item.PRODUCT_CODE;
                            product.COMPULSORY_CIVIL.PACK_CODE = item.PACK_CODE;
                            product.COMPULSORY_CIVIL.DETAIL_CODE = vehicleDt.DETAIL_CODE;
                            product.COMPULSORY_CIVIL.IS_SERIAL_NUM = vehicleDt.IS_SERIAL_NUM;
                            product.COMPULSORY_CIVIL.SERIAL_NUM = vehicleDt.SERIAL_NUM;
                            product.COMPULSORY_CIVIL.EFF = item.EFFECTIVE_DATE;
                            product.COMPULSORY_CIVIL.TIME_EFF = item.EFFECTIVE_TIME;
                            product.COMPULSORY_CIVIL.EXP = item.EXPIRATION_DATE;
                            product.COMPULSORY_CIVIL.TIME_EXP = item.EXPIRATION_TIME;
                            product.COMPULSORY_CIVIL.FEES = item.PACK_FEES - vehicleDt.FEES_VOLUNTARY_CIVIL;
                            product.COMPULSORY_CIVIL.AMOUNT = item.AMOUNT - vehicleDt.AMOUNT_VOLUNTARY_CIVIL;
                            product.COMPULSORY_CIVIL.VAT = item.VAT - vehicleDt.VAT_VOLUNTARY_CIVIL;
                            product.COMPULSORY_CIVIL.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT - vehicleDt.DISCOUNT_VOLUNTARY_CIVIL;
                            product.COMPULSORY_CIVIL.TOTAL_AMOUNT = item.TOTAL_AMOUNT - vehicleDt.TOTAL_VOLUNTARY_CIVIL;
                            product.COMPULSORY_CIVIL.STATUS = item.STATUS;
                            product.COMPULSORY_CIVIL.STATUS_NAME = item.STATUS_NAME;
                            product.COMPULSORY_CIVIL.FILE = new List<DetailFile>();
                            if (files != null && files.Any())
                            {
                                List<DetailFile> file_detail = files.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                product.COMPULSORY_CIVIL.FILE = file_detail;
                            }
                        }

                        #endregion

                        if (vehicleDt.IS_COMPULSORY == "1" && item.PRODUCT_CODE != "XCG_TNDSBB")
                        {
                            product.COMPULSORY_CIVIL = new COMPULSORY_CIVIL_LIABILITY();
                            product.COMPULSORY_CIVIL.PRODUCT_CODE = item.PRODUCT_CODE;
                            product.COMPULSORY_CIVIL.PACK_CODE = item.PACK_CODE;
                            product.COMPULSORY_CIVIL.DETAIL_CODE = vehicleDt.DETAIL_CODE;
                            product.COMPULSORY_CIVIL.IS_SERIAL_NUM = vehicleDt.IS_SERIAL_NUM;
                            product.COMPULSORY_CIVIL.SERIAL_NUM = vehicleDt.SERIAL_NUM;
                            product.COMPULSORY_CIVIL.EFF = item.EFFECTIVE_DATE;
                            product.COMPULSORY_CIVIL.TIME_EFF = item.EFFECTIVE_TIME;
                            product.COMPULSORY_CIVIL.EXP = item.EXPIRATION_DATE;
                            product.COMPULSORY_CIVIL.TIME_EXP = item.EXPIRATION_TIME;
                            product.COMPULSORY_CIVIL.STATUS = item.STATUS;
                            product.COMPULSORY_CIVIL.STATUS_NAME = item.STATUS_NAME;
                            product.COMPULSORY_CIVIL.FILE = new List<DetailFile>();
                            if (files != null && files.Any())
                            {
                                List<DetailFile> file_detail = files.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                product.COMPULSORY_CIVIL.FILE = file_detail;
                            }

                            VehicleInsurFees bbInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.TNDSBB_F.ToString())).ToList().FirstOrDefault();
                            if (bbInfo != null)
                            {
                                product.COMPULSORY_CIVIL.FEES_DATA = bbInfo.FEES_DATA;
                                product.COMPULSORY_CIVIL.FEES = bbInfo.FEES;
                                product.COMPULSORY_CIVIL.AMOUNT = bbInfo.AMOUNT;
                                product.COMPULSORY_CIVIL.VAT = bbInfo.VAT;
                                product.COMPULSORY_CIVIL.TOTAL_DISCOUNT = bbInfo.TOTAL_DISCOUNT;
                                product.COMPULSORY_CIVIL.TOTAL_AMOUNT = bbInfo.TOTAL_AMOUNT;
                            }
                        }
                        #endregion

                        #region TNDS TN
                        if (vehicleDt.IS_VOLUNTARY_ALL == "1")
                        {
                            // Thông tin TN
                            product.VOLUNTARY_CIVIL = new VOLUNTARY_CIVIL_LIABILITY();
                            product.VOLUNTARY_CIVIL.PRODUCT_CODE = vehicleDt.PRODUCT_VOLUNTARY_CIVIL;
                            product.VOLUNTARY_CIVIL.PACK_CODE = vehicleDt.PACK_VOLUNTARY_CIVIL;
                            product.VOLUNTARY_CIVIL.INSUR_3RD = vehicleDt.INSUR_3RD;
                            product.VOLUNTARY_CIVIL.INSUR_PASSENGER = vehicleDt.INSUR_PASSENGER;
                            product.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS = vehicleDt.INSUR_3RD_ASSETS;
                            product.VOLUNTARY_CIVIL.TOTAL_LIABILITY = vehicleDt.TOTAL_LIABILITY;
                            product.VOLUNTARY_CIVIL.DRIVER_NUM = vehicleDt.DRIVER_NUM;
                            product.VOLUNTARY_CIVIL.DRIVER_INSUR = vehicleDt.DRIVER_INSUR;
                            product.VOLUNTARY_CIVIL.WEIGHT_CARGO = vehicleDt.WEIGHT_CARGO;
                            product.VOLUNTARY_CIVIL.CARGO_INSUR = vehicleDt.CARGO_INSUR;

                            VehicleInsurFees tnInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).ToList().FirstOrDefault();
                            if (tnInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA = tnInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES = tnInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT = tnInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT = tnInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT = tnInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_AMOUNT = tnInfo.TOTAL_AMOUNT;
                            }

                            //product.VOLUNTARY_CIVIL.FEES = vehicleDt.FEES_VOLUNTARY_CIVIL;
                            //product.VOLUNTARY_CIVIL.AMOUNT = vehicleDt.AMOUNT_VOLUNTARY_CIVIL;
                            //product.VOLUNTARY_CIVIL.VAT = vehicleDt.VAT_VOLUNTARY_CIVIL;
                            //product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT = vehicleDt.DISCOUNT_VOLUNTARY_CIVIL;
                            //product.VOLUNTARY_CIVIL.TOTAL_AMOUNT = vehicleDt.TOTAL_VOLUNTARY_CIVIL;

                            VehicleInsurFees vqmInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).ToList().FirstOrDefault();
                            if (vqmInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = vqmInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = vqmInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = vqmInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = vqmInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = vqmInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = vqmInfo.TOTAL_AMOUNT;
                            }

                            VehicleInsurFees t3rdInsurInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).ToList().FirstOrDefault();
                            if (t3rdInsurInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = t3rdInsurInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_3RD_INSUR = t3rdInsurInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = t3rdInsurInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_3RD_INSUR = t3rdInsurInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = t3rdInsurInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = t3rdInsurInfo.TOTAL_AMOUNT;
                            }

                            VehicleInsurFees t3rdAssetsInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).ToList().FirstOrDefault();
                            if (t3rdAssetsInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = t3rdAssetsInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_3RD_ASSETS = t3rdAssetsInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = t3rdAssetsInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_3RD_ASSETS = t3rdAssetsInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = t3rdAssetsInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = t3rdAssetsInfo.TOTAL_AMOUNT;
                            }

                            VehicleInsurFees t3rdPassengerInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).ToList().FirstOrDefault();
                            if (t3rdPassengerInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = t3rdPassengerInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_PASSENGER = t3rdPassengerInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_PASSENGER = t3rdPassengerInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_PASSENGER = t3rdPassengerInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = t3rdPassengerInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_PASSENGER = t3rdPassengerInfo.TOTAL_AMOUNT;
                            }

                            VehicleInsurFees driverInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).ToList().FirstOrDefault();
                            if (driverInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_DRIVER = driverInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_DRIVER = driverInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_DRIVER = driverInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_DRIVER = driverInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = driverInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_DRIVER = driverInfo.TOTAL_AMOUNT;
                            }
                            VehicleInsurFees cargoInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).ToList().FirstOrDefault();
                            if (cargoInfo != null)
                            {
                                product.VOLUNTARY_CIVIL.FEES_DATA_CARGO = cargoInfo.FEES_DATA;
                                product.VOLUNTARY_CIVIL.FEES_CARGO = cargoInfo.FEES;
                                product.VOLUNTARY_CIVIL.AMOUNT_CARGO = cargoInfo.AMOUNT;
                                product.VOLUNTARY_CIVIL.VAT_CARGO = cargoInfo.VAT;
                                product.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = cargoInfo.TOTAL_DISCOUNT;
                                product.VOLUNTARY_CIVIL.TOTAL_CARGO = cargoInfo.TOTAL_AMOUNT;
                            }
                            //product.VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = vehicleDt.TOTAL_VOLUNTARY_3RD;
                            //product.VOLUNTARY_CIVIL.TOTAL_DRIVER = vehicleDt.TOTAL_DRIVER;
                            //product.VOLUNTARY_CIVIL.TOTAL_CARGO = vehicleDt.TOTAL_CARGO;
                            //product.VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = vehicleDt.TOTAL_3RD_INSUR;
                            //product.VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = vehicleDt.TOTAL_3RD_ASSETS;
                            //product.VOLUNTARY_CIVIL.TOTAL_PASSENGER = vehicleDt.TOTAL_PASSENGER;
                        }
                        #endregion

                        #region VCX
                        if (vehicleDt.IS_PHYSICAL == "1")
                        {
                            product.PHYSICAL_DAMAGE = new PHYSICAL_DAMAGE_COVERAGE();
                            product.PHYSICAL_DAMAGE.DETAIL_CODE = item.DETAIL_CODE;
                            product.PHYSICAL_DAMAGE.PRODUCT_CODE = item.PRODUCT_CODE;
                            product.PHYSICAL_DAMAGE.PACK_CODE = item.PACK_CODE;
                            product.PHYSICAL_DAMAGE.INSUR_DEDUCTIBLE = vehicleDt.PHYSICAL_INSUR_DEDUCTIBLE;
                            product.PHYSICAL_DAMAGE.EFF = item.EFFECTIVE_DATE;
                            product.PHYSICAL_DAMAGE.TIME_EFF = item.EFFECTIVE_TIME;
                            product.PHYSICAL_DAMAGE.EXP = item.EXPIRATION_DATE;
                            product.PHYSICAL_DAMAGE.TIME_EXP = item.EXPIRATION_TIME;
                            product.PHYSICAL_DAMAGE.IS_VEHICLE_LOSS = vehicleDt.IS_VEHICLE_LOSS;
                            product.PHYSICAL_DAMAGE.VEHICLE_LOSS_NUM = vehicleDt.VEHICLE_LOSS_NUM;
                            product.PHYSICAL_DAMAGE.IS_DISCOUNT = vehicleDt.PHYSICAL_IS_DISCOUNT;
                            product.PHYSICAL_DAMAGE.DISCOUNT = vehicleDt.PHYSICAL_DISCOUNT;
                            product.PHYSICAL_DAMAGE.DISCOUNT_UNIT = vehicleDt.PHYSICAL_DISCOUNT_UNIT;
                            product.PHYSICAL_DAMAGE.REASON_DISCOUNT = vehicleDt.PHYSICAL_REASON_DISCOUNT;
                            product.PHYSICAL_DAMAGE.IS_SEND_HD = vehicleDt.IS_SEND_HD;
                            product.PHYSICAL_DAMAGE.SURVEYOR_TYPE = vehicleDt.SURVEYOR_TYPE;

                            product.PHYSICAL_DAMAGE.VEHICLE_LOSS_AMOUNT = vehicleDt.VEHICLE_LOSS_AMOUNT;
                            product.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT = vehicleDt.INSUR_TOTAL_AMOUNT;

                            DetailCalendar DtCalen = lstDtCalen.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList().FirstOrDefault();

                            product.PHYSICAL_DAMAGE.CAL_CODE = DtCalen.CAL_CODE;

                            product.PHYSICAL_DAMAGE.SURVEYOR_TYPE = DtCalen.SURVEYOR_TYPE;
                            product.PHYSICAL_DAMAGE.CALENDAR_EFF = DtCalen.EFF;
                            product.PHYSICAL_DAMAGE.CALENDAR_EXP = DtCalen.EXP;
                            product.PHYSICAL_DAMAGE.CALENDAR_TIME_F = DtCalen.TIME_F;
                            product.PHYSICAL_DAMAGE.CALENDAR_TIME_T = DtCalen.TIME_T;
                            product.PHYSICAL_DAMAGE.CALENDAR_CONTACT = DtCalen.CONTACT;
                            product.PHYSICAL_DAMAGE.CALENDAR_PHONE = DtCalen.PHONE;
                            product.PHYSICAL_DAMAGE.CALENDAR_PROV = DtCalen.PROV;
                            product.PHYSICAL_DAMAGE.CALENDAR_DIST = DtCalen.DIST;
                            product.PHYSICAL_DAMAGE.CALENDAR_WARDS = DtCalen.WARDS;
                            product.PHYSICAL_DAMAGE.CALENDAR_ADDRESS = DtCalen.ADDRESS;
                            product.PHYSICAL_DAMAGE.CALENDAR_ADDRESS_FORM = DtCalen.ADDRESS_FORM;

                            product.PHYSICAL_DAMAGE.CALENDAR_STATUS = DtCalen.CALENDAR_STATUS;
                            //public List<PHYSICAL_DAMAGE_ADDITIONAL> ADDITIONAL { get; set; }

                            VehicleInsurFees vcxInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).ToList().FirstOrDefault();
                            if (vcxInfo != null)
                            {
                                product.PHYSICAL_DAMAGE.FEES_DATA = vcxInfo.FEES_DATA;
                                product.PHYSICAL_DAMAGE.FEES = vcxInfo.FEES;
                                product.PHYSICAL_DAMAGE.AMOUNT = vcxInfo.AMOUNT;
                                product.PHYSICAL_DAMAGE.VAT = vcxInfo.VAT;
                                product.PHYSICAL_DAMAGE.TOTAL_DISCOUNT = vcxInfo.TOTAL_DISCOUNT;
                                product.PHYSICAL_DAMAGE.TOTAL_AMOUNT = vcxInfo.TOTAL_AMOUNT;
                                product.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = vcxInfo.TOTAL_ADD;
                            }

                            VehicleInsurFees vcxGocInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).ToList().FirstOrDefault();
                            if (vcxGocInfo != null)
                            {
                                product.PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = vcxGocInfo.FEES_DATA;
                                product.PHYSICAL_DAMAGE.PHYSICAL_FEES = vcxGocInfo.FEES;
                                product.PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = vcxGocInfo.AMOUNT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_VAT = vcxGocInfo.VAT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = vcxGocInfo.TOTAL_DISCOUNT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = vcxGocInfo.TOTAL_AMOUNT;
                            }

                            VehicleInsurFees vcxBsInfo = lstFeesVeh.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE) && x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).ToList().FirstOrDefault();
                            if (vcxBsInfo != null)
                            {
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = vcxBsInfo.FEES_DATA;
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = vcxBsInfo.FEES;
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = vcxBsInfo.AMOUNT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = vcxBsInfo.VAT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = vcxBsInfo.TOTAL_DISCOUNT;
                                product.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = vcxBsInfo.TOTAL_AMOUNT;
                            }

                            //product.PHYSICAL_DAMAGE.FEES = vehicleDt.PHYSICAL_FEES;
                            //product.PHYSICAL_DAMAGE.AMOUNT = vehicleDt.PHYSICAL_AMOUNT;
                            //product.PHYSICAL_DAMAGE.VAT = vehicleDt.PHYSICAL_VAT;
                            //product.PHYSICAL_DAMAGE.TOTAL_DISCOUNT = vehicleDt.PHYSICAL_TOTAL_DISCOUNT;
                            //product.PHYSICAL_DAMAGE.TOTAL_AMOUNT = vehicleDt.PHYSICAL_TOTAL_AMOUNT;
                            //product.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = vehicleDt.PHYSICAL_TOTAL_ADDITIONAL;

                            product.PHYSICAL_DAMAGE.STATUS = item.STATUS;
                            product.PHYSICAL_DAMAGE.STATUS_NAME = item.STATUS_NAME;
                            product.PHYSICAL_DAMAGE.FILE = new List<DetailFile>();
                            if (files != null && files.Any())
                            {
                                List<DetailFile> file_detail = files.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                product.PHYSICAL_DAMAGE.FILE = file_detail;
                            }

                            // thông tin thụ hưởng
                            List<Beneficiary> dtBennefi = lstBennefi.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList();
                            if (dtBennefi == null || !dtBennefi.Any())
                                dtBennefi = new List<Beneficiary>();
                            product.PHYSICAL_DAMAGE.BENEFICIARY = dtBennefi;

                            // thông tin bổ sung
                            List<PHYSICAL_DAMAGE_ADDITIONAL> dtAddi = lstAddi.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList();
                            if (dtAddi == null || !dtAddi.Any())
                                dtAddi = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                            product.PHYSICAL_DAMAGE.ADDITIONAL = dtAddi;

                            // thông tin file giám định
                            product.PHYSICAL_DAMAGE.SURVEYOR_FILES = new List<DetailFile>();
                            if (filesSurveyor != null && filesSurveyor.Any())
                            {
                                List<DetailFile> fileSur = filesSurveyor.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                product.PHYSICAL_DAMAGE.SURVEYOR_FILES = fileSur;
                            }
                        }
                        #endregion
                    }
                    vehicle.VEHICLE_INSUR.Add(product);
                }
                return vehicle;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private object GetDetailHouse(BaseResponse res_dt)
        {
            try
            {
                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(res_dt, 0).FirstOrDefault();
                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(res_dt, 1);
                List<HouseInsurExt> lstExt = goBusinessCommon.Instance.GetData<HouseInsurExt>(res_dt, 2);
                List<HouseInfo> lstHouseInfo = goBusinessCommon.Instance.GetData<HouseInfo>(res_dt, 3);

                // file GCN + file thông tin liên quan hợp đồng
                List<DetailFile> files = goBusinessCommon.Instance.GetData<DetailFile>(res_dt, 4);

                // thông tin thụ hưởng
                List<Beneficiary> lstBennefi = goBusinessCommon.Instance.GetData<Beneficiary>(res_dt, 5);

                // thông tin chi tiết phí
                List<InsurFeesDetailGet> lstFees = goBusinessCommon.Instance.GetData<InsurFeesDetailGet>(res_dt, 6);

                BillInfo billInfo = goBusinessCommon.Instance.GetData<BillInfo>(res_dt, 7).FirstOrDefault();

                //thông tin bổ sung của hợp đồng
                ContractExtend contractExtend = goBusinessCommon.Instance.GetData<ContractExtend>(res_dt, 8).FirstOrDefault();

                // Start 2022-06-23 By ThienTVB
                HouseInfoLo houseLo = goBusinessCommon.Instance.GetData<HouseInfoLo>(res_dt, 9).FirstOrDefault();
                // End 2022-06-23 By ThienTVB

                MaskInsur maskInsur = new MaskInsur();
                maskInsur.HOUSE_INSUR = new List<HouseInsur>();
                maskInsur.BILL_INFO = billInfo;
                if (insurContract != null)
                {
                    maskInsur.CATEGORY = insurContract.CATEGORY;
                    maskInsur.PRODUCT_CODE = insurContract.PRODUCT_CODE;
                    maskInsur.PRODUCT_MODE = insurContract.CONTRACT_MODE;
                    maskInsur.CHANNEL = insurContract.CHANNEL;
                    maskInsur.STATUS = insurContract.STATUS;
                    maskInsur.STATUS_NAME = insurContract.STATUS_NAME;
                    ORD_SELLER seller = new ORD_SELLER();
                    seller.SELLER_CODE = insurContract.SELLER_CODE;
                    seller.ORG_CODE = insurContract.ORG_SELLER;
                    seller.ENVIROMENT = insurContract.ENVIROMENT;
                    seller.TRAFFIC_LINK = insurContract.LINK_TRAFIC;
                    seller.ORG_TRAFFIC = insurContract.ORG_TRAFIC;

                    if (contractExtend != null)
                    {
                        seller.SELLER_NAME = contractExtend.SELLER_NAME;
                        seller.SELLER_EMAIL = contractExtend.SELLER_EMAIL;
                        seller.SELLER_PHONE = contractExtend.SELLER_PHONE;
                        seller.SELLER_GENDER = contractExtend.SELLER_GENDER;
                    }

                    maskInsur.SELLER = seller;
                    //maskInsur.FILE = files;
                    ORD_BUYER buyer = new ORD_BUYER();
                    buyer = JsonConvert.DeserializeObject<ORD_BUYER>(JsonConvert.SerializeObject(insurContract));
                    //buyer.TYPE_VAT = insurContract.TYPE_VAT;
                    maskInsur.BUYER = buyer;
                }

                if (insurDetails != null)
                {
                    foreach (var item in insurDetails)
                    {
                        HouseInsur houseInsur = new HouseInsur();
                        houseInsur = JsonConvert.DeserializeObject<HouseInsur>(JsonConvert.SerializeObject(item));

                        HouseInfo houExists = lstHouseInfo.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).FirstOrDefault();
                        houseInsur.HOUSE_INFO = houExists;

                        HouseProduct houseProduct = new HouseProduct();
                        houseProduct.PRODUCT_CODE = item.PRODUCT_CODE;
                        houseProduct.PACK_CODE = item.PACK_CODE;
                        houseProduct.EFF = item.EFFECTIVE_DATE;
                        houseProduct.EXP = item.EXPIRATION_DATE;
                        houseProduct.FEES = item.PACK_FEES;
                        houseProduct.AMOUNT = item.AMOUNT;
                        houseProduct.VAT = item.VAT;
                        houseProduct.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT;
                        houseProduct.TOTAL_AMOUNT = item.TOTAL_AMOUNT;
                        houseProduct.TOTAL_ADDITIONAL = item.ADDTIONAL_FEES;
                        // Start 2022-06-16 By ThienTVB
                        if (houExists != null)
                        {
                            houseProduct.HOUSE_VALUE_REF = houExists.HOUSE_VALUE_REF;
                            houseProduct.ASSET_VALUE_REF = houExists.ASSET_VALUE_REF;
                        }
                        else
                        {
                            if (houseLo != null)
                            {
                                houseProduct.HOUSE_VALUE_REF = houseLo.HOUSE_VALUE;
                                houseProduct.ASSET_VALUE_REF = 0;
                                HouseInfo hI = new HouseInfo();
                                // House_Type from Bank to HDI
                                if (houseLo.HOUSE_TYPE == "VL" || houseLo.HOUSE_TYPE == "BT1" || houseLo.HOUSE_TYPE == "BT2")
                                {
                                    hI.HOUSE_TYPE = "VL";
                                }
                                else if (houseLo.HOUSE_TYPE == "AD" || houseLo.HOUSE_TYPE == "NLK1" || houseLo.HOUSE_TYPE == "NLK2")
                                {
                                    hI.HOUSE_TYPE = "AD";
                                }
                                else if (houseLo.HOUSE_TYPE == "AP" || houseLo.HOUSE_TYPE == "CC")
                                {
                                    hI.HOUSE_TYPE = "AP";
                                }
                                else
                                {
                                    hI.HOUSE_TYPE = "";
                                }
                                hI.HOUSE_VALUE = houseLo.HOUSE_VALUE.ToString();
                                hI.ADDRESS = houseLo.HOUSE_ADD;
                                //hI.ADDRESS_FORM = "";
                                houseInsur.HOUSE_INFO = hI;
                            }
                        }
                        // End 2022-06-16 By ThienTVB

                        List<InsurFeesDetailGet> lstExists = lstFees.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).ToList();
                        if (lstExists != null)
                        {
                            InsurFeesDetailGet feesAll = lstExists.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.NHAF1.ToString())).FirstOrDefault();
                            if (feesAll != null)
                                houseProduct.FEES_DATA = feesAll.FEES_DATA;

                            InsurFeesDetailGet feesHouse = lstExists.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.NHA_HVF.ToString())).FirstOrDefault();
                            if (feesHouse != null)
                            {
                                houseProduct.HOUSE_VAL_FEES_DATA = feesHouse.FEES_DATA;
                                houseProduct.HOUSE_VAL_FEES = feesHouse.FEES;
                                houseProduct.HOUSE_VAL_AMOUNT = feesHouse.AMOUNT;
                                houseProduct.HOUSE_VAL_VAT = feesHouse.VAT;
                                houseProduct.HOUSE_VAL_TOTAL_DISCOUNT = feesHouse.TOTAL_DISCOUNT;
                                houseProduct.HOUSE_VAL_TOTAL_AMOUNT = feesHouse.TOTAL_AMOUNT;
                            }

                            InsurFeesDetailGet feesHouseAssets = lstExists.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.NHA_FVF.ToString())).FirstOrDefault();
                            if (feesHouseAssets != null)
                            {
                                houseProduct.HOUSE_ASSETS_FEES_DATA = feesHouseAssets.FEES_DATA;
                                houseProduct.HOUSE_ASSETS_FEES = feesHouseAssets.FEES;
                                houseProduct.HOUSE_ASSETS_AMOUNT = feesHouseAssets.AMOUNT;
                                houseProduct.HOUSE_ASSETS_VAT = feesHouseAssets.VAT;
                                houseProduct.HOUSE_ASSETS_TOTAL_DISCOUNT = feesHouseAssets.TOTAL_DISCOUNT;
                                houseProduct.HOUSE_ASSETS_TOTAL_AMOUNT = feesHouseAssets.TOTAL_AMOUNT;
                            }
                        }

                        HouseInsurExt extInfo = lstExt.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).FirstOrDefault();
                        if (extInfo != null)
                        {
                            houseProduct.HOUSE_USE_CODE = extInfo.HOUSE_USE_CODE;
                            houseProduct.INSUR_TOTAL_AMOUNT = extInfo.INSUR_TOTAL_AMOUNT;
                            houseProduct.INSUR_HOUSE_VALUE = extInfo.INSUR_HOUSE_VALUE;
                            houseProduct.INSUR_HOUSE_ASSETS = extInfo.INSUR_HOUSE_ASSETS;
                            houseInsur.LAND_USE_RIGHTS = extInfo.LAND_USE_RIGHTS;
                            houseProduct.INSUR_DEDUCTIBLE = extInfo.INSUR_DEDUCTIBLE;
                        }

                        List<Beneficiary> lstBennefiExists = lstBennefi.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).ToList();
                        List<DetailFile> filesExists = files.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).ToList();

                        //houseInsur.BILL_INFO = billInfo;
                        houseInsur.BENEFICIARY = lstBennefiExists;
                        houseInsur.HOUSE_PRODUCT = houseProduct;
                        houseInsur.FILE = filesExists;
                        maskInsur.HOUSE_INSUR.Add(houseInsur);
                    }
                }

                return maskInsur;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        private object GetDetailHealth(BaseResponse res_dt)
        {
            try
            {
                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(res_dt, 0).FirstOrDefault();
                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(res_dt, 1);
                List<HouseInsurExt> lstExt = goBusinessCommon.Instance.GetData<HouseInsurExt>(res_dt, 2);

                // thông tin thụ hưởng
                List<Beneficiary> lstBennefi = goBusinessCommon.Instance.GetData<Beneficiary>(res_dt, 3);
                // file GCN + file thông tin liên quan hợp đồng
                List<DetailFile> files = goBusinessCommon.Instance.GetData<DetailFile>(res_dt, 4);
                // file GCN + file thông tin liên quan hợp đồng chi tiết
                List<DetailFile> filesDetail = goBusinessCommon.Instance.GetData<DetailFile>(res_dt, 5);

                List<Additional> lstAdd = goBusinessCommon.Instance.GetData<Additional>(res_dt, 6);

                BillInfo billInfo = goBusinessCommon.Instance.GetData<BillInfo>(res_dt, 7).FirstOrDefault();

                //thông tin bổ sung của hợp đồng
                ContractExtend contractExtend = goBusinessCommon.Instance.GetData<ContractExtend>(res_dt, 8).FirstOrDefault();

                MaskInsur maskInsur = new MaskInsur();
                maskInsur.HEALTH_INSUR = new List<InsuredPerson>();
                maskInsur.BILL_INFO = billInfo;
                maskInsur.FILE = files;
                if (insurContract != null)
                {
                    maskInsur.CATEGORY = insurContract.CATEGORY;
                    maskInsur.PRODUCT_CODE = insurContract.PRODUCT_CODE;
                    maskInsur.PRODUCT_MODE = insurContract.CONTRACT_MODE;
                    maskInsur.CHANNEL = insurContract.CHANEL;
                    maskInsur.AMOUNT = insurContract.AMOUNT;
                    maskInsur.VAT = insurContract.VAT;
                    maskInsur.TOTAL_AMOUNT = insurContract.TOTAL_AMOUNT;
                    maskInsur.TOTAL_DISCOUNT = insurContract.TOTAL_DISCOUNT;
                    maskInsur.STATUS = insurContract.STATUS;
                    maskInsur.STATUS_NAME = insurContract.STATUS_NAME;

                    ORD_SELLER seller = new ORD_SELLER();
                    seller.SELLER_CODE = insurContract.SELLER_CODE;
                    seller.ORG_CODE = insurContract.ORG_SELLER;
                    seller.ENVIROMENT = insurContract.ENVIROMENT;
                    seller.TRAFFIC_LINK = insurContract.LINK_TRAFIC;
                    seller.ORG_TRAFFIC = insurContract.ORG_TRAFIC;
                    if (contractExtend != null)
                    {
                        seller.SELLER_NAME = contractExtend.SELLER_NAME;
                        seller.SELLER_EMAIL = contractExtend.SELLER_EMAIL;
                        seller.SELLER_PHONE = contractExtend.SELLER_PHONE;
                        seller.SELLER_GENDER = contractExtend.SELLER_GENDER;
                    }
                    maskInsur.SELLER = seller;

                    ORD_BUYER buyer = new ORD_BUYER();
                    buyer = JsonConvert.DeserializeObject<ORD_BUYER>(JsonConvert.SerializeObject(insurContract));
                    //buyer.TYPE_VAT = insurContract.TYPE_VAT;
                    maskInsur.BUYER = buyer;
                }
                if (insurDetails != null)
                {
                    foreach (var item in insurDetails)
                    {
                        InsuredPerson insured = new InsuredPerson();
                        insured = JsonConvert.DeserializeObject<InsuredPerson>(JsonConvert.SerializeObject(item));
                        insured.FEES = item.PACK_FEES;
                        insured.TOTAL_ADD = item.ADDTIONAL_FEES;

                        List<Beneficiary> lstBennefiExists = lstBennefi.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).ToList();
                        List<DetailFile> filesExists = filesDetail.Where(x => x.DETAIL_CODE == item.DETAIL_CODE).ToList();

                        //houseInsur.BILL_INFO = billInfo;
                        insured.BENEFICIARY = lstBennefiExists;
                        insured.FILE = filesExists;
                        maskInsur.HEALTH_INSUR.Add(insured);
                    }
                }

                return maskInsur;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BaseResponse GetQrRecord(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                REQ_QR reqQr = new REQ_QR();
                if (request.Data != null)
                    reqQr = JsonConvert.DeserializeObject<REQ_QR>(JsonConvert.SerializeObject(request.Data));

                var param = new
                {
                    CHANNEL = reqQr.CHANNEL,
                    USERNAME = reqQr.USERNAME,
                    LANG = reqQr.LANG,
                    CATEGORY = reqQr.CATEGORY,
                    PRODUCT_CODE = reqQr.PRODUCT_CODE,
                    TYPE_RECORD = reqQr.TYPE_RECORD
                };
                BaseRequest reqQrInfo = request;
                reqQrInfo.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                // lấy thông json + url gen qr
                BaseResponse res = goBusinessAction.Instance.GetBaseResponse(reqQrInfo);
                if (res.Success)
                {
                    PRODUCT_RECORD productRecord = goBusinessCommon.Instance.GetData<PRODUCT_RECORD>(res, 0).FirstOrDefault();
                    if (productRecord == null)
                        throw new Exception("null productRecord");
                    RequestNodeJs req = goBusinessCommon.Instance.GetBaseRequestHDIUpload(config.env_code);
                    req.Action.ActionCode = goConstantsProcedure.STREAM_UPLOAD.ToString();
                    req.Data = JsonConvert.DeserializeObject(productRecord.INPUT_JSON);

                    string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "api/media-transshipment/initialization";

                    List<goServiceConstants.KeyVal> lsKey = new List<goServiceConstants.KeyVal>();
                    var strJson = JsonConvert.SerializeObject(req);
                    HttpResponseMessage rp = new goServiceInvoke().Invoke(urrl, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw, goServiceConstants.TypeRaw.json, lsKey, lsKey, strJson).Result;
                    if (rp.StatusCode == HttpStatusCode.OK)
                    {
                        string message = rp.Content.ReadAsStringAsync().Result;
                        string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                        var details = JObject.Parse(parsedString);
                        var transId = details["transshipmentId"].ToString();

                        string urlGenQR = productRecord.URL + transId;
                        string base64QR = goBussinessPDF.Instance.getQrCode(urlGenQR, "Q", 8, "");
                        if (string.IsNullOrEmpty(base64QR))
                            throw new Exception();

                        object data = new
                        {
                            TRANS_ID = transId,
                            BASE64_QR = base64QR,
                            URL_QR = urlGenQR
                        };

                        response.Success = true;
                        response.Data = data;

                        // cache data obj input
                        if (reqQr.OBJ_DATA != null && !string.IsNullOrEmpty(transId))
                        {
                            string key = goConstantsRedis.PrefixDataRecord + transId;
                            goBusinessCache.Instance.Add<object>(key, reqQr.OBJ_DATA, goUtility.Instance.ConvertToInt32(goBusinessCommon.Instance.GetConfigDB(goConstants.timeRecordCache), 0));
                        }
                    }
                    else
                    {
                        string message = rp.Content.ReadAsStringAsync().Result;
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, message, Logger.ConcatName(nameClass, nameMethod));
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3008), goConstantsError.Instance.ERROR_3008);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse GetDataRecordCache(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                object[] goParam = null;
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                    goParam = goUtility.Instance.CastJobjectArray((JObject)request.Data);

                string key = goConstantsRedis.PrefixDataRecord + goParam[0];
                response = goBusinessCommon.Instance.getResultApi(true, goBusinessCache.Instance.Get<object>(key), config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse CommitRecord(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                RequestNodeJs req = goBusinessCommon.Instance.GetBaseRequestHDIUpload(config.env_code);
                req.Action.ActionCode = goConstantsProcedure.STREAM_COMMIT.ToString();
                req.Data = request.Data;

                string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "api/media-transshipment/commit";
                List<goServiceConstants.KeyVal> lsKey = new List<goServiceConstants.KeyVal>();
                var strJson = JsonConvert.SerializeObject(req);
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json cmit: " + strJson, Logger.ConcatName(nameClass, nameMethod));
                HttpResponseMessage rp = new goServiceInvoke().Invoke(urrl, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw, goServiceConstants.TypeRaw.json, lsKey, lsKey, strJson).Result;
                if (rp.StatusCode == HttpStatusCode.OK)
                {
                    response = goBusinessCommon.Instance.getResultApi(true, "", config);
                }
                else
                    throw new Exception("upload file hoàng lỗi");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public void InsurMapping(ref string strRaw, ref InsurCallback itemInsur, List<InsurDetailCallback> LstInsurDetail, List<AdditionalCallback> lstAdditional, List<BeneficiaryCallback> lstBenefi,
                List<object> lstObj1, List<object> lstObj2, List<object> lstObj3)
        {
            try
            {
                string contractCode = itemInsur.CONTRACT_CODE;
                // tạo và gán giá trị callback
                List<InsurDetailCallback> lstDetailExists = LstInsurDetail.Where(i => i.CONTRACT_CODE.Equals(contractCode)).ToList();
                if (lstDetailExists != null)
                {
                    // tạo và gán giá trị callback
                    // gán danh sách đkbs
                    foreach (var itemDetail in lstDetailExists)
                    {
                        itemDetail.BUSINESS = new BusinessCallback();
                        List<AdditionalCallback> lstExistsAdd = lstAdditional.Where(i => i.DETAIL_CODE.Equals(itemDetail.DETAIL_CODE)).ToList();
                        itemDetail.ADDITIONALS = lstExistsAdd;

                        List<BeneficiaryCallback> lstExistsBen = lstBenefi.Where(i => i.DETAIL_CODE.Equals(itemDetail.DETAIL_CODE)).ToList();
                        itemDetail.BENEFICIARY = lstExistsBen;

                        // gán nghiệp vụ

                        switch (itemDetail.CATEGORY)
                        {
                            case var s when itemDetail.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                                break;
                            case var s when itemDetail.CATEGORY.Equals(goConstants.CATEGORY_SK) || itemDetail.CATEGORY.Equals(goConstants.CATEGORY_TAINAN):
                                break;
                            case var s when itemDetail.CATEGORY.Equals(goConstants.CATEGORY_XE):
                                // tu data dau vao convert thanh data cua xe
                                List<VehicleInfo> lstVehicleInfo = JsonConvert.DeserializeObject<List<VehicleInfo>>(JsonConvert.SerializeObject(lstObj1));
                                List<VehicleInsurFees> lstVehicleInsurFees = JsonConvert.DeserializeObject<List<VehicleInsurFees>>(JsonConvert.SerializeObject(lstObj2));
                                List<VehicleInsurDetail> lstVehicleExt = JsonConvert.DeserializeObject<List<VehicleInsurDetail>>(JsonConvert.SerializeObject(lstObj3));

                                // lay ra data cua detail
                                VehicleCallback vehicleCallback = new VehicleCallback();
                                VehicleInfo vehicleInfo = lstVehicleInfo.Where(i => i.VEH_CODE.Equals(itemDetail.REF_ID)).FirstOrDefault();
                                List<VehicleInsurFees> lstVehFees = lstVehicleInsurFees.Where(i => i.DETAIL_CODE.Equals(itemDetail.DETAIL_CODE)).ToList();
                                VehicleInsurDetail vehicleDt = lstVehicleExt.Where(x => x.DETAIL_CODE.Equals(itemDetail.DETAIL_CODE)).ToList().FirstOrDefault();

                                // thông tin ext
                                vehicleCallback = JsonConvert.DeserializeObject<VehicleCallback>(JsonConvert.SerializeObject(vehicleInfo));
                                vehicleCallback.IS_VOLUNTARY = vehicleDt.IS_VOLUNTARY;
                                vehicleCallback.IS_DRIVER = vehicleDt.IS_DRIVER;
                                vehicleCallback.IS_CARGO = vehicleDt.IS_CARGO;
                                vehicleCallback.IS_COMPULSORY = vehicleDt.IS_COMPULSORY;
                                vehicleCallback.IS_VOLUNTARY_ALL = vehicleDt.IS_VOLUNTARY_ALL;
                                vehicleCallback.IS_PHYSICAL = vehicleDt.IS_PHYSICAL;

                                #region TNDS BB
                                if (vehicleDt.IS_COMPULSORY == "1") // lam sau
                                {
                                    // thông tin BB
                                    //vehicleCallback.COMPULSORY_CIVIL = new COMPULSORY_CIVIL_LIABILITY();
                                    //vehicleCallback.COMPULSORY_CIVIL.PRODUCT_CODE = itemDetail.PRODUCT_CODE;
                                    //vehicleCallback.COMPULSORY_CIVIL.PACK_CODE = itemDetail.PACK_CODE;
                                    //vehicleCallback.COMPULSORY_CIVIL.DETAIL_CODE = vehicleDt.DETAIL_CODE;
                                    //vehicleCallback.COMPULSORY_CIVIL.IS_SERIAL_NUM = vehicleDt.IS_SERIAL_NUM;
                                    //vehicleCallback.COMPULSORY_CIVIL.SERIAL_NUM = vehicleDt.SERIAL_NUM;
                                    //vehicleCallback.COMPULSORY_CIVIL.EFF = itemDetail.EFFECTIVE_DATE;
                                    //vehicleCallback.COMPULSORY_CIVIL.TIME_EFF = itemDetail.EFFECTIVE_TIME;
                                    //vehicleCallback.COMPULSORY_CIVIL.EXP = itemDetail.EXPIRATION_DATE;
                                    //vehicleCallback.COMPULSORY_CIVIL.TIME_EXP = itemDetail.EXPIRATION_TIME;
                                    //vehicleCallback.COMPULSORY_CIVIL.FEES = itemDetail.PACK_FEES - vehicleDt.FEES_VOLUNTARY_CIVIL;
                                    //vehicleCallback.COMPULSORY_CIVIL.AMOUNT = item.AMOUNT - vehicleDt.AMOUNT_VOLUNTARY_CIVIL;
                                    //vehicleCallback.COMPULSORY_CIVIL.VAT = item.VAT - vehicleDt.VAT_VOLUNTARY_CIVIL;
                                    //vehicleCallback.COMPULSORY_CIVIL.TOTAL_DISCOUNT = item.TOTAL_DISCOUNT - vehicleDt.DISCOUNT_VOLUNTARY_CIVIL;
                                    //vehicleCallback.COMPULSORY_CIVIL.TOTAL_AMOUNT = item.TOTAL_AMOUNT - vehicleDt.TOTAL_VOLUNTARY_CIVIL;
                                    //vehicleCallback.COMPULSORY_CIVIL.STATUS = item.STATUS;
                                    //vehicleCallback.COMPULSORY_CIVIL.STATUS_NAME = item.STATUS_NAME;
                                    //vehicleCallback.COMPULSORY_CIVIL.FILE = new List<DetailFile>();
                                    //if (files != null && files.Any())
                                    //{
                                    //    List<DetailFile> file_detail = files.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                    //    product.COMPULSORY_CIVIL.FILE = file_detail;
                                    //}
                                }
                                #endregion

                                #region TNDS TN
                                if (vehicleDt.IS_VOLUNTARY_ALL == "1")
                                {
                                    // Thông tin TN
                                    vehicleCallback.VOLUNTARY_CIVIL = new VOLUNTARY_CIVIL_LIABILITY();
                                    vehicleCallback.VOLUNTARY_CIVIL.PRODUCT_CODE = vehicleDt.PRODUCT_VOLUNTARY_CIVIL;
                                    vehicleCallback.VOLUNTARY_CIVIL.PACK_CODE = vehicleDt.PACK_VOLUNTARY_CIVIL;
                                    vehicleCallback.VOLUNTARY_CIVIL.INSUR_3RD = vehicleDt.INSUR_3RD;
                                    vehicleCallback.VOLUNTARY_CIVIL.INSUR_PASSENGER = vehicleDt.INSUR_PASSENGER;
                                    vehicleCallback.VOLUNTARY_CIVIL.INSUR_3RD_ASSETS = vehicleDt.INSUR_3RD_ASSETS;
                                    vehicleCallback.VOLUNTARY_CIVIL.TOTAL_LIABILITY = vehicleDt.TOTAL_LIABILITY;
                                    vehicleCallback.VOLUNTARY_CIVIL.DRIVER_NUM = vehicleDt.DRIVER_NUM;
                                    vehicleCallback.VOLUNTARY_CIVIL.DRIVER_INSUR = vehicleDt.DRIVER_INSUR;
                                    vehicleCallback.VOLUNTARY_CIVIL.WEIGHT_CARGO = vehicleDt.WEIGHT_CARGO;
                                    vehicleCallback.VOLUNTARY_CIVIL.CARGO_INSUR = vehicleDt.CARGO_INSUR;

                                    VehicleInsurFees tnInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).ToList().FirstOrDefault();
                                    if (tnInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA = tnInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES = tnInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT = tnInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT = tnInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT = tnInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_AMOUNT = tnInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees vqmInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).ToList().FirstOrDefault();
                                    if (vqmInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_VOLUNTARY_3RD = vqmInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD = vqmInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD = vqmInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD = vqmInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD = vqmInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD = vqmInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees t3rdInsurInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).ToList().FirstOrDefault();
                                    if (t3rdInsurInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_3RD_INSUR = t3rdInsurInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_3RD_INSUR = t3rdInsurInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR = t3rdInsurInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_3RD_INSUR = t3rdInsurInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR = t3rdInsurInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_3RD_INSUR = t3rdInsurInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees t3rdAssetsInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).ToList().FirstOrDefault();
                                    if (t3rdAssetsInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_3RD_ASSETS = t3rdAssetsInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_3RD_ASSETS = t3rdAssetsInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS = t3rdAssetsInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_3RD_ASSETS = t3rdAssetsInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS = t3rdAssetsInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS = t3rdAssetsInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees t3rdPassengerInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).ToList().FirstOrDefault();
                                    if (t3rdPassengerInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_PASSENGER = t3rdPassengerInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_PASSENGER = t3rdPassengerInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_PASSENGER = t3rdPassengerInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_PASSENGER = t3rdPassengerInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER = t3rdPassengerInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_PASSENGER = t3rdPassengerInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees driverInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).ToList().FirstOrDefault();
                                    if (driverInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_DRIVER = driverInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DRIVER = driverInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_DRIVER = driverInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_DRIVER = driverInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER = driverInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DRIVER = driverInfo.TOTAL_AMOUNT;
                                    }
                                    VehicleInsurFees cargoInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).ToList().FirstOrDefault();
                                    if (cargoInfo != null)
                                    {
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_DATA_CARGO = cargoInfo.FEES_DATA;
                                        vehicleCallback.VOLUNTARY_CIVIL.FEES_CARGO = cargoInfo.FEES;
                                        vehicleCallback.VOLUNTARY_CIVIL.AMOUNT_CARGO = cargoInfo.AMOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.VAT_CARGO = cargoInfo.VAT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO = cargoInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.VOLUNTARY_CIVIL.TOTAL_CARGO = cargoInfo.TOTAL_AMOUNT;
                                    }
                                }
                                #endregion

                                #region VCX
                                if (vehicleDt.IS_PHYSICAL == "1")
                                {
                                    vehicleCallback.PHYSICAL_DAMAGE = new PHYSICAL_DAMAGE_COVERAGE();
                                    vehicleCallback.PHYSICAL_DAMAGE.DETAIL_CODE = itemDetail.DETAIL_CODE;
                                    vehicleCallback.PHYSICAL_DAMAGE.PRODUCT_CODE = itemDetail.PRODUCT_CODE;
                                    vehicleCallback.PHYSICAL_DAMAGE.PACK_CODE = itemDetail.PACK_CODE;
                                    vehicleCallback.PHYSICAL_DAMAGE.INSUR_DEDUCTIBLE = vehicleDt.PHYSICAL_INSUR_DEDUCTIBLE;
                                    vehicleCallback.PHYSICAL_DAMAGE.EFF = itemDetail.EFF;
                                    vehicleCallback.PHYSICAL_DAMAGE.EXP = itemDetail.EXP;
                                    vehicleCallback.PHYSICAL_DAMAGE.IS_VEHICLE_LOSS = vehicleDt.IS_VEHICLE_LOSS;
                                    vehicleCallback.PHYSICAL_DAMAGE.VEHICLE_LOSS_NUM = vehicleDt.VEHICLE_LOSS_NUM;
                                    vehicleCallback.PHYSICAL_DAMAGE.IS_DISCOUNT = vehicleDt.PHYSICAL_IS_DISCOUNT;
                                    vehicleCallback.PHYSICAL_DAMAGE.DISCOUNT = vehicleDt.PHYSICAL_DISCOUNT;
                                    vehicleCallback.PHYSICAL_DAMAGE.DISCOUNT_UNIT = vehicleDt.PHYSICAL_DISCOUNT_UNIT;
                                    vehicleCallback.PHYSICAL_DAMAGE.REASON_DISCOUNT = vehicleDt.PHYSICAL_REASON_DISCOUNT;
                                    vehicleCallback.PHYSICAL_DAMAGE.IS_SEND_HD = vehicleDt.IS_SEND_HD;
                                    vehicleCallback.PHYSICAL_DAMAGE.SURVEYOR_TYPE = vehicleDt.SURVEYOR_TYPE;

                                    vehicleCallback.PHYSICAL_DAMAGE.VEHICLE_LOSS_AMOUNT = vehicleDt.VEHICLE_LOSS_AMOUNT;
                                    vehicleCallback.PHYSICAL_DAMAGE.INSUR_TOTAL_AMOUNT = vehicleDt.INSUR_TOTAL_AMOUNT;

                                    //DetailCalendar DtCalen = lstDtCalen.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList().FirstOrDefault();

                                    //product.PHYSICAL_DAMAGE.CAL_CODE = DtCalen.CAL_CODE;

                                    //product.PHYSICAL_DAMAGE.SURVEYOR_TYPE = DtCalen.SURVEYOR_TYPE;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_EFF = DtCalen.EFF;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_EXP = DtCalen.EXP;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_TIME_F = DtCalen.TIME_F;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_TIME_T = DtCalen.TIME_T;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_CONTACT = DtCalen.CONTACT;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_PHONE = DtCalen.PHONE;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_PROV = DtCalen.PROV;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_DIST = DtCalen.DIST;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_WARDS = DtCalen.WARDS;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_ADDRESS = DtCalen.ADDRESS;
                                    //product.PHYSICAL_DAMAGE.CALENDAR_ADDRESS_FORM = DtCalen.ADDRESS_FORM;

                                    //product.PHYSICAL_DAMAGE.CALENDAR_STATUS = DtCalen.CALENDAR_STATUS;

                                    VehicleInsurFees vcxInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).ToList().FirstOrDefault();
                                    if (vcxInfo != null)
                                    {
                                        vehicleCallback.PHYSICAL_DAMAGE.FEES_DATA = vcxInfo.FEES_DATA;
                                        vehicleCallback.PHYSICAL_DAMAGE.FEES = vcxInfo.FEES;
                                        vehicleCallback.PHYSICAL_DAMAGE.AMOUNT = vcxInfo.AMOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.VAT = vcxInfo.VAT;
                                        vehicleCallback.PHYSICAL_DAMAGE.TOTAL_DISCOUNT = vcxInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.TOTAL_AMOUNT = vcxInfo.TOTAL_AMOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL = vcxInfo.TOTAL_ADD;
                                    }

                                    VehicleInsurFees vcxGocInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).ToList().FirstOrDefault();
                                    if (vcxGocInfo != null)
                                    {
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = vcxGocInfo.FEES_DATA;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_FEES = vcxGocInfo.FEES;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_AMOUNT = vcxGocInfo.AMOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_VAT = vcxGocInfo.VAT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT = vcxGocInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT = vcxGocInfo.TOTAL_AMOUNT;
                                    }

                                    VehicleInsurFees vcxBsInfo = lstVehFees.Where(x => x.TYPE_KEY.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).ToList().FirstOrDefault();
                                    if (vcxBsInfo != null)
                                    {
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = vcxBsInfo.FEES_DATA;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES = vcxBsInfo.FEES;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT = vcxBsInfo.AMOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT = vcxBsInfo.VAT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT = vcxBsInfo.TOTAL_DISCOUNT;
                                        vehicleCallback.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT = vcxBsInfo.TOTAL_AMOUNT;
                                    }

                                    vehicleCallback.PHYSICAL_DAMAGE.STATUS = itemDetail.STATUS;
                                    vehicleCallback.PHYSICAL_DAMAGE.STATUS_NAME = itemDetail.STATUS_NAME;
                                    vehicleCallback.PHYSICAL_DAMAGE.FILE = new List<DetailFile>();
                                    //if (files != null && files.Any())
                                    //{
                                    //    List<DetailFile> file_detail = files.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                    //    product.PHYSICAL_DAMAGE.FILE = file_detail;
                                    //}

                                    //// thông tin thụ hưởng
                                    //List<Beneficiary> dtBennefi = lstBennefi.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList();
                                    //if (dtBennefi == null || !dtBennefi.Any())
                                    //    dtBennefi = new List<Beneficiary>();
                                    //product.PHYSICAL_DAMAGE.BENEFICIARY = dtBennefi;

                                    //// thông tin bổ sung
                                    //List<PHYSICAL_DAMAGE_ADDITIONAL> dtAddi = lstAddi.Where(x => x.DETAIL_CODE.Equals(item.DETAIL_CODE)).ToList();
                                    //if (dtAddi == null || !dtAddi.Any())
                                    //    dtAddi = new List<PHYSICAL_DAMAGE_ADDITIONAL>();
                                    //product.PHYSICAL_DAMAGE.ADDITIONAL = dtAddi;

                                    // thông tin file giám định
                                    //product.PHYSICAL_DAMAGE.SURVEYOR_FILES = new List<DetailFile>();
                                    //if (filesSurveyor != null && filesSurveyor.Any())
                                    //{
                                    //    List<DetailFile> fileSur = filesSurveyor.Where(x => x.DETAIL_CODE.Equals(vehicleDt.DETAIL_CODE)).ToList().ToList();
                                    //    product.PHYSICAL_DAMAGE.SURVEYOR_FILES = fileSur;
                                    //}
                                }
                                #endregion


                                itemDetail.BUSINESS.VEHICLE = vehicleCallback;

                                break;
                        }

                    }
                    //gán
                    itemInsur.INSUR_DETAILS = lstDetailExists;
                }

                strRaw += itemInsur.CONTRACT_CODE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public InsurStatusCallback GetObjStatusCallback(OrgDataCBConfig org, ResponseConfig config)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            InsurStatusCallback statusCallback = new InsurStatusCallback();
            try
            {
                statusCallback.ACTION = org.ACTION;
                statusCallback.RESON = org.REASON;
                statusCallback.CONTRACT_CODE = org.CONTRACT_CODE;
                statusCallback.TOTAL_AMOUNT = org.TOTAL_AMOUNT;

                string raw = org.CONTRACT_CODE + org.ACTION + Convert.ToInt32(org.TOTAL_AMOUNT) + config.secret;
                statusCallback.CHECKSUM = goEncryptBase.Instance.Md5Encode(raw);
                return statusCallback;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
        }

        public BaseResponse InsurExists(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //InputCheckExists inputCheck = JsonConvert.DeserializeObject<InputCheckExists>(JsonConvert.SerializeObject(request.Data));
                //object param = null;
                //string category = inputCheck.CATEGORY;
                //switch (category)
                //{
                //    case var s when category.Equals(goConstants.CATEGORY_XE):
                //        param = new
                //        {
                //            CHANNEL = inputCheck.CHANNEL,
                //            USERNAME = inputCheck.USERNAME,
                //            ORG_CODE = inputCheck.ORG_CODE,
                //            CATEGORY = category,
                //            PRODUCT_CODE = inputCheck.PRODUCT_CODE,
                //            EFF = inputCheck.EFF,
                //            OBJ = JsonConvert.SerializeObject(inputCheck.VEHICLE_INFO)
                //        };
                //        break;
                //    case var s when category.Equals(goConstants.CATEGORY_NHATN):
                //        break;
                //    case var s when category.Equals(goConstants.CATEGORY_SK) || category.Equals(goConstants.CATEGORY_TAINAN):
                //        break;
                //}

                //if (param == null)
                //    throw new Exception("Không đúng nghiệp vụ rồi, input: " + category);

                //BaseRequest reqExists = request;
                //reqExists.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                BaseResponse res = goBusinessAction.Instance.GetBaseResponse(request);
                if (res.Success)
                {
                    JObject outData = goBusinessCommon.Instance.GetData<JObject>(res, 0).FirstOrDefault();
                    object objOut = null;
                    if (outData != null)
                    {
                        if (outData["IS_EXISTS"].ToString() == "1")
                        {
                            objOut = new
                            {
                                EXISTS = true
                            };
                        }
                    }

                    if (objOut == null)
                    {
                        objOut = new
                        {
                            EXISTS = false
                        };
                    }
                    response = goBusinessCommon.Instance.getResultApi(true, objOut, config);
                }
                else
                    throw new Exception("Có lỗi, res: " + JsonConvert.SerializeObject(res));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }

        public BaseResponse OrderValidList(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<InsurCheck> lstInsur = new List<InsurCheck>();
                if (request.Data != null)
                    lstInsur = JsonConvert.DeserializeObject<List<InsurCheck>>(JsonConvert.SerializeObject(request.Data));
                if (lstInsur == null || !lstInsur.Any())
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "ERROR_2009", "json truyền lên convert rỗng");

                foreach (var item in lstInsur)
                {
                    try
                    {
                        DateTime dob = goUtility.Instance.ConvertToDateTime(item.dob, DateTime.MinValue);
                        DateTime eff = goUtility.Instance.ConvertToDateTime(item.eff, DateTime.MinValue);

                        //goUtility.Instance.getAge(dob, eff, ref item.age, ref item.age_y, ref item.age_m, ref item.age_d);
                        item.age = goUtility.Instance.getAge(dob, eff);
                        item.age_y = goUtility.Instance.getAgeYear(dob, eff);
                        item.age_m = goUtility.Instance.getAgeMonth(dob, eff);
                        item.age_d = goUtility.Instance.getAgeDay(dob, eff);

                    }
                    catch (Exception ex)
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "ERROR_2009", "Ngày sai: " + JsonConvert.SerializeObject(item));
                    }
                }
                List<InsurCheck> lstInsurD18 = lstInsur.Where(i => i.age < 18).ToList();
                List<InsurCheck> lstInsurT18 = lstInsur.Where(i => i.age >= 18).ToList();
                InsurCheckOut insurCheckOut = new InsurCheckOut();
                insurCheckOut.lstInput = lstInsur;
                //TH: có trẻ dưới 18
                if (lstInsurD18 != null && lstInsurD18.Any())
                {
                    //nếu không có ng lớn >= 18 mua kèm => lỗi
                    if (lstInsurT18 == null || !lstInsurT18.Any())
                    {
                        insurCheckOut.type = "TH1";
                        insurCheckOut.type_mess = "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ";
                        response = goBusinessCommon.Instance.getResultApi(false, insurCheckOut, false, "", "", "ERROR_2009", insurCheckOut.type_mess);
                    }
                    else //nếu có người lớn => đạt yêu cầu
                    {
                        insurCheckOut.type = "TH2";
                        insurCheckOut.type_mess = "Trẻ em dưới 18 tuổi phải mua kèm với cha/mẹ hoặc người giám hộ";
                        response = goBusinessCommon.Instance.getResultApi(true, insurCheckOut, new ResponseConfig());
                    }
                }
                else
                {
                    insurCheckOut.type = "TH3";
                    response = goBusinessCommon.Instance.getResultApi(true, insurCheckOut, new ResponseConfig());
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "ERROR_2009", "Json lỗi");

            }
            return response;
        }

        #endregion

        #region Search Insur

        public BaseResponseV2 SearInsur(BaseRequest request)
        {
            BaseResponseV2 response = new BaseResponseV2();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                ObjSear objSear = new ObjSear();
                if (request.Data != null)
                    objSear = JsonConvert.DeserializeObject<ObjSear>(request.Data.ToString());

                var param = new
                {
                    channel = objSear.CHANNEL,
                    username = request.Action.UserName,
                    category = objSear.CATEGORY,
                    product_code = objSear.PRODUCT_CODE,
                    org_code = request.Action.ParentCode,
                    branch_code = objSear.BRANCH_CODE,
                    from = objSear.FROM,
                    to = objSear.TO,
                    status = objSear.STATUS,
                    page = objSear.PAGE
                };
                BaseRequest reqData = request;
                //reqData.Action.ActionCode = goConstantsProcedure.SEAR_INSUR;
                reqData.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                BaseResponse ResInsurData = goBusinessAction.Instance.GetBaseResponse(reqData);
                if (ResInsurData.Success)
                {
                    List<InsurDataBase> outData = new List<InsurDataBase>();
                    switch (objSear.CATEGORY)
                    {
                        case var s when objSear.CATEGORY.Equals(goConstants.CATEGORY_ATTD):
                            outData = GetInsurData_Lo(ResInsurData);
                            //var a = JsonConvert.SerializeObject(outData);
                            break;
                        // Start 2022-06-23 By ThienTVB
                        case var s when objSear.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                            outData = GetInsurData_LoHouseSelling(ResInsurData);
                            break;
                        // End 2022-06-23 By ThienTVB
                    }
                    response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, true, outData, config);
                    //response.Data = outData;
                    //response.LogId = auditLogs.LogId;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "err data: " + JsonConvert.SerializeObject(reqData), Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.ConvertResponse(ResInsurData, auditLogs.LogId);
                    resLog = ResInsurData;
                    return response;
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApiV2(auditLogs.LogId, false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), ex.Message);

            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }

        public List<InsurDataBase> GetInsurData_Lo(BaseResponse ResInsurData)
        {
            try
            {
                List<InsurDataBase> outData = new List<InsurDataBase>();

                // HĐ BH
                List<InsurContract> lstInsurContract = goBusinessCommon.Instance.GetData<InsurContract>(ResInsurData, 0);
                // CT BH
                List<OutInsurDetails> lstInsurDetail = goBusinessCommon.Instance.GetData<OutInsurDetails>(ResInsurData, 1);
                // HĐ vay
                List<OutLo> lstLo = goBusinessCommon.Instance.GetData<OutLo>(ResInsurData, 2);
                // CT HĐ vay (KUNN)
                List<OutDisbur> lstDisbur = goBusinessCommon.Instance.GetData<OutDisbur>(ResInsurData, 3);

                if (lstInsurContract != null && lstInsurContract.Any())
                {
                    foreach (var item in lstInsurContract)
                    {
                        InsurDataBase itemData = new InsurDataBase();
                        itemData.INSURED = new List<InsurDetailBase>();

                        // gán hđbh
                        itemData = JsonConvert.DeserializeObject<InsurDataBase>(JsonConvert.SerializeObject(item));
                        // lấy hđ vay
                        OutLo loExists = lstLo.Where(x => x.LO_CODE.Equals(item.REF_ID)).FirstOrDefault();

                        // lấy detail thuộc HĐBH
                        List<OutInsurDetails> lstIDetailExists = lstInsurDetail.Where(i => i.CONTRACT_CODE.Equals(item.CONTRACT_CODE)).ToList();

                        foreach (var it2 in lstIDetailExists)
                        {
                            InsurDetailBase itInsur = new InsurDetailBase();
                            // gán chi tiết BH
                            itInsur = JsonConvert.DeserializeObject<InsurDetailBase>(JsonConvert.SerializeObject(it2));

                            // gán hđ vay
                            itInsur.LO_INFO = new BankLoDetail();
                            BankLoDetail loDetail = new BankLoDetail();
                            loDetail.DISBUR = new BankLoDisbur();
                            loDetail = JsonConvert.DeserializeObject<BankLoDetail>(JsonConvert.SerializeObject(loExists));
                            loDetail.SELLER_CODE = loExists.TELLER_CODE;
                            // gán chi tiết giải ngân
                            OutDisbur disburExists = lstDisbur.Where(i => i.LOD_CODE.Equals(it2.REF_ID)).FirstOrDefault();
                            loDetail.DISBUR = JsonConvert.DeserializeObject<BankLoDisbur>(JsonConvert.SerializeObject(disburExists));
                            // gán thông tin vay
                            itInsur.LO_INFO = loDetail;

                            // gán itInsur vào list của HĐBH
                            itemData.TOTAL = it2.TOTAL;
                            itemData.PAGE_CURRENT = it2.PAGE_CURRENT;
                            itemData.PAGE_SIZE = it2.PAGE_SIZE;

                            itemData.INSURED = new List<InsurDetailBase>();
                            itemData.INSURED.Add(itInsur);
                        }

                        // gán HĐBH vào list out
                        outData.Add(itemData);
                    }
                }
                return outData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InsurDataBase> GetInsurData_LoHouseSelling(BaseResponse ResInsurData)
        {
            try
            {
                List<InsurDataBase> outData = new List<InsurDataBase>();

                // HĐ BH
                List<InsurContract> lstInsurContract = goBusinessCommon.Instance.GetData<InsurContract>(ResInsurData, 0);
                // CT BH
                List<OutInsurDetails> lstInsurDetail = goBusinessCommon.Instance.GetData<OutInsurDetails>(ResInsurData, 1);
                // HĐ vay
                List<OutLo> lstLo = goBusinessCommon.Instance.GetData<OutLo>(ResInsurData, 2);
                // CT ASSETS DETAILS HOUSE INFO
                List<OutHouseInfo> lstLoHouse = goBusinessCommon.Instance.GetData<OutHouseInfo>(ResInsurData, 3);

                if (lstInsurContract != null && lstInsurContract.Any())
                {
                    foreach (var item in lstInsurContract)
                    {
                        InsurDataBase itemData = new InsurDataBase();
                        itemData.INSURED = new List<InsurDetailBase>();

                        // gán hđbh
                        itemData = JsonConvert.DeserializeObject<InsurDataBase>(JsonConvert.SerializeObject(item));
                        // lấy hđ vay
                        OutLo loExists = lstLo.Where(x => x.LO_CODE.Equals(item.REF_ID)).FirstOrDefault();

                        // lấy detail thuộc HĐBH
                        List<OutInsurDetails> lstIDetailExists = lstInsurDetail.Where(i => i.CONTRACT_CODE.Equals(item.CONTRACT_CODE)).ToList();


                        foreach (var it2 in lstIDetailExists)
                        {
                            InsurDetailBase itInsur = new InsurDetailBase();
                            // gán chi tiết BH
                            itInsur = JsonConvert.DeserializeObject<InsurDetailBase>(JsonConvert.SerializeObject(it2));

                            // gán hđ vay
                            itInsur.LO_INFO = new BankLoDetail();
                            BankLoDetail loDetail = new BankLoDetail();
                            if (loExists != null)
                            {
                                loDetail = JsonConvert.DeserializeObject<BankLoDetail>(JsonConvert.SerializeObject(loExists));
                                loDetail.SELLER_CODE = loExists.TELLER_CODE;
                                loDetail.ASSET_CODE = loExists.ASSET_CODE;
                                OutHouseInfo hIFDB = lstLoHouse.Where(i => i.LOAD_CODE.Equals(it2.REF_ID)).FirstOrDefault();
                                if (hIFDB != null)
                                {
                                    loDetail.HOUSE_INFO = new HouseInfoLo
                                    {
                                        HOUSE_TYPE = hIFDB.HOUSE_TYPE,
                                        HOUSE_VALUE = hIFDB.HOUSE_VALUE,
                                        HOUSE_ADD = hIFDB.HOUSE_ADD
                                    };
                                }
                            }
                            // gán chi tiết giải ngân
                            //OutDisbur disburExists = lstDisbur.Where(i => i.LOD_CODE.Equals(it2.REF_ID)).FirstOrDefault();
                            loDetail.DISBUR = null; //JsonConvert.DeserializeObject<BankLoDisbur>(JsonConvert.SerializeObject(disburExists));
                            // gán thông tin vay
                            itInsur.LO_INFO = loDetail;

                            // gán itInsur vào list của HĐBH
                            itemData.TOTAL = it2.TOTAL;
                            itemData.PAGE_CURRENT = it2.PAGE_CURRENT;
                            itemData.PAGE_SIZE = it2.PAGE_SIZE;

                            itemData.INSURED = new List<InsurDetailBase>();
                            itemData.INSURED.Add(itInsur);
                        }

                        // gán HĐBH vào list out
                        outData.Add(itemData);
                    }
                }
                return outData;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region Boi thuong        

        public BaseResponse getDocuments(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                string strLanguage = job["LANGUAGE"].ToString(); //Ngôn ngữ
                
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, strLanguage};
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if(response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    response.Data = jDataExport;
                }    
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse searchPolicy(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                string strData = job["SEARCH"].ToString(); //DataSearh

                JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, jData };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataSearch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    List<JObject> jDataClaim = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                    
                    JObject jDataResponse = new JObject();
                    JArray jArrDataSearch = JArray.FromObject(jDataSearch);
                    JArray jArrClaim = JArray.FromObject(jDataClaim);
                    jDataResponse.Add("DATA", jArrDataSearch);
                    jDataResponse.Add("CLAIM", jArrClaim);
                    response.Data = jDataResponse;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse submitClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                //string strData = job["CLAIM"].ToString(); //DataSearh

                //JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, job };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);                    
                    //Send Email toi Ban Boi thuong và Khach hang
                    if (jDataExport != null && jDataExport.Count > 0)
                    {
                        new Task(() => {
                            BaseResponse resLog = new BaseResponse();
                            JObject objEmail = new JObject();
                            objEmail.Add("DATE_CLAIM", jDataExport[0]["DATE_CLAIM"]);
                            objEmail.Add("DATE_REQUIRED", jDataExport[0]["DATE_REQUIRED"]);
                            objEmail.Add("CERTIFICATE_NO", jDataExport[0]["CERTIFICATE_NO"]);
                            objEmail.Add("INSURED_NAME", jDataExport[0]["INSURED_NAME"]);
                            objEmail.Add("DECLARE_NAME", jDataExport[0]["DECLARE_NAME"]);
                            objEmail.Add("REQUIRED_AMOUNT", jDataExport[0]["REQUIRED_AMOUNT"]);
                            objEmail.Add("PRODUCT_NAME", jDataExport[0]["PRODUCT_NAME"]);
                            objEmail.Add("LINK", jDataExport[0]["LINK"]);
                            objEmail.Add("URL_CLAIM", jDataExport[0]["URL_CLAIM"]);
                            objEmail.Add("CLAIM_NO", jDataExport[0]["CLAIM_NO"]);
                            JArray jArrMail = new JArray();
                            JObject objTempEmailTN = new JObject();
                            objTempEmailTN.Add("TEMP_CODE", "BT_TNHAN");
                            objTempEmailTN.Add("PRODUCT_CODE", "BT_TNHAN");
                            objTempEmailTN.Add("ORG_CODE", "HDI");
                            objTempEmailTN.Add("TYPE", "EMAIL");
                            objTempEmailTN.Add("TO", jDataExport[0]["EMAIL"]);
                            objTempEmailTN.Add("CC", jDataExport[0]["INSURED_EMAIL"]);
                            objTempEmailTN.Add("BCC", "");                            
                            jArrMail.Add(objTempEmailTN);

                            JObject objTempEmailTB = new JObject();
                            objTempEmailTB.Add("TEMP_CODE", "BT_TBAO");
                            objTempEmailTB.Add("PRODUCT_CODE", "BT_TBAO");
                            objTempEmailTB.Add("ORG_CODE", "HDI");
                            objTempEmailTB.Add("TYPE", "EMAIL");
                            objTempEmailTB.Add("TO", jDataExport[0]["CLAIM_EMAIL"]);
                            objTempEmailTB.Add("CC", "");
                            objTempEmailTB.Add("BCC", "");                            
                            jArrMail.Add(objTempEmailTB);
                            objEmail.Add("TEMP", jArrMail);
                            resLog = goBusinessServices.Instance.sendMailWithJson(objEmail);
                        }).Start();
                    }                        
                    response.Data = jDataExport;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse getDetailClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();

            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị                
                string strData = job["CLAIM"].ToString(); //DataSearh

                JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, jData };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataClaim = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    List<JObject> jClaimDescription = goBusinessCommon.Instance.GetData<JObject>(response, 2);
                    List<JObject> jAttachFiles = goBusinessCommon.Instance.GetData<JObject>(response, 3);
                    List<JObject> jProcess = goBusinessCommon.Instance.GetData<JObject>(response, 4);
                    List<JObject> jDetailPABT = goBusinessCommon.Instance.GetData<JObject>(response, 5);
                    List<JObject> jFileAttachMore = goBusinessCommon.Instance.GetData<JObject>(response, 6);
                    JObject jDataResponse = new JObject();
                    JArray jArrDataClaim = JArray.FromObject(jDataClaim);
                    JArray jArrClaimDescription = JArray.FromObject(jClaimDescription);
                    JArray jArrAttachFiles = JArray.FromObject(jAttachFiles);
                    JArray jArrProcess = JArray.FromObject(jProcess);
                    JArray jArrDetailPABT = JArray.FromObject(jDetailPABT);
                    JArray jArrFileAttachMore = JArray.FromObject(jFileAttachMore);
                    jDataResponse.Add("CLAIM_DECLARE", jArrDataClaim);
                    jDataResponse.Add("DECLARE_EXT", jArrClaimDescription);
                    jDataResponse.Add("FILE_ATTACH", jArrAttachFiles);
                    jDataResponse.Add("CLAIM_PROCESS", jArrProcess);
                    jDataResponse.Add("CLAIM_PABT", jArrDetailPABT);
                    jDataResponse.Add("FILE_ATTACH_MORE", jArrFileAttachMore);
                    //Send Email toi Ban Boi thuong và Khach hang                   
                    response.Data = jDataResponse;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse updateBeneInfoClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                //string strData = job["CLAIM"].ToString(); //Data Update

                //JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, job };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    response.Data = jDataExport;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse updateStatusClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                string strClaimID = job["CLAIMTRANS"].ToString(); //Id Claim
                string strWorkID = job["WORKID"].ToString(); //Mã tiến trình
                string strStaff= job["STAFFCODE"].ToString(); //Mã người thực hiện - CUS
                string strStaffName = job["STAFFNAME"].ToString(); //Tên người - Hệ thống thực hiện HDOSP
                string strContent = job["CONTENT"].ToString(); //Nội dung thực hiện - "Khách hàng xác nhận đồng ý":"Khách hàng xác nhận không đồng ý"
                //string strData = job["CLAIM"].ToString(); //DataSearh

                //JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, strClaimID, strWorkID , strStaff, strStaffName, strContent, null, "" };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    response.Data = jDataExport;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse uploadDocument(HttpRequest request, byte[] fileUpload)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                //if (config == null)
                //{
                //    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                //    return response;
                //}
                //Lay danh sách file xử lý bồi thường

                String env = request.Headers.Get("ENV");
                String sign = request.Headers.Get("SIGN");
                JObject job = JObject.Parse(request.Headers.Get("DATA"));
                String hashkey = goConfig.Instance.GetAppConfig(goConstants.E_COM_HASHKEY + "_" + env);
                string strMD5 = goEncryptBase.Instance.Md5Encode(hashkey + job["FILE_NAME"].ToString()).ToLower();
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "HDI E-Com " + strMD5 + sign.ToString(), "uploadDocument");
                if (!strMD5.ToLower().Equals(sign))
                {
                    return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), "Lỗi kiểm tra MD");
                }                
                         
                string strFileKey = job["FILE_KEY"].ToString(); //Mã đơn vị
                string strFileName = job["FILE_NAME"].ToString(); //mã sản phẩm
                string strFileType = job["FILE_TYPE"].ToString(); //mã sản phẩm
                string strFileRef = job["FILE_REF"].ToString(); //Số GCN
                //string strFileBase64 = job["BASE64"].ToString(); //Base 64

                String strUrl = "";

                byte[] file = fileUpload;//Convert.FromBase64String(strFileBase64);
                string strGuiID_File = Guid.NewGuid().ToString("N");
                //
                bool bUpload = goBusinessCommon.Instance.uploadFileServer(fileUpload, strFileRef + "." + strFileType, "",
                       null, ref strFileName, ref strUrl, strGuiID_File);

                if(bUpload)
                {
                    JObject jResponse = new JObject();
                    jResponse.Add("FILE_KEY", strFileKey);
                    jResponse.Add("FILE_NAME", job["FILE_NAME"].ToString());
                    jResponse.Add("FILE_ID", strGuiID_File);
                    jResponse.Add("FILE_URL", strUrl + "?download=true");
                    response.Success = bUpload;
                    response.Data = jResponse;
                }    
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        public BaseResponse updateDocumentClaim(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                string strClaimID = job["CLAIMTRANS"].ToString(); //Id Claim
                string strWorkID = job["WORKID"].ToString(); //Mã tiến trình DABS
                string strStaff = job["STAFFCODE"].ToString(); //Mã người thực hiện - CUS
                string strStaffName = job["STAFFNAME"].ToString(); //Tên người - Hệ thống thực hiện HDOSP HDOSP LANDING CLAIM
                string strContent = job["CONTENT"].ToString(); //Nội dung thực hiện - "Bổ sung hồ sơ giấy tờ"
                //string strData = job["CLAIM_FILE"].ToString(); //File Attach More [{FILE_KEY,FILE_NAME,FILE_ID}]

                //JObject jData = JObject.Parse(strData);

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", strOrgCode, strProductCode, strClaimID, strWorkID, strStaff, strStaffName, strContent, null, job };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    response.Data = jDataExport;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        #endregion

        #region ChangeRequest

        public BaseResponse changeRequest(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                //Lay danh sách file xử lý bồi thường

                JObject job = JObject.Parse(request.Data.ToString());
                /*
                string strOrgCode = job["ORG_CODE"].ToString(); //Mã đơn vị
                string strProductCode = job["PRODUCT_CODE"].ToString(); //mã sản phẩm
                string strClaimID = job["CLAIMTRANS"].ToString(); //Id Claim
                string strWorkID = job["WORKID"].ToString(); //Mã tiến trình
                string strStaff = job["STAFFCODE"].ToString(); //Mã người thực hiện - CUS
                string strStaffName = job["STAFFNAME"].ToString(); //Tên người - Hệ thống thực hiện HDOSP
                string strContent = job["CONTENT"].ToString(); //Nội dung thực hiện - "Khách hàng xác nhận đồng ý":"Khách hàng xác nhận không đồng ý"
                */
                //string strData = job["data_request"].ToString(); //DataSearh

                string strData = job["data_request"].ToString(); //DataSearh

                JObject jData = JObject.Parse(strData);
                

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                var param = new object[] { "", "", jData };
                response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);

                if (response.Success)
                {
                    List<JObject> jDataExport = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    response.Data = jDataExport;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009); ;
            }
            return response;
        }

        #endregion

        #region Create Json Mask Object
        private JObject createJsonMask_Health()
        {
            JObject jData = new JObject();
            JObject jDataSeller = new JObject();
            JObject jDataBuyer = new JObject();
            JObject jDataPayInfo = new JObject();
            JObject jDataHealthInsur = new JObject();
            JArray jArrHealthInsur = new JArray();

            #region General

            jData.Add("CHANNEL", "SDK_HEALTH");
            jData.Add("USERNAME", "");
            jData.Add("ORG_CODE", "");
            jData.Add("ACTION", "BH_M");
            jData.Add("CATEGORY", "CN.02");
            jData.Add("PRODUCT_CODE", "");


            #endregion General

            #region Seller

            jDataSeller.Add("SELLER_CODE", "");
            jDataSeller.Add("ORG_CODE", "");
            jDataSeller.Add("SELLER_NAME", "");
            jDataSeller.Add("SELLER_EMAIL", "");
            jDataSeller.Add("SELLER_PHONE", "");
            jDataSeller.Add("TRAFFIC_LINK", "");
            jDataSeller.Add("ENVIROMENT", "");

            #endregion Seller

            #region Buyer

            jDataBuyer.Add("TYPE", "CN");
            jDataBuyer.Add("NAME", "");
            jDataBuyer.Add("DOB", "");
            jDataBuyer.Add("GENDER", "");
            jDataBuyer.Add("ADDRESS", "");
            jDataBuyer.Add("IDCARD", "");
            jDataBuyer.Add("EMAIL", "");
            jDataBuyer.Add("PHONE", "");
            jDataBuyer.Add("PROV", "");
            jDataBuyer.Add("DIST", "");
            jDataBuyer.Add("WARDS", "");
            jDataBuyer.Add("IDCARD_D", "");
            jDataBuyer.Add("IDCARD_P", "");
            jDataBuyer.Add("FAX", "");
            jDataBuyer.Add("TAXCODE", "");

            #endregion Buyer

            #region Payment

            jDataPayInfo.Add("PAYMENT_TYPE", "");
            jDataPayInfo.Add("IS_SMS", "");

            #endregion Payment

            #region HealthInsur

            jDataHealthInsur.Add("CONTRACT_CODE", "");
            jDataHealthInsur.Add("CERTIFICATE_NO", "");
            jDataHealthInsur.Add("CUS_ID", "");
            jDataHealthInsur.Add("TYPE", "CN");
            jDataHealthInsur.Add("NATIONALITY", "VN");
            jDataHealthInsur.Add("NAME", "");
            jDataHealthInsur.Add("DOB", "");
            jDataHealthInsur.Add("GENDER", "");
            jDataHealthInsur.Add("ADDRESS", "");
            jDataHealthInsur.Add("IDCARD", "");
            jDataHealthInsur.Add("EMAIL", "");
            jDataHealthInsur.Add("PHONE", "");
            jDataHealthInsur.Add("PROV", "");
            jDataHealthInsur.Add("DIST", "");
            jDataHealthInsur.Add("WARDS", "");
            jDataHealthInsur.Add("FAX", "");
            jDataHealthInsur.Add("TAXCODE", "");
            jDataHealthInsur.Add("RELATIONSHIP", "");
            jDataHealthInsur.Add("REGION", "");
            jDataHealthInsur.Add("EFFECTIVE_DATE", "");
            jDataHealthInsur.Add("EXPIRATION_DATE", "");
            jDataHealthInsur.Add("PRODUCT_CODE", "");
            jDataHealthInsur.Add("PACK_CODE", "");

            jArrHealthInsur.Add(jDataHealthInsur);            

            #endregion HealthInsur

            jData.Add("SELLER", jDataSeller);
            jData.Add("BUYER", jDataBuyer);
            jData.Add("PAY_INFO", jDataPayInfo);
            jData.Add("HEALTH_INSUR", jArrHealthInsur);
            return jData;
        }
        #endregion
    }
}
