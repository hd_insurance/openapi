﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.DTO.Base;
using GO.DTO.Common;
using GO.BUSINESS.Common;
using GO.CONSTANTS;
using GO.DTO.CalculateFees;
using Newtonsoft.Json;
using GO.HELPER.Utility;
using GO.COMMON.Log;
using Newtonsoft.Json.Linq;

namespace GO.BUSINESS.Insurance
{
    public class goBusinessMaskFees
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessMaskFees> _instance = new Lazy<goBusinessMaskFees>(() =>
        {
            return new goBusinessMaskFees();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessMaskFees Instance { get => _instance.Value; }
        #endregion
        #region Method
        public BaseResponse MaskFees(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                InputDynamicFeesVer3 inputDynamic = new InputDynamicFeesVer3();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFeesVer3>(request.Data.ToString());

                if (inputDynamic == null || string.IsNullOrEmpty(inputDynamic.ORG_CODE) || inputDynamic.PRODUCT_INFO == null || !inputDynamic.PRODUCT_INFO.Any())
                    throw new Exception("request k đúng");

                foreach(var item in inputDynamic.PRODUCT_INFO)
                {
                    switch (item.CATEGORY)
                    {
                        case var s when item.CATEGORY.Equals(goConstants.CATEGORY_XE):
                            if (item.PRODUCT_CODE.Equals("XCG_TNDSBB_NEW"))
                            {
                                DateTime dEff = goUtility.Instance.ConvertToDateTime(item.EFF, DateTime.MinValue);
                                DateTime dExp = goUtility.Instance.ConvertToDateTime(item.EXP, DateTime.MinValue);
                                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);

                                foreach(var itemDyn in item.DYNAMIC_FEES)
                                {
                                    if (itemDyn.KEY_CODE.Equals("XCG_TGBH"))
                                        itemDyn.VAL_CODE = day.ToString();

                                    if (day < 30)
                                    {
                                        if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                                            itemDyn.VAL_CODE = "1";
                                        if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                                            itemDyn.VAL_CODE = "0";
                                    }
                                    else
                                    {
                                        if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                                            itemDyn.VAL_CODE = "0";
                                        if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                                            itemDyn.VAL_CODE = day.ToString();
                                    }
                                }
                            }

                            if (item.PRODUCT_CODE.Equals("XCG_VCX_NEW"))
                            {
                                DateTime dEff = goUtility.Instance.ConvertToDateTime(item.EFF, DateTime.MinValue);
                                DateTime dExp = goUtility.Instance.ConvertToDateTime(item.EXP, DateTime.MinValue);
                                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);

                                foreach (var itemDyn in item.DYNAMIC_FEES)
                                {
                                    if (itemDyn.KEY_CODE.Equals("XCG_TGBH"))
                                        itemDyn.VAL_CODE = day.ToString();
                                }
                            }
                            break;
                    }
                }
                OutDynamicFeesVer3 outFees = goBusinessCalculateFees.Instance.GetOutCalFeesVer3(inputDynamic, request);
                response = goBusinessCommon.Instance.getResultApi(true, outFees, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_FEES_01), goConstantsError.Instance.ERROR_FEES_01);
            }

            return response;
        }

        public BaseResponse MaskFeesHealthCare(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                InputDynamicFeesVer4 inputDynamic = new InputDynamicFeesVer4();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFeesVer4>(request.Data.ToString());

                if (inputDynamic == null || string.IsNullOrEmpty(inputDynamic.ORG_CODE) || inputDynamic.PRODUCT_INFO == null || !inputDynamic.PRODUCT_INFO.Any())
                    throw new Exception("request k đúng");

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                OutDynamicFeesVer3 outFees = new OutDynamicFeesVer3();
                outFees.PRODUCT_DETAIL = new List<OutDynamicProductVer3>();

                foreach (var item in inputDynamic.PRODUCT_INFO)
                {
                    switch (item.CATEGORY)
                    {
                        case var s when item.CATEGORY.Equals(goConstants.CATEGORY_SK):
                            if (item.PRODUCT_CODE.Equals("E-COM-PARTNER_SUCKHOE365"))
                            {
                                JObject jData = new JObject();
                                jData.Add("A1", inputDynamic.ORG_CODE);
                                jData.Add("A2", item.PRODUCT_CODE);
                                jData.Add("A3", item.PACK_CODE);
                                jData.Add("A4", item.DOB);
                                jData.Add("A5", "");
                                jData.Add("A6", item.EFF);
                                jData.Add("A7", "G_HEALTH");
                                jData.Add("A8", "1");

                                object[] param = new object[] { "", "", jData };
                                response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.HEALTH_FEE, strEvm, param);

                                if (response.Success)
                                {                                    
                                    List<JObject> jDataFee = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                                    if(jDataFee.Count > 0)
                                    {
                                        outFees.FEES_DATA = double.Parse(jDataFee[0]["MESSAGE"].ToString());
                                        outFees.AMOUNT += outFees.FEES_DATA;
                                        outFees.TOTAL_DISCOUNT += 0;
                                        outFees.VAT += 0;
                                        outFees.TOTAL_AMOUNT += outFees.AMOUNT + outFees.VAT;
                                    }
                                    
                                }
                            }
                            break;
                    }
                }
                //OutDynamicFeesVer3 outFees = goBusinessCalculateFees.Instance.GetOutCalFeesVer3(inputDynamic, request);
                response = goBusinessCommon.Instance.getResultApi(true, outFees, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_FEES_01), goConstantsError.Instance.ERROR_FEES_01);
            }

            return response;
        }
        #endregion
    }
}
