﻿using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.CalculateFees;
using GO.DTO.Insurance;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.Insurance
{
    /// <summary>
    /// fees input validation with calculate fees server
    /// </summary>
    public class goBusinessValidateFees
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessValidateFees> _instance = new Lazy<goBusinessValidateFees>(() =>
        {
            return new goBusinessValidateFees();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessValidateFees Instance { get => _instance.Value; }
        #endregion
        #region Method

        public BaseValidate ValidationFessVehicle(VehicleInsur vehicleInsur, OutDynamicFeesVer3 outDynamic)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            BaseValidate baseValidate = new BaseValidate();
            baseValidate.Success = true;
            try
            {
                if (outDynamic != null && outDynamic.PRODUCT_DETAIL != null && outDynamic.PRODUCT_DETAIL.Any())
                {
                    foreach (var item in vehicleInsur.VEHICLE_INSUR)
                    {
                        double amount = 0, total_discount = 0, vat = 0, total_amount = 0, total_add = 0;

                        OutDynamicProductVer3 productVer3 = new OutDynamicProductVer3();
                        if (item.IS_PHYSICAL == "1")
                            productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == item.PHYSICAL_DAMAGE.INX).FirstOrDefault();

                        if (item.IS_COMPULSORY == "1")
                            productVer3 = outDynamic.PRODUCT_DETAIL.Where(x => x.INX == item.COMPULSORY_CIVIL.INX).FirstOrDefault();

                        // nếu productVer3 null => error
                        if (productVer3 == null || productVer3.OUT_DETAIL == null || !productVer3.OUT_DETAIL.Any())
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                            return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_32), goConstantsValidate.ERR_CHK_32);
                        }

                        List<ADDITIONAL_FEES> lstOut = productVer3.OUT_DETAIL;
                        // VCX
                        if (item.IS_PHYSICAL == "1")
                        {
                            // Phí VCX All
                            ADDITIONAL_FEES feesVCX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_FEES.ToString())).FirstOrDefault();
                            if (feesVCX == null)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_32), goConstantsValidate.ERR_CHK_32);
                            }
                            //vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.FEES_DATA = feesVCX.FEES_DATA;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.FEES, feesVCX.FEES, nameof(goConstantsValidate.ERR_CHK_33), goConstantsValidate.ERR_CHK_33);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.AMOUNT, feesVCX.AMOUNT, nameof(goConstantsValidate.ERR_CHK_34), goConstantsValidate.ERR_CHK_34);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.TOTAL_DISCOUNT, feesVCX.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_35), goConstantsValidate.ERR_CHK_35);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.VAT, feesVCX.VAT, nameof(goConstantsValidate.ERR_CHK_36), goConstantsValidate.ERR_CHK_36);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.TOTAL_AMOUNT, feesVCX.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_37), goConstantsValidate.ERR_CHK_37);
                            if (!baseValidate.Success)
                                return baseValidate;

                            // thông tin phí gốc (chưa gồm bổ sung)
                            ADDITIONAL_FEES feesVCX_NoBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_PL1.ToString())).FirstOrDefault();
                            if (feesVCX_NoBS == null)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_38), goConstantsValidate.ERR_CHK_38);
                            }
                            //vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_FEES_DATA = feesVCX_NoBS.FEES_DATA;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_FEES, feesVCX_NoBS.FEES, nameof(goConstantsValidate.ERR_CHK_39), goConstantsValidate.ERR_CHK_39);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_AMOUNT, feesVCX_NoBS.AMOUNT, nameof(goConstantsValidate.ERR_CHK_40), goConstantsValidate.ERR_CHK_40);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_DISCOUNT, feesVCX_NoBS.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_41), goConstantsValidate.ERR_CHK_41);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_VAT, feesVCX_NoBS.VAT, nameof(goConstantsValidate.ERR_CHK_42), goConstantsValidate.ERR_CHK_42);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_TOTAL_AMOUNT, feesVCX_NoBS.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_43), goConstantsValidate.ERR_CHK_43);
                            if (!baseValidate.Success)
                                return baseValidate;

                            // Thông tin bổ sung vcx
                            if (item.PHYSICAL_DAMAGE.ADDITIONAL != null && productVer3.ADDITI_DETAIL == null)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ADDITIONAL: " + JsonConvert.SerializeObject(item.PHYSICAL_DAMAGE.ADDITIONAL) + " .productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_44), goConstantsValidate.ERR_CHK_44);
                            }
                            if (item.PHYSICAL_DAMAGE.ADDITIONAL != null && productVer3.ADDITI_DETAIL != null)
                            {
                                ADDITIONAL_FEES feesVcxBS = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCG_VCX_BS.ToString())).FirstOrDefault();
                                // tổng bổ sung vcx
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.TOTAL_ADDITIONAL, feesVcxBS.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_45), goConstantsValidate.ERR_CHK_45);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                // chi tiết tổng BS
                                //vehicleInsur.VEHICLE_INSUR[i].PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES_DATA = feesVcxBS.FEES_DATA;
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_ADD_FEES, feesVcxBS.FEES, nameof(goConstantsValidate.ERR_CHK_46), goConstantsValidate.ERR_CHK_46);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_ADD_AMOUNT, feesVcxBS.AMOUNT, nameof(goConstantsValidate.ERR_CHK_47), goConstantsValidate.ERR_CHK_47);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_DISCOUNT, feesVcxBS.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_48), goConstantsValidate.ERR_CHK_48);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_ADD_VAT, feesVcxBS.VAT, nameof(goConstantsValidate.ERR_CHK_49), goConstantsValidate.ERR_CHK_49);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.PHYSICAL_DAMAGE.PHYSICAL_ADD_TOTAL_AMOUNT, feesVcxBS.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_50), goConstantsValidate.ERR_CHK_50);
                                if (!baseValidate.Success)
                                    return baseValidate;

                                // danh sách bs truyền lên
                                foreach (var itemBs in item.PHYSICAL_DAMAGE.ADDITIONAL)
                                {
                                    List<ADDITIONAL_FEES> lstOutAdd = productVer3.ADDITI_DETAIL;
                                    ADDITIONAL_FEES existAddi = lstOutAdd.Where(x => x.CODE.Equals(itemBs.KEY))?.FirstOrDefault();
                                    if (existAddi == null)
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ADDITIONAL: " + JsonConvert.SerializeObject(item.PHYSICAL_DAMAGE.ADDITIONAL), Logger.ConcatName(nameClass, nameMethod));
                                        return goBusinessCommon.Instance.ResValidate(false, "ERR_BS_VCX", itemBs.KEY);
                                    }
                                    CompareFees(ref baseValidate, itemBs.FEES, existAddi.FEES, "ERR_BS_VCX", itemBs.KEY + " FEES");
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, itemBs.AMOUNT, existAddi.AMOUNT, "ERR_BS_VCX", itemBs.KEY + " AMOUNT");
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, itemBs.TOTAL_DISCOUNT, existAddi.TOTAL_DISCOUNT, "ERR_BS_VCX", itemBs.KEY + " TOTAL_DISCOUNT");
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, itemBs.VAT, existAddi.VAT, "ERR_BS_VCX", itemBs.KEY + " VAT");
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, itemBs.TOTAL_AMOUNT, existAddi.TOTAL_AMOUNT, "ERR_BS_VCX", itemBs.KEY + " TOTAL_AMOUNT");
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                }

                            }

                            // gán tổng tiền của đơn
                            amount += productVer3.AMOUNT;
                            total_discount += productVer3.TOTAL_DISCOUNT;
                            vat += productVer3.VAT;
                            total_amount += productVer3.TOTAL_AMOUNT;
                            total_add += productVer3.TOTAL_ADD;
                        }

                        // TNDS BB
                        if (item.IS_COMPULSORY == "1")
                        {
                            // Phí TNDS BB
                            ADDITIONAL_FEES feesBB = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.TNDSBB_F.ToString())).FirstOrDefault();
                            if (feesBB == null)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_51), goConstantsValidate.ERR_CHK_51);
                            }
                            CompareFees(ref baseValidate, item.COMPULSORY_CIVIL.FEES, feesBB.FEES, nameof(goConstantsValidate.ERR_CHK_52), goConstantsValidate.ERR_CHK_52);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.COMPULSORY_CIVIL.AMOUNT, feesBB.AMOUNT, nameof(goConstantsValidate.ERR_CHK_53), goConstantsValidate.ERR_CHK_53);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.COMPULSORY_CIVIL.VAT, feesBB.VAT, nameof(goConstantsValidate.ERR_CHK_54), goConstantsValidate.ERR_CHK_54);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.COMPULSORY_CIVIL.TOTAL_DISCOUNT, feesBB.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_55), goConstantsValidate.ERR_CHK_55);
                            if (!baseValidate.Success)
                                return baseValidate;
                            if (item.COMPULSORY_CIVIL.TOTAL_DISCOUNT > 0) // TNDS BB không được giảm phí
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_55), goConstantsValidate.ERR_CHK_55);

                            CompareFees(ref baseValidate, item.COMPULSORY_CIVIL.TOTAL_AMOUNT, feesBB.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_56), goConstantsValidate.ERR_CHK_56);
                            if (!baseValidate.Success)
                                return baseValidate;

                            // gán tổng tiền của đơn
                            amount += productVer3.AMOUNT;
                            total_discount += productVer3.TOTAL_DISCOUNT;
                            vat += productVer3.VAT;
                            total_amount += productVer3.TOTAL_AMOUNT;
                            total_add += productVer3.TOTAL_ADD;
                        }

                        // TNDS TN
                        if (item.IS_VOLUNTARY_ALL == "1")
                        {
                            ADDITIONAL_FEES feesTN = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_BS.ToString())).FirstOrDefault();
                            if (feesTN == null)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_57), goConstantsValidate.ERR_CHK_57);
                            }
                            CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES, feesTN.FEES, nameof(goConstantsValidate.ERR_CHK_58), goConstantsValidate.ERR_CHK_58);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT, feesTN.AMOUNT, nameof(goConstantsValidate.ERR_CHK_59), goConstantsValidate.ERR_CHK_59);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT, feesTN.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_60), goConstantsValidate.ERR_CHK_60);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT, feesTN.VAT, nameof(goConstantsValidate.ERR_CHK_61), goConstantsValidate.ERR_CHK_61);
                            if (!baseValidate.Success)
                                return baseValidate;
                            CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_AMOUNT, feesTN.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_62), goConstantsValidate.ERR_CHK_62);
                            if (!baseValidate.Success)
                                return baseValidate;

                            // chi tiết vqm
                            if (item.IS_VOLUNTARY == "1")
                            {
                                // thân thể t3
                                ADDITIONAL_FEES feesTN_T3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_N3_F.ToString())).FirstOrDefault();
                                if (feesTN_T3 != null && (item.VOLUNTARY_CIVIL.FEES_3RD_INSUR == 0 || item.VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR == 0 ||
                                    item.VOLUNTARY_CIVIL.TOTAL_3RD_INSUR == 0))
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_63), goConstantsValidate.ERR_CHK_63);
                                }
                                if (feesTN_T3 != null)
                                {
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_3RD_INSUR, feesTN_T3.FEES, nameof(goConstantsValidate.ERR_CHK_64), goConstantsValidate.ERR_CHK_64);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_3RD_INSUR, feesTN_T3.AMOUNT, nameof(goConstantsValidate.ERR_CHK_65), goConstantsValidate.ERR_CHK_65);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_INSUR, feesTN_T3.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_66), goConstantsValidate.ERR_CHK_66);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_3RD_INSUR, feesTN_T3.VAT, nameof(goConstantsValidate.ERR_CHK_67), goConstantsValidate.ERR_CHK_67);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_3RD_INSUR, feesTN_T3.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_68), goConstantsValidate.ERR_CHK_68);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                }

                                // tài sản t3
                                ADDITIONAL_FEES feesTN_TST3 = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_TSN3_F.ToString())).FirstOrDefault();
                                if (feesTN_TST3 != null && (item.VOLUNTARY_CIVIL.FEES_3RD_ASSETS == 0 || item.VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS == 0 ||
                                    item.VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS == 0))
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_69), goConstantsValidate.ERR_CHK_69);
                                }
                                if (feesTN_TST3 != null)
                                {
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_3RD_ASSETS, feesTN_TST3.FEES, nameof(goConstantsValidate.ERR_CHK_70), goConstantsValidate.ERR_CHK_70);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_3RD_ASSETS, feesTN_TST3.AMOUNT, nameof(goConstantsValidate.ERR_CHK_71), goConstantsValidate.ERR_CHK_71);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_3RD_ASSETS, feesTN_TST3.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_72), goConstantsValidate.ERR_CHK_72);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_3RD_ASSETS, feesTN_TST3.VAT, nameof(goConstantsValidate.ERR_CHK_73), goConstantsValidate.ERR_CHK_73);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_3RD_ASSETS, feesTN_TST3.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_74), goConstantsValidate.ERR_CHK_74);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                }

                                // hành khách
                                ADDITIONAL_FEES feesTN_HK = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_HK_F.ToString())).FirstOrDefault();
                                if (feesTN_HK != null && (item.VOLUNTARY_CIVIL.FEES_PASSENGER == 0 || item.VOLUNTARY_CIVIL.AMOUNT_PASSENGER == 0 ||
                                    item.VOLUNTARY_CIVIL.TOTAL_PASSENGER == 0))
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lstOut: " + JsonConvert.SerializeObject(lstOut), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_75), goConstantsValidate.ERR_CHK_75);
                                }
                                if (feesTN_HK != null)
                                {
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_PASSENGER, feesTN_HK.FEES, nameof(goConstantsValidate.ERR_CHK_76), goConstantsValidate.ERR_CHK_76);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_PASSENGER, feesTN_HK.AMOUNT, nameof(goConstantsValidate.ERR_CHK_77), goConstantsValidate.ERR_CHK_77);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_PASSENGER, feesTN_HK.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_78), goConstantsValidate.ERR_CHK_78);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_PASSENGER, feesTN_HK.VAT, nameof(goConstantsValidate.ERR_CHK_79), goConstantsValidate.ERR_CHK_79);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                    CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_PASSENGER, feesTN_HK.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_80), goConstantsValidate.ERR_CHK_80);
                                    if (!baseValidate.Success)
                                        return baseValidate;
                                }

                                // tổng vqm
                                ADDITIONAL_FEES feesTN_VQM = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_VQM.ToString())).FirstOrDefault();
                                if (feesTN_VQM == null)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_81), goConstantsValidate.ERR_CHK_81);
                                }
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_VOLUNTARY_3RD, feesTN_VQM.FEES, nameof(goConstantsValidate.ERR_CHK_82), goConstantsValidate.ERR_CHK_82);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_VOLUNTARY_3RD, feesTN_VQM.AMOUNT, nameof(goConstantsValidate.ERR_CHK_83), goConstantsValidate.ERR_CHK_83);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_VOLUNTARY_3RD, feesTN_VQM.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_84), goConstantsValidate.ERR_CHK_84);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_VOLUNTARY_3RD, feesTN_VQM.VAT, nameof(goConstantsValidate.ERR_CHK_85), goConstantsValidate.ERR_CHK_85);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_VOLUNTARY_3RD, feesTN_VQM.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_86), goConstantsValidate.ERR_CHK_86);
                                if (!baseValidate.Success)
                                    return baseValidate;
                            }

                            if (item.IS_DRIVER == "1")
                            {
                                ADDITIONAL_FEES feesTN_LPX = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FNTX.ToString())).FirstOrDefault();
                                if (feesTN_LPX == null)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_87), goConstantsValidate.ERR_CHK_87);
                                }
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_DRIVER, feesTN_LPX.FEES, nameof(goConstantsValidate.ERR_CHK_88), goConstantsValidate.ERR_CHK_88);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_DRIVER, feesTN_LPX.AMOUNT, nameof(goConstantsValidate.ERR_CHK_89), goConstantsValidate.ERR_CHK_89);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_DRIVER, feesTN_LPX.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_90), goConstantsValidate.ERR_CHK_90);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_DRIVER, feesTN_LPX.VAT, nameof(goConstantsValidate.ERR_CHK_91), goConstantsValidate.ERR_CHK_91);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DRIVER, feesTN_LPX.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_92), goConstantsValidate.ERR_CHK_92);
                                if (!baseValidate.Success)
                                    return baseValidate;
                            }
                            if (item.IS_CARGO == "1")
                            {
                                ADDITIONAL_FEES feesTN_HH = lstOut.Where(x => x.CODE.Equals(goConstants.KeyFeesInfo.XCGTN_FBHHH.ToString())).FirstOrDefault();
                                if (feesTN_HH == null)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "productVer3: " + JsonConvert.SerializeObject(productVer3), Logger.ConcatName(nameClass, nameMethod));
                                    return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_93), goConstantsValidate.ERR_CHK_93);
                                }
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.FEES_CARGO, feesTN_HH.FEES, nameof(goConstantsValidate.ERR_CHK_94), goConstantsValidate.ERR_CHK_94);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.AMOUNT_CARGO, feesTN_HH.AMOUNT, nameof(goConstantsValidate.ERR_CHK_95), goConstantsValidate.ERR_CHK_95);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_DISCOUNT_CARGO, feesTN_HH.TOTAL_DISCOUNT, nameof(goConstantsValidate.ERR_CHK_96), goConstantsValidate.ERR_CHK_96);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.VAT_CARGO, feesTN_HH.VAT, nameof(goConstantsValidate.ERR_CHK_97), goConstantsValidate.ERR_CHK_97);
                                if (!baseValidate.Success)
                                    return baseValidate;
                                CompareFees(ref baseValidate, item.VOLUNTARY_CIVIL.TOTAL_CARGO, feesTN_HH.TOTAL_AMOUNT, nameof(goConstantsValidate.ERR_CHK_98), goConstantsValidate.ERR_CHK_98);
                                if (!baseValidate.Success)
                                    return baseValidate;
                            }
                        }

                        // validate tổng tiền đơn mỗi xe
                        if (false) // bổ sung sau vì a tú chưa truyền
                        {
                            if (item.AMOUNT != amount)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "item.AMOUNT: " + item.AMOUNT + " - " + amount, Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_99), goConstantsValidate.ERR_CHK_99);

                            }
                            if (item.TOTAL_DISCOUNT != total_discount)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "item.TOTAL_DISCOUNT: " + item.TOTAL_DISCOUNT + " - " + total_discount, Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_100), goConstantsValidate.ERR_CHK_100);
                            }
                            if (item.TOTAL_VAT != vat)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "item.TOTAL_VAT: " + item.TOTAL_VAT + " - " + vat, Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_101), goConstantsValidate.ERR_CHK_101);
                            }
                            if (item.TOTAL_AMOUNT != total_amount)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "item.TOTAL_AMOUNT: " + item.TOTAL_AMOUNT + " - " + total_amount, Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_102), goConstantsValidate.ERR_CHK_102);
                            }
                            if (item.TOTAL_ADDITIONAL != total_add)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "item.TOTAL_ADDITIONAL: " + item.TOTAL_ADDITIONAL + " - " + total_add, Logger.ConcatName(nameClass, nameMethod));
                                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_103), goConstantsValidate.ERR_CHK_103);
                            }
                        }
                        
                    }
                }
                return baseValidate;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return goBusinessCommon.Instance.ResValidate(false, nameof(goConstantsValidate.ERR_CHK_31), goConstantsValidate.ERR_CHK_31);
            }
        }

        private void CompareFees(ref BaseValidate baseValidate, double input, double fees, string errCode, string errMess)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (input != fees)
                {
                    baseValidate = goBusinessCommon.Instance.ResValidate(false, errCode, errMess);
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "err: " + errCode, Logger.ConcatName(nameClass, nameMethod));
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        #endregion
    }
}
