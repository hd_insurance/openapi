﻿using System;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.COMMON.Log;
using GO.ENCRYPTION;
using GO.DTO.Common;
using GO.COMMON.Cache;
using GO.HELPER.Utility;
using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.DTO.CalculateFees;
using System.Text.RegularExpressions;
using GO.DTO.Orders;

namespace GO.BUSINESS.Insurance
{
    public class goBusinessCalculateFees
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        private char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        private string charsFull = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private bool bug = true;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessCalculateFees> _instance = new Lazy<goBusinessCalculateFees>(() =>
        {
            return new goBusinessCalculateFees();
        });
        public static goBusinessCalculateFees Instance { get => _instance.Value; }
        #endregion

        #region Method

        public BaseResponse GetResDynamicFees(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                InputDynamicFees inputDynamic = new InputDynamicFees();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFees>(request.Data.ToString());

                if (string.IsNullOrEmpty(inputDynamic.ORG_CODE) || string.IsNullOrEmpty(inputDynamic.CATEGORY) || string.IsNullOrEmpty(inputDynamic.PRODUCT_CODE) ||
                    inputDynamic.DYNAMIC_FEES == null || !inputDynamic.DYNAMIC_FEES.Any())
                    throw new Exception("request k đúng");

                // tính phí
                if (inputDynamic.CATEGORY.Equals(goConstants.CATEGORY_XE))
                {
                    double fees = CalculateFeesInCache(inputDynamic, request);
                    response.Success = true;
                    response.Data = goUtility.Instance.ConvertToInt64(fees, 0);
                }
                //switch (inputDynamic.CATEGORY)
                //{
                //    case (goConstants.CATEGORY_XE.ToString()):
                //        double fees = CalculateFeesInCache(inputDynamic, request);
                //        response.Success = true;
                //        response.Data = fees;
                //        break;
                //}
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_FEES_01), goConstantsError.Instance.ERROR_FEES_01);
            }

            return response;
        }

        public BaseResponse GetResDynamicFeesVer2(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                InputDynamicFeesVer2 inputDynamic = new InputDynamicFeesVer2();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFeesVer2>(request.Data.ToString());

                if (inputDynamic == null || string.IsNullOrEmpty(inputDynamic.ORG_CODE) || inputDynamic.PRODUCT_INFO == null || !inputDynamic.PRODUCT_INFO.Any())
                    throw new Exception("request k đúng");
                OutDynamicFees outFees = GetOutCalFees(inputDynamic, request);
                response = goBusinessCommon.Instance.getResultApi(true, outFees, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_FEES_01), goConstantsError.Instance.ERROR_FEES_01);
            }

            return response;
        }

        #region Calculate Fees In Cache
        public Double CalculateFeesInCache(InputDynamicFees inputDynamic, BaseRequest request)
        {
            double fees = 0;
            try
            {
                string key_init = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY + inputDynamic.PRODUCT_CODE;
                if (!goBusinessCache.Instance.Exists(key_init))
                    InitCachingFees(request, inputDynamic.CHANNEL, inputDynamic.USERNAME, inputDynamic.ORG_CODE, inputDynamic.CATEGORY, inputDynamic.PRODUCT_CODE);

                string ref_formula = goBusinessCache.Instance.Get<string>(key_init);
                string keyCrit = key_init + "Criteria";
                List<CriteriaFees> lstCrit = goBusinessCache.Instance.Get<List<CriteriaFees>>(keyCrit);
                List<DynamicData> lstFees = inputDynamic.DYNAMIC_FEES;

                foreach (var item in lstFees)
                {
                    switch (item.TYPE_CODE)
                    {
                        case nameof(goConstants.TypeValueDynamic.CONSTANT):
                            int ind1 = lstCrit.FindIndex(a => a.CRITERIA_CODE == item.KEY_CODE);
                            if (ind1 >= 0)
                                lstCrit[ind1].VAL_DATA = goUtility.Instance.ConvertToDouble(item.VAL_CODE, 0);
                            break;
                        default:
                            string keyFees = key_init + item.VAL_CODE;// goEncryptBase.Instance.Md5ProviderEncode(item.KEY_CODE);
                            string strVal = goBusinessCache.Instance.Get<string>(keyFees);
                            double val = 0;
                            if (!string.IsNullOrEmpty(strVal))
                                val = goUtility.Instance.ConvertToDouble(strVal, 0);
                            int index = lstCrit.FindIndex(a => a.CRITERIA_CODE == item.KEY_CODE);
                            if (index >= 0)
                                lstCrit[index].VAL_DATA = val;
                            break;
                    }
                }
                if (string.IsNullOrEmpty(ref_formula) || lstCrit == null || !lstCrit.Any())
                    throw new Exception("ref formula not exsist");

                switch (inputDynamic.CATEGORY)
                {
                    case "XE":
                        ref_formula = Regex.Replace(ref_formula, "@STBH@", inputDynamic.STBH);
                        break;
                }
                //List<CriteriaFees> lstCritOrd = lstCrit.OrderBy(x => x.SORTORDER).ToList();
                //string[] pram = new string[lstCritOrd.Count];
                for (var i = 0; i < lstCrit.Count; i++)
                {
                    //pram[i] = chars[i].ToString();
                    lstCrit[i].CHAR = chars[i];
                }
                string formula = ref_formula;
                foreach (var item in lstCrit)
                {
                    formula = Regex.Replace(formula, "@" + item.CRITERIA_CODE + "@", item.CHAR.ToString());
                }
                string formulaPostfix = FormulaPostfix(formula);
                fees = ExeCalculateFees(formulaPostfix, lstCrit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fees;
        }

        public Double CalculateFeesVer2(DynamicProduct inputDynamic, string org_code, string channel, string username, BaseRequest request)
        {
            double fees = 0;
            try
            {
                string key_init = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY + inputDynamic.PRODUCT_CODE;
                if (!goBusinessCache.Instance.Exists(key_init))
                    InitCachingFees(request, channel, username, org_code, inputDynamic.CATEGORY, inputDynamic.PRODUCT_CODE);

                //string key_init_Crit = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY;
                //string key_init_Product = key_init_Crit + inputDynamic.PRODUCT_CODE;

                ProductFormula product = goBusinessCache.Instance.Get<ProductFormula>(key_init);
                string ref_formula = product.REF_FORMULA;

                //string ref_formula = goBussinessCache.Instance.Get<string>(key_init);
                string keyCrit = key_init + "Criteria";
                List<CriteriaFees> lstCrit = goBusinessCache.Instance.Get<List<CriteriaFees>>(keyCrit);
                List<DynamicData> lstFees = inputDynamic.DYNAMIC_FEES;
                DateTime dEff = goUtility.Instance.ConvertToDateTime(inputDynamic.EFF, DateTime.MinValue);
                DateTime dExp = goUtility.Instance.ConvertToDateTime(inputDynamic.EXP, DateTime.MinValue);
                int day = goUtility.Instance.GetDayRange(dEff, dExp, false);

                foreach (var itemDyn in lstFees)
                {
                    if (itemDyn.KEY_CODE.Equals("XCG_TGBH"))
                        itemDyn.VAL_CODE = day.ToString();

                    if (itemDyn.KEY_CODE.Equals("TN_TGBH"))
                        itemDyn.VAL_CODE = day.ToString();

                    if (day < 30)
                    {
                        if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                            itemDyn.VAL_CODE = "1";
                        if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                            itemDyn.VAL_CODE = "0";
                    }
                    else
                    {
                        if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                            itemDyn.VAL_CODE = "0";
                        if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                            itemDyn.VAL_CODE = day.ToString();
                    }
                }

                foreach (var item in lstFees)
                {
                    switch (item.TYPE_CODE)
                    {
                        case nameof(goConstants.TypeValueDynamic.CONSTANT):
                            int ind1 = lstCrit.FindIndex(a => a.CRITERIA_CODE == item.KEY_CODE);
                            if (ind1 >= 0)
                                lstCrit[ind1].VAL_DATA = goUtility.Instance.ConvertToDouble(item.VAL_CODE, 0);
                            break;
                        default:
                            try
                            {
                                string keyFees = key_init + item.VAL_CODE;// goEncryptBase.Instance.Md5ProviderEncode(item.KEY_CODE);
                                string strVal = goBusinessCache.Instance.Get<string>(keyFees);
                                double val = 0;
                                if (!string.IsNullOrEmpty(strVal))
                                    val = goUtility.Instance.ConvertToDouble(strVal, 0);
                                int index = lstCrit.FindIndex(a => a.CRITERIA_CODE == item.KEY_CODE);
                                if (index >= 0)
                                    lstCrit[index].VAL_DATA = val;
                            }
                            catch (Exception ex)
                            {
                            }

                            break;
                    }
                }
                if (string.IsNullOrEmpty(ref_formula) || lstCrit == null || !lstCrit.Any())
                    throw new Exception("ref formula not exsist");

                switch (inputDynamic.CATEGORY)
                {
                    case "XE":
                        ref_formula = Regex.Replace(ref_formula, "@STBH@", inputDynamic.STBH);
                        break;
                }
                for (var i = 0; i < lstCrit.Count; i++)
                {
                    lstCrit[i].CHAR = chars[i];
                }
                string formula = ref_formula;
                foreach (var item in lstCrit)
                {
                    formula = Regex.Replace(formula, "@" + item.CRITERIA_CODE + "@", item.CHAR.ToString());
                }
                string formulaPostfix = FormulaPostfix(formula);
                fees = ExeCalculateFees(formulaPostfix, lstCrit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return fees;
        }

        public OutDynamicFees GetOutCalFees(InputDynamicFeesVer2 inputFees, BaseRequest request)
        {
            string org_code = inputFees.ORG_CODE, channel = inputFees.CHANNEL, username = inputFees.USERNAME;
            try
            {
                if (inputFees.PRODUCT_INFO == null)
                    throw new Exception("null List<DynamicProduct>");

                OutDynamicFees outFees = new OutDynamicFees();
                outFees.PRODUCT_DETAIL = new List<OutDynamicProduct>();

                double discount_all = 0; string discount_all_unit = "";
                if (inputFees.GIFTS != null && inputFees.GIFTS.Any()) // nếu có gift thì lấy ra thông tin gift
                {
                    // gift tổng đơn và gift sản phẩm
                    // gift sản phẩm cụ thể hay gift tất cả sản phẩm
                }

                foreach (var item in inputFees.PRODUCT_INFO)
                {
                    switch (item.CATEGORY)
                    {
                        case var s when item.CATEGORY.Equals(goConstants.CATEGORY_XE):
                            OutDynamicProduct outProduct = outProductVehicle(request, item, inputFees.GIFTS, org_code, channel, username);

                            outFees.AMOUNT += outProduct.AMOUNT;
                            outFees.TOTAL_DISCOUNT += outProduct.TOTAL_DISCOUNT;
                            outFees.VAT += outProduct.VAT;
                            outFees.TOTAL_AMOUNT += outProduct.TOTAL_AMOUNT;
                            outFees.PRODUCT_DETAIL.Add(outProduct);
                            break;
                    }
                }

                return outFees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public OutDynamicProduct outProductVehicle(BaseRequest request, DynamicProduct item, List<GiftInfo> gifts, string org_code, string channel, string username, string curency = "VND")
        {
            OutDynamicProduct outProduct = new OutDynamicProduct();
            outProduct.DISCOUNT_DETAIL = new List<DiscountInfo>();
            try
            {
                double fees_root = 0, total_discount = 0, vat = 0, total_amount = 0, vat_xe = 10;

                // lấy ra phí gốc
                double fees_vat_2 = CalculateFeesVer2(item, org_code, channel, username, request);
                double fees_vat = goUtility.Instance.FeesRound(fees_vat_2, curency);
                fees_root = goUtility.Instance.FeesRound(goUtility.Instance.FeesRootByFeesVat(fees_vat, vat_xe), curency);

                // tính số tiền giảm phí
                double discount_val = 0; double val_dis = 0;
                if (!string.IsNullOrEmpty(item.DISCOUNT) && !item.PRODUCT_CODE.Equals(goConstants.Product_Xe.XCG_TNDSBB))
                {
                    if (item.DISCOUNT_UNIT.Equals(goConstants.UnitMode.P.ToString()))
                    {
                        val_dis = goUtility.Instance.FeesRound(fees_root * goUtility.Instance.ConvertToDouble(item.DISCOUNT, 0) / 100, curency);
                        discount_val = val_dis;
                    }
                    else if (item.DISCOUNT_UNIT.Equals(goConstants.UnitMode.M.ToString()))
                        discount_val = goUtility.Instance.ConvertToDouble(item.DISCOUNT, 0);

                    //if (fees_root <= discount_val) // số tiền <= số giảm => số tiền giảm = số tiền
                    //    discount_val = fees_root;

                    DiscountInfo outDiscount = new DiscountInfo();
                    outDiscount.AMOUNT = fees_root;
                    outDiscount.DISCOUNT_UNIT = item.DISCOUNT_UNIT;
                    outDiscount.DISCOUNT = goUtility.Instance.ConvertToDouble(item.DISCOUNT, 0);
                    outDiscount.TOTAL_DISCOUNT = discount_val;
                    //add
                    outProduct.DISCOUNT_DETAIL.Add(outDiscount);
                }
                double fees_befor_dis = goUtility.Instance.FeesRound(fees_root - discount_val, curency);
                if (fees_befor_dis < 0)
                    fees_befor_dis = 0;

                // tính giảm phí với gift
                double discount_xe = 0; string discount_xe_unit = "";
                double total_discount_gift = 0;
                if (gifts != null && gifts.Any()) // nếu có gift thì lấy ra thông tin gift
                {
                    // gift sản phẩm xe hay gift tất cả sản phẩm
                    // bổ sung sau (lấy ra danh sách gift của xe và tính phí xong add vào out
                    foreach (var item_gift in gifts) // for list gift của xe
                    {
                        //kiểm tra lại với TH nhiều gift (giảm trên tiền lũy kế)

                        //double discount_gift = 0;
                        //if (discount_xe > 0)
                        //{
                        //    if (discount_xe_unit.Equals(goConstants.UnitMode.P.ToString()))
                        //        discount_gift = goUtility.Instance.FeesRound(fees_befor_dis * discount_xe / 100, curency);
                        //    else if (discount_xe_unit.Equals(goConstants.UnitMode.M.ToString()))
                        //        discount_gift = fees_befor_dis - discount_xe;
                        //}
                        //OutDiscount outDiscount = new OutDiscount();
                        //outDiscount.AMOUNT = fees_befor_dis;
                        //outDiscount.DISCOUNT = discount_xe;
                        //outDiscount.DISCOUNT_UNIT = discount_xe_unit;
                        //outDiscount.TOTAL_DISCOUNT = discount_gift;
                        //outDiscount.GIFT_CODE = item_gift.GIFT_CODE;
                        ////add
                        //outProduct.DISCOUNT_DETAIL.Add(outDiscount);
                        //total_discount_gift += discount_gift;
                    }

                }

                // tính phí tổng của sản phẩm
                double fees_befor_all_dis = fees_befor_dis - total_discount_gift;
                if (fees_befor_all_dis < 0)
                    fees_befor_all_dis = 0;

                total_discount = discount_val + total_discount_gift;
                vat = goUtility.Instance.FeesRound(fees_befor_all_dis * vat_xe / 100, curency);
                if (total_discount != 0)
                {
                    total_amount = fees_root - total_discount + vat;

                    outProduct.CATEGORY = item.CATEGORY;
                    outProduct.PRODUCT_CODE = item.PRODUCT_CODE;
                    outProduct.FEES = fees_root; // phí gốc
                    outProduct.AMOUNT = fees_befor_dis;// phí bán
                    outProduct.TOTAL_DISCOUNT = total_discount; // tổng giảm (phí giảm + gift giảm)
                    outProduct.VAT = vat;
                    outProduct.TOTAL_AMOUNT = total_amount;// tổng thanh toán
                }
                else
                {
                    total_amount = fees_vat;

                    outProduct.CATEGORY = item.CATEGORY;
                    outProduct.PRODUCT_CODE = item.PRODUCT_CODE;
                    outProduct.FEES = fees_root; // phí gốc
                    outProduct.AMOUNT = fees_root;// phí bán
                    outProduct.TOTAL_DISCOUNT = 0; // tổng giảm (phí giảm + gift giảm)
                    outProduct.VAT = fees_vat - fees_root; // do không giảm nên vat = giá sau thuế - giá trước thuế
                    outProduct.TOTAL_AMOUNT = total_amount;// tổng thanh toán
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outProduct;
        }

        #endregion

        #region Tính phí ver 3
        public BaseResponse GetResDynamicFeesVer3(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }

                InputDynamicFeesVer3 inputDynamic = new InputDynamicFeesVer3();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFeesVer3>(request.Data.ToString());

                if (string.IsNullOrEmpty(inputDynamic.ORG_CODE) || inputDynamic.PRODUCT_INFO == null || !inputDynamic.PRODUCT_INFO.Any())
                    throw new Exception("request k đúng");
                OutDynamicFeesVer3 outFees = GetOutCalFeesVer3(inputDynamic, request);
                response = goBusinessCommon.Instance.getResultApi(true, outFees, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_FEES_01), goConstantsError.Instance.ERROR_FEES_01);
            }

            return response;
        }

        // nâng ver tính phí
        public OutDynamicFeesVer3 GetOutCalFeesVer3(InputDynamicFeesVer3 inputFees, BaseRequest request)
        {
            string org_code = inputFees.ORG_CODE, channel = inputFees.CHANNEL, username = inputFees.USERNAME;
            try
            {
                if (inputFees.PRODUCT_INFO == null)
                    throw new Exception("null List<DynamicProduct>");

                OutDynamicFeesVer3 outFees = new OutDynamicFeesVer3();
                outFees.PRODUCT_DETAIL = new List<OutDynamicProductVer3>();

                double discount_all = 0; string discount_all_unit = "";
                if (inputFees.GIFTS != null && inputFees.GIFTS.Any()) // nếu có gift thì lấy ra thông tin gift
                {
                    // gift tổng đơn và gift sản phẩm
                    // gift sản phẩm cụ thể hay gift tất cả sản phẩm
                }

                foreach (var item in inputFees.PRODUCT_INFO)
                {
                    switch (item.CATEGORY)
                    {
                        case var s when item.CATEGORY.Equals(goConstants.CATEGORY_XE):
                        case var s1 when item.CATEGORY.Equals(goConstants.CATEGORY_NHATN):
                            // check discount gift của xe
                            OutDynamicProductVer3 outProduct = new OutDynamicProductVer3();
                            List<GiftInfo> giftVehicle = new List<GiftInfo>();
                            outProduct = outProductVehicleVer3(request, item, giftVehicle, org_code, channel, username);

                            outFees.FEES_DATA += outProduct.FEES_DATA;
                            outFees.AMOUNT += outProduct.AMOUNT;
                            outFees.TOTAL_DISCOUNT += outProduct.TOTAL_DISCOUNT;
                            outFees.VAT += outProduct.VAT;
                            outFees.TOTAL_AMOUNT += outProduct.TOTAL_AMOUNT;
                            outFees.PRODUCT_DETAIL.Add(outProduct);
                            break;
                    }
                }

                return outFees;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Vehicle (Xe cơ giới)
        private OutDynamicProductVer3 outProductVehicleVer3(BaseRequest request, DynamicProductVer3 item, List<GiftInfo> gifts, string org_code, string channel, string username, string curency = "VND")
        {
            OutDynamicProductVer3 outProduct = new OutDynamicProductVer3();
            //outProduct.DISCOUNT_DETAIL = new List<DiscountInfo>();
            try
            {
                double fees_root = 0, total_discount = 0, vat = 0, total_amount = 0, vat_xe = 10;
                double disGift_veh = 0; // giảm giá của gift cho sản phẩm XCG hoặc ALL

                // gán thông tin hiện có
                outProduct.INX = item.INX;
                outProduct.CATEGORY = item.CATEGORY;
                outProduct.PRODUCT_CODE = item.PRODUCT_CODE;
                outProduct.OUT_DETAIL = new List<ADDITIONAL_FEES>();
                outProduct.ADDITI_DETAIL = new List<ADDITIONAL_FEES>();

                CalculateFeesVer3(ref outProduct, item, gifts, org_code, channel, username, request);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return outProduct;
        }
        #endregion

        #region Business Calculate
        private void CalculateFeesVer3(ref OutDynamicProductVer3 outProduct, DynamicProductVer3 inputDynamic, List<GiftInfo> gifts, string org_code, string channel, string username, BaseRequest request)
        {
            double fees = 0;
            double totalAdditi = 0;
            try
            {
                // lấy thông tin trong cache về thông tin công thức fees
                //string key_init = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY;
                string key_init_Crit = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY;
                string key_init_Product = key_init_Crit + inputDynamic.PRODUCT_CODE;
                if (!goBusinessCache.Instance.Exists(key_init_Product))
                    InitCachingFees(request, channel, username, org_code, inputDynamic.CATEGORY, inputDynamic.PRODUCT_CODE);

                // từ org con lấy ra org cha
                List<SysOrgInfo> lstOrg = goBusinessCache.Instance.Get<List<SysOrgInfo>>(goConstantsRedis.OrgKey);
                SysOrgInfo OrgRoot = goBusinessCommon.Instance.GetOrgRoot(lstOrg, org_code).FirstOrDefault();
                if (OrgRoot != null)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "CalculateFeesVer3 org con lấy ra org cha - old: " + org_code + " new: " + OrgRoot.org_code, Logger.ConcatName(nameClass, "CalculateFeesVer3"));
                    org_code = OrgRoot.org_code;
                }

                ProductFormula product = goBusinessCache.Instance.Get<ProductFormula>(key_init_Product);

                string ref_formula = product.REF_FORMULA;//goBussinessCache.Instance.Get<string>(key_init);
                //string keyCrit = key_init_Crit + goConstantsRedis.PrefixCriteria; //key_init + "Criteria";
                string keyCrit = key_init_Product + goConstantsRedis.PrefixCriteria;
                string keyCritDetail = key_init_Product + goConstantsRedis.PrefixCriteriaDetail;
                string keyFeesMin = key_init_Product + goConstantsRedis.PrefixFeesMin;
                // danh sách tiêu chí của sản phẩm
                List<CriteriaFees> lstCrit = goBusinessCache.Instance.Get<List<CriteriaFees>>(keyCrit);
                // Lấy ra danh sách tiêu chí chi tiết
                List<CriteriaFeesDetail> lstCritDetail = goBusinessCache.Instance.Get<List<CriteriaFeesDetail>>(keyCritDetail);
                // Danh sách phí set min
                List<CriteriaFeesMin> lstFeesMin = goBusinessCache.Instance.Get<List<CriteriaFeesMin>>(keyFeesMin);

                if (lstCrit == null || !lstCrit.Any())
                    throw new Exception(goConstantsError.Instance.ERROR_FEES_02);

                // gán danh sách dynamic fees vào list để tính phí
                List<DynamicData> lstFees = inputDynamic.DYNAMIC_FEES;
                //2022012-18 KhanhPT bổ sung Update key phí tính ngày theo nguyên tắc 365 ngày
                if (inputDynamic.EFF != null && inputDynamic.EXP != null)
                {
                    DateTime dEff = goUtility.Instance.ConvertToDateTime(inputDynamic.EFF, DateTime.MinValue);
                    DateTime dExp = goUtility.Instance.ConvertToDateTime(inputDynamic.EXP, DateTime.MinValue);
                    int day = goUtility.Instance.GetDayRange(dEff, dExp, false);
                    foreach (var itemDyn in lstFees)
                    {
                        if (itemDyn.KEY_CODE.Equals("XCG_TGBH"))
                            itemDyn.VAL_CODE = day.ToString();

                        if (day < 30)
                        {
                            if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                                itemDyn.VAL_CODE = "1";
                            if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                                itemDyn.VAL_CODE = "0";
                        }
                        else
                        {
                            if (itemDyn.KEY_CODE.Equals("HS_D30D"))
                                itemDyn.VAL_CODE = "0";
                            if (itemDyn.KEY_CODE.Equals("HS_T30D"))
                                itemDyn.VAL_CODE = day.ToString();
                        }
                    }
                }
                //End

                // lấy ra danh sách tiêu chí của công thức gốc
                List<CriteriaFees> lstRoot = lstCrit.Where(x => x.IS_ROOT == "1").ToList();



                // từ input List<DynamicData> lstFees kiểm tra trong List<CriteriaFees> lstRoot nếu có bổ sung và là multi thì tạo ra List<DynamicData> lstFees input mới đem đi tính toán
                List<DynamicData> lstFeesNew = new List<DynamicData>();

                for (var i = 0; i < lstRoot.Count(); i++)
                {
                    CriteriaFees item = lstRoot[i];
                    GetDynamicData(ref lstFeesNew, item, lstCrit, lstCritDetail, lstFees);
                }

                //group lại với trường hợp trùng
                lstFeesNew = lstFeesNew.GroupBy(x => new { x.KEY_CODE, x.VAL_CODE, x.TYPE_CODE }).Select(g => new DynamicData() { KEY_CODE = g.Key.KEY_CODE, VAL_CODE = g.Key.VAL_CODE, TYPE_CODE = g.Key.TYPE_CODE }).ToList();
                outProduct.DISCOUNT_DETAIL = new List<DiscountInfo>();
                // lấy data từ công thức
                foreach (var item in lstRoot)
                {
                    CriteriaFees critOut = LoopCriteria(ref outProduct, inputDynamic, gifts, item, lstCrit, lstCritDetail, lstFeesNew, key_init_Product, org_code, channel, lstFeesMin);
                    item.VAL_DATA = critOut.VAL_DATA;
                    item.FEES_DATA = critOut.FEES_DATA;
                    item.FEES = critOut.FEES;
                    item.AMOUNT = critOut.AMOUNT;
                    item.TOTAL_DISCOUNT = critOut.TOTAL_DISCOUNT;
                    item.VAT = critOut.VAT;
                    item.TOTAL_AMOUNT = critOut.TOTAL_AMOUNT;

                    #region 05/07/2021 comment sai
                    // thông tin phí của node gốc
                    /*bool IncludeVat = goUtility.Instance.CastToBoolean(critOut.INCLUDE_VAT);
                    bool isDiscount = goUtility.Instance.CastToBoolean(critOut.IS_DISCOUNT);
                    ProductFeesInfo feesInfo = new ProductFeesInfo();
                    if (!string.IsNullOrEmpty(inputDynamic.TYPE_DISCOUNT) && inputDynamic.TYPE_DISCOUNT.Equals(critOut.TYPE_DISCOUNT))
                        feesInfo = GetFeesInfo(critOut.CRITERIA_CODE, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts, critOut.TYPE_FEES);
                    else
                        feesInfo = GetFeesInfo(critOut.CRITERIA_CODE, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, 0, "P", gifts, critOut.TYPE_FEES);
                    if (!critOut.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.CONSTANT)) || (item.MODE_FEES != null && item.MODE_FEES.Equals(goConstants.UnitMode.M)))
                    {
                        item.FEES_DATA = feesInfo.FEES_DATA;
                        item.FEES = feesInfo.FEES_ROOT;
                        item.AMOUNT = feesInfo.FEES_BUY;
                        item.TOTAL_DISCOUNT = feesInfo.TOTAL_DISCOUNT;
                        item.VAT = feesInfo.VAT;
                        item.TOTAL_AMOUNT = feesInfo.TOTAL_AMOUNT;
                        //if(feesInfo.DISCOUNT_DETAIL != null && feesInfo.DISCOUNT_DETAIL.Any())
                        //{
                        //    List<DiscountInfo> lstS = feesInfo.DISCOUNT_DETAIL.Where(x => x.TOTAL_DISCOUNT > 0).ToList();
                        //    //gán thêm vào list cha
                        //    outProduct.DISCOUNT_DETAIL.AddRange(lstS);
                        //}
                    }*/
                    #endregion

                    if (critOut.TYPE_FEES.Equals(nameof(goConstants.TypeFeesDynamic.BSUNG)))
                    {
                        // lấy gift của sản phẩm
                        //ProductFeesInfo feesInfo = GetProductFeesInfo(critOut, inputDynamic, gifts);
                        //bool IncludeVat = goUtility.Instance.CastToBoolean(critOut.INCLUDE_VAT);
                        //bool isDiscount = goUtility.Instance.CastToBoolean(critOut.IS_DISCOUNT);
                        //ProductFeesInfo feesInfo = GetFeesInfo(critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts);
                        totalAdditi += critOut.TOTAL_AMOUNT;
                    }
                }

                //for (var i = 0; i < lstRoot.Count(); i++)
                //{
                //    CriteriaFees item = lstRoot[i];
                //    CriteriaFees critOut = LoopCriteria(ref outProduct, inputDynamic, gifts, item, lstCrit, lstCritDetail, lstFeesNew, key_init_Product);
                //    lstRoot[i].VAL_DATA = critOut.VAL_DATA;

                //    if (critOut.TYPE_FEES.Equals(nameof(goConstants.TypeFeesDynamic.BSUNG)))
                //    {
                //        // lấy gift của sản phẩm
                //        //ProductFeesInfo feesInfo = GetProductFeesInfo(critOut, inputDynamic, gifts);
                //        bool IncludeVat = goUtility.Instance.CastToBoolean(critOut.INCLUDE_VAT);
                //        bool isDiscount = goUtility.Instance.CastToBoolean(critOut.IS_DISCOUNT);
                //        ProductFeesInfo feesInfo = GetFeesInfo(critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts);
                //        totalAdditi += feesInfo.TOTAL_AMOUNT;
                //    }
                //}

                if (string.IsNullOrEmpty(ref_formula) || lstRoot == null || !lstRoot.Any())
                    throw new Exception("ref formula not exsist");

                for (var i = 0; i < lstRoot.Count; i++)
                {
                    lstRoot[i].CHAR = chars[i];
                }
                string formula = ref_formula;
                foreach (var item in lstRoot)
                {
                    formula = Regex.Replace(formula, "@" + item.CRITERIA_CODE + "@", item.CHAR.ToString());
                }
                string formulaPostfix = FormulaPostfix(formula);

                string curency = "VND";
                #region Chuyển qua tính tổng với hàm cuối (TH 1 nửa giảm)
                // đã gồm giá (gốc hoặc bán có thể có vat) + các điều khoản bổ sung (đã tính giảm theo discount và gift)
                /*fees = ExeCalculateFees(formulaPostfix, lstRoot);
                // lấy lại giá gốc, giá bán
                bool IncludeVat2 = goUtility.Instance.CastToBoolean(product.INCLUDE_VAT);
                string[] arrExitst = new[] { "XCG_TNDSBB" };
                ProductFeesInfo feesInfo2 = new ProductFeesInfo();
                if (product.CATEGORY.Equals("XE") && Array.Exists(arrExitst, i => i.Equals(product.PRODUCT_CODE)))
                    feesInfo2 = GetFeesInfo(fees, product.VAT_RATIO, IncludeVat2, false, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts);
                else
                    feesInfo2 = GetFeesInfo(fees, product.VAT_RATIO, IncludeVat2, true, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts);
                outProduct.FEES = feesInfo2.FEES_ROOT;
                outProduct.AMOUNT = feesInfo2.FEES_BUY;
                outProduct.TOTAL_DISCOUNT = feesInfo2.TOTAL_DISCOUNT;
                outProduct.TOTAL_ADD = goUtility.Instance.FeesRound(totalAdditi, curency);
                outProduct.VAT = feesInfo2.VAT;
                outProduct.VAT_RATIO = feesInfo2.VAT_RATIO;
                outProduct.TOTAL_AMOUNT = feesInfo2.TOTAL_AMOUNT;
                outProduct.DISCOUNT_DETAIL = feesInfo2.DISCOUNT_DETAIL;*/
                #endregion
                outProduct.FEES_DATA = ExeCalculate_V2_FeesData(formulaPostfix, lstRoot);
                outProduct.FEES = ExeCalculate_V2_FeesRoot(formulaPostfix, lstRoot);
                outProduct.AMOUNT = ExeCalculate_V2_FeesBuy(formulaPostfix, lstRoot);
                outProduct.TOTAL_DISCOUNT = ExeCalculate_V2_TotalDiscount(formulaPostfix, lstRoot);
                outProduct.TOTAL_ADD = goUtility.Instance.FeesRound(totalAdditi, curency);
                outProduct.VAT = ExeCalculate_V2_Vat(formulaPostfix, lstRoot);
                //outProduct.VAT_RATIO = feesInfo2.VAT_RATIO;
                outProduct.TOTAL_AMOUNT = ExeCalculate_V2_TotalAmount(formulaPostfix, lstRoot);
                //outProduct.DISCOUNT_DETAIL = feesInfo2.DISCOUNT_DETAIL;


                #region comment old

                /*
                List<DiscountInfo> discountDetail = new List<DiscountInfo>();

                double feesInVat = 0, feesRoot = 0, feesBuy = 0, total_discount = 0, vat = 0, total_amount = 0, vat_ratio = 0;
                string curency = "VND";

                // Trường hợp không có vat => phí gồm vat
                if (product.VAT_RATIO == null || product.VAT_RATIO == 0)
                {
                    // không vat => phí gồm vat = data
                    feesInVat = fees;
                    // lấy phí gốc = feesInVat - totalAdditi
                    feesRoot = feesInVat - totalAdditi;

                    // nếu có giảm giá => giá bán = giá gốc sau giảm
                    if (inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0)
                    {
                        feesBuy = feesRoot;
                        total_discount = 0;
                    }
                    else // bổ sung sau -- 12/05/2021
                    {
                        if (bug)
                        {
                            if (inputDynamic.DISCOUNT != null && inputDynamic.DISCOUNT != 0 && !string.IsNullOrEmpty(inputDynamic.DISCOUNT_UNIT))
                            {
                                double disDetail = 0;
                                DiscountInfo disInfo = new DiscountInfo();
                                disDetail = GetTotalDiscount(feesRoot, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, curency);

                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = inputDynamic.DISCOUNT;
                                disInfo.DISCOUNT_UNIT = inputDynamic.DISCOUNT_UNIT;
                                disInfo.TOTAL_DISCOUNT = disDetail;

                                discountDetail.Add(disInfo);
                                // gán lại phí bán và tổng giảm
                                feesBuy = feesRoot - disDetail;
                                total_discount += disDetail;
                            }
                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau
                    if (gifts != null && gifts.Any())
                    {
                    }
                    vat = 0;
                    total_amount = feesInVat;
                }
                else
                {
                    double ratioVat = goUtility.Instance.ConvertToDoubleFees(product.VAT_RATIO);
                    vat_ratio = ratioVat;
                    if (goUtility.Instance.CastToBoolean(product.INCLUDE_VAT))
                    {
                        feesInVat = goUtility.Instance.FeesRound(fees, curency);
                        feesRoot = goUtility.Instance.FeesRound(goUtility.Instance.FeesRootByFeesVat(feesInVat, ratioVat), curency);

                        if (inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0)
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;

                        }
                        else // bổ sung sau
                        {
                            if (bug)
                            {
                                if (inputDynamic.DISCOUNT != null && inputDynamic.DISCOUNT != 0 && !string.IsNullOrEmpty(inputDynamic.DISCOUNT_UNIT))
                                {
                                    double disDetail = 0;
                                    DiscountInfo disInfo = new DiscountInfo();
                                    disDetail = GetTotalDiscount(feesRoot, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, curency);

                                    disInfo.AMOUNT = feesRoot;
                                    disInfo.DISCOUNT = inputDynamic.DISCOUNT;
                                    disInfo.DISCOUNT_UNIT = inputDynamic.DISCOUNT_UNIT;
                                    disInfo.TOTAL_DISCOUNT = disDetail;

                                    discountDetail.Add(disInfo);
                                    // gán lại phí bán và tổng giảm
                                    feesBuy = feesRoot - disDetail;
                                    total_discount += disDetail;
                                }
                            }
                            else
                            {
                                feesBuy = feesRoot;
                                total_discount = 0;
                            }
                        }
                        // bổ sung sau
                        if (gifts != null || gifts.Any())
                        {
                            DiscountInfo disInfo = new DiscountInfo();
                            feesBuy = feesRoot;
                            total_discount = 0;

                            disInfo.GIFT_CODE = "";
                            discountDetail.Add(disInfo);
                        }

                        // nếu không có giảm giá => vat = feesInVat - feesRoot => tránh lệch phí
                        if ((inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0) && !(gifts != null && gifts.Any()))
                        {
                            vat = feesInVat - feesRoot;
                            total_amount = feesInVat;
                            if (feesInVat < 0)
                            {
                                throw new Exception("Không tìm ra phí json: " + JsonConvert.SerializeObject(inputDynamic));
                            }
                        }
                        else //bổ sung sau
                        {

                        }
                    }
                    else //bổ sung sau
                    {
                        feesRoot = goUtility.Instance.ConvertToDoubleFees(product.VAT_RATIO);

                    }
                }

                outProduct.FEES = goUtility.Instance.FeesRound(feesRoot, curency);
                outProduct.AMOUNT = goUtility.Instance.FeesRound(feesBuy, curency);
                outProduct.TOTAL_DISCOUNT = goUtility.Instance.FeesRound(total_discount, curency);
                outProduct.TOTAL_ADD = goUtility.Instance.FeesRound(totalAdditi, curency);
                outProduct.VAT = goUtility.Instance.FeesRound(vat, curency);
                outProduct.VAT_RATIO = goUtility.Instance.FeesRound(vat_ratio, curency);
                outProduct.TOTAL_AMOUNT = goUtility.Instance.FeesRound(total_amount, curency);
                outProduct.DISCOUNT_DETAIL = discountDetail;
                */

                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return fees;
        }
        /// <summary>
        /// Lặp tiêu chí
        /// </summary>
        /// <param name="outProduct">thông tin sản phẩm trả về</param>
        /// <param name="critInput">tiêu chí đầu vào</param>
        /// <param name="lstCrit">Danh sách tiêu chí của sản phẩm</param>
        /// <param name="lstFees">Danh sách tiêu chí client gửi lên</param>
        /// <param name="key_init">Key prefix cua tiêu chí</param>
        private CriteriaFees LoopCriteria(ref OutDynamicProductVer3 outProduct, DynamicProductVer3 inputDynamic, List<GiftInfo> gifts, CriteriaFees critInput, List<CriteriaFees> lstCrit, List<CriteriaFeesDetail> lstCritDetail, List<DynamicData> lstFeesNew, string key_init, string org_code, string channel, List<CriteriaFeesMin> lstFeesMin, string curency = "VND")
        {
            CriteriaFees critOut = new CriteriaFees();
            try
            {
                // gán tiêu chi out = in
                critOut = critInput;

                // check loại phí
                // 1. Nếu là Bổ sung thì check
                // 1.1. Nếu là multi thì split '@' => arr tiêu chí => đệ quy lại đến khi ra data

                int inx = -1;
                switch (critOut.TYPE_CRITERIA)
                {
                    // nếu tiếp tục là công thức thì lặp lại
                    case (nameof(goConstants.TypeValueDynamic.FORMULA)):
                        // lấy ra công thức
                        var formula = critOut.REF_FORMULA;
                        // lấy ra danh sách tiêu chí con
                        List<CriteriaFeesDetail> lstChild = lstCritDetail.Where(x => x.PARENT_CODE == critOut.CRITERIA_CODE).ToList();
                        List<CriteriaFees> lstCritChild = new List<CriteriaFees>();
                        foreach (var it in lstChild)
                        {
                            List<CriteriaFees> lstC = lstCrit.Where(x => x.CRITERIA_CODE == it.CRITERIA_CODE).ToList();
                            lstCritChild.AddRange(lstC);
                        }

                        //lstCritDetail.Where(x => x.PARENT_CODE == critOut.CRITERIA_CODE).ToList();
                        if (lstCritChild != null && lstCritChild.Any())
                        {
                            foreach (var item in lstCritChild)
                            {
                                CriteriaFees oCriteria = LoopCriteria(ref outProduct, inputDynamic, gifts, item, lstCrit, lstCritDetail, lstFeesNew, key_init, org_code, channel, lstFeesMin);
                                item.VAL_DATA = oCriteria.VAL_DATA;

                                bool IncludeVat = goUtility.Instance.CastToBoolean(item.INCLUDE_VAT);
                                bool isDiscount = goUtility.Instance.CastToBoolean(item.IS_DISCOUNT);
                                ProductFeesInfo feesInfo = new ProductFeesInfo();
                                if (!string.IsNullOrEmpty(inputDynamic.TYPE_DISCOUNT) && inputDynamic.TYPE_DISCOUNT.Equals(item.TYPE_DISCOUNT))
                                    feesInfo = GetFeesInfo(item.DISPLAY_CODE/*critOut.CRITERIA_CODE*/, item.VAL_DATA, item.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts, item.TYPE_FEES);
                                else
                                    feesInfo = GetFeesInfo(item.DISPLAY_CODE/*critOut.CRITERIA_CODE*/, item.VAL_DATA, item.VAT_RATIO, IncludeVat, isDiscount, 0, "P", gifts, item.TYPE_FEES);
                                if (item.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (item.MODE_FEES != null && item.MODE_FEES.Equals(goConstants.UnitMode.M)))
                                {
                                    item.FEES_DATA = feesInfo.FEES_DATA;
                                    item.FEES = feesInfo.FEES_ROOT;
                                    item.AMOUNT = feesInfo.FEES_BUY;
                                    item.TOTAL_DISCOUNT = feesInfo.TOTAL_DISCOUNT;
                                    item.VAT = feesInfo.VAT;
                                    item.TOTAL_AMOUNT = feesInfo.TOTAL_AMOUNT;
                                    item.DISCOUNT_DETAIL = feesInfo.DISCOUNT_DETAIL;
                                }
                            }
                        }
                        // đem đi tính toán
                        for (var i = 0; i < lstCritChild.Count; i++)
                        {
                            lstCritChild[i].CHAR = chars[i];
                        }
                        foreach (var item in lstCritChild)
                        {
                            formula = Regex.Replace(formula, "@" + item.CRITERIA_CODE + "@", item.CHAR.ToString());
                        }
                        string formulaPostfix = FormulaPostfix(formula);
                        critOut.VAL_DATA = ExeCalculateFees(formulaPostfix, lstCritChild);

                        // nếu tiêu chí có set min => gán lại val_data nếu min
                        if (goUtility.Instance.CastToBoolean(critOut.IS_MIN))
                        {
                            SetFeesMin(ref critOut, org_code, channel, lstFeesMin);
                        }

                        if (critOut.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (critOut.MODE_FEES != null && critOut.MODE_FEES.Equals(goConstants.UnitMode.M)))
                        {
                            int childForm = 0;
                            childForm = lstCritChild.Where(x => x.TYPE_CRITERIA.Equals(goConstants.TypeValueDynamic.FORMULA.ToString())).Count();
                            if (childForm > 0)
                            {
                                critOut.FEES_DATA = ExeCalculate_V2_FeesData(formulaPostfix, lstCritChild);
                                critOut.FEES = ExeCalculate_V2_FeesRoot(formulaPostfix, lstCritChild);
                                critOut.AMOUNT = ExeCalculate_V2_FeesBuy(formulaPostfix, lstCritChild);
                                critOut.TOTAL_DISCOUNT = ExeCalculate_V2_TotalDiscount(formulaPostfix, lstCritChild);
                                critOut.VAT = ExeCalculate_V2_Vat(formulaPostfix, lstCritChild);
                                critOut.TOTAL_AMOUNT = ExeCalculate_V2_TotalAmount(formulaPostfix, lstCritChild);
                            }
                            else
                            {
                                bool IncludeVat = goUtility.Instance.CastToBoolean(critOut.INCLUDE_VAT);
                                bool isDiscount = goUtility.Instance.CastToBoolean(critOut.IS_DISCOUNT);
                                ProductFeesInfo feesInfo = new ProductFeesInfo();
                                if (!string.IsNullOrEmpty(inputDynamic.TYPE_DISCOUNT) && inputDynamic.TYPE_DISCOUNT.Equals(critOut.TYPE_DISCOUNT))
                                    feesInfo = GetFeesInfo(critOut.DISPLAY_CODE/*critOut.CRITERIA_CODE*/, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts, critOut.TYPE_FEES);
                                else
                                    feesInfo = GetFeesInfo(critOut.DISPLAY_CODE/*critOut.CRITERIA_CODE*/, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, 0, "P", gifts, critOut.TYPE_FEES);

                                critOut.FEES_DATA = feesInfo.FEES_DATA;
                                critOut.FEES = feesInfo.FEES_ROOT;
                                critOut.AMOUNT = feesInfo.FEES_BUY;
                                critOut.TOTAL_DISCOUNT = feesInfo.TOTAL_DISCOUNT;
                                critOut.VAT = feesInfo.VAT;
                                critOut.TOTAL_AMOUNT = feesInfo.TOTAL_AMOUNT;
                                critOut.DISCOUNT_DETAIL = feesInfo.DISCOUNT_DETAIL;
                            }
                        }

                        break;
                    // nếu lấy từ cache thì vào cache lấy data
                    case (nameof(goConstants.TypeValueDynamic.CACHE)):
                        inx = lstFeesNew.FindIndex(a => a.KEY_CODE == critOut.CRITERIA_CODE && a.TYPE_CODE == critOut.TYPE_CRITERIA);
                        if (inx >= 0)
                        {
                            DynamicData dataFees = lstFeesNew[inx];
                            string val_key = string.IsNullOrEmpty(dataFees.VAL_CODE) ? "_-1NULL" : dataFees.VAL_CODE;
                            string key = key_init + val_key;
                            string dataCache = goBusinessCache.Instance.Get<string>(key);
                            critOut.VAL_DATA = goUtility.Instance.ConvertToDoubleFees(dataCache);
                        }
                        else
                        {
                            // nêu k có trong list truyền lên thì gán = 0
                            critOut.VAL_DATA = 0;
                        }
                        break;
                    // nếu là constant thì gán giá trị truyền lên = giá trị tiêu chí
                    case (nameof(goConstants.TypeValueDynamic.CONSTANT)):
                        inx = lstFeesNew.FindIndex(a => a.KEY_CODE == critOut.CRITERIA_CODE && a.TYPE_CODE == critOut.TYPE_CRITERIA);
                        if (inx >= 0)
                        {
                            DynamicData dataFees = lstFeesNew[inx];
                            if (goUtility.Instance.ExistsDouble(dataFees.VAL_CODE))
                                critOut.VAL_DATA = goUtility.Instance.ConvertToDoubleFees(dataFees.VAL_CODE);
                            else
                                critOut.VAL_DATA = 0;
                        }
                        else
                        {
                            // nêu k có trong list truyền lên thì gán = 0
                            critOut.VAL_DATA = 0;
                        }
                        break;
                }


                // kiểm tra tiêu chí có cần out ra ngoài không
                // kiểm tra nếu tiêu chí là bổ sung thì trả về để tính
                if (goUtility.Instance.CastToBoolean(critOut.IS_OUT))// || critOut.TYPE_FEES.Equals(nameof(goConstants.TypeFeesDynamic.BSUNG)))
                {

                    #region Old
                    // lấy gift của sản phẩm
                    //ProductFeesInfo feesInfo = GetProductFeesInfo(critOut, inputDynamic, gifts);
                    /*bool IncludeVat = goUtility.Instance.CastToBoolean(critOut.INCLUDE_VAT);
                    bool isDiscount = goUtility.Instance.CastToBoolean(critOut.IS_DISCOUNT);
                    ProductFeesInfo feesInfo = new ProductFeesInfo();
                    if (!string.IsNullOrEmpty(inputDynamic.TYPE_DISCOUNT) && inputDynamic.TYPE_DISCOUNT.Equals(critOut.TYPE_DISCOUNT))
                        feesInfo = GetFeesInfo(critOut.DISPLAY_CODE, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, gifts, critOut.TYPE_FEES);
                    else
                        feesInfo = GetFeesInfo(critOut.DISPLAY_CODE, critOut.VAL_DATA, critOut.VAT_RATIO, IncludeVat, isDiscount, 0, "P", gifts, critOut.TYPE_FEES);

                    ADDITIONAL_FEES additi = new ADDITIONAL_FEES();
                    additi.CODE = critOut.DISPLAY_CODE;
                    additi.NAME = critOut.CRITERIA_NAME;
                    additi.FEES_DATA = feesInfo.FEES_DATA;
                    additi.FEES = feesInfo.FEES_ROOT;
                    additi.AMOUNT = feesInfo.FEES_BUY;
                    additi.VAT = feesInfo.VAT;
                    additi.VAT_RATIO = feesInfo.VAT_RATIO;
                    additi.TOTAL_DISCOUNT = feesInfo.TOTAL_DISCOUNT;
                    additi.TOTAL_AMOUNT = feesInfo.TOTAL_AMOUNT;

                    if (feesInfo.DISCOUNT_DETAIL != null && feesInfo.DISCOUNT_DETAIL.Any())
                    {
                        List<DiscountInfo> lstS = feesInfo.DISCOUNT_DETAIL.Where(x => x.TOTAL_DISCOUNT > 0).ToList();
                        additi.DISCOUNT_DETAIL = lstS;
                        //gán thêm vào list cha
                        outProduct.DISCOUNT_DETAIL.AddRange(lstS);
                    }*/
                    #endregion
                    ADDITIONAL_FEES additi = new ADDITIONAL_FEES();
                    additi.CODE = critOut.DISPLAY_CODE;
                    additi.NAME = critOut.CRITERIA_NAME;
                    additi.FEES_DATA = critOut.FEES_DATA;
                    additi.FEES = critOut.FEES;
                    additi.AMOUNT = critOut.AMOUNT;
                    additi.VAT = critOut.VAT;
                    additi.VAT_RATIO = critOut.VAT_RATIO;
                    additi.TOTAL_DISCOUNT = critOut.TOTAL_DISCOUNT;
                    additi.TOTAL_AMOUNT = critOut.TOTAL_AMOUNT;

                    if (critOut.DISCOUNT_DETAIL != null && critOut.DISCOUNT_DETAIL.Any())
                    {
                        List<DiscountInfo> lstS = critOut.DISCOUNT_DETAIL.Where(x => x.TOTAL_DISCOUNT > 0).ToList();
                        additi.DISCOUNT_DETAIL = lstS;
                        //gán thêm vào list cha
                        outProduct.DISCOUNT_DETAIL.AddRange(lstS);
                    }
                    if (!critOut.TYPE_FEES.Equals(nameof(goConstants.TypeFeesDynamic.BSUNG)))
                        outProduct.OUT_DETAIL.Add(additi);

                    if (critOut.TYPE_FEES.Equals(nameof(goConstants.TypeFeesDynamic.BSUNG)))
                        outProduct.ADDITI_DETAIL.Add(additi);

                }

                return critOut;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private ProductFeesInfo GetProductFeesInfo(CriteriaFees crit, DynamicProductVer3 inputDynamic, List<GiftInfo> gifts, string curency = "VND")
        {
            ProductFeesInfo feesInfo = new ProductFeesInfo();
            try
            {
                List<DiscountInfo> discountDetail = new List<DiscountInfo>();

                double feesInVat = 0, feesRoot = 0, feesBuy = 0, total_discount = 0, vat = 0, total_amount = 0, vat_ratio = 0;

                if (crit.VAT_RATIO == null || crit.VAT_RATIO == 0)
                {
                    // không vat => phí gốc = data
                    feesInVat = feesRoot = crit.VAL_DATA;
                    if (inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0)
                    {
                        feesBuy = feesRoot;
                        total_discount = 0;
                    }
                    else // bổ sung sau -- 12/05/2021
                    {
                        if (bug)
                        {
                            if (inputDynamic.DISCOUNT != null && inputDynamic.DISCOUNT != 0 && !string.IsNullOrEmpty(inputDynamic.DISCOUNT_UNIT))
                            {
                                double disDetail = 0;
                                DiscountInfo disInfo = new DiscountInfo();
                                if (!goUtility.Instance.CastToBoolean(crit.IS_DISCOUNT))
                                {
                                    disInfo.AMOUNT = feesRoot;
                                    disInfo.DISCOUNT = 0;
                                    disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                    disInfo.TOTAL_DISCOUNT = 0;
                                }
                                if (goUtility.Instance.CastToBoolean(crit.IS_DISCOUNT))
                                {
                                    disDetail = GetTotalDiscount(feesRoot, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, curency);

                                    disInfo.AMOUNT = feesRoot;
                                    disInfo.DISCOUNT = inputDynamic.DISCOUNT;
                                    disInfo.DISCOUNT_UNIT = inputDynamic.DISCOUNT_UNIT;
                                    disInfo.TOTAL_DISCOUNT = disDetail;
                                }
                                discountDetail.Add(disInfo);
                                // gán lại phí bán và tổng giảm
                                feesBuy = feesRoot - disDetail;
                                total_discount += disDetail;
                            }
                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau
                    if (gifts != null && gifts.Any())
                    {
                    }
                    vat = 0;
                    total_amount = feesInVat;
                }
                else
                {
                    double ratioVat = goUtility.Instance.ConvertToDoubleFees(crit.VAT_RATIO);
                    vat_ratio = ratioVat;
                    if (goUtility.Instance.CastToBoolean(crit.INCLUDE_VAT))
                    {
                        feesInVat = goUtility.Instance.FeesRound(crit.VAL_DATA, curency);
                        feesRoot = goUtility.Instance.FeesRound(goUtility.Instance.FeesRootByFeesVat(feesInVat, ratioVat), curency);

                        if (inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0)
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;

                        }
                        else // bổ sung sau -- 12/05/2021
                        {
                            if (bug)
                            {
                                if (inputDynamic.DISCOUNT != null && inputDynamic.DISCOUNT != 0 && !string.IsNullOrEmpty(inputDynamic.DISCOUNT_UNIT))
                                {
                                    double disDetail = 0;
                                    DiscountInfo disInfo = new DiscountInfo();
                                    if (!goUtility.Instance.CastToBoolean(crit.IS_DISCOUNT))
                                    {
                                        disInfo.AMOUNT = feesRoot;
                                        disInfo.DISCOUNT = 0;
                                        disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                        disInfo.TOTAL_DISCOUNT = 0;
                                    }
                                    if (goUtility.Instance.CastToBoolean(crit.IS_DISCOUNT))
                                    {
                                        disDetail = GetTotalDiscount(feesRoot, inputDynamic.DISCOUNT, inputDynamic.DISCOUNT_UNIT, curency);

                                        disInfo.AMOUNT = feesRoot;
                                        disInfo.DISCOUNT = inputDynamic.DISCOUNT;
                                        disInfo.DISCOUNT_UNIT = inputDynamic.DISCOUNT_UNIT;
                                        disInfo.TOTAL_DISCOUNT = disDetail;
                                    }
                                    discountDetail.Add(disInfo);
                                    // gán lại phí bán và tổng giảm
                                    feesBuy = feesRoot - disDetail;
                                    total_discount += disDetail;
                                }
                            }
                            else
                            {
                                feesBuy = feesRoot;
                                total_discount = 0;
                            }
                        }

                        // bổ sung sau
                        if (gifts != null && gifts.Any())
                        {
                            DiscountInfo disInfo = new DiscountInfo();
                            feesBuy = feesRoot;
                            total_discount = 0;

                            disInfo.GIFT_CODE = "";
                            discountDetail.Add(disInfo);
                        }

                        // nếu không có giảm giá => vat = feesInVat - feesRoot => tránh lệch phí
                        if ((inputDynamic.DISCOUNT == null || inputDynamic.DISCOUNT == 0) && !(gifts != null && gifts.Any()))
                        {
                            vat = feesInVat - feesRoot;
                            total_amount = feesInVat;
                            if (feesInVat < 0)
                            {
                                throw new Exception("Không tìm ra phí json: " + JsonConvert.SerializeObject(inputDynamic));
                            }
                        }
                        else //bổ sung sau
                        {
                            vat = (feesRoot - total_discount) * ratioVat / 100;
                            total_amount = (feesRoot - total_discount) + vat;
                        }
                    }
                    else //bổ sung sau
                    {
                        feesRoot = goUtility.Instance.ConvertToDoubleFees(crit.VAT_RATIO);

                    }
                }

                feesInfo.FEES_ROOT = goUtility.Instance.FeesRound(feesRoot, curency);
                feesInfo.FEES_BUY = goUtility.Instance.FeesRound(feesBuy, curency);
                feesInfo.TOTAL_DISCOUNT = goUtility.Instance.FeesRound(total_discount, curency);
                feesInfo.VAT = goUtility.Instance.FeesRound(vat, curency);
                feesInfo.VAT_RATIO = goUtility.Instance.FeesRound(vat_ratio, curency);
                feesInfo.TOTAL_AMOUNT = goUtility.Instance.FeesRound(total_amount, curency);
                feesInfo.DISCOUNT_DETAIL = discountDetail;

                return feesInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fees">Phí truyền vào</param>
        /// <param name="VatRatio"></param>
        /// <param name="isDiscount"></param> goUtility.Instance.CastToBoolean
        /// <param name="Discount"></param>
        /// <param name="DiscountUnit"></param>
        /// <param name="gifts"></param>
        /// <param name="curency"></param>
        /// <returns></returns>
        private ProductFeesInfo GetFeesInfo(string crit_code, double fees, double VatRatio, bool IncludeVat, bool isDiscount, double Discount, string DiscountUnit, List<GiftInfo> gifts, string TypeFees, string curency = "VND")
        {
            ProductFeesInfo feesInfo = new ProductFeesInfo();
            // Do phí trước và sau vat nếu giảm thì bằng nhau chỉ khác nhau khi làm tròn => lệch phí
            try
            {
                List<DiscountInfo> discountDetail = new List<DiscountInfo>();

                double feesInVat = goUtility.Instance.FeesRound(fees, curency), feesRoot = 0, feesBuy = 0, total_discount = 0, vat = 0, total_amount = 0, vat_ratio = VatRatio;
                double feesDis = 0;

                if (IncludeVat) // Phí gồm vat
                {
                    feesRoot = goUtility.Instance.FeesRound(goUtility.Instance.FeesRootByFeesVat(feesInVat, vat_ratio), curency);

                    if (Discount == null || Discount == 0)
                        feesBuy = feesRoot;

                    if (Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit))
                    {
                        if (bug)
                        {
                            double disDetail = 0;
                            DiscountInfo disInfo = new DiscountInfo();
                            if (!goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = 0;
                                disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                disInfo.TOTAL_DISCOUNT = 0;
                            }
                            if (goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disDetail = GetTotalDiscount(feesRoot, Discount, DiscountUnit, curency);

                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = Discount;
                                disInfo.DISCOUNT_UNIT = DiscountUnit;
                                disInfo.TOTAL_DISCOUNT = disDetail;
                                disInfo.TYPE_FEES = TypeFees;

                                discountDetail.Add(disInfo);
                            }
                            // gán lại phí bán và tổng giảm
                            feesBuy = feesRoot - disDetail;
                            total_discount += disDetail;

                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau
                    if (gifts != null && gifts.Any())
                    {
                        DiscountInfo disInfo = new DiscountInfo();
                        feesBuy = feesRoot;
                        total_discount = 0;

                        disInfo.GIFT_CODE = "";
                        disInfo.TYPE_FEES = TypeFees;
                        discountDetail.Add(disInfo);
                    }

                    // nếu không có giảm giá => vat = feesInVat - feesRoot => tránh lệch phí
                    if ((Discount == null || Discount == 0) && !(gifts != null && gifts.Any()))
                    {
                        vat = feesInVat - feesRoot;
                        total_amount = feesInVat;
                        if (feesInVat < 0)
                        {
                            throw new Exception("Không tìm ra phí json: ");
                        }
                    }
                    else //bổ sung sau
                    {
                        vat = (feesRoot - total_discount) * vat_ratio / 100;
                        total_amount = (feesRoot - total_discount) + vat;
                    }
                }
                else
                {
                    feesRoot = goUtility.Instance.FeesRound(fees, curency);
                    if (Discount == null || Discount == 0)
                        feesBuy = feesRoot;

                    if (Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit))
                    {
                        if (bug)
                        {
                            double disDetail = 0;
                            DiscountInfo disInfo = new DiscountInfo();
                            if (!goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = 0;
                                disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                disInfo.TOTAL_DISCOUNT = 0;
                            }
                            if (goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disDetail = GetTotalDiscount(feesRoot, Discount, DiscountUnit, curency);

                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = Discount;
                                disInfo.DISCOUNT_UNIT = DiscountUnit;
                                disInfo.TOTAL_DISCOUNT = disDetail;
                                disInfo.TYPE_FEES = TypeFees;
                                discountDetail.Add(disInfo);
                            }
                            // gán lại phí bán và tổng giảm
                            feesBuy = feesRoot - disDetail;
                            total_discount += disDetail;

                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau => ảnh hướng tổng giảm(gồm vat)
                    if (gifts != null && gifts.Any())
                    {
                        DiscountInfo disInfo = new DiscountInfo();
                        feesBuy = feesRoot;
                        total_discount = 0;

                        disInfo.GIFT_CODE = "";
                        disInfo.TYPE_FEES = TypeFees;
                        discountDetail.Add(disInfo);
                    }

                    vat = (feesRoot - total_discount) * vat_ratio / 100;
                    total_amount = (feesRoot - total_discount) + vat;
                }
                feesInfo.FEES_DATA = goUtility.Instance.FeesRound(feesInVat, curency);
                feesInfo.FEES_ROOT = goUtility.Instance.FeesRound(feesRoot, curency);
                feesInfo.FEES_BUY = goUtility.Instance.FeesRound(feesBuy, curency);
                feesInfo.TOTAL_DISCOUNT = goUtility.Instance.FeesRound(total_discount, curency);
                feesInfo.VAT = goUtility.Instance.FeesRound(vat, curency);
                feesInfo.VAT_RATIO = goUtility.Instance.FeesRound(vat_ratio, curency);
                feesInfo.TOTAL_AMOUNT = goUtility.Instance.FeesRound(total_amount, curency);
                feesInfo.DISCOUNT_DETAIL = discountDetail;

                return feesInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Do lấy ra phí gốc => làm tròn => bị lệch phí thanh toán
        /// </summary>
        private ProductFeesInfo GetFeesInfoV2(string crit_code, double fees, double VatRatio, bool IncludeVat, bool isDiscount, double Discount, string DiscountUnit, List<GiftInfo> gifts, string TypeFees, string curency = "VND")
        {
            ProductFeesInfo feesInfo = new ProductFeesInfo();
            // Do phí trước và sau vat nếu giảm thì bằng nhau chỉ khác nhau khi làm tròn => lệch phí
            try
            {
                List<DiscountInfo> discountDetail = new List<DiscountInfo>();

                double feesInVat = goUtility.Instance.FeesRound(fees, curency), feesRoot = 0, feesBuy = 0, total_discount = 0, vat = 0, total_amount = 0, vat_ratio = VatRatio;
                double feesDis = 0;
                //if (crit_code == "XCG_PL1")
                //{
                //    var a = 1;
                //}
                if (IncludeVat) // Phí gồm vat
                {
                    feesRoot = goUtility.Instance.FeesRound(goUtility.Instance.FeesRootByFeesVat(feesInVat, vat_ratio), curency);

                    if (Discount == null || Discount == 0)
                        feesBuy = feesRoot;

                    if (Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit))
                    {
                        if (bug)
                        {
                            double disDetail = 0;
                            DiscountInfo disInfo = new DiscountInfo();
                            if (!goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = 0;
                                disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                disInfo.TOTAL_DISCOUNT = 0;
                            }
                            if (goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disDetail = GetTotalDiscount(feesRoot, Discount, DiscountUnit, curency);

                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = Discount;
                                disInfo.DISCOUNT_UNIT = DiscountUnit;
                                disInfo.TOTAL_DISCOUNT = disDetail;
                                disInfo.TYPE_FEES = TypeFees;

                                discountDetail.Add(disInfo);
                            }
                            // gán lại phí bán và tổng giảm
                            feesBuy = feesRoot - disDetail;
                            total_discount += disDetail;
                            total_discount = goUtility.Instance.FeesRound(total_discount, curency);
                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau
                    if (gifts != null && gifts.Any())
                    {
                        DiscountInfo disInfo = new DiscountInfo();
                        feesBuy = feesRoot;
                        total_discount = 0;

                        disInfo.GIFT_CODE = "";
                        disInfo.TYPE_FEES = TypeFees;
                        discountDetail.Add(disInfo);
                    }

                    // nếu không có giảm giá => vat = feesInVat - feesRoot => tránh lệch phí
                    if ((Discount == null || Discount == 0) && !(gifts != null && gifts.Any()))
                    {
                        vat = feesInVat - feesRoot;
                        vat = goUtility.Instance.FeesRound(vat, curency);
                        total_amount = feesInVat;
                        if (feesInVat < 0)
                        {
                            throw new Exception("Không tìm ra phí json: ");
                        }
                    }
                    else //bổ sung sau
                    {
                        vat = (feesRoot - total_discount) * vat_ratio / 100;
                        vat = goUtility.Instance.FeesRound(vat, curency);
                        // do phí lệch 1 đồng lên tính lại cách phí thanh toán trước rồi so sánh nếu khác thì thêm 1 đồng vào phí vat
                        // sử lý trường hợp gift sau
                        double total_amount_old = goUtility.Instance.FeesRound((feesRoot - total_discount) + vat, curency);
                        if ((Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit)) && !(gifts != null && gifts.Any()))
                        {
                            total_amount = feesInVat - GetTotalDiscount(feesInVat, Discount, DiscountUnit, curency);
                            total_amount = goUtility.Instance.FeesRound(total_amount, curency);
                        }
                        if(total_amount != total_amount_old)
                        {
                            var exp = (total_amount - total_amount_old);
                            vat = vat + exp;
                            vat = goUtility.Instance.FeesRound(vat, curency);
                        }
                    }
                }
                else
                {
                    feesRoot = goUtility.Instance.FeesRound(fees, curency);
                    if (Discount == null || Discount == 0)
                        feesBuy = feesRoot;

                    if (Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit))
                    {
                        if (bug)
                        {
                            double disDetail = 0;
                            DiscountInfo disInfo = new DiscountInfo();
                            if (!goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = 0;
                                disInfo.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                                disInfo.TOTAL_DISCOUNT = 0;
                            }
                            if (goUtility.Instance.CastToBoolean(isDiscount))
                            {
                                disDetail = GetTotalDiscount(feesRoot, Discount, DiscountUnit, curency);

                                disInfo.REF_CODE = crit_code;
                                disInfo.AMOUNT = feesRoot;
                                disInfo.DISCOUNT = Discount;
                                disInfo.DISCOUNT_UNIT = DiscountUnit;
                                disInfo.TOTAL_DISCOUNT = disDetail;
                                disInfo.TYPE_FEES = TypeFees;
                                discountDetail.Add(disInfo);
                            }
                            // gán lại phí bán và tổng giảm
                            feesBuy = feesRoot - disDetail;
                            total_discount += disDetail;
                            total_discount = goUtility.Instance.FeesRound(total_discount, curency);
                        }
                        else
                        {
                            feesBuy = feesRoot;
                            total_discount = 0;
                        }
                    }

                    // bổ sung sau => ảnh hướng tổng giảm(gồm vat)
                    if (gifts != null && gifts.Any())
                    {
                        DiscountInfo disInfo = new DiscountInfo();
                        feesBuy = feesRoot;
                        total_discount = 0;

                        disInfo.GIFT_CODE = "";
                        disInfo.TYPE_FEES = TypeFees;
                        discountDetail.Add(disInfo);
                    }

                    vat = (feesRoot - total_discount) * vat_ratio / 100;
                    vat = goUtility.Instance.FeesRound(vat, curency);
                    // do phí lệch 1 đồng lên tính lại cách phí thanh toán trước rồi so sánh nếu khác thì thêm 1 đồng vào phí vat
                    // sử lý trường hợp gift sau
                    double total_amount_old = goUtility.Instance.FeesRound((feesRoot - total_discount) + vat, curency);
                    if ((Discount != null && Discount != null && !string.IsNullOrEmpty(DiscountUnit)) && !(gifts != null && gifts.Any()))
                    {
                        total_amount = feesInVat - GetTotalDiscount(feesInVat, Discount, DiscountUnit, curency);
                        total_amount = goUtility.Instance.FeesRound(total_amount, curency);
                    }
                    if (total_amount != total_amount_old)
                    {
                        vat = vat + (total_amount - total_amount_old);
                        vat = goUtility.Instance.FeesRound(vat, curency);
                    }
                }
                feesInfo.FEES_DATA = feesInVat;//goUtility.Instance.FeesRound(feesInVat, curency);
                feesInfo.FEES_ROOT = feesRoot;// goUtility.Instance.FeesRound(feesRoot, curency);
                feesInfo.FEES_BUY = goUtility.Instance.FeesRound(feesBuy, curency);
                feesInfo.TOTAL_DISCOUNT = total_discount;// goUtility.Instance.FeesRound(total_discount, curency);
                feesInfo.VAT = vat;// goUtility.Instance.FeesRound(vat, curency);
                feesInfo.VAT_RATIO = goUtility.Instance.FeesRound(vat_ratio, curency);
                feesInfo.TOTAL_AMOUNT = total_amount;// goUtility.Instance.FeesRound(total_amount, curency);
                feesInfo.DISCOUNT_DETAIL = discountDetail;

                return feesInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void GetDynamicData(ref List<DynamicData> lstFeesNew, CriteriaFees item, List<CriteriaFees> lstCrit, List<CriteriaFeesDetail> lstCritDetail, List<DynamicData> lstFees)
        {
            try
            {
                int inx = -1;
                switch (item.TYPE_FEES)
                {
                    case (nameof(goConstants.TypeFeesDynamic.BSUNG)):
                        if (goUtility.Instance.CastToBoolean(item.IS_MULTI))
                        {
                            inx = lstFees.FindIndex(a => a.KEY_CODE == item.CRITERIA_CODE && a.TYPE_CODE == item.TYPE_CRITERIA);
                            if (inx >= 0)
                            {
                                string dataBS = lstFees[inx].VAL_CODE; // BS01@BS02@BS04
                                string[] arrBS = dataBS.Split('@');
                                // khi client truyền BS01@BS02@BS04 => các BS01 phải là lá và có giá trị trong cache
                                if (arrBS != null)
                                {
                                    foreach (var itemBS in arrBS)
                                    {
                                        CriteriaFees critBS = lstCrit.Where(x => x.CRITERIA_CODE == itemBS).FirstOrDefault();
                                        if (critBS != null)
                                        {
                                            DynamicData bsInfo = new DynamicData();
                                            bsInfo.TYPE_CODE = nameof(goConstants.TypeValueDynamic.CACHE);
                                            bsInfo.KEY_CODE = itemBS;

                                            switch (critBS.TYPE_ENCRYPT)
                                            {
                                                case nameof(goConstants.TypeEncryptFees.NONE):
                                                    bsInfo.VAL_CODE = itemBS;
                                                    break;
                                                case nameof(goConstants.TypeEncryptFees.MD5):
                                                    bsInfo.VAL_CODE = goEncryptBase.Instance.Md5Encode(itemBS);
                                                    break;
                                            }
                                            lstFeesNew.Add(bsInfo);
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)))
                            {
                                List<CriteriaFeesDetail> lstChild = lstCritDetail.Where(x => x.PARENT_CODE == item.CRITERIA_CODE).ToList();
                                List<CriteriaFees> lstCritChild = new List<CriteriaFees>();
                                foreach (var it in lstChild)
                                {
                                    List<CriteriaFees> lstC = lstCrit.Where(x => x.CRITERIA_CODE == it.CRITERIA_CODE).ToList();
                                    lstCritChild.AddRange(lstC);
                                }
                                if (lstCritChild != null && lstCritChild.Any())
                                {
                                    for (var i = 0; i < lstCritChild.Count(); i++)
                                    {
                                        CriteriaFees inpCriteria = lstCritChild[i];
                                        GetDynamicData(ref lstFeesNew, inpCriteria, lstCrit, lstCritDetail, lstFees);
                                    }
                                }
                            }
                            else
                            {
                                inx = lstFees.FindIndex(a => a.KEY_CODE == item.CRITERIA_CODE && a.TYPE_CODE == item.TYPE_CRITERIA);
                                if (inx >= 0)
                                {
                                    lstFeesNew.Add(lstFees[inx]);
                                }
                            }
                        }

                        // Với trường hợp là công thức thì add thêm
                        LoopDynamicData(ref lstFeesNew, item, lstCrit, lstCritDetail, lstFees);
                        break;
                    default:
                        LoopDynamicData(ref lstFeesNew, item, lstCrit, lstCritDetail, lstFees);
                        /*
                        if (item.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)))
                        {
                            List<CriteriaFeesDetail> lstChild = lstCritDetail.Where(x => x.PARENT_CODE == item.CRITERIA_CODE).ToList();
                            List<CriteriaFees> lstCritChild = new List<CriteriaFees>();
                            foreach (var it in lstChild)
                            {
                                List<CriteriaFees> lstC = lstCrit.Where(x => x.CRITERIA_CODE == it.CRITERIA_CODE).ToList();
                                lstCritChild.AddRange(lstC);
                            }
                            if (lstCritChild != null && lstCritChild.Any())
                            {
                                for (var i = 0; i < lstCritChild.Count(); i++)
                                {
                                    CriteriaFees inpCriteria = lstCritChild[i];
                                    GetDynamicData(ref lstFeesNew, inpCriteria, lstCrit, lstCritDetail, lstFees);
                                }
                            }
                        }
                        else
                        {
                            inx = lstFees.FindIndex(a => a.KEY_CODE == item.CRITERIA_CODE && a.TYPE_CODE == item.TYPE_CRITERIA);
                            if (inx >= 0)
                            {
                                lstFeesNew.Add(lstFees[inx]);
                            }
                        }
                        */
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoopDynamicData(ref List<DynamicData> lstFeesNew, CriteriaFees item, List<CriteriaFees> lstCrit, List<CriteriaFeesDetail> lstCritDetail, List<DynamicData> lstFees)
        {
            try
            {
                int inx = -1;
                if (item.TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)))
                {
                    List<CriteriaFeesDetail> lstChild = lstCritDetail.Where(x => x.PARENT_CODE == item.CRITERIA_CODE).ToList();
                    List<CriteriaFees> lstCritChild = new List<CriteriaFees>();
                    foreach (var it in lstChild)
                    {
                        List<CriteriaFees> lstC = lstCrit.Where(x => x.CRITERIA_CODE == it.CRITERIA_CODE).ToList();
                        lstCritChild.AddRange(lstC);
                    }
                    if (lstCritChild != null && lstCritChild.Any())
                    {
                        for (var i = 0; i < lstCritChild.Count(); i++)
                        {
                            CriteriaFees inpCriteria = lstCritChild[i];
                            GetDynamicData(ref lstFeesNew, inpCriteria, lstCrit, lstCritDetail, lstFees);
                        }
                    }
                }
                else
                {
                    inx = lstFees.FindIndex(a => a.KEY_CODE == item.CRITERIA_CODE && a.TYPE_CODE == item.TYPE_CRITERIA);
                    if (inx >= 0)
                    {
                        lstFeesNew.Add(lstFees[inx]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #endregion

        #region Init Cache
        private void InitCachingFees(BaseRequest request, string channel, string userName, string org_code, string category, string product_code)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseRequest reqFees = request;
                reqFees.Action.ActionCode = goConstantsProcedure.GET_FEES_INIT_CACHE;
                var pram = new
                {
                    channel = channel,
                    userName = userName,
                    org_code = org_code,
                    category = category,
                    product_code = product_code
                };
                reqFees.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(pram));
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "a json: " + JsonConvert.SerializeObject(reqFees), Logger.ConcatName(nameClass, nameMethod));
                BaseResponse response = goBusinessAction.Instance.GetBaseResponse(reqFees);
                if (response.Success)
                {
                    List<ProductFormula> lstProduct = goBusinessCommon.Instance.GetData<ProductFormula>(response, 0);
                    List<CriteriaFees> lstCrit = goBusinessCommon.Instance.GetData<CriteriaFees>(response, 1);
                    List<DynamicFeesInfo> lstDynamic = goBusinessCommon.Instance.GetData<DynamicFeesInfo>(response, 2);
                    List<CriteriaFeesDetail> lstCritDetail = goBusinessCommon.Instance.GetData<CriteriaFeesDetail>(response, 3);
                    List<ProductDefineFees> lstDefine = goBusinessCommon.Instance.GetData<ProductDefineFees>(response, 4);

                    List<SysOrgInfo> lstOrg = goBusinessCommon.Instance.GetData<SysOrgInfo>(response, 7);

                    if (lstProduct != null && lstProduct.Any() &&
                        lstCrit != null && lstCrit.Any() &&
                        lstDynamic != null && lstDynamic.Any())
                    {
                        ProductFormula product = lstProduct.FirstOrDefault();
                        string key_init_Crit = goConstantsRedis.PrefixDynamicFess + product.CATEGORY;
                        string key_init_Product = key_init_Crit + product.PRODUCT_CODE;
                        goBusinessCache.Instance.Add(key_init_Product, product);

                        //string keyCrit = key_init_Crit + goConstantsRedis.PrefixCriteria;
                        string keyCrit = key_init_Product + goConstantsRedis.PrefixCriteria;
                        goBusinessCache.Instance.Add(keyCrit, lstCrit);
                        foreach (var item in lstDynamic)
                        {
                            string keyFees = key_init_Product + item.KEYPAIR_CODE;// goEncryptBase.Instance.Md5ProviderEncode(item.KEYPAIR_CODE);
                            goBusinessCache.Instance.Add(keyFees, item.FEES);
                        }
                        // thêm danh sách detail tiêu chí, với trường hợp Formula
                        string keyCritDt = key_init_Product + goConstantsRedis.PrefixCriteriaDetail;
                        if (lstCritDetail != null && lstCritDetail.Any())
                            goBusinessCache.Instance.Add(keyCritDt, lstCritDetail);

                        // danh sách define của sản phẩm
                        string keyDefine = key_init_Product + goConstantsRedis.PrefixDefineFees;
                        if (lstDefine != null && lstDefine.Any())
                            goBusinessCache.Instance.Add(keyDefine, lstDefine);

                        // Thông tin phí min
                        string keyFeesMin = key_init_Product + goConstantsRedis.PrefixFeesMin;
                        List<CriteriaFeesMin> lstFeesMin = goBusinessCommon.Instance.GetData<CriteriaFeesMin>(response, 6);
                        if (lstFeesMin != null && lstFeesMin.Any())
                        {
                            goBusinessCache.Instance.Add(keyFeesMin, lstFeesMin);
                        }

                        // Thông tin không cho bán
                        string keyNotBuy = key_init_Product + goConstantsRedis.PrefixNotBuy;
                        switch (category)
                        {
                            case var s when category.Equals(goConstants.CATEGORY_XE):
                                List<VehicleNotBuy> lstNotBuy = goBusinessCommon.Instance.GetData<VehicleNotBuy>(response, 5);
                                if (lstNotBuy != null && lstNotBuy.Any())
                                    goBusinessCache.Instance.Add(keyNotBuy, lstNotBuy);
                                break;
                        }
                    }
                    else
                        throw new Exception("Dynamic Fees none data");


                    // Thông tin đối tác
                    if (lstOrg != null && lstOrg.Any())
                    {
                        string keyOrg = goConstantsRedis.OrgKey;
                        if (!goBusinessCache.Instance.Exists(keyOrg))
                            goBusinessCache.Instance.Add(keyOrg, lstOrg);
                    }
                }
                else
                    throw new Exception("Error Dynamic Fees:" + JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
        }

        public BaseResponse GetRecache(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                InputDynamicFees inputDynamic = new InputDynamicFees();
                if (request.Data != null)
                    inputDynamic = JsonConvert.DeserializeObject<InputDynamicFees>(request.Data.ToString());
                string key_init = goConstantsRedis.PrefixDynamicFess + inputDynamic.CATEGORY + inputDynamic.PRODUCT_CODE;
                goBusinessCache.Instance.RemoveAllPrefix(key_init);
                goBusinessCache.Instance.Remove(goConstantsRedis.OrgKey);
                response.Success = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public void CheckInitCaching(string key, BaseRequest request, string channel, string userName, string org_code, string category, string product_code)
        {
            if (!goBusinessCache.Instance.Exists(key))
                InitCachingFees(request, channel, userName, org_code, category, product_code);
        }
        #endregion

        #region Ký pháp ba lan
        private int Priority(char c)
        {
            if (c == '+' || c == '-')
                return 1;
            if (c == '*' || c == '/')
                return 2;
            return 0;
        }

        private bool IsNumber(char c)
        {
            return (c >= '0' && c <= '9') || (c == '.');
        }

        /// <summary>
        /// Ký pháp balan
        /// 1.prefix: Trong ký pháp này, toán tử được viết trước hai toán hạng tương ứng, người ta còn gọi ký pháp này là ký pháp Ba Lan.
        /// 2.infix: Công thức BT
        /// 3.postfix: Trong ký pháp này toán tử được viết sau hai toán hạng, người ta còn gọi ký pháp này là ký pháp nghịch đảo Balan
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        private string FormulaPostfix(string expression)
        {
            string result = "";
            Stack<char> stk = new Stack<char>();
            foreach (char i in expression)
            {
                //Nếu là toán tử
                if (i == '+' || i == '-' || i == '/' || i == '*')
                {
                    if (stk.Count > 0)
                    {
                        if (Priority(stk.Peek()) >= Priority(i))
                        {
                            result += stk.Pop();
                        }
                    }
                    stk.Push(i);
                    continue;
                }
                //Nếu là toán hạng
                if ((i >= 'a' && i <= 'z') || (i >= '0' && i <= '9'))
                {
                    result += i;
                    continue;
                }
                //Nếu là dấu mở ngoặc
                if (i == '(')
                {
                    stk.Push('(');
                    continue;
                }
                //Nếu là dấu đóng ngoặc
                if (i == ')')
                {
                    char tmp = stk.Pop();
                    while (tmp != '(')
                    {
                        result += tmp;
                        tmp = stk.Pop();
                    }
                }
            }
            //lấy hết tất cả những gì trong stack cho ra result
            foreach (char i in stk)
                result += i;
            return result;
        }

        private Double ExeCalculateFees(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (lstCrit[index].VAL_DATA == null)
                            stk.Push(0);
                        else
                            stk.Push(lstCrit[index].VAL_DATA);
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }
        private Double ExeCalculate_V2_FeesData(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].FEES_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].FEES_DATA);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }

        private Double ExeCalculate_V2_FeesRoot(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].FEES == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].FEES);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }

        private Double ExeCalculate_V2_FeesBuy(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].AMOUNT == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].AMOUNT);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }

        private Double ExeCalculate_V2_TotalDiscount(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].TOTAL_DISCOUNT == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].TOTAL_DISCOUNT);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }

        private Double ExeCalculate_V2_Vat(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].VAT == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAT);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }

        private Double ExeCalculate_V2_TotalAmount(string formula, List<CriteriaFees> lstCrit)
        {
            Stack<double> stk = new Stack<double>();
            double tmp1, tmp2;
            for (int i = 0; i < formula.Length; i++)
            {
                var amn = formula[i];
                switch (formula[i])
                {
                    case var s when charsFull.Contains(formula[i].ToString()):
                        int index = Array.IndexOf(chars, formula[i]);
                        if (!lstCrit[index].TYPE_CRITERIA.Equals(nameof(goConstants.TypeValueDynamic.FORMULA)) || (lstCrit[index].MODE_FEES != null && lstCrit[index].MODE_FEES.Equals(goConstants.UnitMode.P)))
                        {
                            if (lstCrit[index].VAL_DATA == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].VAL_DATA);
                        }
                        else
                        {
                            if (lstCrit[index].TOTAL_AMOUNT == null)
                                stk.Push(0);
                            else
                                stk.Push(lstCrit[index].TOTAL_AMOUNT);
                        }
                        break;
                    case '*':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 * tmp2);
                        break;
                    case '/':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 / tmp1);
                        break;
                    case '-':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp2 - tmp1);
                        break;
                    case '+':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(tmp1 + tmp2);
                        break;
                    case '^':
                        tmp1 = stk.Pop();
                        tmp2 = stk.Pop();
                        stk.Push(Math.Pow(tmp2, tmp1));
                        break;
                    default:
                        if (IsNumber(formula[i]))
                        {
                            int pos = i;
                            while (IsNumber(formula[i]))
                                i++;
                            stk.Push(int.Parse(formula.Substring(pos, i - pos)));
                            i--;
                        }
                        break;
                }
            }

            return stk.Pop();
        }
        #endregion

        #region Chung
        private double GetTotalDiscount(double amount, double discount, string discount_unit, string curency)
        {
            try
            {
                double disVal = 0;
                if (amount == null || amount == 0)
                    return 0;
                if (discount == null || discount == 0)
                    return 0;
                switch (discount_unit)
                {
                    case nameof(goConstants.UnitMode.M):
                        disVal = discount;
                        if (disVal > amount)
                            disVal = amount;
                        break;
                    case nameof(goConstants.UnitMode.P):
                        disVal = amount * discount / 100;
                        if (disVal > amount)
                            disVal = amount;
                        break;
                }
                return goUtility.Instance.FeesRound(disVal, curency);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SetFeesMin(ref CriteriaFees crit, string org_code, string channel, List<CriteriaFeesMin> lstFeesMin)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string critCode = crit.CRITERIA_CODE;
                CriteriaFeesMin infoMin = lstFeesMin.Where(i => i.ORG_CODE.Equals(org_code) && i.CRITERIA_CODE.Equals(critCode)).FirstOrDefault();
                if (crit == null || crit.VAL_DATA == null || infoMin == null || infoMin.MIN_VAL == null)
                    throw new Exception("chưa cấu hình phí min: " + org_code + " - " + channel + " - " + critCode);

                if (crit.VAL_DATA <= infoMin.MIN_VAL)
                    crit.VAL_DATA = infoMin.MIN_VAL;
                else
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Không vào phí min ", Logger.ConcatName(nameClass, nameMethod));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Chưa set phí min: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        #endregion
        #endregion
    }
}
