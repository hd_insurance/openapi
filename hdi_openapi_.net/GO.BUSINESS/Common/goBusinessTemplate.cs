﻿using GO.COMMON.Log;
using GO.DTO.Base;
using GO.DTO.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static GO.HELPER.Utility.goUtility;

namespace GO.BUSINESS.Common
{
    public class goBusinessTemplate
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessTemplate> _instance = new Lazy<goBusinessTemplate>(() =>
        {
            return new goBusinessTemplate();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessTemplate Instance { get => _instance.Value; }
        #endregion

        public string replaceHtmlValueJson(JObject jValuesData, TempEmailModel lsTemplate) //GCN, EMAIL
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            //jValuesData gồm master và detail thực hiện lấy để replate trong mẫu các trường hợp đặc biệt xử lý riêng
            List<string> lsResult = new List<string>();
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("");
                if (jValuesData != null)
                {
                    List<JObject> lsTempDetail = new List<JObject>();
                    sb.Append(lsTemplate.DATA);
                    List<string> lsReplace = lsTemplate.PARAM_REPLACE.Split(';').ToList();
                    int idexData = 0;
                    foreach (string strParam in lsReplace)
                    {
                        if (string.IsNullOrEmpty(strParam))
                            break;
                        switch (strParam.Substring(0, 1))
                        {
                            case "[": //nếu trong list có danh sách 
                                {   
                                    //string strParamNew = strParam.Substring(1, strParam.Length - 2);
                                    JArray jarr = JArray.Parse(jValuesData["DETAIL"]?.ToString());
                                    string strReplaceDetail = replaceTempDetailBody(lsTemplate.DETAIL[idexData], jarr);
                                    sb.Replace("@"+ lsTemplate.DETAIL[idexData].PARAM_NAME + "@", strReplaceDetail);
                                    idexData++;
                                    break;
                                }
                            case "{": //GIÁ TRỊ ĐƯỢC HIỂN THỊ DƯỚI DẠNG XUỐNG DÒNG {A} VỚI A=B;C;D
                                {
                                    string strParamNew = strParam.Substring(1, strParam.Length - 2);
                                    string strValueDB = jValuesData[strParamNew].ToString().Replace(";"," </br> ");
                                    sb.Replace("@" + strParamNew + "@", strValueDB);
                                    break;
                                }
                            case "("://giá trị của cột ở trong ngoặc được thay bằng giá trị bên cạnh (A:BCD)
                                {
                                    List<string> lsParam = strParam.Split(':').ToList();
                                    string strColumnName = lsParam[0].Substring(1, lsParam[0].Length - 1);
                                    string strValueReplace = lsParam[1].Substring(0, lsParam[1].Length - 1);
                                    sb.Replace("@" + jValuesData[strColumnName].ToString() + "@", strValueReplace);
                                    break;
                                }
                            default:
                                {
                                    if (strParam.Equals("MONEY"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(jValuesData[strParam]?.ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                    else if (strParam.Equals("MONEY_ENT"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(jValuesData[strParam]?.ToString().Replace(".", ""), MoneyFomat.VIET_TO_ENGLISH));
                                    if (strParam.Equals("AMOUNT") || strParam.Equals("TOTAL_AMOUNT"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().FomatMoneyVN(jValuesData[strParam]?.ToString().Replace(".", "")));
                                    if (strParam.Equals("NAME"))
                                        sb.Replace("@" + strParam + "@", jValuesData[strParam].ToString().ToUpper());
                                    else
                                        sb.Replace("@" + strParam + "@", jValuesData[strParam]?.ToString()); //12344
                                    break;
                                }
                        }
                    }

                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string replaceTempDetailBody(TempEmailDTModel obj, JArray arrDataDetail)
        {
            try
            {   
                string strReplaceDetail = "";                
                    List<string> strListParamDetail = obj.LIST_REPLACE.ToString().Split(';').ToList();
                    List<JToken> lsJTK_DataDetail = arrDataDetail.ToList();
                    //vòng for dữ liệu truyền vào
                    int iSTT = 1;
                    foreach (JObject jOjDataDetail in lsJTK_DataDetail)
                    {
                        string strTempHTMLDetail = obj.DATA.ToString();
                        //vòng for replate từng tham số
                        foreach (string strCTRP in strListParamDetail)
                        {
                            if (strCTRP.Equals("STT"))
                            {
                                strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", iSTT.ToString());
                                iSTT++;
                            }
                            else
                                strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jOjDataDetail[strCTRP].ToString());
                        }
                        strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                    }
                  
                return strReplaceDetail;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        //HÀM DÙNG CHUNG FILL VALUE TO HTML 
        public string replaceHtmlBody(JObject jValuesData,List<JObject> lsTemplate)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            string strBodyHtml = "";
            List<string> lsResult = new List<string>();
            try
            {                
                StringBuilder sb = new StringBuilder();
                sb.Append("");
                if (jValuesData != null)
                {
                    JObject objTempMaster = lsTemplate[0];
                    List<JObject> lsTempDetail = new List<JObject>();                    
                    sb.Append(objTempMaster["DATA"].ToString());
                    List<string> lsReplace = objTempMaster["PARAM_REPLACE"].ToString().Split(';').ToList();
                    int idexData = 1;
                    foreach (string strParam in lsReplace)
                    {
                        if (string.IsNullOrEmpty(strParam))
                            break;
                        switch (strParam.Substring(0, 1))
                        {
                            case "[": //nếu trong list có danh sách 
                                {
                                    if (lsTemplate.Count <= idexData)
                                    {
                                        break;
                                    }
                                    string strParamNew = strParam.Substring(1, strParam.Length - 2);
                                    JArray jarr = JArray.FromObject(jValuesData[strParamNew]);
                                    string strReplaceDetail = replaceTempDetailBody(lsTemplate[idexData], strParamNew, jarr);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    idexData++;
                                    break;
                                }
                            case "{": //Chưa xử lý trường hợp này
                                {
                                    string strParamNew = "";
                                    //string strReplaceDetail = replaceTempDetailBody(lsTemplateDetail, strParam, lsData[0], ref strParamNew);
                                    //sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    break;
                                }
                            case "("://giá trị của cột ở trong ngoặc được thay bằng giá trị bên cạnh
                                {
                                    List<string> lsParam = strParam.Split(':').ToList();
                                    string strColumnName = lsParam[0].Substring(1, lsParam[0].Length - 1);
                                    string strValueReplace = lsParam[1].Substring(0, lsParam[1].Length - 1);
                                    sb.Replace("@" + jValuesData[strColumnName].ToString() + "@", strValueReplace);
                                    break;
                                }
                            default:
                                {
                                    if (strParam.Equals("MONEY"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(jValuesData[strParam]?.ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                    else if (strParam.Equals("MONEY_ENT"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(jValuesData[strParam]?.ToString().Replace(".", ""), MoneyFomat.VIET_TO_ENGLISH));
                                    else
                                        sb.Replace("@" + strParam + "@", jValuesData[strParam]?.ToString()); //12344
                                    break;
                                }
                        }
                    }

                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public List<string> fillValueToHtml(List<object> resDataValue,  BaseResponse resDataTemplace)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            List<string> lsResult = new List<string>();
            try
            {
                JObject jobjData = JObject.Parse(resDataValue[0].ToString());
                StringBuilder sb = new StringBuilder();
                sb.Append("");
                List<List<JObject>> lstData = new List<List<JObject>>();
                lstData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataTemplace.Data));
                if (lstData != null && lstData.Any())
                {
                    JObject objTempMaster = lstData[0][0];
                    List<JObject> lsTempDetail = new List<JObject>();
                    if(lstData.Count > 1)
                    {
                        lsTempDetail = lstData[1];
                    }
                    sb.Append(objTempMaster["DATA"].ToString());
                    List<string> lsReplace = objTempMaster["PARAM_REPLACE"].ToString().Split(';').ToList();
                    int i = 1;
                    foreach (string strParam in lsReplace)
                    {
                        string strParamNew = strParam.Substring(1, strParam.Length - 2);
                        if (strParam.StartsWith("[") || strParam.StartsWith("{"))
                        {
                            JObject jObjDetail = new JObject();
                            string strReplaceDetail = "";
                            foreach(JObject obj in lsTempDetail)
                            {
                                if (obj["PARAM_NAME"].ToString().Equals(strParamNew))
                                {
                                    
                                    List<string> strListParam = obj["LIST_REPLACE"].ToString().Split(';').ToList();
                                    JArray arrData = JArray.FromObject(resDataValue[i]);
                                    List<JToken> lsJTl = arrData.ToList();
                                    //vòng for dữ liệu truyền vào
                                    foreach (JObject jT1 in lsJTl)
                                    {
                                        string strTempHTMLDetail = obj["DATA"].ToString();
                                        //vòng for replate từng tham số
                                        foreach (string strCTRP in strListParam)
                                        {
                                            strTempHTMLDetail = strTempHTMLDetail.Replace("@"+ strCTRP + "@", jT1[strCTRP].ToString());
                                        }

                                        strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                                    }
                                    break;    
                                }
                            }
                            sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                            i++;
                        }
                        else
                        {
                            sb.Replace("@" + strParam + "@", jobjData[strParam].ToString());
                        }
                    }

                    lsResult.Add(sb.ToString());
                }
                return lsResult;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string fillValueToHtmlBody(BaseResponse resDataValue, BaseResponse resDataTemplace)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            try
            {   
                List<List<JObject>> lstDataTemplate = new List<List<JObject>>();
                int idexData = 1;
                lstDataTemplate = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataTemplace.Data));
                List<List<JObject>> lsData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataValue.Data));
                if (lstDataTemplate != null && lstDataTemplate.Any() && lsData != null && lsData.Any())
                {
                    JObject objTempMaster = lstDataTemplate[0][0];
                    //List<JObject> lsTempDetail = new List<JObject>();
                    //if (lstData.Count > 1)
                    //{
                    //    lsTempDetail = lstData[1];
                    //}
                    if (objTempMaster["DATA_TYPE"].ToString().Equals("FILE"))
                    {
                        byte[] btepmlate = goBusinessCommon.Instance.getFileServer(objTempMaster[0]["DATA"].ToString());//"HDI-GCNBH.html";
                        sb.Append(System.Text.Encoding.UTF8.GetString(btepmlate));
                    }
                    else
                    {
                        sb.Append(objTempMaster["DATA"].ToString());
                    }
                    
                    List<string> lsReplace = objTempMaster["PARAM_REPLACE"].ToString().Split(';').ToList();
                    
                    foreach (string strParam in lsReplace)
                    {
                        if (string.IsNullOrEmpty(strParam))
                            break;
                        List<JObject> lsTemplateDetail = lstDataTemplate[0];
                        switch (strParam.Substring(0,1))
                        {                            
                            case "[": 
                                {
                                    if (lsData.Count <= idexData)
                                    {
                                        break;
                                    }
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lstDataTemplate[idexData], strParam, lsData[idexData], ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    idexData++;
                                    break;
                                }
                            case "{":
                                {                                    
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lsTemplateDetail, strParam, lsData[0], ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    break;
                                }
                            case "("://giá trị của cột ở trong ngoặc được thay bằng giá trị bên cạnh
                                {
                                    List<string> lsParam = strParam.Split(':').ToList();
                                    string strColumnName = lsParam[0].Substring(1, lsParam[0].Length - 1);
                                    string strValueReplace = lsParam[1].Substring(0, lsParam[1].Length - 1);
                                    sb.Replace("@" + lsData[0][0][strColumnName].ToString() + "@", strValueReplace);
                                        break;                                    
                                }
                            default:
                                {
                                    if (strParam.Equals("MONEY"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(lsData[0][0][strParam]?.ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                    else if (strParam.Equals("MONEY_ENT"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(lsData[0][0][strParam]?.ToString().Replace(".", ""), MoneyFomat.VIET_TO_ENGLISH));
                                    else
                                        sb.Replace("@" + strParam + "@", lsData[0][0][strParam]?.ToString()); //12344
                                    break;
                                }
                        }
                    }
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string fillValueToHtmlBodyBatch(List<JObject> lsData, BaseResponse resDataTemplace)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            try
            {
                List<List<JObject>> lstDataTemplate = new List<List<JObject>>();
                int idexData = 1;
                lstDataTemplate = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataTemplace.Data));
                //List<List<JObject>> lsData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataValue.Data));
                if (lstDataTemplate != null && lstDataTemplate.Any() && lsData != null && lsData.Any())
                {
                    JObject objTempMaster = lstDataTemplate[0][0];
                    if (objTempMaster["DATA_TYPE"].ToString().Equals("FILE"))
                    {
                        byte[] btepmlate = goBusinessCommon.Instance.getFileServer(objTempMaster[0]["DATA"].ToString());//"HDI-GCNBH.html";
                        sb.Append(System.Text.Encoding.UTF8.GetString(btepmlate));
                    }
                    else
                    {
                        sb.Append(objTempMaster["DATA"].ToString());
                    }

                    List<string> lsReplace = objTempMaster["PARAM_REPLACE"].ToString().Split(';').ToList();

                    foreach (string strParam in lsReplace)
                    {
                        if (string.IsNullOrEmpty(strParam))
                            break;
                        List<JObject> lsTemplateDetail = lstDataTemplate[0];
                        switch (strParam.Substring(0, 1))
                        {
                            case "[":
                                {
                                    if (lsData.Count <= idexData)
                                    {
                                        break;
                                    }
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lstDataTemplate[idexData], strParam, lsData, ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    idexData++;
                                    break;
                                }
                            case "{":
                                {
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lsTemplateDetail, strParam, lsData, ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    break;
                                }
                            case "("://giá trị của cột ở trong ngoặc được thay bằng giá trị bên cạnh
                                {
                                    List<string> lsParam = strParam.Split(':').ToList();
                                    string strColumnName = lsParam[0].Substring(1, lsParam[0].Length - 1);
                                    string strValueReplace = lsParam[1].Substring(0, lsParam[1].Length - 1);
                                    sb.Replace("@" + lsData[0][0][strColumnName].ToString() + "@", strValueReplace);
                                    break;
                                }
                            default:
                                {
                                    if (strParam.Equals("MONEY"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(lsData[0][strParam]?.ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                    else
                                        sb.Replace("@" + strParam + "@", lsData[0][strParam]?.ToString());
                                    break;
                                }
                        }
                    }
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string fillValueToHtmlGYC(List<List<JObject>> lsData, BaseResponse resDataTemplace)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            StringBuilder sb = new StringBuilder();
            sb.Append("");
            try
            {
                List<List<JObject>> lstDataTemplate = new List<List<JObject>>();
                int idexData = 1;
                lstDataTemplate = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataTemplace.Data));
                //List<List<JObject>> lsData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(resDataValue.Data));
                if (lstDataTemplate != null && lstDataTemplate.Any() && lsData != null && lsData.Any())
                {
                    JObject objTempMaster = lstDataTemplate[0][0];
                    //List<JObject> lsTempDetail = new List<JObject>();
                    //if (lstData.Count > 1)
                    //{
                    //    lsTempDetail = lstData[1];
                    //}
                    if (objTempMaster["DATA_TYPE"].ToString().Equals("FILE"))
                    {
                        byte[] btepmlate = goBusinessCommon.Instance.getFileServer(objTempMaster[0]["DATA"].ToString());//"HDI-GCNBH.html";
                        sb.Append(System.Text.Encoding.UTF8.GetString(btepmlate));
                    }
                    else
                    {
                        sb.Append(objTempMaster["DATA"].ToString());
                    }

                    List<string> lsReplace = objTempMaster["PARAM_REPLACE"].ToString().Split(';').ToList();

                    foreach (string strParam in lsReplace)
                    {
                        if (string.IsNullOrEmpty(strParam))
                            break;
                        List<JObject> lsTemplateDetail = lstDataTemplate[0];
                        switch (strParam.Substring(0, 1))
                        {
                            case "[":
                                {
                                    if (lsData.Count <= idexData)
                                    {
                                        break;
                                    }
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lstDataTemplate[idexData], strParam, lsData[idexData], ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    idexData++;
                                    break;
                                }
                            case "{":
                                {
                                    string strParamNew = "";
                                    string strReplaceDetail = replaceTempDetail(lsTemplateDetail, strParam, lsData[0], ref strParamNew);
                                    sb.Replace("@" + strParamNew + "@", strReplaceDetail);
                                    break;
                                }
                            case "("://giá trị của cột ở trong ngoặc được thay bằng giá trị bên cạnh
                                {
                                    List<string> lsParam = strParam.Split(':').ToList();
                                    string strColumnName = lsParam[0].Substring(1, lsParam[0].Length - 1);
                                    string strValueReplace = lsParam[1].Substring(0, lsParam[1].Length - 1);
                                    sb.Replace("@" + lsData[0][0][strColumnName].ToString() + "@", strValueReplace);
                                    break;
                                }
                            default:
                                {
                                    if (strParam.Equals("MONEY"))
                                        sb.Replace("@" + strParam + "@", new GO.HELPER.Utility.goUtility().ConvertMoneyToText(lsData[0][0][strParam]?.ToString().Replace(".", ""), MoneyFomat.MONEY_TO_VIET));
                                    else
                                        sb.Replace("@" + strParam + "@", lsData[0][0][strParam]?.ToString());
                                    break;
                                }
                        }
                    }
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string replaceTempDetail(List<JObject> lsDataTempDetail,string strParam,List<JObject> lsData, ref string strParamNew)
        {
            try
            {
                strParamNew = strParam.Substring(1, strParam.Length - 2);
                string strReplaceDetail = "";
                foreach (JObject obj in lsDataTempDetail)
                {
                    if (obj["PARAM_NAME"].ToString().Equals(strParamNew))
                    {
                        List<string> strListParamDetail = obj["LIST_REPLACE"].ToString().Split(';').ToList();
                        
                        JArray arrDataDetail = JArray.FromObject(lsData);
                        List<JToken> lsJTK_DataDetail = arrDataDetail.ToList();
                        //vòng for dữ liệu truyền vào
                        foreach (JObject jOjDataDetail in lsJTK_DataDetail)
                        {
                            string strTempHTMLDetail = obj["DATA"].ToString();
                            //vòng for replate từng tham số
                            foreach (string strCTRP in strListParamDetail)
                            {
                                strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jOjDataDetail[strCTRP].ToString());
                            }
                            strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                        }
                        break;
                    }
                }
                return strReplaceDetail;

            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string replaceTempDetailBody(JObject obj, string strParam, JArray arrDataDetail)
        {
            try
            {
                //string strParamNew = strParam.Substring(1, strParam.Length - 2);
                string strReplaceDetail = "";
                //foreach (JObject obj in lsDataTempDetail)
                //{
                    if (obj["PARAM_NAME"].ToString().Equals(strParam))
                    {
                        List<string> strListParamDetail = obj["LIST_REPLACE"].ToString().Split(';').ToList();

                        //JArray arrDataDetail = JArray.FromObject(lsData);
                        List<JToken> lsJTK_DataDetail = arrDataDetail.ToList();
                    //vòng for dữ liệu truyền vào
                    int iSTT = 1;
                    foreach (JObject jOjDataDetail in lsJTK_DataDetail)
                        {
                            string strTempHTMLDetail = obj["DATA"].ToString();
                            
                            //vòng for replate từng tham số
                            foreach (string strCTRP in strListParamDetail)
                            {
                            if (strCTRP.Equals("STT"))
                            {
                                strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", iSTT.ToString());
                                iSTT++;
                            }else
                                strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jOjDataDetail[strCTRP].ToString());
                            }
                            strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                        }
                        //break;
                    }
                //}
                return strReplaceDetail;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string replaceJobjectToHtml(JObject objValue, List<string> lsParam, string strBody)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(strBody);
                foreach (string strCTRP in lsParam)
                {
                    sb = sb.Replace("@" + strCTRP + "@", objValue[strCTRP]?.ToString());
                }
                
                return sb.ToString();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
