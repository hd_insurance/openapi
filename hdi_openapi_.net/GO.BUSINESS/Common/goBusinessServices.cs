﻿using GO.BUSINESS.Service;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.CONSTANTS;
using GO.COMMON.Log;
using System.Reflection;
using GO.DTO.Insurance;
using Newtonsoft.Json;
using GO.BUSINESS.ServiceAccess;
using System.Globalization;
using System.Threading;
using GO.ENCRYPTION;
using GO.DTO.SystemModels;
using GO.BUSINESS.Action;
using GO.DTO.Insurance.ImportFile;
using GO.DTO.Payment;
using GO.HELPER.Utility;
using GO.BUSINESS.BusPayment;
using GO.BUSINESS.Ecommerce;

namespace GO.BUSINESS.Common
{
    public class goBusinessServices
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessServices> _instance = new Lazy<goBusinessServices>(() =>
        {
            return new goBusinessServices();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessServices Instance { get => _instance.Value; }
        #endregion
        //public BaseResponse callPackage(string strPKG_Name, string environment, object[] Param)
        //{
        //    ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
        //    return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
        //}

        public BaseResponse reSignGCN(JObject jObjectReSign) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                string strOrgCode = jObjectReSign["ORG_CODE"].ToString();
                string strContractCode = jObjectReSign["CONTRACT_CODE"].ToString();
                string strStructCode = jObjectReSign["STRUCT_CODE"].ToString();
                string strProductCode = jObjectReSign["PRODUCT_CODE"].ToString();
                string strPackCode = jObjectReSign["PACK_CODE"].ToString();
                string strDate = jObjectReSign["EFFECTIVE_NUM"].ToString(); //dd/MM/yyyy
                string strFileID = jObjectReSign["FILE_ID"].ToString();//=1-giữ nguyên cũ, 0 là cấp mới
                string strSendEmail = jObjectReSign["EMAIL"].ToString();//1- là có gửi email
                string strSendSMS = jObjectReSign["SMS"].ToString();//1- là có gửi sms
                string strIsSignFile = jObjectReSign["IS_SIGN"].ToString();
                JArray arrFileID = JArray.Parse(jObjectReSign["FILE_LIST"].ToString());
                string strTypeFind = jObjectReSign["TYPE_DATA"].ToString();
                TempConfigModel template = get_all_template(strOrgCode, strStructCode, strProductCode, strPackCode, "GCN", "HDI_RESIGN", "WEB", strDate, strEvm);
                if (template != null)
                {
                    List<SignFileReturnModel> lsFilePdf = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    List<JObject> lsDataExt = new List<JObject>();
                    List<JObject> lsDetail = get_Data_GCN(strOrgCode, strProductCode, strContractCode, "HDI_RESIGN", strEvm, strTypeFind, ref jValueContract, ref lsDataExt);
                    //KhanhPT 20230720 Dieu chinh khong lay mau duy nhat, ma phai map voi hop dong, GCN cua khach hang
                    TempEmailModel tempGCN_CheckBackdate = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" &&
                    // Start 2022-08-26 By ThienTVB
                    (m.PACK_CODE == strPackCode || string.IsNullOrEmpty(strPackCode)) 
                    // End 2022-08-26 By ThienTVB
                    ).ToList()[0];                    
                    //End
                    bool iBackDate = false;
                    if (tempGCN_CheckBackdate.IS_BACK_DATE != null && tempGCN_CheckBackdate.IS_BACK_DATE.Equals("1"))
                    {
                        iBackDate = true;
                    }
                    string strUrl = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    if (lsDetail != null && lsDetail.Any())
                    {
                        List<JObject> lsObjectNew = new List<JObject>();
                        JArray jValueDetail = new JArray();
                        foreach (JObject item in lsDetail)
                            {
                                SignFileReturnModel filePdf = new SignFileReturnModel();
                                string strGuiID_File = "";
                                if (strFileID.Equals("1")) //sử dụng lại file id
                                {
                                    foreach (JToken jtoken in arrFileID)
                                    {
                                        string strContractNo = jtoken["CERTIFICATE_NO"].ToString();
                                        string strPackCodeInput = jtoken["PACK_CODE"].ToString();
                                        if (item["CERTIFICATE_NO"].ToString().Equals(strContractNo) && strPackCodeInput.Equals(item["PACK_CODE"].ToString()))
                                        {
                                            strGuiID_File = strUrl + jtoken["FILE_ID"].ToString();
                                            break;
                                        }
                                    }
                                }
                                else
                                    strGuiID_File = Guid.NewGuid().ToString("N");
                            if (!string.IsNullOrEmpty(strGuiID_File))
                            {
                                item.Add("URL_FILE", strGuiID_File);
                                item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                                item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                                //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                                item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                                item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                                InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                                lsInsDetailExt.Add(itemExt);

                                //KhanhPT 20230720 lay mau theo tung GCN bao hiem cua khach hang
                                TempEmailModel tempGCN = new TempEmailModel();
                                if (strProductCode.StartsWith("ATTD"))
                                {                                    
                                    tempGCN = tempGCN_CheckBackdate;
                                }
                                else
                                { 
                                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" &&
                                    (m.PACK_CODE == item["PACK_CODE"].ToString())
                                    ).ToList();
                                    
                                    if (lstempGCN == null && lstempGCN.Count == 0)
                                    {
                                        baseResponse.Success = false;
                                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + item["CERTIFICATE_NO"].ToString();
                                        return baseResponse;
                                    }

                                    tempGCN = lstempGCN[0];
                                }                                                                        

                                //End

                                if (strIsSignFile.Equals("1"))
                                {
                                    if (strProductCode.StartsWith("ATTD"))
                                    {
                                        jValueContract.Add("DETAIL",JArray.FromObject(lsDetail));
                                        filePdf = createPDF_GCN(tempGCN, jValueContract, strGuiID_File);
                                        filePdf.USER_NAME = "HDI_RESIGN";
                                        //filePdf.dtSignBack = DateTime.ParseExact(itemExt.EFFECTIVE_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                                        lsFilePdf.Add(filePdf);
                                        break;
                                    }
                                    else
                                    filePdf = createPDF_GCN(tempGCN, item, strGuiID_File);

                                }
                                else
                                {
                                    filePdf.FILE_BASE64 = "";
                                    filePdf.FILE_ID = strGuiID_File;
                                    filePdf.FILE_REF = itemExt.CERTIFICATE_NO;
                                    filePdf.FILE_URL = strUrl + strGuiID_File;
                                }
                                    filePdf.USER_NAME = "HDI_RESIGN";
                                    filePdf.dtSignBack = DateTime.ParseExact(itemExt.EFFECTIVE_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                                    lsFilePdf.Add(filePdf);
                                lsObjectNew.Add(item);
                            }
                                else
                                {
                                baseResponse.Success = false;
                                baseResponse.Error = "Không tìm được ID file GCN: " + item["CERTIFICATE_NO"].ToString();
                                return baseResponse;
                                }                                
                            }
                        jValueDetail = JArray.FromObject(lsObjectNew);
                        if (!strProductCode.StartsWith("ATTD"))
                        {
                            jValueContract.Add("DETAIL", jValueDetail);
                        }
                    }
                    
                   
                    List<SignFileReturnModel> lsFileSigned = new List<SignFileReturnModel>();
                    if (strIsSignFile.Equals("1"))
                    {
                        lsFileSigned = signPDF_GCN(template.TEMP_SIGN, lsFilePdf, true, false, iBackDate, strEvm);
                    }
                    else
                    {
                        lsFileSigned = lsFilePdf;
                    }
                    if (lsFileSigned != null && lsFilePdf.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFileSigned;
                        if (template.TEMP_EMAIL.Count > 1 && strSendEmail.Equals("1"))
                        {
                            bool b = sendMail_GCN(template, lsFileSigned, jValueContract, strEvm);
                            if (!b)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data resign " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            }
                        }
                        if (template.TEMP_SMS != null)
                        {
                            List<TempSMSModel> lsSMS = template.TEMP_SMS.Where(m => m.TEMP_CODE == "GCN").ToList();
                            if (lsSMS != null && lsSMS.Any())
                            {
                                TempSMSModel tSMS = lsSMS[0];
                                if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && strSendSMS.Equals("1"))
                                {
                                    //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                                    bool bSMS = bSMS = sendSMS(tSMS, template, jValueContract, "RE_SIGN", lsFileSigned);
                                    if (tSMS.SEND_NOW.Equals("1"))//chỉ dành cho test
                                    {

                                    }
                                    if (!bSMS)
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số gửi mail";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu " + jObjectReSign.ToString();
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
            }
            if (!baseResponse.Success)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail businis_service 167", jObjectReSign.ToString());
            }
            return baseResponse;
        }

        public BaseResponse reSendEmailContract(JObject jObjectReSign) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                string strOrgCode = jObjectReSign["ORG_CODE"].ToString();
                string strContractCode = jObjectReSign["CONTRACT_CODE"].ToString();
                string strStructCode = jObjectReSign["STRUCT_CODE"].ToString();
                string strProductCode = jObjectReSign["PRODUCT_CODE"].ToString();
                string strPackCode = jObjectReSign["PACK_CODE"].ToString();
                string strDate = jObjectReSign["EFFECTIVE_NUM"].ToString(); //dd/MM/yyyy    
                string strTypeFind = jObjectReSign["TYPE_DATA"].ToString();
                TempConfigModel template = get_all_template(strOrgCode, strStructCode, strProductCode, strPackCode, "GCN", "HDI_RESIGN", "WEB", strDate, strEvm);
                if (template != null)
                {
                    List<SignFileReturnModel> lsFileSigned = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    List<JObject> lsDataExt = new List<JObject>();
                    List<JObject> lsDetail = get_Data_GCN(strOrgCode, strProductCode, strContractCode, "HDI_RESIGN", strEvm, strTypeFind, ref jValueContract, ref lsDataExt);                    
                    string strUrl = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    if (lsDetail != null && lsDetail.Any())
                    {
                        int i = 1;
                        List<JObject> lsObjectNew = new List<JObject>();
                        JArray jValueDetail = new JArray();
                        foreach (JObject item in lsDetail)
                        {
                            SignFileReturnModel filePdf = new SignFileReturnModel();
                            filePdf.FILE_ID = item["FILE_ID"]?.ToString();
                            filePdf.FILE_REF = item["CERTIFICATE_NO"]?.ToString();
                            lsFileSigned.Add(filePdf);
                            item["URL_FILE"] = strUrl + item["ID_FILE"]?.ToString();
                            item["STT"] = i.ToString();
                            i++;
                        }
                        jValueDetail = JArray.FromObject(lsDetail);
                        jValueContract.Add("DETAIL", jValueDetail);
                    }
                    if (lsFileSigned != null && lsFileSigned.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFileSigned;
                        if (template.TEMP_EMAIL.Count > 1)
                        {
                            bool b = sendMail_GCN(template, lsFileSigned, jValueContract, strEvm);
                            if (!b)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data resign " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            }
                        }                        
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số gửi mail";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu " + jObjectReSign.ToString();
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
            }
            if (!baseResponse.Success)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail businis_service 167", jObjectReSign.ToString());
            }
            return baseResponse;
        }

        public BaseResponse signAndSendMail(InsurContract iContract, List<InsurDetail> lsDetailsAll, string strUser, string strChannel, string strTypeFind, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            JObject jInput = JObject.FromObject(iContract);
            JArray jarrInput = JArray.FromObject(lsDetailsAll);
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", strUser);
            jInput.Add("CHANNEL_CALL", strChannel);
            jInput.Add("TYPE_CALL", strTypeFind);
            try
            {
                //thực hiện tạo toàn bộ pdf
                DateTime dtEFF = getDateEff(iContract.EFFECTIVE_NUM);
                TempConfigModel template = get_all_template(iContract.ORG_SELLER, iContract.STRUCT_CODE, iContract.PRODUCT_CODE, iContract.PACK_CODE,
                    "GCN", strUser, strChannel.ToUpper(), dtEFF.ToString("yyyyMMddHHmmss"), strEvm);
                if (template != null && template.TEMP_EMAIL !=null)
                {
                    if (template.ISQUEUE != null && template.ISQUEUE.Equals(1))
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Thực hiện Queue ký số";
                        baseResponse.Data = null;
                        return baseResponse;
                    }
                    List<SignFileReturnModel> lsFilePdf = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    List<string> lsPackCode = lsDetailsAll.Select(m => m.PACK_CODE).Distinct().ToList();
                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" &&
                    // Start 2022-08-26 By ThienTVB
                    (m.PACK_CODE == iContract.PACK_CODE || string.IsNullOrEmpty(iContract.PACK_CODE))
                    // End 2022-08-26 By ThienTVB
                    ).ToList();
                    TempEmailModel tempGCN = new TempEmailModel();
                    if (lstempGCN == null && lstempGCN.Count == 0 && lsPackCode.Count < (lstempGCN.Count+1))
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Mẫu giấy chứng nhận ít hơn số gói được khai báo", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + JsonConvert.SerializeObject(lsPackCode));
                        return baseResponse;
                    }
                    if (template.TEMP_SIGN == null || template.TEMP_SIGN.ID_KEY.Length == 0)
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Chưa khai báo quyền ký số", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                        return baseResponse;
                    }
                    //có 2 trường hợp tạo GCN: tạo GCN cho người mua (ALL) và tạo GCN cho từng người được bảo hiểm (ONE)
                    List<JObject> lsDataExt = new List<JObject>();
                    //List<JObject> lsGenGCN = new List<JObject>();
                    Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                    tempGCN = lstempGCN[0];
                    List<JObject> lsDetail = new List<JObject>();
                    if (tempGCN.TYPE_SEND.Equals("ALL"))
                    {
                        
                        switch (tempGCN.DATA_SRC.ToUpper())
                        {
                            case "JSON":
                                {
                                    lsInsDetailExt = getDetailExt(iContract, lsDetailsAll, ref lsInsContractExt);
                                    jValueContract = JObject.FromObject(lsInsContractExt);
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    lsGenGCN.Add(lsInsContractExt.CONTRACT_NO, jValueContract);
                                    break;
                                }
                            case "DATA":
                                {

                                    lsDetail = get_Data_GCN(iContract.ORG_SELLER, iContract.PRODUCT_CODE, iContract.CONTRACT_CODE, strUser, strEvm, strTypeFind, ref jValueContract, ref lsDataExt);
                                    //tạo list insuran exten --> tạo giấy chứng nhận
                                    foreach (JObject item in lsDetail)
                                    {
                                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                                        item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                                        //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                                        item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                                        //InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                                        //lsInsDetailExt.Add(itemExt);
                                        
                                    }
                                    jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                                    lsGenGCN.Add(iContract.CONTRACT_NO, jValueContract);
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));

                                    break;
                                }
                            case "PDF":
                                {
                                    break;
                                }
                            case "DOC":
                            case "XSL":
                                {
                                    break;
                                }
                        }
                    }
                    else
                    {
                        switch (tempGCN.DATA_SRC.ToUpper())
                        {
                            case "JSON":
                                {
                                    lsInsDetailExt = getDetailExt(iContract, lsDetailsAll, ref lsInsContractExt);
                                    foreach(InsurDetailExt itExt in lsInsDetailExt)
                                    {
                                        JObject obj = JObject.FromObject(itExt);
                                        obj.Add("DETAIL", null);
                                        lsGenGCN.Add(itExt.CERTIFICATE_NO,obj);
                                    }
                                    jValueContract = JObject.FromObject(lsInsContractExt);
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    break;
                                }
                            case "DATA":
                                {

                                    lsDetail = get_Data_GCN(iContract.ORG_SELLER, iContract.PRODUCT_CODE, iContract.CONTRACT_CODE, strUser, strEvm, strTypeFind, ref jValueContract, ref lsDataExt);
                                    //tạo list insuran exten --> tạo giấy chứng nhận
                                    foreach (JObject item in lsDetail)
                                    {
                                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                                        item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                                        //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                                        item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                                        if (lsDataExt != null && lsDataExt.Count > 0)
                                        {
                                            List<JObject> lsDetailExt = new List<JObject>();
                                            foreach (JObject objExt in lsDataExt)
                                            {
                                                if (item["CERTIFICATE_NO"].ToString().Equals(objExt["CERTIFICATE_NO"]?.ToString()))
                                                {
                                                    lsDetailExt.Add(objExt);
                                                }
                                            }
                                            if (lsDetailExt != null && lsDataExt.Any())
                                            {
                                                item.Add("DETAIL", JArray.FromObject(lsDetailExt));
                                            }
                                            else
                                            {
                                                item.Add("DETAIL", null);
                                            }
                                        }
                                        //InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                                        //lsInsDetailExt.Add(itemExt);
                                        lsGenGCN.Add(item["CERTIFICATE_NO"].ToString(),item);
                                    }
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    break;
                                }
                            case "PDF":
                                {

                                    break;
                                }
                            case "DOC":
                            case "XSL":
                                {
                                    break;
                                }
                        }
                    }
                    SignFileReturnModel filePdf = new SignFileReturnModel();
                    int i = 1;
                    //string strUrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/";
                    string strUrl = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    if (tempGCN.TYPE_SEND.Equals("ALL"))
                    {
                        
                        if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                            goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE );
                            return baseResponse;
                        }
                        string strGuiID_File = Guid.NewGuid().ToString("N");
                        //itemExt.URL_FILE = strGuiID_File;
                        filePdf = createPDF_GCN(tempGCN, lsGenGCN[iContract.CONTRACT_NO], strGuiID_File);
                        filePdf.dtSignBack = dtEFF;
                        filePdf.USER_NAME = strUser;
                        filePdf.FILE_URL = strGuiID_File;
                        lsFilePdf.Add(filePdf);                        
                        
                        if (tempGCN.DATA_SRC.Equals("JSON"))
                        {
                            foreach (InsurDetailExt itemExt in lsInsDetailExt)
                            {
                                itemExt.URL_FILE = strUrl + strGuiID_File;
                                itemExt.STT = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                        }
                        else
                        {
                            foreach (JObject item in lsDetail)
                            {
                                item["URL_FILE"] = strUrl + strGuiID_File;
                                item["STT"] = i.ToString();
                                i++;
                            }
                            try
                            {
                                jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                            }
                            catch (Exception)
                            {

                                jValueContract["DETAIL"] = JArray.FromObject(lsDetail);
                            }
                            
                        }
                        
                    }
                    else
                    {
                        if (tempGCN.DATA_SRC.Equals("JSON"))
                        {
                            foreach (InsurDetailExt itemExt in lsInsDetailExt)
                            {
                                tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && m.PACK_CODE == itemExt.PACK_CODE).FirstOrDefault();
                                if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                                {
                                    baseResponse.Success = false;
                                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE + itemExt.PACK_CODE;
                                    goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + "_" + itemExt.PACK_CODE);
                                    return baseResponse;
                                }
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //itemExt.URL_FILE = strGuiID_File;
                                filePdf = createPDF_GCN(tempGCN, lsGenGCN[itemExt.CERTIFICATE_NO], strGuiID_File);
                                filePdf.dtSignBack = dtEFF;
                                filePdf.USER_NAME = strUser;
                                filePdf.FILE_URL = strGuiID_File;
                                lsFilePdf.Add(filePdf);
                                itemExt.URL_FILE = strUrl + strGuiID_File;
                                itemExt.STT = i.ToString();
                                i++;
                            }
                             jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                        }
                        else
                        {
                            foreach (JObject item in lsDetail)
                            {
                                tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && (m.PACK_CODE == item["PACK_CODE"].ToString() || m.PACK_CODE =="null")).FirstOrDefault();
                                if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                                {
                                    baseResponse.Success = false;
                                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE + item["PACK_CODE"].ToString();
                                    goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + "_" + item["PACK_CODE"].ToString());
                                    return baseResponse;
                                }
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //itemExt.URL_FILE = strGuiID_File;
                                filePdf = createPDF_GCN(tempGCN, lsGenGCN[item["CERTIFICATE_NO"].ToString()], strGuiID_File);
                                filePdf.dtSignBack = dtEFF;
                                filePdf.USER_NAME = strUser;
                                filePdf.FILE_URL = strGuiID_File;
                                lsFilePdf.Add(filePdf);
                                item["URL_FILE"] = strUrl + strGuiID_File;
                                item["STT"] = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                        }
                        

                    }
                    bool iBackDate = false;
                    if (tempGCN.IS_BACK_DATE != null && tempGCN.IS_BACK_DATE.Equals("1"))
                    {
                        iBackDate = true;
                    }

                     List<SignFileReturnModel> lsFileSigned = signPDF_GCN(template.TEMP_SIGN, lsFilePdf, true, false, iBackDate, strEvm);

                    if (lsFileSigned != null && lsFilePdf.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFileSigned;
                        //Check nếu onpoint tạo đơn thì call cập nhật trạng thái
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Gửi thông tin onpoint "+ iContract.ORG_SELLER + iContract.CONTRACT_CODE, Logger.ConcatName(nameClass, nameMethod));
                        if (iContract.ORG_SELLER.Equals("ONPOINT"))
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Gửi thông tin onpoint " + iContract.CONTRACT_CODE, Logger.ConcatName(nameClass, nameMethod));
                            new Task(() => goBusinessEcommerce.Instance.update_status_GCN(iContract.ORG_SELLER, iContract.CONTRACT_CODE)).Start(); 
                        }
                        jValueContract.Add("LINK_ECER", goBusinessCommon.Instance.GetConfigDB("ECER"));
                        if (template.TEMP_EMAIL.Count > 1 && isEmailTest)
                        {
                            // Start 2022-07-14 By ThienTVB
                            new Task(() =>
                            {
                                // Start 2022-08-18 By ThienTVB
                                if (iContract.SELLER_MAIL != null)
                                {
                                    if (iContract.SELLER_MAIL.Length > 0)
                                    {
                                        template.MAIL_BCC = iContract.SELLER_MAIL;
                                    }
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "MailBCC: " + template.MAIL_BCC, Logger.ConcatName(nameClass, nameMethod));
                                }
                                // End 2022-08-18 By ThienTVB
                                bool b = sendMail_GCN(template, lsFileSigned, jValueContract, strEvm, isTest);
                                if (!b)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                }
                            }).Start();
                            // End 2022-07-14 By ThienTVB
                            //bool b = sendMail_GCN(template, lsFileSigned, jValueContract, strEvm, isTest);
                            //if (!b)
                            //{
                            //    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            //}
                        }
                        if (template.TEMP_SMS != null)
                        {
                            // Start 2022-07-14 By ThienTVB
                            new Task(() =>
                            {
                                List<TempSMSModel> lsSMS = template.TEMP_SMS.Where(m => m.TEMP_CODE == "GCN").ToList();
                                if (lsSMS != null && lsSMS.Any())
                                {
                                    TempSMSModel tSMS = lsSMS[0];
                                    if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && tSMS.SEND_NOW.Equals("1") && isSMSTest)
                                    {
                                        //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                                        bool bSMS = bSMS = sendSMS(tSMS, template, jValueContract, strUser, lsFileSigned);
                                        if (tSMS.SEND_NOW.Equals("1"))//chỉ dành cho test
                                        {

                                        }
                                        if (!bSMS)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                        }
                                    }
                                }
                            }).Start();
                            // End 2022-07-14 By ThienTVB
                            //List<TempSMSModel> lsSMS = template.TEMP_SMS.Where(m => m.TEMP_CODE == "GCN").ToList();
                            //if (lsSMS != null && lsSMS.Any())
                            //{
                            //    TempSMSModel tSMS = lsSMS[0];
                            //    if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && tSMS.SEND_NOW.Equals("1") && isSMSTest)
                            //    {
                            //        //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                            //        bool bSMS = bSMS = sendSMS(tSMS, template, jValueContract, strUser, lsFileSigned);
                            //        if (tSMS.SEND_NOW.Equals("1"))//chỉ dành cho test
                            //        {

                            //        }
                            //        if (!bSMS)
                            //        {
                            //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            //        }
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số gửi mail";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu ";
                }

                //thực hiện ký số toàn bộ pdf

                //thực hiện gửi email nếu có

                //thực hiện gửi sms nếu có
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail Exception service-signAndSendMail 336", jInput.ToString() + " ---" + ex.ToString());
                return baseResponse;
            }
            //if (!baseResponse.Success)
            //{
            //    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail service-signAndSendMail 340", jInput.ToString());
            //}
            return baseResponse;
        }

        public BaseResponse previewGCN(JObject iContract, List<JObject> lsDetailsAll, string strUser, string strChannel, string strTypeFind, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            JObject jInput = JObject.FromObject(iContract);
            JArray jarrInput = JArray.FromObject(lsDetailsAll);
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", strUser);
            jInput.Add("CHANNEL_CALL", strChannel);
            jInput.Add("TYPE_CALL", strTypeFind);

            String strOrgSeller = iContract["ORG_SELLER"]?.ToString();
            String strStructCode = iContract["STRUCT_CODE"]?.ToString();
            String strProductCode = iContract["PRODUCT_CODE"]?.ToString();
            String strPackCode = iContract["PACK_CODE"]?.ToString();
            try
            {
                //thực hiện tạo toàn bộ pdf
                DateTime dtEFF = getDateEff(iContract["EFFECTIVE_NUM"].ToString());
                TempConfigModel template = get_all_template(strOrgSeller, strStructCode, strProductCode, strPackCode,
                    "GCN", strUser, strChannel.ToUpper(), dtEFF.ToString("yyyyMMddHHmmss"), strEvm);
                if (template != null && template.TEMP_EMAIL != null)
                {
                    if (template.ISQUEUE != null && template.ISQUEUE.Equals(1))
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Thực hiện Queue ký số";
                        baseResponse.Data = null;
                        return baseResponse;
                    }
                    List<SignFileReturnModel> lsFilePdf = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    List<string> lsPackCode = new List<string>();
                    lsPackCode.Add(lsDetailsAll[0]["PACK_CODE"].ToString());                    
                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" &&
                    // Start 2022-08-26 By ThienTVB
                    (m.PACK_CODE == strPackCode || string.IsNullOrEmpty(strPackCode))
                    // End 2022-08-26 By ThienTVB
                    ).ToList();
                    TempEmailModel tempGCN = new TempEmailModel();
                    if (lstempGCN == null && lstempGCN.Count == 0 && lsPackCode.Count < (lstempGCN.Count + 1))
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + strProductCode;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Mẫu giấy chứng nhận ít hơn số gói được khai báo", strOrgSeller + "_" + strProductCode + JsonConvert.SerializeObject(lsPackCode));
                        return baseResponse;
                    }
                    if (template.TEMP_SIGN == null || template.TEMP_SIGN.ID_KEY.Length == 0)
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + strProductCode;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Chưa khai báo quyền ký số", strOrgSeller + "_" + strProductCode);
                        return baseResponse;
                    }
                    //có 2 trường hợp tạo GCN: tạo GCN cho người mua (ALL) và tạo GCN cho từng người được bảo hiểm (ONE)
                    List<JObject> lsDataExt = new List<JObject>();
                    //List<JObject> lsGenGCN = new List<JObject>();
                    Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                    tempGCN = lstempGCN[0];
                    List<JObject> lsDetail = new List<JObject>();

                    jValueContract = JObject.FromObject(iContract);
                    
                    foreach(JObject obj in JArray.FromObject(lsDetailsAll))
                    {
                        lsDetail.Add(obj);
                    }    

                    //lsDetail = get_Data_GCN(iContract.ORG_SELLER, iContract.PRODUCT_CODE, iContract.CONTRACT_CODE, strUser, strEvm, strTypeFind, ref jValueContract, ref lsDataExt);

                    //tạo list insuran exten --> tạo giấy chứng nhận
                    foreach (JObject item in lsDetail)
                    {
                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                        item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                        //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                        item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                        if (lsDataExt != null && lsDataExt.Count > 0)
                        {
                            List<JObject> lsDetailExt = new List<JObject>();
                            foreach (JObject objExt in lsDataExt)
                            {
                                if (item["CERTIFICATE_NO"].ToString().Equals(objExt["CERTIFICATE_NO"]?.ToString()))
                                {
                                    lsDetailExt.Add(objExt);
                                }
                            }
                            if (lsDetailExt != null && lsDataExt.Any())
                            {
                                item.Add("DETAIL", JArray.FromObject(lsDetailExt));
                            }
                            else
                            {
                                item.Add("DETAIL", null);
                            }
                        }
                        //InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                        //lsInsDetailExt.Add(itemExt);
                        lsGenGCN.Add(item["CERTIFICATE_NO"].ToString(), item);
                    }
                    
                    SignFileReturnModel filePdf = new SignFileReturnModel();
                    int i = 1;
                    //string strUrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/";
                    string strUrl = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    //Data for upload
                    foreach (JObject item in lsDetail)
                    {
                        tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && (m.PACK_CODE == item["PACK_CODE"].ToString() || m.PACK_CODE == "null")).FirstOrDefault();
                        if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + strProductCode + item["PACK_CODE"].ToString();
                            goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", strOrgSeller + "_" + strProductCode + "_" + item["PACK_CODE"].ToString());
                            return baseResponse;
                        }
                        string strGuiID_File = Guid.NewGuid().ToString("N");
                        //itemExt.URL_FILE = strGuiID_File;
                        filePdf = createPDF_GCN_Preview(tempGCN, lsGenGCN[item["CERTIFICATE_NO"].ToString()], strGuiID_File);
                        filePdf.dtSignBack = dtEFF;
                        filePdf.USER_NAME = strUser;
                        filePdf.FILE_URL = strGuiID_File;
                        lsFilePdf.Add(filePdf);
                        item["URL_FILE"] = strUrl + strGuiID_File;
                        item["STT"] = i.ToString();
                        i++;
                    }
                    jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                    //Data for upload

                    bool iBackDate = false;
                    if (tempGCN.IS_BACK_DATE != null && tempGCN.IS_BACK_DATE.Equals("1"))
                    {
                        iBackDate = true;
                    }

                    List<SignFileReturnModel> lsFileSigned = previewPDF_GCN(template.TEMP_SIGN, lsFilePdf, true, false, iBackDate, strEvm);

                    if (lsFileSigned != null && lsFileSigned.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFilePdf;
                        //Check nếu onpoint tạo đơn thì call cập nhật trạng thái
                        jValueContract.Add("LINK_ECER", goBusinessCommon.Instance.GetConfigDB("ECER"));
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số gửi mail";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu ";
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail Exception service-signAndSendMail 336", jInput.ToString() + " ---" + ex.ToString());
                return baseResponse;
            }
            return baseResponse;
        }
        public DateTime getDateEff(string strDate)
        {
            DateTime dtEFF = new DateTime();
            try
            {
                dtEFF = DateTime.ParseExact(strDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
            }
            catch
            {
                dtEFF = DateTime.ParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture);
            }
            return dtEFF;
        }

        public BaseResponse signAndSendMail_IDFile(string strOrgCode, string strProductCode, string strPackCode, string strStructCode, string strChannel,
            string strDateBack, string strCerNo, string strGuiID_File, TempConfigModel templateInput) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                TempConfigModel template = new TempConfigModel();
                if (templateInput != null && templateInput.ORG_CODE != null)
                {
                    template = templateInput;
                }
                else
                    template = get_all_template(strOrgCode, strStructCode, strProductCode, strPackCode,
                    "GCN", "HDI_Sys", strChannel.ToUpper(), strDateBack, strEvm);
                if (template != null && template.ORG_CODE != null)
                {
                    List<SignFileReturnModel> lsFilePdf = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    
                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" &&
                    // Start 2022-08-26 By ThienTVB
                    (m.PACK_CODE == strPackCode || string.IsNullOrEmpty(strPackCode))
                    // End 2022-08-26 By ThienTVB
                    ).ToList();
                    TempEmailModel tempGCN = new TempEmailModel();
                    if (lstempGCN == null && lstempGCN.Count == 0)
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + strProductCode;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Mẫu giấy chứng nhận ít hơn số gói được khai báo", strOrgCode + "_" + strProductCode);
                        return baseResponse;
                    }
                    if (template.TEMP_SIGN == null || template.TEMP_SIGN.ID_KEY.Length == 0)
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + strProductCode;
                        goBussinessEmail.Instance.sendEmaiCMS("", "Chưa khai báo quyền ký số", strOrgCode + "_" + strProductCode);
                        return baseResponse;
                    }
                    tempGCN = lstempGCN[0];

                    //dữ liệu được lấy từ trong DB
                    List<JObject> lsDataExt = new List<JObject>();
                    List<JObject> lsDetail = get_Data_GCN(strOrgCode, strProductCode, strCerNo, "HDI_Sys", strEvm, "ID", ref jValueContract, ref lsDataExt);
                    if (lsDetail != null && lsDetail.Any())
                    {
                        JObject item = lsDetail[0];
                        SignFileReturnModel filePdf = new SignFileReturnModel();
                        //string strGuiID_File = Guid.NewGuid().ToString("N");
                        item.Add("URL_FILE", strGuiID_File);
                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                        InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                        lsInsDetailExt.Add(itemExt);
                        if (lsDataExt != null && lsDataExt.Count > 0)
                        {
                            List<JObject> lsDetailExt = new List<JObject>();
                            foreach (JObject objExt in lsDataExt)
                            {
                                if (item["CERTIFICATE_NO"].ToString().Equals(objExt["CERTIFICATE_NO"]?.ToString()))
                                {
                                    lsDetailExt.Add(objExt);
                                }
                            }
                            if (lsDetailExt != null && lsDataExt.Any())
                            {
                                item.Add("DETAIL", JArray.FromObject(lsDetailExt));
                            }
                            else
                            {
                                item.Add("DETAIL", null);
                            }
                        }
                        filePdf = createPDF_GCN(tempGCN, item, strGuiID_File);
                        filePdf.FILE_URL = strGuiID_File;
                        filePdf.USER_NAME = "HDI_Sys";
                        filePdf.dtSignBack = DateTime.ParseExact(item["EFFECTIVE_NUM"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                        lsFilePdf.Add(filePdf);
                    }

                    bool iBackDate = false;
                    if (tempGCN.IS_BACK_DATE != null && tempGCN.IS_BACK_DATE.Equals("1"))
                    {
                        iBackDate = true;
                    }
                    List<SignFileReturnModel> lsFileSigned = signPDF_GCN(template.TEMP_SIGN, lsFilePdf, true, false, iBackDate, strEvm);
                    if (lsFileSigned != null && lsFilePdf.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFileSigned;
                        //if (template.TEMP_EMAIL.Count > 1)
                        //{
                        //    JObject jobjEmail = jValueContract;
                        //    JArray jDetail = JArray.FromObject(lsInsDetailExt);
                        //    jobjEmail.Add("DETAIL", jDetail);
                        //    bool b = sendMail_GCN(template, lsFileSigned, jobjEmail, strEvm);
                        //}
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số id FILE";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu " + strCerNo;
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
            }
            if (!baseResponse.Success)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail businis_service 438", strOrgCode + strProductCode + strPackCode + strStructCode + strChannel + strDateBack + strCerNo + strGuiID_File + baseResponse.ErrorMessage);
            }
            return baseResponse;
        }
        public List<SignFileReturnModel> signAndSendMailBackDate(string strOrrgCode, string strProductCode, string strPackCode, string strStructCode, string strChannel,
            string strDateBack, string strContractCode, string strTypeFind)
        {
            try
            {
                InsurContract iContract = new InsurContract();
                InsurDetail iDetail = new InsurDetail();
                iContract.ORG_CODE = strOrrgCode;
                iContract.PRODUCT_CODE = strProductCode;
                iDetail.PACK_CODE = strPackCode;
                iContract.EFFECTIVE_NUM = strDateBack;
                iContract.CONTRACT_CODE = strContractCode;
                iContract.STRUCT_CODE = strStructCode;
                List<InsurDetail> lsDetails = new List<InsurDetail>() { iDetail };
                BaseResponse baseResponse = signAndSendMail(iContract, lsDetails, "HDI_Sys", strChannel, strTypeFind);
                if (baseResponse.Success)
                {
                    List<SignFileReturnModel> lsFileSigned = JsonConvert.DeserializeObject<List<SignFileReturnModel>>(baseResponse.Data.ToString());
                    return lsFileSigned;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        //lấy toàn bộ mẫu mã gửi mail sms
        public TempConfigModel get_all_template(string strOrgCode, string strStructCode, string strProductCode, string strPackCode, string strTempCode, string strUser, string strChannel, string strDate, string strEvm)
        {
            
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(strEvm))
                {
                    strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                }
                DateTime dtEFF = new DateTime();
                try
                {
                    dtEFF = DateTime.ParseExact(strDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                }catch(Exception ex)
                {
                    try
                    {
                        dtEFF = DateTime.ParseExact(strDate, "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        dtEFF = DateTime.Now;
                    }
                    
                }                
                object[] param = new object[] { strOrgCode, strStructCode, strProductCode + "_" + strTempCode, strProductCode, strPackCode, strUser, strChannel, dtEFF.ToString("dd/MM/yyyy") };
                // Start 2022-07-14 BY ThienTVB
                BaseResponse responseTemplate = new BaseResponse();
                if (strProductCode == "CRACK_SCREEN")
                {
                    string cacheGCN = "Cache_MauGCN_" + strOrgCode + "_" + strProductCode + "_" + strTempCode;
                    if (goBusinessCache.Instance.Exists(cacheGCN))
                    {
                        responseTemplate = goBusinessCache.Instance.Get<BaseResponse>(cacheGCN);
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Get Data mẫu GCN từ cache: " + cacheGCN, Logger.ConcatName(nameClass, nameMethod));
                    }
                    else
                    {
                        responseTemplate = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ALL_TEMP, strEvm, param); //PKG_C_MEDIA.GET_TEMPLATE_ALL
                        goBusinessCache.Instance.Add(cacheGCN, responseTemplate);
                    }
                }
                else
                {
                    responseTemplate = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ALL_TEMP, strEvm, param); //PKG_C_MEDIA.GET_TEMPLATE_ALL
                }
                // End 2022-07-14 BY ThienTVB
                List<TempConfigModel> arrTemplate = goBusinessCommon.Instance.GetData<TempConfigModel>(responseTemplate, 0);
                TempConfigModel tempReturn = new TempConfigModel();
                List<TempEmailModel> arrTemplateMail = goBusinessCommon.Instance.GetData<TempEmailModel>(responseTemplate, 1);
                if (arrTemplateMail != null && arrTemplateMail.Any())
                {
                    tempReturn = arrTemplate[0];
                    List<TempEmailDTModel> mailTemplateDeatil = goBusinessCommon.Instance.GetData<TempEmailDTModel>(responseTemplate, 2);
                    if (mailTemplateDeatil != null && mailTemplateDeatil.Any())
                    {
                        foreach (TempEmailModel item in arrTemplateMail)
                        {
                            List<TempEmailDTModel> lsCT = mailTemplateDeatil.Where(o => o.TEMP_ID == item.TEMP_ID).ToList();
                            item.DETAIL = lsCT;
                        }
                    }
                    tempReturn.TEMP_EMAIL = arrTemplateMail;
                }
                List<TempSMSModel> arrTemplateSMS = goBusinessCommon.Instance.GetData<TempSMSModel>(responseTemplate, 3);
                if (arrTemplateSMS != null && arrTemplateSMS.Any())
                {
                    tempReturn.TEMP_SMS = arrTemplateSMS;
                }
                List<TempSignModel> arrTemplateSign = goBusinessCommon.Instance.GetData<TempSignModel>(responseTemplate, 4);
                if (arrTemplateSign != null && arrTemplateSign.Any())
                {
                    tempReturn.TEMP_SIGN = arrTemplateSign[0];
                }
                if (tempReturn == null && !(tempReturn.TEMP_EMAIL.Any() || tempReturn.TEMP_SMS.Any()))
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.WARN, "Lỗi lấy mẫu đơn vị " + strOrgCode + " sản phẩm " + strProductCode + " # " + strTempCode + strUser + strChannel + strStructCode, Logger.ConcatName(nameClass, nameMethod));
                    return null;
                }
                tempReturn.USER_NAME = strUser;
                return tempReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lấy mẫu đơn vị " + strOrgCode + " sản phẩm " + strProductCode + " # " + strTempCode + strUser + strChannel + strStructCode + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu  businis_service 541", "Lỗi " + strOrgCode + " sản phẩm " + strProductCode + " # " + strTempCode + strUser + strChannel + strStructCode + ex.ToString());
                return null;
            }
        }
        public List<JObject> get_Data_GCN(string strOrgCode, string strProductCode, string strContractCode, string strUser, string strEvm, string strTypeFind, ref JObject jIcontract, ref List<JObject> jDataExt)
        {

            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                object[] paramFind = new object[] { "OPENAPI", strUser, strContractCode, strProductCode, strTypeFind, strProductCode + "_GCN", strOrgCode };
                BaseResponse responseFind = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                List<JObject> objcontract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                if (objcontract != null && objcontract.Any())
                {
                    jIcontract = objcontract[0];
                    List<JObject> objDetail = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);
                    jDataExt = goBusinessCommon.Instance.GetData<JObject>(responseFind, 2);
                    return objDetail;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không lấy được hợp đồng ky số " + strOrgCode + " sản phẩm " + strProductCode + " # " + strContractCode + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Không lấy được hợp đồng ký số businis_service 568", "Lỗi " + strOrgCode + " sản phẩm " + strProductCode + " # " + strContractCode + ex.ToString());
                return null;
            }
        }
        public SignFileReturnModel createPDF_GCN(TempEmailModel tempConfigModel, JObject jValueInsuran, string strGuiID_File)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {
                fileReturn = goBussinessPDF.Instance.createPDF_GCN(tempConfigModel, jValueInsuran, strGuiID_File);

                if (fileReturn == null)
                {
                    //lỗi tạo file pdf
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao file pdf " + jValueInsuran.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao file pdf  businis_service 584", "Lỗi " + jValueInsuran.ToString());
                    return null;
                }
                return fileReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao file pdf " + jValueInsuran.ToString() + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao file pdf  businis_service 592", "Lỗi " + jValueInsuran.ToString() + ex.ToString());
                return null;
            }
        }

        public SignFileReturnModel createPDF_GCN_Preview(TempEmailModel tempConfigModel, JObject jValueInsuran, string strGuiID_File)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {
                fileReturn = goBussinessPDF.Instance.createPDF_GCN_Preview(tempConfigModel, jValueInsuran, strGuiID_File);

                if (fileReturn == null)
                {
                    //lỗi tạo file pdf
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao file pdf " + jValueInsuran.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao file pdf  businis_service 584", "Lỗi " + jValueInsuran.ToString());
                    return null;
                }
                return fileReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao file pdf " + jValueInsuran.ToString() + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao file pdf  businis_service 592", "Lỗi " + jValueInsuran.ToString() + ex.ToString());
                return null;
            }
        }
        public List<SignFileReturnModel> signPDF_GCN(TempSignModel tempConfigSign, List<SignFileReturnModel> lsFilePDf, bool isUpload, bool isBase64, bool isBackDate, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                if(tempConfigSign !=null && tempConfigSign.CER_FILE.Length > 0)
                {
                    List<SignFileReturnModel> lsFile = goBussinessSign.Instance.signGCNDT(tempConfigSign, lsFilePDf, isUpload, isBase64, isBackDate, strEVM);
                    if (lsFile == null || !lsFile.Any())
                    {
                        //lỗi ký số
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so " + JsonConvert.SerializeObject(lsFilePDf), Logger.ConcatName(nameClass, nameMethod));
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so businis_service 608 ", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf));
                        return null;
                    }
                    return lsFile;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi lấy mẫu hiển thị " + JsonConvert.SerializeObject(lsFilePDf), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi lay mẫu hiển thị businis_service 616", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf));
                    return null;
                }    
                
                
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so " + JsonConvert.SerializeObject(lsFilePDf) + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so businis_service 625 ", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf) + ex.ToString());
                return null;
            }
        }

        public List<SignFileReturnModel> previewPDF_GCN(TempSignModel tempConfigSign, List<SignFileReturnModel> lsFilePDf, bool isUpload, bool isBase64, bool isBackDate, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                if (tempConfigSign != null && tempConfigSign.CER_FILE.Length > 0)
                {
                    List<SignFileReturnModel> lsFile = goBussinessSign.Instance.previewGCNDT(tempConfigSign, lsFilePDf, isUpload, isBase64, isBackDate, strEVM);
                    if (lsFile == null || !lsFile.Any())
                    {
                        //lỗi ký số
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so " + JsonConvert.SerializeObject(lsFilePDf), Logger.ConcatName(nameClass, nameMethod));
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so businis_service 608 ", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf));
                        return null;
                    }
                    return lsFile;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi lấy mẫu hiển thị " + JsonConvert.SerializeObject(lsFilePDf), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi lay mẫu hiển thị businis_service 616", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf));
                    return null;
                }


            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so " + JsonConvert.SerializeObject(lsFilePDf) + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi ky so businis_service 625 ", "Lỗi " + JsonConvert.SerializeObject(lsFilePDf) + ex.ToString());
                return null;
            }
        }

        public bool sendMail_GCN(TempConfigModel tempConfigModel, List<SignFileReturnModel> fileSign, JObject jValueInsuran, string strEVM, bool isTest = false)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {
                //KIỂM TRA TEMPLATE ĐỂ XEM CÓ NHỮNG LOẠI NÀO CẦN GỬI ALL-theo hợp đồng, ONE- từng giấy chứng nhận, LIST-gửi cho hợp đồng và liệt kê danh sách giấy chứng nhận
                TempEmailModel itemEmail = tempConfigModel.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL" && (m.TYPE_SEND == nameof(TYPE_SEND.ALLONE) || m.TYPE_SEND == nameof(TYPE_SEND.ALL) || m.TYPE_SEND == nameof(TYPE_SEND.ALLNO))).FirstOrDefault();
                TempEmailModel itemEmailCT = tempConfigModel.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL" && (m.TYPE_SEND == nameof(TYPE_SEND.ONE) || m.TYPE_SEND == nameof(TYPE_SEND.ONENO))).FirstOrDefault();
                List<logEmailModel> lsLogSend = new List<logEmailModel>();
                List<ServiceEmailModel> lsMailSendAll = new List<ServiceEmailModel>();
                //gửi toàn bộ thông tin cho người mua
                ServiceEmailModel emailContract = new ServiceEmailModel();
                List<FileModel> lsFileContract = new List<FileModel>();
                FileModel fileContract = new FileModel();
                emailContract.file = new List<FileModel>();
                string strEmailTo = jValueInsuran["EMAIL"]?.ToString();
                bool bCheckDuplicate = false;
                if (itemEmail != null && itemEmail.TEMP_CODE.Length > 0 && (fileSign.Count > 1 || itemEmailCT == null))
                {
                    if (isTest)
                    {
                        strEmailTo = itemEmail.LIST_REPLACE;
                    }
                    //thực hiện gửi all, allone
                    emailContract = new ServiceEmailModel();
                    //strEmailTo = 
                    if (!string.IsNullOrEmpty(strEmailTo)) //kiểm tra xem có mail để gửi ko
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailTo } };
                        if (!string.IsNullOrEmpty(tempConfigModel.MAIL_BCC))
                        {
                            List<string> lsBCC = tempConfigModel.MAIL_BCC.ToString().Split(';').ToList();
                            List<EmailModel> lsBCCModel = new List<EmailModel>();
                            foreach (string strMail in lsBCC)
                            {
                                EmailModel emaibcccc = new EmailModel();
                                emaibcccc.email = strMail;
                                lsBCCModel.Add(emaibcccc);
                                emailContract.bcc = lsBCCModel;
                            }
                        }
                        emailContract.channel = tempConfigModel.CHANNEL;
                        emailContract.subject = tempConfigModel.DESCRIPTION;
                        string strBody = goBusinessTemplate.Instance.replaceHtmlValueJson(jValueInsuran, itemEmail);
                        emailContract.content = strBody;
                        emailContract.file = new List<FileModel>();
                        if (nameof(TYPE_SEND.ALL).Equals(itemEmail.TYPE_SEND))
                        {
                            foreach (SignFileReturnModel fileTemp in fileSign)
                            {
                                if (itemEmail.TYPE_SEND == nameof(TYPE_SEND.ALLNO))
                                {
                                    emailContract.attach = "NO";
                                }
                                else
                                {
                                    emailContract.attach = "YES";
                                }    
                                fileContract.path = fileTemp.FILE_ID;
                                fileContract.name = fileTemp.FILE_ID;
                                emailContract.file.Add(fileContract);
                                //var newProperty = new JProperty("LINK_GCN", fileTemp.FILE_URL);
                                //jValueInsuran.AddAfterSelf(newProperty);
                            }
                        }
                        lsMailSendAll.Add(emailContract);
                        bCheckDuplicate = true;
                        logEmailModel logSend = new logEmailModel();
                        logSend.BCC = string.IsNullOrEmpty(tempConfigModel.MAIL_BCC) ? "" : tempConfigModel.MAIL_BCC;
                        logSend.CAME_ID = "";
                        logSend.CAMPAIGN_CODE = "LOG_HDI";
                        logSend.REF_ID = jValueInsuran["CONTRACT_NO"]?.ToString();
                        logSend.CONTENT = strBody;
                        logSend.CREATE_BY = tempConfigModel.USER_NAME;
                        logSend.EMAIL = strEmailTo;
                        logSend.ORG_CODE = tempConfigModel.ORG_CODE;
                        logSend.PACK_CODE = tempConfigModel.PACK_CODE;
                        logSend.PRODUCT_CODE = tempConfigModel.PRODUCT_CODE;
                        logSend.STATUS = "CREATE";
                        logSend.SUBJECT = tempConfigModel.DESCRIPTION;
                        logSend.TYPE_EMAIL = "GCN";
                        lsLogSend.Add(logSend);

                    }
                    //bool b = sendMailAll(lsMailSendAll, lsLogSend, tempConfigModel.ORG_CODE, strEVM);
                }

                if (itemEmailCT != null && itemEmailCT.TEMP_CODE.Length > 0)
                {
                    JArray jarrValueDetail = JArray.Parse(jValueInsuran["DETAIL"]?.ToString());
                    foreach (JToken jTkValue in jarrValueDetail)
                    {
                        emailContract = new ServiceEmailModel();
                        string strEmailToCT = jTkValue["EMAIL"]?.ToString();
                        if (isTest)
                        {
                            strEmailToCT = itemEmailCT.LIST_REPLACE;
                        }
                        if (!string.IsNullOrEmpty(strEmailToCT) || (string.IsNullOrEmpty(strEmailToCT) && jarrValueDetail.Count == 1)) //kiểm tra xem có mail để gửi ko
                        {
                            if (string.IsNullOrEmpty(strEmailToCT) && jarrValueDetail.Count == 1 && !string.IsNullOrEmpty(strEmailTo))
                            {
                                strEmailToCT = strEmailTo;
                            }
                            if (strEmailToCT.Equals(strEmailTo) && bCheckDuplicate)
                            {

                            }
                            else
                            {
                                if (fileSign.Count == 1 && !string.IsNullOrEmpty(strEmailTo) && !strEmailTo.Equals(strEmailToCT))
                                    emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailToCT }, new EmailModel { email = strEmailTo } };
                                else
                                    emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailToCT } };
                                if (!string.IsNullOrEmpty(tempConfigModel.MAIL_BCC))
                                {
                                    List<string> lsBCC = tempConfigModel.MAIL_BCC.ToString().Split(';').ToList();
                                    List<EmailModel> lsBCCModel = new List<EmailModel>();
                                    foreach (string strMail in lsBCC)
                                    {
                                        EmailModel emaibcccc = new EmailModel();
                                        emaibcccc.email = strMail;
                                        lsBCCModel.Add(emaibcccc);
                                        emailContract.bcc = lsBCCModel;
                                    }
                                }
                                emailContract.channel = tempConfigModel.CHANNEL;
                                emailContract.subject = tempConfigModel.DESCRIPTION;

                                emailContract.file = new List<FileModel>();
                                fileContract = new FileModel();
                                foreach (SignFileReturnModel fileTemp in fileSign)
                                {
                                    if (fileTemp.FILE_REF.Equals(jTkValue["CERTIFICATE_NO"].ToString()))
                                    {
                                        fileContract.path = fileTemp.FILE_ID;
                                        fileContract.name = fileTemp.FILE_ID;
                                        if(itemEmailCT.TYPE_SEND == nameof(TYPE_SEND.ONENO))
                                        {
                                            emailContract.attach = "NO";
                                        }
                                        else
                                        {
                                            emailContract.attach = "YES";
                                        }
                                        emailContract.file.Add(fileContract);
                                        //var newProperty = new JProperty("LINK_GCN", fileTemp.FILE_URL);
                                        //jTkValue.AddAfterSelf(newProperty);
                                    }
                                }

                                string strBody = goBusinessTemplate.Instance.replaceHtmlValueJson((JObject)jTkValue, itemEmailCT);
                                emailContract.content = strBody;
                                lsMailSendAll.Add(emailContract);
                                logEmailModel logSend = new logEmailModel();
                                logSend.BCC = string.IsNullOrEmpty(tempConfigModel.MAIL_BCC) ? "" : tempConfigModel.MAIL_BCC.ToString();
                                logSend.CAME_ID = "";
                                logSend.CAMPAIGN_CODE = "LOG_HDI";
                                logSend.REF_ID = jTkValue["CERTIFICATE_NO"].ToString();
                                logSend.CONTENT = strBody;
                                logSend.CREATE_BY = tempConfigModel.USER_NAME;
                                logSend.EMAIL = strEmailToCT;
                                logSend.ORG_CODE = tempConfigModel.ORG_CODE;
                                logSend.PACK_CODE = tempConfigModel.PACK_CODE;
                                logSend.PRODUCT_CODE = tempConfigModel.PRODUCT_CODE;
                                logSend.STATUS = "CREATE";
                                logSend.SUBJECT = tempConfigModel.DESCRIPTION;
                                logSend.TYPE_EMAIL = "GCN";
                                lsLogSend.Add(logSend);
                            }

                        }
                    }
                }
                bool bOne = sendMailAll(lsMailSendAll, lsLogSend, tempConfigModel.ORG_CODE, strEVM);
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lấy mẫu  send mail " + JsonConvert.SerializeObject(tempConfigModel) + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu  send mail", "Lỗi " + isTest.ToString() + "_" + JsonConvert.SerializeObject(tempConfigModel) + ex.ToString());
                return false;
            }
        }
        public bool sendMailAll(List<ServiceEmailModel> lsEmailSend, List<logEmailModel> lsLogSend, string strOrgCode, string strEVM)
        {
            try
            {
                List<BaseRequest> lsBaseReq = new List<BaseRequest>();
                if(lsEmailSend !=null && lsEmailSend.Any())
                {
                    goBussinessEmail cls = new goBussinessEmail();
                    foreach (ServiceEmailModel item in lsEmailSend)
                    {
                        BaseRequest requestSendMail = new BaseRequest();
                        requestSendMail.Action = new ActionInfo();
                        requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                        requestSendMail.Action.ParentCode = "HDI_PRIVATE";
                        requestSendMail.Action.UserName = "HDI_PRIVATE";
                        requestSendMail.Data = JsonConvert.SerializeObject(item);
                        //cls.GetResponseEmail(requestSendMail);
                        new Task(() => cls.GetResponseEmail(requestSendMail)).Start();
                        Thread.Sleep(1000);
                    }
                    //goBussinessEmail.Instance.createLogSendMailJson(lsLogSend, strOrgCode, strEVM);
                    new Task(() => goBussinessEmail.Instance.createLogSendMailJson(lsLogSend, strOrgCode, strEVM)).Start();
                }
                
            }
            catch (Exception ex)
            {

            }
            return true;
        }
        public bool sendSMS(TempSMSModel tempSMS, TempConfigModel tempConfig, JObject jValueInsuran, string strUser, List<SignFileReturnModel> lsFileSigned)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            try
            {
                if (tempSMS != null && tempSMS.TEMP_CODE !=null)
                {                    
                    string strBody = "";
                    List<string> lsReplace = tempSMS.PARAM_REPLACE.Split(';').ToList();
                    string strPhone = "";
                    string strRef = "";
                    if (tempSMS.TYPE_SEND.Equals("ALL"))
                    {
                        strBody = tempSMS.DATA.ToString();
                        strPhone = jValueInsuran["PHONE"]?.ToString();
                        strRef = jValueInsuran["CONTRACT_NO"]?.ToString();
                        //vòng for replate từng tham số
                        if (!string.IsNullOrEmpty(strPhone))
                        {
                            var prt = jValueInsuran.Properties();
                            foreach (string strCTRP in lsReplace)
                            {
                                foreach (var prtct in prt)
                                {
                                    if (prtct.Name.Equals(strCTRP))
                                    {
                                        strBody = strBody.Replace("@" + strCTRP + "@", jValueInsuran[strCTRP]?.ToString().Trim());
                                        break;
                                    }
                                }
                            }
                            //xử lý trường hợp có short-link
                            if (strBody.IndexOf("@LINK@") > 1)
                            {
                                //lấy link giấy chứng nhận để xử lý 
                                SignFileReturnModel fileMd = lsFileSigned.Where(m => m.FILE_REF == strRef).ToList()[0];
                                if(fileMd != null)
                                {
                                    string strLink = getUrlShortLink("SL_02", fileMd.FILE_ID, strEvm);
                                    strBody.Replace("@LINK@", strLink);
                                }                                
                                strBody = goBussinessPDF.Instance.clearParram(strBody);
                            }
                            return goBussinessSMS.Instance.sendSMSSystem(tempConfig, strPhone, strBody, strUser, strRef);
                        }
                        return true;
                    }
                    else
                    {
                        //gửi cho từng người trong hợp đồng
                        JArray arrValue = new JArray();
                        arrValue = JArray.Parse(jValueInsuran["DETAIL"].ToString());
                        foreach(JToken jtk in arrValue)
                        {
                            strBody = tempSMS.DATA.ToString();
                            jValueInsuran = (JObject)jtk;
                            strPhone = jValueInsuran["PHONE"].ToString();
                            strRef = jValueInsuran["CERTIFICATE_NO"].ToString();
                            if (!string.IsNullOrEmpty(strPhone))
                            {
                                var prt = jValueInsuran.Properties();
                                foreach (string strCTRP in lsReplace)
                                {
                                    foreach (var prtct in prt)
                                    {
                                        if (prtct.Name.Equals(strCTRP))
                                        {
                                            strBody = strBody.Replace("@" + strCTRP + "@", jValueInsuran[strCTRP]?.ToString().Trim());
                                            break;
                                        }
                                    }
                                }
                                //xử lý trường hợp có short-link
                                if (strBody.IndexOf("@LINK@") > 1)
                                {
                                    //lấy link giấy chứng nhận để xử lý 
                                    SignFileReturnModel fileMd = lsFileSigned.Where(m => m.FILE_REF == strRef).ToList()[0];
                                    if (fileMd != null)
                                    {
                                        string strLink = getUrlShortLink("SL_02", "id="+ fileMd.FILE_ID, strEvm);
                                        strBody= strBody.Replace("@LINK@", strLink);
                                    }
                                    strBody = goBussinessPDF.Instance.clearParram(strBody);
                                }
                                goBussinessSMS.Instance.sendSMSSystem(tempConfig, strPhone, strBody, strUser, strRef);
                            }
                        }
                        return true;
                    }

                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi sms " + tempConfig.ORG_CODE + " data " + jValueInsuran.ToString() + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi sms  businis_service 848", "Lỗi " + tempConfig.ORG_CODE + " sản phẩm " + " data " + jValueInsuran.ToString() + ex.ToString());
                return false;
            }
            return false;
        }
        public BaseResponse sendSMSPrivateHDI(JObject request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["DATA"].ToString()).ToUpper();
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    JArray arr = JArray.Parse(request["DATA"].ToString());
                    response = sendSmsPrivate(request["ORG_CODE"].ToString(), request["PRODUCT_CODE"].ToString(), request["PACK_CODE"].ToString(), request["STRUCT_CODE"].ToString(),
                        request["TYPE"].ToString(), request["DATE"].ToString(), arr);
                    return response;
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            return response;
        }
        public BaseResponse emailNotifi(JObject request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["ORDER_CODE"].ToString() + request["PRODUCT_CODE"].ToString()).ToUpper();
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    string strOrgCode = request["ORG_CODE"].ToString();
                    string strOrderCode = request["ORDER_CODE"].ToString();
                    int iNum = Convert.ToInt32(request["NUM"].ToString());
                    string strType = request["TYPE"].ToString();
                    string strProductCode = request["PRODUCT_CODE"].ToString();
                    string strPayDate = request["PAY_DATE"].ToString();
                    response = SendMailNotifi(strOrgCode,strOrderCode,iNum,strType,strProductCode,strPayDate);
                    return response;
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            return response;
        }
        public bool sendSMSContentAPI(SMS_CONTENT sms)
        {
            string strUrl= goBusinessCommon.Instance.GetConfigDB(goConstants.SMS_API);
            return goBussinessSMS.Instance.sendSMS_API(sms, strUrl);
        }

        public BaseResponse signAndSendMailBatch(InsurContract iContract, List<InsurDetail> lsDetailsAll, string strUser, string strChannel, string strTypeFind) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();
            JObject jInput = JObject.FromObject(iContract);
            JArray jarrInput = JArray.FromObject(lsDetailsAll);
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", strUser);
            jInput.Add("CHANNEL_CALL", strChannel);
            jInput.Add("TYPE_CALL", strTypeFind);
            try
            {
                //thực hiện tạo toàn bộ pdf
                DateTime dtEFF = getDateEff(iContract.EFFECTIVE_NUM);
                TempConfigModel template = get_all_template(iContract.ORG_SELLER, iContract.STRUCT_CODE, iContract.PRODUCT_CODE, iContract.PACK_CODE,
                    "GCN", strUser, strChannel.ToUpper(), dtEFF.ToString("yyyyMMddHHmmss"), strEvm);
                if (template != null)
                {
                    List<SignFileReturnModel> lsFilePdf = new List<SignFileReturnModel>();
                    List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                    InsurContractExt lsInsContractExt = new InsurContractExt();
                    JObject jValueContract = new JObject();// = JObject.FromObject(iContract);
                    List<string> lsPackCode = lsDetailsAll.Select(m => m.PACK_CODE).Distinct().ToList();
                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF").ToList();
                    TempEmailModel tempGCN = new TempEmailModel();
                    if (lstempGCN == null && lstempGCN.Count == 0 && lsPackCode.Count < (lstempGCN.Count + 1))
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                        goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                        return baseResponse;
                    }
                    //có 2 trường hợp tạo GCN: tạo GCN cho người mua (ALL) và tạo GCN cho từng người được bảo hiểm (ONE)
                    List<JObject> lsDataExt = new List<JObject>();
                    //List<JObject> lsGenGCN = new List<JObject>();
                    Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                    tempGCN = lstempGCN[0];
                    List<JObject> lsDetail = new List<JObject>();
                    if (tempGCN.TYPE_SEND.Equals("ALL"))
                    {

                        switch (tempGCN.DATA_SRC.ToUpper())
                        {
                            case "JSON":
                                {
                                    lsInsDetailExt = getDetailExt(iContract, lsDetailsAll, ref lsInsContractExt);
                                    jValueContract = JObject.FromObject(lsInsContractExt);
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    lsGenGCN.Add(lsInsContractExt.CONTRACT_NO, jValueContract);
                                    break;
                                }
                            case "DATA":
                                {

                                    lsDetail = get_Data_GCN(iContract.ORG_SELLER, iContract.PRODUCT_CODE, iContract.CONTRACT_CODE, strUser, strEvm, strTypeFind, ref jValueContract, ref lsDataExt);
                                    //tạo list insuran exten --> tạo giấy chứng nhận
                                    foreach (JObject item in lsDetail)
                                    {
                                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                                        item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                                        //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                                        item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                                        //InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                                        //lsInsDetailExt.Add(itemExt);
                                        lsGenGCN.Add(iContract.CONTRACT_NO, item);
                                    }
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));

                                    break;
                                }
                            case "PDF":
                                {
                                    break;
                                }
                            case "DOC":
                            case "XSL":
                                {
                                    break;
                                }
                        }
                    }
                    else
                    {
                        switch (tempGCN.DATA_SRC.ToUpper())
                        {
                            case "JSON":
                                {
                                    lsInsDetailExt = getDetailExt(iContract, lsDetailsAll, ref lsInsContractExt);
                                    foreach (InsurDetailExt itExt in lsInsDetailExt)
                                    {
                                        JObject obj = JObject.FromObject(itExt);
                                        obj.Add("DETAIL", null);
                                        lsGenGCN.Add(itExt.CERTIFICATE_NO, obj);
                                    }
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    break;
                                }
                            case "DATA":
                                {

                                    lsDetail = get_Data_GCN(iContract.ORG_SELLER, iContract.PRODUCT_CODE, iContract.CONTRACT_CODE, strUser, strEvm, strTypeFind, ref jValueContract, ref lsDataExt);
                                    //tạo list insuran exten --> tạo giấy chứng nhận
                                    foreach (JObject item in lsDetail)
                                    {
                                        item["AMOUNT"] = item["AMOUNT"].ToString().Replace(".", "");
                                        item["DISCOUNT"] = item["DISCOUNT"].ToString().Replace(".", "");
                                        //item["DISCOUNT_UNIT"] = item["DISCOUNT_UNIT"].ToString().Replace(".", ""); 
                                        item["TOTAL_DISCOUNT"] = item["TOTAL_DISCOUNT"].ToString().Replace(".", "");
                                        item["TOTAL_AMOUNT"] = item["TOTAL_AMOUNT"].ToString().Replace(".", "");
                                        if (lsDataExt != null && lsDataExt.Count > 0)
                                        {
                                            List<JObject> lsDetailExt = new List<JObject>();
                                            foreach (JObject objExt in lsDataExt)
                                            {
                                                if (item["CERTIFICATE_NO"].ToString().Equals(objExt["CERTIFICATE_NO"]?.ToString()))
                                                {
                                                    lsDetailExt.Add(objExt);
                                                }
                                            }
                                            if (lsDetailExt != null && lsDataExt.Any())
                                            {
                                                item.Add("DETAIL", JArray.FromObject(lsDetailExt));
                                            }
                                            else
                                            {
                                                item.Add("DETAIL", null);
                                            }
                                        }
                                        //InsurDetailExt itemExt = JsonConvert.DeserializeObject<InsurDetailExt>(item.ToString());
                                        //lsInsDetailExt.Add(itemExt);
                                        lsGenGCN.Add(item["CERTIFICATE_NO"].ToString(), item);
                                    }
                                    //jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                                    break;
                                }
                            case "PDF":
                                {

                                    break;
                                }
                            case "DOC":
                            case "XSL":
                                {
                                    break;
                                }
                        }
                    }
                    SignFileReturnModel filePdf = new SignFileReturnModel();
                    int i = 1;
                    string strUrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/";
                    if (tempGCN.TYPE_SEND.Equals("ALL"))
                    {

                        if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                            goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                            return baseResponse;
                        }
                        string strGuiID_File = Guid.NewGuid().ToString("N");
                        //itemExt.URL_FILE = strGuiID_File;
                        filePdf = createPDF_GCN(tempGCN, lsGenGCN[iContract.CONTRACT_NO], strGuiID_File);
                        filePdf.dtSignBack = dtEFF;
                        filePdf.USER_NAME = strUser;
                        filePdf.FILE_URL = strGuiID_File;
                        lsFilePdf.Add(filePdf);

                        if (tempGCN.DATA_SRC.Equals("JSON"))
                        {
                            foreach (InsurDetailExt itemExt in lsInsDetailExt)
                            {
                                itemExt.URL_FILE = strUrl + strGuiID_File;
                                itemExt.STT = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                        }
                        else
                        {
                            foreach (JObject item in lsDetail)
                            {
                                item["URL_FILE"] = strUrl + strGuiID_File;
                                item["STT"] = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                        }

                    }
                    else
                    {
                        if (tempGCN.DATA_SRC.Equals("JSON"))
                        {
                            foreach (InsurDetailExt itemExt in lsInsDetailExt)
                            {
                                tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && m.PACK_CODE == itemExt.PACK_CODE).FirstOrDefault();
                                if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                                {
                                    baseResponse.Success = false;
                                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE + itemExt.PACK_CODE;
                                    goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + "_" + itemExt.PACK_CODE);
                                    return baseResponse;
                                }
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //itemExt.URL_FILE = strGuiID_File;
                                filePdf = createPDF_GCN(tempGCN, lsGenGCN[itemExt.CERTIFICATE_NO], strGuiID_File);
                                filePdf.dtSignBack = dtEFF;
                                filePdf.USER_NAME = strUser;
                                filePdf.FILE_URL = strGuiID_File;
                                lsFilePdf.Add(filePdf);
                                itemExt.URL_FILE = strUrl + strGuiID_File;
                                itemExt.STT = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));
                        }
                        else
                        {
                            foreach (JObject item in lsDetail)
                            {
                                tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && m.PACK_CODE == item["PACK_CODE"].ToString()).FirstOrDefault();
                                if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                                {
                                    baseResponse.Success = false;
                                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE + item["PACK_CODE"].ToString();
                                    goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận service-signAndSendMail 203", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + "_" + item["PACK_CODE"].ToString());
                                    return baseResponse;
                                }
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //itemExt.URL_FILE = strGuiID_File;
                                filePdf = createPDF_GCN(tempGCN, lsGenGCN[item["CERTIFICATE_NO"].ToString()], strGuiID_File);
                                filePdf.dtSignBack = dtEFF;
                                filePdf.USER_NAME = strUser;
                                filePdf.FILE_URL = strGuiID_File;
                                lsFilePdf.Add(filePdf);
                                item["URL_FILE"] = strUrl + strGuiID_File;
                                item["STT"] = i.ToString();
                                i++;
                            }
                            jValueContract.Add("DETAIL", JArray.FromObject(lsDetail));
                        }


                    }
                    bool iBackDate = false;
                    if (tempGCN.IS_BACK_DATE != null && tempGCN.IS_BACK_DATE.Equals("1"))
                    {
                        iBackDate = true;
                    }
                    List<SignFileReturnModel> lsFileSigned = signPDF_GCN(template.TEMP_SIGN, lsFilePdf, true, false, iBackDate, strEvm);
                    if (lsFileSigned != null && lsFilePdf.Any())
                    {
                        baseResponse.Success = true;
                        baseResponse.Data = lsFileSigned;

                        if (template.TEMP_EMAIL.Count > 1)
                        {

                            bool b = sendMail_GCN(template, lsFileSigned, jValueContract, strEvm);
                            if (!b)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            }
                        }
                        if (template.TEMP_SMS != null)
                        {
                            List<TempSMSModel> lsSMS = template.TEMP_SMS.Where(m => m.TEMP_CODE == "GCN").ToList();
                            if (lsSMS != null && lsSMS.Any())
                            {
                                TempSMSModel tSMS = lsSMS[0];
                                if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && tSMS.SEND_NOW.Equals("1"))
                                {
                                    //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                                    bool bSMS = bSMS = sendSMS(tSMS, template, jValueContract, strUser, lsFileSigned);
                                    if (tSMS.SEND_NOW.Equals("1"))//chỉ dành cho test
                                    {

                                    }
                                    if (!bSMS)
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                    }
                                }
                            }
                        }


                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Lỗi ký số gửi mail";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không tìm thấy mẫu ";
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail Exception service-signAndSendMail 336", jInput.ToString() + " ---" + ex.ToString());
                return baseResponse;
            }
            //if (!baseResponse.Success)
            //{
            //    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail service-signAndSendMail 340", jInput.ToString());
            //}
            return baseResponse;
        }
        public BaseResponse sendSMSContenHDI(SMS_CONTENT request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request.PHONE + request.PRODUCT_CODE + request.ORG_CODE).ToUpper();
                if (strMD5.Equals(request.TOKEN.ToString()))
                {
                    response = sendSmsContent(request.ORG_CODE, request.PRODUCT_CODE, request.PACK_CODE, request.STRUCT_CODE,
                        request.TYPE, request.DATE,request.CONTENT, request.PHONE,request.REF_ID);
                    return response;
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            return response;
        }
        public BaseResponse sendSmsPrivate(string strOrgCode, string strProduct_code, string strPackCode, string strStrucCode, string strType, string strDate, JArray arrValue)
        {
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                TempConfigModel template = get_all_template(strOrgCode, strStrucCode, strProduct_code, strPackCode,
                        strType, "HDI_PRIVATE", "WEB", strDate, "LIVE");
                if (template != null && template.TEMP_SMS !=null && template.TEMP_SMS.Count >0)
                {
                    TempSMSModel tempSMS = template.TEMP_SMS[0];
                    string strBody = "";
                    List<string> lsReplace = tempSMS.PARAM_REPLACE.Split(';').ToList();
                    string strPhone = "";
                    string strRef = "";
                    foreach (JToken jtk in arrValue)
                    {
                        strBody = tempSMS.DATA.ToString();
                        JObject jValueInsuran = (JObject)jtk;
                        strPhone = jValueInsuran["PHONE"].ToString();
                        strRef = jValueInsuran["CERTIFICATE_NO"].ToString();
                        if (!string.IsNullOrEmpty(strPhone))
                        {
                            var prt = jValueInsuran.Properties();
                            foreach (string strCTRP in lsReplace)
                            {
                                foreach (var prtct in prt)
                                {
                                    if (prtct.Name.Equals(strCTRP))
                                    {
                                        strBody = strBody.Replace("@" + strCTRP + "@", jValueInsuran[strCTRP]?.ToString().Trim());
                                        break;
                                    }
                                }
                            }
                            //xử lý trường hợp có short-link
                            if (strBody.IndexOf("@LINK@") > 1)
                            {
                                string strLink = getUrlShortLink("SL_02", "id=" + jValueInsuran["FILE_ID"].ToString(), "LIVE");
                                strBody = strBody.Replace("@LINK@", strLink);                            
                                strBody = goBussinessPDF.Instance.clearParram(strBody);
                            }
                            // kiểm tra xem gửi bằng hàm hay api
                            Thread.Sleep(2000);
                            bool b = goBussinessSMS.Instance.sendSMSSystem(template, strPhone, strBody, "HDI_PRIVATE", strRef); //strPhone
                            if (b)
                            {
                                baseResponse.Success = true;
                                baseResponse.ErrorMessage = "Thành công";
                            }else
                            {
                                baseResponse.Success = false;
                                baseResponse.ErrorMessage = "Lỗi gửi mail";
                            }    
                        }
                        else
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "Không có số điện thoại";
                        }
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không cấu hình mẫu gửi sms";
                }

            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = "Lỗi gửi sms " + ex.Message;
                
            }
            return baseResponse;
        }

        public BaseResponse sendSmsContent(string strOrgCode, string strProduct_code, string strPackCode, string strStrucCode, string strType, string strDate,
            string strContent, string strPhone, string strRef)
        {
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                TempConfigModel template = get_all_template(strOrgCode, strStrucCode, strProduct_code, strPackCode,
                        strType, "HDI_PRIVATE", "WEB", strDate, "LIVE");
                if (template != null && template.TEMP_SMS != null && template.TEMP_SMS.Count > 0)
                {
                    TempSMSModel tempSMS = template.TEMP_SMS[0];
                    string strBody = tempSMS.DATA.ToString();
                    if (strBody.StartsWith(strContent.Substring(0, 12)))
                    {
                        bool b = goBussinessSMS.Instance.sendSMSSystem(template, strPhone, strContent, "HDI_PRIVATE", strRef); //strPhone
                        if (b)
                        {
                            baseResponse.Success = true;
                            baseResponse.ErrorMessage = "Thành công";
                        }
                        else
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "Lỗi gửi mail";
                        }
                    }
                    else
                    {
                        baseResponse.Success = false;
                        baseResponse.ErrorMessage = "Không cấu hình mẫu gửi SMS";
                    }
                }
                else
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "Không cấu hình mẫu gửi sms";
                }

            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = "Lỗi gửi sms " + ex.Message;

            }
            return baseResponse;
        }

        //public void callBackOrg(JObject jInputOrg, string strTypeMethod)
        //{
        //    string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    try
        //    {
        //        NotifyConfigModel config = new NotifyConfigModel();
        //        BaseResponse responseData = new BaseResponse();
        //        string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        //        switch (strTypeMethod)
        //        {
        //            //sau khi cấp đơn sinh số giấy chứng nhận thành công
        //            case "IPN_CER":
        //                {
        //                    string strOrg = jInputOrg["ORG_CODE"].ToString();
        //                    if (strOrg.Equals("CREDIFYVN"))
        //                    {
        //                        PayCallBackModels orgCallback = goLibPay.Instance.getCallBack(strOrg, "", "", "WEB", strTypeMethod);

        //                    }

        //                    PayAssignModels trans = JsonConvert.DeserializeObject<PayAssignModels>(JsonConvert.SerializeObject(jInputNotify));
        //                    //string strUrlPay = jInputNotify[""].ToString();
        //                    //string strQrpay = jInputNotify[""].ToString();

        //                    config = getConfig(trans.ORG_SELLER, trans.PRODUCT_CODE, "WEB", strNotifyType);
        //                    if (config != null)
        //                    {
        //                        //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
        //                        TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
        //                        if (temp != null)
        //                        {
        //                            //tạo dữ liệu gửi
        //                            JObject JValue = JObject.Parse(jInputNotify["result"].ToString());
        //                            if (config.SEND_TYPE.Equals("EMAIL"))
        //                            {
        //                                JObject jdataSend = new JObject();
        //                                jdataSend.Add("CONTRACT_NO", JValue["orderId"].ToString());
        //                                jdataSend.Add("EMAIL", JValue["payerEmail"].ToString());
        //                                DateTime dt = goUtility.Instance.ConvertToDateTimePay(JValue["dtExp"].ToString(), "yyMMddHHmm");
        //                                jdataSend.Add("DATE", dt.ToString("dd/MM/yyyy HH:mm"));
        //                                jdataSend.Add("NAME", JValue["payerName"].ToString());
        //                                jdataSend.Add("NAME_EN", goUtility.Instance.RemoveMark(JValue["payerName"].ToString().ToUpper()));
        //                                jdataSend.Add("LINK", JValue["url_redirect"].ToString());
        //                                jdataSend.Add("AMOUNT", JValue["amount"].ToString());
        //                                jdataSend.Add("HH", dt.ToString("HH"));
        //                                jdataSend.Add("SS", dt.ToString("mm"));
        //                                jdataSend.Add("DD", dt.ToString("dd"));
        //                                jdataSend.Add("MM", dt.ToString("MM"));
        //                                jdataSend.Add("MM_EN", dt.ToString("MMMM"));
        //                                jdataSend.Add("YY", dt.ToString("yyyy"));
        //                                //jdataSend.Add("QR", JValue["imageQR"].ToString());
        //                                List<SignFileReturnModel> fileSign = new List<SignFileReturnModel>() { new SignFileReturnModel { FILE_ID = "1" } };
        //                                bool b = sendMail_GCN(temp, fileSign, jdataSend, strEvm);
        //                                if (!b)
        //                                {
        //                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
        //                                }
        //                                //insert log 
        //                                string[] param = new string[] { trans.ORG_SELLER, config.TEMP_ID, "ORDER_CODE", JValue["orderId"].ToString(), JValue["payerEmail"].ToString(), JsonConvert.SerializeObject(jdataSend), config.MAX.ToString() };
        //                                responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
        //                            }

        //                        }

        //                    }

        //                    break;
        //                }
        //            case "SEND_CLAIM":
        //                {
        //                    //Xử lý thông báo thanh toán cho khách hàng
        //                    PayAssignModels trans = JsonConvert.DeserializeObject<PayAssignModels>(JsonConvert.SerializeObject(jInputNotify));
        //                    //string strUrlPay = jInputNotify[""].ToString();
        //                    //string strQrpay = jInputNotify[""].ToString();

        //                    config = getConfig(trans.ORG_SELLER, trans.PRODUCT_CODE, "WEB", strNotifyType);
        //                    if (config != null)
        //                    {
        //                        //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
        //                        TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
        //                        if (temp != null)
        //                        {
        //                            //tạo dữ liệu gửi
        //                            JObject JValue = JObject.Parse(jInputNotify["result"].ToString());
        //                            if (config.SEND_TYPE.Equals("EMAIL"))
        //                            {
        //                                JObject jdataSend = new JObject();
        //                                jdataSend.Add("CONTRACT_NO", JValue["orderId"].ToString());
        //                                jdataSend.Add("EMAIL", JValue["payerEmail"].ToString());
        //                                DateTime dt = goUtility.Instance.ConvertToDateTimePay(JValue["dtExp"].ToString(), "yyMMddHHmm");
        //                                jdataSend.Add("DATE", dt.ToString("dd/MM/yyyy HH:mm"));
        //                                jdataSend.Add("NAME", JValue["payerName"].ToString());
        //                                jdataSend.Add("NAME_EN", goUtility.Instance.RemoveMark(JValue["payerName"].ToString().ToUpper()));
        //                                jdataSend.Add("LINK", JValue["url_redirect"].ToString());
        //                                jdataSend.Add("AMOUNT", JValue["amount"].ToString());
        //                                jdataSend.Add("HH", dt.ToString("HH"));
        //                                jdataSend.Add("SS", dt.ToString("mm"));
        //                                jdataSend.Add("DD", dt.ToString("dd"));
        //                                jdataSend.Add("MM", dt.ToString("MM"));
        //                                jdataSend.Add("MM_EN", dt.ToString("MMMM"));
        //                                jdataSend.Add("YY", dt.ToString("yyyy"));
        //                                //jdataSend.Add("QR", JValue["imageQR"].ToString());
        //                                List<SignFileReturnModel> fileSign = new List<SignFileReturnModel>() { new SignFileReturnModel { FILE_ID = "1" } };
        //                                bool b = sendMail_GCN(temp, fileSign, jdataSend, strEvm);
        //                                if (!b)
        //                                {
        //                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
        //                                }
        //                                //insert log 
        //                                string[] param = new string[] { trans.ORG_SELLER, config.TEMP_ID, "ORDER_CODE", JValue["orderId"].ToString(), JValue["payerEmail"].ToString(), JsonConvert.SerializeObject(jdataSend), config.MAX.ToString() };
        //                                responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
        //                            }

        //                        }

        //                    }

        //                    break;
        //                }
        //            case "IPN_SMS":
        //                {
        //                    //Xử lý thông báo thanh toán cho khách hàng
        //                    PayAssignModels trans = JsonConvert.DeserializeObject<PayAssignModels>(JsonConvert.SerializeObject(jInputNotify));
        //                    config = getConfig(trans.ORG_SELLER, trans.PRODUCT_CODE, "WEB", strNotifyType);
        //                    if (config != null)
        //                    {
        //                        //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
        //                        TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
        //                        if (temp != null)
        //                        {
        //                            //tạo dữ liệu gửi
        //                            JObject JValue = JObject.Parse(jInputNotify["result"].ToString());
        //                            if (config.SEND_TYPE.Equals("EMAIL"))
        //                            {
        //                                JObject jdataSend = new JObject();
        //                                jdataSend.Add("CONTRACT_NO", JValue["orderId"].ToString());
        //                                jdataSend.Add("EMAIL", JValue["payerEmail"].ToString());
        //                                DateTime dt = goUtility.Instance.ConvertToDateTimePay(JValue["dtExp"].ToString(), "yyMMddHHmm");
        //                                jdataSend.Add("DATE", dt.ToString("dd/MM/yyyy HH:mm"));
        //                                jdataSend.Add("NAME", JValue["payerName"].ToString());
        //                                jdataSend.Add("NAME_EN", goUtility.Instance.RemoveMark(JValue["payerName"].ToString().ToUpper()));
        //                                jdataSend.Add("LINK", JValue["url_redirect"].ToString());
        //                                jdataSend.Add("AMOUNT", JValue["amount"].ToString());
        //                                jdataSend.Add("HH", dt.ToString("HH"));
        //                                jdataSend.Add("SS", dt.ToString("mm"));
        //                                jdataSend.Add("DD", dt.ToString("dd"));
        //                                jdataSend.Add("MM", dt.ToString("MM"));
        //                                jdataSend.Add("MM_EN", dt.ToString("MMMM"));
        //                                jdataSend.Add("YY", dt.ToString("yyyy"));
        //                                //jdataSend.Add("QR", JValue["imageQR"].ToString());
        //                                List<SignFileReturnModel> fileSign = new List<SignFileReturnModel>() { new SignFileReturnModel { FILE_ID = "1" } };
        //                                bool b = sendMail_GCN(temp, fileSign, jdataSend, strEvm);
        //                                if (!b)
        //                                {
        //                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
        //                                }
        //                                //insert log 
        //                                string[] param = new string[] { trans.ORG_SELLER, config.TEMP_ID, "ORDER_CODE", JValue["orderId"].ToString(), JValue["payerEmail"].ToString(), JsonConvert.SerializeObject(jdataSend), config.MAX.ToString() };
        //                                responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
        //                            }

        //                        }

        //                    }

        //                    break;
        //                }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi callNotify " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
        //    }
        //}


        public void callNotify(JObject jInputNotify, string strNotifyType)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                NotifyConfigModel config = new NotifyConfigModel();
                BaseResponse responseData = new BaseResponse();
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                switch (strNotifyType)
                {
                    case "TBTT":
                        {
                            //Xử lý thông báo thanh toán cho khách hàng
                            PayAssignModels trans = JsonConvert.DeserializeObject<PayAssignModels>(JsonConvert.SerializeObject(jInputNotify));
                            //string strUrlPay = jInputNotify[""].ToString();
                            //string strQrpay = jInputNotify[""].ToString();

                            config = getConfig(trans.ORG_SELLER, trans.PRODUCT_CODE, "WEB", strNotifyType);
                            if (config != null)
                            {
                                //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
                                TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
                                if (temp != null)
                                {
                                    //tạo dữ liệu gửi
                                    JObject JValue = JObject.Parse(jInputNotify["result"].ToString());
                                    if (config.SEND_TYPE.Equals("EMAIL"))
                                    {
                                        JObject jdataSend = new JObject();
                                        jdataSend.Add("CONTRACT_NO", JValue["orderId"].ToString());
                                        jdataSend.Add("EMAIL", JValue["payerEmail"].ToString());
                                        DateTime dt = goUtility.Instance.ConvertToDateTimePay(JValue["dtExp"].ToString(), "yyMMddHHmm");
                                        jdataSend.Add("DATE", dt.ToString("dd/MM/yyyy HH:mm"));
                                        jdataSend.Add("NAME", JValue["payerName"].ToString());
                                        jdataSend.Add("NAME_EN", goUtility.Instance.RemoveMark(JValue["payerName"].ToString().ToUpper()));
                                        jdataSend.Add("LINK", JValue["url_redirect"].ToString());
                                        jdataSend.Add("AMOUNT", JValue["amount"].ToString());
                                        jdataSend.Add("HH", dt.ToString("HH"));
                                        jdataSend.Add("SS", dt.ToString("mm"));
                                        jdataSend.Add("DD", dt.ToString("dd"));
                                        jdataSend.Add("MM", dt.ToString("MM"));
                                        jdataSend.Add("MM_EN", dt.ToString("MMMM"));
                                        jdataSend.Add("YY", dt.ToString("yyyy"));
                                        //jdataSend.Add("QR", JValue["imageQR"].ToString());
                                        List<SignFileReturnModel> fileSign = new List<SignFileReturnModel>() { new SignFileReturnModel { FILE_ID = "1" } };
                                        bool b = sendMail_GCN(temp, fileSign, jdataSend, strEvm);
                                        if (!b)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
                                        }
                                        //insert log 
                                        string[] param = new string[] { trans.ORG_SELLER, config.ID, "ORDER_CODE", JValue["orderId"].ToString() , JValue["payerEmail"].ToString(), JsonConvert.SerializeObject(jdataSend), config.MAX.ToString()};
                                        responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
                                    }

                                }

                            }

                            break;
                        }
                    case "TTK_EMAIL":
                        {
                            config = getConfig(jInputNotify["ORG_CODE"].ToString(), strNotifyType, "WEB", strNotifyType);
                            if (config != null)
                            {
                                //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
                                TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
                                if (temp != null)
                                {
                                    //tạo dữ liệu gửi
                                    if (config.SEND_TYPE.Equals("EMAIL"))
                                    {
                                        JObject jdataSend = new JObject();
                                        jdataSend.Add("CONTRACT_NO", jInputNotify["USER_NAME"].ToString());
                                        jdataSend.Add("EMAIL", jInputNotify["EMAIL"].ToString());
                                        jdataSend.Add("STAFF_NAME", jInputNotify["STAFF_NAME"].ToString());
                                        jdataSend.Add("STAFF_CODE", jInputNotify["STAFF_CODE"].ToString());
                                        jdataSend.Add("PASSWORD", jInputNotify["PASSWORD"].ToString());
                                        jdataSend.Add("USER_NAME", jInputNotify["USER_NAME"].ToString());
                                        List<SignFileReturnModel> fileSign = new List<SignFileReturnModel>() { new SignFileReturnModel { FILE_ID = "1" } };
                                        bool b = sendMail_GCN(temp, fileSign, jdataSend, strEvm);
                                        if (!b)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
                                        }
                                        //insert log 
                                        string[] param = new string[] { config.ORG_CODE, config.ID, "ORDER_CODE", jdataSend["CONTRACT_NO"].ToString(), jdataSend["EMAIL"].ToString(), JsonConvert.SerializeObject(jdataSend), config.MAX.ToString() };
                                        responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
                                    }

                                }

                            }

                            break;
                        }
                    default:
                        {
                            config = getConfig(jInputNotify["ORG_CODE"].ToString(), jInputNotify["PRODUCT_CODE"].ToString(), "WEB", jInputNotify["NOTIFY_TYPE"].ToString());
                            if (config != null)
                            {
                                //lấy dữ liệu cần gửi đã có dữ liệu đối với trường hợp này
                                TempConfigModel temp = get_template_byID(config.TEMP_ID, strEvm);
                                if (temp != null)
                                {
                                    //tạo dữ liệu gửi
                                    if (config.SEND_TYPE.Equals("EMAIL"))
                                    {   
                                        bool b = sendMail_Notify(temp, jInputNotify, strEvm);
                                        if (!b)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo  " + JsonConvert.SerializeObject(jInputNotify), Logger.ConcatName(nameClass, LoggerName));
                                        }
                                        else
                                        {
                                            string[] param = new string[] { config.ORG_CODE, config.ID, jInputNotify["TYPE_DATA"]?.ToString(), jInputNotify["VALUE_DATA"]?.ToString(), jInputNotify["EMAIL"].ToString(), JsonConvert.SerializeObject(jInputNotify), config.MAX.ToString() };
                                            responseData = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.INSERT_NOTIFI_LOG, strEvm, param);
                                        }
                                    }
                                    if (config.SEND_TYPE.Equals("SMS"))
                                    {
                                        TempSMSModel tSMS = temp.TEMP_SMS[0];
                                        if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && tSMS.SEND_NOW.Equals("1"))
                                        {
                                            //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                                            bool bSMS = sendSMS_Notify(tSMS, temp, jInputNotify, jInputNotify["ORG_CODE"].ToString());
                                            if (!bSMS)
                                            {
                                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi sms  thong bao notify " + jInputNotify.ToString(), Logger.ConcatName(nameClass, LoggerName));
                                            }
                                        }
                                    }
                                }

                            }

                            break;
                        }
                }
            }
            catch(Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi callNotify " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi callNotify  " + strNotifyType, JsonConvert.SerializeObject(jInputNotify) + ex.ToString());
            }
        }
        public bool sendSMS_Notify(TempSMSModel tempSMS, TempConfigModel tempConfig, JObject jValueInsuran, string strUser)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            try
            {
                if (tempSMS != null && tempSMS.TEMP_CODE != null)
                {
                    string strBody = "";
                    List<string> lsReplace = tempSMS.PARAM_REPLACE.Split(';').ToList();
                    string strPhone = "";
                    string strRef = "";
                    
                        strBody = tempSMS.DATA.ToString();
                        strPhone = jValueInsuran["PHONE"]?.ToString();
                        strRef = jValueInsuran["VALUE_DATA"]?.ToString();
                        //vòng for replate từng tham số
                        if (!string.IsNullOrEmpty(strPhone))
                        {
                            var prt = jValueInsuran.Properties();
                            foreach (string strCTRP in lsReplace)
                            {
                                foreach (var prtct in prt)
                                {
                                    if (prtct.Name.Equals(strCTRP))
                                    {
                                        strBody = strBody.Replace("@" + strCTRP + "@", jValueInsuran[strCTRP]?.ToString().Trim());
                                        break;
                                    }
                                }
                            }
                            //xử lý trường hợp có short-link
                            //if (strBody.IndexOf("@LINK@") > 1)
                            //{
                            //    //lấy link giấy chứng nhận để xử lý 
                            //    SignFileReturnModel fileMd = lsFileSigned.Where(m => m.FILE_REF == strRef).ToList()[0];
                            //    if (fileMd != null)
                            //    {
                            //        string strLink = getUrlShortLink("SL_02", fileMd.FILE_ID, strEvm);
                            //        strBody.Replace("@LINK@", strLink);
                            //    }
                            //    strBody = goBussinessPDF.Instance.clearParram(strBody);
                            //}
                            return goBussinessSMS.Instance.sendSMSSystem(tempConfig, strPhone, strBody, strUser, strRef);
                        }
                        return false;
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi sms " + tempConfig.ORG_CODE + " data " + jValueInsuran.ToString() + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi sms  businis_service 2181", "Lỗi " + tempConfig.ORG_CODE + " sản phẩm " + " data " + jValueInsuran.ToString() + ex.ToString());
                return false;
            }
            return false;
        }
        public bool sendMail_Notify(TempConfigModel tempConfigModel, JObject jValueInsuran, string strEVM)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            SignFileReturnModel fileReturn = new SignFileReturnModel();
            try
            {                
                TempEmailModel itemEmailCT = tempConfigModel.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL").FirstOrDefault();
                List<logEmailModel> lsLogSend = new List<logEmailModel>();
                List<ServiceEmailModel> lsMailSendAll = new List<ServiceEmailModel>();
                //gửi toàn bộ thông tin cho người mua
                ServiceEmailModel emailContract = new ServiceEmailModel();
                List<FileModel> lsFileContract = new List<FileModel>();
                FileModel fileContract = new FileModel();
                emailContract.file = new List<FileModel>();
                string strEmailTo = jValueInsuran["EMAIL"]?.ToString();
                if (itemEmailCT != null && itemEmailCT.TEMP_CODE.Length > 0)
                {
                    emailContract = new ServiceEmailModel();
                    if (!string.IsNullOrEmpty(strEmailTo)) //kiểm tra xem có mail để gửi ko
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailTo } };
                        if (!string.IsNullOrEmpty(tempConfigModel.MAIL_BCC))
                        {
                            List<string> lsBCC = tempConfigModel.MAIL_BCC.ToString().Split(';').ToList();
                            List<EmailModel> lsBCCModel = new List<EmailModel>();
                            foreach (string strMail in lsBCC)
                            {
                                EmailModel emaibcccc = new EmailModel();
                                emaibcccc.email = strMail;
                                lsBCCModel.Add(emaibcccc);
                                emailContract.bcc = lsBCCModel;
                            }
                        }
                        emailContract.channel = tempConfigModel.CHANNEL;
                        emailContract.subject = tempConfigModel.DESCRIPTION;
                        string strBody = goBusinessTemplate.Instance.replaceHtmlValueJson(jValueInsuran, itemEmailCT);
                        emailContract.content = strBody;
                        emailContract.file = new List<FileModel>();
                        lsMailSendAll.Add(emailContract);
                        logEmailModel logSend = new logEmailModel();
                        logSend.BCC = string.IsNullOrEmpty(tempConfigModel.MAIL_BCC) ? "" : tempConfigModel.MAIL_BCC;
                        logSend.CAME_ID = "";
                        logSend.CAMPAIGN_CODE = "LOG_HDI";
                        logSend.REF_ID = jValueInsuran["VALUE_DATA"]?.ToString();
                        logSend.CONTENT = strBody;
                        logSend.CREATE_BY = tempConfigModel.USER_NAME;
                        logSend.EMAIL = strEmailTo;
                        logSend.ORG_CODE = tempConfigModel.ORG_CODE;
                        logSend.PACK_CODE = tempConfigModel.PACK_CODE;
                        logSend.PRODUCT_CODE = tempConfigModel.PRODUCT_CODE;
                        logSend.STATUS = "CREATE";
                        logSend.SUBJECT = tempConfigModel.DESCRIPTION;
                        logSend.TYPE_EMAIL = "NOTIFY";
                        lsLogSend.Add(logSend);
                        bool bOne = sendMailAll(lsMailSendAll, lsLogSend, tempConfigModel.ORG_CODE, strEVM);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lấy mẫu  send mail " + JsonConvert.SerializeObject(tempConfigModel) + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu  send mail", "Lỗi "+ "_" + JsonConvert.SerializeObject(tempConfigModel) + ex.ToString());
                return false;
            }
        }
        public NotifyConfigModel getConfig(string strOrg, string strProduct, string strChannel, string strType)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            try
            {

                List<NotifyConfigModel> config = new List<NotifyConfigModel>();
                try
                {
                    config = goBusinessCache.Instance.Get<List<NotifyConfigModel>>("NOTiFY_CONFIG");
                    
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                    config = null;
                }
                if (config == null)
                {
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    string[] param = new string[] { strOrg };
                    response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.GET_NOTIFI_CONFIG, strEvm, param);
                    if (response.Success)
                    {
                        config = goBusinessCommon.Instance.GetData<NotifyConfigModel>(response, 0);
                        if (config != null && config.Any())
                        {                            
                            bool b = goBusinessCache.Instance.Add("NOTiFY_CONFIG", config, true);
                            if (!b)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong day duoc cau hinh don vi thanh toan vào redis", Logger.ConcatName(nameClass, LoggerName));
                                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lib pay redis 76 ", "Khong day duoc cau hinh don vi thanh toan vào redis");
                            }
                        }
                    }
                    else
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lib pay khong tim thay cau hinh thanh toan 81", Logger.ConcatName(nameClass, LoggerName));
                        return null;
                    }
                }
                return config.Where(x => x.ORG_CODE == strOrg && (x.PRODUCT_CODE == strProduct || x.PRODUCT_CODE == null ||x.PRODUCT_CODE =="")
                    && (x.CHANNEL =="WEB" || x.CHANNEL ==strChannel) && x.NOTIFY_TYPE ==strType).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 90 ", ex.Message);
                return null;
            }
        }
        public TempConfigModel get_template_byID(string strTempID, string strEvm)
        {

            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(strEvm))
                {
                    strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                }
                object[] param = new object[] { strTempID };
                BaseResponse responseTemplate = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_ID_TEMP, strEvm, param);
                List<TempConfigModel> arrTemplate = goBusinessCommon.Instance.GetData<TempConfigModel>(responseTemplate, 0);
                TempConfigModel tempReturn = new TempConfigModel();
                List<TempEmailModel> arrTemplateMail = goBusinessCommon.Instance.GetData<TempEmailModel>(responseTemplate, 1);
                if (arrTemplateMail != null && arrTemplateMail.Any())
                {
                    tempReturn = arrTemplate[0];
                    List<TempEmailDTModel> mailTemplateDeatil = goBusinessCommon.Instance.GetData<TempEmailDTModel>(responseTemplate, 2);
                    if (mailTemplateDeatil != null && mailTemplateDeatil.Any())
                    {
                        foreach (TempEmailModel item in arrTemplateMail)
                        {
                            List<TempEmailDTModel> lsCT = mailTemplateDeatil.Where(o => o.TEMP_ID == item.TEMP_ID).ToList();
                            item.DETAIL = lsCT;
                        }
                    }
                    tempReturn.TEMP_EMAIL = arrTemplateMail;
                }
                List<TempSMSModel> arrTemplateSMS = goBusinessCommon.Instance.GetData<TempSMSModel>(responseTemplate, 3);
                if (arrTemplateSMS != null && arrTemplateSMS.Any())
                {
                    tempReturn.TEMP_SMS = arrTemplateSMS;
                }
                if (tempReturn == null && !(tempReturn.TEMP_EMAIL.Any() || tempReturn.TEMP_SMS.Any()))
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.WARN, "Lỗi lấy mẫu id temp " + strTempID, Logger.ConcatName(nameClass, nameMethod));
                    return null;
                }
                tempReturn.USER_NAME = "";
                return tempReturn;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi lấy mẫu id temp " + strTempID + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy mẫu id temp " + strTempID , ex.ToString());
                return null;
            }
        }
        public BaseResponse SendMailNotifi(string strOrgCode,string strOrderCode,int iNum, string strType, string strProductCode, string strPayDate) ///NO
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse responseData = new BaseResponse();
            //Lấy danh sách dữ liệu cần xử lý
            try
            {
                object[] param = new object[] { strOrgCode, strOrderCode, iNum, strType, strProductCode };
                responseData = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.GET_DATA_NOTIFI, strEvm, param); //PKG_C_MEDIA.GET_TEMPLATE_ALL
                List<JObject> arrOrders = goBusinessCommon.Instance.GetData<JObject>(responseData, 0);
                List<JObject> arrDetailOrder = goBusinessCommon.Instance.GetData<JObject>(responseData, 1);
                List<JObject> arrContracts = goBusinessCommon.Instance.GetData<JObject>(responseData, 2);
                List<JObject> arrDetailContract = goBusinessCommon.Instance.GetData<JObject>(responseData, 3);
                if (arrOrders != null && arrOrders.Any() && arrDetailOrder != null && arrDetailOrder.Any() &&
                    arrContracts != null && arrContracts.Any() && arrDetailContract != null && arrDetailContract.Any())
                {
                    TempConfigModel template = get_all_template(strOrgCode, "STRUCT_HDI", strProductCode, "null",
                    strType, "HDI_TB", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm); //strType-TBAO
                    if (template != null)
                    {
                        List<JObject> lsInsert = new List<JObject>();
                        foreach(JObject jobOrder in arrDetailOrder)
                        {
                            JObject jObjInsertData = new JObject();
                            JObject jObjContract = new JObject();
                            JObject jObjDetail = new JObject();
                            List<JObject> lsDetail = new List<JObject>();
                            foreach(JObject objCT in arrContracts)
                            {
                                if (jobOrder["REF_ID"].ToString().Equals(objCT["CONTRACT_CODE"].ToString()))
                                {
                                    jObjContract = objCT;
                                    break;
                                }
                            }
                            foreach (JObject objCT1 in arrDetailContract)
                            {
                                if (jobOrder["REF_ID"].ToString().Equals(objCT1["CONTRACT_CODE"].ToString()))
                                {
                                    lsDetail.Add(objCT1);
                                }
                            }

                            DateTime dt = new DateTime();
                            if (string.IsNullOrEmpty(strPayDate))
                            {
                                if (jObjContract["EFFECTIVE_NUM"].ToString().Length > 8)
                                    dt = DateTime.ParseExact(jObjContract["EFFECTIVE_NUM"].ToString(), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                                else
                                    dt = DateTime.ParseExact(jObjContract["EFFECTIVE_NUM"].ToString(), "yyyyMMdd", CultureInfo.InvariantCulture);
                            }
                            else
                            {
                                dt = DateTime.ParseExact(strPayDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            }
                            //thực hiện khởi tạo các giao dịch thanh toán
                            JObject jQrCode = goBussinessPayment.Instance.re_creatQRCode(jobOrder["ORDER_TRANS_CODE"].ToString(), dt);
                            if (template.TEMP_EMAIL.Count > 0)
                            {
                                SignFileReturnModel lsFile = new SignFileReturnModel() { FILE_REF = jobOrder["ORDER_CODE"].ToString() };
                                List<SignFileReturnModel> lsFileOrder = new List<SignFileReturnModel>();
                                lsFileOrder.Add(lsFile);
                                jObjContract.Add("ORDER_CODE", strOrderCode);
                                jObjContract.Add("QR_DATA", jQrCode["QR_DATA"].ToString());
                                jObjContract.Add("QR_TRACE", jQrCode["QR_TRACE"].ToString());
                                jObjContract.Add("QR_BASE64", jQrCode["QR_BASE64"]?.ToString());
                                jObjContract.Add("LINK_TT", jQrCode["LINK_TT"]?.ToString());
                                jObjContract.Add("DETAIL", JArray.FromObject(lsDetail));
                                if (!string.IsNullOrEmpty(strPayDate))
                                {
                                    jObjContract["DATE_VN"] = strPayDate;
                                    jObjContract["DATE_EN"] = dt.ToString("dd MMM yyyy");
                                }
                                bool b = sendMail_GCN(template, lsFileOrder, jObjContract, strEvm);
                                if (!b)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email thông báo thanh toán data " + jObjContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                }else
                                {
                                    jObjInsertData.Add("ORG_CODE", strOrgCode);
                                    jObjInsertData.Add("ORDER_CODE", strOrderCode);
                                    jObjInsertData.Add("NAME", jObjContract["NAME"].ToString());
                                    jObjInsertData.Add("EMAIL", jObjContract["EMAIL"].ToString());
                                    jObjInsertData.Add("PHONE", jObjContract["PHONE"].ToString());
                                    jObjInsertData.Add("EMAIL_COUNT", iNum.ToString());
                                    jObjInsertData.Add("SMS_COUNT", "0");
                                    jObjInsertData.Add("STATUS", "EMAIL");
                                    lsInsert.Add(jObjInsertData);
                                }    
                            }
                            //if (template.TEMP_SMS != null)
                            //{
                            //    List<TempSMSModel> lsSMS = template.TEMP_SMS.Where(m => m.TEMP_CODE == "TBAO").ToList();
                            //    if (lsSMS != null && lsSMS.Any())
                            //    {
                            //        TempSMSModel tSMS = lsSMS[0];
                            //        if (tSMS != null && tSMS.TEMP_CODE.Length > 1 && tSMS.SEND_NOW.Equals("1"))
                            //        {
                            //            //ở đây kiểm tra xem send bằng hàm hay api (api không cho send quá MAX_API sms)
                            //            bool bSMS = bSMS = sendSMS(tSMS, template, jValueContract, strUser, lsFileSigned);
                            //            if (tSMS.SEND_NOW.Equals("1"))//chỉ dành cho test
                            //            {

                            //            }
                            //            if (!bSMS)
                            //            {
                            //                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi gửi email  data " + jValueContract.ToString(), Logger.ConcatName(nameClass, nameMethod));
                            //            }
                            //        }
                            //    }
                            //}
                        }
                        object[] paramInsert = new object[] { strOrgCode, strType, JArray.FromObject(lsInsert) };
                        responseData = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.INSERT_DATA_NOTIFI, strEvm, paramInsert);

                    }
                    else
                    {
                        responseData.Success = false;
                        responseData.ErrorMessage = "Không tìm thấy mẫu ";
                        return responseData;
                    }
                }
                else
                {
                    responseData.Success = false;
                    responseData.ErrorMessage = "Không có dữ liệu thực hiện gửi thông báo";
                    return responseData;
                }
            }
            catch (Exception ex)
            {
                responseData.Success = false;
                responseData.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi mail thông báo Exception service- 1511 SendMailNotifi ", strOrgCode.ToString() + " ---" + ex.ToString());
                return responseData;
            }
            if (!responseData.Success)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi gửi mail thong bao thanh toan service- 1572", strOrgCode.ToString());
            }
            return responseData;
        }
        public string getUrlShortLink(string strKeyDomain, string strRf,  string strEvm)
        {
            try
            {
                BaseResponse response = new BaseResponse();
                string strLink = goBusinessCommon.Instance.GetConfigDB(goConstants.short_link);
                response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.CREATE_SHORT_LINK, strEvm, new object[] { "sys", "WEB", strKeyDomain, strRf, 1, "10/06/2022", "15/06/2022" });
                if (response != null)
                {
                    List<JObject> lsLink = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (lsLink != null && lsLink.Any())
                    {
                        strLink = strLink + lsLink[0]["SHORT_CODE"].ToString();
                        return strLink;
                    }
                    else
                    {
                        response.Success = false;
                        response.ErrorMessage = "Không tạo được short link";
                        goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", strKeyDomain);
                        return "";
                    }
                }
                else
                {
                    response.Success = false;
                    response.ErrorMessage = "Không tạo được short link";
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi getSortLink ecommerce Không tạo được short link", strKeyDomain);
                    return "";
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy short link service- 946", ex.ToString());
                return "";
                
            }
        }

        public BaseResponse ImpTepmlate(BaseRequest request) 
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject job = JObject.Parse(request.Data.ToString());
                    var param = new
                    {
                        project = "",
                        username = "userName",
                        progid = job["ID"].ToString()
                    };
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    BaseRequest reqProgram = new BaseRequest();
                    reqProgram = goBusinessCommon.Instance.GetBaseRequestHDI(config.env_code);
                    reqProgram.Action.ActionCode = goConstantsProcedure.IMP_GET_PROGRAM;
                    reqProgram.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                    BaseResponse resProgram = goBusinessAction.Instance.GetBaseResponse(reqProgram);
                    if (resProgram.Success)
                    {
                        string strParamCer = ""; string strHtmlCer = "";
                        string strParamMail = ""; string strHtmlMail = "";
                        ImpProgram impProgram = goBusinessCommon.Instance.GetData<ImpProgram>(resProgram, 0).FirstOrDefault();
                        if(impProgram ==null || impProgram.ORG_CODE.Length <1)
                            return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Không tìm thấy chương trình  " + job["ID"].ToString());
                        if (impProgram.CERTIFICATE_TEMP.Length > 0)
                        {
                            byte[] bHtmlCer = goBusinessCommon.Instance.getFileServer(impProgram.CERTIFICATE_TEMP);
                            if (bHtmlCer==null)
                                return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Không tải được mẫu GCN " + impProgram.CERTIFICATE_TEMP);
                            strHtmlCer = System.Text.Encoding.UTF8.GetString(bHtmlCer);
                            strParamCer = getParamToHtml(strHtmlCer);
                            if(strParamCer.Length <1)
                                return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Kiểm tra lại mẫu giấy chứng nhận -- không lấy được paramner" + impProgram.CERTIFICATE_TEMP);
                            strParamCer = strParamCer.Substring(0, strParamCer.Length - 1);
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Kiểm tra lại mẫu giấy chứng nhận" + impProgram.CERTIFICATE_TEMP);
                            return response;
                        }
                        if (impProgram.EMAIL_TEMP.Length > 0)
                        {
                            byte[] bHtmlMail = goBusinessCommon.Instance.getFileServer(impProgram.EMAIL_TEMP);
                            if (bHtmlMail == null)
                                return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Không tải được mẫu mail " + impProgram.EMAIL_TEMP);
                            strHtmlMail = System.Text.Encoding.UTF8.GetString(bHtmlMail);
                            strParamMail = getParamToHtml(strHtmlMail);
                            if (strParamMail.Length < 1)
                                return response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Kiểm tra lại mẫu mail" + impProgram.EMAIL_TEMP);
                            strParamMail = strParamMail.Substring(0, strParamMail.Length - 1);
                        }
                        else
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Kiểm tra lại mẫu mail" + impProgram.EMAIL_TEMP);
                            return response;
                        }
                        string strfooter = ""; string strHeader = "";
                        strHeader = goBusinessCommon.Instance.GetConfigDB(goConstants.TEMP_HEADER);
                        strfooter = goBusinessCommon.Instance.GetConfigDB(goConstants.TEMP_FOOTER);
                        byte[] byteHdeader = goBusinessCommon.Instance.getFileServer(strHeader);
                        if (byteHdeader == null)
                            return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Không tải được header " + strHeader);
                        strHeader = System.Text.Encoding.UTF8.GetString(byteHdeader);
                        byteHdeader = goBusinessCommon.Instance.getFileServer(strfooter);
                        if (byteHdeader == null)
                            return goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Không tải được footer " + strfooter);
                        strfooter = System.Text.Encoding.UTF8.GetString(byteHdeader);
                        JArray jarrTempLate = new JArray();
                        JObject jTemplate = new JObject();

                        jTemplate.Add("TEMP_TYPE", "EMAIL");
                        jTemplate.Add("EMAIL_TEST", "ithdi2021@gmail.com");
                        jTemplate.Add("HEADER", strHeader);
                        jTemplate.Add("FOOTER", strfooter);
                        jTemplate.Add("PARAM_REPLACE", strParamMail);
                        jTemplate.Add("URL_PAGE_ADD", "");
                        jTemplate.Add("PRODUCT_CODE", impProgram.PRODUCT_CODE);
                        jTemplate.Add("PACK_CODE", "null");
                        jTemplate.Add("DATA", strHtmlMail);
                        jTemplate.Add("TYPE_SEND", "ONE");
                        //jTemplate.Add("DATA_SRC", "JSON");QuynhTM edit
                        jTemplate.Add("DATA_SRC", "DATA");
                        jTemplate.Add("IS_BACK_DATE", "0");
                        jTemplate.Add("ORIEN", "");
                        jarrTempLate.Add(jTemplate);

                        JArray arrTem = JArray.Parse(impProgram.PACK_OBJ);
                        foreach(JToken token in arrTem)
                        {
                            jTemplate = new JObject();
                            jTemplate.Add("TEMP_TYPE", "PDF");
                            jTemplate.Add("EMAIL_TEST", "NO");
                            jTemplate.Add("HEADER", strHeader);
                            jTemplate.Add("FOOTER", strfooter);
                            jTemplate.Add("PARAM_REPLACE", strParamCer);
                            jTemplate.Add("URL_PAGE_ADD", token["BENEFIT_URL"]?.ToString());
                            jTemplate.Add("PRODUCT_CODE", impProgram.PRODUCT_CODE);
                            jTemplate.Add("PACK_CODE", token["PACK_CODE"].ToString());
                            jTemplate.Add("DATA", strHtmlCer);
                            jTemplate.Add("TYPE_SEND", "ONE");
                            //jTemplate.Add("DATA_SRC", "JSON");QuynhTM edit
                            jTemplate.Add("DATA_SRC", "DATA");
                            jTemplate.Add("IS_BACK_DATE", "0");
                            jTemplate.Add("ORIEN", "");
                            jarrTempLate.Add(jTemplate);
                        }
                        object[] paramJson = new object[] { impProgram.ORG_SELLER, impProgram.PRODUCT_CODE+"_GCN", impProgram.PRODUCT_CODE, impProgram.EMAIL_SUBJECT,
                        "WEB","PDF","",impProgram.STRUCT_CODE,DateTime.Now.ToString("dd/MM/yyyy"),"",jarrTempLate,arrTem,arrTem};
                        ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.INSERT_TEMPLATE, strEvm, paramJson);
                        response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramJson), false, null, null, null, null);
                        List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (objBatch == null || !objBatch.Any())
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Loi insert temp vao database ", string.Join("-", paramJson));
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi insert log sendmail vao database " + string.Join("-", paramJson), Logger.ConcatName(nameClass, nameMethod));
                        }

                    }
                    else
                    {
                        response = resProgram;
                        return response;
                    }
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi json khong dung cau truc", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), goConstantsError.Instance.ERROR_3002);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail json ", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), "Lỗi dữ liệu khai báo id " + ex.Message);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public string getParamToHtml(string strBody)
        {
            bool bcheck = true;
            string strtemp = "";
            string strListParam = "";
            int i1 = strBody.IndexOf("@");
            while (i1 > 0)
            {
                bcheck = false;
                i1 = strBody.IndexOf("@");
                if (i1 > 0)
                {
                    strtemp = strtemp + strBody.Substring(0, i1);
                    strBody = strBody.Substring(i1 + 1);
                    int k = strBody.IndexOf("@");
                    if (k > 20)
                    {
                        strtemp = strtemp + "@";
                    }
                    else if (k > 0)
                    {
                        strListParam = strListParam + strBody.Substring(0, k) + ";";
                        strBody = strBody.Substring(k + 1);                        
                    }
                    else
                    {
                        strtemp = strtemp + "@" + strBody;
                        break;
                    }
                }
                else
                {
                    strtemp = strtemp + strBody;
                }
            }
            if (bcheck)
                strtemp = strBody;
            return strListParam;


        }


        public List<InsurDetailExt> getDetailExt(InsurContract contract, List<InsurDetail> lsInsurDetail, ref InsurContractExt lsInsContractExt)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<InsurDetailExt> lsDetailExt = new List<InsurDetailExt>();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(contract), Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(lsInsurDetail), Logger.ConcatName(nameClass, nameMethod));
                foreach (InsurDetail insurDetail in lsInsurDetail)
                {
                    string strInsuranceDetail = JsonConvert.SerializeObject(insurDetail);
                    InsurDetailExt ext = JsonConvert.DeserializeObject<InsurDetailExt>(strInsuranceDetail);
                    ext.MONEY_VN = ((int)insurDetail.TOTAL_AMOUNT).ToString();
                    ext.MONEY = ((int)insurDetail.TOTAL_AMOUNT).ToString();
                    ext.MONEY_ENT = ((int)insurDetail.TOTAL_AMOUNT).ToString();
                    ext.CONTRACT_NO = contract.CONTRACT_NO;
                    DateTime dtEFF = new DateTime();
                    DateTime dtEXP = new DateTime();
                    try
                    {
                        dtEFF = DateTime.ParseExact(insurDetail.EFFECTIVE_DATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        dtEXP = DateTime.ParseExact(insurDetail.EXPIRATION_DATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch
                    {                        
                        dtEFF = DateTime.ParseExact(insurDetail.EFFECTIVE_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                        dtEXP = DateTime.ParseExact(insurDetail.EXPIRATION_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                    ext.EFFECTIVE_DATE_EN = dtEFF.ToString("dd MMMM, yyyy");
                    ext.HH_TO = dtEFF.Hour.ToString("00");
                    ext.SS_TO = dtEFF.Minute.ToString("00");
                    ext.DAY_TO = dtEFF.Day.ToString("00");
                    ext.MM_TO = dtEFF.Month.ToString("00");
                    ext.YY_TO = dtEFF.Year.ToString("00");
                    ext.EXPIRATION_DATE_EN = dtEXP.ToString("dd MMMM, yyyy");
                    ext.HH_F = dtEXP.Hour.ToString("00");
                    ext.SS_F = dtEXP.Minute.ToString("00");
                    ext.DAY_F = dtEXP.Day.ToString("00");
                    ext.MM_F = dtEXP.Month.ToString("00");
                    ext.YY_F = dtEXP.Year.ToString("00");
                    ext.EXPIRATION_DATE1 = dtEFF.AddDays(1).ToString("dd/MM/yyyy");
                    ext.EXPIRATION_DATE1_EN = dtEFF.AddDays(1).ToString("dd MMMM, yyyy");
                    if (!string.IsNullOrEmpty(insurDetail.RELATIONSHIP) && insurDetail.RELATIONSHIP.Equals("BAN_THAN"))
                    {
                        ext.RELATIONSHIP_VN = "Bản thân";
                    }
                    ext.NAME_EN = goBussinessSMS.Instance.removeMark(insurDetail.NAME).ToUpper();
                    //ext.DATE_SIGN = DateTime.Now.ToString("dd/MM/yyyy");
                    //RELATIONSHIP_VN
                    //  REGION_VI
                    //  REGION_EN
                    //  BENEFICIARY
                    //  PRODUCT_NAME
                    lsDetailExt.Add(ext);
                }
                string strInsurance = JsonConvert.SerializeObject(contract);
                lsInsContractExt = JsonConvert.DeserializeObject<InsurContractExt>(strInsurance);
                lsInsContractExt.MONEY_VN = contract.TOTAL_AMOUNT.ToString(); // 26/07/2021 tuan chuyen sting thanh double trong class => thêm toString 
                lsInsContractExt.MONEY = contract.TOTAL_AMOUNT.ToString();
                lsInsContractExt.MONEY_ENT = contract.TOTAL_AMOUNT.ToString();
                DateTime dtEFF1 = new DateTime();
                DateTime dtEXP1 = new DateTime();
                try
                {
                    dtEFF1 = DateTime.ParseExact(contract.EFFECTIVE_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                    dtEXP1 = DateTime.ParseExact(contract.EXPIRATION_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                }
                catch
                {
                    dtEFF1 = DateTime.ParseExact(contract.EFFECTIVE_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                    dtEXP1 = DateTime.ParseExact(contract.EXPIRATION_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                }
                lsInsContractExt.EFFECTIVE_DATE_EN = dtEFF1.ToString("dd MMMM, yyyy");
                lsInsContractExt.HH_TO = dtEFF1.Hour.ToString("00");
                lsInsContractExt.SS_TO = dtEFF1.Minute.ToString("00");
                lsInsContractExt.DAY_TO = dtEFF1.Day.ToString("00");
                lsInsContractExt.MM_TO = dtEFF1.Month.ToString("00");
                lsInsContractExt.YY_TO = dtEFF1.Year.ToString("00");
                lsInsContractExt.EXPIRATION_DATE_EN = dtEXP1.ToString("dd MMMM, yyyy");
                lsInsContractExt.HH_F = dtEXP1.Hour.ToString("00");
                lsInsContractExt.SS_F = dtEXP1.Minute.ToString("00");
                lsInsContractExt.DAY_F = dtEXP1.Day.ToString("00");
                lsInsContractExt.MM_F = dtEXP1.Month.ToString("00");
                lsInsContractExt.YY_F = dtEXP1.Year.ToString("00");
                lsInsContractExt.EXPIRATION_DATE1 = dtEFF1.AddDays(1).ToString("dd/MM/yyyy");
                lsInsContractExt.EXPIRATION_DATE1_EN = dtEFF1.AddDays(1).ToString("dd MMMM, yyyy");
                lsInsContractExt.RELATIONSHIP_VN = "";

                //RELATIONSHIP_VN
                //  REGION_VI
                //  REGION_EN
                //  BENEFICIARY
                //  PRODUCT_NAME
                return lsDetailExt;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
        }

        public List<InsurDetailExt> getDetailExt_Preview(InsurContract contract, List<InsurDetail> lsInsurDetail, ref InsurContractExt lsInsContractExt)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<InsurDetailExt> lsDetailExt = new List<InsurDetailExt>();
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(contract), Logger.ConcatName(nameClass, nameMethod));
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(lsInsurDetail), Logger.ConcatName(nameClass, nameMethod));
                foreach (InsurDetail insurDetail in lsInsurDetail)
                {
                    string strInsuranceDetail = JsonConvert.SerializeObject(insurDetail);
                    InsurDetailExt ext = JsonConvert.DeserializeObject<InsurDetailExt>(strInsuranceDetail);
                    ext.MONEY_VN = "0";//((int)insurDetail.TOTAL_AMOUNT).ToString();
                    ext.MONEY = "0"; ((int)insurDetail.TOTAL_AMOUNT).ToString();
                    ext.MONEY_ENT = "0"; ((int)insurDetail.TOTAL_AMOUNT).ToString();                    
                    ext.CONTRACT_NO = contract.CONTRACT_NO;
                    DateTime dtEFF = new DateTime();
                    DateTime dtEXP = new DateTime();
                    try
                    {
                        dtEFF = DateTime.ParseExact(insurDetail.EFFECTIVE_DATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        dtEXP = DateTime.ParseExact(insurDetail.EXPIRATION_DATE, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        dtEFF = DateTime.ParseExact(insurDetail.EFFECTIVE_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                        dtEXP = DateTime.ParseExact(insurDetail.EXPIRATION_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                    }
                    ext.EFFECTIVE_DATE_EN = dtEFF.ToString("dd MMMM, yyyy");
                    ext.HH_TO = dtEFF.Hour.ToString("00");
                    ext.SS_TO = dtEFF.Minute.ToString("00");
                    ext.DAY_TO = dtEFF.Day.ToString("00");
                    ext.MM_TO = dtEFF.Month.ToString("00");
                    ext.YY_TO = dtEFF.Year.ToString("00");
                    ext.EXPIRATION_DATE_EN = dtEXP.ToString("dd MMMM, yyyy");
                    ext.HH_F = dtEXP.Hour.ToString("00");
                    ext.SS_F = dtEXP.Minute.ToString("00");
                    ext.DAY_F = dtEXP.Day.ToString("00");
                    ext.MM_F = dtEXP.Month.ToString("00");
                    ext.YY_F = dtEXP.Year.ToString("00");
                    ext.EXPIRATION_DATE1 = dtEFF.AddDays(1).ToString("dd/MM/yyyy");
                    ext.EXPIRATION_DATE1_EN = dtEFF.AddDays(1).ToString("dd MMMM, yyyy");
                    if (!string.IsNullOrEmpty(insurDetail.RELATIONSHIP) && insurDetail.RELATIONSHIP.Equals("BAN_THAN"))
                    {
                        ext.RELATIONSHIP_VN = "Bản thân";
                    }
                    ext.NAME_EN = goBussinessSMS.Instance.removeMark(insurDetail.NAME).ToUpper();
                    //ext.DATE_SIGN = DateTime.Now.ToString("dd/MM/yyyy");
                    //RELATIONSHIP_VN
                    //  REGION_VI
                    //  REGION_EN
                    //  BENEFICIARY
                    //  PRODUCT_NAME
                    lsDetailExt.Add(ext);
                }
                string strInsurance = JsonConvert.SerializeObject(contract);
                lsInsContractExt = JsonConvert.DeserializeObject<InsurContractExt>(strInsurance);

                lsInsContractExt.MONEY_VN = "0";//contract.TOTAL_AMOUNT.ToString(); // 26/07/2021 tuan chuyen sting thanh double trong class => thêm toString 
                lsInsContractExt.MONEY = "0";//contract.TOTAL_AMOUNT.ToString();
                lsInsContractExt.MONEY_ENT = "0";//contract.TOTAL_AMOUNT.ToString();
                
                DateTime dtEFF1 = new DateTime();
                DateTime dtEXP1 = new DateTime();
                try
                {
                    dtEFF1 = DateTime.ParseExact(contract.EFFECTIVE_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                    dtEXP1 = DateTime.ParseExact(contract.EXPIRATION_NUM, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                }
                catch
                {
                    dtEFF1 = DateTime.ParseExact(contract.EFFECTIVE_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                    dtEXP1 = DateTime.ParseExact(contract.EXPIRATION_NUM, "yyyyMMdd", CultureInfo.InvariantCulture);
                }
                lsInsContractExt.EFFECTIVE_DATE_EN = dtEFF1.ToString("dd MMMM, yyyy");
                lsInsContractExt.HH_TO = dtEFF1.Hour.ToString("00");
                lsInsContractExt.SS_TO = dtEFF1.Minute.ToString("00");
                lsInsContractExt.DAY_TO = dtEFF1.Day.ToString("00");
                lsInsContractExt.MM_TO = dtEFF1.Month.ToString("00");
                lsInsContractExt.YY_TO = dtEFF1.Year.ToString("00");
                lsInsContractExt.EXPIRATION_DATE_EN = dtEXP1.ToString("dd MMMM, yyyy");
                lsInsContractExt.HH_F = dtEXP1.Hour.ToString("00");
                lsInsContractExt.SS_F = dtEXP1.Minute.ToString("00");
                lsInsContractExt.DAY_F = dtEXP1.Day.ToString("00");
                lsInsContractExt.MM_F = dtEXP1.Month.ToString("00");
                lsInsContractExt.YY_F = dtEXP1.Year.ToString("00");
                lsInsContractExt.EXPIRATION_DATE1 = dtEFF1.AddDays(1).ToString("dd/MM/yyyy");
                lsInsContractExt.EXPIRATION_DATE1_EN = dtEFF1.AddDays(1).ToString("dd MMMM, yyyy");
                lsInsContractExt.RELATIONSHIP_VN = "";

                //RELATIONSHIP_VN
                //  REGION_VI
                //  REGION_EN
                //  BENEFICIARY
                //  PRODUCT_NAME
                return lsDetailExt;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
        }

        public TempFileSign fileCerBackDate(string strProductCode, string strInsuranceContract, string strOrrgCode)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                string strGuiID_File = Guid.NewGuid().ToString("N");
                TempFileSign fileSigned = new TempFileSign();
                fileSigned.FILE_URL = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + strGuiID_File;
                fileSigned.CERTIFICATE_NO = strInsuranceContract;
                fileSigned.ID_FILE = strGuiID_File;
                fileSigned.signed = true;
                //fileSigned = fileCerBackDate_Task(strProductCode, strInsuranceContract, strGuiID_File);
                new Task(() => fileCerBackDate_Task(strProductCode, strInsuranceContract, strGuiID_File, strOrrgCode)).Start();
                return fileSigned;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi ký backdate " + strInsuranceContract + strProductCode + " # " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký backdate ", "Lỗi " + strInsuranceContract + strProductCode + " # " + ex.ToString());
                return null;
            }
        }
        public TempFileSign fileCerBackDate_Task(string strProductCode, string strInsuranceContract, string strGuiID_File, string strOrrgCode)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {

                ///List<JObject> lsJValueConvert = new List<JObject>();
                //trường hợp dữ liệu vào convert từ db
                //List<JObject> objContract = new List<JObject>();
                List<JObject> objDetails = new List<JObject>();
                object[] paramFind = new object[] { "OPENAPI", "HDI", strInsuranceContract, strProductCode, "NO", strProductCode + "_GCN", strOrrgCode };
                BaseResponse responseFind = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                //objContract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                objDetails = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);
                if (objDetails.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin hợp đồng " + strInsuranceContract + strProductCode + " # " + objDetails.Count.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin hợp đồng businis_service 990", "Lỗi không lấy được thông tin hợp đồng " + strInsuranceContract + strProductCode + " # " + objDetails.Count.ToString());
                    return null;
                }
                string strOrgCode = objDetails[0]["ORG_CODE"].ToString();
                string strPackCode = objDetails[0]["PACK_CODE"].ToString();
                //thực hiện lấy mẫu 
                //string strPackCode = lsItem[0].PACK_CODE.ToString();
                object[] param = new object[] { strOrgCode, "PDF", strProductCode + "_GCN", strProductCode, strPackCode };
                BaseResponse responseTemplate = goBusinessCommon.Instance.exePackagePrivate("GET_TEMPLATE", strEvm, param);
                List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
                List<JObject> arrTemplateDetail = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 1);
                if (arrTemplate == null || arrTemplate.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strPackCode, Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin mẫu businis_service 1004", "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strPackCode);
                    return null;
                }
                List<JObject> lsTemplate = new List<JObject>();
                lsTemplate = arrTemplateDetail;
                lsTemplate.Insert(0, arrTemplate[0]);
                //kiểm tra dữ liệu convert            
                List<TempFileSign> lsFilePDf = goBussinessPDF.Instance.createPdfBackDate(lsTemplate, objDetails[0], strProductCode, strOrgCode, strGuiID_File);
                TempFileSign fileSigned = new TempFileSign();
                if (lsFilePDf != null && lsFilePDf.Count > 0)
                {
                    //thực hiện ký số
                    lsFilePDf[0].dtSignBack = DateTime.ParseExact(objDetails[0]["DATE_SIGN"].ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    fileSigned = goBussinessSign.Instance.signBackDate(lsFilePDf[0], strProductCode, strOrgCode, strPackCode);
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không convert được pdf " + strInsuranceContract + strProductCode + " # ", Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không convert được pdf businis_service 1022", "Lỗi không convert được pdf " + strInsuranceContract + strProductCode + " # ");
                    return null;
                }
                return fileSigned;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi ký backdate " + strInsuranceContract + strProductCode + " # " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký backdate businis_service 1030", "Lỗi " + strInsuranceContract + strProductCode + " # " + ex.ToString());
                return null;
            }

        }
        public BaseResponse viewCertificateJson(string strOrgCode, string strProductCode, string strPackCode, JObject jValueConvert)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            //thực hiện lấy mẫu 
            //string strPackCode = lsItem[0].PACK_CODE.ToString();
            if (string.IsNullOrEmpty(strPackCode))
                strPackCode = "null";

            object[] param = new object[] { strOrgCode, "PDF", strProductCode + "_VIEW", strProductCode, strPackCode };
            BaseResponse responseTemplate = goBusinessCommon.Instance.exePackagePrivate("GET_TEMPLATE", strEvm, param);
            List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
            List<JObject> arrTemplateDetail = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 1);
            if (arrTemplate == null || arrTemplate.Count == 0)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin mẫu viewCertificateJson" + strProductCode + strOrgCode + " # " + strPackCode, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin mẫu viewCertificateJson ", "Lỗi không lấy được thông tin mẫu viewCertificateJson " + strProductCode + strOrgCode + " # " + strPackCode);
                return null;
            }
            List<JObject> lsTemplate = new List<JObject>();
            lsTemplate = arrTemplateDetail;
            lsTemplate.Insert(0, arrTemplate[0]);
            List<JObject> lsJValueConvert = new List<JObject>();
            if (jValueConvert == null)
            {
                //trường hợp lấy dữ liệu
            }
            else
            {
                //convert từ json truyền vào
                lsJValueConvert.Add(jValueConvert);
            }

            List<TempFileSign> lsFilePDf = goBussinessPDF.Instance.createAllPdf(lsTemplate, lsJValueConvert, strProductCode, strOrgCode);
            responseTemplate.Data = lsFilePDf[0];
            return responseTemplate;
        }
        public BaseResponse signEmailAll(string strOrgCode, string strProductCode, string strPackCode, string strInsuranceContract, List<JObject> lsJValueConvert)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            //thực hiện lấy mẫu 
            //string strPackCode = lsItem[0].PACK_CODE.ToString();
            object[] param = new object[] { strOrgCode, "PDF", strProductCode + "_GCN", strProductCode, strPackCode };
            BaseResponse responseTemplate = goBusinessCommon.Instance.exePackagePrivate("GET_TEMPLATE", strEvm, param);
            List<JObject> arrTemplate = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
            List<JObject> arrTemplateDetail = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 1);
            if (arrTemplate == null || arrTemplate.Count == 0)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strPackCode, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin mẫu businis_service 1085", "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strPackCode);
                return null;
            }
            List<JObject> lsTemplate = new List<JObject>();
            lsTemplate = arrTemplateDetail;
            lsTemplate.Insert(0, arrTemplate[0]);
            List<JObject> objContract = new List<JObject>();
            List<JObject> objDetails = new List<JObject>();
            //kiểm tra dữ liệu convert
            if (arrTemplate[0]["DATA_SRC"].ToString().Equals("DATA"))
            {
                //trường hợp dữ liệu vào convert từ db
                object[] paramFind = new object[] { "OPENAPI", "HDI", strInsuranceContract, strProductCode, "NO", strProductCode + "_GCN", strOrgCode };
                BaseResponse responseFind = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Pay_get_contract, strEvm, paramFind);
                objContract = goBusinessCommon.Instance.GetData<JObject>(responseFind, 0);
                objDetails = goBusinessCommon.Instance.GetData<JObject>(responseFind, 1);
                if (objContract.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin hợp đồng " + strInsuranceContract + strProductCode + " # " + objDetails.Count.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin hợp đồng businis_service 1104", "Lỗi không lấy được thông tin hợp đồng " + strInsuranceContract + strProductCode + " # " + objDetails.Count.ToString());
                    return null;
                }
                if (arrTemplate[0]["TYPE_SEND"].ToString().Equals("ALL") && objDetails.Count > 1)
                {
                    JObject jobInsurance = objContract[0];
                    jobInsurance.Add("CERTIFICATE_NO", objContract[0]["CONTRACT_NO"].ToString());
                    jobInsurance.Add("DS_DK", JArray.FromObject(objDetails));
                    lsJValueConvert = new List<JObject>() { jobInsurance };
                }
                else
                {
                    lsJValueConvert = objDetails;
                }
                //nếu có chi tiết của giấy chứng nhận thì xử lý sau

            }
            List<TempFileSign> lsFilePDf = goBussinessPDF.Instance.createAllPdf(lsTemplate, lsJValueConvert, strProductCode, strOrgCode);
            if (lsFilePDf != null && lsFilePDf.Count > 0)
            {
                //thực hiện ký số
                lsFilePDf = goBussinessSign.Instance.signBatchPDF(lsFilePDf, strProductCode, strOrgCode, strPackCode);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không convert được pdf " + strInsuranceContract + strProductCode + " # ", Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không convert được pdf businis_service 1130", "Lỗi không convert được pdf " + strInsuranceContract + strProductCode + " # ");
                return null;
            }
            //thực hiện gửi mail
            //lấy mẫu của mail
            param = new object[] { strOrgCode, "EMAIL", strProductCode + "_GCN", strProductCode, strPackCode };
            BaseResponse responseTemplateMail = goBusinessCommon.Instance.exePackagePrivate("GET_TEMPLATE", strEvm, param);
            List<JObject> arrTemplateMail = goBusinessCommon.Instance.GetData<JObject>(responseTemplateMail, 0);
            List<JObject> arrTemplateDetailEmail = goBusinessCommon.Instance.GetData<JObject>(responseTemplateMail, 1);
            if (arrTemplateMail == null || arrTemplateMail.Count == 0)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin mẫu mail " + strProductCode + strOrgCode + " # " + strPackCode, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin mẫu MAIL businis_service 1142", "Lỗi không lấy được thông tin mẫu MAIL" + strProductCode + strOrgCode + " # " + strPackCode);
                return null;
            }
            if (arrTemplateMail[0]["TYPE_SEND"].ToString().Equals("ALL"))
            {
                if (objContract != null)
                {
                    //gửi toàn bộ thông tin cho người mua
                    ServiceEmailModel emailContract = new ServiceEmailModel();
                    emailContract.to = new List<EmailModel> { new EmailModel { email = objContract[0]["EMAIL"].ToString() } };
                    List<FileModel> lsFileContract = new List<FileModel>();
                    FileModel fileContract = new FileModel();
                    emailContract.file = new List<FileModel>();
                    foreach (TempFileSign fileTemp in lsFilePDf)
                    {
                        fileContract.path = fileTemp.ID_FILE;
                        fileContract.name = fileTemp.ID_FILE;
                        emailContract.file.Add(fileContract);
                    }
                    BaseRequest requestSendMail = new BaseRequest();
                    requestSendMail.Action = new ActionInfo();
                    requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                    requestSendMail.Action.ParentCode = strOrgCode;
                    requestSendMail.Action.UserName = "HDI_PRIVATE";

                    string strBody = "";
                    List<string> lsReplace = arrTemplateMail[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    JArray arrValue = JArray.FromObject(objContract);
                    strBody = goBusinessTemplate.Instance.replaceJobjectToHtml(objContract[0], lsReplace, arrTemplateMail[0]["DATA"].ToString());
                    if (arrTemplateDetailEmail != null && arrTemplateDetailEmail.Count > 0)
                    {
                        //string strHTMLTemp = objTempValue1[0]["DATA"].ToString();
                        //if (objTempValue1[0]["PARAM_NAME"].ToString().Equals(strParamNew))
                        //{
                        List<string> strListParamDetail = arrTemplateDetailEmail[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                        //vòng for dữ liệu truyền vào
                        string strReplaceDetail = "";
                        foreach (JObject jobj in objDetails)
                        {
                            var prt = jobj.Properties();
                            string strTempHTMLDetail = arrTemplateDetailEmail[0]["DATA"].ToString();
                            //vòng for replate từng tham số
                            foreach (string strCTRP in strListParamDetail)
                            {
                                foreach (var prtct in prt)
                                {
                                    if (prtct.Name.Equals(strCTRP))
                                    {
                                        strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jobj[strCTRP].ToString());
                                        break;
                                    }
                                }

                            }
                            strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                            //}
                            //break;
                        }
                        strBody = strBody.Replace("@" + arrTemplateDetailEmail[0]["PARAM_NAME"].ToString() + "@", strReplaceDetail);
                    }
                    strBody = goBussinessPDF.Instance.clearParram(strBody);
                    emailContract.content = strBody;
                    //jobSign.Add("content", strBody);
                    emailContract.subject = arrTemplateMail[0]["TITLE"].ToString();
                    //add cc,bcc
                    List<string> lsBCC = arrTemplateMail[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                    string strMailBCC = "";
                    foreach (string strMail in lsBCC)
                    {
                        if (!string.IsNullOrEmpty(strMail))
                        {
                            List<string> lsEmailBCC = strMail.Split(',').ToList();
                            List<EmailModel> lsBCCModel = new List<EmailModel>();
                            foreach (string emaiBcc in lsEmailBCC)
                            {
                                if (emaiBcc.Length > 10)
                                {
                                    EmailModel emaibcccc = new EmailModel();
                                    emaibcccc.email = emaiBcc;
                                    lsBCCModel.Add(emaibcccc);
                                    strMailBCC = strMailBCC + emaiBcc + ";";
                                }

                            }
                            emailContract.bcc = lsBCCModel;
                            break;
                        }
                    }
                    requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                    goBussinessEmail cls = new goBussinessEmail();
                    cls.createLogSendMail(strOrgCode, strProductCode, strProductCode, strPackCode, "EMAIL", DateTime.Now.ToString("dd/MM/yyyy"), objContract[0]["EMAIL"].ToString(), emailContract.subject,
                "", strMailBCC, emailContract.content, "HDI", objContract[0]["CONTRACT_NO"].ToString());
                    cls.GetResponseEmail(requestSendMail);
                }
            }

            responseTemplate.Data = lsFilePDf;


            return responseTemplate;
        }
        public BaseResponse sendMailWithJson(JObject jsonValues)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            JArray jArrtemp = JArray.FromObject(jsonValues["TEMP"]);
            List<JToken> lsJTK_DataDetail = jArrtemp.ToList();
            //vòng for dữ liệu truyền vào
            int iSTT = 1;
            BaseResponse responseTemplate = new BaseResponse();
            foreach (JObject jOjDataDetail in lsJTK_DataDetail)
            {
                //lấy mẫu tương ứng với teamp truyền vào
                string strOrgCode = jOjDataDetail["ORG_CODE"]?.ToString();
                string strTempCode = jOjDataDetail["TEMP_CODE"].ToString();
                string strProductCode = jOjDataDetail["PRODUCT_CODE"].ToString();
                string strType = jOjDataDetail["TYPE"].ToString();
                string strTo = jOjDataDetail["TO"].ToString();
                string strCC = jOjDataDetail["CC"].ToString();
                string strBCC = jOjDataDetail["BCC"].ToString();
                string strAttach = jOjDataDetail["file"]?.ToString();
                string strUser = jOjDataDetail["ACCOUNT"]?.ToString();

                if (string.IsNullOrEmpty(strOrgCode))
                    strOrgCode = "HDI";
                if (string.IsNullOrEmpty(strProductCode))
                    strProductCode = "HDI";
                //thực hiện lấy mẫu 
                object[] param = new object[] { strOrgCode, "EMAIL", strProductCode + "_GCN", strProductCode, "null" };
                responseTemplate = goBusinessCommon.Instance.exePackagePrivate("GET_TEMPLATE", strEvm, param);
                List<JObject> arrTemplateMail = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 0);
                List<JObject> arrTemplateDetailEmail = goBusinessCommon.Instance.GetData<JObject>(responseTemplate, 1);
                if (arrTemplateMail == null || arrTemplateMail.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strTempCode, Logger.ConcatName(nameClass, nameMethod));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi không lấy được thông tin mẫu businis_service 1276", "Lỗi không lấy được thông tin mẫu " + strProductCode + strOrgCode + " # " + strTempCode);
                    return null;
                }
                ServiceEmailModel emailContract = new ServiceEmailModel();
                emailContract.to = getMailModle(strTo);
                List<FileModel> lsFileContract = new List<FileModel>();
                FileModel fileContract = new FileModel();
                emailContract.file = new List<FileModel>();
                BaseRequest requestSendMail = new BaseRequest();
                requestSendMail.Action = new ActionInfo();
                requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                requestSendMail.Action.ParentCode = strOrgCode;
                requestSendMail.Action.UserName = "HDI_PRIVATE";

                string strBody = "";
                List<string> lsReplace = arrTemplateMail[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                strBody = goBusinessTemplate.Instance.replaceJobjectToHtml(jsonValues, lsReplace, arrTemplateMail[0]["DATA"].ToString());
                if (arrTemplateDetailEmail != null && arrTemplateDetailEmail.Count > 0)
                {
                    foreach (JObject jobCT in arrTemplateDetailEmail)
                    {
                        List<string> strListParamDetail = jobCT["LIST_REPLACE"].ToString().Split(';').ToList();
                        JArray jArrDataList = JArray.Parse(jsonValues[jobCT["PARAM_NAME"].ToString()]?.ToString());
                        List<JToken> lsJTK_DataDetailLs = jArrDataList.ToList();
                        //vòng for dữ liệu truyền vào
                        string strReplaceDetail = "";
                        foreach (JObject jobj in lsJTK_DataDetailLs)
                        {
                            var prt = jobj.Properties();
                            string strTempHTMLDetail = jobCT["DATA"].ToString();
                            //vòng for replate từng tham số
                            foreach (string strCTRP in strListParamDetail)
                            {
                                foreach (var prtct in prt)
                                {
                                    if (prtct.Name.Equals(strCTRP))
                                    {
                                        strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jobj[strCTRP].ToString());
                                        break;
                                    }
                                }

                            }
                            strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                            //}
                            //break;
                        }
                        strBody = strBody.Replace("@" + arrTemplateDetailEmail[0]["PARAM_NAME"].ToString() + "@", strReplaceDetail);
                    }
                    //string strHTMLTemp = objTempValue1[0]["DATA"].ToString();
                    //if (objTempValue1[0]["PARAM_NAME"].ToString().Equals(strParamNew))
                    //{

                }
                strBody = goBussinessPDF.Instance.clearParram(strBody);
                emailContract.content = strBody;
                //jobSign.Add("content", strBody);
                emailContract.subject = arrTemplateMail[0]["TITLE"].ToString();
                //add cc,bcc
                emailContract.bcc = getMailModle(strBCC);
                emailContract.cc = getMailModle(strCC);
                //đẩy file attach
                if (!string.IsNullOrEmpty(strAttach))
                {
                    List<FileModel> lsFileAtt = new List<FileModel>();
                    JArray jarrAttach = JArray.Parse(strAttach);
                    foreach (JToken jtk in jarrAttach)
                    {
                        FileModel fAtt = new FileModel();
                        fAtt.name = jtk["name"]?.ToString();
                        fAtt.path = jtk["path"].ToString();
                        lsFileAtt.Add(fAtt);
                    }
                    emailContract.file = lsFileAtt;
                }

                requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                goBussinessEmail cls = new goBussinessEmail();
                cls.createLogSendMail(strOrgCode, strProductCode, strProductCode, "null", "EMAIL", DateTime.Now.ToString("dd/MM/yyyy"), strTo, emailContract.subject,
                strCC, strBCC, emailContract.content, "HDI", jsonValues["REFF_ID"]?.ToString());
                cls.GetResponseEmail(requestSendMail, strUser);

            }
            responseTemplate.Data = "";
            return responseTemplate;
        }
        public BaseResponse sendMailAdmin(JArray jsonValues)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            List<JToken> lsJTK_DataDetail = jsonValues.ToList();
            //vòng for dữ liệu truyền vào
            int iSTT = 1;
            BaseResponse responseTemplate = new BaseResponse();
            foreach (JObject jOjDataDetail in lsJTK_DataDetail)
            {   
                string strContent = jOjDataDetail["CONTENT"].ToString();
                string strSubject = jOjDataDetail["SUBJECT"].ToString();
                string strType = jOjDataDetail["TYPE"].ToString();
                string strTo = jOjDataDetail["TO"].ToString();
                string strCC = jOjDataDetail["CC"].ToString();
                string strBCC = jOjDataDetail["BCC"].ToString();
                string strAttach = jOjDataDetail["file"]?.ToString();
                string strUser = jOjDataDetail["ACCOUNT"]?.ToString();

                ServiceEmailModel emailContract = new ServiceEmailModel();
                emailContract.to = getMailModle(strTo);
                List<FileModel> lsFileContract = new List<FileModel>();
                FileModel fileContract = new FileModel();
                emailContract.file = new List<FileModel>();
                BaseRequest requestSendMail = new BaseRequest();
                requestSendMail.Action = new ActionInfo();
                requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                requestSendMail.Action.ParentCode = "HDI";
                requestSendMail.Action.UserName = "HDI_PRIVATE";
                string strBody = Encoding.UTF8.GetString(Convert.FromBase64String(strContent)); ;                
                emailContract.content = strBody;
                emailContract.subject = strSubject;
                //add cc,bcc
                emailContract.bcc = getMailModle(strBCC);
                emailContract.cc = getMailModle(strCC);
                //đẩy file attach
                if (!string.IsNullOrEmpty(strAttach))
                {
                    List<FileModel> lsFileAtt = new List<FileModel>();
                    JArray jarrAttach = JArray.Parse(strAttach);
                    foreach (JToken jtk in jarrAttach)
                    {
                        FileModel fAtt = new FileModel();
                        fAtt.name = jtk["name"]?.ToString();
                        fAtt.path = jtk["path"].ToString();
                        lsFileAtt.Add(fAtt);
                    }
                    emailContract.file = lsFileAtt;
                }

                requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                goBussinessEmail cls = new goBussinessEmail();
                cls.createLogSendMail("HDI", "PORTAL_EMAIL", strType, "null", "EMAIL", DateTime.Now.ToString("dd/MM/yyyy"), strTo, emailContract.subject,
                strCC, strBCC, emailContract.content, "HDI", jOjDataDetail["REFF_ID"]?.ToString());
                cls.GetResponseEmail(requestSendMail, strUser);

            }
            responseTemplate.Success = true;
            responseTemplate.Data = "";
            return responseTemplate;
        }
        public List<EmailModel> getMailModle(string strEmail)
        {
            List<EmailModel> ls = new List<EmailModel>();
            List<string> lsEmailBCC = strEmail.Split(';').ToList();
            foreach (string emaiBcc in lsEmailBCC)
            {
                if (emaiBcc.Length > 10)
                {
                    EmailModel emaibcccc = new EmailModel();
                    emaibcccc.email = emaiBcc;
                    ls.Add(emaibcccc);
                }

            }
            return ls;
        }
        public BaseResponse signEmail(InsurDetail item)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien gui mail : " + JsonConvert.SerializeObject(item), Logger.ConcatName(nameClass, nameMethod));
            BaseRequest requestSign = new BaseRequest();
            requestSign.Action = new ActionInfo();
            //thực hiện ký số
            requestSign.Action.ActionCode = "GET_TEMPLATE";
            requestSign.Action.ParentCode = "HDI_PRIVATE";
            requestSign.Action.UserName = "HDI_PRIVATE";
            JObject jobSign = new JObject();
            jobSign.Add("USER_NAME", "HDI_PRIVATE");
            jobSign.Add("TEMP_CODE", item.PRODUCT_CODE + "_GCN");
            jobSign.Add("TYPE_RES", "BASE64");
            jobSign.Add("PARAM_CODE", "NO");
            jobSign.Add("PARAM_VALUE", item.CERTIFICATE_NO);
            jobSign.Add("TRANSACTION_ID", item.CERTIFICATE_NO);
            jobSign.Add("DATA_TYPE", "DATA");
            jobSign.Add("PRODUCT_CODE", item.PRODUCT_CODE);
            jobSign.Add("PACK_CODE", item.PACK_CODE);
            requestSign.Data = jobSign;
            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien tao file: " + JsonConvert.SerializeObject(requestSign), Logger.ConcatName(nameClass, nameMethod));
            BaseResponse basePDF = goBussinessPDF.Instance.convertPDF(requestSign);
            if (basePDF.Success)
            {
                JObject jData = JObject.Parse(basePDF.Data.ToString());
                SignRequestModel signModel = new SignRequestModel();
                signModel.USER_NAME = "HDI_PRIVATE";
                signModel.LOCATION = "Thành phố Hồ Chí Minh";
                signModel.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                signModel.TypeResponse = "BASE64";
                signModel.TextNote = "";
                signModel.Alert = "";
                signModel.FILE_REFF = item.CERTIFICATE_NO;
                signModel.EXT_FILE = "HTMLHDI";
                signModel.FILE = jData["FILE_BASE64"].ToString();
                signModel.TEXT_CA = "";
                signModel.TYPE_FILE = "BASE64";
                signModel.TEMP_CODE = item.PRODUCT_CODE + "_GCN";
                signModel.FILE_CODE = jData["FILE_CODE"].ToString();
                signModel.PRODUCT_CODE = item.PRODUCT_CODE;
                signModel.STRUCT_CODE = "HDI";
                //signModel.PACK_CODE = item.PACK_CODE;
                // Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien ky so: " + JsonConvert.SerializeObject(signModel), Logger.ConcatName(nameClass, nameMethod));
                basePDF = goBussinessSign.Instance.signHDI("HDI_PRIVATE", "HDI_SIGN", "", signModel, null);
                if (basePDF.Success)
                {
                    requestSign.Action.ActionCode = "HDI_EMAIL_01";
                    jobSign = new JObject();
                    JObject jEmailTo = JObject.Parse(" {\"email\":\"" + item.EMAIL + "\"}");//
                    JArray jArrMailTo = new JArray();
                    jArrMailTo.Add(jEmailTo);
                    jobSign.Add("to", jArrMailTo);
                    JObject jData1 = JObject.Parse(basePDF.Data.ToString());
                    BaseResponse responseTemp = new BaseResponse();
                    responseTemp = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Get_Temp_Value, strEvm, new string[] { requestSign.Action.ParentCode, "EMAIL", item.PRODUCT_CODE + "_GCN", item.PACK_CODE, "GCN", item.CERTIFICATE_NO });
                    List<JObject> objTempValue = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);
                    string strBody = "";
                    if (objTempValue == null || objTempValue[0].Count == 0)
                    {
                        jobSign.Add("content", " ");
                        jobSign.Add("subject", "Cấp giấy chứng nhận Bảo hiểm HDI");
                    }
                    else
                    {
                        List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                        List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                        strBody = goBussinessPDF.Instance.replaceHTML_JSON(goBussinessPDF.Instance.createJsonKeyValue(objTempValue), objTemp[0]["DATA"].ToString(), lsReplace);
                        jobSign.Add("content", strBody);
                        jobSign.Add("subject", objTemp[0]["TITLE"].ToString());
                        //add cc,bcc
                        List<string> lsBCC = objTemp[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                        foreach (string strMail in lsBCC)
                        {
                            if (!string.IsNullOrEmpty(strMail) && strMail.Length > item.ORG_CODE.Length
                                && strMail.Substring(0, item.ORG_CODE.Length).Equals(item.ORG_CODE))
                            {
                                List<string> lsEmailBCC = strMail.Substring(item.ORG_CODE.Length + 1).Split(',').ToList();
                                JArray jArrMailBCC = new JArray();
                                foreach (string emaiBcc in lsEmailBCC)
                                {
                                    JObject jEmailBCC = JObject.Parse(" {\"email\":\"" + emaiBcc + "\"}");//
                                    jArrMailBCC.Add(jEmailBCC);
                                }

                                jobSign.Add("bcc", jArrMailBCC);
                                break;
                            }
                        }
                    }
                    jobSign.Add("file", JArray.Parse("[{\"path\" : \"" + jData1["FILE_NAME"].ToString() + "\"}" + ",{\"path\" : \"" + jData1["FILE_ATTACH"]?.ToString() + "\"}" + "]"));
                    requestSign.Data = jobSign;
                    new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSign.Action.ParentCode, item.PRODUCT_CODE + "_GCN" + item.CONTRACT_TYPE, "Gửi giấy chứng nhận Bảo hiểm HD", "EMAIL", "Gửi giấy chứng nhận Bảo hiểm HD",
                        "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), item.EMAIL, "", "", strBody, "X", "HIGH", "ALL", "abc", "Cấp giấy chứng nhận Bảo hiểm HDI", requestSign.Action.UserName)).Start();
                    new Task(() => goBussinessEmail.Instance.GetResponseEmail(requestSign)).Start();
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, basePDF + " loi ky so " + basePDF.ErrorMessage, Logger.ConcatName(nameClass, nameMethod));
                }

            }
            return basePDF;
        }
        public BaseResponse signEmail_cssk(InsurContract insuContract, List<InsurDetail> lsItem)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse basePDFReturn = new BaseResponse();
            //tạo file ký số và thông tin gửi mail cho người mua, người được bảo hiểm
            bool bSendOne = false;
            ServiceEmailModel emailContract = new ServiceEmailModel();
            if (string.IsNullOrEmpty(insuContract.EMAIL) || lsItem.Count == 1)
            {
                bSendOne = false;
            }
            if (!string.IsNullOrEmpty(insuContract.EMAIL))
            {
                emailContract.to = new List<EmailModel> { new EmailModel { email = insuContract.EMAIL } };
            }
            List<FileModel> lsFileContract = new List<FileModel>();

            //content, subject, file            
            List<ServiceEmailModel> lsEmailInsuDetail = new List<ServiceEmailModel>();
            JArray jArrayFileContract = new JArray();
            JArray jArrayFileDetail = new JArray();
            List<InsurDetail> lsItemLink = new List<InsurDetail>();
            int i = 1;
            foreach (InsurDetail item in lsItem)
            {
                ServiceEmailModel emailDetail = new ServiceEmailModel();
                BaseRequest requestSign = new BaseRequest();
                requestSign.Action = new ActionInfo();
                requestSign.Action.ActionCode = "GET_TEMPLATE";
                requestSign.Action.ParentCode = "HDI_PRIVATE";
                requestSign.Action.UserName = "HDI_PRIVATE";
                JObject jobSign = new JObject();
                jobSign.Add("USER_NAME", "HDI_PRIVATE");
                jobSign.Add("TEMP_CODE", item.PRODUCT_CODE + "_GCN");
                jobSign.Add("TYPE_RES", "BASE64");
                jobSign.Add("PARAM_CODE", "NO");
                jobSign.Add("PARAM_VALUE", item.CERTIFICATE_NO);
                jobSign.Add("TRANSACTION_ID", item.CERTIFICATE_NO);
                jobSign.Add("DATA_TYPE", "DATA");
                jobSign.Add("PRODUCT_CODE", item.PRODUCT_CODE);
                jobSign.Add("PACK_CODE", item.PACK_CODE);
                requestSign.Data = jobSign;
                BaseResponse basePDF = goBussinessPDF.Instance.convertPDF(requestSign);
                basePDFReturn = basePDF;
                if (basePDF.Success)
                {
                    JObject jData = JObject.Parse(basePDF.Data.ToString());
                    SignRequestModel signModel = new SignRequestModel();
                    signModel.USER_NAME = "HDI_PRIVATE";
                    signModel.LOCATION = "Thành phố Hồ Chí Minh";
                    signModel.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                    signModel.TypeResponse = "BASE64";
                    signModel.TextNote = "";
                    signModel.Alert = "";
                    signModel.FILE_REFF = item.CERTIFICATE_NO;
                    signModel.EXT_FILE = "HTMLHDI";
                    signModel.FILE = jData["FILE_BASE64"].ToString();
                    signModel.TEXT_CA = "";
                    signModel.TYPE_FILE = "BASE64";
                    signModel.TEMP_CODE = item.PRODUCT_CODE + "_GCN";
                    signModel.FILE_CODE = jData["FILE_CODE"].ToString();
                    signModel.PRODUCT_CODE = item.PRODUCT_CODE;
                    signModel.STRUCT_CODE = "HDI";
                    basePDF = goBussinessSign.Instance.signHDI("HDI_PRIVATE", "HDI_SIGN", "", signModel, null);
                    if (basePDF.Success)
                    {
                        JObject jSignRes = JObject.Parse(basePDF.Data.ToString());
                        //jData1["FILE_ATTACH"]?.ToString() 
                        FileModel fileContract = new FileModel();
                        if (lsItem.Count == 1)
                        {

                            emailContract.file = new List<FileModel>();
                            fileContract.path = jSignRes["FILE_NAME"].ToString();
                            fileContract.name = jSignRes["FILE_NAME"].ToString();
                            emailContract.file.Add(fileContract);
                        }
                        if (insuContract.EMAIL.Equals(item.EMAIL) || string.IsNullOrEmpty(item.EMAIL))
                        {
                            //jArrayFileContract.Add("{\"path\" : \"" + jSignRes["FILE_NAME"].ToString() + "\"}");
                            //jArrayFileContract.Add("{\"path\" : \"" + jSignRes["FILE_ATTACH"].ToString() + "\"}");                            
                            //fileContract.name = jSignRes["FILE_NAME"].ToString();

                        }
                        else
                        {
                            emailDetail.to = new List<EmailModel> { new EmailModel { email = item.EMAIL } };
                            //fileContract.name = jSignRes["FILE_NAME"].ToString();
                            emailDetail.file = new List<FileModel>();
                            fileContract.path = jSignRes["FILE_NAME"].ToString();
                            fileContract.name = jSignRes["FILE_NAME"].ToString();
                            emailDetail.file.Add(fileContract);
                            lsEmailInsuDetail.Add(emailDetail);
                            bSendOne = true;
                        }
                        item.REGION = jSignRes["FILE_URL"].ToString();
                        item.TAXCODE = i.ToString();
                        i++;
                        lsItemLink.Add(item);
                    }
                }

            }

            BaseRequest requestSendMail = new BaseRequest();
            requestSendMail.Action = new ActionInfo();
            requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
            requestSendMail.Action.ParentCode = "HDI_PRIVATE";
            requestSendMail.Action.UserName = "HDI_PRIVATE";
            string[] param = new string[] { requestSendMail.Action.ParentCode, "EMAIL", lsItem[0].PRODUCT_CODE + "_GCN", lsItem[0].PRODUCT_CODE, "null" };
            BaseResponse responseTemp = new BaseResponse();
            responseTemp = goBusinessCommon.Instance.executeProHDI("GET_TEMPLATE", strEvm, param);
            List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
            List<JObject> objTempValue1 = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);

            //xử lý send mail người nhận 

            if (objTemp == null || objTemp[0].Count == 0)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail khong co template " + requestSendMail.Action.ParentCode + lsItem[0].PRODUCT_CODE + "_GCN" + lsItem[0].PRODUCT_CODE, Logger.ConcatName(nameClass, nameMethod));
            }
            else
            {
                string strBody = "";
                List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                JArray arrValue = new JArray();
                JObject jo = new JObject();
                jo.Add("key", "NAME");
                jo.Add("value", insuContract.NAME);
                jo.Add("type", "string");
                JArray arrValue1 = new JArray();
                arrValue1.Add(jo);
                arrValue.Add(arrValue1);
                strBody = goBussinessPDF.Instance.replaceHTML_JSON(arrValue, objTemp[0]["DATA"].ToString(), lsReplace);
                if (objTempValue1 != null && objTempValue1.Count > 0)
                {
                    //string strHTMLTemp = objTempValue1[0]["DATA"].ToString();
                    //if (objTempValue1[0]["PARAM_NAME"].ToString().Equals(strParamNew))
                    //{
                    List<string> strListParamDetail = objTempValue1[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                    //vòng for dữ liệu truyền vào
                    string strReplaceDetail = "";
                    foreach (InsurDetail item1 in lsItem)
                    {
                        JObject jobj = JObject.FromObject(item1);
                        var prt = jobj.Properties();
                        string strTempHTMLDetail = objTempValue1[0]["DATA"].ToString();
                        //vòng for replate từng tham số
                        foreach (string strCTRP in strListParamDetail)
                        {
                            foreach (var prtct in prt)
                            {
                                if (prtct.Name.Equals(strCTRP))
                                {
                                    strTempHTMLDetail = strTempHTMLDetail.Replace("@" + strCTRP + "@", jobj[strCTRP].ToString());
                                    break;
                                }
                            }

                        }
                        strReplaceDetail = strReplaceDetail + strTempHTMLDetail;
                        //}
                        //break;
                    }
                    strBody = strBody.Replace("@" + objTempValue1[0]["PARAM_NAME"].ToString() + "@", strReplaceDetail);
                }
                strBody = goBussinessPDF.Instance.clearParram(strBody);
                emailContract.content = strBody;
                //jobSign.Add("content", strBody);
                emailContract.subject = objTemp[0]["TITLE"].ToString();
                //add cc,bcc
                List<string> lsBCC = objTemp[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                foreach (string strMail in lsBCC)
                {
                    if (!string.IsNullOrEmpty(strMail) && strMail.Length > lsItem[0].ORG_CODE.Length
                        && strMail.Substring(0, lsItem[0].ORG_CODE.Length).Equals(lsItem[0].ORG_CODE))
                    {
                        List<string> lsEmailBCC = strMail.Substring(lsItem[0].ORG_CODE.Length + 1).Split(',').ToList();
                        List<EmailModel> lsBCCModel = new List<EmailModel>();
                        foreach (string emaiBcc in lsEmailBCC)
                        {
                            EmailModel emaibcccc = new EmailModel();
                            emaibcccc.email = emaiBcc;
                            lsBCCModel.Add(emaibcccc);
                        }
                        emailContract.bcc = lsBCCModel;
                        break;
                    }
                }
                requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                //new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSendMail.Action.ParentCode, lsItem[0].PRODUCT_CODE + "_GCN" + lsItem[0].CONTRACT_TYPE, "Gửi giấy chứng nhận Bảo hiểm HD", "EMAIL", "Gửi giấy chứng nhận Bảo hiểm HD",
                //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Cấp giấy chứng nhận Bảo hiểm HDI", "HDI_Send")).Start();
                goBussinessEmail cls = new goBussinessEmail();
                cls.createDBSendMail(requestSendMail.Action.ParentCode, lsItem[0].PRODUCT_CODE + "_GCN", "Gửi giấy chứng nhận Bảo hiểm HD", "EMAIL", "Gửi giấy chứng nhận Bảo hiểm HD",
                    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Cấp giấy chứng nhận Bảo hiểm HDI", "HDI_Send");

                cls.GetResponseEmail(requestSendMail);
                if (bSendOne)
                {
                    //xử lý send mail người được bảo hiểm
                    foreach (ServiceEmailModel emailChiTiet in lsEmailInsuDetail)
                    {
                        string strBody1 = "";
                        BaseRequest requestSendMailCT = new BaseRequest();
                        requestSendMailCT.Action = new ActionInfo();
                        requestSendMailCT.Action.ActionCode = "HDI_EMAIL_01";
                        requestSendMailCT.Action.ParentCode = "HDI_PRIVATE";
                        requestSendMailCT.Action.UserName = "HDI_PRIVATE";
                        arrValue = new JArray();
                        jo = new JObject();
                        jo.Add("key", "NAME");
                        jo.Add("value", insuContract.NAME);
                        jo.Add("type", "string");
                        arrValue1 = new JArray();
                        arrValue1.Add(jo);
                        arrValue.Add(arrValue1);
                        strBody1 = goBussinessPDF.Instance.replaceHTML_JSON(arrValue, objTemp[0]["HEADER"].ToString(), lsReplace);
                        emailChiTiet.content = strBody1;
                        emailChiTiet.subject = objTemp[0]["TITLE"].ToString();
                        //add cc,bcc
                        if (!bSendOne)
                        {
                            List<string> lsBCCTem = objTemp[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                            foreach (string strMail in lsBCCTem)
                            {
                                if (!string.IsNullOrEmpty(strMail) && strMail.Length > lsItem[0].ORG_CODE.Length
                                    && strMail.Substring(0, lsItem[0].ORG_CODE.Length).Equals(lsItem[0].ORG_CODE))
                                {
                                    List<string> lsEmailBCC = strMail.Substring(lsItem[0].ORG_CODE.Length + 1).Split(',').ToList();
                                    List<EmailModel> lsBCCModel = new List<EmailModel>();
                                    foreach (string emaiBcc in lsEmailBCC)
                                    {
                                        EmailModel emaibcccc = new EmailModel();
                                        emaibcccc.email = emaiBcc;
                                        lsBCCModel.Add(emaibcccc);
                                    }
                                    emailChiTiet.bcc = lsBCCModel;
                                    break;
                                }
                            }
                        }
                        cls = new goBussinessEmail();
                        requestSendMailCT.Data = JsonConvert.SerializeObject(emailChiTiet);
                        cls.createDBSendMail(requestSendMail.Action.ParentCode, lsItem[0].PRODUCT_CODE + "_GCN" + lsItem[0].CONTRACT_TYPE, "Gửi giấy chứng nhận Bảo hiểm HD", "EMAIL", "Gửi giấy chứng nhận Bảo hiểm HD",
                            "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Cấp giấy chứng nhận Bảo hiểm HDI", "HDI");
                        cls.GetResponseEmail(requestSendMailCT);
                    }
                }
            }


            return basePDFReturn;
        }
        public bool sendEmailContent(string strOrgCode, string strTempCode, string strEmailTo, JObject jInput)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseRequest requestSendMail = new BaseRequest();
                requestSendMail.Action = new ActionInfo();
                requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                requestSendMail.Action.ParentCode = strOrgCode;// "HDI_PRIVATE";
                requestSendMail.Action.UserName = "HDI_PRIVATE";
                string[] param = new string[] { requestSendMail.Action.ParentCode, "EMAIL", strTempCode, strTempCode, "null" };
                BaseResponse responseTemp = new BaseResponse();
                responseTemp = goBusinessCommon.Instance.executeProHDI("GET_TEMPLATE", strEvm, param);
                List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                List<JObject> objTempValue1 = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);

                if (objTemp == null || objTemp.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail khong co template " + requestSendMail.Action.ParentCode + strTempCode, Logger.ConcatName(nameClass, nameMethod));
                }
                else
                {

                    string strBody = "";
                    List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    strBody = objTemp[0]["DATA"].ToString();
                    var prt = jInput.Properties();
                    //vòng for replate từng tham số
                    foreach (string strCTRP in lsReplace)
                    {
                        foreach (var prtct in prt)
                        {
                            if (prtct.Name.Equals(strCTRP))
                            {
                                strBody = strBody.Replace("@" + strCTRP + "@", jInput[strCTRP].ToString());
                                break;
                            }
                        }

                    }
                    ServiceEmailModel emailContract = new ServiceEmailModel();
                    strBody = goBussinessPDF.Instance.clearParram(strBody);
                    emailContract.content = strBody;
                    //jobSign.Add("content", strBody);
                    emailContract.subject = objTemp[0]["TITLE"].ToString();
                    emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailTo } };
                    //add cc,bcc
                    List<string> lsBCC = objTemp[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                    foreach (string strMail in lsBCC)
                    {
                        if (!string.IsNullOrEmpty(strMail) && strMail.Length > requestSendMail.Action.ParentCode.Length
                            && strMail.Substring(0, requestSendMail.Action.ParentCode.Length).Equals(requestSendMail.Action.ParentCode))
                        {
                            List<string> lsEmailBCC = strMail.Substring(requestSendMail.Action.ParentCode.Length + 1).Split(',').ToList();
                            List<EmailModel> lsBCCModel = new List<EmailModel>();
                            foreach (string emaiBcc in lsEmailBCC)
                            {
                                EmailModel emaibcccc = new EmailModel();
                                emaibcccc.email = emaiBcc;
                                lsBCCModel.Add(emaibcccc);
                            }
                            emailContract.bcc = lsBCCModel;
                            break;
                        }
                    }
                    requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                    //new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Nội dung chuyển khoản",
                    //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), strEmailTo, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send")).Start();
                    goBussinessEmail cls = new goBussinessEmail();
                    cls.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Nội dung chuyển khoản",
                        "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), strEmailTo, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send");
                    goBussinessEmail.Instance.GetResponseEmail(requestSendMail);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, " loi send mail chuyen khoan " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                return false;
            }
            return true;
        }

        public bool sendEmailGCN_core(string strFile, string strFileUrl, JObject jInput, JObject jExtData)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseRequest requestSendMail = new BaseRequest();
                requestSendMail.Action = new ActionInfo();
                requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                requestSendMail.Action.ParentCode = "HDI_PRIVATE";
                requestSendMail.Action.UserName = "HDI_PRIVATE";

                string[] param = new string[] { requestSendMail.Action.ParentCode, "EMAIL", jInput["TEMP_CODE"]?.ToString(), jInput["TEMP_CODE"]?.ToString(), "null" };//pack_code
                BaseResponse responseTemp = new BaseResponse();
                responseTemp = goBusinessCommon.Instance.executeProHDI("GET_TEMPLATE", strEvm, param);
                List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                if (objTemp == null || objTemp[0].Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail khong co template " + requestSendMail.Action.ParentCode + jInput["TEMP_CODE"]?.ToString(), Logger.ConcatName(nameClass, nameMethod));
                }
                else
                {
                    string strBody = "";
                    List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    strBody = objTemp[0]["DATA"].ToString();
                    if (jExtData != null)
                    {
                        var prt = jExtData.Properties();
                        //vòng for replate từng tham số
                        foreach (string strCTRP in lsReplace)
                        {
                            foreach (var prtct in prt)
                            {
                                if (prtct.Name.Equals(strCTRP))
                                {
                                    if (strCTRP.ToLower().Equals("phi"))
                                    {
                                        strBody = strBody.Replace("@" + strCTRP.Trim() + "@", new GO.HELPER.Utility.goUtility().FomatMoneyVN(jExtData[strCTRP]?.ToString().Replace(".", "")));
                                    }
                                    else
                                    strBody = strBody.Replace("@" + strCTRP.Trim() + "@", jExtData[strCTRP]?.ToString());
                                    break;
                                }
                            }

                        }
                    }
                    strBody = goBussinessPDF.Instance.clearParram(strBody);
                    ServiceEmailModel emailContract = new ServiceEmailModel();
                    //strBody = goBussinessPDF.Instance.clearParram(strBody);
                    strBody = strBody.Replace("@LINK@", strFileUrl);
                    emailContract.content = strBody;
                    //jobSign.Add("content", strBody);
                    emailContract.subject = objTemp[0]["TITLE"].ToString();
                    FileModel fileContract = new FileModel();
                    emailContract.file = new List<FileModel>();
                    fileContract.name = strFile;
                    fileContract.path = strFile;
                    emailContract.file.Add(fileContract);
                    //add cc,bcc
                    if (!string.IsNullOrEmpty(jInput["MAIL_CC"]?.ToString()))
                    {
                        List<string> lsBCC = objTemp[0]["MAIL_CC"].ToString().Split(';').ToList();
                        List<EmailModel> lsBCCModel = new List<EmailModel>();
                        foreach (string strMail in lsBCC)
                        {
                            EmailModel emaibcccc = new EmailModel();
                            emaibcccc.email = strMail;
                            lsBCCModel.Add(emaibcccc);
                            emailContract.cc = lsBCCModel;
                        }
                    }
                    if (!string.IsNullOrEmpty(jInput["MAIL_BCC"]?.ToString()))
                    {
                        List<string> lsBCC = objTemp[0]["MAIL_BCC"].ToString().Split(';').ToList();
                        List<EmailModel> lsBCCModel = new List<EmailModel>();
                        foreach (string strMail in lsBCC)
                        {
                            EmailModel emaibcccc = new EmailModel();
                            emaibcccc.email = strMail;
                            lsBCCModel.Add(emaibcccc);
                            emailContract.bcc = lsBCCModel;
                        }
                    }
                    goBussinessEmail cls = new goBussinessEmail();
                    if (!string.IsNullOrEmpty(jInput["MAIL_BUYER"]?.ToString()))
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = jInput["MAIL_BUYER"]?.ToString() } };
                        requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                        //cls.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Gửi mail core",
                        //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send");
                        cls.GetResponseEmail(requestSendMail);
                        emailContract.bcc = null;
                        emailContract.cc = null;
                    }
                    if (!string.IsNullOrEmpty(jInput["MAIL_INSURED"]?.ToString()))
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = jInput["MAIL_INSURED"]?.ToString() } };
                        requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                        //cls.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Nội dung chuyển khoản",
                        //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send");
                        cls.GetResponseEmail(requestSendMail);
                        emailContract.bcc = null;
                        emailContract.cc = null;
                    }
                    if (!string.IsNullOrEmpty(jInput["MAIL_BENEFICIARY"]?.ToString()))
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = jInput["MAIL_BENEFICIARY"]?.ToString() } };
                        requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                        //cls.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Gửi mail core",
                        //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send");
                        cls.GetResponseEmail(requestSendMail);
                        emailContract.bcc = null;
                        emailContract.cc = null;
                    }
                    if (!string.IsNullOrEmpty(jInput["MAIL_USER"]?.ToString()))
                    {
                        emailContract.to = new List<EmailModel> { new EmailModel { email = jInput["MAIL_USER"]?.ToString() } };
                        requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                        //cls.createDBSendMail(requestSendMail.Action.ParentCode, "HDI_PRIVATE", "Gửi nội dung chuyển khoản", "EMAIL", "Gửi mail core",
                        //    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), emailContract.to[0].email, "", "", strBody, "X", "HIGH", "ALL", "abc", "Nội dung chuyển khoản", "HDI_Send");
                        cls.GetResponseEmail(requestSendMail);
                        emailContract.bcc = null;
                        emailContract.cc = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, " loi send mail gcn core " + ex.ToString(), Logger.ConcatName(nameClass, nameMethod));
                return false;
            }
            return true;
        }

        public BaseResponse signVietJet(InsurDetail item)
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien gui mail : " + JsonConvert.SerializeObject(item), Logger.ConcatName(nameClass, nameMethod));
            BaseRequest requestSign = new BaseRequest();
            requestSign.Action = new ActionInfo();
            //thực hiện ký số
            requestSign.Action.ActionCode = "GET_TEMPLATE";
            requestSign.Action.ParentCode = item.ORG_CODE;
            requestSign.Action.UserName = "HDI_PRIVATE";
            JObject jobSign = new JObject();
            jobSign.Add("USER_NAME", "HDI_PRIVATE");
            jobSign.Add("TEMP_CODE", item.PRODUCT_CODE + "_GCN");
            jobSign.Add("TYPE_RES", "BASE64");
            jobSign.Add("PARAM_CODE", "NO");
            jobSign.Add("PARAM_VALUE", item.CERTIFICATE_NO);
            jobSign.Add("TRANSACTION_ID", item.CERTIFICATE_NO);
            jobSign.Add("DATA_TYPE", "DATA");
            jobSign.Add("PRODUCT_CODE", item.PRODUCT_CODE);
            jobSign.Add("PACK_CODE", item.PACK_CODE);
            requestSign.Data = jobSign;
            //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien tao file: " + JsonConvert.SerializeObject(requestSign), Logger.ConcatName(nameClass, nameMethod));
            BaseResponse basePDF = goBussinessPDF.Instance.convertPDF(requestSign);
            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "VJC cver : " + 1, Logger.ConcatName(nameClass, nameMethod));
            if (basePDF.Success)
            {
                JObject jData = JObject.Parse(basePDF.Data.ToString());
                SignRequestModel signModel = new SignRequestModel();
                signModel.USER_NAME = "HDI_PRIVATE";
                signModel.LOCATION = "Thành phố Hồ Chí Minh";
                signModel.REASON = "Cấp giấy chứng nhận Bảo hiểm";
                signModel.TypeResponse = "BASE64";
                signModel.TextNote = "";
                signModel.Alert = "";
                signModel.FILE_REFF = item.CERTIFICATE_NO;
                signModel.EXT_FILE = "HTMLHDI";
                signModel.FILE = jData["FILE_BASE64"].ToString();
                signModel.TEXT_CA = "";
                signModel.TYPE_FILE = "BASE64";
                signModel.TEMP_CODE = item.PRODUCT_CODE + "_GCN";
                signModel.FILE_CODE = jData["FILE_CODE"].ToString();
                signModel.PRODUCT_CODE = item.PRODUCT_CODE;
                signModel.STRUCT_CODE = "HDI";
                //signModel.PACK_CODE = item.PACK_CODE;
                // Logger.Instance.WriteLog(Logger.TypeLog.INFO, "thuc hien ky so: " + JsonConvert.SerializeObject(signModel), Logger.ConcatName(nameClass, nameMethod));
                basePDF = goBussinessSign.Instance.signHDI(item.ORG_CODE, "HDI_SIGN", "", signModel, null);
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "VJC sig END: ", Logger.ConcatName(nameClass, nameMethod));
                if (basePDF.Success)
                {

                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, basePDF + " loi ky so " + basePDF.ErrorMessage, Logger.ConcatName(nameClass, nameMethod));
                }

            }
            return basePDF;
        }
        public BaseResponse signVietJetBatch(InsurContract insurContract)
        {
            BaseResponse basePDF = new BaseResponse();
            basePDF = goBussinessSign.Instance.createAndSignBatch(insurContract.CONTRACT_CODE, insurContract.CATEGORY, insurContract.PRODUCT_CODE, insurContract.ORG_CODE);
            if (basePDF.Success)
            {
                //Xử lý send mail tiếp theo
                JArray arr = JArray.Parse(basePDF.Data.ToString());
                JArray jarrReturn = new JArray();
                foreach (JToken jtk in arr)
                {
                    JObject job = new JObject();
                    job.Add("FILE_NAME", jtk["FILE_NAME"].ToString());
                    job.Add("FILE_REFF", jtk["FILE_REFF"].ToString());
                    jarrReturn.Add(job);
                }
                basePDF.Data = jarrReturn;
            }
            return basePDF;
        }
        public BaseResponse getQueueSign(JObject request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strMD5 = goEncryptBase.Instance.Md5Encode(DateTime.Now.ToString("ddMMyyyy") + "HDI@2022" + request["ACTION"].ToString()+ request["APP_NAME"].ToString()).ToLower() ;
                object[] param;
                if (strMD5.Equals(request["MD5"].ToString()))
                {
                    switch (request["ACTION"].ToString())
                    {
                        case "APIYF2RRO1": //lấy hàng đợi
                                {
                                param = new object[] { request["STATUS"].ToString(), request["COUNT"].ToString(), request["CATEGORY"].ToString(), request["PRODUCT_CODE"].ToString() };
                                response = goBusinessCommon.Instance.exePackagePrivate("APIYF2RRO1", strEvm, param); 
                                break;
                            }
                        case "APIULQTPYQ": //update trạng thái hàng đợi
                            {
                                param = new object[] { request["ORG_CODE"].ToString(),JArray.Parse(request["QUEUE"].ToString()) };
                                response = goBusinessCommon.Instance.exePackagePrivate("APIULQTPYQ", strEvm, param);
                                break;
                            }
                        case "APIC15CT0G": //lấy mẫu giấy chứng nhận
                            {
                                JArray JarrInput = JArray.Parse(request["QUEUE"].ToString());
                                param = new object[] { JarrInput };
                                JObject jobjQueueInput = (JObject) JarrInput[0];
                                string strEff = jobjQueueInput["EFFECTIVE_NUM"]?.ToString();
                                DateTime dtEFF = DateTime.Now;
                                if (!string.IsNullOrEmpty(strEff))
                                    dtEFF = getDateEff(strEff);
                                //DateTime dtEFF = getDateEff(iContract.EFFECTIVE_NUM); //Tam fix ngày hiệu lực // 
                                TempConfigModel template = get_all_template(jobjQueueInput["ORG_SELLER"].ToString(), "null", jobjQueueInput["PRODUCT_CODE"].ToString(), "null",
                                    "GCN", "tool-sign", "WEB", dtEFF.ToString("yyyyMMddHHmmss"), strEvm);
                                if (template != null && template.TEMP_EMAIL != null)
                                {
                                    template.TEMP_SIGN.DATA_FILE = "";
                                    template.TEMP_SIGN.CER_FILE = "";
                                    template.TEMP_SIGN.S_IMAGE = "";
                                    template.TEMP_SIGN.F_IMAGE = "";
                                    //chưa xử lý mẫu hiển thị 
                                    response = goBusinessCommon.Instance.exePackagePrivate("APIC15CT0G", strEvm, param);
                                    List<JObject> arrayAll = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                                    if(arrayAll !=null && arrayAll.Count > 0)
                                    {
                                        string strpathserver = "";
                                        int i = 0;
                                        string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/";
                                        List<string> lsFileID = goBusinessCommon.Instance.getFileIDbefore(arrayAll.Count, "pdf", "hdi_gcn_baohiem", "/" + jobjQueueInput["PRODUCT_CODE"].ToString(), "TOOL-SIGN", ref strpathserver);
                                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "da lay duoc danh sach dat cho " + lsFileID.Count + " So luong " + arrayAll.Count.ToString(), Logger.ConcatName(nameClass, nameMethod));
                                        if (lsFileID !=null && lsFileID.Count == arrayAll.Count)
                                        {
                                            List<FileQueueSignModel> lsFileCreate = new List<FileQueueSignModel>();
                                            foreach (JObject objAll in arrayAll)
                                            {
                                                JObject jobj = JObject.Parse(objAll["DATA_PDF"].ToString().Replace("\\", ""));
                                                InsurContract ctr = JsonConvert.DeserializeObject<InsurContract>(jobj["CONTRACT"].ToString());
                                                InsurDetail ctr_detail = JsonConvert.DeserializeObject<InsurDetail>(jobj["DETAIL"].ToString());
                                                TempEmailModel tempMail = new TempEmailModel();
                                                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "den buoc get file" , Logger.ConcatName(nameClass, nameMethod));
                                                //làm trường hợp chỉ có 1 mẫu
                                                TempEmailModel lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF").FirstOrDefault();
                                                SignFileReturnModel fileReturn = new SignFileReturnModel();
                                                if (lstempGCN.DATA_SRC.Equals("JSON"))
                                                    fileReturn = getFileSign(ctr, ctr_detail, template, lsFileID[i], ref tempMail);
                                                else
                                                    fileReturn = getFileSign_Data(JObject.Parse(jobj["CONTRACT"].ToString()), JObject.Parse(jobj["DETAIL"].ToString()), template, lsFileID[i], ref tempMail);
                                                if (fileReturn == null)
                                                {
                                                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Loi tao file giay chung nhan", Logger.ConcatName(nameClass, nameMethod));
                                                }
                                                if (!string.IsNullOrEmpty(tempMail.URL_PAGE_ADD)){
                                                    tempMail.URL_PAGE_ADD = urrl + tempMail.URL_PAGE_ADD;
                                                }
                                                i++;
                                                FileQueueSignModel fileModel = new FileQueueSignModel();
                                                fileModel.APP_ID = request["APP_NAME"].ToString();
                                                fileModel.BASE64 = fileReturn.FILE_BASE64;
                                                fileModel.CATEGORY = ctr.CATEGORY;
                                                fileModel.CERTIFICATE_NO = ctr_detail.CERTIFICATE_NO;
                                                fileModel.CONTRACT_CODE = ctr.CONTRACT_CODE;
                                                fileModel.FILE_ID = fileReturn.FILE_ID;
                                                fileModel.FILE_LINK = urrl + fileReturn.FILE_URL;
                                                fileModel.FILE_PATH = strpathserver;
                                                fileModel.ID = Convert.ToInt32(jobj["ID"].ToString());
                                                fileModel.ORG_SELLER = jobj["ORG_SELLER"].ToString();
                                                fileModel.PACK_CODE = ctr_detail.PACK_CODE;
                                                fileModel.TYPE = "GCN";
                                                fileModel.STATUS = "SIGN";
                                                fileModel.SIGN_TEMP = template.TEMP_SIGN;
                                                fileModel.GCN_TEMP = tempMail;
                                                fileModel.PRODUCT_CODE = ctr.PRODUCT_CODE;
                                                lsFileCreate.Add(fileModel);
                                            }
                                            JArray arr = JArray.FromObject(lsFileCreate);
                                            JArray arrReturn = new JArray();
                                            arrReturn.Add(arr);
                                            response.Data = arrReturn;
                                        }
                                        
                                    }
                                    
                                }
                                break;
                            }
                        case "GET_EMAIL":
                            {
                                JArray JarrInput = JArray.Parse(request["QUEUE"].ToString());
                                param = new object[] { JarrInput };
                                JObject jobjQueueInput = (JObject)JarrInput[0];
                                //DateTime dtEFF = getDateEff(iContract.EFFECTIVE_NUM); //Tam fix ngày hiệu lực
                                TempConfigModel template = get_all_template(jobjQueueInput["ORG_SELLER"].ToString(), "null", jobjQueueInput["PRODUCT_CODE"].ToString(), "null",
                                    "GCN", "tool-sign", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                                if (template != null && template.TEMP_EMAIL != null)
                                {
                                    template.TEMP_SIGN = null;
                                    //chưa xử lý mẫu hiển thị 
                                    response = goBusinessCommon.Instance.exePackagePrivate("APIC15CT0G", strEvm, param);
                                    List<JObject> arrayAll = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                                    if (arrayAll != null && arrayAll.Count > 0)
                                    {
                                        int i = 0;
                                        string strQrHeader = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                                        List<FileQueueSignModel> lsFileCreate = new List<FileQueueSignModel>();
                                        foreach (JObject objAll in arrayAll)
                                        {
                                            JObject jobj = JObject.Parse(objAll["DATA_PDF"].ToString().Replace("\\", ""));
                                            InsurContract ctr = JsonConvert.DeserializeObject<InsurContract>(jobj["CONTRACT"].ToString());
                                            InsurDetail ctr_detail = JsonConvert.DeserializeObject<InsurDetail>(jobj["DETAIL"].ToString());                                            
                                            TempEmailModel tempMail = getEmailContent(ctr, ctr_detail, template, strQrHeader, jobj["FILE_ID"].ToString());
                                            i++;
                                            FileQueueSignModel fileModel = new FileQueueSignModel();
                                            fileModel.APP_ID = request["APP_NAME"].ToString();
                                            fileModel.BASE64 = "";
                                            fileModel.CATEGORY = ctr.CATEGORY;
                                            fileModel.CERTIFICATE_NO = ctr_detail.CERTIFICATE_NO;
                                            fileModel.CONTRACT_CODE = ctr.CONTRACT_CODE;
                                            fileModel.FILE_ID = jobj["FILE_ID"].ToString();
                                            fileModel.FILE_LINK = jobj["FILE_LINK"].ToString();
                                            fileModel.FILE_PATH = jobj["FILE_PATH"].ToString();
                                            fileModel.ID = Convert.ToInt32(jobj["ID"].ToString());
                                            fileModel.ORG_SELLER = jobj["ORG_SELLER"].ToString();
                                            fileModel.PACK_CODE = ctr_detail.PACK_CODE;
                                            fileModel.TYPE = "GCN";
                                            fileModel.STATUS = "EMAIL";
                                            fileModel.SIGN_TEMP = null;
                                            fileModel.GCN_TEMP = tempMail;
                                            fileModel.PRODUCT_CODE = ctr.PRODUCT_CODE;
                                            lsFileCreate.Add(fileModel);
                                        }
                                        JArray arr = JArray.FromObject(lsFileCreate);
                                        JArray arrReturn = new JArray();
                                        arrReturn.Add(arr);
                                        response.Data = arrReturn;

                                    }

                                }
                                break;
                            }
                        case "GET_EMAIL_ALL":
                            {
                                JArray JarrInput = JArray.Parse(request["QUEUE"].ToString());
                                param = new object[] { JarrInput };
                                JObject jobjQueueInput = (JObject)JarrInput[0];
                                //DateTime dtEFF = getDateEff(iContract.EFFECTIVE_NUM); //Tam fix ngày hiệu lực
                                TempConfigModel template = get_all_template(jobjQueueInput["ORG_SELLER"].ToString(), "null", jobjQueueInput["PRODUCT_CODE"].ToString(), "null",
                                    "GCN", "tool-sign", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                                if (template != null && template.TEMP_EMAIL != null)
                                {
                                    template.TEMP_SIGN = null;
                                    //chưa xử lý mẫu hiển thị 
                                    List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL" && m.TYPE_SEND != "ONE").ToList();
                                    if(lstempGCN == null || lstempGCN.Count == 0)
                                    {
                                        response.Success = false;
                                        response.Data = "[]";
                                        return response;
                                    }
                                    lstempGCN[0].TITLE = template.DESCRIPTION;
                                    lstempGCN[0].DATA = Base64Encode(lstempGCN[0].DATA);
                                    lstempGCN[0].HEADER = template.MAIL_BCC;
                                    lstempGCN[0].FOOTER = "";
                                    lstempGCN[0].URL_PAGE_ADD = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                                    response = goBusinessCommon.Instance.exePackagePrivate("APIC15CT0G", strEvm, param);
                                    List<JObject> arrayAll = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                                    if (arrayAll != null && arrayAll.Count > 0)
                                    {                                               
                                        JArray arr = new JArray();
                                        foreach(JObject job in arrayAll)
                                        {
                                            JObject objCT = JObject.Parse(job["DATA_PDF"].ToString());
                                            arr.Add(objCT);
                                        }
                                        JArray arrTemp = JArray.FromObject(lstempGCN);
                                        JArray arrReturn = new JArray();
                                        arrReturn.Add(arr);
                                        arrReturn.Add(arrTemp);
                                        response.Data = arrReturn;
                                    }

                                }
                                break;
                            }
                        case "INSER_SIGN":
                            {

                                param = new object[] { request["ORG_CODE"].ToString(), "HDI", JArray.Parse(request["QUEUE"].ToString()) };//jInput['FILE'].ToString()
                                response = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.sign_InsBatch_Json, strEvm, param);                                
                                List<JObject> objBatch = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                                if (objBatch == null || !objBatch.Any())
                                {
                                    response.Success = false;
                                    response.ErrorMessage = "Không insert được dữ liệu ký số vào DB";
                                }
                                break;
                            }
                        case "INSER_LOG_MAIL":
                            {

                                //param = new object[] { request["ORG_CODE"].ToString(), "HDI", JArray.Parse(request["QUEUE"].ToString()) };//jInput['FILE'].ToString()
                                List<logEmailModel> lsLogSend = JsonConvert.DeserializeObject<List<logEmailModel>>(request["QUEUE"].ToString());
                                //goBussinessEmail.Instance.createLogSendMailJson(lsLogSend, request["ORG_CODE"].ToString(), strEvm);
                                new Task(() => goBussinessEmail.Instance.createLogSendMailJson(lsLogSend, request["ORG_CODE"].ToString(), strEvm)).Start();
                                response.Success = true;
                                response.ErrorMessage = "Insert log mail";
                                break;
                            }
                    }
                    
                }
                else
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003 + " MD");
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_3003), goConstantsError.Instance.ERROR_3003);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "getQueueSign", request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public SignFileReturnModel getFileSign(InsurContract iContract, InsurDetail lsDetailsAll, TempConfigModel template, string strGuiID_File, ref TempEmailModel tempMail) ///NO , 
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();

            JObject jInput = JObject.FromObject(iContract);
            JObject jarrInput = JObject.FromObject(lsDetailsAll);
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", "tool_sign");
            jInput.Add("CHANNEL_CALL", "WEB");
            jInput.Add("TYPE_CALL", "NO");
            try
            {
                List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                InsurContractExt lsInsContractExt = new InsurContractExt();
                JObject jValueContract = new JObject();// = JObject.FromObject(iContract);                    
                List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF").ToList();
                TempEmailModel tempGCN = new TempEmailModel();
                if (lstempGCN == null && lstempGCN.Count == 0)
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                    goBussinessEmail.Instance.sendEmaiCMS("", "Mẫu giấy chứng nhận tool ko tìm thấy", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE );
                    return null;
                }
                if (template.TEMP_SIGN == null || template.TEMP_SIGN.ID_KEY.Length == 0)
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract.PRODUCT_CODE;
                    goBussinessEmail.Instance.sendEmaiCMS("", "Chưa khai báo quyền ký số", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                    return null;
                }
                //có 2 trường hợp tạo GCN: tạo GCN cho người mua (ALL) và tạo GCN cho từng người được bảo hiểm (ONE)
                List<JObject> lsDataExt = new List<JObject>();
                Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                tempGCN = lstempGCN[0];
                List<JObject> lsDetail = new List<JObject>();
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "tool ko ky loa all " + iContract.PRODUCT_CODE;
                    goBussinessEmail.Instance.sendEmaiCMS("", "tool ko ky loa all", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                    return null;
                }
                else
                {
                    lsInsDetailExt = getDetailExt(iContract, new List<InsurDetail> { lsDetailsAll }, ref lsInsContractExt);                   
                    foreach (InsurDetailExt itExt in lsInsDetailExt)
                    {
                        JObject obj = JObject.FromObject(itExt);
                        obj.Add("DETAIL", null);
                        lsGenGCN.Add(itExt.CERTIFICATE_NO, obj);
                    }
                }
                SignFileReturnModel filePdf = new SignFileReturnModel();
                string strUrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "f/";
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    return null;
                }
                else
                {
                    string strQrHeaderURL = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    foreach (InsurDetailExt itemExt in lsInsDetailExt)
                    {
                        tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && m.PACK_CODE == itemExt.PACK_CODE).FirstOrDefault();
                        if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận tool" + iContract.PRODUCT_CODE + itemExt.PACK_CODE;
                            goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận tool", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE + "_" + itemExt.PACK_CODE);
                            return null;
                        }
                        string strHeader = tempGCN.HEADER;    
                        string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlValueJson(lsGenGCN[itemExt.CERTIFICATE_NO], tempGCN);
                        string strQrHeader = strQrHeaderURL + strGuiID_File;
                        strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                        strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                        tempMail =new TempEmailModel() ;
                        tempMail.LIST_REPLACE = tempGCN.LIST_REPLACE;
                        tempMail.URL_PAGE_ADD = tempGCN.URL_PAGE_ADD;
                        tempMail.HEADER = Base64Encode(strHeader);
                        tempMail.FOOTER = Base64Encode(tempGCN.FOOTER);
                        tempMail.IS_BACK_DATE = tempGCN.IS_BACK_DATE;
                        tempMail.DATA_SRC = itemExt.EFFECTIVE_NUM;
                        filePdf.FILE_BASE64 = Base64Encode(strBodyHtml);
                        filePdf.FILE_ID = strGuiID_File;
                        filePdf.FILE_REF = itemExt.CERTIFICATE_NO;
                        filePdf.Success = true;
                        filePdf.dtSignBack = DateTime.Now;
                        filePdf.USER_NAME = "tool_sign";
                        filePdf.FILE_URL = strGuiID_File;
                        return filePdf;
                    }
                    jValueContract.Add("DETAIL", JArray.FromObject(lsInsDetailExt));

                }
            }            
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail Exception service-signAndSendMail 336", jInput.ToString() + " ---" + ex.ToString());
                return null;
            }
            return null;
        }
        public SignFileReturnModel getFileSign_Data(JObject iContract, JObject lsDetailsAll, TempConfigModel template, string strGuiID_File, ref TempEmailModel tempMail) ///NO , 
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();

            JObject jInput = iContract;
            JObject jarrInput = lsDetailsAll;
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", "tool_sign");
            jInput.Add("CHANNEL_CALL", "WEB");
            jInput.Add("TYPE_CALL", "NO");
            try
            {                
                JObject jValueContract = new JObject();// = JObject.FromObject(iContract);                    
                List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF").ToList();
                TempEmailModel tempGCN = new TempEmailModel();
                if (lstempGCN == null && lstempGCN.Count == 0)
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract["PRODUCT_CODE"].ToString() ;
                    goBussinessEmail.Instance.sendEmaiCMS("", "Mẫu giấy chứng nhận tool ko tìm thấy", iContract["ORG_SELLER"].ToString() + "_" + iContract["PRODUCT_CODE"].ToString());
                    return null;
                }
                if (template.TEMP_SIGN == null || template.TEMP_SIGN.ID_KEY.Length == 0)
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận " + iContract["PRODUCT_CODE"].ToString();
                    goBussinessEmail.Instance.sendEmaiCMS("", "Chưa khai báo quyền ký số", iContract["ORG_SELLER"].ToString() + "_" + iContract["PRODUCT_CODE"].ToString());
                    return null;
                }
                //có 2 trường hợp tạo GCN: tạo GCN cho người mua (ALL) và tạo GCN cho từng người được bảo hiểm (ONE)
                List<JObject> lsDataExt = new List<JObject>();
                Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                tempGCN = lstempGCN[0];
                List<JObject> lsDetail = new List<JObject>();
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "tool ko ky loa all " + iContract["PRODUCT_CODE"].ToString();
                    goBussinessEmail.Instance.sendEmaiCMS("", "tool ko ky loa all", iContract["ORG_SELLER"].ToString() + "_" + iContract["PRODUCT_CODE"].ToString());
                    return null;
                }
                else
                {
                        JObject obj = lsDetailsAll;
                        obj.Add("DETAIL", null);
                        lsGenGCN.Add(lsDetailsAll["CERTIFICATE_NO"].ToString(), obj);                    
                }
                SignFileReturnModel filePdf = new SignFileReturnModel();
                string strUrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "f/";
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    return null;
                }
                else
                {
                    string strQrHeaderURL = goBusinessCommon.Instance.GetConfigDB("ECER") + "?id=";
                    tempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "PDF" && m.PACK_CODE == lsDetailsAll["PACK_CODE"].ToString()).FirstOrDefault();
                        if (tempGCN == null || string.IsNullOrEmpty(tempGCN.PACK_CODE))
                        {
                            baseResponse.Success = false;
                            baseResponse.ErrorMessage = "không lấy được mẫu giấy chứng nhận tool" + iContract["PRODUCT_CODE"].ToString() + lsDetailsAll["PACK_CODE"].ToString();
                            goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu giấy chứng nhận tool", iContract["ORG_SELLER"].ToString() + "_" + iContract["PRODUCT_CODE"].ToString() + "_" + lsDetailsAll["PACK_CODE"].ToString());
                            return null;
                        }
                        string strHeader = tempGCN.HEADER;
                        string strBodyHtml = goBusinessTemplate.Instance.replaceHtmlValueJson(lsGenGCN[lsDetailsAll["CERTIFICATE_NO"].ToString()], tempGCN);
                        string strQrHeader = strQrHeaderURL + strGuiID_File;
                        strHeader = strHeader.Replace("@QR_CODE@", goBussinessPDF.Instance.getQrCode(strQrHeader, "Q", 8, ""));
                        strBodyHtml = goBussinessPDF.Instance.clearParram(strBodyHtml);
                        tempMail = new TempEmailModel();
                        tempMail.LIST_REPLACE = tempGCN.LIST_REPLACE;
                        tempMail.URL_PAGE_ADD = tempGCN.URL_PAGE_ADD;
                        tempMail.HEADER = Base64Encode(strHeader);
                        tempMail.FOOTER = Base64Encode(tempGCN.FOOTER);
                        tempMail.IS_BACK_DATE = tempGCN.IS_BACK_DATE;
                        tempMail.DATA_SRC = lsDetailsAll["EFFECTIVE_NUM"].ToString();
                        filePdf.FILE_BASE64 = Base64Encode(strBodyHtml);
                        filePdf.FILE_ID = strGuiID_File;
                        filePdf.FILE_REF = lsDetailsAll["CERTIFICATE_NO"].ToString();
                        filePdf.Success = true;
                        filePdf.dtSignBack = DateTime.Now;
                        filePdf.USER_NAME = "tool_sign";
                        filePdf.FILE_URL = strGuiID_File;
                        return filePdf;

                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi ký gửi mail Exception service-signAndSendMail 336", jInput.ToString() + " ---" + ex.ToString());
                return null;
            }
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public TempEmailModel getEmailContent(InsurContract iContract, InsurDetail lsDetailsAll, TempConfigModel template, string strUrl, string strGuiID_File) ///NO , 
        {
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse baseResponse = new BaseResponse();

            JObject jInput = JObject.FromObject(iContract);
            JObject jarrInput = JObject.FromObject(lsDetailsAll);
            jInput.Add("CONTRACT", jarrInput);
            jInput.Add("USER", "tool_sign");
            jInput.Add("CHANNEL_CALL", "WEB");
            jInput.Add("TYPE_CALL", "NO");
            try
            {
                TempEmailModel tempMail = new TempEmailModel();
                List<InsurDetailExt> lsInsDetailExt = new List<InsurDetailExt>();
                InsurContractExt lsInsContractExt = new InsurContractExt();
                JObject jValueContract = new JObject();// = JObject.FromObject(iContract);                    
                List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL" && m.TYPE_SEND =="ONE").ToList();
                TempEmailModel tempGCN = new TempEmailModel();
                if (lstempGCN == null && lstempGCN.Count == 0)
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "không lấy được mẫu mail cho tool " + iContract.PRODUCT_CODE;
                    goBussinessEmail.Instance.sendEmaiCMS("", "không lấy được mẫu mail cho tool", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                    return null;
                }
                List<JObject> lsDataExt = new List<JObject>();
                Dictionary<string, JObject> lsGenGCN = new Dictionary<string, JObject>();
                tempGCN = lstempGCN[0];
                List<JObject> lsDetail = new List<JObject>();
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    baseResponse.Success = false;
                    baseResponse.ErrorMessage = "tool ko ky loai all " + iContract.PRODUCT_CODE;
                    goBussinessEmail.Instance.sendEmaiCMS("", "tool ko ky loai all", iContract.ORG_SELLER + "_" + iContract.PRODUCT_CODE);
                    return null;
                }
                else
                {
                    lsInsDetailExt = getDetailExt(iContract, new List<InsurDetail> { lsDetailsAll }, ref lsInsContractExt);
                    foreach (InsurDetailExt itExt in lsInsDetailExt)
                    {
                        itExt.URL_FILE = strUrl + strGuiID_File;
                        JObject obj = JObject.FromObject(itExt);
                        obj.Add("DETAIL", null);
                        lsGenGCN.Add(itExt.CERTIFICATE_NO, obj);
                    }
                }
                SignFileReturnModel filePdf = new SignFileReturnModel();
                if (tempGCN.TYPE_SEND.Equals("ALL"))
                {
                    return null;
                }
                else
                {
                    foreach (InsurDetailExt itemExt in lsInsDetailExt)
                    {                        
                        string strBody = goBusinessTemplate.Instance.replaceHtmlValueJson(lsGenGCN[itemExt.CERTIFICATE_NO], tempGCN);
                        strBody = goBussinessPDF.Instance.clearParram(strBody);
                        tempMail.DATA = Base64Encode(strBody);
                        tempMail.FOOTER = template.MAIL_BCC;
                        tempMail.HEADER = itemExt.EMAIL;
                        tempMail.LIST_REPLACE = tempGCN.LIST_REPLACE;
                        tempMail.TITLE = template.DESCRIPTION;
                        return tempMail;
                    }
                }
            }
            catch (Exception ex)
            {
                baseResponse.Success = false;
                baseResponse.ErrorMessage = ex.ToString();
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi tạo body mail cho tool", jInput.ToString() + " ---" + ex.ToString());
                return null;
            }
            return null;
        }
        public BaseResponse getTokenVerify(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                JObject jSonOutPut = new JObject();
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {                    
                    JObject job = JObject.Parse(request.Data.ToString());
                    string strTypeCode = job["TYPE_CODE"].ToString(); //mã nghiệp vụ lấy xác thực
                    string strParam = job["PARAM_VALUE"].ToString(); //giá trị tìm kiếm
                    string strTYPE_SEND = job["TYPE_SEND"].ToString(); //gửi qua email hay zalo hay ...
                    string strLeng = job["LENG"].ToString();// độ dài mã xác thực
                    //lấy thông tin về email, phone, appid
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);                    
                    var param = new object[] { strTypeCode, strParam, strTYPE_SEND, strLeng };
                    response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);
                    if (response != null)
                    {
                        BaseRequest requestSendMail = new BaseRequest();
                        requestSendMail.Action = new ActionInfo();
                        requestSendMail.Action.ActionCode = "HDI_EMAIL_01";
                        requestSendMail.Action.ParentCode = "HDI_PRIVATE";
                        requestSendMail.Action.UserName = "HDI_PRIVATE";
                        List<JObject> lsOToken = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        List<JObject> lsOData = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                        string strToken = lsOToken[0]["TOKEN"]?.ToString();
                        string strEmailTo = lsOToken[0]["ADDRESS_SEND"]?.ToString();
                        if (string.IsNullOrEmpty(strToken) || string.IsNullOrEmpty(strEmailTo))
                        {
                            jSonOutPut.Add("status", "ERROR");
                            jSonOutPut.Add("Message", lsOToken[0]["MESSAGE"]?.ToString());
                            return goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "ERROR", lsOToken[0]["MESSAGE"]?.ToString());
                        }
                        TempConfigModel template = get_all_template("HDI", "HDI", "TOKEN", "null",
                            "GCN", "HDI", "WEB", DateTime.Now.ToString("yyyyMMddHHmmss"), strEvm);
                        if (template != null && template.TEMP_EMAIL != null)
                        {  
                            List<TempEmailModel> lstempGCN = template.TEMP_EMAIL.Where(m => m.TEMP_TYPE == "EMAIL").ToList();
                            if(lstempGCN !=null && lstempGCN.Count > 0 && strTYPE_SEND.Equals("EMAIL"))
                            {
                                List<string> lsParam = lstempGCN[0].PARAM_REPLACE.Split(';').ToList();
                                string strBody = lstempGCN[0].DATA;
                                strBody = strBody.Replace("@TOKEN@", strToken);
                                foreach(string strPa in lsParam)
                                {
                                    string str = lsOData[0][strPa]?.ToString();
                                    strBody = strBody.Replace("@" + strPa + "@", string.IsNullOrEmpty(str)? " ": str);
                                }
                                ServiceEmailModel emailContract = new ServiceEmailModel();
                                emailContract.content = strBody;
                                emailContract.subject = template.DESCRIPTION;
                                goBussinessEmail cls = new goBussinessEmail();
                                emailContract.to = new List<EmailModel> { new EmailModel { email = strEmailTo } };
                                requestSendMail.Data = JsonConvert.SerializeObject(emailContract);
                                cls.GetResponseEmail(requestSendMail);
                                logEmailModel logSend = new logEmailModel();
                                logSend.BCC = "";
                                logSend.CAME_ID = "";
                                logSend.CAMPAIGN_CODE = "LOG_HDI";
                                logSend.REF_ID = strParam;
                                logSend.CONTENT = strBody;
                                logSend.CREATE_BY = "TOKEN_API";
                                logSend.EMAIL = strEmailTo;
                                logSend.ORG_CODE = "HDI";
                                logSend.PACK_CODE = "null";
                                logSend.PRODUCT_CODE = strTypeCode;
                                logSend.STATUS = "CREATE";
                                logSend.SUBJECT = template.DESCRIPTION;
                                logSend.TYPE_EMAIL = "TOKEN";
                                new Task(() => goBussinessEmail.Instance.createLogSendMailJson(new List<logEmailModel> { logSend }, "HDI", strEvm)).Start();
                                jSonOutPut.Add("status", "DONE");
                                jSonOutPut.Add("Message", "Mã xác thực đã được gửi tới địa chỉ mail " + strEmailTo);
                                return goBusinessCommon.Instance.getResultApi(true, jSonOutPut, config, "", "");
                            }
                            if (template.TEMP_SMS != null && template.TEMP_SMS.Count > 0 && strTYPE_SEND.Equals("SMS"))
                            {
                                string strBody = template.TEMP_SMS[0].DATA;
                                strBody = strBody.Replace("@TOKEN@", strToken);
                                bool b = goBussinessSMS.Instance.sendSMSSystem(template, strEmailTo, strBody, "HDI_PRIVATE", strParam); //strPhone
                                if (b)
                                {
                                    jSonOutPut.Add("status", "DONE");
                                    jSonOutPut.Add("Message", "Mã xác thực đã được gửi tới số điện thoại " + strEmailTo);
                                    return goBusinessCommon.Instance.getResultApi(true, jSonOutPut, config, "","");
                                }
                                else
                                {
                                    jSonOutPut.Add("status", "ERROR");
                                    jSonOutPut.Add("Message", "Có lỗi trong quá trình gửi sms cho khách hàng " + strEmailTo);
                                    return goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "", "Lỗi send sms " + strEmailTo);
                                }
                                
                                
                            }
                        }
                        //không có mẫu gửi mail thực hiện gửi mặc định
                        //ServiceEmailModel emailContract1 = new ServiceEmailModel();
                        //emailContract1.content = "Mã xác thực của Quý khách là: " + strToken;
                        //emailContract1.subject = "[HD Insurance] – Mã xác nhận Bảo hiểm HD";
                        //goBussinessEmail cls1 = new goBussinessEmail();
                        //emailContract1.to = new List<EmailModel> { new EmailModel { email = strEmailTo } };
                        //requestSendMail.Data = JsonConvert.SerializeObject(emailContract1);
                        //cls1.GetResponseEmail(requestSendMail);
                        //logEmailModel logSend1 = new logEmailModel();
                        //logSend1.BCC = "";
                        //logSend1.CAME_ID = "";
                        //logSend1.CAMPAIGN_CODE = "LOG_HDI";
                        //logSend1.REF_ID = strParam;
                        //logSend1.CONTENT = "Mã xác thực của Quý khách là: " + strToken;
                        //logSend1.CREATE_BY = "TOKEN_API";
                        //logSend1.EMAIL = strEmailTo;
                        //logSend1.ORG_CODE = "HDI";
                        //logSend1.PACK_CODE = "null";
                        //logSend1.PRODUCT_CODE = strTypeCode;
                        //logSend1.STATUS = "CREATE";
                        //logSend1.SUBJECT = "[HD Insurance] – Mã xác nhận Bảo hiểm HD";
                        //logSend1.TYPE_EMAIL = "TOKEN";
                        //new Task(() => goBussinessEmail.Instance.createLogSendMailJson(new List<logEmailModel> { logSend1 }, "HDI", strEvm)).Start();
                        jSonOutPut.Add("status", "ERROR");
                        jSonOutPut.Add("Message", "Chưa khai báo mẫu xác thực " + strTypeCode);
                        return goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "", "Chưa khai báo mẫu xác thực " + strTypeCode);
                    }
                    else
                    {
                        jSonOutPut.Add("status", "ERROR");
                        jSonOutPut.Add("Message", "Không lấy được thông tin cần xác thực " + strTypeCode);
                        response = goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "ERROR", "Không lấy được thông tin cần xác thực");
                    }
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json get token" + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi json khong dung cau truc", JsonConvert.SerializeObject(request).ToString());
                    jSonOutPut.Add("status", "ERROR");
                    jSonOutPut.Add("Message", "Dữ liệu không đúng đặc tả");
                    response = goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, nameof(goConstantsError.Instance.ERROR_3002), "Dữ liệu không đúng đặc tả");
                }
                //response = goBusinessCommon.Instance.getResultApi(true, "", config, "DONE", "Mã xác thực đã được gửi tới địa chỉ mail akdaoe@gmail.com");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail json token ", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), "Lỗi dữ liệu không đúng đặc tả " + ex.Message);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        public BaseResponse verifyToken(BaseRequest request)
        {
            string nameMethod = MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                JObject jSonOutPut = new JObject();
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject job = JObject.Parse(request.Data.ToString());
                    string strTypeCode = job["TYPE_CODE"].ToString(); //mã nghiệp vụ cần xác thực
                    string strParam = job["PARAM_VALUE"].ToString(); //giá trị tìm kiếm
                    string strTYPE_SEND = job["TYPE_SEND"].ToString(); //gửi qua email hay zalo hay ...
                    string strValue = job["TOKEN"].ToString();// giá trị token khách hàng nhập
                    //lấy thông tin về email, phone, appid
                    var param = new object[] { strTypeCode, strParam, strTYPE_SEND, strValue };
                    string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                    response = goBusinessCommon.Instance.exePackagePrivate(request.Action.ActionCode, strEvm, param);
                    if (response != null)
                    {
                        List<JObject> lsOToken = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        string strStatus = lsOToken[0]["STATUS"]?.ToString();
                        if(!string.IsNullOrEmpty(strStatus) && strStatus.Equals("DONE"))
                        {
                            jSonOutPut.Add("status", "DONE");
                            jSonOutPut.Add("Message", lsOToken[0]["MESSAGE"]?.ToString());
                            return  goBusinessCommon.Instance.getResultApi(true, jSonOutPut, config, "", "");
                        }else
                        {
                            jSonOutPut.Add("status", "ERROR");
                            jSonOutPut.Add("Message", lsOToken[0]["MESSAGE"]?.ToString());
                            return goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "ERROR", lsOToken[0]["MESSAGE"]?.ToString());
                        }    
                    }
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cau truc json " + JsonConvert.SerializeObject(request).ToString(), Logger.ConcatName(nameClass, nameMethod));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi json khong dung cau truc", JsonConvert.SerializeObject(request).ToString());
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3002), "Dữ liệu không đúng đặc tả");
                }
                jSonOutPut.Add("status", "ERROR");
                jSonOutPut.Add("Message", "Xác nhận không thành công!");
                response = goBusinessCommon.Instance.getResultApi(false, jSonOutPut, config, "ERROR", "Xác nhận không thành công!");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, ex.ToString());
                //goBussinessEmail.Instance.sendEmaiCMS("", "Loi gửi mail json ", JsonConvert.SerializeObject(request).ToString());
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_3003), "Lỗi dữ liệu không đúng đặc tả " + ex.Message);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
    }
}
