﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.CONSTANTS;
using GO.COMMON.Cache;
using GO.HELPER.Utility;
using GO.HELPER.Config;

namespace GO.BUSINESS.Common
{
    public class goBusinessCache
    {
        #region Variables
        public string typeCache = goConfig.Instance.GetAppConfig(goConstantsRedis.KeyTypeCache);
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessCache> _instance = new Lazy<goBusinessCache>(() =>
        {
            return new goBusinessCache();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessCache Instance { get => _instance.Value; }
        #endregion
        #region Method

        public void SetConfigCache()
        {
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        SetConfigRedis();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetConfigCache(string Connection, int IndexDb = 0, string DbAsync = "")
        {
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        SetConfigRedis(Connection, IndexDb, DbAsync);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetConfigRedis()
        {
            goRedisCache.Connect = goConfig.Instance.GetAppConfig(goConstantsRedis.KeyConnectionRedis);
            goRedisCache.IndexDb = Convert.ToInt32(goConfig.Instance.GetAppConfig(goConstantsRedis.KeyDBRedisIndex));
            goRedisCache.DbAsync = goConfig.Instance.GetAppConfig(goConstantsRedis.KeyDBRedisAsync);
        }

        public void SetConfigRedis(string Connection, int IndexDb = 0, string DbAsync = "")
        {
            goRedisCache.Connect = Connection;
            goRedisCache.IndexDb = IndexDb == 0 ? Convert.ToInt32(goConfig.Instance.GetAppConfig(goConstantsRedis.KeyDBRedisIndex)) : IndexDb;
            goRedisCache.DbAsync = DbAsync == null ? goConfig.Instance.GetAppConfig(goConstantsRedis.KeyDBRedisAsync) : DbAsync;
        }

        public bool Add<T>(string key, T value)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        goRedisCache.Instance.Add(key, value);
                        break;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add<T>(string key, T value, int expiredTime, bool isNew)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        goRedisCache.Instance.Add(key, value, expiredTime, isNew);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add<T>(string key, T value, int expiredTime)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        goRedisCache.Instance.Add(key, value, expiredTime);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Add<T>(string key, T value, bool isNew)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        goRedisCache.Instance.Add(key, value, isNew);
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Remove(string key)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        chk = goRedisCache.Instance.Remove(key);
                        break;
                }
                return chk;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemoveAll()
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        chk = goRedisCache.Instance.RemoveAll();
                        break;
                }
                return chk;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RemoveAllPrefix(string prefix)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        chk = goRedisCache.Instance.RemoveAllPrefix(prefix);
                        break;
                }
                return chk;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public T Get<T>(string key)
        {
            try
            {
                T data = default(T);
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        data = goRedisCache.Instance.Get<T>(key);
                        break;
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Exists(string key)
        {
            bool chk = false;
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        chk = goRedisCache.Instance.Exists(key);
                        break;
                }
                return chk;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TimeSpan? TimeToLive(string key)
        {
            TimeSpan? timeSpan = new TimeSpan();
            try
            {
                switch (typeCache)
                {
                    case nameof(goConstants.TypeCache.REDIS):
                        timeSpan = goRedisCache.Instance.TimeToLive(key);
                        break;
                }
                return timeSpan;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
