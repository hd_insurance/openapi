﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.CONSTANTS;
using GO.COMMON.Log;
using GO.HELPER.Config;
using GO.DAO.Common;
using GO.DTO.Base;
using Newtonsoft.Json;
using GO.ENCRYPTION;
using GO.DTO.Common;
using GO.DTO.SystemModels;
using GO.DTO.Service;
using GO.DTO.Partner;
using GO.BUSINESS.Partner;
using GO.COMMON.Cache;
using GO.BUSINESS.Action;
using GO.BUSINESS.ModelBusiness;
using GO.HELPER.Utility;
using GO.SERVICE.CallApi;
using System.Net.Http;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using GO.BUSINESS.Service;
using GO.BUSINESS.CacheSystem;
using System.Web;
using System.Web.Caching;
using GO.DTO.SingleSignOn;
using GO.DTO.Insurance;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using GO.BUSINESS.ServiceAccess;
using Renci.SshNet.Messages;
using System.Net.Http.Headers;
using System.Net;

namespace GO.BUSINESS.Common
{
    public class goBusinessCommon
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessCommon> _instance = new Lazy<goBusinessCommon>(() =>
        {
            return new goBusinessCommon();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessCommon Instance { get => _instance.Value; }
        #endregion

        #region Method
        #region Set Config Database execute
        public void SetConfigDataBase(ref string connection, ref goVariables.DATABASE_TYPE ModeDB)
        {
            try
            {
                connection = goConfig.Instance.GetConnectionString(goConfig.Instance.GetAppConfig(goConstants.KeyConnectionDB));
                string typeDB = goConfig.Instance.GetAppConfig(goConstants.KeyModeDB);
                if (typeDB.Equals(goConstants.KeySql))
                    ModeDB = goVariables.DATABASE_TYPE.SQLSERVER;
                if (typeDB.Equals(goConstants.KeyOracle))
                    ModeDB = goVariables.DATABASE_TYPE.ORACLE;
                if (typeDB.Equals(goConstants.KeyMySql))
                    ModeDB = goVariables.DATABASE_TYPE.MYSQL;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "SetConfigDataBase: " + ex.Message, "goDataSystem - SetConfigDataBase");
                throw ex;
            }
        }
        #endregion

        public BaseResponse getResultApi(bool isSuccess, object data, bool isSignature, string secret, string clientId, string errorCode = "", string errorMessage = "")//, string cerPath = ""
        {
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.Success = isSuccess;
            baseResponse.Error = errorCode;
            baseResponse.ErrorMessage = errorMessage;
            baseResponse.Data = data;
            baseResponse.Signature = null;

            if (isSignature)
            {
                string encryptData = data == null ? "" : goEncryptBase.Instance.Md5Encode(JsonConvert.SerializeObject(data));
                var raw = "Success=" + isSuccess + "Error=" + errorCode + "ErrorMessage=" + errorMessage + "Data=" + encryptData + secret;
                baseResponse.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, clientId);
            }
            return baseResponse;
        }
        public BaseResponseV2 getResultApiV2(string LogId, bool isSuccess, object data, bool isSignature, string secret, string clientId, string errorCode = "", string errorMessage = "")//, string cerPath = ""
        {
            BaseResponseV2 baseResponse = new BaseResponseV2();
            baseResponse.Success = isSuccess;
            baseResponse.Error = errorCode;
            baseResponse.ErrorMessage = errorMessage;
            baseResponse.Data = data;
            baseResponse.LogId = LogId;
            baseResponse.Signature = null;

            if (isSignature)
            {
                string encryptData = data == null ? "" : goEncryptBase.Instance.Md5Encode(JsonConvert.SerializeObject(data));
                var raw = "Success=" + isSuccess + "Error=" + errorCode + "ErrorMessage=" + errorMessage + "Data=" + encryptData + secret;
                baseResponse.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, clientId);
            }
            return baseResponse;
        }

        public BaseResponseV2 ConvertResponse(BaseResponse response, string LogId)
        {
            BaseResponseV2 baseResponse = new BaseResponseV2();
            baseResponse.Success = response.Success;
            baseResponse.Error = response.Error;
            baseResponse.ErrorMessage = response.ErrorMessage;
            baseResponse.Data = response.Data;
            baseResponse.LogId = LogId;
            baseResponse.Signature = response.Signature;
            return baseResponse;
        }

        public BaseResponseV2 ConvertResponse_v1(BaseResponse response, string LogId)
        {
            BaseResponseV2 baseResponse = new BaseResponseV2();
            baseResponse.Success = response.Success;
            baseResponse.Error = response.Error;
            baseResponse.ErrorMessage = response.ErrorMessage;
            string parsedString = Regex.Unescape(JsonConvert.SerializeObject(response.Data).Replace("\\", ""));
            parsedString = Regex.Unescape(parsedString.Replace("\"[", "["));
            parsedString = Regex.Unescape(parsedString.Replace("]\"", "]"));
            //JArray jArrDetail = JArray.Parse(parsedString);
            baseResponse.Data = JsonConvert.DeserializeObject(parsedString);
            baseResponse.LogId = LogId;
            baseResponse.Signature = response.Signature;
            return baseResponse;
        }

        public BaseResponseV2 getResultApiV2(string LogId, bool isSuccess, object data, ResponseConfig config, string errorCode = "", string errorMessage = "")
        {
            return getResultApiV2(LogId, isSuccess, data, config.isSign, config.secret, config.client_id, errorCode, errorMessage);
        }

        public BaseResponse getResultApi(bool isSuccess, object data, ResponseConfig config, string errorCode = "", string errorMessage = "")
        {
            return getResultApi(isSuccess, data, config.isSign, config.secret, config.client_id, errorCode, errorMessage);
        }
        public ResponseLogin getResultLogin(bool isSuccess, string token, int expries, string refeshToken, bool isSignature, string secret, string clientId, string errorCode = "", string errorMessage = "") // , string cerPath = ""
        {
            ResponseLogin response = new ResponseLogin();
            response.Success = isSuccess;
            response.Token = token;
            response.Expries = expries;
            response.RefeshToken = refeshToken;
            response.Error = errorCode;
            response.ErrorMessage = errorMessage;
            if (isSignature)
            {
                var raw = "Success=" + isSuccess + "Error=" + errorCode + "ErrorMessage=" + errorMessage + "Token=" + token + "Expries=" + expries + secret;
                response.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, clientId);
            }
            return response;
        }

        public ResponseLoginSSO getResultSSO(UserSSO UserInfo, bool isSuccess, string token, int expries, string refeshToken, bool isSignature, string secret, string clientId, string errorCode = "", string errorMessage = "")
        {
            ResponseLoginSSO response = new ResponseLoginSSO();
            response.Success = isSuccess;
            response.Token = token;
            response.Expries = expries;
            response.RefeshToken = refeshToken;
            response.Error = errorCode;
            response.ErrorMessage = errorMessage;
            response.UserInfo = UserInfo;
            if (isSignature)
            {
                var raw = "Success=" + isSuccess + "Error=" + errorCode + "ErrorMessage=" + errorMessage + "Token=" + token + "Expries=" + expries + secret;
                response.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, clientId);
            }
            return response;
        }
        public async Task<bool> AuditLogsApi(AuditLogsInfoV2 auditLogs)
        {
            try
            {
                string connection = goConfig.Instance.GetConnectionString(goConstants.KeyConnectMongo);
                string database = goBusinessCommon.Instance.GetConfigDB(goConstants.DatabaseMongo);
                string table = goBusinessCommon.Instance.GetConfigDB(goConstants.TableLogAudit);
                
                return goDataAccess.Instance.MongoDbAdd<AuditLogsInfoV2>(connection, database, table, auditLogs);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Logs Mongo: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                throw ex;
            }
        }

        public void GetModelAutdit(ref AuditLogsInfoV2 auditLogs, string parentCode, string actionCode, object request, object response)
        {
            try
            {
                if (auditLogs == null)
                    auditLogs = new AuditLogsInfoV2();
                auditLogs.ParentCode = parentCode;
                auditLogs.ActionCode = actionCode;
                auditLogs.request = JsonConvert.SerializeObject(request);
                auditLogs.response = JsonConvert.SerializeObject(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<AuditLogsInfoV2>> GetAuditLogs(string ParnertCode, string LogId, string DayRequest)
        {
            try
            {
                string connection = goConfig.Instance.GetConnectionString(goConstants.KeyConnectMongo);
                string database = goBusinessCommon.Instance.GetConfigDB(goConstants.DatabaseMongo);
                string table = goBusinessCommon.Instance.GetConfigDB(goConstants.TableLogAudit);

                DateTime date = goUtility.Instance.ConvertToDateTime(DayRequest, DateTime.MinValue);
                
                string year, month, day;
                year = date.Year.ToString();
                month = date.Month.ToString();
                day = date.ToString("yyyyMMdd");
                var ob = new
                {
                    LogId = LogId,
                    ParentCode = ParnertCode,
                    DayInfo = new
                    {
                        Year = year,
                        Month = month,
                        Day = day
                    },
                };

                BsonDocument objFind = ob.ToBsonDocument();

                List<AuditLogsInfoV2> lstAudit = new List<AuditLogsInfoV2>();
                var lstData = goDataAccess.Instance.MongoDbGet(connection, database, table, objFind); 
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "data Mongo: " + JsonConvert.SerializeObject(ob), System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

                if (lstData != null && lstData.Any())
                {
                    foreach(var item in lstData)
                    {
                        try
                        {
                            AuditLogsInfoV2 info = BsonSerializer.Deserialize<AuditLogsInfoV2>(item);
                            lstAudit.Add(info);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                return lstAudit;
            }
            catch (Exception ex)
            {

                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Logs Mongo: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                throw ex;
            }
        }
        public async Task<bool> LogsService(LogServiceModel logServiceModel)
        {
            try
            {
                string connection = goConfig.Instance.GetConnectionString(goConstants.KeyConnectMongo);
                string database = goBusinessCommon.Instance.GetConfigDB(goConstants.DatabaseMongo);
                string table = goBusinessCommon.Instance.GetConfigDB(goConstants.TableLogService);
                return goDataAccess.Instance.MongoDbAdd<LogServiceModel>(connection, database, table, logServiceModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<bool> addFileSign(TempFileSign logServiceModel)
        {
            try
            {
                string connection = goConfig.Instance.GetConnectionString(goConstants.KeyConnectMongo);
                string database = goBusinessCommon.Instance.GetConfigDB(goConstants.DatabaseMongo);
                string table = "FileSign";
                return goDataAccess.Instance.MongoDbAdd<TempFileSign>(connection, database, table, logServiceModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get info Redis Cache

        public PartnerConfigModel GetConfigPartner(BaseRequest request)
        {
            PartnerConfigModel partnerConfig = null;// new PartnerConfigModel();
            try
            {
                if (request != null && request.Action != null && request.Action.ParentCode != null && request.Action.Secret != null)
                {
                    partnerConfig = goBusinessPartner.Instance.GetPartnerConfig(request.Action.ParentCode, request.Action.Secret);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                partnerConfig = null;
            }
            return partnerConfig;
        }
        public ActionApi GetActionApi(string ActionCode)
        {
            goInstanceCaching.Instance.InstanceCaching();
            ActionApi infoAction = new ActionApi();
            List<ActionApi> lstAction = new List<ActionApi>();
            lstAction = goBusinessCache.Instance.Get<List<ActionApi>>(goConstantsRedis.ActionApis_Key);
            if (lstAction != null && lstAction.Any())
                infoAction = lstAction.Where(i => i.Api_Code == ActionCode).FirstOrDefault();
            return infoAction;
        }

        public ActionApiDatabase GetApiDatabase(string action, string environment, bool isServerCache)
        {
            ActionApiDatabase info = new ActionApiDatabase();
            try
            {
                List<ActionApiDatabase> lstData = goBusinessCache.Instance.Get<List<ActionApiDatabase>>(goConstantsRedis.ActionApiDatabase_Key);
                if (lstData != null && lstData.Any())
                {
                    info = lstData.Where(i => i.Api_Code == action && i.Enviroment_Code == environment && ((i.Db_Mode == goConstants.ModeServerCache && isServerCache) || (!isServerCache && i.Db_Mode != goConstants.ModeServerCache))).ToList()?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.FATAL, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                info = null;
            }
            return info;
        }
        #endregion

        #region Get data
        public List<T> GetData<T>(BaseResponse response, int index)
        {
            try
            {
                List<List<object>> lstData = new List<List<object>>();
                List<T> data = new List<T>();
                lstData = JsonConvert.DeserializeObject<List<List<object>>>(JsonConvert.SerializeObject(response.Data));
                if (lstData != null && lstData.Any())
                {
                    for (var i = 0; i < lstData.Count; i++)
                    {
                        if (index == i)
                        {
                            if (lstData[i] != null && lstData[i].Any())
                            {
                                data = JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(lstData[i]));
                            }
                            break;
                        }
                    }
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get info execute


        public BaseResponse exePackagePrivate(string strPKG_Name, string environment, object[] Param)
        {
            ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(strPKG_Name, environment, Param);
            return goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), false, null, null, null, null);
        }
        public ExecuteInfo GetInfoExecute(string action, string environment, object[] goParam)
        {
            ExecuteInfo info = new ExecuteInfo();
            try
            {
                string connectString = "";
                goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;

                ActionApi apiInfo = goBusinessCommon.Instance.GetActionApi(action);
                info.param = goParam;
                ActionApiDatabase apiDatabase = goBusinessAction.Instance.GetApiDatabase(action, environment, false);
                if (apiInfo != null)
                    info.store = apiInfo.Pro_Code;
                if (apiDatabase != null && apiDatabase.Data != null)
                {
                    ModeDB = goVariables.Instance.GetDATABASE_TYPE(apiDatabase.Db_Mode);
                    info.ModeDB = ModeDB;
                    info.connectString = apiDatabase.Data;
                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && !string.IsNullOrEmpty(apiDatabase.Packages))
                    {
                        if (info.store.IndexOf(".") > 0)
                        {
                            info.store = apiDatabase.Schema + "." + info.store;
                        }
                        else
                        {
                            info.store = apiDatabase.Schema + "." + apiDatabase.Packages + "." + info.store;
                        }
                    }
                    if (!string.IsNullOrEmpty(apiDatabase.Schema) && string.IsNullOrEmpty(apiDatabase.Packages))
                        info.store = apiDatabase.Schema + "." + info.store;
                    if (string.IsNullOrEmpty(apiDatabase.Schema) && !string.IsNullOrEmpty(apiDatabase.Packages))
                        info.store = apiDatabase.Packages + "." + info.store;
                }
                if (string.IsNullOrEmpty(info.connectString))
                {
                    goBusinessCommon.Instance.SetConfigDataBase(ref connectString, ref ModeDB);
                    info.connectString = connectString;
                    info.ModeDB = ModeDB;
                }
                return info;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Get config partner response
        public ResponseConfig GetResponseConfig(BaseRequest request)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                ResponseConfig config = new ResponseConfig();
                PartnerConfigModel partnerConfig = new PartnerConfigModel();
                partnerConfig = goBusinessCommon.Instance.GetConfigPartner(request);
                if (partnerConfig == null)
                {
                    return null;
                }

                config.secret = partnerConfig.Secret;
                config.client_id = partnerConfig.Client_ID;
                config.isSign = HELPER.Utility.goUtility.Instance.CastToBoolean(partnerConfig.isSignature_Response);
                config.env_code = partnerConfig.Enviroment_Code;
                config.partnerCode = partnerConfig.Partner_Code;
                return config;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    BaseResponseV2 resLog = new BaseResponseV2();
                    resLog.Data = "GetResponseConfig error: " + ex.Message;
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex1)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
                return null;
            }

        }

        public ResponseConfig GetResponseConfig(string partnerCode, string secret)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                ResponseConfig config = new ResponseConfig();
                PartnerConfigModel partnerConfig = new PartnerConfigModel();
                partnerConfig = goBusinessPartner.Instance.GetPartnerConfig(partnerCode, secret);
                if (partnerConfig == null)
                {
                    return null;
                }

                config.secret = partnerConfig.Secret;
                config.client_id = partnerConfig.Client_ID;
                config.isSign = HELPER.Utility.goUtility.Instance.CastToBoolean(partnerConfig.isSignature_Response);
                config.env_code = partnerConfig.Enviroment_Code;
                config.partnerCode = partnerConfig.Partner_Code;
                return config;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }

        }
        #endregion

        #region Env
        public void GetEnvironmentConfig(ref string environment)
        {
            environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        }
        #endregion

        #region Get signature Baserequest / response
        public void GetSignRequest(ref BaseRequest request, PartnerConfigModel partnerConfig)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string raw = "";
                string deviceCode = request.Device.DeviceCode;
                string ipPrivate = request.Device.IpPrivate;
                string deviceEnvironment = request.Device.DeviceEnvironment;
                string userName = request.Action.UserName;
                string secret = request.Action.Secret;
                string actionCode = request.Action.ActionCode;
                string parentCode = request.Action.ParentCode;
                var txtData = JsonConvert.SerializeObject(request.Data);
                if (txtData.Length > 5000)
                {
                    txtData = txtData.Substring(0, 5000);
                }
                string data = goEncryptBase.Instance.Md5Encode(txtData);
                string constantsText = "HDI";
                raw = constantsText + deviceCode + ipPrivate + deviceEnvironment + userName + secret + actionCode + parentCode + data + constantsText;
                request.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, partnerConfig.Client_ID);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public void GetSignRequest(ref BaseRequest request, ResponseConfig partnerConfig)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string raw = "";
                string deviceCode = request.Device.DeviceCode;
                string ipPrivate = request.Device.IpPrivate;
                string deviceEnvironment = request.Device.DeviceEnvironment;
                string userName = request.Action.UserName;
                string secret = request.Action.Secret;
                string actionCode = request.Action.ActionCode;
                string parentCode = request.Action.ParentCode;
                var txtData = JsonConvert.SerializeObject(request.Data);
                if (txtData.Length > 5000)
                {
                    txtData = txtData.Substring(0, 5000);
                }
                string data = goEncryptBase.Instance.Md5Encode(txtData);
                string constantsText = "HDI";
                raw = constantsText + deviceCode + ipPrivate + deviceEnvironment + userName + secret + actionCode + parentCode + data + constantsText;
                request.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, partnerConfig.client_id);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public void GetSignRequest(ref BaseRequest request)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                ResponseConfig config = GetResponseConfig(request);
                string raw = "";
               string deviceCode = request.Device.DeviceCode;
                string ipPrivate = request.Device.IpPrivate;
                string deviceEnvironment = request.Device.DeviceEnvironment;
                string userName = request.Action.UserName;
                string secret = request.Action.Secret;
                string actionCode = request.Action.ActionCode;
                string parentCode = request.Action.ParentCode;
                var txtData = JsonConvert.SerializeObject(request.Data);
                if (txtData.Length > 5000)
                {
                    txtData = txtData.Substring(0, 5000);
                }
                string data = goEncryptBase.Instance.Md5Encode(txtData);
                string constantsText = "HDI";
                raw = constantsText + deviceCode + ipPrivate + deviceEnvironment + userName + secret + actionCode + parentCode + data + constantsText;
                request.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, config.client_id);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public BaseResponse GetSignInfo(BaseRequest request)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            BaseResponse response = new BaseResponse();
            try
            {
                ResponseConfig config = GetResponseConfig(request);
                string raw = "";
                string deviceCode = request.Device.DeviceCode;
                string ipPrivate = request.Device.IpPrivate;
                string deviceEnvironment = request.Device.DeviceEnvironment;
                string userName = request.Action.UserName;
                string secret = request.Action.Secret;
                string actionCode = request.Action.ActionCode;
                string parentCode = request.Action.ParentCode;
                var txtData = JsonConvert.SerializeObject(request.Data);
                if (txtData.Length > 5000)
                {
                    txtData = txtData.Substring(0, 5000);
                }
                string data = goEncryptBase.Instance.Md5Encode(txtData);
                string constantsText = "HDI";
                raw = constantsText + deviceCode + ipPrivate + deviceEnvironment + userName + secret + actionCode + parentCode + data + constantsText;
                string Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, config.client_id);

                var obj = new
                {
                    txtData = txtData,
                    md5Data = data,
                    raw = raw,
                    signature = Signature
                };
                response.Data = obj;
                response.Success = true;
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }


        public void SignResponse(ref BaseResponse response, ResponseConfig partnerConfig)
        {
            if (partnerConfig.isSign)
            {
                string encryptData = response.Data == null ? "" : goEncryptBase.Instance.Md5Encode(JsonConvert.SerializeObject(response.Data));
                var raw = "Success=" + response.Success + "Error=" + response.Error + "ErrorMessage=" + response.ErrorMessage + "Data=" + encryptData + partnerConfig.secret;
                response.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, partnerConfig.client_id);
            }
        }

        public BaseResponse SignResponseOut(BaseResponse response, ResponseConfig partnerConfig)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            BaseResponse resOut = response;
            try
            {
                if (partnerConfig.isSign)
                {
                    string encryptData = response.Data == null ? "" : goEncryptBase.Instance.Md5Encode(JsonConvert.SerializeObject(response.Data));
                    var raw = "Success=" + response.Success + "Error=" + response.Error + "ErrorMessage=" + response.ErrorMessage + "Data=" + encryptData + partnerConfig.secret;
                    resOut.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, partnerConfig.client_id);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
            return resOut;
        }
        #endregion

        #region upload, get file server 
        public bool uploadFileServer(byte[] byteFile, string strFileName, string strPath, BaseRequest re, ref string strIDFile, ref string strUrlFile, string strID)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                //request.Action.ParentCode = "HDI_UPLOAD";
                //request.Action.Secret = "HDI_UPLOAD_198282911FASE1239212";
                //request.Action.ActionCode = "UPLOAD_SIGN";
                List<goServiceConstants.Byte_File> lstF = new List<goServiceConstants.Byte_File>();
                lstF.Add(new goServiceConstants.Byte_File() { byteFile = byteFile, name = strFileName });
                string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "upload/";
                if (!string.IsNullOrEmpty(strID) && strID.Length > 30)
                    urrl = urrl + "?id=" + strID;
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, urrl, Logger.ConcatName(nameClass, nameMethod));
                goServiceConstants.KeyVal key1 = new goServiceConstants.KeyVal();
                key1.key = "ParentCode";
                key1.val = "HDI_UPLOAD"; //re.Action.ParentCode;
                goServiceConstants.KeyVal key2 = new goServiceConstants.KeyVal();
                key2.key = "Secret";
                key2.val = "HDI_UPLOAD_198282911FASE1239212"; //re.Action.Secret;
                goServiceConstants.KeyVal key3 = new goServiceConstants.KeyVal();
                key3.key = "environment";
                key3.val = "LIVE";
                goServiceConstants.KeyVal key4 = new goServiceConstants.KeyVal();
                key4.key = "DeviceEnvironment";
                key4.val = "WEB";
                goServiceConstants.KeyVal key5 = new goServiceConstants.KeyVal();
                key5.key = "ActionCode";
                key5.val = "UPLOAD_SIGN"; //re.Action.ActionCode;
                goServiceConstants.KeyVal key6 = new goServiceConstants.KeyVal();
                key6.key = "UserName";
                key6.val = "HDI_UPLOAD";
                List<goServiceConstants.KeyVal> lsKey = new List<goServiceConstants.KeyVal>() { key1, key2, key3, key4, key5, key6 };
                HttpResponseMessage rp = new goServiceInvoke().Invoke(urrl, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.form_data, goServiceConstants.TypeRaw.none, lsKey
                    , string.Empty, "", lstF).Result;
                string message = rp.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                var details = JObject.Parse(parsedString);
                if (!string.IsNullOrEmpty(details["success"]?.ToString()))
                {
                    JArray arrRes = JArray.Parse(details["data"].ToString());
                    if (arrRes.Count > 0 && arrRes[0]["file_key"]?.ToString().Length > 20)
                    {
                        strIDFile = arrRes[0]["file_key"].ToString();
                        if (!string.IsNullOrEmpty(strID) && strID.Length > 30)
                            strUrlFile = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + strID;
                        else
                            strUrlFile = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE) + "f/" + strIDFile;
                    }
                    else
                    {
                        strIDFile = details["error_message"].ToString();
                        //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi đẩy file lên server ", strIDFile);
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi đẩy file lên server ", Logger.ConcatName(nameClass, nameMethod));

                        return false;
                    }
                }
                else
                {
                    strUrlFile = details["data"].ToString();
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi đẩy file lên server " + strIDFile, strIDFile);
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi đẩy file lên server " + strIDFile, Logger.ConcatName(nameClass, nameMethod));
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi đẩy file lên server " + "Ex: " + strUrlFile + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi đẩy file lên server ", "Ex: " + strUrlFile + ex.ToString());                
                return false;
            }
        }
        public List<string> getFileIDbefore(int count, string file_ext, string original_name, string subpath, string tags, ref string strPathServer)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                JObject jInput = new JObject();
                jInput.Add("count", count);
                jInput.Add("file_ext", file_ext);
                jInput.Add("original_name", original_name);
                jInput.Add("subpath", subpath);
                jInput.Add("tags", tags);
                string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "upload/initialization";
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, urrl, Logger.ConcatName(nameClass, nameMethod));
                goServiceConstants.KeyVal key1 = new goServiceConstants.KeyVal();
                key1.key = "ParentCode";
                key1.val = "HDI_UPLOAD"; //re.Action.ParentCode;
                goServiceConstants.KeyVal key2 = new goServiceConstants.KeyVal();
                key2.key = "Secret";
                key2.val = "HDI_UPLOAD_198282911FASE1239212"; //re.Action.Secret;
                goServiceConstants.KeyVal key3 = new goServiceConstants.KeyVal();
                key3.key = "environment";
                key3.val = "LIVE";
                goServiceConstants.KeyVal key4 = new goServiceConstants.KeyVal();
                key4.key = "DeviceEnvironment";
                key4.val = "WEB";
                goServiceConstants.KeyVal key5 = new goServiceConstants.KeyVal();
                key5.key = "ActionCode";
                key5.val = "UPLOAD_SIGN_NEW"; //re.Action.ActionCode;
                goServiceConstants.KeyVal key6 = new goServiceConstants.KeyVal();
                key6.key = "UserName";
                key6.val = "HDI_UPLOAD";
                List<goServiceConstants.KeyVal> lsKey = new List<goServiceConstants.KeyVal>() { key1, key2, key3, key4, key5, key6 };
                HttpResponseMessage rp = new goServiceInvoke().Invoke(urrl, goServiceConstants.MethodApi.POST, goServiceConstants.TypeBody.raw, goServiceConstants.TypeRaw.json, lsKey
                    , string.Empty, jInput,null).Result;
                string message = rp.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                var details = JObject.Parse(parsedString);
                if (!string.IsNullOrEmpty(details["success"]?.ToString()))
                {
                    JObject jOutput = JObject.Parse(details["data"].ToString());
                    strPathServer = jOutput["full_path"].ToString();
                    JArray jFile = JArray.Parse(jOutput["files"].ToString());
                    List<string> lsFile = new List<string>();
                    foreach(JToken jk in jFile)
                    {
                        lsFile.Add(jk["file_key"].ToString());
                    }
                    return lsFile;
                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi nhận chỗ file lên server ", parsedString);
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi nhận chỗ file lên server ", "Ex: " + ex.ToString());
                return null;
            }
        }
        public byte[] getFileServer(string strIDFile)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string urrl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_SERVER_FILE_IP) + "f/" + strIDFile;
                HttpResponseMessage rp1 = new goServiceInvoke().Invoke(urrl, goServiceConstants.MethodApi.GET, goServiceConstants.TypeBody.none, goServiceConstants.TypeRaw.none, null, string.Empty, "", null).Result;
                if (rp1.IsSuccessStatusCode)
                {
                    byte[] str1 = rp1.Content.ReadAsByteArrayAsync().Result;
                    return str1;
                }
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy file từ server " + strIDFile, rp1.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "getFileServer" + "Lỗi lấy file từ server " + strIDFile, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "getFileServer" + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                //goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lấy file từ server " + strIDFile, "Ex: " + ex.ToString());
                return null;
            }
        }

        #endregion
        #region Xử lý json return ipn

        public string getValueJson(string strName, List<string> strList)
        {
            //List<string> strList = getDSKey(input);
            foreach (string str in strList)
            {
                if (str.Length >= strName.Length && strName.Equals(str.Substring(0, strName.Length)))
                {
                    if (str.IndexOf(";") == strName.Length)
                    {
                        return str.Substring(strName.Length + 1);
                        //return true;
                    }

                }
            }
            return "";
        }
        public List<string> getDSKey(JObject input)
        {

            // Create JObject from object
            Newtonsoft.Json.Linq.JObject inputJson = Newtonsoft.Json.Linq.JObject.FromObject(input);

            // Read Properties of the JObject
            var properties = inputJson.Properties();

            List<string> fieldNames = new List<string>();
            foreach (var property in properties)
            {
                if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                                 property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                {
                    var subFields = GetFieldNames(property.Value.ToString());

                    if (subFields != null && subFields.Count() > 0)
                    {
                        fieldNames.AddRange(
                            subFields
                            .Select(n =>
                            string.IsNullOrEmpty(n) ? property.Name :
                            string.Format("{0}.{1}", property.Name, n)));
                    }
                }
                else
                {
                    fieldNames.Add(property.Name + ";" + property.Value);
                }
            }
            return fieldNames;
        }
        public static List<string> GetFieldNames(dynamic input)
        {
            List<string> fieldNames = new List<string>();

            try
            {
                // Deserialize the input json string to an object
                input = Newtonsoft.Json.JsonConvert.DeserializeObject(input);
                input = input.Root ?? input.First ?? input;

                if (input != null)
                {
                    // Get to the first element in the array
                    bool isArray = true;
                    while (isArray)
                    {
                        input = input.First ?? input;
                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                        input.GetType() == typeof(Newtonsoft.Json.Linq.JValue) ||
                        input == null)
                            isArray = false;
                    }

                    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject))
                    {
                        // Create JObject from object
                        Newtonsoft.Json.Linq.JObject inputJson =
                            Newtonsoft.Json.Linq.JObject.FromObject(input);

                        var properties = inputJson.Properties();
                        foreach (var property in properties)
                        {
                            if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                            property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                // If yes, enter the recursive loop to extract sub-field names
                                var subFields = GetFieldNames(property.Value.ToString());

                                if (subFields != null && subFields.Count() > 0)
                                {
                                    fieldNames.AddRange(
                                        subFields
                                        .Select(n =>
                                        string.IsNullOrEmpty(n) ? property.Name :
                                     string.Format("{0}.{1}", property.Name, n)));
                                }
                            }
                            else
                            {
                                fieldNames.Add(property.Name + ";" + property.Value);
                            }
                        }
                    }
                    else
                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JValue))
                    {
                        fieldNames.Add(string.Empty);
                    }
                }
            }
            catch
            {
                throw;
            }

            return fieldNames;
        }

        #endregion

        #region Gen request private hdi
        public BaseRequest GetBaseRequestHDI(string env)
        {
            BaseRequest request = new BaseRequest();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                request.Device = new ClientInfo();
                request.Device.DeviceId = "";
                request.Device.DeviceCode = "";
                request.Device.IpPrivate = DateTime.Now.ToString("yyMMddhhmmss");
                request.Device.DeviceEnvironment = "WEB";
                request.Action = new ActionInfo();
                request.Action.ParentCode = goConstants.Org_Code.HDI_PRIVATE.ToString();
                request.Action.UserName = goConstants.Org_Code.HDI_PRIVATE.ToString();

                switch (env)
                {
                    case nameof(goConstants.Env_Code.DEV):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_D;
                        break;
                    case nameof(goConstants.Env_Code.TEST):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_T;
                        break;
                    case nameof(goConstants.Env_Code.UAT):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_U;
                        break;
                    case nameof(goConstants.Env_Code.LIVE):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_L;
                        break;
                }
                return request;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public BaseResponse executeProHDI(string strPKG_Name, string environment, string[] Param)
        {
            BaseRequest reqInternal = GetBaseRequestHDI(environment);
            reqInternal.Action.ActionCode = strPKG_Name;

            reqInternal.Data = goUtility.Instance.convertArrayToJsonParam(Param);
            return goBusinessAction.Instance.GetBaseResponse(reqInternal);
        }

        // gen request HDI UPLOAD
        public RequestNodeJs GetBaseRequestHDIUpload(string env)
        {
            RequestNodeJs request = new RequestNodeJs();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                request.Device = new ClientNodeInfo();
                request.Device.DeviceId = "";
                request.Device.DeviceCode = "";
                request.Device.Device_name = "";
                request.Device.IpPrivate = DateTime.Now.ToString("yyMMddhhmmss");
                request.Device.IpPublic = DateTime.Now.ToString("yyMMddhhmmss");
                request.Device.X = "";
                request.Device.Y = "";
                request.Device.environment = env;
                request.Device.DeviceEnvironment = "WEB";
                request.Action = new ActionInfo();
                request.Action.ParentCode = goConstants.Org_Code.HDI_UPLOAD.ToString();
                request.Action.UserName = goConstants.UserApi.HDI_UPLOAD.ToString();

                switch (env)
                {
                    case nameof(goConstants.Env_Code.DEV):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_UP_D;
                        break;
                    case nameof(goConstants.Env_Code.TEST):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_UP_T;
                        break;
                    case nameof(goConstants.Env_Code.UAT):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_UP_U;
                        break;
                    case nameof(goConstants.Env_Code.LIVE):
                        request.Action.Secret = goConstants.Instance.secret_private_HDI_UP_L;
                        break;
                }
                return request;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        #endregion

        #region Send and Verify OTP
        public BaseResponse SendOtp(SendOtpModel info)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (info == null || string.IsNullOrEmpty(info.CAMPAIGN_CODE) || string.IsNullOrEmpty(info.OTP_TYPE) || string.IsNullOrEmpty(info.PHONE) || string.IsNullOrEmpty(info.USER_CREATE) || string.IsNullOrEmpty(info.ENV_CODE) || info.EXP_SECONDS == null || info.EXP_SECONDS == 0)
                    throw new Exception("obiect invalid");
                string otp = "";
                if (info.ENV_CODE != "LIVE")
                {
                    otp = "123456";
                } 
                else
                {
                    otp = goUtility.Instance.RandomOTP(info.OTP_SIZE);
                }                    
                string content_otp = "Ma OTP xac thuc Bao hiem HD la " + otp;
                int totalSms = 0;
                string messB64 = goBussinessSMS.Instance.checkMessage(content_otp, info.PHONE, ref totalSms);
                info.MESS_SEND = messB64;
                info.TOTAL = totalSms;
                info.OTP = otp;
                info.EFFECTIVE_DATE = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss");
                info.EXPIRATION_DATE = DateTime.Now.AddSeconds(info.EXP_SECONDS).ToString("dd/MM/yyyy hh:mm:ss");

                string t1 = DateTime.Now.ToString("yy/MM/dd hh:mm:ss");
                string t2 = "";
                string t3 = "";
                try
                {
                    BaseRequest request = GetBaseRequestHDI(info.ENV_CODE);
                    request.Action.ActionCode = goConstantsProcedure.INS_SEND_OTP;
                    object param = GetParamsInsSendOtp(info);
                    request.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                    BaseResponse responseIns = goBusinessAction.Instance.GetBaseResponse(request);
                    if (responseIns.Success)
                    {
                        ResSendOtpModel resSendOtp = GetData<ResSendOtpModel>(responseIns, 0).FirstOrDefault();
                        if (!string.IsNullOrEmpty(resSendOtp.CAMSMS_ID))
                        {
                            BaseRequest request2 = request;
                            request2.Action.ActionCode = goConstantsProcedure.SEND_SMS_HDI;
                            object param2 = GetParamSendOtp(info.PHONE, content_otp, resSendOtp.CAMSMS_ID);
                            request2.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param2));

                            string keyRedis = GetKeyRedisOtp(info.PHONE);
                            goBusinessCache.Instance.SetConfigCache();
                            goBusinessCache.Instance.Add<string>(keyRedis, otp, info.EXP_SECONDS, true);
                            t2 = DateTime.Now.ToString("yy/MM/dd hh:mm:ss");
                            response.Success = true;
                            new Task(() => goBussinessSMS.Instance.GetResponseSMS(request2)).Start();
                            // Start 2022-09-07 By ThienTVB
                            if (info.EMAIL != null)
                            {
                                //goBussinessEmail.Instance.sendEmaiCMS(info.EMAIL, "OTP xác nhận", "Số điện thoại: " + info.PHONE + " Mã xác nhận: " + info.OTP);
                                new Task(() => goBussinessEmail.Instance.sendEmaiCMS(info.EMAIL, "OTP xác nhận" , "Số điện thoại: " + info.PHONE + " Mã xác nhận: " + info.OTP)).Start();
                            }
                            // End 2022-09-07 By ThienTVB
                            t3 = DateTime.Now.ToString("yy/MM/dd hh:mm:ss");
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                            throw new Exception("send otp error. SMS ID is null");
                        }
                    }
                    else
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));
                        response = responseIns;
                    }
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "time: " + t1 + "-" + t2 + "-" + t3, Logger.ConcatName(nameClass, nameMethod));
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    response = goBusinessCommon.Instance.getResultApi(false, "", new ResponseConfig(), nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                }
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public object GetParamsInsSendOtp(SendOtpModel info)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    REF_CODE = info.REF_CODE,
                    CAMPAIGN_CODE = info.CAMPAIGN_CODE,
                    PHONE = info.PHONE,
                    OTP_TYPE = info.OTP_TYPE,
                    OTP = info.OTP,
                    MESS_SEND = info.MESS_SEND,
                    TOTAL = info.TOTAL,
                    EFFECTIVE_DATE = info.EFFECTIVE_DATE,
                    EXPIRATION_DATE = info.EXPIRATION_DATE,
                    USER_CREATE = info.USER_CREATE
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public object GetParamSendOtp(string phone, string mess, string sms_id)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    phone = phone,
                    mess = mess,
                    sms_id = sms_id,
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public string GetKeyRedisOtp(string phone)
        {
            return goConstantsRedis.PrefixOtp + phone;
        }

        public int VerifyOtp(string phone, string otp)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string keyRedis = GetKeyRedisOtp(phone);
                goBusinessCache.Instance.SetConfigCache();
                string dataOtp = goBusinessCache.Instance.Get<string>(keyRedis);
                if (!string.IsNullOrEmpty(dataOtp) && !string.IsNullOrEmpty(otp) && dataOtp.Equals(otp))
                {
                    goBusinessCache.Instance.Remove(keyRedis);
                    return 0;//Valid OTP
                }
                else if(!string.IsNullOrEmpty(dataOtp) && !string.IsNullOrEmpty(otp) && !dataOtp.Equals(otp))
                {
                    return 1;//InValid OTP
                }
                else if (string.IsNullOrEmpty(dataOtp) && !string.IsNullOrEmpty(otp))
                {
                    return 2;//Expired OTP
                }
                else
                {
                    return -1;
                }    
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return -1;
            }

        }

        public bool DDOS_OTP(SendOtpModel info, int maxRequest, int seconds)
        {
            var chk = true;
            try
            {
                string phone, campaign_code = "";
                phone = info.PHONE;
                campaign_code = info.CAMPAIGN_CODE;

                var key = string.Join(
                    "-",
                    seconds,
                    phone,
                    campaign_code
                );
                // increment the cache value
                var count = 1;
                if (HttpRuntime.Cache[key] != null)
                    count = (int)HttpRuntime.Cache[key] + 1;
                HttpRuntime.Cache.Insert(key, count, null, DateTime.UtcNow.AddSeconds(seconds), Cache.NoSlidingExpiration, CacheItemPriority.Low, null);
                if (count > maxRequest)
                    chk = false;
            }
            catch (Exception ex)
            {
                chk = false;
            }
            return chk;
        }
        #endregion

        #region Từ danh xưng lấy mã giới tính
        public string NameTitleToGender(string NameTitle)
        {
            string gender = "";
            switch (NameTitle)
            {
                case nameof(goConstants.DanhXung.ONG):
                    gender = nameof(goConstants.Gender_Unit.M);
                    break;
                case nameof(goConstants.DanhXung.BA):
                    gender = nameof(goConstants.Gender_Unit.F);
                    break;
            }
            return gender;
        }
        #endregion

        public string GetConfigDB(string keyConfig)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                goInstanceCaching.Instance.InstanceCaching();
                string key = goConstantsRedis.PrefixWebconfig + keyConfig;
                return goBusinessCache.Instance.Get<string>(key);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public BaseValidate ResValidate(bool success, string err, string errMess)
        {
            BaseValidate baseValidate = new BaseValidate();
            baseValidate.Success = success;
            baseValidate.Error = err;
            baseValidate.ErrorMessage = errMess;
            return baseValidate;
        }

        public MaskValidate ResValidateMask(bool err, string errCode, string errMess)
        {
            MaskValidate validate = new MaskValidate();
            validate.Err = true;
            validate.ErrCode = errCode;
            validate.ErrMess = errMess;
            return validate;
        }

        public List<SysOrgInfo> GetOrgRoot(List<SysOrgInfo> lstOrg, string org_code)
        {
            return lstOrg
                .Where(x => x.org_code.Equals(org_code))
                .Union(lstOrg.Where(x => x.org_code.Equals(org_code))
                    .SelectMany(y => GetOrgRoot(lstOrg, y.parent_code))
                ).Where(i => string.IsNullOrEmpty(i.parent_code)).ToList();
        }
        #endregion

        #region Remove key null JOBject

        public JToken RemoveEmptyChildren(JToken token)
        {
            if (token.Type == JTokenType.Object)
            {
                JObject copy = new JObject();
                foreach (JProperty prop in token.Children<JProperty>())
                {
                    JToken child = prop.Value;
                    if (child.HasValues)
                    {
                        child = RemoveEmptyChildren(child);
                    }
                    if (!IsEmpty(child))
                    {
                        copy.Add(prop.Name, child);
                    }
                }
                return copy;
            }
            else if (token.Type == JTokenType.Array)
            {
                JArray copy = new JArray();
                foreach (JToken item in token.Children())
                {
                    JToken child = item;
                    if (child.HasValues)
                    {
                        child = RemoveEmptyChildren(child);
                    }
                    if (!IsEmpty(child))
                    {
                        copy.Add(child);
                    }
                }
                return copy;
            }
            return token;
        }

        public bool IsEmpty(JToken token)
        {
            return (token.Type == JTokenType.Null) ||
               (token.Type == JTokenType.Array && !token.HasValues) ||
               (token.Type == JTokenType.Object && !token.HasValues);
        }

        #endregion Remove key null JOBject

        #region Call Another Url

        public HttpResponseMessage postJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PostAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API call pvc ", ex.Message + JInput.ToString());
                throw new Exception("Service timeout " + ex.ToString());
            }

        }

        public HttpResponseMessage putJsonAPI(JObject JInput, string strUrl, JObject JHeader)
        {
            try
            {
                HttpClient client = new HttpClient();
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromMilliseconds(15000);
                if (JHeader != null)
                {
                    foreach (JProperty prop in JHeader.Properties())
                    {
                        client.DefaultRequestHeaders.Add(prop.Name, JHeader[prop.Name].ToString());
                    }
                }
                HttpResponseMessage response = client.PutAsJsonAsync(strUrl, JInput).Result;
                return response;
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API call pvc ", ex.Message + JInput.ToString());
                throw new Exception("Service timeout " + ex.ToString());
            }

        }

        #endregion
    }
    public class AuditLogsInfoV2
    {
        public ObjectId _id { get; set; }
        /// <summary>
        /// Id log trả về trong response (phục vụ tra cứu (Org + id + date)(Không cần truyền)
        /// </summary>
        public string LogId { get; set; } = Guid.NewGuid().ToString() + Random(3);
        /// <summary>
        /// Mã đối tác yêu cầu (phục vụ tra cứu)
        /// </summary>
        public string ParentCode { get; set; }
        /// <summary>
        /// Mã action
        /// </summary>
        public string ActionCode { get; set; }
        /// <summary>
        /// Request gửi tới
        /// </summary>
        public string request { get; set; }
        /// <summary>
        /// Lỗi cụ thể nếu exception
        /// </summary>
        public string response { get; set; }
        /// <summary>
        /// Ngày tháng năm (Không cần truyền)
        /// </summary>
        public TimeInit DayInfo { get; set; } = new TimeInit();
        /// <summary>
        /// Thời gian tạo (Không cần truyền)
        /// </summary>
        public string TimeInit { get; set; } = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

        private static string Random(int size)
        {
            var chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            var data = new byte[size];
            using (var crypto = new System.Security.Cryptography.RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            var result = new StringBuilder(size);
            foreach (var b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }       
    }
    public class TimeInit
    {
        public string Year { get; set; } = DateTime.Now.ToString("yyyy");
        public string Month { get; set; } = DateTime.Now.ToString("MM");
        public string Day { get; set; } = DateTime.Now.ToString("yyyyMMdd");
    }
}
