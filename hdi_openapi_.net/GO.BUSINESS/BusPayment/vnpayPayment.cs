﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Service;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Orders;
using GO.DTO.Payment;
using GO.ENCRYPTION;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GO.BUSINESS.BusPayment
{
    public class vnpayPayment
    {
        #region Contructor
        private static readonly Lazy<vnpayPayment> InitInstance = new Lazy<vnpayPayment>(() => new vnpayPayment());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        public static vnpayPayment Instance => InitInstance.Value;
        #endregion

        public JObject createQr(string orderId, string amount, string payDate, string desc,
           string tipAndFee, PayOrgModels payOrg, PayConfigModels payConfigRef)
        {
            try
            {
                CREATE_QR qr = new CREATE_QR();
                qr.appId = payConfigRef.CLIENT_ID;// jConfig["CLIENT_ID"].ToString();// "HDI";
                qr.masterMerCode = payConfigRef.MASTER_CODE;// jConfig["MASTER_CODE"].ToString();// "A000000775";
                qr.merchantName = payConfigRef.CLIENT_NAME;// jConfig["CLIENT_NAME"].ToString();// "BH_HDI";
                qr.merchantCode = payConfigRef.MERCHANT_ID;// jConfig["MERCHANT_ID"].ToString();// "HDI";
                qr.merchantType = payConfigRef.USERNAME;// "";
                qr.terminalId = payConfigRef.TERMINAL_ID;// jConfig["TERMINAL_ID"].ToString();
                qr.serviceCode = payConfigRef.SERVICE_CODE;// jConfig["SERVICE_CODE"].ToString(); //"03";// "03";//mã dịch vụ cho QR               
                qr.countryCode = payConfigRef.COUNTRY_CODE;// jConfig["COUNTRY_CODE"].ToString(); //"VN";
                string _secretKey = payConfigRef.SECRET;// jConfig["SECRET"].ToString();

                qr.payType = payConfigRef.PAY_CODE; //"03";//QR sử dụng để thanh toán
                qr.ccy = payConfigRef.CCY;// jConfig["CCY"].ToString(); //"704";
                qr.productId = "";
                qr.txnId = orderId;
                qr.billNumber = orderId;
                qr.amount = amount;
                qr.expDate = payDate;
                qr.desc = desc;
                qr.tipAndFee = tipAndFee;

                qr.checksum = goEncryptBase.Instance.Md5Encode(qr.appId + "|" +
                        qr.merchantName + "|" + qr.serviceCode
                        + "|" + qr.countryCode + "|" +
                        qr.masterMerCode + "|" +
                        qr.merchantType + "|" +
                        qr.merchantCode + "|" + qr.terminalId
                        + "|" + qr.payType + "|" + qr.productId +
                        "|" + qr.txnId + "|" + qr.amount + "|" +
                        qr.tipAndFee + "|" + qr.ccy + "|" +
                        qr.expDate + "|" + _secretKey);
                JObject oJsonInput = JObject.FromObject(qr);
                PayFuncModels func = payOrg.PAY_FUNC.Where(x => x.TYPE_METHOD == "CREATE").FirstOrDefault();
                HttpResponseMessage rp = goLibPay.Instance.callApiQRCode(oJsonInput,func);
                RESULT_QR rs = new RESULT_QR();
                string message = rp.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (rp.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    rs.code = "00";
                    rs.message = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    rs.code = details["code"].ToString();
                    rs.message = details["message"].ToString();
                }
                if (rs.code == "00")
                {
                    JObject jsonOut = JObject.Parse(rs.message);
                    if (jsonOut["code"].ToString().Equals("00"))
                    {
                        //xử lý data qr trả về                   
                        rs.message = jsonOut["message"].ToString();
                        rs.data = jsonOut["data"].ToString();
                        rs.checkSum = jsonOut["checksum"].ToString();
                        rs.idQrcode = jsonOut["idQrcode"].ToString();
                        //kiểm tra md5
                        string strMD5 = goEncryptBase.Instance.Md5Encode(rs.code + "|" + rs.message + "|" +
                        rs.data + "|null|" + _secretKey);
                        if (!strMD5.Equals(rs.checkSum))
                        {
                            rs.code = "15";
                            rs.message = "Lỗi check sum dữ liệu HD";
                        }
                    }
                    else
                    {
                        rs.code = jsonOut["code"].ToString();
                    }

                }
                JObject obj = new JObject();
                if (rs.code == "00")
                {
                    string strQRBase64 = goBussinessPayment.Instance.getQrCode(rs.data, "Q", 8, "");
                    obj.Add("code", "000");
                    obj.Add("message", rs.message);
                    obj.Add("imageQR", strQRBase64);
                    obj.Add("dataQR", rs.data);
                    obj.Add("txnId", rs.idQrcode);
                }
                else
                {
                    obj.Add("code", rs.code);
                    obj.Add("message", rs.message);
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string vnpay_CreateUrlPay(TransInfo trans, PayOrgModels orgModels, PayConfigModels config, string strChannel,string strTypePay, string strLang)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                PayFuncModels pay_fun = orgModels.PAY_FUNC.Where(m => m.TYPE_METHOD == "CREATE").FirstOrDefault();
                string vnp_Url = pay_fun.URL;
                string vnp_HashSecret = config.SECRET;
                string vnp_Returnurl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                if (!strChannel.Equals("WEB"))
                {
                    vnp_Returnurl = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY_NEW);
                }
                string strUrlPay = "";
                OrderInfo order = new OrderInfo();
                order.OrderId = trans.TRANSACTION_ID;
                order.Amount = Convert.ToInt64(trans.TOTAL_AMOUNT);
                order.OrderDesc = goBussinessSMS.Instance.removeMark(trans.ORDER_TITLE);
                order.CreatedDate = DateTime.Now;
                VnPayLibrary vnpay = new VnPayLibrary();
                vnpay.AddRequestData("vnp_Version", "2.0.0");
                vnpay.AddRequestData("vnp_Command", "pay");//pay  genqr
                vnpay.AddRequestData("vnp_TmnCode", config.CLIENT_ID);
                vnpay.AddRequestData("vnp_Amount", (order.Amount * 100).ToString());
                vnpay.AddRequestData("vnp_CreateDate", order.CreatedDate.ToString("yyyyMMddHHmmss"));
                vnpay.AddRequestData("vnp_CurrCode", "VND");
                vnpay.AddRequestData("vnp_IpAddr", Utils.GetIpAddress());
                if (!string.IsNullOrEmpty(strLang) && strLang.ToUpper().Equals("EN"))
                {
                    vnpay.AddRequestData("vnp_Locale", "en");
                }
                else
                {
                    vnpay.AddRequestData("vnp_Locale", "vn");
                }
                switch (strTypePay)
                {
                    case "ND":
                        {
                            vnpay.AddRequestData("vnp_BankCode", "VNBANK");
                            break;
                        }
                    case "VISA":
                        {
                            vnpay.AddRequestData("vnp_BankCode", "INTCARD");
                            break;
                        }
                }
                vnpay.AddRequestData("vnp_OrderInfo", order.OrderDesc);
                vnpay.AddRequestData("vnp_OrderType", "250001");
                vnpay.AddRequestData("vnp_ReturnUrl", vnp_Returnurl);
                vnpay.AddRequestData("vnp_TxnRef", order.OrderId.ToString());
                strUrlPay = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
                return strUrlPay;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao url pay vnpay" + JsonConvert.SerializeObject(orgModels), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi tao url pay vnpay 228", JsonConvert.SerializeObject(trans) + " --- " + JsonConvert.SerializeObject(orgModels));
                return "";
            }
        }

        public dataRequestOrderIpn convertDataIpnPayVnpay(VnPayLibrary vnpay, PayOrgModels org, TransInfo transPay)
        {
            try
            {
                PayConfigModels configmd = org.PAY_CONFIG.Where(x => x.CLIENT_ID == vnpay.GetResponseData("vnp_TmnCode")).FirstOrDefault();
                dataRequestOrderIpn jOutPut = new dataRequestOrderIpn();
                transCard trans = new transCard();
                paymentResult pay = new paymentResult();
                jOutPut.code = vnpay.GetResponseData("vnp_ResponseCode");
                jOutPut.messageCode = vnpay.GetResponseData("vnp_ResponseCode");
                pay.merchantId = configmd.MERCHANT_ID;
                pay.paymentType = configmd.PAY_CODE;
                pay.masterCode = configmd.MASTER_CODE;                
                orderIpn odr = new orderIpn();
                odr.transactionNo = vnpay.GetResponseData("vnp_TransactionNo");
                odr.orderCode = vnpay.GetResponseData("vnp_TxnRef");
                odr.amount = transPay.TOTAL_AMOUNT;
                odr.creationTime = transPay.PAY_DATE;
                odr.currency = transPay.CURRENCY;
                odr.reference = "";
                odr.totalAuthorizedAmount = transPay.TOTAL_AMOUNT;
                odr.totalCapturedAmount = transPay.TOTAL_AMOUNT;
                odr.totalRefundedAmount = transPay.DISCOUNT;
                //DateTime dtPay = DateTime.ParseExact(vnpay.GetResponseData("vnp_PayDate"), "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                odr.payDate = vnpay.GetResponseData("vnp_PayDate");// dtPay.ToString("yyMMddHHmm");//yyMMddHHmm
                odr.bankCode = vnpay.GetResponseData("vnp_BankCode");
                odr.mobile = "";
                odr.accountNo = vnpay.GetResponseData("vnp_BankTranNo");
                pay.order = odr;
                jOutPut.paymentResult = pay;
                trans.brand = vnpay.GetResponseData("vnp_BankCode");
                trans.expiryMonth = "";
                trans.expiryYear = "";
                trans.issuer = "";
                trans.issuerCode = "";
                trans.nameOnCard = "";
                trans.issueDate = "";
                trans.number = "";
                trans.scheme = "";
                trans.status3ds = "";
                trans.deviceId = "";
                trans.type = "";
                jOutPut.card = trans;
                jOutPut.checksum = vnpay.GetResponseData("vnp_SecureHash");
                return jOutPut;
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán QR ", ex.ToString() + JsonConvert.SerializeObject(vnpay));
                return null;
            }
        }
    }
}
