﻿using GO.BUSINESS.Action;
using GO.BUSINESS.Common;
using GO.BUSINESS.Orders;
using GO.BUSINESS.Service;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.DTO.Orders;
using GO.DTO.Payment;
using GO.DTO.SystemModels;
using GO.ENCRYPTION;
using GO.HELPER.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.HELPER.Utility.goUtility;

namespace GO.BUSINESS.BusPayment
{
    public class goBusPay
    {
        #region Contructor
        private static readonly Lazy<goBusPay> InitInstance = new Lazy<goBusPay>(() => new goBusPay());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        public static goBusPay Instance => InitInstance.Value;
        #endregion

        #region create trans
        public BaseResponse payment(BaseRequest request)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            BaseRequest reGoc = request;
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                string action = request.Action.ActionCode; /*goConstantsProcedure.Pay_Get_Config----->HDB_QR*/
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    CreateTrans orderCreate = JsonConvert.DeserializeObject<CreateTrans>(JsonConvert.SerializeObject(request.Data));
                    if (orderCreate == null)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi tao transaction pay" + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P02), goConstantsError.Instance.ERROR_P02);
                    }
                    //Lấy thông tin đê tạo giao dịch thanh toán
                    PayOrgModels payOrg = new PayOrgModels();
                    PayConfigModels payConfigRef = new PayConfigModels();
                    PayAssignModels assign = goLibPay.Instance.getConfigQR(orderCreate.orgSeller, orderCreate.productCode, orderCreate.structCode, orderCreate.channel, ref payOrg, ref payConfigRef);
                    if (assign == null || string.IsNullOrEmpty(assign.ORG_SELLER))
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Chưa phân quyền thanh toán cho đơn vị" + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P01), goConstantsError.Instance.ERROR_P01);
                    }
                    string strTransactionID = Guid.NewGuid().ToString("N").Substring(0, 8) + DateTime.Now.ToString("MMHHmmss");// DateTime.Now.ToString("ddMMyyyy");
                    JObject QRResult = new JObject();
                    string dtExp = "";
                    if (assign.QR_ID.Length > 0 && payOrg != null && payOrg.ORG_CODE.Length > 0)
                    {
                        //có khởi tạo qr code

                        if (assign.EXP_DATE.Equals("TN"))
                        {
                            dtExp = DateTime.Now.ToString("yyMMdd") + "2359";
                        }
                        else
                        {
                            dtExp = DateTime.Now.AddHours(double.Parse(assign.EXP_DATE)).ToString("yyMMddHHmm");
                        }

                        switch (payOrg.ORG_CODE)
                        {
                            case "VNPAY_QR":
                                {
                                    QRResult = vnpayPayment.Instance.createQr(strTransactionID, orderCreate.totalAmount, dtExp, orderCreate.desc, orderCreate.discountAmount, payOrg, payConfigRef);
                                    break;
                                }
                            case "HDBANK_QR":
                                {
                                    QRResult = hdbankPayment.Instance.createQr(strTransactionID, orderCreate.totalAmount, dtExp, orderCreate.desc, orderCreate.discountAmount, payOrg, payConfigRef);
                                    break;
                                }
                        }
                    }
                    //khởi tạo QR code nếu giao dịch phục vụ cổng thanh toán

                    string strQRData = "CK";
                    string strTaxID = "-1";
                    string strImageData = "";
                    if (QRResult != null && QRResult["code"].ToString().Equals("000"))
                    {
                        strQRData = QRResult["dataQR"].ToString();
                        strTaxID = QRResult["txnId"].ToString();
                        strImageData = QRResult["imageQR"]?.ToString();
                    }
                    //đẩy dữ liệu vào bảng giao dịch
                    string strMD5 = goEncryptBase.Instance.Md5Encode(orderCreate.orderId + strTransactionID);
                    string[] ParamInsert = new string[] { strTransactionID, payOrg.ORG_CODE, orderCreate.orgSeller,  "CREATE", orderCreate.orderId, orderCreate.desc , orderCreate.desc_En , orderCreate.taxAmount , orderCreate.discountAmount ,
                          orderCreate.amount,orderCreate.totalAmount,"VN",DateTime.Now.ToString("ddMMyyyy HHmm"), goUtility.Instance.ConvertToDateTimePay(dtExp, "yyMMddHHmm").ToString("ddMMyyyy HHmm"),assign.ID,strQRData,strTaxID,
                          strTransactionID,orderCreate.structCode,strMD5,orderCreate.payerName,orderCreate.payerMobile,orderCreate.payerEmail, orderCreate.contentPay,"[]", orderCreate.payType}; // Add PayType to procedure
                    JObject JoutPut = new JObject();
                    request.Data = goUtility.Instance.convertArrayToJsonParam(ParamInsert);
                    request.Action.ActionCode = goConstantsProcedure.Pay_insert_trans;
                    response = goBusinessAction.Instance.GetBaseResponse(request);
                    if (response.Success)
                    {
                        List<JObject> objKQ = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                        if (objKQ[0]["TRANSACTION_ID"].ToString().Equals("X"))
                        {
                            goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng payment 120", JsonConvert.SerializeObject(reGoc));
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng: " + reGoc, Logger.ConcatName(nameClass, LoggerName));
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", "01", "Không tạo được giao dịch thanh toán do đã tồn tại đơn hàng");
                        }
                        JoutPut.Add("transactionNo", strTransactionID);
                        JoutPut.Add("payDate", orderCreate.payDate);
                        JoutPut.Add("dtExp", dtExp);
                        JoutPut.Add("appId", config.partnerCode);
                        JoutPut.Add("bankCode", "HDI_PAY");
                        JoutPut.Add("amount", orderCreate.totalAmount);
                        JoutPut.Add("url_redirect", strURLWEBPay + "?id=" + goLibPay.Instance.Encrypt(strMD5));
                        JoutPut.Add("imageQR", strImageData);
                        JoutPut.Add("dataQR", strQRData);
                        JoutPut.Add("orderId", orderCreate.orderId);
                        JoutPut.Add("payerName", orderCreate.payerName);
                        JoutPut.Add("payerMobile", orderCreate.payerMobile);
                        JoutPut.Add("payerEmail", orderCreate.payerEmail);
                        // Add 2022-12-06 BY ThienTVB add param payType
                        JoutPut.Add("payType", orderCreate.payType);
                        // End 2022-12-06 BY ThienTVB
                    }
                    else
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 138", JsonConvert.SerializeObject(response) + response.Error + response.ErrorMessage);
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P03), goConstantsError.Instance.ERROR_P03);
                    }
                    //tạo dữ liệu và kích hoạt gửi thông báo
                    JObject jThongBao = JObject.FromObject(assign);
                    jThongBao.Add("result", JoutPut);
                    //goBusinessServices.Instance.callNotify(jThongBao, "TBTT");
                    new Task(() => goBusinessServices.Instance.callNotify(jThongBao, "TBTT")).Start();
                    return goBusinessCommon.Instance.getResultApi(response.Success, JoutPut, config, response.Error, response.ErrorMessage);
                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 929", JsonConvert.SerializeObject(response));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không tạo được giao dịch thanh toán: " + request, Logger.ConcatName(nameClass, LoggerName));
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_P02), goConstantsError.Instance.ERROR_P02);
                    return response;
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Không tạo được giao dịch thanh toán payment 937", JsonConvert.SerializeObject(request) + ex.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P02), goConstantsError.Instance.ERROR_P02);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        #endregion
        #region get trans webpayment
        public BaseResponse getInfoPayGateway(ReqPayModel request)
        {
            RepPayModel models = new RepPayModel();
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                string strDataCheck = goLibPay.Instance.Decrypt(request.Id);
                string[] paramQR = new string[] { strDataCheck, "DATA_CHECK" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_info_getway, environment, paramQR);
                List<JObject> objTRANS = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objTRANS != null && objTRANS.Count > 0)
                {
                    List<PayInfo> lstPayInfo = new List<PayInfo>();
                    PayInfo info = new PayInfo();
                    info.Total_Amount = objTRANS[0]["TOTAL_AMOUNT"].ToString();
                    if (request.lang.Equals("EN"))
                        info.Total_Amount_Text = new GO.HELPER.Utility.goUtility().ConvertMoneyToText(objTRANS[0]["TOTAL_AMOUNT"].ToString(), MoneyFomat.VIET_TO_ENGLISH);
                    else
                        info.Total_Amount_Text = new GO.HELPER.Utility.goUtility().ConvertMoneyToText(objTRANS[0]["TOTAL_AMOUNT"].ToString(), MoneyFomat.MONEY_TO_VIET);
                    info.Amount = objTRANS[0]["AMOUNT"].ToString();
                    info.VAT = objTRANS[0]["TAX_AMOUNT"]?.ToString();
                    info.Gif_Code = "";
                    info.Discount = objTRANS[0]["DISCOUNT"]?.ToString();
                    info.Transaction_Id = request.Id;
                    info.Order_Id = objTRANS[0]["TRANSACTION_ID"].ToString();
                    info.Payer_Name = objTRANS[0]["PAYER_NAME"]?.ToString();
                    string strQRImage = "";
                    if (request.lang.Equals("EN"))
                    {
                        info.Description = objTRANS[0]["ORDER_SUMMARY"]?.ToString();
                        if (objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                        {
                            info.Status_Name = "Wait for pay";
                            strQRImage = goBussinessPayment.Instance.getQrCode(objTRANS[0]["QR_DATA"]?.ToString(), "Q", 8, "");
                        }
                        else
                        {
                            info.Status_Name = "Customer paid";
                            strQRImage = goBussinessPayment.Instance.getQrCode("Đã thanh toán", "Q", 8, "");
                        }

                    }
                    else
                    {
                        info.Description = objTRANS[0]["ORDER_TITLE"]?.ToString();
                        if (objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                        {
                            strQRImage = goBussinessPayment.Instance.getQrCode(objTRANS[0]["QR_DATA"]?.ToString(), "Q", 8, "");
                            info.Status_Name = "Chờ thanh toán";
                        }
                        else
                        {
                            info.Status_Name = "Khách hàng đã thanh toán";
                            strQRImage = goBussinessPayment.Instance.getQrCode("Đã thanh toán", "Q", 8, "");
                        }

                    }

                    info.Status = objTRANS[0]["STATUS"].ToString();
                    info.Date_Pay = objTRANS[0]["CREATE_DATE"]?.ToString();
                    info.Path_Logo = "";
                    info.Path_Banner = "";
                    info.Qr_Code = strQRImage;
                    info.Qr_Expire = objTRANS[0]["PAY_DATE_VIEW"].ToString();
                    info.Min_Amount = "10000";
                    info.Max_Amount = "50000000";
                    info.Currency = "VNĐ";
                    info.Contract_Code = objTRANS[0]["TRANSACTION_REF"]?.ToString();
                    // Add 2022-12-06 By ThienTVB add Pay_type request from Vinh
                    info.Pay_Type = objTRANS[0]["PAY_TYPE"]?.ToString();
                    // End 2022-12-06
                    info.Description_Min = "Số tiền tối thiểu của thanh toán qua mã QR là 10.000 VNĐ. Bằng việc quét mã QR, bạn chấp nhận thanh toán theo số tiền tối thiểu.";
                    info.Description_Max = "Số tiền tối đa của thanh toán qua mã QR là 50.000.000 VNĐ.";

                    //lấy thông tin của đơn vị thanh toán
                    List<PayOrgModels> payOrg = new List<PayOrgModels>();
                    PayConfigModels payConfigRef = new PayConfigModels();
                    PayAssignModels assign = goLibPay.Instance.getConfigWebPayment(objTRANS[0]["ASSIGN_ID"].ToString(), ref payOrg);
                    if (assign == null || string.IsNullOrEmpty(assign.ORG_SELLER))
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Chưa phân quyền thanh toán cho đơn vị" + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, LoggerName));
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P01), goConstantsError.Instance.ERROR_P01);
                    }
                    if (assign.IS_TRANSFER.Equals("1") && objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                    {
                        info.Content_Pay = objTRANS[0]["CONTENT_TRANSFER"]?.ToString();
                        info.Bank_Account = assign.BANK_ACCOUNT;
                        if (request.lang.Equals("EN"))
                        {
                            info.Bank_Branch = assign.BANK_BRANCH_EN;
                            info.Bank_Name = assign.BANK_NAME_EN;
                            info.Bank_Branch = assign.BANK_BRANCH_EN;
                            info.Account_Name = assign.ACCOUNT_NAME_EN;
                        }
                        else
                        {
                            info.Bank_Branch = assign.BANK_BRANCH;
                            info.Bank_Name = assign.BANK_NAME;
                            info.Bank_Branch = assign.BANK_BRANCH;
                            info.Account_Name = assign.ACCOUNT_NAME;
                        }

                    }
                    lstPayInfo.Add(info);
                    List<PayGateway> lstPayGateway = new List<PayGateway>();
                    if (assign != null && assign.PAY_CONFIG != null && assign.PAY_CONFIG.Count > 0 && objTRANS[0]["STATUS"].ToString().Equals("NEXT"))
                    {
                        foreach (PayConfigModels org_pay in assign.PAY_CONFIG)
                        {
                            PayOrgModels orgConfig = payOrg.Where(x => x.ORG_ID == org_pay.ORG_ID).FirstOrDefault();
                            PayGateway hdbank = new PayGateway();
                            hdbank.Org_Code = orgConfig.ORG_CODE;
                            if (request.lang.Equals("EN"))
                            {
                                hdbank.Org_Name = orgConfig.ORG_NAME_EN;
                            }
                            else
                            {
                                hdbank.Org_Name = orgConfig.ORG_NAME;
                            }
                            hdbank.Path_Logo = orgConfig.LOGO;
                            hdbank.Min_Amount = org_pay.MIN_AMOUNT;
                            hdbank.Max_Amount = org_pay.MAX_AMOUNT;
                            hdbank.Description_Min = "";
                            hdbank.Description_Max = "";
                            hdbank.Disable = true;
                            hdbank.LinkPay = "";
                            lstPayGateway.Add(hdbank);
                        }
                    }

                    models.lstPayInfo = lstPayInfo;
                    models.lstPayGateway = lstPayGateway;
                }
            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "lỗi ngoại lệ hàm trả về cho webpayment payment 118", ex.ToString());
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "getInfoPayGateway", request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = models;
            return response;
        }
        #endregion

        #region tạo link thanh toán từ cổng
        public BaseResponse createUrlPay(BaseRequest request)
        {
            //khi click vào đơn vị thanh toán sẽ chuyển sang web của đơn vị đó
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    string strID_Trans = goLibPay.Instance.Decrypt(jInput["ID"].ToString()); //LibSign.Decrypt(jInput["ID"].ToString());
                    if (string.IsNullOrEmpty(strID_Trans))
                    {
                        response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                    }
                    else
                    {
                        jInput["ID"] = strID_Trans;
                    }
                    string[] paramQR = new string[] { strID_Trans, "DATA_CHECK" };
                    response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_info_getway, environment, paramQR);
                    TransInfo trans = new TransInfo();
                    if (response.Success)
                    {
                        List<TransInfo> lsTran = goBusinessCommon.Instance.GetData<TransInfo>(response, 0);
                        if (lsTran != null && lsTran.Count > 0)
                        {
                            if (lsTran[0].STATUS.Equals("DONE"))
                            {
                                return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P04), goConstantsError.Instance.ERROR_P04);
                            }
                            else
                            {
                                trans = lsTran[0];
                            }

                        }
                        else
                        {
                            return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P05), goConstantsError.Instance.ERROR_P05);
                        }
                    }
                    else
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P05), goConstantsError.Instance.ERROR_P05);
                    }
                    //Lấy thông tin cấu hình đơn vị thanh toán
                    PayOrgModels payOrg = new PayOrgModels();
                    string strChannel = "", strTypePay = "";
                    PayConfigModels payConfigRef = goLibPay.Instance.getConfigCreateUrlPay(jInput["ORG_CODE"].ToString(), trans.ASSIGN_ID, ref payOrg, ref strTypePay, ref strChannel);
                    if (payOrg != null && payOrg.PAY_FUNC.Count > 0)
                    {
                        //lấy xong thông tin giao dịch tạo url thanh toán
                        string strUrlPay = goLibPay.Instance.getUrlPay(trans, payOrg, payConfigRef, jInput["ORG_CODE"].ToString(), strChannel, jInput["LINK_CALLBACK"].ToString(), strTypePay, jInput["lang"]?.ToString());
                        JObject jOutPut = new JObject();
                        jOutPut.Add("URL_REDIRECT", strUrlPay);
                        response.Success = true;
                        response.Data = jOutPut;
                        return response;
                    }
                    else
                    {
                        return goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_P06), goConstantsError.Instance.ERROR_P06);
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }

            return response;
        }

        #endregion


        #region IPN thanh toán
        public dataRequestOrderIpn convertJsonIPNQRCode(JObject jInput)
        {
            try
            {
                dataRequestOrderIpn jOutPut = new dataRequestOrderIpn();
                transCard trans = new transCard();
                paymentResult pay = new paymentResult();
                jOutPut.code = jInput["code"].ToString();
                jOutPut.messageCode = jInput["message"].ToString();
                if (jOutPut.code.Equals("00") || jOutPut.code.Equals("000"))
                {
                    pay.merchantId = jInput["merchantCode"].ToString();
                    pay.paymentType = jInput["msgType"].ToString();
                    pay.masterCode = jInput["masterMerCode"].ToString();
                    JArray arr = JArray.Parse(jInput["addData"].ToString());
                    JObject jDetail = (JObject)arr[0];
                    orderIpn odr = new orderIpn();
                    odr.transactionNo = jInput["qrTrace"].ToString();
                    odr.orderCode = jInput["txnId"].ToString();
                    odr.amount = jInput["amount"].ToString();
                    odr.creationTime = jInput["payDate"].ToString();
                    odr.currency = jDetail["ccy"].ToString();
                    odr.reference = jDetail["note"].ToString();
                    odr.totalAuthorizedAmount = jDetail["amount"].ToString();
                    odr.totalCapturedAmount = jDetail["amount"].ToString();
                    odr.totalRefundedAmount = jDetail["tipAndFee"] == null ? "" : jDetail["tipAndFee"]?.ToString();
                    odr.payDate = jInput["payDate"].ToString();
                    odr.bankCode = jInput["bankCode"].ToString();
                    odr.mobile = jInput["mobile"].ToString();
                    odr.accountNo = jInput["accountNo"].ToString();
                    pay.order = odr;
                    jOutPut.paymentResult = pay;
                    trans.brand = jInput["bankCode"].ToString();
                    trans.expiryMonth = "";
                    trans.expiryYear = "";
                    trans.issuer = "";
                    trans.issuerCode = "";
                    trans.nameOnCard = "";
                    trans.issueDate = "";
                    trans.number = "";
                    trans.scheme = "";
                    trans.status3ds = "";
                    trans.deviceId = "";
                    trans.type = "";
                    jOutPut.card = trans;
                    jOutPut.checksum = jInput["checksum"].ToString();
                    return jOutPut;
                }
                else
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi trừ tiền từ bank ", jInput.ToString());
                    return null;
                }

            }
            catch (Exception ex)
            {
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán QR ", ex.ToString() + jInput.ToString());
                return null;
            }
        }

        public BaseResponse updateTransPayment(TransInfo trans, dataRequestOrderIpn data, string strTypePay, string strOrgPay)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                List<PayOrgModels> payOrg = new List<PayOrgModels>();
                PayAssignModels assign = goLibPay.Instance.getConfigWebPayment(trans.ASSIGN_ID.ToString(), ref payOrg);
                //PayOrgModels orgConfig = payOrg.Where(x => x.ORG_ID == assign.QR_PAY.ORG_ID).FirstOrDefault();
                string[] paramUpdate = new string[] { data.code, data.messageCode, data.paymentResult.order.orderCode, strOrgPay, data.paymentResult.order.amount,
                    data.paymentResult.order.amount,  data.paymentResult.order.transactionNo,strTypePay, data.paymentResult.order.bankCode , data.paymentResult.order.reference};
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Update_Trans, environment, paramUpdate);
                List<JObject> objHDBQRUpdate = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                if (objHDBQRUpdate.Count == 0)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai hdi: " + JsonConvert.SerializeObject(data), Logger.ConcatName(nameClass, LoggerName));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật transaction thành công", JsonConvert.SerializeObject(data));
                    objReturn["code"] = "04";
                    objReturn["message"] = "Lỗi tạo đơn hàng";
                    response.Data = objReturn;
                    response.Success = false;
                    return response;
                }
                //thực thi gọi api webpaymet để cập nhật trạng thái
                if (strTypePay.Equals("QR"))
                {
                    PayCallBackModels orgCallback = goLibPay.Instance.getCallBack("HDI", "", "", assign.CHANNEL, "WEB_PAY");
                    PayCallBackModels orgRedirect = goLibPay.Instance.getCallBack(assign.ORG_SELLER, assign.PRODUCT_CODE, assign.STRUCT_CODE, assign.CHANNEL, "REDIRECT");
                    string strUrlCallBack = "";
                    if (orgRedirect != null)
                    {
                        strUrlCallBack = orgRedirect.URL;
                    }
                    bool b = goLibPay.Instance.callWebPayment(goLibPay.Instance.Encrypt(trans.DATA_CHECK), strUrlCallBack, orgCallback);
                    if (!b)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi cập nhật webpayment  payment 429", "cấu hình call webpayment " + JsonConvert.SerializeObject(orgCallback));
                        Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap nhat trang thai webpayment : " + JsonConvert.SerializeObject(orgCallback), Logger.ConcatName(nameClass, LoggerName));
                    }
                }
                objReturn["message"] = "Cập nhật thành công";
                objCT["txnId"] = data.paymentResult.order.orderCode;
                objReturn["data"] = objCT;
                //set lai tham so tra ve
                data.paymentResult.order.transactionNo = data.paymentResult.order.orderCode;
                data.paymentResult.order.orderCode = objHDBQRUpdate[0]["ORDER_CODE"].ToString();
                data.paymentResult.order.amount = objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString();
                ResponseOrderIpn returnIPN = new ResponseOrderIpn();
                returnIPN.Success = true;
                returnIPN.Data = data;
                returnIPN.Data.paymentResult.paymentType = objHDBQRUpdate[0]["PAY_TYPE"].ToString();
                returnIPN.Data.paymentResult.payMethod = objHDBQRUpdate[0]["ORG_PAY_CODE"].ToString();
                DateTime dtPay = DateTime.ParseExact(data.paymentResult.order.payDate, "yyyyMMddHHmmss", CultureInfo.InvariantCulture);
                // Edit 2022-12-08 By ThienTVB Change format DateTime 12 -> 24h
                returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy HH:mm:ss", "");
                //returnIPN.Data.paymentResult.order.payDate = goUtility.Instance.DateToString(dtPay, "dd/MM/yyyy hh:mm:ss", "");
                // End 2022-12-08 By ThienTVB
                //ở đây phải kiểm tra thêm trường hợp 
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Goi cap don: ", Logger.ConcatName(nameClass, LoggerName));
                BaseResponse respIPN_HDI = goBusinessOrders.Instance.IpnOrder(returnIPN, objHDBQRUpdate[0]["ORDER_CODE"].ToString(), objHDBQRUpdate[0]["TOTAL_AMOUNT"].ToString());
                if (!respIPN_HDI.Success)
                {
                    objReturn["code"] = "03";
                    objReturn["message"] = "Đơn hàng đã được thanh toán";
                    goBussinessEmail.Instance.sendEmaiCMS("", "Không cập nhật được trạng thái đơn HDI payment 460", respIPN_HDI.Error + respIPN_HDI.ErrorMessage + JsonConvert.SerializeObject(returnIPN));
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Loi cap đơn ipn: " + JsonConvert.SerializeObject(data), Logger.ConcatName(nameClass, LoggerName));
                    string[] strparam = new string[] { objHDBQRUpdate[0]["ORG_INIT_CODE"].ToString(), respIPN_HDI.Error, respIPN_HDI.ErrorMessage, data.paymentResult.order.transactionNo, JsonConvert.SerializeObject(returnIPN) };
                    respIPN_HDI = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_Insert_IPNError, environment, strparam);
                    if (!respIPN_HDI.Success && goBusinessCommon.Instance.GetData<JObject>(respIPN_HDI, 0).Count == 0)
                    {
                        goBussinessEmail.Instance.sendEmaiCMS("", "không insert được pay_ipn_error  payment 466", respIPN_HDI.Error + respIPN_HDI.ErrorMessage);
                    }
                    response.Success = false;
                    response.Data = objReturn;
                    return response;
                }

                response.Success = true;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["code"] = "04";
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "updateTransPayment", data, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = objReturn;
            return response;
        }
        //IPN QR HDBANK
        public BaseResponse ipnQRHdbank(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                JObject jConfig = new JObject();
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN QR HDBANK new: " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                dataRequestOrderIpn data = convertJsonIPNQRCode(JObject.Parse(obj.ToString()));
                if (data == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán  payment 380", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    objReturn["code"] = "04";
                    objReturn["message"] = "Lỗi tạo đơn hàng";
                    response.Data = objReturn;
                    return response;
                }
                string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);

                //lấy thông tin giao dịch
                string[] paramQR = new string[] { data.paymentResult.order.orderCode, "TRANS_ID" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_info_getway, environment, paramQR);
                TransInfo trans = new TransInfo();
                if (response.Success)
                {
                    List<TransInfo> lsTran = goBusinessCommon.Instance.GetData<TransInfo>(response, 0);
                    if (lsTran != null && lsTran.Count > 0)
                    {
                        if (lsTran[0].STATUS.Equals("DONE"))
                        {
                            objReturn["code"] = "03";
                            objReturn["message"] = "Đơn hàng đã được thanh toán";
                            response.Data = objReturn;
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Đơn hàng đã được thanh toán 655", Logger.ConcatName(nameClass, LoggerName));
                            return response;
                        }
                        else
                        {
                            trans = lsTran[0];
                            List<PayOrgModels> payOrg = new List<PayOrgModels>();
                            PayAssignModels assign = goLibPay.Instance.getConfigWebPayment(trans.ASSIGN_ID, ref payOrg);
                            //kiểm tra checksum code|msgType|txnId|qrTrace|bankCode|mobile|accountNo|amount|payDate|merchantCode|
                            string strMD5 = data.code + "|" + data.paymentResult.paymentType + "|" + data.paymentResult.order.orderCode + "|" + data.paymentResult.order.transactionNo + "|" + data.paymentResult.order.bankCode + "|" + data.paymentResult.order.mobile + "|"
                                + data.paymentResult.order.accountNo + "|" + data.paymentResult.order.amount + "|" + data.paymentResult.order.payDate + "|" + data.paymentResult.merchantId + "|" + assign.QR_PAY.SECRET;
                            if (!data.checksum.ToUpper().Equals(goEncryptBase.Instance.Md5Encode(strMD5)))
                            {
                                //objReturn["code"] = "04";
                                //objReturn["message"] = "Lỗi checksum";
                                //response.Data = objReturn;
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Đơn hàng đã được thanh toán " + strMD5 + "--" + goEncryptBase.Instance.Md5Encode(strMD5), Logger.ConcatName(nameClass, LoggerName));
                                //return response;
                            }

                            //goEncryptBase.Instance.Md5Encode
                        }

                    }
                    else
                    {
                        objReturn["code"] = "04";
                        objReturn["message"] = "Lỗi tạo đơn hàng";
                        response.Data = objReturn;
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không tìm thấy giao dịch", Logger.ConcatName(nameClass, LoggerName));
                        return response;
                    }
                }
                else
                {
                    objReturn["code"] = "04";
                    objReturn["message"] = "Lỗi tạo đơn hàng";
                    response.Data = objReturn;
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Không tìm thấy giao dịch", Logger.ConcatName(nameClass, LoggerName));
                    return response;
                }

                // thực thi cập nhật dữ liệu
                return updateTransPayment(trans, data, "QR", "HDBANK_QR");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnQRHdbank", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = objReturn;
            return response;
        }

        //IPN QR VNPAY
        public BaseResponse ipnQRVnpay(object obj)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            JObject objReturn = new JObject();
            objReturn.Add("code", "00");
            objReturn.Add("message", "HDI da nhan duoc thong tin");
            JObject objCT = new JObject();
            objCT.Add("txnId", "");
            objReturn.Add("data", objCT);
            try
            {
                JObject jConfig = new JObject();
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "JsonIPN : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                dataRequestOrderIpn data = convertJsonIPNQRCode(JObject.Parse(obj.ToString()));
                if (data == null)// || ipnHdbank.addData == null
                {
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán  payment 380", obj.ToString());
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi map cấu trúc json API cập nhật trạng thái thanh toán : " + obj.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    objReturn["code"] = "04";
                    objReturn["message"] = "Lỗi tạo đơn hàng";
                    response.Data = objReturn;
                    return response;
                }
                string environment = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
                string strURLWEBPay = goBusinessCommon.Instance.GetConfigDB(goConstants.URL_WEB_PAY);
                //lấy thông tin giao dịch
                string[] paramQR = new string[] { data.paymentResult.order.orderCode, "TRANS_ID" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_info_getway, environment, paramQR);
                TransInfo trans = new TransInfo();
                if (response.Success)
                {
                    List<TransInfo> lsTran = goBusinessCommon.Instance.GetData<TransInfo>(response, 0);
                    if (lsTran != null && lsTran.Count > 0)
                    {
                        if (lsTran[0].STATUS.Equals("DONE"))
                        {
                            objReturn["code"] = "03";
                            objReturn["message"] = "Đơn hàng đã được thanh toán";
                            response.Data = objReturn;
                            return response;
                        }
                        else
                        {
                            trans = lsTran[0];
                            List<PayOrgModels> payOrg = new List<PayOrgModels>();
                            PayAssignModels assign = goLibPay.Instance.getConfigWebPayment(trans.ASSIGN_ID, ref payOrg);
                            //kiểm tra checksum code|msgType|txnId|qrTrace|bankCode|mobile|accountNo|amount|payDate|merchantCode|
                            string strMD5 = data.code + "|" + data.paymentResult.paymentType + "|" + data.paymentResult.order.orderCode + "|" + data.paymentResult.order.transactionNo + "|" + data.paymentResult.order.bankCode + "|" + data.paymentResult.order.mobile + "|"
                                + data.paymentResult.order.accountNo + "|" + data.paymentResult.order.amount + "|" + data.paymentResult.order.payDate + "|" + data.paymentResult.merchantId + "|" + assign.QR_PAY.SECRET;
                            if (!data.checksum.ToUpper().Equals(goEncryptBase.Instance.Md5Encode(strMD5)))
                            {
                                objReturn["code"] = "04";
                                objReturn["message"] = "Lỗi checksum";
                                response.Data = objReturn;
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Đơn hàng đã được thanh toán = " + strMD5 + "--" + goEncryptBase.Instance.Md5Encode(strMD5), Logger.ConcatName(nameClass, LoggerName));
                                return response;
                            }
                        }

                    }
                    else
                    {
                        objReturn["code"] = "04";
                        objReturn["message"] = "Lỗi tạo đơn hàng";
                        response.Data = objReturn;
                        return response;
                    }
                }
                else
                {
                    objReturn["code"] = "04";
                    objReturn["message"] = "Lỗi tạo đơn hàng";
                    response.Data = objReturn;
                    return response;
                }

                // thực thi cập nhật dữ liệu
                return updateTransPayment(trans, data, "QR", "VNPAY_QR");
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi IPN khi thanh toán  payment 475", ex.Message);
                response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_4004), goConstantsError.Instance.ERROR_4004);
                objReturn["message"] = "Lỗi cập nhật đơn hàng";
                response.Data = objReturn;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, "HDI", "ipnQRVnpay", obj, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            response.Data = objReturn;
            return response;
        }
        //IPN CTT VNPAY

        public BaseResponse ipnPaymentVNPAY(SortedList<String, String> obj, string abc)
        {
            //test git tesst master
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            BaseResponse response = new BaseResponse();
            //AuditLogsInfo auditLogs = new AuditLogsInfo();
            JObject objReturn = new JObject();
            objReturn.Add("RspCode", "00");
            objReturn.Add("Message", "Confirm Success");
            JObject jInput = new JObject();
            bool bCheckSign = false;
            try
            {
                response.Success = false;
                jInput = JObject.Parse(JsonConvert.SerializeObject(obj));
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "IPN web VNPAY " + jInput.ToString(), Logger.ConcatName(nameClass, LoggerName));
                List<PayOrgModels> lsOrg = goBusinessCache.Instance.Get<List<PayOrgModels>>("GET_PAY_ORG");
                if (lsOrg == null)
                {
                    goLibPay.Instance.getAllConfigPayOrg();
                }
                PayOrgModels org = lsOrg.Where(x => x.ORG_CODE == "VNPAY").FirstOrDefault();
                if (org == null)
                {
                    response.Data = getResponseVNPAY("01", "Order not found");
                    goBussinessEmail.Instance.sendEmaiCMS("", "pay controler ipnPaymentApi khong tim thay cau hinh thanh toan ", JsonConvert.SerializeObject(obj));
                    return response;
                }

                string strOrderId = "";

                VnPayLibrary vnpay = goLibPay.Instance.checkIpnVNPay(obj, org, ref bCheckSign);
                if (vnpay == null)
                {
                    response.Data = getResponseVNPAY("99", "Input data required");
                    goBussinessEmail.Instance.sendEmaiCMS("", "Input data required 170 ", JsonConvert.SerializeObject(obj));
                    return response;
                }
                if (!bCheckSign)
                {
                    response.Data = getResponseVNPAY("97", "Invalid signature");
                    goBussinessEmail.Instance.sendEmaiCMS("", "vnpay Invalid signature 170 ", JsonConvert.SerializeObject(obj));
                    return response;
                }
                string vnp_ResponseCode = vnpay.GetResponseData("vnp_ResponseCode");
                if (vnp_ResponseCode != "00")
                {
                    response.Data = objReturn;
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Khach hang bo giua chung ", Logger.ConcatName(nameClass, LoggerName));
                    return response;
                }
                strOrderId = vnpay.GetResponseData("vnp_TxnRef");
                // lấy thông tin giao dịch
                string[] paramQR = new string[] { strOrderId, "TRANS_ID" };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_get_info_getway, strEvm, paramQR);
                TransInfo trans = new TransInfo();
                if (response.Success)
                {
                    List<TransInfo> lsTran = goBusinessCommon.Instance.GetData<TransInfo>(response, 0);
                    if (lsTran != null && lsTran.Count > 0)
                    {
                        trans = lsTran[0];
                    }
                    else
                    {
                        response.Data = getResponseVNPAY("01", "Order not found");
                        goBussinessEmail.Instance.sendEmaiCMS("", "pay controler ipnPaymentApi khong tim thay giao dịch ", JsonConvert.SerializeObject(obj));
                        return response;
                    }
                }
                else
                {
                    response.Data = getResponseVNPAY("01", "Order not found");
                    goBussinessEmail.Instance.sendEmaiCMS("", "pay controler ipnPaymentApi lỗi pkg get trans ", JsonConvert.SerializeObject(obj));
                    return response;
                }
                if (!trans.TOTAL_AMOUNT.Equals(vnpay.GetResponseData("vnp_Amount").Substring(0, vnpay.GetResponseData("vnp_Amount").Length - 2)))
                {
                    response.Data = getResponseVNPAY("04", "Invalid amount");
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Invalid amount 189", Logger.ConcatName(nameClass, LoggerName));
                    return response;
                }
                jInput["vnp_Amount"] = trans.TOTAL_AMOUNT;
                if (trans.STATUS.Equals("DONE"))
                {
                    response.Data = getResponseVNPAY("02", "Order already confirmed");
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Order not found 182", Logger.ConcatName(nameClass, LoggerName));
                    return response;
                }

                //map dữ liệu 
                dataRequestOrderIpn data = vnpayPayment.Instance.convertDataIpnPayVnpay(vnpay, org, trans);
                response = updateTransPayment(trans, data, org.PAY_TYPE, org.ORG_CODE);
                if (response.Success)
                {
                    response.Data = objReturn;
                }
                else
                {
                    response.Data = getResponseVNPAY("01", "Order not found");
                }

            }
            catch (Exception)
            {
                response.Success = false;
                response.Data = objReturn;
            }



            return response;
        }
        public JObject getResponseVNPAY(string strValue1, string strValue2)
        {
            JObject objReturn = new JObject();
            objReturn["RspCode"] = strValue1;
            objReturn["Message"] = strValue2;
            return objReturn;
        }
        #endregion
    }
}
