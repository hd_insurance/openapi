﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Service;
using GO.CONSTANTS;
using GO.DTO.Payment;
using GO.ENCRYPTION;
using GO.HELPER.Utility;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace GO.BUSINESS.BusPayment
{
    public class hdbankPayment
    {
        #region Contructor
        private static readonly Lazy<hdbankPayment> InitInstance = new Lazy<hdbankPayment>(() => new hdbankPayment());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        public static hdbankPayment Instance => InitInstance.Value;
        #endregion

        public JObject createQr(string orderId, string amount, string payDate, string desc,
            string tipAndFee, PayOrgModels payOrg, PayConfigModels payConfigRef)
        {
            try
            {
                DateTime dt = goUtility.Instance.ConvertToDateTimePay(payDate, "yyMMddHHmm");// Convert.ToDateTime(payDate); // 25/11/2020 -> MM/dd/yyyy
                string strDatePay = dt.ToString("yyMMddHHmm");// "2011011000";//thời gian hết hạn thanh toán    yyMMddHHmm            
                RESULT_QR rs = createQrHDBank(orderId, amount, strDatePay, desc, tipAndFee, payOrg, payConfigRef);
                JObject obj = new JObject();
                if (rs.code == "00")
                {
                    string strQRBase64 = goBussinessPayment.Instance.getQrCode(rs.data, "Q", 8, "");
                    obj.Add("code", "000");
                    obj.Add("message", rs.message);
                    obj.Add("imageQR", strQRBase64);
                    obj.Add("dataQR", rs.data);
                    obj.Add("txnId", rs.idQrcode);
                }
                else
                {
                    obj.Add("code", rs.code);
                    obj.Add("message", rs.message);
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public RESULT_QR createQrHDBank(string orderId, string amount, string payDate, string desc,
            string tipAndFee, PayOrgModels payOrg, PayConfigModels payConfigRef)
        {
            try
            {
                CREATE_QR qr = new CREATE_QR();
                qr.appId = payConfigRef.CLIENT_ID.ToString();// "HDI";
                qr.masterMerCode = payConfigRef.MASTER_CODE.ToString();// "A000000775";
                qr.merchantName = payConfigRef.CLIENT_NAME.ToString();// "BH_HDI";
                qr.merchantCode = payConfigRef.MERCHANT_ID.ToString();// "HDI";
                qr.merchantType = "";// "";
                qr.terminalId = payConfigRef.TERMINAL_ID.ToString();
                qr.serviceCode = payConfigRef.SERVICE_CODE.ToString(); //"03";// "03";//mã dịch vụ cho QR               
                qr.countryCode = payConfigRef.COUNTRY_CODE.ToString(); //"VN";
                string _secretKey = payConfigRef.SECRET.ToString();

                qr.payType = payConfigRef.PAY_CODE.ToString(); //"03";//QR sử dụng để thanh toán
                qr.ccy = payConfigRef.CCY.ToString(); //"704";
                qr.productId = "";
                qr.txnId = orderId;
                qr.billNumber = orderId;
                qr.amount = amount;
                qr.expDate = payDate;
                qr.desc = desc;
                qr.tipAndFee = tipAndFee;

                qr.checksum = goEncryptBase.Instance.Md5Encode(qr.appId + "|" +
                        qr.merchantName + "|" + qr.serviceCode
                        + "|" + qr.countryCode + "|" +
                        qr.masterMerCode + "|" +
                        qr.merchantType + "|" +
                        qr.merchantCode + "|" + qr.terminalId
                        + "|" + qr.payType + "|" + qr.productId +
                        "|" + qr.txnId + "|" + qr.amount + "|" +
                        qr.tipAndFee + "|" + qr.ccy + "|" +
                        qr.expDate + "|" + _secretKey);
                JObject oJsonInput = JObject.FromObject(qr);
                PayFuncModels func = payOrg.PAY_FUNC.Where(x => x.TYPE_METHOD == "CREATE").FirstOrDefault();
                HttpResponseMessage rp = goLibPay.Instance.callApiQRCode(oJsonInput, func);
                RESULT_QR rs = goLibPay.Instance.callApi(rp);
                if (rs.code == "00")
                {
                    JObject jsonOut = JObject.Parse(rs.message);
                    if (jsonOut["code"].ToString().Equals("00"))
                    {
                        //xử lý data qr trả về                   
                        rs.message = jsonOut["message"].ToString();
                        rs.data = jsonOut["data"].ToString();
                        rs.checkSum = jsonOut["checksum"].ToString();
                        rs.idQrcode = jsonOut["idQrcode"].ToString();
                        //kiểm tra md5
                        string strMD5 = goEncryptBase.Instance.Md5Encode(rs.code + "|" + rs.message + "|" +
                        rs.data + "|" + qr.url + "|" + _secretKey);
                        if (!strMD5.Equals(rs.checkSum))
                        {
                            rs.code = "15";
                            rs.message = "Lỗi check sum dữ liệu HD";
                        }
                    }
                    else
                    {
                        rs.code = jsonOut["code"].ToString();
                    }

                }
                return rs;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
