﻿using GO.BUSINESS.Common;
using GO.BUSINESS.Service;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Payment;
using GO.SERVICE.CallApi;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GO.BUSINESS.BusPayment
{
    public class goLibPay
    {
        #region Contructor
        private static readonly Lazy<goLibPay> InitInstance = new Lazy<goLibPay>(() => new goLibPay());
        public string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
        public static goLibPay Instance => InitInstance.Value;
        #endregion

        string strKeyRedisAssign = "GET_PAY_ASSIGN";
        string strKeyRedisOrgPay = "GET_PAY_ORG";
        string strKeyRedisCallBack = "GET_PAY_CALLBACK";
        public PayAssignModels getConfigQR(string strOrgCode, string strProductCode, string strStructCode, string strChannel, ref PayOrgModels orgConfig, ref PayConfigModels payConfigQr)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                bool b= getAllConfigPayOrg();
                if (b)
                {
                    List<PayAssignModels> lsAssign = goBusinessCache.Instance.Get<List<PayAssignModels>>(strKeyRedisAssign);
                    List<PayOrgModels> lsOrg = goBusinessCache.Instance.Get<List<PayOrgModels>>(strKeyRedisOrgPay);

                    PayAssignModels assign = lsAssign.Where(x => x.ORG_SELLER == strOrgCode && (x.PRODUCT_CODE == "" || x.PRODUCT_CODE ==null || x.PRODUCT_CODE == strProductCode)
                                                            && (x.STRUCT_CODE == "" || x.STRUCT_CODE ==null || x.STRUCT_CODE == strStructCode) && (x.CHANNEL == strChannel)).FirstOrDefault();
                    payConfigQr = assign.QR_PAY;
                    orgConfig = lsOrg.Where(x => x.ORG_ID == assign.QR_PAY.ORG_ID).FirstOrDefault();                    
                    return assign;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                return null;
            }
        }
        public PayCallBackModels getCallBack(string strOrgCode, string strProductCode, string strStructCode, string strChannel, string strType)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                bool b = getAllConfigPayOrg();
                if (b)
                {
                    List<PayCallBackModels> lsCallback = goBusinessCache.Instance.Get<List<PayCallBackModels>>(strKeyRedisCallBack);
                    PayCallBackModels assign = lsCallback.Where(x => x.ORG_SELLER == strOrgCode && (x.PRODUCT_CODE == "" || x.PRODUCT_CODE == null || x.PRODUCT_CODE == strProductCode)
                                                            && (x.STRUCT_CODE == "" || x.STRUCT_CODE == null || x.STRUCT_CODE == strStructCode) && (x.CHANNEL == strChannel)
                                                            && x.TYPE_METHOD == strType).FirstOrDefault();                  
                    return assign;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                return null;
            }
        }
        public PayAssignModels getConfigWebPayment(string strAssignID,ref List<PayOrgModels> lsOrg)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                bool b = getAllConfigPayOrg();
                if (b)
                {
                    List<PayAssignModels> lsAssign = goBusinessCache.Instance.Get<List<PayAssignModels>>("GET_PAY_ASSIGN");
                    lsOrg = goBusinessCache.Instance.Get<List<PayOrgModels>>("GET_PAY_ORG");
                    PayAssignModels assign = lsAssign.Where(x => x.ID == strAssignID).FirstOrDefault();
                    return assign;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                return null;
            }
        }
        public PayConfigModels getConfigCreateUrlPay(string strOrgCode,string strAssignID, ref PayOrgModels lsOrgRef, ref string strTypePay, ref string strChannel)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                bool b = getAllConfigPayOrg();
                if (b)
                {
                    List<PayOrgModels> lsOrg = goBusinessCache.Instance.Get<List<PayOrgModels>>("GET_PAY_ORG");
                    List<PayAssignModels> lsAssign = goBusinessCache.Instance.Get<List<PayAssignModels>>("GET_PAY_ASSIGN");
                    PayOrgModels org = lsOrg.Where(x => x.ORG_CODE == strOrgCode).FirstOrDefault();
                    lsOrgRef = org;
                    PayConfigModels payConfig = new PayConfigModels();
                    PayAssignModels assign = lsAssign.Where(x => x.ID == strAssignID).FirstOrDefault();
                    payConfig = assign.PAY_CONFIG.Where(x => x.ORG_ID == org.ORG_ID).FirstOrDefault();
                    strChannel = assign.CHANNEL;
                    strTypePay = assign.PAYMENTS_TYPE;
                    return payConfig;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                return null;
            }
        }
        public bool getAllConfigPayOrg()
        {
            //load toàn bộ cấu hình thanh toán vào redis
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            try
            {
                // Start 2022-07-13 BY ThienTVB
                //List<PayAssignModels> config = new List<PayAssignModels>();
                //try
                //{
                //    config = goBusinessCache.Instance.Get<List<PayAssignModels>>(strKeyRedisAssign);
                //    if (config != null && config.Count > 0)
                //    {
                //        return true;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong lay duoc org_pay redis " + ex.Message, Logger.ConcatName(nameClass, LoggerName));
                //    config = null;
                //}
                //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Không Lưu Cache getAllConfigPayOrg", Logger.ConcatName(nameClass, LoggerName));
                // End 2022-07-13 BY ThienTVB
                string[] param = new string[] { strEvm };
                response = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.Pay_assign_config, strEvm, param);
                if (response.Success)
                {
                    List<PayAssignModels> lsAssign = goBusinessCommon.Instance.GetData<PayAssignModels>(response, 0);
                    List<PayOrgModels> lsOrg = goBusinessCommon.Instance.GetData<PayOrgModels>(response, 1);
                    List<PayFuncModels> lsFunc = goBusinessCommon.Instance.GetData<PayFuncModels>(response, 2);
                    List<PayCallBackModels> lsCallBack = goBusinessCommon.Instance.GetData<PayCallBackModels>(response, 3);
                    List<PayConfigModels> lsConfig = goBusinessCommon.Instance.GetData<PayConfigModels>(response, 4);
                    if (lsAssign != null && lsAssign.Any() && lsOrg != null && lsOrg.Any())
                    {
                        foreach(PayAssignModels payAssign in lsAssign)
                        {
                            List<string> lsOrgConfig = payAssign.BANK_ID.Split(';').ToList();
                            payAssign.QR_PAY = lsConfig.Where(x => x.BANK_ID == payAssign.QR_ID).FirstOrDefault();
                            payAssign.PAY_CONFIG = lsConfig.Where(x => lsOrgConfig.Contains(x.BANK_ID)).ToList();
                        }

                        foreach(PayOrgModels org in lsOrg)
                        {
                            org.PAY_FUNC = lsFunc.Where(x => x.ORG_ID == org.ORG_ID).ToList();
                            org.PAY_CONFIG = lsConfig.Where(x => x.ORG_ID==org.ORG_ID).ToList();
                        }
                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "strKeyRedisAssign" + JsonConvert.SerializeObject(lsAssign), Logger.ConcatName(nameClass, LoggerName));
                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "strKeyRedisOrgPay" + JsonConvert.SerializeObject(lsOrg), Logger.ConcatName(nameClass, LoggerName));
                        //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "strKeyRedisCallBack" + JsonConvert.SerializeObject(lsCallBack), Logger.ConcatName(nameClass, LoggerName));
                        bool b = goBusinessCache.Instance.Add(strKeyRedisAssign, lsAssign, true);
                        bool b1 = goBusinessCache.Instance.Add(strKeyRedisOrgPay, lsOrg, true);
                        bool b2 = goBusinessCache.Instance.Add(strKeyRedisCallBack, lsCallBack, true);
                        if (b && b1 && b2)
                        {
                            return true;
                        }
                    }
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong day duoc cau hinh don vi thanh toan vào redis", Logger.ConcatName(nameClass, LoggerName));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Lỗi lib pay redis 76 ", "Khong day duoc cau hinh don vi thanh toan vào redis");

                    return false;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "lib pay khong tim thay cau hinh thanh toan 81", Logger.ConcatName(nameClass, LoggerName));
                    return false;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "lib pay khong tim thay cau hinh thanh toan 90 ", ex.Message);
                return false;
            }
        }

        public VnPayLibrary checkIpnVNPay(SortedList<String, String> inputData, PayOrgModels org, ref bool bCheckSign)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strInput = JsonConvert.SerializeObject(inputData);
                VnPayLibrary vnpay = new VnPayLibrary();
                foreach (KeyValuePair<string, string> s in inputData)
                {
                    if (!string.IsNullOrEmpty(s.Key) && s.Key.StartsWith("vnp_"))
                    {
                        vnpay.AddResponseData(s.Key, s.Value);
                    }
                }
                String vnp_SecureHash = vnpay.GetResponseData("vnp_SecureHash");
                String vnp_ClientID = vnpay.GetResponseData("vnp_TmnCode");
                String sercret = org.PAY_CONFIG.Where(x => x.CLIENT_ID == vnp_ClientID).FirstOrDefault().SECRET;
                bCheckSign = vnpay.ValidateSignature(vnp_SecureHash, sercret);
                return vnpay;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, ex.Message, Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Loi checkResponsePay 279 ", ex.Message);
                return null;
            }
        }
        public string Encrypt(string toEncrypt)
        {
            string key = "HDinsur@nce@)@)";
            byte[] keyArray;
            bool useHashing = true;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock
                    (toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            string strReturn = Convert.ToBase64String(resultArray, 0, resultArray.Length);
            strReturn = strReturn.Replace("+", "cccc_11");
            strReturn = strReturn.Replace("/", "aaaa_11");
            strReturn = strReturn.Replace("=", "bbbb_11"); 
            return strReturn;
        }
        public string Decrypt(string cipherString)
        {
            string para = cipherString;
            para = para.Replace("cccc_11", "+");
            para = para.Replace("aaaa_11", "/");
            para = para.Replace("bbbb_11", "=");
            string key = "HDinsur@nce@)@)";
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(para);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock
                    (toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public bool callWebPayment(string orderId, string strUrlCallback, PayCallBackModels jApiConfig)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                Logger.Instance.WriteLog(Logger.TypeLog.INFO,"Call webpaymenthdi " + orderId+ strUrlCallback, Logger.ConcatName(nameClass, LoggerName));
                JObject oJsonInput = new JObject();
                oJsonInput.Add("id", orderId);
                oJsonInput.Add("status", 1);
                oJsonInput.Add("url_callback", strUrlCallback);
                PayFuncModels jApi = new PayFuncModels();
                jApi.METHOD = jApiConfig.METHOD;
                jApi.HEADER = jApiConfig.CONFIG;
                jApi.TYPE_BODY = jApiConfig.TYPE_BODY;
                jApi.TYPE_RAW = jApiConfig.TYPE_RAW;
                jApi.URL = jApiConfig.URL;
                HttpResponseMessage rp = callApiQRCode(oJsonInput, jApi);
                if (rp.IsSuccessStatusCode)
                {
                    string message = rp.Content.ReadAsStringAsync().Result;
                    if (message.IndexOf("true") > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
                //throw new Exception(ex.ToString());
            }
        }
        public HttpResponseMessage callApiQRCode(JObject jInput, PayFuncModels jApi)
        {
            try
            {
                goServiceConstants.MethodApi method = goServiceConstants.MethodApi.POST;
                switch (jApi.METHOD.ToString().ToUpper())
                {
                    case "GET":
                        {
                            method = goServiceConstants.MethodApi.GET;
                            break;
                        }
                    case "DELETE":
                        {
                            method = goServiceConstants.MethodApi.DELETE;
                            break;
                        }
                    case "PUT":
                        {
                            method = goServiceConstants.MethodApi.PUT;
                            break;
                        }
                }
                JArray jHeader = JArray.Parse(jApi.HEADER.ToString());
                List<JToken> lsJToken = jHeader.ToList();
                List<goServiceConstants.KeyVal> ls = new List<goServiceConstants.KeyVal>();
                foreach (JToken jT in lsJToken)
                {
                    JObject job = JObject.FromObject(jT);
                    goServiceConstants.KeyVal key = new goServiceConstants.KeyVal();
                    key.key = job["key"].ToString();
                    key.val = job["value"].ToString();
                    ls.Add(key);
                }
                goServiceConstants.TypeBody typebody = goServiceConstants.TypeBody.raw;
                switch (jApi.TYPE_BODY.ToString().ToUpper())
                {
                    case "FORM_DATA":
                        {
                            typebody = goServiceConstants.TypeBody.form_data;
                            break;
                        }
                    case "FORM_URLENDCODED":
                        {
                            typebody = goServiceConstants.TypeBody.form_urlendcoded;
                            break;
                        }
                    case "NONE":
                        {
                            typebody = goServiceConstants.TypeBody.none;
                            break;
                        }
                }
                goServiceConstants.TypeRaw text = goServiceConstants.TypeRaw.json;
                switch (jApi.TYPE_RAW.ToString().ToUpper())
                {
                    case "HTML":
                        {
                            text = goServiceConstants.TypeRaw.html;
                            break;
                        }
                    case "JAVASCRIPT":
                        {
                            text = goServiceConstants.TypeRaw.javascript;
                            break;
                        }
                    case "NONE":
                        {
                            text = goServiceConstants.TypeRaw.none;
                            break;
                        }
                    case "TEXT":
                        {
                            text = goServiceConstants.TypeRaw.text;
                            break;
                        }
                    case "XML":
                        {
                            text = goServiceConstants.TypeRaw.xml;
                            break;
                        }
                }
                HttpResponseMessage rp = new goServiceInvoke().Invoke(jApi.URL.ToString(), method, typebody, text, ls, string.Empty, jInput).Result;
                return rp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public RESULT_QR callApi(HttpResponseMessage response)
        {
            RESULT_QR result = new RESULT_QR();
            try
            {
                string message = response.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                if (response.IsSuccessStatusCode && parsedString.Length > 5)
                {
                    result.code = "00";
                    result.message = parsedString;
                }
                else
                {
                    var details = JObject.Parse(parsedString);
                    result.code = details["code"].ToString();
                    result.message = details["message"].ToString();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        public string getUrlPay(TransInfo trans, PayOrgModels org, PayConfigModels config,string strOrgCode, string strChannel, string strUrlCallBack,string strTypePay, string strLang)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            try
            {
                string strUrlPay = "";
                List<TransDetail> lsTransD = new List<TransDetail>();
                switch (strOrgCode)
                {
                    case "VNPAY":
                        {
                            strUrlPay = vnpayPayment.Instance.vnpay_CreateUrlPay(trans, org, config, strChannel,strTypePay, strLang);
                            TransDetail transDT = new TransDetail();
                            transDT.ORG_PAY_CODE = strOrgCode;
                            transDT.STATUS = "CREATE";
                            transDT.TRANSACTION_ID = trans.TRANSACTION_ID;
                            transDT.URL_PAY = strUrlPay;
                            transDT.URL_CALLBACK = strUrlCallBack;
                            lsTransD.Add(transDT);
                            break;
                        }
                }
                //insert detail transaction

                JObject jInputDb = new JObject();
                jInputDb.Add("transaction_id", trans.TRANSACTION_ID);
                jInputDb.Add("p_PM_TRANSFER_INFOR", JsonConvert.SerializeObject(lsTransD));
                BaseResponse response = dbJsonObjectParam(jInputDb, goConstantsProcedure.Pay_Insert_DetailTrans);
                if (response == null || !response.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "khong insert duoc pay trans detail" + jInputDb.ToString(), Logger.ConcatName(nameClass, LoggerName));
                }
                return strUrlPay;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public BaseResponse dbJsonObjectParam(JObject jInput, string strAction)
        {
            string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
            BaseResponse response = new BaseResponse();
            try
            {
                List<string> lsParam = new List<string>();
                var properties = jInput.Properties();
                foreach (var property in properties)
                {
                    lsParam.Add(property.Value.ToString());
                }
                response = goBusinessCommon.Instance.executeProHDI(strAction, strEvm, lsParam.ToArray());
                if (response.Success)
                {
                    return response;
                }
                else
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong insert được dữ liệu" + jInput.ToString(), Logger.ConcatName(nameClass, LoggerName));
                    goBussinessEmail.Instance.sendEmaiCMS("", "Khong insert được dữ liệu 401 ", "action strAction " + strAction + jInput.ToString());
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Khong insert được dữ liệu " + ex.Message + jInput.ToString(), Logger.ConcatName(nameClass, LoggerName));
                goBussinessEmail.Instance.sendEmaiCMS("", "Khong insert được dữ liệu 407 ", "action strAction " + strAction + jInput.ToString() + ex.Message);
                return null;
            }
        }

        //public void checkCallIPNPartner(string strOrg, string strOderid, JObject transaction_amount, string reference_id, JObject vat)
        //{
        //    string LoggerName = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        //    try
        //    {
        //        string strUrlIpn = "";
        //        string strSecret = getLink_CallBack(strOrg, ref strUrlIpn);
        //        string strToken = "";
        //        //strToken = GenerateJWToken(jInput.ToString(), "aGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFoZGlfand0XzIwMjFfaGRpX2p3dF8yMDIxX2hkaV9qd3RfMjAyMWhkaV9qd3RfMjAyMV9oZGlfand0XzIwMjFfaGRpX2p3dF8yMDIx");
        //        //strToken = goEncryptMix.Instance.encryptMd5HmacSha256(jInput.ToString(), strSecret);
        //        strToken = GenerateJWToken(strOderid, transaction_amount.ToString(), strSecret, reference_id, vat.ToString());
        //        JObject jobjIpn = new JObject();
        //        jobjIpn.Add("order_id", strOderid);
        //        jobjIpn.Add("transaction_amount", transaction_amount);
        //        jobjIpn.Add("reference_id", reference_id);
        //        jobjIpn.Add("vat", vat);
        //        HttpResponseMessage response = postJsonAPI(jobjIpn, strUrlIpn + strToken, null);
        //        string message = response.Content.ReadAsStringAsync().Result;
        //        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ipn creadify " + message, Logger.ConcatName(nameClass, LoggerName));
        //        string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
        //        if (!(parsedString.IndexOf("success") > 1))
        //        {
        //            goBussinessEmail.Instance.sendEmaiCMS("", "Loi goi API cap nhạt Hoang ", parsedString + "-----------" + jobjIpn.ToString());
        //        }
        //        Logger.Instance.WriteLog(Logger.TypeLog.INFO, parsedString, Logger.ConcatName(nameClass, LoggerName));
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "loi ipn creadify " + ex.Message, Logger.ConcatName(nameClass, LoggerName));

        //    }
        //}
        public HttpResponseMessage callApiAll(JObject jInput, PayCallBackModels jApi, JArray jHeader)
        {
            try
            {
                goServiceConstants.MethodApi method = goServiceConstants.MethodApi.POST;
                switch (jApi.METHOD.ToString().ToUpper())
                {
                    case "GET":
                        {
                            method = goServiceConstants.MethodApi.GET;
                            break;
                        }
                    case "DELETE":
                        {
                            method = goServiceConstants.MethodApi.DELETE;
                            break;
                        }
                    case "PUT":
                        {
                            method = goServiceConstants.MethodApi.PUT;
                            break;
                        }
                }
                List<JToken> lsJToken = jHeader.ToList();
                List<goServiceConstants.KeyVal> ls = new List<goServiceConstants.KeyVal>();
                foreach (JToken jT in lsJToken)
                {
                    JObject job = JObject.FromObject(jT);
                    goServiceConstants.KeyVal key = new goServiceConstants.KeyVal();
                    key.key = job["key"].ToString();
                    key.val = job["value"].ToString();
                    ls.Add(key);
                }
                goServiceConstants.TypeBody typebody = goServiceConstants.TypeBody.raw;
                switch (jApi.TYPE_BODY.ToString().ToUpper())
                {
                    case "FORM_DATA":
                        {
                            typebody = goServiceConstants.TypeBody.form_data;
                            break;
                        }
                    case "FORM_URLENDCODED":
                        {
                            typebody = goServiceConstants.TypeBody.form_urlendcoded;
                            break;
                        }
                    case "NONE":
                        {
                            typebody = goServiceConstants.TypeBody.none;
                            break;
                        }
                }
                goServiceConstants.TypeRaw text = goServiceConstants.TypeRaw.json;
                switch (jApi.TYPE_RAW.ToString().ToUpper())
                {
                    case "HTML":
                        {
                            text = goServiceConstants.TypeRaw.html;
                            break;
                        }
                    case "JAVASCRIPT":
                        {
                            text = goServiceConstants.TypeRaw.javascript;
                            break;
                        }
                    case "NONE":
                        {
                            text = goServiceConstants.TypeRaw.none;
                            break;
                        }
                    case "TEXT":
                        {
                            text = goServiceConstants.TypeRaw.text;
                            break;
                        }
                    case "XML":
                        {
                            text = goServiceConstants.TypeRaw.xml;
                            break;
                        }
                }
                HttpResponseMessage rp = new goServiceInvoke().Invoke(jApi.URL.ToString(), method, typebody, text, ls, string.Empty, jInput).Result;
                return rp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
