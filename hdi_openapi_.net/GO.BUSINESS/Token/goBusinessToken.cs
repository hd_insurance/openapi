﻿using GO.BUSINESS.Common;
using GO.COMMON.Cache;
using GO.CONSTANTS;
using GO.DTO.Base;
using GO.DTO.Common;
using GO.ENCRYPTION;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GO.ENCRYPTION.goEncryptBase;

namespace GO.BUSINESS.Token
{
    public class goBusinessToken
    {
        #region Variable

        internal string strA = "aa<!document>html<script>adsfbcasc";
        internal string strB = "a<hdi>html<script>abcsaaasc</hdi>";
        internal string strC = "abc<brid>html<script>abcadsfdssc</brid>";
        #endregion

        #region Contructor
        private static readonly Lazy<goBusinessToken> InitInstance = new Lazy<goBusinessToken>(() => new goBusinessToken());
        public static goBusinessToken Instance => InitInstance.Value;
        #endregion

        #region Method

        public void ValidTokenAuthorization(ref TokenAuthorizationInfo result, BaseRequest request, string secret, string token, bool refeshToken)
        {
            if (result == null) result = new TokenAuthorizationInfo();
            try
            {
                if (!IsValidToken(request?.Action?.UserName, secret, token, request.Device.DeviceEnvironment, refeshToken))
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2004);
                }
            }
            catch (Exception ex)
            {
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2004);
            }
        }

        public string generateToken(string userName, string passWord, string secret)
        {
            string plainText = strA + userName + passWord + strB + secret + strC + DateTime.Now.Ticks;
            string cipherText = goEncryptMix.Instance.encryptAesMd5(plainText);
            return cipherText;
        }

        public bool IsValidToken(string userName, string secret, string authToken, string deviceEnvironment, bool refeshToken)
        {
            string key = userName + secret + deviceEnvironment, token = "";

            if (goBusinessCache.Instance.Exists(key))
            {
                token = goBusinessCache.Instance.Get<string>(key);
            }

            //COMMON.Log.Logger.Instance.WriteLog(COMMON.Log.Logger.TypeLog.DEGBUG, "key: " + key + ". token cache: " + token + ". token client: " +authToken, "IsValidToken");

            if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(authToken) && token == authToken)
            {
                ///COMMON.Log.Logger.Instance.WriteLog(COMMON.Log.Logger.TypeLog.DEGBUG, "status true" + "key: " + key + ". token cache: " + token + ". token client: " + authToken, "IsValidToken");

                if (refeshToken)
                {
                    goBusinessCache.Instance.Add<string>(key, authToken, goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCacheToken), 0));
                }
                return true;
            }
            //COMMON.Log.Logger.Instance.WriteLog(COMMON.Log.Logger.TypeLog.DEGBUG, "status false" + "key: " + key + ". token cache: " + token + ". token client: " + authToken, "IsValidToken");
            return false;
        }

        #region MyRegion
        public string generateTokenSSO(string channel, string userName, string passWord, string secret)
        {
            string plainText = strA + userName + channel + passWord + strB + secret + strC + DateTime.Now.Ticks;
            string cipherText = goEncryptMix.Instance.encryptAesMd5(plainText);
            return cipherText;
        }

        public bool IsValidTokenSSO(string chanel, string userName, string secret, string authToken, string deviceEnvironment, bool refeshToken)
        {
            string key = "SSO_" + chanel + userName + secret + deviceEnvironment, token = "";
            if (goBusinessCache.Instance.Exists(key))
            {
                token = goBusinessCache.Instance.Get<string>(key);
            }
            if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(authToken) && token == authToken)
            {
                if (refeshToken)
                {
                    goBusinessCache.Instance.Add<string>(key, authToken, goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCacheToken), 0));
                }
                return true;
            }
            return false;
        }
        #endregion
        #endregion
    }
}
