﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.DTO.Orders;
using GO.BUSINESS.Common;
using GO.HELPER.Utility;
using GO.COMMON.Log;
using GO.DTO.SystemModels;
using GO.BUSINESS.Action;
using GO.BUSINESS.ServiceAccess;
using GO.DTO.Common;
using GO.DTO.Insurance;
using GO.DTO.Centech.Loans;
using GO.DTO.Centech;
using Newtonsoft.Json.Linq;
using GO.BUSINESS.Service;
using GO.DTO.Voucher;
using GO.DTO.Insurance.Common;
using GO.DTO.InsurRegister;
using GO.DTO.Org.Hdi;
using GO.DTO.Org.Vietjet;
using GO.DTO.Payment;
using GO.DTO.CalculateFees;
using GO.DTO.Service;
using GO.DTO.CommonBusiness;
using System.Globalization;
using GO.DTO.Insurance.BANK_LO;
using GO.BUSINESS.BusPayment;
using GO.DTO.Insurance.House;
using GO.DTO.Insurance.Travel;
using DnsClient.Protocol;
using System.Net.Http;
using System.Text.RegularExpressions;
using GO.HELPER.Config;
using static GO.DTO.Payment.QRCodeModels;
using System.Xml.Linq;
using GO.ENCRYPTION;
using static GO.CONSTANTS.goConstants;
using System.Net.PeerToPeer;
using System.Net.Sockets;
using Renci.SshNet.Messages;
using Org.BouncyCastle.Utilities.Encoders;

namespace GO.BUSINESS.Orders
{
    public class goBusinessOrders
    {

        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        public static string pathRoot = goUtility.Instance.GetPathProject();
        public string strPathTemplate = pathRoot + @"/Template/";
        #endregion
        #region Contructor
        private static readonly Lazy<goBusinessOrders> _instance = new Lazy<goBusinessOrders>(() =>
        {
            return new goBusinessOrders();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goBusinessOrders Instance { get => _instance.Value; }
        #endregion
        #region Method
        // sản phẩm qua selling qua đây
        public BaseResponse GetBaseResponse_Orders_Create(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrdersModel ordersModel = new OrdersModel();

                if (request.Data != null)
                    ordersModel = JsonConvert.DeserializeObject<OrdersModel>(JsonConvert.SerializeObject(request.Data));

                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }

                // lưu all Order
                response = SaveOrderBase(request, ordersModel, config);
                if (response.Success)
                {
                    try
                    {
                        OrderResInfo resInfoOrder = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                        // send mail
                        try
                        {
                            if (resInfoOrder != null && resInfoOrder.status != null && resInfoOrder.status.Equals(nameof(goConstants.OrderStatus.PAID)))
                            {
                                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);
                                //send mail GCN theo insurDetails                            
                                //BaseResponse res = signEmail(insurDetails[0].CERTIFICATE_NO, "", insurDetails[0].EMAIL, insurDetails[0].CERTIFICATE_NO, insurDetails[0].PACK_CODE);
                                //if (res.Success)
                                //{
                                //    response.Data = res.Data;
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "send mail: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                        }

                        // Chuẩn hóa data trả về
                        List<InsurContract> lstContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 8);
                        List<InsurDetail> lstDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 9);
                        List<ResInsurData> dataOut = ResGetData_V2(lstContract, lstDetails);
                        response.Data = dataOut;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            return response;
        }

        // sản phẩm qua selling qua đây
        public BaseResponse GetBaseResponse_Orders_CreateAndPay(BaseRequest request, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrdersModel ordersModel = new OrdersModel();

                if (request.Data != null)
                    ordersModel = JsonConvert.DeserializeObject<OrdersModel>(JsonConvert.SerializeObject(request.Data));

                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                string payment_type = ordersModel.COMMON?.PAY_INFO?.PAYMENT_TYPE;
                //
                if (string.IsNullOrEmpty(payment_type))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6009), goConstantsError.Instance.ERROR_6009);
                    return response;
                }

                if (payment_type != null && payment_type.Equals(nameof(goConstants.Payment_type.TH)))
                    ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();

                // lưu all Order
                response = SaveOrderBase(request, ordersModel, config);
                if (response.Success)
                {                    
                    bool isSms = goUtility.Instance.CastToBoolean(ordersModel.COMMON?.PAY_INFO?.IS_SMS);
                    bool isEmail = goUtility.Instance.CastToBoolean(ordersModel.COMMON?.PAY_INFO?.IS_EMAIL);
                    // Bước 5: Lấy thông tin thanh toán
                    OrderResInfo orderResInfo = goBusinessCommon.Instance.GetData<OrderResInfo>(response, 0).FirstOrDefault();
                    if (orderResInfo != null && orderResInfo.status != null && orderResInfo.status.Equals(nameof(goConstants.OrderStatus.PAID))) // nêu đơn thu hộ thì ký số gửi mail
                    {
                        //InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                        List<InsurContract> lstContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2);
                        List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);

                        List<CustomerLuggagesVJC> lstLuggages = new List<CustomerLuggagesVJC>();
                        if (lstContract != null && lstContract.Any())
                        {
                            switch (lstContract[0].PRODUCT_CODE)
                            {
                                case (nameof(goConstants.ProducInsur.LOST_BAGGAGE)):
                                    lstLuggages = goBusinessCommon.Instance.GetData<CustomerLuggagesVJC>(response, 4);
                                    break;
                            }
                            // Start 2022-08-18 By ThienTVB
                            try
                            {
                                if (ordersModel.COMMON.SELLER.ORG_CODE == "HDBANK_VN" && ordersModel.COMMON.SELLER.SELLER_EMAIL != null)
                                {
                                    lstContract[0].SELLER_MAIL = ordersModel.COMMON.SELLER.SELLER_EMAIL;
                                }
                                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Email" + lstContract[0].SELLER_MAIL, Logger.ConcatName(nameClass, nameMethod));
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "ORG_CODE: NULL" + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                            }
                            // End 2022-08-18 By ThienTVB
                        }
                        OrderCreateAndPay(ref response, lstContract, insurDetails, isTest, isEmailTest, isSMSTest, lstLuggages);

                        //KhanhPT 20230907
                        //Neu la Kenh mua hang tren Web, tien hanh CallBack lai don vi
                        if(response.Success)
                        {
                            if (ordersModel.COMMON.SELLER.ORG_CODE == goConstants.HDI_E_COM)
                            {
                                new Task(() => {
                                    //CallBack Website IPN
                                    String env = ordersModel.COMMON.ORD_PARTNER.ENV;
                                    String urlCallBack = goConfig.Instance.GetAppConfig(goConstants.E_COM_CALLBACK + "_" + env);
                                    String hashkey = goConfig.Instance.GetAppConfig(goConstants.E_COM_HASHKEY + "_" + env);
                                    String id = ordersModel.COMMON.ORD_PARTNER.TRANS_ID;
                                    
                                    urlCallBack = urlCallBack + "/" + id;

                                    JArray arrData = JArray.FromObject(response.Data);
                                    List<JToken> lstData = arrData.ToList();
                                    JObject objPut = new JObject();                                    

                                    var txtData = JsonConvert.SerializeObject(goBusinessCommon.Instance.RemoveEmptyChildren(lstData[0]));
                                    String objInput = goEncryptBase.Instance.Base64Endcode(txtData.ToString());

                                    String txtDataBase64 = goEncryptBase.Instance.Base64Endcode(txtData.ToString());

                                    if (txtDataBase64.Length > 5000)
                                    {
                                        txtDataBase64 = txtDataBase64.Substring(0, 5000);
                                    }
                                    
                                    string data = txtDataBase64 + hashkey;
                                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Data SHA " + data, Logger.ConcatName(nameClass, nameMethod));
                                    String sign = goEncryptMix.Instance.encryptSha256(data, "");
                                    objPut.Add("data", objInput);
                                    objPut.Add("signature", sign);

                                    eCommerce_CallBack(objPut, urlCallBack, "");
                                }).Start();
                            }
                        }                            
                        //End

                        #region Old
                        /*
                        //send mail GCN theo insurDetails                            
                        BaseResponse res = goBusinessServices.Instance.signAndSendMail(insurContract, insurDetails, "CreateAndPay","WEB","NO");
                        if (res.Success)
                        {
                            try
                            {
                                List<SignFileReturnModel> lsFile = JsonConvert.DeserializeObject<List<SignFileReturnModel>>(JsonConvert.SerializeObject(res.Data));
                                foreach (SignFileReturnModel returnFile in lsFile)
                                {
                                    foreach (InsurDetail detail in insurDetails)
                                    {
                                        if (returnFile.FILE_REF.Equals(detail.CERTIFICATE_NO))
                                        {
                                            detail.URL_GCN = returnFile.FILE_URL;
                                            break;
                                        }
                                    }
                                }
                                //response.Data = res.Data;
                            }
                            catch (Exception ex)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi file ký số null : ", Logger.ConcatName(nameClass, nameMethod));
                            }
                        }
                        else
                        {
                            //response.Success = false;
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));
                            //goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails[0]));
                        }
                        // tạo response data trả về
                        List<InsurContract> lstContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2);
                        //List<InsurDetail> lstDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 3);
                        List<ResInsurData> dataOut = ResGetData_V2(lstContract, insurDetails);
                        response.Data = dataOut;
                        //ResInsurData dataOut = goBusinessOrders.Instance.ResGetData(insurContract, insurDetails);
                        //response.Data = dataOut;
                        */
                        #endregion
                    }
                    else
                    {
                        BaseResponse resOrdTrans = GetOrderTrans(request, orderResInfo.order_code, orderResInfo.str_period, orderResInfo.total_pay, payment_type, ordersModel.COMMON?.PAY_INFO?.PAYMENT_MIX, null);
                        response = GetResOrdTrans(resOrdTrans, request, config, payment_type, isSms, isEmail);

                        #region Old
                        //List<OrderTransInfo> lstOrdTrans = new List<OrderTransInfo>();
                        //List<OrdersInfo> lstOrd = new List<OrdersInfo>();
                        //List<OrderDetailInfo> lstOrdDetail = new List<OrderDetailInfo>();
                        //if (resOrdTrans.Success)
                        //{
                        //    lstOrdTrans = goBusinessCommon.Instance.GetData<OrderTransInfo>(resOrdTrans, 0);
                        //    lstOrd = goBusinessCommon.Instance.GetData<OrdersInfo>(resOrdTrans, 6);
                        //    lstOrdDetail = goBusinessCommon.Instance.GetData<OrderDetailInfo>(resOrdTrans, 7);
                        //    goBusinessOrders.Instance.GetResActionPayment(ref response, resOrdTrans, request, config, payment_type, lstOrdTrans, lstOrd, lstOrdDetail, isSms, isEmail);
                        //}
                        //else
                        //    response = resOrdTrans;
                        //BaseRequest request_pay = goBusinessOrders.Instance.GetBaseRequest_OrderPay(request, orderResInfo.order_code, orderResInfo.str_period, orderResInfo.total_pay, null);
                        //// Bước 6: Gọi qua thanh toán
                        ///response = goBussinessPayment.Instance.payment(request_pay);
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            return response;
        }

        private void eCommerce_CallBack(JObject jData, String url_CallBack, String strToken)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                //Thực hiện đẩy dữ liệu                                                
                //Neu API su dung authen "Bearer " + strToken
                JObject jHeaderData = new JObject();
                //jHeaderData.Add("Authorization", "Bearer " + strToken);
                //End
                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "URL CallBack " + url_CallBack + "Data" + jData.ToString(), Logger.ConcatName(nameClass, nameMethod));
                HttpResponseMessage response1 = goBusinessCommon.Instance.putJsonAPI(jData, url_CallBack, jHeaderData);
                string message = response1.Content.ReadAsStringAsync().Result;
                string parsedString = Regex.Unescape(message.Replace("\\\"", ""));
                JObject jobReturn = JObject.Parse(parsedString);
                string strKq = jobReturn["data"]?.ToString();

                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "CallBack " + url_CallBack + "Data" + jData.ToString() + strKq + "content " + message, Logger.ConcatName(nameClass, nameMethod));
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
            }
        }

        public BaseResponse GetBaseResponse_Orders_CreateAndPay_Preview(BaseRequest request, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrdersModel ordersModel = new OrdersModel();

                if (request.Data != null)
                    ordersModel = JsonConvert.DeserializeObject<OrdersModel>(JsonConvert.SerializeObject(request.Data));

                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                string payment_type = ordersModel.COMMON?.PAY_INFO?.PAYMENT_TYPE;
                //
                if (string.IsNullOrEmpty(payment_type))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6009), goConstantsError.Instance.ERROR_6009);
                    return response;
                }

                if (payment_type != null && payment_type.Equals(nameof(goConstants.Payment_type.TH)))
                    ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();

                // lưu all Order
                response = SaveOrderBase_Preview(request, ordersModel, config);
                if (response.Success)
                {                    
                    Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, response.ToString(), Logger.ConcatName(nameClass, nameMethod));
                    //InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 2).FirstOrDefault();
                    List<JObject> lstContract = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    List<JObject> insurDetails = goBusinessCommon.Instance.GetData<JObject>(response, 1);

                    

                    List<CustomerLuggagesVJC> lstLuggages = new List<CustomerLuggagesVJC>();
                    if (lstContract != null && lstContract.Any())
                    {
                        String strProductCode = lstContract[0]["PRODUCT_CODE"]?.ToString();
                        String strSELLER_MAIL = lstContract[0]["SELLER_MAIL"]?.ToString();
                        switch (strProductCode)
                        {
                            case (nameof(goConstants.ProducInsur.LOST_BAGGAGE)):
                                lstLuggages = goBusinessCommon.Instance.GetData<CustomerLuggagesVJC>(response, 4);
                                break;
                        }
                        // Start 2022-08-18 By ThienTVB
                        try
                        {
                            if (ordersModel.COMMON.SELLER.ORG_CODE == "HDBANK_VN" && ordersModel.COMMON.SELLER.SELLER_EMAIL != null)
                            {
                                strSELLER_MAIL = ordersModel.COMMON.SELLER.SELLER_EMAIL;
                            }
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "Email" + strSELLER_MAIL, Logger.ConcatName(nameClass, nameMethod));
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "ORG_CODE: NULL" + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                        }
                        // End 2022-08-18 By ThienTVB
                    }
                    OrderCreateAndPay_Preview(ref response, lstContract, insurDetails, isTest, isEmailTest, isSMSTest, lstLuggages);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            return response;
        }

        public BaseResponse GetBaseResponse_Orders_Import(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                OrdersModel ordersModel = new OrdersModel();

                if (request.Data != null)
                    ordersModel = JsonConvert.DeserializeObject<OrdersModel>(JsonConvert.SerializeObject(request.Data));

                if (ordersModel == null || ordersModel.COMMON == null || ordersModel.COMMON.BUYER == null || ordersModel.COMMON.ORDER == null || ordersModel.COMMON.ORDER_DETAIL == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    return response;
                }
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "json: " + JsonConvert.SerializeObject(request), Logger.ConcatName(nameClass, nameMethod));

                string payment_type = ordersModel.COMMON?.PAY_INFO?.PAYMENT_TYPE;
                //
                if (string.IsNullOrEmpty(payment_type))
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6009), goConstantsError.Instance.ERROR_6009);
                    return response;
                }

                if (payment_type != null && payment_type.Equals(nameof(goConstants.Payment_type.TH)))
                    ordersModel.COMMON.ORDER.STATUS = goConstants.OrderStatus.PAID.ToString();

                // lưu all Order
                response = SaveOrderBase(request, ordersModel, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            return response;
        }

        // các sp chạy job thì qua đây
        public BaseResponse SaveOrderBase(BaseRequest request, OrdersModel ordersModel, ResponseConfig config)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse response = new BaseResponse();
                //object[] Param = GetParamsOrdersSaveAll(ordersModel);
                //ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.SaveOrderObject, enviromentCode, Param);
                //response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), config, null, null);

                object Param = GetParamsOrdersSaveAll_new(ordersModel);
                BaseRequest requestOrder = new BaseRequest();
                requestOrder.Device = request.Device;
                requestOrder.Action = request.Action;
                requestOrder.Action.ActionCode = goConstantsProcedure.SaveOrderObject;
                requestOrder.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(Param));
                goBusinessCommon.Instance.GetSignRequest(ref requestOrder, config);
                response = goBusinessAction.Instance.GetBaseResponse(requestOrder);
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public BaseResponse SaveOrderBase_Preview(BaseRequest request, OrdersModel ordersModel, ResponseConfig config)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse response = new BaseResponse();
                //object[] Param = GetParamsOrdersSaveAll(ordersModel);
                //ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.SaveOrderObject, enviromentCode, Param);
                //response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, Param), config, null, null);

                object Param = GetParamsOrdersSaveAll_new(ordersModel);
                BaseRequest requestOrder = new BaseRequest();
                requestOrder.Device = request.Device;
                requestOrder.Action = request.Action;
                requestOrder.Action.ActionCode = goConstantsProcedure.SaveOrderObject_Preview;
                requestOrder.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(Param));
                goBusinessCommon.Instance.GetSignRequest(ref requestOrder, config);
                response = goBusinessAction.Instance.GetBaseResponse(requestOrder);
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public object GetParamsOrdersSaveAll_new(OrdersModel ordersModel)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (ordersModel.COMMON.SELLER == null)
                ordersModel.COMMON.SELLER = new ORD_SELLER();
            if (ordersModel.COMMON.RECEIVER == null)
                ordersModel.COMMON.RECEIVER = new ORD_RECEIVER();
            if (ordersModel.COMMON.PAY_INFO == null)
                ordersModel.COMMON.PAY_INFO = new ORD_PAYINFO();
            try
            {
                string strJsonBusiness = "";
                if (ordersModel.BUSINESS != null)
                    strJsonBusiness = JsonConvert.SerializeObject(ordersModel.BUSINESS);

                object param = new
                {
                    Channel = ordersModel.Channel,
                    UserName = ordersModel.UserName,

                    SELLER_CODE = ordersModel.COMMON.SELLER.SELLER_CODE,
                    SELLER_NAME = ordersModel.COMMON.SELLER.SELLER_NAME,
                    SELLER_EMAIL = ordersModel.COMMON.SELLER.SELLER_EMAIL,
                    SELLER_PHONE = ordersModel.COMMON.SELLER.SELLER_PHONE,
                    SELLER_GENDER = ordersModel.COMMON.SELLER.SELLER_GENDER,
                    ORG_CODE = ordersModel.COMMON.SELLER.ORG_CODE,
                    ORG_TRAFFIC = ordersModel.COMMON.SELLER.ORG_TRAFFIC,
                    TRAFFIC_LINK = ordersModel.COMMON.SELLER.TRAFFIC_LINK,
                    ENVIROMENT = ordersModel.COMMON.SELLER.ENVIROMENT,

                    //ID = ordersModel.COMMON.BUYER.CUS_ID,
                    //TYPE = ordersModel.COMMON.BUYER.TYPE,
                    //NAME = ordersModel.COMMON.BUYER.NAME,
                    //DOB = ordersModel.COMMON.BUYER.DOB,
                    //GENDER = ordersModel.COMMON.BUYER.GENDER,
                    //IDCARD = ordersModel.COMMON.BUYER.IDCARD,
                    //IDCARD_D = ordersModel.COMMON.BUYER.IDCARD_D,
                    //IDCARD_P = ordersModel.COMMON.BUYER.IDCARD_P,
                    //PROVINCE = ordersModel.COMMON.BUYER.PROV,
                    //DISTRICT = ordersModel.COMMON.BUYER.DIST,
                    //WARDS = ordersModel.COMMON.BUYER.WARDS,
                    //ADDRESS = ordersModel.COMMON.BUYER.ADDRESS,
                    //EMAIL = ordersModel.COMMON.BUYER.EMAIL,
                    //PHONE = ordersModel.COMMON.BUYER.PHONE,
                    //FAX = ordersModel.COMMON.BUYER.FAX,
                    //TAXCODE = ordersModel.COMMON.BUYER.TAXCODE,

                    strBuyer = JsonConvert.SerializeObject(ordersModel.COMMON.BUYER),

                    str_RECEIVER = JsonConvert.SerializeObject(ordersModel.COMMON.RECEIVER),

                    ORDER_CODE = ordersModel.COMMON.ORDER.ORDER_CODE,
                    FIELD = ordersModel.COMMON.ORDER.FIELD,
                    ORDER_TYPE = ordersModel.COMMON.ORDER.TYPE,
                    TITLE = ordersModel.COMMON.ORDER.TITLE,
                    SUMMARY = ordersModel.COMMON.ORDER.SUMMARY,
                    AMOUNT = ordersModel.COMMON.ORDER.AMOUNT,
                    DISCOUNT = ordersModel.COMMON.ORDER.DISCOUNT,
                    DISCOUNT_UNIT = ordersModel.COMMON.ORDER.DISCOUNT_UNIT,
                    VAT = ordersModel.COMMON.ORDER.VAT,
                    TOTAL_AMOUNT = ordersModel.COMMON.ORDER.TOTAL_AMOUNT,
                    CURRENCY = ordersModel.COMMON.ORDER.CURRENCY,
                    GIF_CODE = ordersModel.COMMON.ORDER.GIF_CODE,
                    STATUS = ordersModel.COMMON.ORDER.STATUS,
                    PAY_METHOD = ordersModel.COMMON.ORDER.PAY_METHOD,

                    str_ORDER_DETAIL = JsonConvert.SerializeObject(ordersModel.COMMON.ORDER_DETAIL),
                    str_Order_Payment = JsonConvert.SerializeObject(ordersModel.COMMON.PAY_INFO),
                    str_Order_Bill = ordersModel.COMMON.BILL_INFO == null ? "" : JsonConvert.SerializeObject(ordersModel.COMMON.BILL_INFO),
                    str_Order_sku = ordersModel.COMMON.ORD_SKU == null ? "" : JsonConvert.SerializeObject(ordersModel.COMMON.ORD_SKU),
                    str_Order_partner = ordersModel.COMMON.ORD_PARTNER == null ? "" : JsonConvert.SerializeObject(ordersModel.COMMON.ORD_PARTNER),
                    strJsonBusiness
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public object[] GetParamsOrdersSaveAll(OrdersModel ordersModel)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (ordersModel.COMMON.SELLER == null)
                ordersModel.COMMON.SELLER = new ORD_SELLER();
            if (ordersModel.COMMON.RECEIVER == null)
                ordersModel.COMMON.RECEIVER = new ORD_RECEIVER();
            try
            {
                string strJsonBusiness = "";
                if (ordersModel.BUSINESS != null)
                    strJsonBusiness = JsonConvert.SerializeObject(ordersModel.BUSINESS);

                object[] param = new object[] {
                    ordersModel.Channel,
                    ordersModel.UserName,

                    ordersModel.COMMON.SELLER.SELLER_CODE,
                    ordersModel.COMMON.SELLER.SELLER_NAME,
                    ordersModel.COMMON.SELLER.SELLER_EMAIL,
                    ordersModel.COMMON.SELLER.SELLER_PHONE,
                    ordersModel.COMMON.SELLER.SELLER_GENDER,
                    ordersModel.COMMON.SELLER.ORG_CODE,
                    ordersModel.COMMON.SELLER.ORG_TRAFFIC,
                    ordersModel.COMMON.SELLER.TRAFFIC_LINK,
                    ordersModel.COMMON.SELLER.ENVIROMENT,

                    ordersModel.COMMON.BUYER.CUS_ID,
                    ordersModel.COMMON.BUYER.TYPE,
                    ordersModel.COMMON.BUYER.NAME,
                    ordersModel.COMMON.BUYER.DOB,
                    ordersModel.COMMON.BUYER.GENDER,
                    ordersModel.COMMON.BUYER.IDCARD,
                    ordersModel.COMMON.BUYER.IDCARD_D,
                    ordersModel.COMMON.BUYER.IDCARD_P,
                    ordersModel.COMMON.BUYER.PROV,
                    ordersModel.COMMON.BUYER.DIST,
                    ordersModel.COMMON.BUYER.WARDS,
                    ordersModel.COMMON.BUYER.ADDRESS,
                    ordersModel.COMMON.BUYER.EMAIL,
                    ordersModel.COMMON.BUYER.PHONE,
                    ordersModel.COMMON.BUYER.FAX,
                    ordersModel.COMMON.BUYER.TAXCODE,

                    JsonConvert.SerializeObject(ordersModel.COMMON.RECEIVER),

                    ordersModel.COMMON.ORDER.ORDER_CODE,
                    ordersModel.COMMON.ORDER.FIELD,
                    ordersModel.COMMON.ORDER.TYPE,
                    ordersModel.COMMON.ORDER.TITLE,
                    ordersModel.COMMON.ORDER.SUMMARY,
                    ordersModel.COMMON.ORDER.AMOUNT,
                    ordersModel.COMMON.ORDER.DISCOUNT,
                    ordersModel.COMMON.ORDER.DISCOUNT_UNIT,
                    ordersModel.COMMON.ORDER.VAT,
                    ordersModel.COMMON.ORDER.TOTAL_AMOUNT,
                    ordersModel.COMMON.ORDER.CURRENCY,
                    ordersModel.COMMON.ORDER.GIF_CODE,
                    ordersModel.COMMON.ORDER.STATUS,
                    ordersModel.COMMON.ORDER.PAY_METHOD,

                    JsonConvert.SerializeObject(ordersModel.COMMON.ORDER_DETAIL),

                    strJsonBusiness
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        #region Lĩnh vực bảo hiểm
        /*
        public string GetStringJsonBaoHiem(OrdersModel ordersModel, ORD_ORDER_DETAIL orderDetailCurrent)
        {
            string strJsonBaoHiem = "";
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (orderDetailCurrent.FIELD.Equals(nameof(goConstants.FieldOrder.BH)))
                {
                    switch (orderDetailCurrent.PRODUCT_TYPE)
                    {
                        case nameof(goConstants.CategoryOrderDetail.BAO_HIEM_VAY):
                            ORD_BUSINESS businessLoan = new ORD_BUSINESS();
                            businessLoan = JsonConvert.DeserializeObject<ORD_BUSINESS>(ordersModel.BUSINESS.ToString());
                            if (businessLoan == null || businessLoan.LOAN == null || businessLoan.LOAN.INSURED == null || businessLoan.LOAN.INSURED.Count == 0)
                                throw new Exception(nameof(goConstantsError.Instance.ERROR_6004) + goConstantsError.Instance.ERROR_6004);
                            strJsonBaoHiem = GetStringJsonLoan(businessLoan);
                            break;
                    }
                }
                return strJsonBaoHiem;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        #region Bảo hiểm vay vốn
        public string GetStringJsonLoan(ORD_BUSINESS businessLoan)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                return JsonConvert.SerializeObject(businessLoan.LOAN.INSURED);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        #endregion
        */
        #endregion

        #region Cent - HDB
        /*
        public OrdersModel GetOrderByLoContract_Old(ContractLoans contractLoans)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.COMMON.SELLER = new ORD_SELLER();
                ordersModel.COMMON.BUYER = new ORD_BUYER();
                ordersModel.COMMON.ORDER = new ORD_ORDER();
                //ordersModel.COMMON.ORDER_DETAIL = ;
                ordersModel.COMMON.PAY_INFO = new ORD_PAYINFO();
                ordersModel.BUSINESS = new ORD_BUSINESS();

                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();

                LO loan = new LO();
                List<LO_INSURED> lstInsured = new List<LO_INSURED>();
                LO_INSURED insured = new LO_INSURED();
                insured.DISBUR = new LO_DISBUR();
                //LO_PRODUCT loProduct = new LO_PRODUCT();
                ordersModel.UserName = contractLoans.BANK_CODE;

                ordersModel.COMMON.SELLER.ORG_CODE = contractLoans.BANK_CODE;
                ordersModel.COMMON.SELLER.BRANCH_CODE = insured.BRANCH_CODE = contractLoans.BRANCH_CODE;
                ordersModel.COMMON.SELLER.SELLER_CODE = insured.TELLER_CODE = contractLoans.TELLER_CODE;
                ordersModel.COMMON.SELLER.SELLER_NAME = insured.TELLER_NAME = contractLoans.TELLER_NAME;
                ordersModel.COMMON.SELLER.SELLER_EMAIL = insured.TELLER_EMAIL = contractLoans.TELLER_EMAIL;
                ordersModel.COMMON.SELLER.SELLER_PHONE = insured.TELLER_PHONE = contractLoans.TELLER_PHONE;
                ordersModel.COMMON.SELLER.SELLER_GENDER = insured.TELLER_PHONE = contractLoans.TELLER_GENDER;

                ordersModel.COMMON.BUYER.CUS_ID = insured.CUS_ID = contractLoans.CIF;
                ordersModel.COMMON.BUYER.TYPE = insured.TYPE = contractLoans.TYPE;
                ordersModel.COMMON.BUYER.NAME = insured.NAME = contractLoans.NAME;

                if (!string.IsNullOrEmpty(contractLoans.DOB))
                    ordersModel.COMMON.BUYER.DOB = insured.DOB = contractLoans.DOB;
                //else
                //ordersModel.COMMON.BUYER.DOB = insured.DOB = contractLoans.DOB;
                ordersModel.COMMON.BUYER.GENDER = insured.GENDER = contractLoans.GENDER;
                ordersModel.COMMON.BUYER.IDCARD = insured.IDCARD = contractLoans.IDCARD;
                ordersModel.COMMON.BUYER.IDCARD_D = insured.IDCARD_D = contractLoans.IDCARD_D;
                ordersModel.COMMON.BUYER.IDCARD_P = insured.IDCARD_P = contractLoans.IDCARD_P;

                ordersModel.COMMON.BUYER.PROV = insured.PROV = contractLoans.PROV;
                ordersModel.COMMON.BUYER.DIST = insured.DIST = contractLoans.DIST;
                ordersModel.COMMON.BUYER.WARDS = insured.WARDS = contractLoans.WARDS;
                ordersModel.COMMON.BUYER.ADDRESS = insured.ADDRESS = contractLoans.ADD;
                ordersModel.COMMON.BUYER.EMAIL = insured.EMAIL = contractLoans.EMAIL;
                ordersModel.COMMON.BUYER.PHONE = insured.PHONE = contractLoans.PHONE;
                ordersModel.COMMON.BUYER.FAX = insured.FAX = contractLoans.FAX;
                ordersModel.COMMON.BUYER.TAXCODE = insured.TAXCODE = contractLoans.TAXCODE;
                insured.RELATIONSHIP = goConstants.Relationship.BAN_THAN.ToString();
                //insured.TOTAL_AMOUNT = contractLoans.LO_TOTAL;
                insured.INSUR_TOTAL_AMOUNT = contractLoans.LO_TOTAL;
                insured.CONTRACT_NO = contractLoans.so_hd;
                insured.SO_ID_CENT = contractLoans.so_id;
                insured.BANK_CODE = contractLoans.BANK_CODE;
                insured.LO_CONTRACT = contractLoans.LO_CONTRACT;
                insured.LO_TYPE = goConstants.ModeInsurance.TRUC_TIEP.ToString();
                insured.CURRENCY = contractLoans.CURRENCY;
                insured.LO_TOTAL_AMOUNT = contractLoans.LO_TOTAL;
                insured.NV_CENT = goConstants.Cent_nv.ATTD.ToString();
                insured.DURATION = contractLoans.DURATION;
                insured.LO_UNIT = contractLoans.UNIT;
                insured.LO_DATE = contractLoans.LO_DATE;

                insured.DISBUR.BRANCH_CODE = contractLoans.DISBUR.BRANCH_CODE;
                insured.DISBUR.DISBUR_NUM = contractLoans.DISBUR.DISBUR_NUM;
                insured.DISBUR.DISBUR_DATE = contractLoans.DISBUR.DISBUR_DATE;
                insured.DISBUR.DISBUR_AMOUNT = contractLoans.DISBUR.DISBUR_AMOUNT;

                ordersModel.COMMON.ORDER.FIELD = orderDetail.FIELD = goConstants.FieldOrder.BH.ToString();
                ordersModel.COMMON.ORDER.TYPE = goConstants.TypeOrder.BH_M.ToString();
                ordersModel.COMMON.ORDER.PAY_METHOD = goConstants.PayMethod.ON_HDI_CHANNEL.ToString();

                orderDetail.PRODUCT_TYPE = goConstants.CategoryOrderDetail.BAO_HIEM_VAY.ToString();
                orderDetail.EFFECTIVE_DATE = insured.EFFECTIVE_DATE = insured.LO_EFFECTIVE_DATE = contractLoans.LO_EFF_DATE;
                orderDetail.EXPIRATION_DATE = insured.EXPIRATION_DATE = insured.LO_EXPIRATION_DATE = contractLoans.LO_EXP_DATE;

                lstOrderDetail.Add(orderDetail);
                lstInsured.Add(insured);

                loan.INSURED = lstInsured;

                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                ordersModel.BUSINESS.LOAN = loan;

            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }

        public OrdersModel GetOrderByCentInsurLo(CentInsurLoInfo insurLoInfo)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            ordersModel.COMMON = new ORD_COMMON();
            ordersModel.COMMON.RECEIVER = new ORD_RECEIVER();
            try
            {
                LO_INSURED insur = new LO_INSURED();
                ORD_SELLER seller = new ORD_SELLER();
                seller.SELLER_CODE = insur.TELLER_CODE = insurLoInfo.data.ctgcn[0].SALEID;
                seller.SELLER_NAME = insur.TELLER_NAME = insurLoInfo.data.ctgcn[0].TEN_SALE;
                seller.SELLER_EMAIL = insur.TELLER_EMAIL = insurLoInfo.data.ctgcn[0].EMAIL_NHANG;
                seller.ORG_CODE = goConstants.Org_Code.HDBANK_VN.ToString();
                insur.DISBUR = new LO_DISBUR();
                insur.DISBUR.DISBUR_NUM = "1";

                ORD_BUYER buyer = new ORD_BUYER();
                buyer.CUS_ID = insur.CUS_ID = insurLoInfo.data.ctgcn[0].SO_CIF_KH;
                if (insurLoInfo.data.ctgcn[0].NHOM_DN.Equals("C"))
                    buyer.TYPE = insur.TYPE = goConstants.PersonType.CN.ToString();

                buyer.NAME = insur.NAME = insurLoInfo.data.ctgcn[0].TEN;
                buyer.DOB = insur.DOB = insurLoInfo.data.ds_dk[0].NGAY_SINH;//insurLoInfo.data.ctgcn[0].NGAY_SINH;
                if (insurLoInfo.data.ctgcn[0].GIOI == "1")
                    buyer.GENDER = insur.GENDER = goConstants.Gender_Unit.F.ToString();
                else if (insurLoInfo.data.ctgcn[0].GIOI == "0")
                    buyer.GENDER = insur.GENDER = goConstants.Gender_Unit.M.ToString();

                buyer.IDCARD = insur.IDCARD = insurLoInfo.data.ctgcn[0].SO_CMT;
                buyer.IDCARD_D = insur.IDCARD_D = insurLoInfo.data.ctgcn[0].NGAY_CMT;
                buyer.IDCARD_P = insur.IDCARD_P = insurLoInfo.data.ctgcn[0].NOI_CMT;
                //buyer.PROVINCE = buyer.PROVINCE =
                //buyer.DISTRICT = buyer.DISTRICT =
                //buyer.WARDS = buyer.WARDS =
                buyer.ADDRESS = insur.ADDRESS = insurLoInfo.data.ctgcn[0].DCHI;
                buyer.EMAIL = insur.EMAIL = insurLoInfo.data.ctgcn[0].EMAIL;
                buyer.PHONE = insur.PHONE = insurLoInfo.data.ctgcn[0].PHONE;
                //buyer.FAX = buyer.FAX = 
                //buyer.TAXCODE = buyer.TAXCODE =
                insur.PRODUCT_CODE = insurLoInfo.data.ctgcn[0].PVI_BH;// goConstants.Product_ATTD.A.ToString();
                insur.INSUR_TOTAL_AMOUNT = insur.DISBUR.INSUR_TOTAL_AMOUNT = goUtility.Instance.GetMoneyToDouble(insurLoInfo.data.ctgcn[0].TIEN_BH, 0);
                insur.LO_CONTRACT = insurLoInfo.data.ctgcn[0].SO_HD_KENH;
                insur.LO_TOTAL_AMOUNT = insur.DISBUR.DISBUR_AMOUNT = goUtility.Instance.GetMoneyToDouble(insurLoInfo.data.ctgcn[0].TIEN_VAY, 0);
                insur.LO_TYPE = goConstants.ModeInsurance.TRUC_TIEP.ToString();
                insur.BANK_CODE = goConstants.Org_Code.HDBANK_VN.ToString();
                insur.BRANCH_CODE = insur.DISBUR.BRANCH_CODE = insurLoInfo.data.ctgcn[0].MA_CN;
                insur.CURRENCY = goConstants.Currency.VND.ToString();
                insur.CONTRACT_NO = insurLoInfo.data.ctgcn[0].SO_HD;
                insur.SO_ID_CENT = goUtility.Instance.ConvertToDouble(insurLoInfo.data.ctgcn[0].SO_ID, 0).ToString();
                insur.NV_CENT = insurLoInfo.data.ctgcn[0].NV;
                insur.REGION = insurLoInfo.data.ctgcn[0].PVI_DLY;
                insur.DURATION = insurLoInfo.data.ctgcn[0].HAN_VAY;
                insur.LO_UNIT = goConstants.Duration_Unit.M.ToString();
                insur.LO_EFFECTIVE_DATE = insurLoInfo.data.ctgcn[0].HAN_VAY_HL;
                insur.EFFECTIVE_DATE = insur.DISBUR.DISBUR_DATE = insurLoInfo.data.ctgcn[0].NGAY_HL;
                insur.EXPIRATION_DATE = insurLoInfo.data.ctgcn[0].NGAY_KT;
                insur.LO_DATE = insurLoInfo.data.ctgcn[0].HAN_VAY_HL;

                List<ADDTIONAL_COVERAGE> lstCover = new List<ADDTIONAL_COVERAGE>();
                if (insurLoInfo.data.ctgcn[0].PVI_BH.Length > 0)
                {
                    ADDTIONAL_COVERAGE addCoverage = new ADDTIONAL_COVERAGE();
                    var name = insurLoInfo.data.ctgcn[0].PVI_BH.Substring(0, 1);
                    addCoverage.CODE = name;
                    addCoverage.NAME_DISPLAYED = name;
                    addCoverage.NAME_PRINT = name;
                    addCoverage.NAME = name;
                    lstCover.Add(addCoverage);
                }

                if (insurLoInfo.data.ctgcn[0].PVI_BH.Length > 1)
                {
                    ADDTIONAL_COVERAGE addCoverage = new ADDTIONAL_COVERAGE();
                    var name = insurLoInfo.data.ctgcn[0].PVI_BH.Substring(1, 1);
                    addCoverage.CODE = name;
                    addCoverage.NAME_DISPLAYED = name;
                    addCoverage.NAME_PRINT = name;
                    addCoverage.NAME = name;
                    lstCover.Add(addCoverage);
                    if (insurLoInfo.data.ctgcn[0].PVI_BH.Length > 2)
                    {
                        ADDTIONAL_COVERAGE addCoverage1 = new ADDTIONAL_COVERAGE();
                        var name1 = insurLoInfo.data.ctgcn[0].PVI_BH.Substring(2, 1);
                        addCoverage1.CODE = name1;
                        addCoverage1.NAME_DISPLAYED = name1;
                        addCoverage1.NAME_PRINT = name1;
                        addCoverage1.NAME = name1;
                        lstCover.Add(addCoverage1);
                    }
                }
                insur.ADD_COVERAGE = lstCover;

                List<Beneficiary> lstBenefi = new List<Beneficiary>();
                if (!string.IsNullOrEmpty(insurLoInfo.data.ctgcn[0].NGUOI_TH))
                {
                    Beneficiary Benefi = new Beneficiary();
                    Benefi.TYPE = goConstants.PersonType.CQ.ToString();
                    Benefi.ORG_CODE = goConstants.Org_Code.HDBANK_VN.ToString();
                    Benefi.NAME = insurLoInfo.data.ctgcn[0].NGUOI_TH;
                    Benefi.ADDRESS = insurLoInfo.data.ctgcn[0].DCHI_NGUOI_TH;
                    lstBenefi.Add(Benefi);
                }
                insur.BENEFICIARY = lstBenefi;

                List<PaymentPeriod> lstPeriod = new List<PaymentPeriod>();
                if (insurLoInfo.data.ds_tt != null && insurLoInfo.data.ds_tt.Any())
                {
                    foreach (var item in insurLoInfo.data.ds_tt)
                    {
                        PaymentPeriod per = new PaymentPeriod();
                        per.CODE = item.NGAY;
                        per.NAME = item.NGAY;
                        per.EFFECTIVE_DATE = item.NGAY;
                        per.AMOUNT = goUtility.Instance.GetMoneyToDouble(item.TIEN, 0).ToString();
                        per.CURRENCY = goConstants.Currency.VND.ToString();
                        lstPeriod.Add(per);
                    }
                }
                insur.PERIOD = lstPeriod;

                ORD_ORDER order = new ORD_ORDER();
                ORD_ORDER_DETAIL order_dt = new ORD_ORDER_DETAIL();
                List<ORD_ORDER_DETAIL> lstOrder_dt = new List<ORD_ORDER_DETAIL>();

                order.FIELD = order_dt.FIELD = goConstants.FieldOrder.BH.ToString();
                if (insurLoInfo.data.ctgcn[0].NV.Equals(goConstants.Cent_nv.ATTD))
                    order_dt.PRODUCT_TYPE = goConstants.CategoryOrderDetail.BAO_HIEM_VAY.ToString();
                //order.ORDER_CODE = 
                order.TYPE = goConstants.TypeOrder.BH_M.ToString();
                //order.TITLE =
                //order.SUMMARY =
                order.AMOUNT = order_dt.AMOUNT = insur.AMOUNT = goUtility.Instance.ConvertToDouble(insurLoInfo.data.ctgcn[0].PHI, 0);
                order.DISCOUNT = order_dt.DISCOUNT = insur.DISCOUNT = goUtility.Instance.ConvertToDouble(insurLoInfo.data.ctgcn[0].TL_GIAM, 0);
                order.DISCOUNT_UNIT = order_dt.DISCOUNT_UNIT = insur.DISCOUNT_UNIT = goConstants.UnitMode.P.ToString();
                order.VAT = order_dt.VAT = insur.VAT = goUtility.Instance.ConvertToDouble(insurLoInfo.data.ctgcn[0].THUE, 0);
                order.TOTAL_AMOUNT = order_dt.TOTAL_AMOUNT = insur.TOTAL_AMOUNT = goUtility.Instance.GetMoneyToDouble(insurLoInfo.data.ctgcn[0].TTOAN, 0);
                //order.CURRENCY = 
                //order.GIF_CODE =
                //order.STATUS = go
                order.PAY_METHOD = goConstants.PayMethod.ON_HDI_CHANNEL.ToString();
                order_dt.PRODUCT_TYPE = goConstants.CategoryOrderDetail.BAO_HIEM_VAY.ToString();

                lstOrder_dt.Add(order_dt);

                ORD_BUSINESS business = new ORD_BUSINESS();
                LO lo = new LO();
                List<LO_INSURED> lstInsur = new List<LO_INSURED>();
                lstInsur.Add(insur);
                lo.INSURED = lstInsur;
                business.LOAN = lo;

                ordersModel.UserName = goConstants.Org_Code.HDBANK_VN.ToString();
                ordersModel.COMMON.SELLER = seller;
                ordersModel.COMMON.BUYER = buyer;
                ordersModel.COMMON.ORDER = order;
                ordersModel.COMMON.ORDER_DETAIL = lstOrder_dt;
                ordersModel.BUSINESS = business;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        */
        #endregion

        #region ATTD
        public OrdersModel GetOrderByLoContract(LoContract lo)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = lo.BANK_CODE;
                ordersModel.UserName = lo.TELLER_CODE;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.SELLER_CODE = lo.TELLER_CODE;
                seller.SELLER_NAME = lo.TELLER_NAME;
                seller.SELLER_GENDER = lo.TELLER_GENDER;
                seller.SELLER_EMAIL = lo.TELLER_EMAIL;
                seller.SELLER_PHONE = lo.TELLER_PHONE;
                seller.STRUCT_CODE = lo.TELLER_CODE;
                seller.ORG_CODE = lo.BANK_CODE;
                seller.ENVIROMENT = lo.BANK_CODE;
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.C)))
                {
                    buyer.TYPE = goConstants.PersonType.CN.ToString();
                    buyer.NAME = lo.NAME;
                    buyer.DOB = lo.DOB;
                    buyer.GENDER = lo.GENDER;
                    buyer.IDCARD = lo.IDCARD;
                    buyer.IDCARD_D = lo.IDCARD_D;
                    buyer.IDCARD_P = lo.IDCARD_P;
                    buyer.PROV = lo.PROV;
                    buyer.DIST = lo.DIST;
                    buyer.WARDS = lo.WARDS;
                    buyer.ADDRESS = lo.ADDRESS;
                    buyer.EMAIL = lo.EMAIL;
                    buyer.PHONE = lo.PHONE;
                }
                else if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.B)))
                {
                    buyer.TYPE = goConstants.PersonType.CQ.ToString();
                    buyer.CUS_ID = lo.BANK_CODE;
                    ordersModel.COMMON.BUYER = buyer;
                }
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm ATTD";
                order.SUMMARY = "Đơn bảo hiểm ATTD";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                order.STATUS = "";
                if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.B)))
                    order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                else
                    order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = lo.LO_MODE;
                orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_ATTD;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm ATTD";
                orderDetail.EFFECTIVE_DATE = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance
                List<LoInsured> lstInsured = new List<LoInsured>();
                LoInsured insured = new LoInsured();
                insured.CUS_ID = lo.CUS_ID;
                insured.TYPE = lo.TYPE;
                insured.NAME = lo.NAME;
                insured.DOB = lo.DOB;
                insured.GENDER = lo.GENDER;
                insured.IDCARD = lo.IDCARD;
                insured.IDCARD_D = lo.IDCARD_D;
                insured.IDCARD_P = lo.IDCARD_P;
                insured.PROV = lo.PROV;
                insured.DIST = lo.DIST;
                insured.WARDS = lo.WARDS;
                insured.ADDRESS = lo.ADDRESS;
                insured.EMAIL = lo.EMAIL;
                insured.PHONE = lo.PHONE;
                insured.RELATIONSHIP = nameof(goConstants.Relationship.KHAC);
                if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.B)))
                    insured.PRODUCT_CODE = nameof(goConstants.Product_Attd.ATTD_NH);
                else
                    insured.PRODUCT_CODE = nameof(goConstants.Product_Attd.ATTD);
                insured.PACK_CODE = nameof(goConstants.Pack_Attd.A);
                insured.EFFECTIVE_DATE = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                insured.EXPIRATION_DATE = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy HH:mm:ss");//.AddMonths(1).AddDays(1)
                insured.AMOUNT = 0;
                insured.DISCOUNT = 0;
                insured.DISCOUNT_UNIT = "";
                insured.VAT = 0;
                insured.TOTAL_AMOUNT = 0;

                insured.INSUR_TOTAL_AMOUNT = lo.LO_TOTAL;
                insured.BANK_CODE = lo.BANK_CODE;
                insured.BRANCH_CODE = lo.BANK_CODE;

                insured.LO_CONTRACT = lo.LO_CONTRACT;
                insured.LO_TYPE = lo.LO_TYPE;
                insured.LO_MODE = lo.LO_MODE;
                insured.PAYER = lo.PAYER;
                insured.CURRENCY = lo.CURRENCY;
                insured.LO_TOTAL_AMOUNT = lo.LO_TOTAL;
                insured.LO_EFFECTIVE_DATE = lo.LO_EFF_DATE;
                insured.LO_EXPIRATION_DATE = lo.LO_EXP_DATE;
                insured.LO_DATE = lo.LO_DATE;
                insured.INTEREST_RATE = "0";
                insured.DURATION = lo.DURATION;
                insured.DURATION_UNIT = lo.UNIT;
                if (lo.DISBUR != null)
                {
                    insured.DISBUR = new Disbur();
                    insured.DISBUR.BRANCH_CODE = lo.DISBUR.BRANCH_CODE;
                    insured.DISBUR.DISBUR_CODE = lo.DISBUR.DISBUR_CODE;
                    insured.DISBUR.DISBUR_NUM = lo.DISBUR.DISBUR_NUM;
                    insured.DISBUR.DISBUR_DATE = lo.DISBUR.DISBUR_DATE;
                    insured.DISBUR.DISBUR_AMOUNT = lo.DISBUR.DISBUR_AMOUNT;
                    insured.DISBUR.INSUR_TOTAL = lo.DISBUR.INSUR_TOTAL;
                }

                lstInsured.Add(insured);
                LoanInsurance loInsur = new LoanInsurance();
                loInsur.INSURED = lstInsured;
                ordersModel.BUSINESS.LO = loInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }

        // 18/10/2021
        public OrdersModel GetOrdersByBankLo(BankLoRequest bankLo, LoUserChange infoUser)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = bankLo.CHANNEL;
                ordersModel.UserName = bankLo.USER_NAME;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                // bên LO => SELLER_CODE, bên contract_insur => USER_NAME
                seller.SELLER_CODE = infoUser.USER_NAME;//bankLo.SELLER_INFO.SELLER_CODE;
                seller.SELLER_NAME = bankLo.SELLER_INFO.SELLER_NAME;
                seller.SELLER_GENDER = bankLo.SELLER_INFO.SELLER_GENDER;
                seller.SELLER_EMAIL = bankLo.SELLER_INFO.SELLER_EMAIL;
                seller.SELLER_PHONE = bankLo.SELLER_INFO.SELLER_PHONE;
                //seller.STRUCT_CODE = bankLo.SELLER_INFO.se;
                // gán branch vào org vì khai báo cha con trong sys_org và để gán org bán
                seller.ORG_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BRANCH_CODE;
                seller.BRANCH_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BRANCH_CODE;
                //seller.ENVIROMENT = bankLo.BANK_CODE;
                ordersModel.COMMON.SELLER = seller;
                #endregion
                #region Buyer - Người mua

                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = goConstants.PersonType.CN.ToString();
                buyer.NAME = bankLo.LO_INFO.NAME;
                buyer.DOB = bankLo.LO_INFO.DOB;
                buyer.GENDER = bankLo.LO_INFO.GENDER;
                buyer.IDCARD = bankLo.LO_INFO.IDCARD;
                buyer.IDCARD_D = bankLo.LO_INFO.IDCARD_D;
                buyer.IDCARD_P = bankLo.LO_INFO.IDCARD_P;
                buyer.PROV = bankLo.LO_INFO.PROV;
                buyer.DIST = bankLo.LO_INFO.DIST;
                buyer.WARDS = bankLo.LO_INFO.WARDS;
                buyer.ADDRESS = bankLo.LO_INFO.ADDRESS;
                buyer.EMAIL = bankLo.LO_INFO.EMAIL;
                buyer.PHONE = bankLo.LO_INFO.PHONE;

                //if (string.IsNullOrEmpty(bankLo.LO_INFO.PAYER) || bankLo.LO_INFO.PAYER.Equals(nameof(goConstants.Payer_Unit.C)))
                //{
                //    buyer.TYPE = goConstants.PersonType.CN.ToString();
                //    buyer.NAME = bankLo.LO_INFO.NAME;
                //    buyer.DOB = bankLo.LO_INFO.DOB;
                //    buyer.GENDER = bankLo.LO_INFO.GENDER;
                //    buyer.IDCARD = bankLo.LO_INFO.IDCARD;
                //    buyer.IDCARD_D = bankLo.LO_INFO.IDCARD_D;
                //    buyer.IDCARD_P = bankLo.LO_INFO.IDCARD_P;
                //    buyer.PROV = bankLo.LO_INFO.PROV;
                //    buyer.DIST = bankLo.LO_INFO.DIST;
                //    buyer.WARDS = bankLo.LO_INFO.WARDS;
                //    buyer.ADDRESS = bankLo.LO_INFO.ADDRESS;
                //    buyer.EMAIL = bankLo.LO_INFO.EMAIL;
                //    buyer.PHONE = bankLo.LO_INFO.PHONE;
                //}
                //else if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.B)))
                //{
                //    buyer.TYPE = goConstants.PersonType.CQ.ToString();
                //    buyer.CUS_ID = lo.BANK_CODE;
                //    ordersModel.COMMON.BUYER = buyer;
                //}
                ordersModel.COMMON.BUYER = buyer;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                //Từ bank sang là đơn nháp và phí = 0 => tạp type mới để tránh lỗi khi cấp đơn validate amount > 0
                order.TYPE = nameof(goConstants.TypeOrder.BH_NHAP);
                // Start 2022-06-03 By ThienTVB
                if (bankLo.LO_INFO.DISBUR != null)
                {
                    order.TITLE = "Đơn bảo hiểm ATTD";
                    order.SUMMARY = "Đơn bảo hiểm ATTD";
                }
                if (bankLo.LO_INFO.HOUSE_INFO != null && bankLo.LO_INFO.ASSET_CODE != null)
                {
                    order.TITLE = "Đơn bảo hiểm Nhà tư nhân";
                    order.SUMMARY = "Đơn bảo hiểm Nhà tư nhân";
                }
                //order.TITLE = "Đơn bảo hiểm ATTD";
                //order.SUMMARY = "Đơn bảo hiểm ATTD";
                // End 2022-06-03 By ThienTVB
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                order.STATUS = "";
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);
                //if (lo.PAYER.Equals(nameof(goConstants.Payer_Unit.B)))
                //    order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                //else
                //    order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                //orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP); // default là Toàn hạn mức
                // Start 2022-06-03 By ThienTVB
                if (bankLo.LO_INFO.DISBUR != null)
                {
                    orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_ATTD;
                }
                if (bankLo.LO_INFO.HOUSE_INFO != null && bankLo.LO_INFO.ASSET_CODE != null)
                {
                    orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_NHATN;
                }
                // End 2022-06-03 By ThienTVB
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm ATTD";
                orderDetail.EFFECTIVE_DATE = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.PRODUCT_CODE = bankLo.PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance

                // Hiện với ATTD chỉ có 1 - 1
                List<LoProduct> lstInsur = new List<LoProduct>();
                LoProduct itemInsur = JsonConvert.DeserializeObject<LoProduct>(JsonConvert.SerializeObject(bankLo.LO_INFO));
                // Start 2022-05-31 By ThienTVB
                itemInsur.BRANCH_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BRANCH_CODE;
                if (bankLo.LO_INFO.DISBUR != null)
                {
                    itemInsur.DISBUR = JsonConvert.DeserializeObject<DisburV2>(JsonConvert.SerializeObject(bankLo.LO_INFO.DISBUR));
                    itemInsur.DISBUR.BRANCH_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BRANCH_CODE;
                }
                if (bankLo.LO_INFO.HOUSE_INFO != null && bankLo.LO_INFO.ASSET_CODE != null)
                {
                    itemInsur.ASSET_CODE = bankLo.LO_INFO.ASSET_CODE;
                    itemInsur.HOUSE_INFO = JsonConvert.DeserializeObject<HouseInfoLo>(JsonConvert.SerializeObject(bankLo.LO_INFO.HOUSE_INFO));
                }
                // End 2022-05-31 By ThienTVB
                itemInsur.BANK_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BANK_CODE;
                itemInsur.PRODUCT_CODE = bankLo.PRODUCT_CODE;
                itemInsur.LO_TOTAL_AMOUNT = bankLo.LO_INFO.LO_TOTAL;

                // auto gán thụ hưởng
                Beneficiary benef = new Beneficiary();
                benef.TYPE = goConstants.PersonType.CQ.ToString();
                benef.ORG_CODE = bankLo.SELLER_INFO.BRANCH_INFO.BRANCH_CODE;
                itemInsur.BENEFICIARY = new List<Beneficiary>();
                itemInsur.BENEFICIARY.Add(benef);
                
                lstInsur.Add(itemInsur);

                OrdLoInsur ordLoInsur = new OrdLoInsur();
                ordLoInsur.INSURED = lstInsur;
                ordersModel.BUSINESS.LO_INSUR = ordLoInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
            return ordersModel;
        }
        #endregion

        #region Vietjet
        public OrdersModel GetOrderBySafeFlight(FLIGHT flight)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.COMMON.SELLER = new ORD_SELLER();
                ordersModel.COMMON.BUYER = new ORD_BUYER();
                ordersModel.COMMON.ORDER = new ORD_ORDER();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = "";
                ordersModel.UserName = flight.INFO.ORG_CODE;
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();

                ordersModel.COMMON.SELLER.SELLER_CODE = flight.INFO.ORG_CODE;
                ordersModel.COMMON.SELLER.ORG_CODE = flight.INFO.ORG_CODE;

                switch (flight.INFO.ORG_CODE)
                {
                    case "VIETJET_VN":
                        ordersModel.COMMON.BUYER.TYPE = "CQ";
                        ordersModel.COMMON.BUYER.NAME = "CÔNG TY CỔ PHẦN HÀNG KHÔNG VIETJET";
                        ordersModel.COMMON.ORDER.FIELD = orderDetail.FIELD = goConstants.FieldOrder.BH.ToString();
                        ordersModel.COMMON.ORDER.TYPE = goConstants.TypeOrder.BH_M.ToString();
                        ordersModel.COMMON.ORDER.PAY_METHOD = goConstants.PayMethod.ON_PARTNER_CHANNEL.ToString();
                        break;
                }
                orderDetail.PRODUCT_TYPE = goConstants.CategoryOrderDetail.BH_VNAT.ToString();
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                //ordersModel.BUSINESS.FLIGHT = flight;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }

        /// <summary>
        /// Bay an toàn
        /// </summary>
        /// <param name="flySafe"></param>
        /// <returns></returns>
        public OrdersModel GetOrderByVJC(OrderVJC orderVJC)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = nameof(goConstants.Org_Code.VIETJET_VN);
                ordersModel.UserName = "HDI_AUTO";

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = nameof(goConstants.Org_Code.HDI);
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.WEB_LANDING);
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                switch (orderVJC.FLIGHT_INFO.ORG_CODE)
                {
                    case "VIETJET_VN":
                        buyer.TYPE = "CQ";
                        buyer.NAME = "CÔNG TY CỔ PHẦN HÀNG KHÔNG VIETJET";
                        break;
                }
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                order.STATUS = goConstants.OrderStatus.PAID.ToString();
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion


                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.HD_BAO);
                orderDetail.PRODUCT_TYPE = orderVJC.CUSTOMERS[0].CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm bay an toàn";
                orderDetail.EFFECTIVE_DATE = orderVJC.CUSTOMERS[0].EFF_DATE;
                orderDetail.EXPIRATION_DATE = orderVJC.CUSTOMERS[0].EXP_DATE;
                orderDetail.PRODUCT_CODE = orderVJC.CUSTOMERS[0].PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion


                #region List Insurance
                ordersModel.BUSINESS.FLIGHT = orderVJC;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }

        /// <summary>
        /// Bay an toàn
        /// </summary>
        /// <param name="flySafe"></param>
        /// <returns></returns>
        public OrdersModel GetOrderTravelByVJC(OrderVJC orderVJC)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = nameof(goConstants.Org_Code.VIETJET_VN);
                ordersModel.UserName = "HDI_AUTO";

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = nameof(goConstants.Org_Code.HDI);                
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.WEB_LANDING);
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = "CQ";
                buyer.NAME = "CÔNG TY CỔ PHẦN HÀNG KHÔNG VIETJET";                
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Thông tin thanh toán
                ordersModel.COMMON.PAY_INFO = orderVJC.PAY_INFO;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                order.STATUS = goConstants.OrderStatus.PAID.ToString();
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion


                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.HD_BAO);
                orderDetail.PRODUCT_TYPE = orderVJC.CUSTOMERS[0].CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm du lịch";
                orderDetail.EFFECTIVE_DATE = orderVJC.CUSTOMERS[0].EFF_DATE;
                orderDetail.EXPIRATION_DATE = orderVJC.CUSTOMERS[0].EXP_DATE;
                orderDetail.PRODUCT_CODE = orderVJC.CUSTOMERS[0].PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion


                #region List Insurance
                ordersModel.BUSINESS.FLIGHT = orderVJC;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }

        /// <summary>
        /// Các sản phẩm khác VJC
        /// </summary>
        /// <param name="orderVJC"></param>
        /// <returns></returns>
        public OrdersModel GetOrderByV2VJC(OrderVJC orderVJC)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = nameof(goConstants.Org_Code.VIETJET_VN);
                ordersModel.UserName = orderVJC.SELLER.SELLER_CODE;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = orderVJC.SELLER;
                #endregion

                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = orderVJC.BUYER;
                #endregion

                #region Thông tin thanh toán
                ordersModel.COMMON.PAY_INFO = orderVJC.PAY_INFO;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                //order.STATUS = goConstants.OrderStatus.PAID.ToString();
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = orderVJC.CUSTOMERS[0].CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                orderDetail.EFFECTIVE_DATE = orderVJC.CUSTOMERS[0].EFF_DATE;
                orderDetail.EXPIRATION_DATE = orderVJC.CUSTOMERS[0].EXP_DATE;
                orderDetail.PRODUCT_CODE = orderVJC.CUSTOMERS[0].PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion


                #region List Insurance
                ordersModel.BUSINESS.FLIGHT = orderVJC;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Sức khỏe

        #region Voucher Free
        public OrdersModel GetOrdersByVoucherFree(VoucherInsurFree voucherInsur, VoucherInfo voucherInfo)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = voucherInfo.channel;
                ordersModel.UserName = voucherInfo.org_code;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.SELLER_CODE = voucherInfo.org_code;
                seller.STRUCT_CODE = voucherInfo.org_code;
                seller.ORG_CODE = voucherInfo.org_code;
                seller.ENVIROMENT = voucherInfo.channel;
                ordersModel.COMMON.SELLER = seller;
                #endregion
                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = goConstants.PersonType.CQ.ToString();
                buyer.NAME = voucherInfo.org_name;
                buyer.ADDRESS = voucherInfo.address;
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm qua voucher giảm 100%";
                order.SUMMARY = "Đơn bảo hiểm qua voucher giảm 100%";
                order.AMOUNT = 0;
                order.DISCOUNT = 100;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = voucherInfo.activation_code;
                order.STATUS = voucherInfo.status;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = voucherInfo.category;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 100;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm qua voucher giảm 100%";
                orderDetail.EFFECTIVE_DATE = voucherInfo.effective_date;
                orderDetail.EXPIRATION_DATE = voucherInfo.expriration_date;
                orderDetail.PRODUCT_CODE = voucherInfo.product_code;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance
                List<InsuredPerson> lstInsured = new List<InsuredPerson>();
                InsuredPerson insured = new InsuredPerson();
                insured.TYPE = goConstants.PersonType.CN.ToString();
                insured.NAME = voucherInsur.name;
                insured.DOB = voucherInsur.dob;
                insured.GENDER = goBusinessCommon.Instance.NameTitleToGender(voucherInsur.name_title);
                insured.IDCARD = voucherInsur.idCard;
                insured.PROV = voucherInsur.prov_code;
                insured.DIST = voucherInsur.dist_code;
                insured.WARDS = voucherInsur.wards_code;
                insured.ADDRESS = voucherInsur.address;
                insured.EMAIL = voucherInsur.email;
                insured.PHONE = voucherInsur.phone;
                insured.RELATIONSHIP = nameof(goConstants.Relationship.KHAC);
                insured.PRODUCT_CODE = voucherInfo.product_code;
                insured.PACK_CODE = voucherInfo.pack_code;
                insured.EFFECTIVE_DATE = voucherInfo.effective_date;
                insured.EXPIRATION_DATE = voucherInfo.expriration_date;
                insured.AMOUNT = 0;
                insured.DISCOUNT = 0;
                insured.DISCOUNT_UNIT = "";
                insured.VAT = 0;
                insured.TOTAL_AMOUNT = 0;

                if (voucherInfo.product_code == "TRAU" && voucherInfo.pack_code == "TRAU_KIM_CUONG")
                    insured.REGION = "TCKMC";

                lstInsured.Add(insured);
                HealthCare healthCare = new HealthCare();
                healthCare.INSURED = lstInsured;
                ordersModel.BUSINESS.HEALTH_CARE = healthCare;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #endregion

        #region Order pay
        public BaseRequest GetBaseRequest_OrderPay(BaseRequest request, string orderCode, string period, double totalPay, string status)
        {
            if (request == null)
                throw new Exception("requset error");
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);

            BaseRequest reqOrderTrans = request;
            BaseResponse resOrderTrans = new BaseResponse();
            reqOrderTrans.Action.ActionCode = goConstantsProcedure.CREATE_ORDER_TRANS;
            object param = GetParamsOrderPay(request, orderCode, period, totalPay, status);
            reqOrderTrans.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
            goBusinessCommon.Instance.GetSignRequest(ref reqOrderTrans, config);
            resOrderTrans = goBusinessAction.Instance.GetBaseResponse(reqOrderTrans);

            OrderTransInfo orderTransInfo = goBusinessCommon.Instance.GetData<OrderTransInfo>(resOrderTrans, 0).FirstOrDefault();
            if (orderTransInfo == null || string.IsNullOrEmpty(orderTransInfo.order_code) || string.IsNullOrEmpty(orderTransInfo.order_trans_code))
                throw new Exception("order not exesits");

            BaseRequest request_pay = new BaseRequest();
            ActionInfo action = new ActionInfo();
            OrderPay orderPay = new OrderPay();
            orderPay.appId = goConstants.Org_Code.HDI.ToString();
            orderPay.orderId = orderTransInfo.order_trans_code;
            orderPay.amount = orderTransInfo.amount;
            orderPay.payDate = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            orderPay.desc = orderTransInfo.description;
            orderPay.payer_name = orderTransInfo.payer;
            orderPay.payer_mobile = orderTransInfo.payer_phone;
            orderPay.payer_email = orderTransInfo.payer_email;
            orderPay.contentPay = orderTransInfo.content_pay;

            request_pay.Device = request.Device;
            action.ParentCode = request.Action.ParentCode;
            action.Secret = request.Action.Secret;
            action.UserName = request.Action.UserName;
            action.ActionCode = goConstantsProcedure.Pay_Get_Config;
            request_pay.Action = action;
            request_pay.Data = orderPay;
            return request_pay;
        }

        public object GetParamsOrderPay(BaseRequest request, string orderCode, string period, double totalPay, string status)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    Project = "",
                    UserName = request.Action.UserName,
                    orderCode = orderCode,
                    period = period,
                    totalPay = totalPay,
                    status = status
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public BaseResponse GetOrderTrans(BaseRequest request, string orderCode, string period, double totalPay, string payment_type, List<PaymentMix> lstPayMix, string status)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (request == null)
                    throw new Exception("requset error");
                ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);

                BaseRequest reqOrderTrans = request;
                BaseResponse res = new BaseResponse();
                reqOrderTrans.Action.ActionCode = goConstantsProcedure.CREAT_ORDER_TRANS_V2;
                object param = GetParamsOrderTrans(request, orderCode, period, totalPay, payment_type, lstPayMix, status);
                reqOrderTrans.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));

                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, JsonConvert.SerializeObject(reqOrderTrans), Logger.ConcatName(nameClass, nameMethod));
                goBusinessCommon.Instance.GetSignRequest(ref reqOrderTrans, config);
                res = goBusinessAction.Instance.GetBaseResponse(reqOrderTrans);
                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "ress" + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));

                return res;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public object GetParamsOrderTrans(BaseRequest request, string orderCode, string period, double totalPay, string payment_type, List<PaymentMix> lstPayMix, string status)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    Project = "",
                    UserName = request.Action.UserName,
                    orderCode = orderCode,
                    period = period,
                    totalPay = totalPay,
                    status = status,
                    payment_type = payment_type,
                    obj_paymix = JsonConvert.SerializeObject(lstPayMix)
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }

        public void GetResActionPayment(ref BaseResponse res, BaseResponse resOrdTrans, BaseRequest request, ResponseConfig config, string payment_type, List<OrderTransInfo> lstOrdTrans, List<OrdersInfo> lstOrd, List<OrderDetailInfo> lstOrdDetail, bool isSms = false, bool isEmail = false)
        {
            BaseRequest reqGateWays = new BaseRequest();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            switch (payment_type)
            {
                case nameof(goConstants.Payment_type.CTT):
                    if (lstOrdTrans != null && lstOrdTrans.Count > 1)
                        res = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    // Edit 2022-12-06 By ThienTVB add paytype to model
                    //reqGateWays = goBusinessOrders.Instance.GetReqGateways(request, lstOrdTrans[0]);
                    reqGateWays = goBusinessOrders.Instance.GetReqGatewaysV2(request, lstOrdTrans[0], payment_type);
                    // End 2022-12-06 By ThienTVB
                    //res = goBussinessPayment.Instance.payment(reqGateWays);
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "req ctt: " + JsonConvert.SerializeObject(reqGateWays), Logger.ConcatName(nameClass, nameMethod));

                    res = goBusPay.Instance.payment(reqGateWays);
                    break;
                case nameof(goConstants.Payment_type.CK):
                    //case nameof(goConstants.Payment_type.TM):
                    if (lstOrdTrans != null && lstOrdTrans.Count > 1)
                        res = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
                    // Edit 2022-12-06 By ThienTVB add paytype to model
                    //reqGateWays = goBusinessOrders.Instance.GetReqGateways(request, lstOrdTrans[0]);
                    reqGateWays = goBusinessOrders.Instance.GetReqGatewaysV2(request, lstOrdTrans[0], payment_type);
                    // End 2022-12-06 By ThienTVB
                    goConstants.Payment_type type = goConstants.Payment_type.CK;
                    //if (payment_type.Equals(nameof(goConstants.Payment_type.TM)))
                    //    type = goConstants.Payment_type.CK;

                    //goBussinessPayment.Instance.payment(reqGateWays, type);
                    //Edit 2022-12-06 BY ThienTVB tạo cổng thanh toán
                    //res = resOrdTrans;
                    res = goBusPay.Instance.payment(reqGateWays);
                    //End 2022-12-06 BY ThienTVB

                    if (isSms)
                    {
                        BaseRequest request2 = request;
                        request2.Action.ActionCode = goConstantsProcedure.SEND_SMS_HDI;

                        string mess = "QK vui long CK so tien " + new GO.HELPER.Utility.goUtility().FomatMoneyVN(lstOrdTrans[0].amount) + "VND voi noi dung " + lstOrdTrans[0].content_pay + " den STK HD Bank: 168704079888888-chu tai khoan CONG TY TNHH BAO HIEM HD";
                        object param = new
                        {
                            phone = lstOrdTrans[0].payer_phone,
                            mess = mess,
                            sms_id = "X",
                        };
                        request2.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                        try
                        {
                            new Task(() => goBussinessSMS.Instance.GetResponseSMS(request2)).Start();
                        }
                        catch (Exception)
                        {
                        }
                    }
                    if (isEmail)
                    {
                        object obj = new
                        {
                            NAME = lstOrdTrans[0].payer,
                            AMOUNT = new GO.HELPER.Utility.goUtility().FomatMoneyVN(lstOrdTrans[0].amount),
                            CONTENT = lstOrdTrans[0].content_pay,
                            DESCRIPTION = lstOrdTrans[0].description,
                            ORDER_TRANS = lstOrdTrans[0].order_trans_code,
                            ORDERID = lstOrdTrans[0].order_trans_code,
                            PAYER_NAME = lstOrdTrans[0].payer,
                            PAYER_MOBILE = lstOrdTrans[0].payer_phone,
                            PAYER_EMAIL = lstOrdTrans[0].payer_email
                        };
                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Send CK: " + JsonConvert.SerializeObject(obj), Logger.ConcatName(nameClass, nameMethod));
                        goBusinessServices.Instance.sendEmailContent(lstOrdTrans[0].org_code, "ND_CK", lstOrdTrans[0].payer_email, JsonConvert.DeserializeObject<JObject>(JsonConvert.SerializeObject(obj)));
                    }

                    break;
            }
        }

        public BaseRequest GetReqGateways(BaseRequest request, OrderTransInfo OrdTrans)
        {
            BaseRequest request_pay = new BaseRequest();
            ActionInfo action = new ActionInfo();
            OrderPay orderPay = new OrderPay();
            orderPay.appId = goConstants.Org_Code.HDI.ToString();
            orderPay.orderId = OrdTrans.order_trans_code;
            orderPay.amount = OrdTrans.amount;
            orderPay.payDate = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            orderPay.desc = OrdTrans.description;
            orderPay.desc_en = OrdTrans.description_en;
            orderPay.payer_name = OrdTrans.payer;
            orderPay.payer_mobile = OrdTrans.payer_phone;
            orderPay.payer_email = OrdTrans.payer_email;
            orderPay.contentPay = OrdTrans.content_pay;
            orderPay.struct_code = OrdTrans.struct_code;

            request_pay.Device = request.Device;
            action.ParentCode = request.Action.ParentCode;
            action.Secret = request.Action.Secret;
            action.UserName = request.Action.UserName;
            action.ActionCode = goConstantsProcedure.Pay_Get_Config;
            request_pay.Action = action;
            request_pay.Data = orderPay;
            return request_pay;
        }

        public BaseRequest GetReqGatewaysV2(BaseRequest request, OrderTransInfo OrdTrans, string payType)
        {
            BaseRequest request_pay = new BaseRequest();
            ActionInfo action = new ActionInfo();
            CreateTrans orderPay = new CreateTrans();
            //orderPay.appId = goConstants.Org_Code.HDI.ToString();
            orderPay.orgSeller = OrdTrans.org_seller;
            orderPay.productCode = OrdTrans.product_code;
            orderPay.structCode = OrdTrans.struct_code;
            orderPay.channel = OrdTrans.channel;
            orderPay.orderId = OrdTrans.order_trans_code;
            orderPay.payDate = DateTime.Now.ToString("yyMMddHHmm");
            orderPay.amount = OrdTrans.amount;
            orderPay.taxAmount = "0";
            orderPay.discountAmount = "0";
            orderPay.totalAmount = OrdTrans.amount;
            orderPay.desc = OrdTrans.description;
            orderPay.desc_En = OrdTrans.description_en;
            orderPay.contentPay = OrdTrans.content_pay;
            orderPay.payerName = OrdTrans.payer;
            orderPay.payerMobile = OrdTrans.payer_phone;
            orderPay.payerEmail = OrdTrans.payer_email;
            // Start 2022-12-06 By ThienTVB add PayType to model
            orderPay.payType = payType;
            // End 2022-12-06 By ThienTVB

            request_pay.Device = request.Device;
            action.ParentCode = request.Action.ParentCode;
            action.Secret = request.Action.Secret;
            action.UserName = request.Action.UserName;
            action.ActionCode = goConstantsProcedure.Pay_Get_Config;
            request_pay.Action = action;
            request_pay.Data = orderPay;

            return request_pay;
        }

        #endregion

        #region Order IPN
        public BaseResponse IpnOrder(ResponseOrderIpn responseIpn, string strTransCode, string strAmount)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                BaseResponse response = new BaseResponse();
                if (responseIpn.Success)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "IPN INFO: " + JsonConvert.SerializeObject(responseIpn), Logger.ConcatName(nameClass, nameMethod));
                    // update db.
                    object param = GetParamsIpnOrder(responseIpn);
                    string env = "";

                    goBusinessCommon.Instance.GetEnvironmentConfig(ref env);
                    BaseRequest requestUpdIpn = goBusinessCommon.Instance.GetBaseRequestHDI(env);
                    requestUpdIpn.Action.ActionCode = goConstantsProcedure.ORDER_IPN;
                    requestUpdIpn.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param));
                    response = goBusinessAction.Instance.GetBaseResponse(requestUpdIpn);
                    if (response.Success)
                    {
                        try
                        {
                            InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response, 0).FirstOrDefault();
                            List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response, 1);
                            if (insurContract.ORG_SELLER.Equals("CREDIFYVN"))
                            {
                                string strData = insurContract.CONTRACT_NO + ";";
                                int k = 1;
                                foreach (InsurDetail insu in insurDetails)
                                {
                                    if (insurDetails.Count > k)
                                        strData = strData + insu.CERTIFICATE_NO + ",";
                                    else
                                        strData = strData + insu.CERTIFICATE_NO;
                                    k++;
                                }
                                //JObject trans = new JObject();
                                //trans.Add("value", insurContract.AMOUNT);
                                //trans.Add("currency", insurContract.CURRENCY);
                                //JObject vat = new JObject();
                                //vat.Add("value", insurContract.VAT);
                                //vat.Add("currency", insurContract.CURRENCY);
                                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Ipn hoang: " + strTransCode, Logger.ConcatName(nameClass, nameMethod));
                                goPaymentLib.Instance.checkCallIPNPartner(insurContract.ORG_SELLER, strTransCode, strData, insurContract.TOTAL_AMOUNT.ToString(),
                                    insurContract.CURRENCY, insurContract.VAT.ToString(), insurContract.CURRENCY);
                            }
                            else if (insurContract.ORG_SELLER.Equals("GALAXY_CONNECT"))
                            {
                                //List<JObject> objTempValue = goBusinessCommon.Instance.GetData<JObject>(response, 1);
                                
                                string strData = insurContract.CONTRACT_NO + ";";
                                int k = 1;
                                foreach (InsurDetail insu in insurDetails)
                                {
                                    if (insurDetails.Count > k)
                                        strData = strData + insu.CERTIFICATE_NO + ",";
                                    else
                                        strData = strData + insu.CERTIFICATE_NO;
                                    k++;
                                }
                                //goPaymentLib.Instance.checkCallIPNGalaxyConnect(insurContract.ORG_SELLER, partner_Callback);
                            }
                            //if (insurContract != null && insurContract.CATEGORY.Equals(goConstants.CATEGORY_ATTD))
                            //{
                            DataIpnCent ipnCent = goBusinessCommon.Instance.GetData<DataIpnCent>(response, 2).FirstOrDefault();
                            //IpnCent ipnInfo = new IpnCent();
                            //ipnInfo.so_id = ipnCent.so_id;
                            //ipnInfo.tien = responseIpn.Data.paymentResult.order.totalCapturedAmount;

                            // ném ra ngoài
                            if (insurDetails != null)
                            {
                                BaseResponse res = goBusinessServices.Instance.signAndSendMail(insurContract, insurDetails, "PAYMENT", "WEB", "NO");
                                if (!res.Success)
                                {
                                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so gui mail IPN QR HDBANK: " + insurContract.CONTRACT_CODE, Logger.ConcatName(nameClass, nameMethod));
                                }
                                //if (insurContract.PRODUCT_CODE == "CSVX")
                                //{
                                //    //goBusinessServices.Instance.signEmailAll(insurContract.ORG_CODE, insurContract.PRODUCT_CODE, insurDetails[0].PACK_CODE, insurContract.CONTRACT_CODE, null);
                                //    BaseResponse res = goBusinessServices.Instance.signAndSendMail(insurContract, insurDetails, "PAYMENT", "WEB","NO");
                                //}                                    
                                //else
                                //{
                                //    foreach (var item in insurDetails)
                                //    {
                                //        BaseResponse res = goBusinessServices.Instance.signEmail(item);
                                //        if (!res.Success)
                                //        {
                                //            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi ky so gui mail ATTD: " + JsonConvert.SerializeObject(item), Logger.ConcatName(nameClass, nameMethod));
                                //        }
                                //    }
                                //}    

                            }

                            //2022-12-14 KHANHPT add callback Partner
                            if (insurContract.ORG_SELLER.Equals("GALAXY_CONNECT"))
                            {
                                //Get Data CallBack

                                JObject jobjCallback = new JObject();
                                jobjCallback.Add("A1", insurContract.CONTRACT_CODE);
                                jobjCallback.Add("A2", "");
                                jobjCallback.Add("A3", insurDetails[0].PRODUCT_CODE);
                                jobjCallback.Add("A4", "");
                                jobjCallback.Add("A5", "");
                                jobjCallback.Add("A6", "");
                                jobjCallback.Add("A7", "");
                                jobjCallback.Add("A8", "");
                                jobjCallback.Add("A9", "");
                                jobjCallback.Add("A10", "");
                                jobjCallback.Add("A11", "");
                                jobjCallback.Add("A12", "");
                                jobjCallback.Add("A13", "");
                                jobjCallback.Add("A14", "");
                                jobjCallback.Add("A15", "");
                                jobjCallback.Add("A16", "");

                                object[] paramFind = new object[] { "OPENAPI", "HDI_PRIVATE", jobjCallback };
                                BaseResponse resContractInfo = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Get_Callback_Data, env, paramFind);
                                List<JObject> objDetail = goBusinessCommon.Instance.GetData<JObject>(resContractInfo, 0);

                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Data callback: " + JsonConvert.SerializeObject(resContractInfo), Logger.ConcatName(nameClass, nameMethod));

                                //Get Transaction ID by  Order Trans Code

                                object[] paramTrans = new object[] { "HDI_PRIVATE", "HDI_PRIVATE", insurContract.ORG_SELLER, strTransCode};
                                BaseResponse resTrans = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Get_TransID_ByTranscode, env, paramTrans);
                                List<JObject> objTrans = goBusinessCommon.Instance.GetData<JObject>(resTrans, 0);

                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Transaction info: " + strTransCode + JsonConvert.SerializeObject(resTrans), Logger.ConcatName(nameClass, nameMethod));

                                //Call Partner IPN

                                if(objTrans.Count > 0)
                                {
                                    string transId = objTrans[0]["TRANS_ID"].ToString();

                                    goPaymentLib.Instance.checkCallIPNGalaxyConnect(insurContract.ORG_SELLER, transId, objDetail, true);

                                    //Update Transaction Status

                                    object[] paramTransUpdate = new object[] { "HDI_PRIVATE", "HDI_PRIVATE", insurContract.ORG_SELLER, transId, "SUCCESS" };
                                    BaseResponse resUpdateTrans = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Update_Trans_Status, env, paramTransUpdate);

                                    if (!resUpdateTrans.Success)
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai: " + transId, Logger.ConcatName(nameClass, nameMethod));
                                    }
                                }
                            }
                            //End

                            //new Task(() => goBusinessCentech.Instance.Ipn(ipnInfo)).Start();
                            //}
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "IPN CENT: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                            // send mail
                        }

                    }
                }
                else
                {
                    //BaseResponse baseResp = signEmail(responseIpn, "20201216000001", "akdaoe@gmail.com", "20/000/ATTD-HDI/NH/PC00321");
                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "IPN INFO: " + JsonConvert.SerializeObject(responseIpn), Logger.ConcatName(nameClass, nameMethod));
                    string env = "";

                    goBusinessCommon.Instance.GetEnvironmentConfig(ref env);
                    //Get Transaction ID by  Order Trans Code

                    object[] paramTrans = new object[] { "HDI_PRIVATE", "HDI_PRIVATE", "", strTransCode };
                    BaseResponse resTrans = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Get_TransID_ByTranscode, env, paramTrans);
                    List<JObject> objTrans = goBusinessCommon.Instance.GetData<JObject>(resTrans, 0);

                    Logger.Instance.WriteLog(Logger.TypeLog.INFO, "Transaction info: " + strTransCode + JsonConvert.SerializeObject(resTrans), Logger.ConcatName(nameClass, nameMethod));

                    //Call Partner IPN

                    if(objTrans.Count > 0)
                    {
                        string transId = objTrans[0]["TRANS_ID"].ToString();
                        string strOrg = objTrans[0]["ORG_SELLER"].ToString();

                        goPaymentLib.Instance.checkCallIPNGalaxyConnect(strOrg, transId, null, false);

                        //Update Transaction Status

                        object[] paramTransUpdate = new object[] { "HDI_PRIVATE", "HDI_PRIVATE", strOrg, transId, "FAILED" };
                        BaseResponse resUpdateTrans = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.Update_Trans_Status, env, paramTransUpdate);

                        if (!resUpdateTrans.Success)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Loi cap nhat trang thai: " + transId, Logger.ConcatName(nameClass, nameMethod));
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                throw ex;
            }
        }
        public BaseResponse signEmail(ResponseOrderIpn responseIpn, string strIDCerCen, string strEmailTo, string strconttractNo, InsurDetail item)
        {
            //thực hiện ký số
            BaseRequest requestSign = new BaseRequest();
            requestSign.Action = new ActionInfo();
            requestSign.Action.ActionCode = "HDI_CER_FILE";
            requestSign.Action.ParentCode = "HDI_PRIVATE";
            JObject jobSign = new JObject();
            jobSign.Add("USER_NAME", "HDI_PRIVATE");
            jobSign.Add("TEMP_CODE", "ATTD_GCN");
            jobSign.Add("TYPE_RES", "BASE64");
            jobSign.Add("PARAM_CODE", "ID");
            jobSign.Add("PARAM_VALUE", strIDCerCen);
            jobSign.Add("TRANSACTION_ID", responseIpn.Data.paymentResult.order.transactionNo);
            jobSign.Add("DATA_TYPE", "DATA");
            requestSign.Data = jobSign;
            BaseResponse basePDF = goBussinessPDF.Instance.GetBaseResponse(requestSign);
            if (basePDF.Success)
            {
                JObject jData = JObject.Parse(basePDF.Data.ToString());
                requestSign.Action.ActionCode = "HDI_SIGN";
                requestSign.Action.UserName = "CENTECH_VN";
                requestSign.Action.ParentCode = "CENTECH_VN";
                requestSign.Action.Secret = "B1B12FDEB22977ABE0551F6E21B40329";
                jobSign["USER_NAME"] = "TUANNV";
                jobSign.Add("LOCATION", "Hà Nội");
                jobSign.Add("REASON", "Cấp giấy chứng nhận Bảo hiểm");
                jobSign.Add("TypeResponse", "BASE64");
                jobSign.Add("TextNote", "");
                jobSign.Add("Alert", "");
                jobSign.Add("FILE_REFF", strconttractNo);
                jobSign.Add("TYPE_FILE", "FILE");
                jobSign.Add("FILE", jData["FILE_NAME"].ToString());
                requestSign.Data = jobSign;
                basePDF = goBussinessSign.Instance.GetBaseResponse(requestSign);
                string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, basePDF + " loi ky so " + basePDF.ErrorMessage, Logger.ConcatName(nameClass, nameMethod));
                //thực hiện gửi mail
                requestSign.Action.ActionCode = "HDI_EMAIL_01";
                requestSign.Action.UserName = "CENTECH_VN";
                jobSign = new JObject();
                jobSign.Add("subject", "Giấy chứng nhận bảo hiểm HDI cấp");
                JObject jEmailTo = JObject.Parse(" {\"email\":\"" + strEmailTo + "\"}");
                JArray jArrMailTo = new JArray();
                jArrMailTo.Add(jEmailTo);
                jobSign.Add("to", jArrMailTo);
                JObject jData1 = JObject.Parse(basePDF.Data.ToString());
                //thực hiện lấy tạm 1 html cho vào body mail -->phải lấy trong templace

                BaseResponse responseTemp = new BaseResponse();
                responseTemp = goBusinessCommon.Instance.executeProHDI(goConstantsProcedure.MD_Get_Temp_Value, "LIVE", new string[] { "CENTECH_VN", "EMAIL", "ATTD_GCN", "GCN", strconttractNo });
                List<JObject> objTempValue = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 1);
                string strBody = "";
                if (objTempValue == null || objTempValue[0].Count == 0)
                {
                    jobSign.Add("content", "");
                }
                else
                {
                    List<JObject> objTemp = goBusinessCommon.Instance.GetData<JObject>(responseTemp, 0);
                    List<string> lsReplace = objTemp[0]["PARAM_REPLACE"].ToString().Split(';').ToList();
                    strBody = goBussinessPDF.Instance.replaceHTML_JSON(goBussinessPDF.Instance.createJsonKeyValue(objTempValue), objTemp[0]["DATA"].ToString(), lsReplace);
                    jobSign.Add("content", strBody);
                }
                jobSign.Add("file", JArray.Parse("[{\"path\" : \"" + jData1["FILE_NAME"].ToString() + "\"}" + ",{\"path\" : \"" + jData1["FILE_ATTACH"]?.ToString() + "\"}" + "]"));
                requestSign.Data = jobSign;
                new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSign.Action.ParentCode, "ATTD", "Gửi giấy chứng nhận cho khách hàng", "EMAIL", "An tâm tín dụng GCN",
                    "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), strEmailTo, "", "", strBody, "X", "HIGH", "ALL", "abc", "Giấy chứng nhận bảo hiểm HDI cấp", requestSign.Action.UserName)).Start();
                goBussinessEmail.Instance.GetResponseEmail(requestSign);
            }
            return basePDF;
        }
        public object GetParamsIpnOrder(ResponseOrderIpn request)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                object param = new
                {
                    Project = "",
                    UserName = "HDI",
                    order_trans = request.Data.paymentResult.order.orderCode,
                    trans_id = request.Data.paymentResult.order.transactionNo,
                    date_pay = request.Data.paymentResult.order.payDate,
                    amount_pay = request.Data.paymentResult.order.totalCapturedAmount,
                    discount_pay = request.Data.paymentResult.order.totalRefundedAmount,
                    pay_method = request.Data.paymentResult.payMethod,
                    pay_type = request.Data.paymentResult.paymentType,
                    cashier_code = "",
                    org_pay = request.Data.paymentResult.order.bankCode
                };
                return param;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));

                // send mail
                throw ex;
            }
        }
        #endregion

        #region Đăng ký khách hàng GH
        public BaseResponse regis_staff(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null)
                {
                    //object[] paramSign = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);
                    //ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(request.Action.ActionCode, "LIVE", paramSign);
                    //response = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, paramSign), false, null, null, null, null);
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    JObject jParam = new JObject();
                    if (request.Action.ActionCode.Equals("UPDATE_REGIS"))
                    {
                        jParam.Add("CUS_CODE", jInput["CUS_CODE"].ToString());
                    }
                    string strActive = jInput["ACTIVATION_CODE"]?.ToString();
                    if (string.IsNullOrEmpty(strActive))
                        strActive = "null";
                    jParam.Add("ACTIVATION_CODE", strActive);

                    jParam.Add("ORG_CODE", jInput["ORG_CODE"].ToString());
                    jParam.Add("STAFF_CODE", jInput["STAFF_CODE"].ToString());
                    jParam.Add("STAFF_NAME", jInput["STAFF_NAME"].ToString());
                    jParam.Add("DOB", jInput["DOB"].ToString());
                    jParam.Add("BANK_ACCOUNT_NUM", jInput["BANK_ACCOUNT_NUM"].ToString());
                    jParam.Add("GENDER", jInput["GENDER"].ToString());
                    jParam.Add("IDCARD", jInput["IDCARD"].ToString());
                    jParam.Add("PROVINCE", jInput["PROVINCE"].ToString());
                    jParam.Add("DISTRICT", jInput["DISTRICT"].ToString());
                    jParam.Add("WARDS", jInput["WARDS"].ToString());
                    jParam.Add("ADDRESS", jInput["ADDRESS"].ToString());
                    jParam.Add("EMAIL", jInput["EMAIL"].ToString());
                    jParam.Add("PHONE", jInput["PHONE"].ToString());
                    jParam.Add("TOTAL_AMOUNT", jInput["TOTAL_AMOUNT"].ToString());
                    jParam.Add("PAID_BY_SAL", jInput["PAID_BY_SAL"].ToString());
                    jParam.Add("GH_ORDER_INSUR", jInput["GH_ORDER_INSUR"]);

                    request.Data = jParam;
                    goBusinessCommon.Instance.GetSignRequest(ref request, config);
                    response = goBusinessAction.Instance.GetBaseResponse(request);
                    List<JObject> obj = goBusinessCommon.Instance.GetData<JObject>(response, 0);
                    if (obj != null && obj.Count != 0)
                    {
                        string strCount = obj[0]["COUNT"]?.ToString();
                        if (!string.IsNullOrEmpty(strCount))
                        {
                            response = goBusinessCommon.Instance.getResultApi(false, obj[0]["CUS_CODE"]?.ToString(), config, nameof(goConstantsError.Instance.ERROR_6001),
                                "Khách hàng " + obj[0]["STAFF_NAME"]?.ToString() + "(" + obj[0]["STAFF_CODE"]?.ToString() + ") đã đăng ký. Vui lòng truy cập mail " + obj[0]["EMAIL"]?.ToString() + " để cập nhật thông tin!");
                            return response;
                        }
                        List<object> lsData = new List<object>();
                        lsData.Add(goBusinessCommon.Instance.GetData<JObject>(response, 0)[0]);
                        lsData.Add(goBusinessCommon.Instance.GetData<JObject>(response, 1));
                        //thực hiện gửi mail xác thực
                        object[] param = new object[] { obj[0]["ORG_CODE"]?.ToString(), "EMAIL", "CSSK_DK" };
                        ModelBusiness.ExecuteInfo GetInfoExecute = goBusinessCommon.Instance.GetInfoExecute(goConstantsProcedure.ser_get_temp, strEvm, param);
                        BaseResponse response1 = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(GetInfoExecute.ModeDB, GetInfoExecute.connectString, GetInfoExecute.store, param), false, null, null, null, null);
                        List<JObject> lsObjtemp = goBusinessCommon.Instance.GetData<JObject>(response1, 0);
                        List<string> lsStringBody = goBusinessTemplate.Instance.fillValueToHtml(lsData, response1);
                        string strBody = lsStringBody[0];
                        if (obj[0]["ORG_CODE"].ToString().Equals("HDBANK_VN"))
                        {
                            strBody = strBody.Replace("@LINK_XAC_NHAN@", obj[0]["CUS_CODE"].ToString());
                            strBody = strBody.Replace("@LINK_SUA@", obj[0]["CUS_CODE"].ToString());
                        }
                        else
                        {
                            strBody = strBody.Replace("@LINK_XAC_NHAN@", obj[0]["CUS_CODE"].ToString());
                            strBody = strBody.Replace("@LINK_SUA@", obj[0]["CUS_CODE"].ToString());
                        }
                        //thực hiện gửi mail
                        BaseRequest requestSign = new BaseRequest();
                        requestSign.Action = request.Action;
                        requestSign.Device = request.Device;
                        requestSign.Action.ActionCode = "HDI_EMAIL_01";
                        requestSign.Action.UserName = "HDI_PRIVATE";
                        JObject jobSign = new JObject();
                        jobSign.Add("subject", lsObjtemp[0]["TITLE"].ToString());
                        JObject jEmailTo = JObject.Parse(" {\"email\":\"" + obj[0]["EMAIL"]?.ToString() + "\"}");
                        JArray jArrMailTo = new JArray();
                        jArrMailTo.Add(jEmailTo);
                        jobSign.Add("to", jArrMailTo);
                        List<string> lsBCC = lsObjtemp[0]["LIST_REPLACE"].ToString().Split(';').ToList();
                        foreach (string strMail in lsBCC)
                        {
                            if (!string.IsNullOrEmpty(strMail) && strMail.Length > obj[0]["ORG_CODE"].ToString().Length
                                && strMail.Substring(0, obj[0]["ORG_CODE"].ToString().Length).Equals(obj[0]["ORG_CODE"].ToString()))
                            {
                                List<string> lsEmailBCC = strMail.Substring(obj[0]["ORG_CODE"].ToString().Length + 1).Split(',').ToList();
                                JArray jArrMailBCC = new JArray();
                                foreach (string emaiBcc in lsEmailBCC)
                                {
                                    JObject jEmailBCC = JObject.Parse(" {\"email\":\"" + emaiBcc + "\"}");//
                                    jArrMailBCC.Add(jEmailBCC);
                                }

                                jobSign.Add("bcc", jArrMailBCC);
                                break;
                            }
                        }
                        jobSign.Add("content", strBody);
                        requestSign.Data = jobSign;
                        new Task(() => goBussinessEmail.Instance.createDBSendMail(requestSign.Action.ParentCode, "CSSK_DK", "Gửi thông báo đăng ký", "EMAIL", "Trâu vàng sức khỏe vàng",
                            "ON_DEMAND", DateTime.Now.ToString("dd/MM/yyyy"), obj[0]["EMAIL"]?.ToString(), "", "", strBody, "X", "HIGH", "ALL", "abc", "Thông báo", requestSign.Action.UserName)).Start();
                        goBusinessCommon.Instance.GetSignRequest(ref requestSign, config);
                        goBussinessEmail.Instance.GetResponseEmail(requestSign);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
            return response;
        }
        public BaseResponse update_regis_staff(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null)
                {
                    JObject jInput = JObject.Parse(request.Data.ToString());
                    JObject jParam = new JObject();
                    jParam.Add("CUS_CODE", jInput["CUS_CODE"].ToString());
                    jParam.Add("ORG_CODE", jInput["ORG_CODE"].ToString());
                    jParam.Add("STAFF_CODE", jInput["STAFF_CODE"].ToString());
                    jParam.Add("STAFF_NAME", jInput["STAFF_NAME"].ToString());
                    jParam.Add("DOB", jInput["DOB"].ToString());
                    jParam.Add("BANK_ACCOUNT_NUM", jInput["BANK_ACCOUNT_NUM"].ToString());
                    jParam.Add("GENDER", jInput["GENDER"].ToString());
                    jParam.Add("IDCARD", jInput["IDCARD"].ToString());
                    jParam.Add("PROVINCE", jInput["PROVINCE"].ToString());
                    jParam.Add("DISTRICT", jInput["DISTRICT"].ToString());
                    jParam.Add("WARDS", jInput["WARDS"].ToString());
                    jParam.Add("ADDRESS", jInput["ADDRESS"].ToString());
                    jParam.Add("EMAIL", jInput["EMAIL"].ToString());
                    jParam.Add("PHONE", jInput["PHONE"].ToString());
                    jParam.Add("STATUS", jInput["STATUS"].ToString());
                    jParam.Add("GH_ORDER_INSUR", jInput["GH_ORDER_INSUR"]);

                    request.Data = jParam;
                    goBusinessCommon.Instance.GetSignRequest(ref request, config);
                    response = goBusinessAction.Instance.GetBaseResponse(request);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
            return response;
        }
        public BaseResponse get_staff_byID(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null)
                {

                    goBusinessCommon.Instance.GetSignRequest(ref request, config);
                    response = goBusinessAction.Instance.GetBaseResponse(request);

                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
            return response;
        }

        public BaseResponse regis_staff_create_insur(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if (request.Data != null)
                {
                    BaseResponse response_get = new BaseResponse();
                    response_get = goBusinessAction.Instance.GetBaseResponse(request);
                    if (response_get.Success)
                    {
                        List<StaffRegister> lstStaff = new List<StaffRegister>();
                        List<InsuredRegister> lstInsured = new List<InsuredRegister>();
                        lstStaff = goBusinessCommon.Instance.GetData<StaffRegister>(response_get, 0);
                        lstInsured = goBusinessCommon.Instance.GetData<InsuredRegister>(response_get, 1);
                        if (lstStaff != null && lstInsured != null)
                        {
                            foreach (var item in lstStaff)
                            {
                                Logger.Instance.WriteLog(Logger.TypeLog.INFO, "item_create, model: " + JsonConvert.SerializeObject(item), Logger.ConcatName(nameClass, nameMethod));

                                List<InsuredRegister> lstInsuredByStaff = new List<InsuredRegister>();
                                lstInsuredByStaff = lstInsured.Where(i => i.STAFF_CODE == item.STAFF_CODE).ToList();
                                if (lstInsuredByStaff != null)
                                {
                                    BaseResponse response_cr = new BaseResponse();
                                    OrdersModel ordersModel = GetOrdersByRegister(item, lstInsuredByStaff);
                                    response_cr = goBusinessOrders.Instance.SaveOrderBase(request, ordersModel, config);
                                    if (response_cr.Success)
                                    {
                                        OrderResInfo resInfoOrder = goBusinessCommon.Instance.GetData<OrderResInfo>(response_cr, 0).FirstOrDefault();
                                        // send mail
                                        try
                                        {
                                            if (resInfoOrder != null && resInfoOrder.status.Equals(nameof(goConstants.OrderStatus.PAID)))
                                            {
                                                InsurContract insurContract = goBusinessCommon.Instance.GetData<InsurContract>(response_cr, 2).FirstOrDefault();
                                                List<InsurDetail> insurDetails = goBusinessCommon.Instance.GetData<InsurDetail>(response_cr, 3);
                                                //send mail GCN theo insurDetails
                                                BaseResponse res = goBusinessServices.Instance.signEmail_cssk(insurContract, insurDetails);
                                                //JArray arrSendMail = new JArray();
                                                //foreach (InsurDetail insurDetail in insurDetails)
                                                //{
                                                //    BaseResponse res = goBusinessServices.Instance.signEmail(insurDetails[0]);
                                                //    if (res.Success)
                                                //    {
                                                //        //response.Data = res.Data;
                                                //        //arrSendMail.Add(res.Data);
                                                //    }
                                                //    else
                                                //    {
                                                //        //response.Success = false;
                                                //        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));
                                                //        goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails[0]));
                                                //    }
                                                //}
                                                response.Data = res.Data;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "send mail voucher: " + ex.Message, Logger.ConcatName(nameClass, nameMethod));
                                        }
                                        BaseRequest req_udp_staff = new BaseRequest();
                                        req_udp_staff = request;
                                        object[] param = null;
                                        if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                                            param = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

                                        object param_udp = new
                                        {
                                            channel = param[0],
                                            username = param[1],
                                            orgcode = param[2],
                                            product_code = param[3],
                                            package_code = param[4],
                                            staff_code = item.STAFF_CODE
                                        };
                                        req_udp_staff.Action.ActionCode = goConstantsProcedure.UDP_STAFF_ORDER_CREATE;
                                        req_udp_staff.Data = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(param_udp));
                                        BaseResponse res_udp = goBusinessAction.Instance.GetBaseResponse(req_udp_staff);
                                        Logger.Instance.WriteLog(Logger.TypeLog.INFO, "update done, staff: " + item.STAFF_CODE + " - json response: " + JsonConvert.SerializeObject(res_udp), Logger.ConcatName(nameClass, nameMethod));
                                    }
                                    else
                                    {
                                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "create register fail, model: " + JsonConvert.SerializeObject(ordersModel), Logger.ConcatName(nameClass, nameMethod));
                                        goBussinessEmail.Instance.sendEmaiCMS("", "create register fail, model: ", JsonConvert.SerializeObject(ordersModel));
                                    }
                                }
                            }
                        }
                        else
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "null data: " + JsonConvert.SerializeObject(response_get), Logger.ConcatName(nameClass, nameMethod));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_6001), goConstantsError.Instance.ERROR_6001);
            }
            finally
            {
                try
                {
                    AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, response);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                }
            }
            return response;
        }

        public OrdersModel GetOrdersByRegister(StaffRegister staffInfo, List<InsuredRegister> lstInsuredRegister)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = staffInfo.CHANNEL;
                ordersModel.UserName = staffInfo.ORG_CODE;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.SELLER_CODE = staffInfo.ORG_CODE;
                seller.STRUCT_CODE = staffInfo.ORG_CODE;
                seller.ORG_CODE = staffInfo.ORG_CODE;
                seller.ENVIROMENT = staffInfo.CHANNEL;
                ordersModel.COMMON.SELLER = seller;
                #endregion
                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = staffInfo.TYPE;
                buyer.NAME = staffInfo.STAFF_NAME;
                buyer.DOB = staffInfo.DOB;
                buyer.GENDER = staffInfo.GENDER;
                buyer.PROV = staffInfo.PROV;
                buyer.DIST = staffInfo.DIST;
                buyer.WARDS = staffInfo.WARDS;
                buyer.ADDRESS = staffInfo.ADDRESS;
                buyer.IDCARD = staffInfo.IDCARD;
                buyer.IDCARD_D = staffInfo.IDCARD_D;
                buyer.IDCARD_P = staffInfo.IDCARD_P;
                buyer.EMAIL = staffInfo.EMAIL;
                buyer.PHONE = staffInfo.PHONE;
                buyer.FAX = staffInfo.FAX;
                buyer.TAXCODE = staffInfo.TAXCODE;
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 100;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = staffInfo.ACTIVATION_CODE;
                order.STATUS = staffInfo.STATUS_ORD;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = staffInfo.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 100;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm qua voucher giảm 100%";
                orderDetail.EFFECTIVE_DATE = staffInfo.EFF_DATE;
                orderDetail.EXPIRATION_DATE = staffInfo.EXP_DATE;
                orderDetail.PRODUCT_CODE = staffInfo.PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance
                List<InsuredPerson> lstInsured = new List<InsuredPerson>();
                foreach (var item in lstInsuredRegister)
                {
                    InsuredPerson insured = new InsuredPerson();
                    insured.TYPE = item.TYPE;
                    insured.NAME = item.NAME;
                    insured.DOB = item.DOB;
                    insured.GENDER = item.GENDER;
                    insured.IDCARD = item.IDCARD;
                    insured.PROV = item.PROV;
                    insured.DIST = item.DIST;
                    insured.WARDS = item.WARDS;
                    insured.ADDRESS = item.ADDRESS;
                    insured.EMAIL = item.EMAIL;
                    insured.PHONE = item.PHONE;
                    insured.RELATIONSHIP = item.RELATIONSHIP;
                    insured.PRODUCT_CODE = item.PRODUCT_CODE;
                    insured.PACK_CODE = item.PACK_CODE;
                    insured.EFFECTIVE_DATE = item.EFF_DATE;
                    insured.EXPIRATION_DATE = item.EXP_DATE;
                    //insured.AMOUNT = item.FULL_FEES;
                    insured.DISCOUNT = 0;
                    insured.DISCOUNT_UNIT = "";
                    insured.VAT = 0;
                    //insured.TOTAL_AMOUNT = item.FULL_FEES;
                    lstInsured.Add(insured);
                }
                if (staffInfo.CATEGORY.Equals(goConstants.CATEGORY_SK))
                {
                    HealthCare healthCare = new HealthCare();
                    healthCare.INSURED = lstInsured;
                    ordersModel.BUSINESS.HEALTH_CARE = healthCare;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Chiến sỹ vắc xin
        public OrdersModel GetOrdersCSVX(ReqCsvx reqCsvx)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = reqCsvx.CHANNEL;
                ordersModel.UserName = "HDI_AUTO";

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = nameof(goConstants.Org_Code.HDI);
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.WEB_LANDING);
                ordersModel.COMMON.SELLER = seller;
                #endregion
                #region Buyer - Người mua
                ORD_BUYER buyer = new ORD_BUYER();
                buyer.TYPE = reqCsvx.BUYER.TYPE;
                buyer.NAME = reqCsvx.BUYER.NAME;
                buyer.DOB = reqCsvx.BUYER.DOB;
                buyer.GENDER = reqCsvx.BUYER.GENDER;
                buyer.PROV = reqCsvx.BUYER.PROV;
                buyer.DIST = reqCsvx.BUYER.DIST;
                buyer.WARDS = reqCsvx.BUYER.WARDS;
                buyer.ADDRESS = reqCsvx.BUYER.ADDRESS;
                buyer.IDCARD = reqCsvx.BUYER.IDCARD;
                buyer.IDCARD_D = reqCsvx.BUYER.IDCARD_D;
                buyer.IDCARD_P = reqCsvx.BUYER.IDCARD_P;
                buyer.EMAIL = reqCsvx.BUYER.EMAIL;
                buyer.PHONE = reqCsvx.BUYER.PHONE;
                buyer.FAX = reqCsvx.BUYER.FAX;
                buyer.TAXCODE = reqCsvx.BUYER.TAXCODE;
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = nameof(goConstants.TypeOrder.BH_M);
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                //order.STATUS = "";
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = "CN.02";
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm chiến sỹ vắc xin";
                orderDetail.EFFECTIVE_DATE = goUtility.Instance.ConvertToDateTime(reqCsvx.INSURED[0].EFFECTIVE_DATE, DateTime.MinValue).ToString("dd/MM/yyyy") + " 08:00:00";
                orderDetail.EXPIRATION_DATE = goUtility.Instance.ConvertToDateTime(reqCsvx.INSURED[0].EFFECTIVE_DATE, DateTime.MinValue).AddYears(1).ToString("dd/MM/yyyy") + " 08:00:00";
                orderDetail.PRODUCT_CODE = reqCsvx.INSURED[0].PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance
                List<InsuredPerson> lstInsured = new List<InsuredPerson>();
                foreach (var item in reqCsvx.INSURED)
                {
                    InsuredPerson insured = new InsuredPerson();
                    insured.TYPE = item.TYPE;
                    insured.NAME = item.NAME;
                    insured.DOB = item.DOB;
                    insured.GENDER = item.GENDER;
                    insured.IDCARD = item.IDCARD;
                    insured.PROV = item.PROV;
                    insured.DIST = item.DIST;
                    insured.WARDS = item.WARDS;
                    insured.ADDRESS = item.ADDRESS;
                    insured.EMAIL = item.EMAIL;
                    insured.PHONE = item.PHONE;
                    insured.RELATIONSHIP = item.RELATIONSHIP;
                    insured.PRODUCT_CODE = item.PRODUCT_CODE;
                    insured.PACK_CODE = item.PACK_CODE;
                    insured.EFFECTIVE_DATE = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue).ToString("dd/MM/yyyy") + " 08:00:00";
                    insured.EXPIRATION_DATE = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue).AddYears(1).ToString("dd/MM/yyyy") + " 08:00:00";
                    //insured.AMOUNT = item.FULL_FEES;
                    insured.DISCOUNT = 0;
                    insured.DISCOUNT_UNIT = "";
                    insured.VAT = 0;
                    //insured.TOTAL_AMOUNT = item.FULL_FEES;
                    lstInsured.Add(insured);
                }
                HealthCare healthCare = new HealthCare();
                healthCare.INSURED = lstInsured;
                ordersModel.BUSINESS.HEALTH_CARE = healthCare;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Order transfer
        public BaseResponse ApproveOrdTransfer(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                BaseResponse resApprove = goBusinessAction.Instance.GetBaseResponse(request);
                //Logger.Instance.WriteLog(Logger.TypeLog.DEGBUG, "ApproveOrdTransfer: " + JsonConvert.SerializeObject(resApprove), Logger.ConcatName(nameClass, nameMethod));
                if (resApprove.Success)
                {
                    List<OrdersTransferApprove> lstApprove = goBusinessCommon.Instance.GetData<OrdersTransferApprove>(resApprove, 0);
                    //JArray array = new JArray();
                    foreach (var item in lstApprove)
                    {
                        IpnHDIModels obj = new IpnHDIModels();
                        obj.amount = item.TOTAL_PAY.ToString();
                        obj.pay_type = "CK";
                        obj.order_trans_code = item.ORDER_TRANS_CODE;
                        obj.message = "Thành công";
                        obj.code = "00";
                        obj.trans_id = "-1";
                        BaseResponse res = goBussinessPayment.Instance.ipnHDITrans(obj);
                        if (res.Success)
                        {
                            //JObject jobRes = JObject.Parse(res.Data.ToString());
                            //array.Add(jobRes);
                        }

                        // IPN
                    }
                    response.Success = true;
                    //response.Data = array;
                }
                else
                    response = resApprove;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
            }
            return response;
        }
        #endregion

        #region Xe cơ giới
        // 1 xe
        public OrdersModel GetOrderVehicle(VEHICLE_PRODUCT vehicleProduct, OutDynamicFees outDynamic, string ORG_CODE, string CHANNEL, string USERNAME, string action, ORD_BUYER buyer, Customer BILL_INFO, ORD_SKU ordSku, ORD_PART ordPart)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = CHANNEL;
                ordersModel.UserName = USERNAME;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = ORG_CODE;
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.SELLING);
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = action;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                //if (vehicleProduct.GIFTS != null && vehicleProduct.GIFTS.Any())
                //{
                //    foreach (var item in vehicleProduct.GIFTS)
                //    {
                //        if (string.IsNullOrEmpty(arr_gift))
                //            arr_gift = item.GIFT_CODE;
                //        else
                //            arr_gift = arr_gift + ";" + item.GIFT_CODE;
                //    }
                //}
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);

                // check trạng thái của đơn
                // nếu TNDS đã cấp bản cứng => đã thanh toán
                if (vehicleProduct.COMPULSORY_CIVIL != null && vehicleProduct.PHYSICAL_DAMAGE == null && vehicleProduct.COMPULSORY_CIVIL.IS_SERIAL_NUM == "1")
                {
                    //order.STATUS = goConstants.OrderStatus.PAID.ToString();
                }

                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_XE;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm xe cơ giới";

                string eff_date = "", exp_date = "", product_code = "";
                if (vehicleProduct.COMPULSORY_CIVIL != null && vehicleProduct.PHYSICAL_DAMAGE == null)
                {
                    eff_date = vehicleProduct.COMPULSORY_CIVIL.EFF + " " + vehicleProduct.COMPULSORY_CIVIL.TIME_EFF;
                    exp_date = vehicleProduct.COMPULSORY_CIVIL.EXP + " " + vehicleProduct.COMPULSORY_CIVIL.TIME_EXP;
                }
                else if (vehicleProduct.COMPULSORY_CIVIL == null && vehicleProduct.PHYSICAL_DAMAGE != null)
                {
                    eff_date = vehicleProduct.PHYSICAL_DAMAGE.EFF + " " + vehicleProduct.PHYSICAL_DAMAGE.TIME_EFF;
                    exp_date = vehicleProduct.PHYSICAL_DAMAGE.EXP + " " + vehicleProduct.PHYSICAL_DAMAGE.TIME_EXP;
                }
                else if (vehicleProduct.COMPULSORY_CIVIL != null && vehicleProduct.PHYSICAL_DAMAGE != null)
                {
                    eff_date = vehicleProduct.COMPULSORY_CIVIL.EFF + " " + vehicleProduct.COMPULSORY_CIVIL.TIME_EFF;
                    exp_date = vehicleProduct.COMPULSORY_CIVIL.EXP + " " + vehicleProduct.COMPULSORY_CIVIL.TIME_EXP;
                }
                orderDetail.EFFECTIVE_DATE = eff_date;
                orderDetail.EXPIRATION_DATE = exp_date;

                if (vehicleProduct.COMPULSORY_CIVIL != null && vehicleProduct.PHYSICAL_DAMAGE == null)
                {
                    if (vehicleProduct.VOLUNTARY_CIVIL != null)
                        product_code = goConstants.Product_Xe.XCG_TNDSBB.ToString();
                    else
                        product_code = goConstants.Product_Xe.XCG_TNDSBB.ToString();
                }
                else if (vehicleProduct.COMPULSORY_CIVIL == null && vehicleProduct.PHYSICAL_DAMAGE != null)
                {
                    if (vehicleProduct.VOLUNTARY_CIVIL != null)
                        product_code = goConstants.Product_Xe.XCG_VCX_NEW.ToString();
                    else
                        product_code = goConstants.Product_Xe.XCG_VCX_NEW.ToString();
                }
                else if (vehicleProduct.COMPULSORY_CIVIL != null && vehicleProduct.PHYSICAL_DAMAGE != null)
                {
                    product_code = goConstants.Product_Xe.XCG_HH.ToString();
                }
                orderDetail.PRODUCT_CODE = product_code;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region Thông tin bill
                ordersModel.COMMON.BILL_INFO = BILL_INFO;
                #endregion

                #region Order sku
                ordersModel.COMMON.ORD_SKU = ordSku;
                #endregion

                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = ordPart;
                #endregion


                #region List Insurance
                VehicleInsur vehicleInsur = new VehicleInsur();
                vehicleInsur.ORG_CODE = ORG_CODE;
                vehicleInsur.CHANNEL = CHANNEL;
                vehicleInsur.USERNAME = USERNAME;
                vehicleInsur.VEHICLE_INSUR = new List<VEHICLE_PRODUCT>();
                vehicleInsur.VEHICLE_INSUR.Add(vehicleProduct);
                ordersModel.BUSINESS.VEHICLE = vehicleInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;

        }

        // nhiều xe phase 1
        public OrdersModel GetOrderVehicle(List<VEHICLE_PRODUCT> lstVehicle, string ORG_CODE, string CHANNEL, string USERNAME, string action, ORD_BUYER buyer, Customer BILL_INFO, ORD_SKU ordSku, ORD_PART ordPart)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = CHANNEL;
                ordersModel.UserName = USERNAME;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = ORG_CODE;
                seller.SELLER_CODE = USERNAME;
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.SELLING);
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = buyer;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = action;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion

                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_XE;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm xe cơ giới";

                //string eff_date = "", exp_date = "", product_code = "";

                DateTime date_eff = DateTime.MinValue, date_exp = DateTime.MinValue;
                foreach (var item in lstVehicle)
                {

                    DateTime d_eff = DateTime.MinValue;
                    DateTime d_exp = DateTime.MinValue;
                    string str_d_eff = "", str_d_exp = "";
                    if (item.COMPULSORY_CIVIL != null && item.PHYSICAL_DAMAGE == null)
                    {
                        str_d_eff = item.COMPULSORY_CIVIL.EFF + " " + item.COMPULSORY_CIVIL.TIME_EFF;
                        str_d_exp = item.COMPULSORY_CIVIL.EXP + " " + item.COMPULSORY_CIVIL.TIME_EXP;
                    }
                    else if (item.COMPULSORY_CIVIL == null && item.PHYSICAL_DAMAGE != null)
                    {
                        str_d_eff = item.PHYSICAL_DAMAGE.EFF + " " + item.PHYSICAL_DAMAGE.TIME_EFF;
                        str_d_exp = item.PHYSICAL_DAMAGE.EXP + " " + item.PHYSICAL_DAMAGE.TIME_EXP;
                    }
                    else if (item.COMPULSORY_CIVIL != null && item.PHYSICAL_DAMAGE != null)
                    {
                        str_d_eff = item.COMPULSORY_CIVIL.EFF + " " + item.COMPULSORY_CIVIL.TIME_EFF;
                        str_d_exp = item.COMPULSORY_CIVIL.EXP + " " + item.COMPULSORY_CIVIL.TIME_EXP;
                    }

                    d_eff = goUtility.Instance.ConvertToDateTime(str_d_eff, DateTime.MinValue);
                    d_exp = goUtility.Instance.ConvertToDateTime(str_d_exp, DateTime.MinValue);

                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }

                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");

                string product_code = "";
                foreach (var item in lstVehicle)
                {
                    if (item.IS_COMPULSORY != null && item.IS_COMPULSORY == "1")
                    {
                        product_code = goConstants.Product_Xe.XCG_TNDSBB.ToString();
                    }
                    else if (item.IS_COMPULSORY != null && item.IS_COMPULSORY == "0" && item.PHYSICAL_DAMAGE != null && item.IS_PHYSICAL == "1")
                    {
                        product_code = goConstants.Product_Xe.XCG_VCX_NEW.ToString();
                    }
                }
                orderDetail.PRODUCT_CODE = product_code;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region Thông tin bill
                ordersModel.COMMON.BILL_INFO = BILL_INFO;
                #endregion

                #region Thông tin bill
                ordersModel.COMMON.BILL_INFO = BILL_INFO;
                #endregion

                #region Order sku
                ordersModel.COMMON.ORD_SKU = ordSku;
                #endregion

                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = ordPart;
                #endregion

                #region List Insurance
                VehicleInsur vehicleInsur = new VehicleInsur();
                vehicleInsur.ORG_CODE = ORG_CODE;
                vehicleInsur.CHANNEL = CHANNEL;
                vehicleInsur.USERNAME = USERNAME;
                vehicleInsur.VEHICLE_INSUR = lstVehicle;
                ordersModel.BUSINESS.VEHICLE = vehicleInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;

        }

        /// <summary>
        /// Trường hợp n xe và không tách đơn và trên mỗi xe không tách đơn 
        /// </summary>
        /// <param name="vehicleInsur"></param>
        /// <param name="outDynamic"></param>
        /// <returns></returns>
        public OrdersModel GetOrderVehicleVer3(VehicleInsur vehicleInsur)//, OutDynamicFeesVer3 outDynamic
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = vehicleInsur.CHANNEL;
                ordersModel.UserName = vehicleInsur.USERNAME;

                #region Seller - Nhân viên bán
                ORD_SELLER seller = new ORD_SELLER();
                seller.ORG_CODE = vehicleInsur.ORG_CODE;
                seller.SELLER_CODE = vehicleInsur.USERNAME;
                seller.ENVIROMENT = nameof(goConstants.EnviromentHDI.SELLING);
                ordersModel.COMMON.SELLER = seller;
                #endregion

                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = vehicleInsur.BUYER;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = vehicleInsur.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                //if (vehicleProduct.GIFTS != null && vehicleProduct.GIFTS.Any())
                //{
                //    foreach (var item in vehicleProduct.GIFTS)
                //    {
                //        if (string.IsNullOrEmpty(arr_gift))
                //            arr_gift = item.GIFT_CODE;
                //        else
                //            arr_gift = arr_gift + ";" + item.GIFT_CODE;
                //    }
                //}
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion

                #region bill
                ordersModel.COMMON.BILL_INFO = vehicleInsur.BILL_INFO;
                #endregion

                #region Order sku
                ordersModel.COMMON.ORD_SKU = vehicleInsur.ORD_SKU;
                #endregion

                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = vehicleInsur.ORD_PARTNER;
                #endregion


                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();

                // trường hợp nhiều xe thì mỗi xe là 1 detail
                foreach (var item in vehicleInsur.VEHICLE_INSUR)
                {
                    ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                    orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                    orderDetail.PRODUCT_MODE = nameof(goConstants.ModeInsurance.TRUC_TIEP);
                    orderDetail.PRODUCT_TYPE = goConstants.CATEGORY_XE;
                    orderDetail.COUNT = 1;
                    orderDetail.AMOUNT = 0;
                    orderDetail.DISCOUNT = 0;
                    orderDetail.DISCOUNT_UNIT = "P";
                    orderDetail.VAT = 0;
                    orderDetail.TOTAL_AMOUNT = 0;
                    orderDetail.DESCRIPTION = "Đơn bảo hiểm xe cơ giới";

                    string eff_date = "", exp_date = "", product_code = "";

                    if (item.COMPULSORY_CIVIL != null && item.PHYSICAL_DAMAGE == null)
                    {
                        eff_date = item.COMPULSORY_CIVIL.EFF + " " + item.COMPULSORY_CIVIL.TIME_EFF;
                        exp_date = item.COMPULSORY_CIVIL.EXP + " " + item.COMPULSORY_CIVIL.TIME_EXP;
                    }
                    else if (item.COMPULSORY_CIVIL == null && item.PHYSICAL_DAMAGE != null)
                    {
                        eff_date = item.PHYSICAL_DAMAGE.EFF + " " + item.PHYSICAL_DAMAGE.TIME_EFF;
                        exp_date = item.PHYSICAL_DAMAGE.EXP + " " + item.PHYSICAL_DAMAGE.TIME_EXP;
                    }
                    else if (item.COMPULSORY_CIVIL != null && item.PHYSICAL_DAMAGE != null)
                    {
                        eff_date = item.COMPULSORY_CIVIL.EFF + " " + item.COMPULSORY_CIVIL.TIME_EFF;
                        exp_date = item.COMPULSORY_CIVIL.EXP + " " + item.COMPULSORY_CIVIL.TIME_EXP;
                    }
                    orderDetail.EFFECTIVE_DATE = eff_date;
                    orderDetail.EXPIRATION_DATE = exp_date;

                    if (item.COMPULSORY_CIVIL == null && item.PHYSICAL_DAMAGE != null)
                        product_code = item.PHYSICAL_DAMAGE.PRODUCT_CODE;//goConstants.Product_Xe.XCG_VCX_NEW.ToString();

                    if (item.COMPULSORY_CIVIL != null && item.IS_COMPULSORY == "1")
                        product_code = item.COMPULSORY_CIVIL.PRODUCT_CODE;

                    orderDetail.PRODUCT_CODE = product_code;

                    if (vehicleInsur.ACTION.Equals("BH_S"))
                    {
                        orderDetail.REF_ID = item.CONTRACT_CODE;
                    }

                    lstOrderDetail.Add(orderDetail);
                    break; // 1 đơn nhiều chi tiết 
                }


                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion

                #region List Insurance
                ordersModel.BUSINESS.VEHICLE = vehicleInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Cấp đơn theo lô - import
        public OrdersModel GetOrderCommonByReq(ReqOrdersCommon reqOrder)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = nameof(goConstants.Org_Code.VIETJET_VN);
                ordersModel.UserName = "HDI_AUTO";

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = reqOrder.SELLER;
                #endregion

                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = reqOrder.BUYER;
                #endregion

                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = reqOrder.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                order.GIF_CODE = "";
                order.STATUS = reqOrder.CONTRACT.STATUS;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_PARTNER_CHANNEL);
                ordersModel.COMMON.ORDER = order;
                #endregion


                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_MODE = reqOrder.CONTRACT.PRODUCT_MODE;
                orderDetail.PRODUCT_TYPE = reqOrder.CONTRACT.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm bay an toàn";
                orderDetail.EFFECTIVE_DATE = reqOrder.CONTRACT.EFFECTIVE_DATE;
                orderDetail.EXPIRATION_DATE = reqOrder.CONTRACT.EXPIRATION_DATE;
                orderDetail.PRODUCT_CODE = reqOrder.CONTRACT.PRODUCT_CODE;
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion


                #region List Insurance
                if (reqOrder.CONTRACT.CATEGORY.Equals("CN.02"))
                {
                    ordersModel.BUSINESS.HEALTH_CARE = new HealthCare();
                    ordersModel.BUSINESS.HEALTH_CARE.INSURED = reqOrder.INSURED;
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Mask - All SP
        #region ATTD ver 2
        public OrdersModel GetOrdersByLoVer2(MaskInsur lo, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = lo.CHANNEL;
                ordersModel.UserName = lo.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = lo.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = lo.BUYER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = lo.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_TYPE = lo.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm An tâm tín dụng";
                orderDetail.PRODUCT_CODE = lo.PRODUCT_CODE;
                if (lo.ACTION.Equals("BH_S"))
                {
                    orderDetail.REF_ID = lo.LO_INSUR[0].CONTRACT_CODE;
                }
                DateTime date_eff, date_exp;
                date_eff = date_exp = DateTime.MinValue;
                foreach (var item in lo.LO_INSUR)
                {
                    DateTime d_eff = goUtility.Instance.ConvertToDateTime(item.EFF_DATE, DateTime.MinValue);
                    DateTime d_exp = goUtility.Instance.ConvertToDateTime(item.EXP_DATE, DateTime.MinValue);
                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }
                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);

                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = lo.PAY_INFO;
                #endregion

                #region List Insurance
                OrdLoInsur ordLoInsur = new OrdLoInsur();
                ordLoInsur.INSURED = lo.LO_INSUR;
                ordersModel.BUSINESS.LO_INSUR = ordLoInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Sức khỏe
        public OrdersModel GetOrdersByHeath(MaskInsur ms, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "json mask health: " + JsonConvert.SerializeObject(ms), Logger.ConcatName(nameClass, nameMethod));

                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = ms.CHANNEL;
                ordersModel.UserName = ms.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = ms.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = ms.BUYER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = ms.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = ms.ORD_PARTNER;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_TYPE = ms.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                orderDetail.PRODUCT_CODE = ms.PRODUCT_CODE;
                orderDetail.PRODUCT_MODE = ms.PRODUCT_MODE == null ? goConstants.ModeInsurance.TRUC_TIEP.ToString() : ms.PRODUCT_MODE;
                if (ms.ACTION.Equals("BH_S"))
                {
                    orderDetail.REF_ID = ms.HEALTH_INSUR[0].CONTRACT_CODE;
                }
                DateTime date_eff, date_exp;
                date_eff = date_exp = DateTime.MinValue;
                foreach (var item in ms.HEALTH_INSUR)
                {
                    DateTime d_eff = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue);
                    DateTime d_exp = goUtility.Instance.ConvertToDateTime(item.EXPIRATION_DATE, DateTime.MinValue);
                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }
                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region bill
                ordersModel.COMMON.BILL_INFO = ms.BILL_INFO;
                #endregion

                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = ms.PAY_INFO;
                #endregion

                #region List Insurance
                HealthCare ordInsur = new HealthCare();
                ordInsur.INSURED = ms.HEALTH_INSUR;
                ordInsur.FILE = ms.FILE;
                ordersModel.BUSINESS.HEALTH_CARE = ordInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        public OrdersModel GetOrdersByHeathV2(MaskInsur ms, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "json mask health: " + JsonConvert.SerializeObject(ms), Logger.ConcatName(nameClass, nameMethod));

                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = ms.CHANNEL;
                ordersModel.UserName = ms.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = ms.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = ms.BUYER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = ms.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                foreach (var item in ms.HEALTH_INSUR)
                {
                    ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                    orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                    orderDetail.PRODUCT_TYPE = ms.CATEGORY;
                    orderDetail.COUNT = 1;
                    orderDetail.AMOUNT = 0;
                    orderDetail.DISCOUNT = 0;
                    orderDetail.DISCOUNT_UNIT = "P";
                    orderDetail.VAT = 0;
                    orderDetail.TOTAL_AMOUNT = 0;
                    orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                    orderDetail.PRODUCT_CODE = ms.PRODUCT_CODE;
                    orderDetail.PRODUCT_MODE = goConstants.ModeInsurance.TRUC_TIEP.ToString();
                    orderDetail.EFFECTIVE_DATE = item.EFFECTIVE_DATE;
                    orderDetail.EXPIRATION_DATE = item.EXPIRATION_DATE;
                    string id = Guid.NewGuid().ToString() + goUtility.Instance.RandomV2(4);
                    orderDetail.MAPPING_ID = id;
                    item.ID_COMMON = id;
                    if (ms.ACTION.Equals("BH_S"))
                    {
                        orderDetail.REF_ID = item.CONTRACT_CODE;
                    }

                    lstOrderDetail.Add(orderDetail);
                }
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region bill
                ordersModel.COMMON.BILL_INFO = ms.BILL_INFO;
                #endregion

                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = ms.PAY_INFO;
                #endregion

                #region List Insurance
                HealthCare ordInsur = new HealthCare();
                ordInsur.INSURED = ms.HEALTH_INSUR;
                ordInsur.FILE = ms.FILE;
                ordersModel.BUSINESS.HEALTH_CARE = ordInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion

        #region Nhà tư nhân
        public OrdersModel GetOrdersByHouse(MaskInsur ms, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = ms.CHANNEL;
                ordersModel.UserName = ms.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = ms.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = ms.BUYER;
                #endregion
                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = ms.ORD_PARTNER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = ms.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_TYPE = ms.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                orderDetail.PRODUCT_CODE = ms.PRODUCT_CODE;
                orderDetail.PRODUCT_MODE = goConstants.ModeInsurance.TRUC_TIEP.ToString();
                if (ms.ACTION.Equals("BH_S"))
                {
                    orderDetail.REF_ID = ms.HOUSE_INSUR[0].CONTRACT_CODE;
                }
                DateTime date_eff, date_exp;
                date_eff = date_exp = DateTime.MinValue;
                foreach (var item in ms.HOUSE_INSUR)
                {
                    DateTime d_eff = goUtility.Instance.ConvertToDateTime(item.HOUSE_PRODUCT.EFF, DateTime.MinValue);
                    DateTime d_exp = goUtility.Instance.ConvertToDateTime(item.HOUSE_PRODUCT.EXP, DateTime.MinValue);
                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }
                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = ms.PAY_INFO;
                #endregion
                #region Bill
                ordersModel.COMMON.BILL_INFO = ms.BILL_INFO;
                #endregion

                #region List Insurance
                ordersModel.BUSINESS.HOUSE_INSUR = ms.HOUSE_INSUR;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion
        // Add 2022-04-07 By ThienTVB
        #region Màn hình điện thoại
        public OrdersModel GetOrdersByDevice(MaskInsur ms, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = ms.CHANNEL;
                ordersModel.UserName = ms.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = ms.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = ms.BUYER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = ms.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_TYPE = ms.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                orderDetail.PRODUCT_CODE = ms.PRODUCT_CODE;
                orderDetail.PRODUCT_MODE = goConstants.ModeInsurance.TRUC_TIEP.ToString();
                if (ms.ACTION.Equals("BH_S"))
                {
                    orderDetail.REF_ID = ms.DEVICE_INSUR[0].CONTRACT_CODE;
                }
                DateTime date_eff, date_exp;
                date_eff = date_exp = DateTime.MinValue;
                foreach (var item in ms.DEVICE_INSUR)
                {
                    DateTime d_eff = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue);
                    DateTime d_exp = goUtility.Instance.ConvertToDateTime(item.EXPIRATION_DATE, DateTime.MinValue);
                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }
                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = ms.PAY_INFO;
                #endregion
                #region Bill
                ordersModel.COMMON.BILL_INFO = ms.BILL_INFO;
                #endregion

                #region List Insurance
                ordersModel.BUSINESS.DEVICE_INSUR = ms.DEVICE_INSUR;
                foreach (DeviceInsur item in ms.DEVICE_INSUR)
                {
                    item.DEVICE_INFO.URL_IMAGE_STRING = JsonConvert.SerializeObject(item.DEVICE_INFO.URL);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion
        // End 2022-04-07 By ThienTVB
        #endregion

        #region Du lich
        public OrdersModel GetOrdersByTravel(MaskInsur ms, bool pay = false)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrdersModel ordersModel = new OrdersModel();
            try
            {
                //Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "json mask health: " + JsonConvert.SerializeObject(ms), Logger.ConcatName(nameClass, nameMethod));

                ordersModel.COMMON = new ORD_COMMON();
                ordersModel.BUSINESS = new ORD_BUSINESS();
                ordersModel.Channel = ms.CHANNEL;
                ordersModel.UserName = ms.USERNAME;

                #region Seller - Nhân viên bán
                ordersModel.COMMON.SELLER = ms.SELLER;
                #endregion
                #region Buyer - Người mua
                ordersModel.COMMON.BUYER = ms.BUYER;
                #endregion
                #region Order - Đơn hàng
                ORD_ORDER order = new ORD_ORDER();
                order.FIELD = nameof(goConstants.FieldOrder.BH);
                order.ORDER_CODE = null;
                order.TYPE = ms.ACTION;
                order.TITLE = "Đơn bảo hiểm";
                order.SUMMARY = "Đơn bảo hiểm";
                order.AMOUNT = 0;
                order.DISCOUNT = 0;
                order.DISCOUNT_UNIT = "P";
                order.VAT = 0;
                order.TOTAL_AMOUNT = 0;
                order.CURRENCY = nameof(goConstants.Currency.VND);
                string arr_gift = "";
                order.GIF_CODE = arr_gift;
                order.PAY_METHOD = nameof(goConstants.PayMethod.ON_HDI_CHANNEL);

                ordersModel.COMMON.ORDER = order;
                #endregion
                #region Order partner
                ordersModel.COMMON.ORD_PARTNER = ms.ORD_PARTNER;
                #endregion
                #region Order detail
                List<ORD_ORDER_DETAIL> lstOrderDetail = new List<ORD_ORDER_DETAIL>();
                ORD_ORDER_DETAIL orderDetail = new ORD_ORDER_DETAIL();
                orderDetail.FIELD = nameof(goConstants.FieldOrder.BH);
                orderDetail.PRODUCT_TYPE = ms.CATEGORY;
                orderDetail.COUNT = 1;
                orderDetail.AMOUNT = 0;
                orderDetail.DISCOUNT = 0;
                orderDetail.DISCOUNT_UNIT = "P";
                orderDetail.VAT = 0;
                orderDetail.TOTAL_AMOUNT = 0;
                orderDetail.DESCRIPTION = "Đơn bảo hiểm";
                orderDetail.PRODUCT_CODE = ms.PRODUCT_CODE;
                orderDetail.PRODUCT_MODE = ms.PRODUCT_MODE == null ? goConstants.ModeInsurance.TRUC_TIEP.ToString() : ms.PRODUCT_MODE;
                if (ms.ACTION.Equals("BH_S"))
                {
                    orderDetail.REF_ID = ms.TRAVEL.INSURED[0].CONTRACT_CODE;
                }
                DateTime date_eff, date_exp;
                date_eff = date_exp = DateTime.MinValue;
                foreach (var item in ms.TRAVEL.INSURED)
                {
                    DateTime d_eff = goUtility.Instance.ConvertToDateTime(item.EFFECTIVE_DATE, DateTime.MinValue);
                    DateTime d_exp = goUtility.Instance.ConvertToDateTime(item.EXPIRATION_DATE, DateTime.MinValue);
                    if (date_eff == DateTime.MinValue)
                        date_eff = d_eff;
                    if (date_exp == DateTime.MinValue)
                        date_exp = d_exp;

                    if (date_eff != DateTime.MinValue && d_eff != DateTime.MinValue && date_eff > d_eff)
                        date_eff = d_eff;
                    if (date_exp != DateTime.MinValue && d_exp != DateTime.MinValue && date_exp < d_exp)
                        date_exp = d_exp;
                }
                orderDetail.EFFECTIVE_DATE = date_eff.ToString("dd/MM/yyyy HH:mm:ss");
                orderDetail.EXPIRATION_DATE = date_exp.ToString("dd/MM/yyyy HH:mm:ss");
                lstOrderDetail.Add(orderDetail);
                ordersModel.COMMON.ORDER_DETAIL = lstOrderDetail;
                #endregion
                #region bill
                ordersModel.COMMON.BILL_INFO = ms.BILL_INFO;
                #endregion

                #region Pay
                if (pay)
                    ordersModel.COMMON.PAY_INFO = ms.PAY_INFO;
                #endregion

                #region List Insurance                
                TravelCare ordInsur = new TravelCare();
                ordInsur.INSURED = ms.TRAVEL.INSURED;
                ordInsur.TRAVEL_INFO = ms.TRAVEL.TRAVEL_INFO;
                ordersModel.BUSINESS.TRAVEL_CARE = ordInsur;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                ordersModel = null;
            }
            return ordersModel;
        }
        #endregion Du lich

        #region Common

        // Output
        public ResInsurData ResGetData(InsurContract insurContract, List<InsurDetail> insurDetails)
        {
            ResInsurData resData = new ResInsurData();
            try
            {
                if (insurContract == null || insurDetails == null)
                    throw new Exception();

                resData = JsonConvert.DeserializeObject<ResInsurData>(JsonConvert.SerializeObject(insurContract));
                List<ResInsurDetail> lstInsur = new List<ResInsurDetail>();
                lstInsur = JsonConvert.DeserializeObject<List<ResInsurDetail>>(JsonConvert.SerializeObject(insurDetails));

                resData.INSURED = lstInsur;
            }
            catch (Exception ex)
            {
            }
            return resData;
        }

        public List<ResInsurData> ResGetData_V2(List<InsurContract> insurContracts, List<InsurDetail> insurDetails, List<CustomerLuggagesVJC> lstLuggages = null)
        {
            List<ResInsurData> lstData = new List<ResInsurData>();
            try
            {
                if (insurContracts == null || insurDetails == null)
                    throw new Exception();

                foreach (var item in insurContracts)
                {
                    ResInsurData resData = new ResInsurData();
                    resData = JsonConvert.DeserializeObject<ResInsurData>(JsonConvert.SerializeObject(item));
                    List<InsurDetail> lstDetail = insurDetails.Where(i => i.CONTRACT_CODE.Equals(item.CONTRACT_CODE)).ToList();
                    List<ResInsurDetail> lstInsur = new List<ResInsurDetail>();
                    lstInsur = JsonConvert.DeserializeObject<List<ResInsurDetail>>(JsonConvert.SerializeObject(lstDetail));
                    if (lstLuggages != null && lstLuggages.Any())
                    {
                        foreach (var detail in lstInsur)
                        {
                            List<CustomerLuggagesVJC> lstExists = lstLuggages.Where(i => i.DETAIL_CODE.Equals(detail.DETAIL_CODE)).ToList();
                            detail.LUGGAGES = lstExists;
                        }
                    }
                    resData.INSURED = lstInsur;
                    lstData.Add(resData);
                }
            }
            catch (Exception ex)
            {
            }
            return lstData;
        }        

        public BaseResponse GetResOrdTrans(BaseResponse resOrdTrans, BaseRequest request, ResponseConfig config, string payment_type, bool isSms = false, bool isEmail = false)
        {
            BaseResponse response = new BaseResponse();
            try
            {

                List<OrderTransInfo> lstOrdTrans = new List<OrderTransInfo>();
                List<OrdersInfo> lstOrd = new List<OrdersInfo>();
                List<OrderDetailInfo> lstOrdDetail = new List<OrderDetailInfo>();
                if (resOrdTrans.Success)
                {
                    lstOrdTrans = goBusinessCommon.Instance.GetData<OrderTransInfo>(resOrdTrans, 0);
                    lstOrd = goBusinessCommon.Instance.GetData<OrdersInfo>(resOrdTrans, 6);
                    lstOrdDetail = goBusinessCommon.Instance.GetData<OrderDetailInfo>(resOrdTrans, 7);
                    goBusinessOrders.Instance.GetResActionPayment(ref response, resOrdTrans, request, config, payment_type, lstOrdTrans, lstOrd, lstOrdDetail, isSms, isEmail);
                }
                else
                    response = resOrdTrans;
            }
            catch (Exception ex)
            {
                response = resOrdTrans;
            }
            return response;
        }

        /// <summary>
        /// Hàm sử dụng khi tạo đơn và thanh toán
        /// </summary>
        public void OrderCreateAndPay(ref BaseResponse response, List<InsurContract> insurContracts, List<InsurDetail> insurDetails, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true, List<CustomerLuggagesVJC> lstLuggages = null)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                //send mail GCN theo insurDetails
                String strChannel = "WEB";
                if(!String.IsNullOrEmpty(insurContracts[0].CHANNEL))
                {                    
                    strChannel = insurContracts[0].CHANNEL;
                    if (strChannel != "WEB" && strChannel != "APP")
                    {
                        if(!strChannel.StartsWith("CUAKHAU"))
                        {
                            strChannel = "WEB";
                        }                               
                    }    
                }    
                BaseResponse res = goBusinessServices.Instance.signAndSendMail(insurContracts[0], insurDetails, "CreateAndPay", strChannel, "NO", isTest, isEmailTest, isSMSTest);
                if (res.Success)
                {
                    try
                    {
                        List<SignFileReturnModel> lsFile = JsonConvert.DeserializeObject<List<SignFileReturnModel>>(JsonConvert.SerializeObject(res.Data));
                        foreach (SignFileReturnModel returnFile in lsFile)
                        {
                            foreach (InsurDetail detail in insurDetails)
                            {
                                if (returnFile.FILE_REF.Equals(detail.CERTIFICATE_NO))
                                {
                                    detail.URL_GCN = returnFile.FILE_URL;
                                    break;
                                }
                            }
                        }
                        //response.Data = res.Data;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi file ký số null : ", Logger.ConcatName(nameClass, nameMethod));
                    }
                }
                else
                {
                    //response.Success = false;
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails[0]));
                }
                // tạo response data trả về
                List<ResInsurData> dataOut = ResGetData_V2(insurContracts, insurDetails, lstLuggages);
                response.Data = dataOut;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void OrderCreateAndPay_Preview(ref BaseResponse response, List<JObject> insurContracts, List<JObject> insurDetails, bool isTest = false, bool isEmailTest = true, bool isSMSTest = true, List<CustomerLuggagesVJC> lstLuggages = null)
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                //send mail GCN theo insurDetails
                JObject objReturn = new JObject();
                BaseResponse res = goBusinessServices.Instance.previewGCN(insurContracts[0], insurDetails, "CreateAndPay", "WEB", "NO", isTest, isEmailTest, isSMSTest);
                if (res.Success)
                {
                    try
                    {
                        List<SignFileReturnModel> lsFile = JsonConvert.DeserializeObject<List<SignFileReturnModel>>(JsonConvert.SerializeObject(res.Data));
                        if(lsFile != null && lsFile.Count > 0)
                        {
                            objReturn.Add("DATA", lsFile[0].FILE_URL);
                        }    
                        //response.Data = res.Data;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Lỗi file ký số null : ", Logger.ConcatName(nameClass, nameMethod));
                    }
                }
                else
                {
                    //response.Success = false;
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Send mail ky so loi : " + JsonConvert.SerializeObject(res), Logger.ConcatName(nameClass, nameMethod));
                    //goBussinessEmail.Instance.sendEmaiCMS("", "Send mail ky so loi :", JsonConvert.SerializeObject(insurDetails[0]));
                }
                // tạo response data trả về
                //List<ResInsurData> dataOut = ResGetData_Preview_V2(insurContracts, insurDetails, lstLuggages);
                response.Data = objReturn;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion
    }
}
