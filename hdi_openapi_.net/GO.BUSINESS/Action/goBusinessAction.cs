﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using GO.DTO.Base;
using GO.CONSTANTS;
using GO.DTO.Common;
using GO.HELPER.Utility;
using GO.DTO.SystemModels;
using GO.COMMON.Cache;
using GO.COMMON.Log;
using GO.BUSINESS.CacheSystem;
using GO.DTO.Partner;
using GO.BUSINESS.Partner;
using Newtonsoft.Json;
using GO.DAO.Common;
using GO.HELPER.Config;
using GO.BUSINESS.Common;
using GO.BUSINESS.ServiceAccess;
using GO.ENCRYPTION;
using GO.COMMON.ErrDB;
using Newtonsoft.Json.Linq;
using GO.DTO.Service;

namespace GO.BUSINESS.Action
{
    public class goBusinessAction
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion

        #region Contructor
        private static readonly Lazy<goBusinessAction> InitInstance = new Lazy<goBusinessAction>(() => new goBusinessAction());
        public static goBusinessAction Instance => InitInstance.Value;
        #endregion

        #region Method

        public BaseResponse GetBaseResponse(BaseRequest request)
        {
            BaseResponse baseResponse = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            BaseResponse resLog = new BaseResponse();
            //baseResponse.LogId = baseResLogs.LogId = auditLogs.LogId;
            try
            {
                baseResponse.Success = true;

                if (request == null || request.Device == null || request.Action == null)
                {
                    baseResponse.Success = false;
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2006);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2006;
                    return baseResponse;
                }

                #region 1. config log
                request.Process = new ProcessTimeInfo();
                request.Process.RequestTime = DateTime.Now;
                if (request.Device != null)
                {
                    try
                    {
                        var browser = HttpContext.Current?.Request?.Browser;
                        request.Device.Browser = browser?.Browser;//string.IsNullOrEmpty(request.Device.Browser) ? browser?.Browser : request.Device.Browser;
                        request.Device.Environment = Environment.GetEnvironmentVariable("CLIENTNAME");// string.IsNullOrEmpty(request.Device.Environment) ? Environment.GetEnvironmentVariable("CLIENTNAME") : request.Device.Environment;
                        request.Device.IpPublic = HttpContext.Current?.Request?.UserHostAddress;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "request.Device ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                    }
                }
                #endregion

                ActionApi infoAction = new ActionApi();
                #region 2. get info action
                List<ActionApi> lstAction = new List<ActionApi>();
                lstAction = goBusinessCache.Instance.Get<List<ActionApi>>(goConstantsRedis.ActionApis_Key);

                if (lstAction != null && lstAction.Any())
                    infoAction = lstAction.Where(i => i.Api_Code == request.Action.ActionCode).FirstOrDefault();
                #endregion

                #region 3. check type action
                if (infoAction == null)
                {
                    baseResponse.Success = false;
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2008);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2008;
                    return baseResponse;
                }

                switch (infoAction.Action_Type)
                {
                    case nameof(goConstants.ActionType.EXECUTE):
                        GetResponseExecute(ref baseResponse, ref request, infoAction);
                        break;
                    case nameof(goConstants.ActionType.FORWARD):
                        break;
                }
                #endregion
                request.Process.ResponseTime = DateTime.Now;

                resLog = baseResponse;
                return baseResponse;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBaseResponse ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                string errCode = "";
                string errMess = "";
                goErrDB.Instance.ReadExcep(ex.Message, ref errCode, ref errMess);
                baseResponse.Success = false;
                resLog.Success = false;
                if (!string.IsNullOrEmpty(errCode) && !string.IsNullOrEmpty(errMess))
                {
                    baseResponse.Error = resLog.Error = errCode;
                    baseResponse.ErrorMessage = resLog.ErrorMessage = errMess;
                }
                else
                {
                    baseResponse.Error = resLog.Error = nameof(goConstantsError.Instance.ERROR_2009);
                    resLog.ErrorMessage = ex.Message;
                    int isDev = goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig("isDev").ToString(), 0);
                    if (isDev == 1)
                        baseResponse.ErrorMessage = ex.Message;
                    else
                        baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2009;
                    //baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2009;
                }
                return baseResponse;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
        }
        public BaseResponse GetInfoCer_SignBack(BaseRequest request)
        {
            // 1. config log request (request time, execute time, response time, data request)
            // 2. get info action by action code (store, type action (execute store, forwarded), auto cache, is behavios cache)
            // 3. check type action execute, forward 
            // a. if cache or behavios cache -> key cache -> get data by key cache if exitst
            // b. get partner config -> Enviroment, client id, secret
            // c. if data null -> db execute
            // d. if cache -> db cache
            // e. execute, forward 
            // f. if partner is signature response -> signature
            BaseResponse baseResponse = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            try
            {
                baseResponse.Success = true;

                if (request == null || request.Device == null || request.Action == null)
                {
                    baseResponse.Success = false;
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2006);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2006;
                    return baseResponse;
                }

                #region 1. config log
                request.Process = new ProcessTimeInfo();
                request.Process.RequestTime = DateTime.Now;
                if (request.Device != null)
                {
                    try
                    {
                        var browser = HttpContext.Current?.Request?.Browser;
                        request.Device.Browser = browser?.Browser;//string.IsNullOrEmpty(request.Device.Browser) ? browser?.Browser : request.Device.Browser;
                        request.Device.Environment = Environment.GetEnvironmentVariable("CLIENTNAME");// string.IsNullOrEmpty(request.Device.Environment) ? Environment.GetEnvironmentVariable("CLIENTNAME") : request.Device.Environment;
                        request.Device.IpPublic = HttpContext.Current?.Request?.UserHostAddress;
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "request.Device ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                    }
                }
                #endregion

                ActionApi infoAction = new ActionApi();
                #region 2. get info action
                List<ActionApi> lstAction = new List<ActionApi>();
                lstAction = goBusinessCache.Instance.Get<List<ActionApi>>(goConstantsRedis.ActionApis_Key);

                if (lstAction != null && lstAction.Any())
                    infoAction = lstAction.Where(i => i.Api_Code == request.Action.ActionCode).FirstOrDefault();
                #endregion

                #region 3. check type action
                if (infoAction == null)
                {
                    baseResponse.Success = false;
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2008);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2008;
                    return baseResponse;
                }

                switch (infoAction.Action_Type)
                {
                    case nameof(goConstants.ActionType.EXECUTE):
                        GetResponseExecute(ref baseResponse, ref request, infoAction);
                        break;
                    case nameof(goConstants.ActionType.FORWARD):
                        break;
                }
                #endregion
                request.Process.ResponseTime = DateTime.Now;
                //Dữ liệu truy vấn ra phải để ở cusor1 với trường certificate_no, file_signed là số giấy chứng nhận và id file
                //kiểm tra xem dữ liệu trả về có file chưa 
                if (baseResponse.Success)
                {
                    List<JObject> objSignInfo = goBusinessCommon.Instance.GetData<JObject>(baseResponse, 0);
                    if (objSignInfo != null && objSignInfo.Any())
                    {
                        List<List<JObject>> lstData = new List<List<JObject>>();
                        lstData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(baseResponse.Data));
                        foreach (JObject jobInput in objSignInfo)
                        {
                            string strIDFile = jobInput["FILE_SIGNED"]?.ToString();
                            if (string.IsNullOrEmpty(strIDFile))
                            {
                                TempConfigModel template = goBusinessServices.Instance.get_all_template(jobInput["ORG_SELLER"]?.ToString(), jobInput["STRUCT_CODE"]?.ToString(),
                                jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(), "GCN", "HDI_Sys", "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), "");
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //new Task(() => goBusinessServices.Instance.signAndSendMail_IDFile(jobInput["ORG_CODE"]?.ToString(), jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(),
                                //jobInput["STRUCT_CODE"]?.ToString(), "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), jobInput["CERTIFICATE_NO"].ToString(), strGuiID_File, template)).Start();
                                goBusinessServices.Instance.signAndSendMail_IDFile(jobInput["ORG_SELLER"]?.ToString(), jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(),
                                jobInput["STRUCT_CODE"]?.ToString(), "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), jobInput["CERTIFICATE_NO"].ToString(), strGuiID_File, template);
                                if (lstData != null && lstData.Any())
                                {
                                    if (lstData.Count > 0)
                                    {
                                        if (lstData[0] != null && lstData[0].Any())
                                        {
                                            for (int i = 0; i < lstData[0].Count; i++)
                                            {
                                                //thực hiện for
                                                if (jobInput["CERTIFICATE_NO"].ToString().Equals(lstData[0][i]["CERTIFICATE_NO"].ToString()))
                                                {
                                                    lstData[0][i]["FILE_SIGNED"] = strGuiID_File;
                                                    break;
                                                }
                                            }

                                        }
                                    }

                                }
                            }

                        }

                        //string strIDFile = objSignInfo[0]["FILE_SIGNED"]?.ToString();
                        //if (string.IsNullOrEmpty(strIDFile))
                        //{
                        //    //TempFileSign temp = goBusinessServices.Instance.fileCerBackDate(objSignInfo[0]["PRODUCT_CODE"]?.ToString(), objSignInfo[0]["CERTIFICATE_NO"].ToString(), objSignInfo[0]["ORG_CODE"]?.ToString());
                        //    //if (temp != null && temp.signed)
                        //    //{
                        //        //gán lại url id
                        //    List<List<JObject>> lstData = new List<List<JObject>>();
                        //    lstData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(baseResponse.Data));
                        //    string strGuiID_File = Guid.NewGuid().ToString("N");
                        //    if (lstData != null && lstData.Any())
                        //    {
                        //        if (lstData.Count > 0)
                        //        {
                        //            if (lstData[0] != null && lstData[0].Any())
                        //            {
                        //                //thực hiện for
                        //                    lstData[0][0]["FILE_SIGNED"] = strGuiID_File;
                        //                    //lstData[0][0]["FILE_SIGNED"] = temp.ID_FILE;

                        //                //thực hiện sinh file
                        //            }
                        //        }

                        //    }
                        baseResponse.Data = lstData;
                        //lấy thêm template 

                        //goBusinessServices.Instance.signAndSendMail_IDFile(objSignInfo[0]["ORG_CODE"]?.ToString(), objSignInfo[0]["PRODUCT_CODE"]?.ToString(), objSignInfo[0]["PACK_CODE"]?.ToString(),
                        //    objSignInfo[0]["STRUCT_CODE"]?.ToString(), "WEB", objSignInfo[0]["EFFECTIVE_NUM"]?.ToString(), objSignInfo[0]["CERTIFICATE_NO"].ToString(), strGuiID_File, template);

                        //  }
                        //}
                    }

                }
                return baseResponse;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBaseResponse ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                string errCode = "";
                string errMess = "";
                goErrDB.Instance.ReadExcep(ex.Message, ref errCode, ref errMess);
                baseResponse.Success = false;
                if (!string.IsNullOrEmpty(errCode) && !string.IsNullOrEmpty(errMess))
                {
                    baseResponse.Error = errCode;
                    baseResponse.ErrorMessage = errMess;
                }
                else
                {
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2009);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2009;
                }
                return baseResponse;
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, baseResponse);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
        }

        public String GetInfoCer_SignBack_Certificate(String cert_no, String name, String hash)
        {
            // 1. config log request (request time, execute time, response time, data request)
            // 2. get info action by action code (store, type action (execute store, forwarded), auto cache, is behavios cache)
            // 3. check type action execute, forward 
            // a. if cache or behavios cache -> key cache -> get data by key cache if exitst
            // b. get partner config -> Enviroment, client id, secret
            // c. if data null -> db execute
            // d. if cache -> db cache
            // e. execute, forward 
            // f. if partner is signature response -> signature
            BaseResponse baseResponse = new BaseResponse();
            String url_Redirect = "";
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            try
            {
                baseResponse.Success = true;
                string strMD5 = goEncryptBase.Instance.Md5Encode("HDI@2023" + cert_no + name).ToLower();

                if (!strMD5.ToLower().Equals(hash.ToLower()))
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "MD5 ERROR Logs: " + "Sai dữ liệu MD5", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                    return url_Redirect;
                }

                string strEvm = goBusinessCommon.Instance.GetConfigDB(goConstants.ENVIRONMENT);

                #region Lay thong tin hop dong

                JObject jRequestParam = new JObject();
                jRequestParam.Add("A1", cert_no);
                jRequestParam.Add("A2", name);
                jRequestParam.Add("A3", "GENERAL");
                //lấy thông tin json call
                object[] param = new object[] { "sys", "WEB", jRequestParam };
                baseResponse = goBusinessCommon.Instance.exePackagePrivate(goConstantsProcedure.SEARCH_LANDING, strEvm, param);

                #endregion
                
                //Dữ liệu truy vấn ra phải để ở cusor1 với trường certificate_no, file_signed là số giấy chứng nhận và id file
                //kiểm tra xem dữ liệu trả về có file chưa 
                if (baseResponse.Success)
                {
                    List<JObject> objSignInfo = goBusinessCommon.Instance.GetData<JObject>(baseResponse, 0);
                    if (objSignInfo != null && objSignInfo.Any())
                    {
                        List<List<JObject>> lstData = new List<List<JObject>>();
                        lstData = JsonConvert.DeserializeObject<List<List<JObject>>>(JsonConvert.SerializeObject(baseResponse.Data));
                        foreach (JObject jobInput in objSignInfo)
                        {
                            string strIDFile = jobInput["FILE_SIGNED"]?.ToString();
                            if (string.IsNullOrEmpty(strIDFile))
                            {
                                TempConfigModel template = goBusinessServices.Instance.get_all_template(jobInput["ORG_SELLER"]?.ToString(), jobInput["STRUCT_CODE"]?.ToString(),
                                jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(), "GCN", "HDI_Sys", "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), "");
                                string strGuiID_File = Guid.NewGuid().ToString("N");
                                //new Task(() => goBusinessServices.Instance.signAndSendMail_IDFile(jobInput["ORG_CODE"]?.ToString(), jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(),
                                //jobInput["STRUCT_CODE"]?.ToString(), "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), jobInput["CERTIFICATE_NO"].ToString(), strGuiID_File, template)).Start();
                                goBusinessServices.Instance.signAndSendMail_IDFile(jobInput["ORG_SELLER"]?.ToString(), jobInput["PRODUCT_CODE"]?.ToString(), jobInput["PACK_CODE"]?.ToString(),
                                jobInput["STRUCT_CODE"]?.ToString(), "WEB", jobInput["EFFECTIVE_NUM"]?.ToString(), jobInput["CERTIFICATE_NO"].ToString(), strGuiID_File, template);
                                if (lstData != null && lstData.Any())
                                {
                                    if (lstData.Count > 0)
                                    {
                                        if (lstData[0] != null && lstData[0].Any())
                                        {
                                            for (int i = 0; i < lstData[0].Count; i++)
                                            {
                                                //thực hiện for
                                                if (jobInput["CERTIFICATE_NO"].ToString().Equals(lstData[0][i]["CERTIFICATE_NO"].ToString()))
                                                {
                                                    lstData[0][i]["FILE_SIGNED"] = strGuiID_File;
                                                    url_Redirect = lstData[0][i]["URL_CERTIFICATE"].ToString() + strGuiID_File;
                                                    break;
                                                }
                                            }

                                        }
                                    }

                                }
                            }
                            else
                            {
                                url_Redirect = jobInput["URL_CERTIFICATE"]?.ToString();
                            }    

                        }
                        
                        baseResponse.Data = lstData;
                    }

                }
                return url_Redirect;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBaseResponse ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                string errCode = "";
                string errMess = "";
                goErrDB.Instance.ReadExcep(ex.Message, ref errCode, ref errMess);
                baseResponse.Success = false;
                if (!string.IsNullOrEmpty(errCode) && !string.IsNullOrEmpty(errMess))
                {
                    baseResponse.Error = errCode;
                    baseResponse.ErrorMessage = errMess;
                }
                else
                {
                    baseResponse.Error = nameof(goConstantsError.Instance.ERROR_2009);
                    baseResponse.ErrorMessage = goConstantsError.Instance.ERROR_2009;
                }
                return url_Redirect;
            }
            finally
            {
                
            }
        }

        public void GetResponseExecute(ref BaseResponse baseResponse, ref BaseRequest request, ActionApi infoAction)
        {
            // 1. check auto cache true/false
            // 2. if auto cache false -> execute store
            // 3. if auto cache true -> check behavios cache true/false
            // 4. if behavios cache false -> get data and cache by key = action + store
            // 5. if behavios cache true -> check cache client true/false
            // 6. if cache client true -> check count request cache 
            // 7. if check count request = count define -> key = IpStatic + action + store -> get data and cache
            // 8. if check count request < count define -> key = action + store -> get data and not cache
            // 1->8 => is cache, key cache, is data, list data, procedure
            // 9. get info action api database -> db execute, schema, packages, db cache, index cache
            // 10. get database execute if is data false
            // 11. get database cache if is cache true
            // 12. bool signature response
            try
            {
                goInstanceCaching.Instance.InstanceCaching();
                DataExecuteInfo dataExecuteInfo = new DataExecuteInfo();
                dataExecuteInfo.storePro = infoAction.Pro_Code;
                if (goUtility.Instance.CastToBoolean(infoAction.AutoCache))
                {
                    ActionApiBehavios behavios = GetInfoBehavios(infoAction.Api_Code);
                    GetExecuteInfo(ref dataExecuteInfo, request, infoAction, behavios);
                }
                else
                {
                    dataExecuteInfo.IsCache = false;
                    dataExecuteInfo.IsData = false;
                    dataExecuteInfo.keyCache = "";
                }
                GetDatabaseInfo(ref dataExecuteInfo, request, infoAction);

                if (dataExecuteInfo == null)
                {
                    baseResponse = goBusinessCommon.Instance.getResultApi(false, null, false, null, null, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                }
                #region Check data and execute and cache
                if (dataExecuteInfo.IsData)
                {
                    baseResponse = JsonConvert.DeserializeObject<BaseResponse>(dataExecuteInfo.Data.ToString());
                }
                else
                {
                    object[] goParam = null;
                    if (request.Data != null && !string.IsNullOrEmpty(request.Data.ToString()))
                        goParam = goUtility.Instance.CastJobjectArray((Newtonsoft.Json.Linq.JObject)request.Data);

                    string dbExecute = "";
                    string modeDb = "";
                    string storeProcedure = dataExecuteInfo.storePro;
                    goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
                    if (dataExecuteInfo.dbExecute != null && !string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Data))
                    {
                        dbExecute = dataExecuteInfo.dbExecute.Data;
                        ModeDB = goVariables.Instance.GetDATABASE_TYPE(dataExecuteInfo.dbExecute.Db_Mode);
                        if (!string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Schema) && !string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Packages))
                        //storeProcedure = dataExecuteInfo.dbExecute.Schema + "." + dataExecuteInfo.dbExecute.Packages + "." + dataExecuteInfo.storePro;
                        {
                            if (storeProcedure.IndexOf(".") > 0)
                            {
                                storeProcedure = dataExecuteInfo.dbExecute.Schema + "." + dataExecuteInfo.storePro;
                            }
                            else
                            {
                                storeProcedure = dataExecuteInfo.dbExecute.Schema + "." + dataExecuteInfo.dbExecute.Packages + "." + dataExecuteInfo.storePro;
                            }
                        }    

                        if (!string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Schema) && string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Packages))
                            storeProcedure = dataExecuteInfo.dbExecute.Schema + "." + dataExecuteInfo.storePro;

                        if (string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Schema) && !string.IsNullOrEmpty(dataExecuteInfo.dbExecute.Packages))
                            storeProcedure = dataExecuteInfo.dbExecute.Packages + "." + dataExecuteInfo.storePro;
                    }
                    //set config default
                    if (string.IsNullOrEmpty(dbExecute))
                    {
                        goBusinessCommon.Instance.SetConfigDataBase(ref dbExecute, ref ModeDB);
                    }
                    baseResponse = goBusinessCommon.Instance.getResultApi(true, goExecute.Instance.ExecuteMultiple(ModeDB, dbExecute, storeProcedure, goParam), dataExecuteInfo.IsSignature, dataExecuteInfo.secret, dataExecuteInfo.ClientID, null, null);
                    try
                    {
                        request.Process.ExecuteTime = DateTime.Now;
                        if (dataExecuteInfo.IsCache)
                        {
                            string dbCache = dataExecuteInfo.dbCache?.Data;
                            string index = dataExecuteInfo.dbCache?.DB_Index;
                            if (string.IsNullOrEmpty(dbCache))
                            {
                                goBusinessCache.Instance.SetConfigCache();
                            }
                            else
                            {
                                goBusinessCache.Instance.SetConfigCache(dbCache, goUtility.Instance.ConvertToInt32(index, 0));
                            }
                            goBusinessCache.Instance.Add(dataExecuteInfo.keyCache, baseResponse, goUtility.Instance.ConvertToInt32(goConfig.Instance.GetAppConfig(goConstants.timeCache), 0));
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                throw ex;
            }
        }

        internal ActionApiBehavios GetInfoBehavios(string Action)
        {
            ActionApiBehavios info = new ActionApiBehavios();
            try
            {
                List<ActionApiBehavios> lstData = goBusinessCache.Instance.Get<List<ActionApiBehavios>>(goConstantsRedis.ActionApiBehavios_Key);
                if (lstData != null && lstData.Any())
                {
                    info = lstData.Where(i => i.Api_Code == Action).ToList()?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.FATAL, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                info = null;
            }
            return info;
        }

        /// <summary>
        /// check hashtable and list_BehaveCode 
        /// return goRequestData
        /// </summary>
        /// <param name="key"> key hashtable</param>
        /// <param name="value"> value = RequestBevavios  </param>
        /// <param name="behavios">ActionApiBehavios: data BehaviorsCache to cache </param>
        /// <returns></returns>
        internal RequestBevavios CheckHashtableBehavios(string key, object value, ActionApiBehavios behavios)
        {
            try
            {
                RequestBevavios info = new RequestBevavios();
                if (goHashtable.Instance.Exists(key)) // true
                {
                    info = goHashtable.Instance.Get<RequestBevavios>(key);
                    info.LastExecute = DateTime.Now;
                    info.CountExecute++; // số lần request khi lấy ra + thêm 1 = số lần request thực tế
                    TimeSpan time = (info.LastExecute - info.CreateExecute);
                    int day = time.Days;
                    int gio = time.Hours;
                    int phut = time.Minutes;
                    int giay = time.Seconds;
                    int miniGiay = time.Milliseconds;
                    int tongMiniGiay = ((((gio * 60) + phut) * 60) + giay) * 1000 + miniGiay;

                    if (int.Parse(behavios.PeriodValidity) < tongMiniGiay) // nếu tổng giây vượt quá điều kiện xóa và add lại hashtable
                    {
                        goHashtable.Instance.Remove(key);
                        RequestBevavios data_new = new RequestBevavios();
                        data_new.ActionCode = info.ActionCode;
                        data_new.ActionProCode = info.ActionProCode;
                        data_new.CreateExecute = DateTime.Now;
                        data_new.Pram = info.Pram;
                        data_new.IsCache = false;

                        goHashtable.Instance.Add(key, data_new);
                        return data_new;
                    }
                    else
                    {
                        int dem = int.Parse(behavios.BehaviorsAppear);
                        if (dem == (info.CountExecute + 1))
                        {
                            info.IsCache = true;
                            goHashtable.Instance.Remove(key); // khi đạt yêu cầu cache thì xóa hashtable
                        }
                        return info;
                    }
                }
                else
                {
                    goHashtable.Instance.Add(key, value);
                    return info;
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.FATAL, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                return null;
            }
        }
        // note
        // cần thêm params khi check behavios
        // cần thêm enviroiment
        internal void GetExecuteInfo(ref DataExecuteInfo dataExecuteInfo, BaseRequest request, ActionApi infoAction, ActionApiBehavios behavios)
        {
            string keyBevavios = "", IpStatic = request?.Device?.IpPublic, keyCache = "", actionApi = infoAction.Api_Code,
                    storePro = infoAction.Pro_Code;
            bool IsData = false, IsCache = false;
            Object lst_data = null;

            string md5Data = goEncryptBase.Instance.Md5Encode(request?.Data?.ToString());

            if (behavios != null)
            {
                RequestBevavios requestBevavios = new RequestBevavios();
                if (!goUtility.Instance.CastToBoolean(infoAction.BehaviosCache))
                {
                    keyCache = actionApi + storePro + md5Data;
                    IsCache = true;
                }
                else
                {
                    if (goUtility.Instance.CastToBoolean(behavios.ClientTracking))//ClientTracking == true, key = IP + actioncode + Procode
                    {
                        keyCache = IpStatic + infoAction.Api_Code + infoAction.Pro_Code + md5Data;
                    }
                    else //client checking == false, key = actioncode + Procode
                    {
                        keyCache = infoAction.Api_Code + infoAction.Pro_Code + md5Data;
                    }
                }
                requestBevavios = CheckHashtableBehavios(keyCache, requestBevavios, behavios);
                lst_data = goBusinessCache.Instance.Get<Object>(keyCache);
                if (goUtility.Instance.CastToBoolean(infoAction.BehaviosCache))
                {
                    IsCache = true;
                    IsData = true;
                    if (lst_data == null)
                    {
                        IsData = false;
                        IsCache = true;
                    }
                }
                else
                {
                    if (requestBevavios.IsCache)// nếu check cache = true thì cache
                    {
                        IsData = true;
                        if (lst_data == null) // chưa có gọi từ db ra và cache
                        {
                            IsData = false;
                            IsCache = true;
                        }
                    }
                    else
                    {
                        IsData = true;
                        if (lst_data == null) // chưa có gọi từ db
                        {
                            IsData = false;
                            IsCache = false;
                            keyCache = string.Empty;
                        }
                    }
                }


            }
            else
            {
                keyCache = actionApi + storePro + md5Data;
                lst_data = goBusinessCache.Instance.Get<Object>(keyCache);
                IsData = true;
                if (lst_data == null)
                {
                    IsData = false;
                    IsCache = true;
                }
            }
            dataExecuteInfo.IsCache = IsCache;
            dataExecuteInfo.IsData = IsData;
            dataExecuteInfo.keyCache = keyCache;
            dataExecuteInfo.Data = lst_data;
            dataExecuteInfo.storePro = storePro;
        }

        internal void GetDatabaseInfo(ref DataExecuteInfo dataExecuteInfo, BaseRequest request, ActionApi infoAction)
        {
            // 9. get info action api database -> db execute, schema, packages, db cache, index cache
            // 10. get database execute if is data false
            // 11. get database cache if is cache true
            // 12. bool signature response
            // 13. bool signature by cer path
            try
            {
                PartnerConfigModel partnerConfig = new PartnerConfigModel();
                partnerConfig = goBusinessCommon.Instance.GetConfigPartner(request);

                if (partnerConfig == null)
                    throw new Exception(goConstantsError.Instance.ERROR_2007);

                #region 10. get database execute if is data false
                if (!dataExecuteInfo.IsData)
                {
                    ActionApiDatabase apiDbExecute = new ActionApiDatabase();
                    apiDbExecute = GetApiDatabase(infoAction.Api_Code, partnerConfig.Enviroment_Code, false);
                    dataExecuteInfo.dbExecute = apiDbExecute;
                }
                #endregion

                #region 11. get database cache if is cache true
                if (dataExecuteInfo.IsCache)
                {
                    ActionApiDatabase apiDbCache = new ActionApiDatabase();
                    apiDbCache = GetApiDatabase(infoAction.Api_Code, partnerConfig.Enviroment_Code, true);
                    dataExecuteInfo.dbCache = apiDbCache;
                }
                #endregion

                #region 12. bool signature response
                dataExecuteInfo.IsSignature = goUtility.Instance.CastToBoolean(partnerConfig.isSignature_Response);
                dataExecuteInfo.secret = partnerConfig.Secret;
                dataExecuteInfo.ClientID = partnerConfig.Client_ID;
                #endregion

                #region 13. bool signature by cer path
                dataExecuteInfo.Path_Cer = partnerConfig.Path_Cer;
                #endregion
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
            }


        }

        internal ActionApiDatabase GetApiDatabase(string action, string environment, bool isServerCache)
        {
            ActionApiDatabase info = new ActionApiDatabase();
            try
            {
                List<ActionApiDatabase> lstData = goBusinessCache.Instance.Get<List<ActionApiDatabase>>(goConstantsRedis.ActionApiDatabase_Key);
                if (lstData != null && lstData.Any())
                {
                    info = lstData.Where(i => i.Api_Code == action && i.Enviroment_Code == environment && ((i.Db_Mode == goConstants.ModeServerCache && isServerCache) || (!isServerCache && i.Db_Mode != goConstants.ModeServerCache))).ToList()?.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.FATAL, "ERROR: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                info = null;
            }
            return info;
        }

        public TokenAuthorizationInfo ValidateSignature(BaseRequest request, PartnerConfigModel partnerConfig)
        {
            TokenAuthorizationInfo result = new TokenAuthorizationInfo();
            result.Sucess = true;
            try
            {
                string raw = "";
                string deviceCode = request.Device.DeviceCode;
                string ipPrivate = request.Device.IpPrivate;
                string deviceEnvironment = request.Device.DeviceEnvironment;
                string userName = request.Action.UserName;
                string secret = request.Action.Secret;
                string actionCode = request.Action.ActionCode;
                string parentCode = request.Action.ParentCode;
                var txtData = JsonConvert.SerializeObject(request.Data);
                if (txtData.Length > 5000)
                {
                    txtData = txtData.Substring(0, 5000);
                }
                string data = goEncryptBase.Instance.Md5Encode(txtData);
                string constantsText = "HDI";
                raw = constantsText + deviceCode + ipPrivate + deviceEnvironment + userName + secret + actionCode + parentCode + data + constantsText;
                string pathCer = partnerConfig.Path_Cer;
                if (string.IsNullOrEmpty(pathCer))
                {
                    result.Signature = goEncryptMix.Instance.encryptMd5HmacSha256(raw, partnerConfig.Client_ID);
                    //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "txtData: " + txtData + "data: " + data + "raw: " + raw + "Signature: " + result.Signature, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                    if (!result.Signature.Equals(request.Signature))
                    {
                        result.Sucess = false;
                        result.Error = nameof(goConstantsError.ERROR_2012);
                    }
                }
                else
                {
                    //string sign = goEncryptBase.Instance.SignCertificates(raw, "", "", goEncryptBase.EncryptType.SHA256, false);
                    if (!goEncryptBase.Instance.VerifyCertificates(raw, request.Signature, pathCer, goEncryptBase.Instance.GetEncryptType(partnerConfig.Type_Encode), true))
                    {
                        result.Sucess = false;
                        result.Error = nameof(goConstantsError.ERROR_2012);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Sucess = false;
                result.Error = nameof(goConstantsError.ERROR_2011);
            }
            return result;
        }

        public BaseResponse GetResponseByClearCache(string key)
        {
            BaseResponse baseResponse = new BaseResponse();
            try
            {
                goBusinessCache.Instance.SetConfigCache();
                if (string.IsNullOrEmpty(key))
                {
                    goBusinessCache.Instance.Remove(goConstantsRedis.ActionApis_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.ActionApiBehavios_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.ActionApiDatabase_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.User_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.PartnerConfig_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.PartnerGroupApi_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.Domain_Key);
                    goBusinessCache.Instance.Remove(goConstantsRedis.WhiteList_Key);
                    goBusinessCache.Instance.RemoveAllPrefix(goConstantsRedis.PrefixWebconfig);
                    goBusinessCache.Instance.Remove(goConstantsRedis.BlackList_Key);
                    goInstanceCaching.isGetBlackList = false;
                }
                else
                {
                    goBusinessCache.Instance.Remove(key);
                }
                baseResponse = goBusinessCommon.Instance.getResultApi(true, null, false, null, null, null, null);
            }
            catch (Exception ex)
            {
                baseResponse = goBusinessCommon.Instance.getResultApi(false, null, false, null, null, "9999", ex.Message);
            }
            return baseResponse;
        }

        public void ValidDomain(ref TokenAuthorizationInfo result, string environment, string ip)
        {
            try
            {
                if (string.IsNullOrEmpty(environment) || string.IsNullOrEmpty(ip)) result = new TokenAuthorizationInfo();
                goInstanceCaching.Instance.InstanceCaching();
                List<DomainInfo> lstData = goBusinessCache.Instance.Get<List<DomainInfo>>(goConstantsRedis.Domain_Key);
                List<DomainInfo> lstUse = new List<DomainInfo>();
                lstUse = lstData.Where(i => i.Domain.Equals(ip) && i.Enviroment_Code.Equals(environment)).ToList();
                if (lstUse == null || !lstUse.Any())
                {
                    result.Sucess = false;
                    result.Error = nameof(goConstantsError.Instance.ERROR_2014);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ValidDomain ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2014);
            }
        }

        public void ValidIp(ref TokenAuthorizationInfo result, string env_code, string ip)
        {
            try
            {
                if (string.IsNullOrEmpty(env_code) || string.IsNullOrEmpty(ip)) result = new TokenAuthorizationInfo();
                goInstanceCaching.Instance.InstanceCaching();
                List<BlackListModel> lstData = goBusinessCache.Instance.Get<List<BlackListModel>>(goConstantsRedis.BlackList_Key);
                if (lstData != null && lstData.Any())
                {
                    List<BlackListModel> lst = lstData.Where(i => i.IP.Equals(ip) && i.ENV_CODE.Equals(env_code)).ToList();
                    if (lst != null && lst.Any())
                    {
                        result.Sucess = false;
                        result.Error = nameof(goConstantsError.Instance.ERROR_2016);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ValidIp ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                result.Sucess = false;
                result.Error = nameof(goConstantsError.Instance.ERROR_2016);
            }
        }

        public BaseResponse RemoveParams(string type)
        {
            BaseResponse baseResponse = new BaseResponse();
            goVariables.DATABASE_TYPE typeDB = goVariables.Instance.GetDATABASE_TYPE(type);
            switch (typeDB)
            {
                case goVariables.DATABASE_TYPE.ORACLE:
                    goDataAccess.Instance.ClearParams(typeDB);
                    break;
            }
            return baseResponse;
        }

        #region Audit Logs
        public BaseResponse GetAuditLos(BaseRequest request)
        {
            BaseResponse response = new BaseResponse();
            BaseResponse resLog = new BaseResponse();
            AuditLogsInfoV2 auditLogs = new AuditLogsInfoV2();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            ResponseConfig config = goBusinessCommon.Instance.GetResponseConfig(request);
            try
            {
                if (config == null)
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2007), goConstantsError.Instance.ERROR_2007);
                    return response;
                }
                if(request.Data == null || string.IsNullOrEmpty(request.Data.ToString()))
                    throw new Exception("null data");
                if(!request.Action.ActionCode.Equals("HDI_AUDIT_LOGS"))
                {
                    response = resLog = goBusinessCommon.Instance.getResultApi(false, "", false, "", "", nameof(goConstantsError.Instance.ERROR_2017), goConstantsError.Instance.ERROR_2017);
                    return response;
                }    

                AuditLogsGet reqAudit = new AuditLogsGet();
                if (request.Data != null)
                    reqAudit = JsonConvert.DeserializeObject<AuditLogsGet>(request.Data.ToString());

                if(string.IsNullOrEmpty(reqAudit.LogId) || string.IsNullOrEmpty(reqAudit.DayRequest))
                    throw new Exception("null logId or DayRequest");

                List<AuditLogsInfoV2> lstAudit = goBusinessCommon.Instance.GetAuditLogs(request.Action.ParentCode, reqAudit.LogId, reqAudit.DayRequest).Result;

                //Logger.Instance.WriteLog(Logger.TypeLog.INFO, "data json Logs: " + JsonConvert.SerializeObject(reqAudit), System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);

                List<AuditLogsResponse> lstRes = new List<AuditLogsResponse>();
                if(lstAudit != null)
                {
                    foreach (var item in lstAudit)
                    {
                        AuditLogsResponse info = new AuditLogsResponse();
                        info.LogId = item.LogId;
                        info.TimeInit = item.TimeInit;
                        try
                        {
                            info.request = JsonConvert.DeserializeObject(item.request);
                            info.response = JsonConvert.DeserializeObject(item.response);
                        }
                        catch (Exception ex)
                        {
                            Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "Case json Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                            info.request = item.request;
                            info.response = item.response;
                        }
                        lstRes.Add(info);
                    }
                }
                response = resLog = goBusinessCommon.Instance.getResultApi(true, lstRes, config);
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                response = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), goConstantsError.Instance.ERROR_2009);
                resLog = goBusinessCommon.Instance.getResultApi(false, "", config, nameof(goConstantsError.Instance.ERROR_2009), ex.Message);
            }
            finally
            {
                try
                {
                    goBusinessCommon.Instance.GetModelAutdit(ref auditLogs, request.Action.ParentCode, request.Action.ActionCode, request, resLog);
                    new Task(() => goBusinessCommon.Instance.AuditLogsApi(auditLogs)).Start();
                }
                catch (Exception ex)
                {
                    Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                }
            }
            return response;
        }
        #endregion
        #endregion

        public void Sample()
        {
            //AuditLogsInfo auditLogs = new AuditLogsInfo();
            //auditLogs.request = "a";
            //auditLogs.response = "b";
            //goBusinessCommon.Instance.AuditLogsApi(auditLogs);
            //goBusinessCommon.Instance.GetAuditLogs("HDI_PORTAL", "a699bcd1-1b33-4fa6-9aef-188b76ad972fXlo", "2021", "11", "20211123");
        }
    }
}
