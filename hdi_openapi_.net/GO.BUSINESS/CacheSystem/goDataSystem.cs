﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GO.BUSINESS.Common;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DAO.Common;
using GO.DTO.Partner;
using GO.DTO.SystemModels;
using GO.DTO.UseSystem;

namespace GO.BUSINESS.CacheSystem
{
    public class goDataSystem
    {
        #region Variable
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion

        #region Contructor
        private static readonly Lazy<goDataSystem> _instance = new Lazy<goDataSystem>(() =>
        {
            return new goDataSystem();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goDataSystem Instance { get => _instance.Value; }
        #endregion

        #region Method

        public List<ActionApi> ActionApis_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<ActionApi> result = new List<ActionApi>();
            IEnumerable<ActionApi> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<ActionApi>(ModeDB, connection, goConstantsProcedure.ActionApis_Store);
                if (data != null)
                    result = data.Cast<ActionApi>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetActionApis_GetAll: " + ex.Message, "goDataSystem - GetActionApis_GetAll");
                return null;
            }
        }

        public List<ActionApiBehavios> Behavios_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<ActionApiBehavios> result = new List<ActionApiBehavios>();
            IEnumerable<ActionApiBehavios> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<ActionApiBehavios>(ModeDB, connection, goConstantsProcedure.ActionApiBehavios_Store);
                if (data != null)
                    result = data.Cast<ActionApiBehavios>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBehavios_GetAll: " + ex.Message, "goDataSystem - GetBehavios_GetAll");
                return null;
            }
        }

        public List<ActionApiDatabase> ActionApiDatabase_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<ActionApiDatabase> result = new List<ActionApiDatabase>();
            IEnumerable<ActionApiDatabase> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<ActionApiDatabase>(ModeDB, connection, goConstantsProcedure.ActionApiDatabase_Store);
                if (data != null)
                    result = data.Cast<ActionApiDatabase>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBehavios_GetAll: " + ex.Message, "goDataSystem - GetBehavios_GetAll");
                return null;
            }
        }
        public List<DomainInfo> Domain_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<DomainInfo> result = new List<DomainInfo>();
            IEnumerable<DomainInfo> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<DomainInfo>(ModeDB, connection, goConstantsProcedure.Domain_Store);
                if (data != null)
                    result = data.Cast<DomainInfo>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBehavios_GetAll: " + ex.Message, "goDataSystem - GetBehavios_GetAll");
                return null;
            }
        }
        public List<PartnerConfigModel> PartnerConfig_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<PartnerConfigModel> result = new List<PartnerConfigModel>();
            IEnumerable<PartnerConfigModel> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<PartnerConfigModel>(ModeDB, connection, goConstantsProcedure.PartnerConfig_Store);
                if (data != null)
                    result = data.Cast<PartnerConfigModel>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetActionApis_GetAll: " + ex.Message, "goDataSystem - GetActionApis_GetAll");
                return null;
            }
        }
        public List<UserInfo> User_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<UserInfo> result = new List<UserInfo>();
            IEnumerable<UserInfo> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<UserInfo>(ModeDB, connection, goConstantsProcedure.User_Store);
                if (data != null)
                    result = data.Cast<UserInfo>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "User_GetAll ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                return null;
            }
        }
        public List<PartnerUseGroupApi> PartnerGroupApi_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<PartnerUseGroupApi> result = new List<PartnerUseGroupApi>();
            IEnumerable<PartnerUseGroupApi> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<PartnerUseGroupApi>(ModeDB, connection, goConstantsProcedure.PartnerGroupApi_Store);
                if (data != null)
                    result = data.Cast<PartnerUseGroupApi>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "PartnerGroupApi_GetAll ERROR Logs: " + ex.Message, System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
                return null;
            }
        }
        public List<PartnerWhiteListModel> PartnerWhiteList_GetAll()
        {
            string connection = "";
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<PartnerWhiteListModel> result = new List<PartnerWhiteListModel>();
            IEnumerable<PartnerWhiteListModel> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<PartnerWhiteListModel>(ModeDB, connection, goConstantsProcedure.PartnerWhiteList_Store);
                if (data != null)
                    result = data.Cast<PartnerWhiteListModel>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
        }

        public List<BlackListModel> BlackList_GetAll()
        {
            string connection = "";
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<BlackListModel> result = new List<BlackListModel>();
            IEnumerable<BlackListModel> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<BlackListModel>(ModeDB, connection, goConstantsProcedure.BlackList_Store);
                if (data != null)
                    result = data.Cast<BlackListModel>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "GetBehavios_GetAll: " + ex.Message, "goDataSystem - GetBehavios_GetAll");
                return null;
            }
        }

        #region User use system
        //public List<SysUserSSO> SysUserSSO_GetAll()
        //{

        //}
        #endregion

        public List<WebconfigModels> Webconfig_GetAll()
        {
            string connection = "";
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            goVariables.DATABASE_TYPE ModeDB = goVariables.DATABASE_TYPE.NONE;
            List<WebconfigModels> result = new List<WebconfigModels>();
            IEnumerable<WebconfigModels> data = null;
            try
            {
                goBusinessCommon.Instance.SetConfigDataBase(ref connection, ref ModeDB);
                data = goDataAccess.Instance.ExecuteQuery<WebconfigModels>(ModeDB, connection, goConstantsProcedure.Webconfig_Store);
                if (data != null)
                    result = data.Cast<WebconfigModels>().ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, ex.Message, Logger.ConcatName(nameClass, nameMethod));
                return null;
            }
        }
        #endregion
    }
}
