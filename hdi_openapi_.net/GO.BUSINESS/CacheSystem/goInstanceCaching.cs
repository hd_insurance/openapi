﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GO.BUSINESS.Common;
using GO.COMMON.Cache;
using GO.COMMON.Log;
using GO.CONSTANTS;
using GO.DTO.Partner;
using GO.DTO.SystemModels;
using GO.HELPER.Config;
using GO.HELPER.Utility;
using Newtonsoft.Json;

namespace GO.BUSINESS.CacheSystem
{
    public class goInstanceCaching
    {
        #region Variable
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;

        private static Hashtable hashtableCache = Hashtable.Synchronized(new Hashtable());
        public static bool isGetBlackList = false;
        #endregion

        #region Contructor
        private static readonly Lazy<goInstanceCaching> _instance = new Lazy<goInstanceCaching>(() =>
        {
            return new goInstanceCaching();
        });

        /// <summary>
        /// Lazy instance
        /// </summary>
        public static goInstanceCaching Instance { get => _instance.Value; }
        #endregion

        #region Method
        public void InstanceCaching()
        {
            goBusinessCache.Instance.SetConfigCache();
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.ActionApis_Key))
            {
                CachingApi();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.ActionApiBehavios_Key))
            {
                CachingBehavios();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.ActionApiDatabase_Key))
            {
                CachingApiDatabase();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.Domain_Key))
            {
                CachingDomain();
            }
            InstancePartner();

            if (!goBusinessCache.Instance.Exists(goConstantsRedis.ExitstWebconfig))
            {
                CachingWebconfig();
            }

            if (!goBusinessCache.Instance.Exists(goConstantsRedis.BlackList_Key) && !isGetBlackList)
            {
                CachingBlackList();
            }
        }
        /// <summary>
        /// Sử dụng update cache khi thêm sửa xóa partner
        /// </summary>
        public void InstancePartner()
        {
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.PartnerConfig_Key))
            {
                CachingPartnerConfig();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.User_Key))
            {
                CachingUser();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.PartnerGroupApi_Key))
            {
                CachingPartnerUseGroupApi();
            }
            if (!goBusinessCache.Instance.Exists(goConstantsRedis.WhiteList_Key))
            {
                CachingPartnerWhiteList();
            }
        }

        public void CachingApi()
        {
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<ActionApi> lstApi = new List<ActionApi>();
            lstApi = goDataSystem.Instance.ActionApis_GetAll();
            goBusinessCache.Instance.SetConfigCache();
            if (lstApi != null && lstApi.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.ActionApis_Key, lstApi);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: ActionApi nul data", Logger.ConcatName(nameClass, nameMethod));
            }
        }
        public void CachingBehavios()
        {
            List<ActionApiBehavios> lstBehavios = new List<ActionApiBehavios>();
            lstBehavios = goDataSystem.Instance.Behavios_GetAll();

            if (lstBehavios != null && lstBehavios.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.ActionApiBehavios_Key, lstBehavios);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: ActionApiBehavios nul data", "goInstanceCaching - InstanceCaching");
            }
        }
        public void CachingApiDatabase()
        {
            List<ActionApiDatabase> lstApiDb = new List<ActionApiDatabase>();
            lstApiDb = goDataSystem.Instance.ActionApiDatabase_GetAll();
            if (lstApiDb != null && lstApiDb.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.ActionApiDatabase_Key, lstApiDb);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: ActionApiDatabase nul data", "goInstanceCaching - InstanceCaching");
            }
        } 
        public void CachingDomain()
        {
            List<DomainInfo> lstData = new List<DomainInfo>();
            lstData = goDataSystem.Instance.Domain_GetAll();
            if (lstData != null && lstData.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.Domain_Key, lstData);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: ActionApiDatabase nul data", "goInstanceCaching - InstanceCaching");
            }
        }
        public void CachingPartnerConfig()
        {
            List<PartnerConfigModel> lst = new List<PartnerConfigModel>();
            lst = goDataSystem.Instance.PartnerConfig_GetAll();

            if (lst != null && lst.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.PartnerConfig_Key, lst);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "CachingPartner: nul data", "goInstanceCaching - CachingPartner");
            }
        }
        public void CachingUser()
        {
            List<UserInfo> lstUser = new List<UserInfo>();
            lstUser = goDataSystem.Instance.User_GetAll();

            if (lstUser != null && lstUser.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.User_Key, lstUser);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: User nul data", "goInstanceCaching - InstanceCaching");
            }
        }
        public void CachingPartnerUseGroupApi()
        {
            List<PartnerUseGroupApi> lst = new List<PartnerUseGroupApi>();
            lst = goDataSystem.Instance.PartnerGroupApi_GetAll();
            if (lst != null && lst.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.PartnerGroupApi_Key, lst);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "CachingPartnerUseGroupApi: nul data", "goInstanceCaching - CachingPartnerUseGroupApi");
            }
        }
        public void CachingPartnerWhiteList()
        {
            List<PartnerWhiteListModel> lst = new List<PartnerWhiteListModel>();
            lst = goDataSystem.Instance.PartnerWhiteList_GetAll();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (lst != null && lst.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.WhiteList_Key, lst);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "nul data", Logger.ConcatName(nameClass, nameMethod));
            }
        }

        public void CachingBlackList()
        {
            List<BlackListModel> lstData = new List<BlackListModel>();
            lstData = goDataSystem.Instance.BlackList_GetAll();
            if (lstData != null && lstData.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.BlackList_Key, lstData);
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.WARN, "InstanceCaching: CachingBlackList nul data", "goInstanceCaching - InstanceCaching");
            }
            isGetBlackList = true;
        }

        #region User use system
        public void CachingUserUseSys()
        {

        }
        #endregion

        public void CachingWebconfig()
        {
            List<WebconfigModels> lst = new List<WebconfigModels>();
            lst = goDataSystem.Instance.Webconfig_GetAll();
            string nameMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (lst != null && lst.Any())
            {
                goBusinessCache.Instance.Add(goConstantsRedis.ExitstWebconfig, "1");
                foreach (var item in lst)
                {
                    string key = goConstantsRedis.PrefixWebconfig + item.TYPE_CODE;
                    goBusinessCache.Instance.Add(key, item.TYPE_NAME);
                }
            }
            else
            {
                Logger.Instance.WriteLog(Logger.TypeLog.ERROR, "nul data", Logger.ConcatName(nameClass, nameMethod));
            }
        }

        #region Cache fees product
        public void InstanceCachingFees(string category = "", string product_code = "")
        {

        }
        #endregion

        #endregion
    }
}
