﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.COMMON.ErrDB
{
    public class goErrDB
    {
        #region Variables
        private string nameClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name;
        #endregion
        #region Contructor
        private static readonly Lazy<goErrDB> _instance = new Lazy<goErrDB>(() =>
        {
            return new goErrDB();
        });
        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goErrDB Instance { get => _instance.Value; }
        #endregion
        #region Method
        public void ReadExcep(string excep, ref string code, ref string mess)
        {
            try
            {
                if (!string.IsNullOrEmpty(excep))
                {
                    string[] arr = excep.Split('@');
                    if(arr.Length > 1)
                    {
                        string strErrMess = arr[1];
                        string[] arrData = strErrMess.Split(':');
                        if(arrData.Length > 1)
                        {
                            code = arrData[0];
                            mess = arrData[1];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                code = null;
                mess = null;
            }
        }
        #endregion
    }
}
