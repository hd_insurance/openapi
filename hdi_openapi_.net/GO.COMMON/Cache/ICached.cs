﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.COMMON.Cache
{
    public interface ICached
    {
        Task<bool> Add<T>(string key, T value);
        Task<bool> Add<T>(string key, T value, int expiredTime);
        bool Remove(string key);
        bool Exists(string key);
        T Get<T>(string key);
    }
}
