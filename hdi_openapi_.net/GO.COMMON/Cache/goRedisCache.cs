﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GO.COMMON.Cache
{
    public class goRedisCache : ICached
    {
        #region NutGet Packages
        // StackExchange.B ver 2.0.601
        #endregion

        #region Variable
        public static string Connect { get; set; }
        public static int IndexDb { get; set; }
        public static string DbAsync { get; set; }

        private static List<RedisConn> lstConn = new List<RedisConn>();

        private static Lazy<ConnectionMultiplexer> redisLazyConnection = new Lazy<ConnectionMultiplexer>(() => // a => b +
        {
            try
            {
                if (string.IsNullOrEmpty(Connect))
                    return ConnectionMultiplexer.Connect("localhost,allowAdmin=true,abortConnect = false");
                else
                    return ConnectionMultiplexer.Connect(Connect);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        });

        #endregion

        #region Properties
        public static ConnectionMultiplexer RedisConnection
        {
            get
            {
                return redisLazyConnection.Value;
            }
        }

        #endregion

        #region Contructor
        private static readonly Lazy<goRedisCache> _instance = new Lazy<goRedisCache>(() =>
        {
            return new goRedisCache();
        });

        /// <summary>
        /// Thuộc tính khởi tạo cho class (Lazy instance)
        /// </summary>
        public static goRedisCache Instance { get => _instance.Value; }
        #endregion

        #region Method
        private IDatabase GetIdDatabaseCache()
        {
            try
            {
                int Index = -1;
                try
                {
                    Index = IndexDb;// Convert.ToInt32(goConstants.KeyDBRedisIndex);
                    if (string.IsNullOrEmpty(IndexDb.ToString()))
                        Index = -1;

                }
                catch
                {
                    Index = -1;
                }
                var async = DbAsync;//goConstants.KeyDBRedisAsync;
                if (string.IsNullOrEmpty(async))
                    async = null;
                IDatabase cache = RedisConnection.GetDatabase(Index, async);

                return cache;
            }
            catch (Exception ex)
            {
                //return null;
                throw ex;
            }

        }

        public IDatabase GetIdDatabaseCacheV2()
        {
            try
            {
                if (lstConn != null && lstConn.Any())
                {
                    RedisConn Conn = lstConn.Where(i => i.connect.Equals(Connect) && i.index.Equals(IndexDb) && i.async.Equals(DbAsync)).FirstOrDefault();
                    if (Conn.conMulti != null)
                    {
                        if (Conn.conMulti.IsConnected)
                            return Conn.cache;
                        else
                        {
                            lstConn.Remove(Conn);
                            return SetDatabase();
                        }
                    }
                    else
                        return SetDatabase();
                }
                else
                    return SetDatabase();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IDatabase SetDatabase()
        {
            try
            {
                int Index = -1;
                try
                {
                    Index = IndexDb;// Convert.ToInt32(goConstants.KeyDBRedisIndex);
                    if (string.IsNullOrEmpty(IndexDb.ToString()))
                        Index = -1;

                }
                catch
                {
                    Index = -1;
                }
                var async = DbAsync;//goConstants.KeyDBRedisAsync;
                if (string.IsNullOrEmpty(async))
                    async = null;
                IDatabase cache = RedisConnection.GetDatabase(Index, async);

                RedisConn conn = new RedisConn();
                conn.connect = Connect;
                conn.index = IndexDb;
                conn.async = DbAsync;
                conn.conMulti = RedisConnection;
                conn.cache = cache;
                lstConn.Add(conn);

                return cache;
            }
            catch (Exception ex)
            {
                //return null;
                throw ex;
            }
        }

        public Task<bool> Add<T>(string key, T value)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                string serializedTeams = cache.StringGet(key);
                if (String.IsNullOrEmpty(serializedTeams)) // neu chua ton tai key redis thi cache
                {
                    cache.StringSet(key, JsonConvert.SerializeObject(value));
                    return Task.FromResult<bool>(true);
                }
                else
                {
                    return Task.FromResult<bool>(false);
                }
            }
            catch (Exception ex)
            {
                //return Task.FromResult<bool>(false);
                throw ex;
            }
        }

        public Task<bool> Add<T>(string key, T value, bool isNew)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                if (isNew)
                {
                    cache.StringSet(key, JsonConvert.SerializeObject(value));
                    return Task.FromResult<bool>(true);
                }
                else
                {
                    string serializedTeams = cache.StringGet(key);
                    if (String.IsNullOrEmpty(serializedTeams)) // neu chua ton tai key redis thi cache
                    {
                        cache.StringSet(key, JsonConvert.SerializeObject(value));
                        return Task.FromResult<bool>(true);
                    }
                    else
                    {
                        return Task.FromResult<bool>(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Task<bool> Add<T>(string key, T value, int expiredTime)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                string serializedTeams = cache.StringGet(key);
                if (String.IsNullOrEmpty(serializedTeams)) // neu chua ton tai key redis thi cache
                {
                    cache.StringSet(key, JsonConvert.SerializeObject(value), TimeSpan.FromSeconds(expiredTime));
                    return Task.FromResult<bool>(true);
                }
                else
                {
                    return Task.FromResult<bool>(false);
                }
            }
            catch (Exception ex)
            {
                //goShares.Instance.WriterLog(System.Reflection.MethodBase.GetCurrentMethod() + " - add allow time - " + System.Reflection.MethodBase.GetCurrentMethod() + " - " + ex.Message);
                //return Task.FromResult<bool>(false);
                throw ex;
            }
        }

        public Task<bool> Add<T>(string key, T value, int expiredTime, bool isNew)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                if (isNew)
                {
                    cache.StringSet(key, JsonConvert.SerializeObject(value), TimeSpan.FromSeconds(expiredTime));
                    return Task.FromResult<bool>(true);
                }
                else
                {
                    string serializedTeams = cache.StringGet(key);
                    if (String.IsNullOrEmpty(serializedTeams)) // neu chua ton tai key redis thi cache
                    {
                        cache.StringSet(key, JsonConvert.SerializeObject(value), TimeSpan.FromSeconds(expiredTime));
                        return Task.FromResult<bool>(true);
                    }
                    else
                    {
                        return Task.FromResult<bool>(false);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Task<bool> Appen<T>(string key, T value)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                string serializedTeams = cache.StringGet(key);
                if (!String.IsNullOrEmpty(serializedTeams)) // neu ton tai key redis thi appen
                {
                    cache.StringAppend(key, JsonConvert.SerializeObject(value));
                    return Task.FromResult<bool>(true);
                }
                else
                {
                    return Task.FromResult<bool>(false);
                }
            }
            catch (Exception ex)
            {
                //goShares.Instance.WriterLog(System.Reflection.MethodBase.GetCurrentMethod() + " - appen - " + System.Reflection.MethodBase.GetCurrentMethod() + " - " + ex.Message);
                //return Task.FromResult<bool>(false);
                throw ex;
            }
        }

        public bool Remove(string key)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                cache.KeyDelete(key);
                return true;
            }
            catch (Exception ex)
            {
                //goShares.Instance.WriterLog(System.Reflection.MethodBase.GetCurrentMethod() + " - ClearRedis - " + ex.Message);
                //return false;
                throw ex;
            }
        }

        /// <summary>
        /// Remove all key redis
        /// </summary>
        public bool RemoveAll()
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                var endpoints = RedisConnection.GetEndPoints(true); //xóa all key redis
                foreach (var point in endpoints)
                {
                    var server = RedisConnection.GetServer(point);
                    server.FlushAllDatabases();
                }
                return true;
            }
            catch (Exception ex)
            {
                //goShares.Instance.WriterLog(System.Reflection.MethodBase.GetCurrentMethod() + " - ClearAllRedis - " + ex.Message);
                //return false;
                throw ex;
            }
        }

        public RedisKey[] GetRedisKeysByPrefix(string prefix)
        {
            var indx = IndexDb;
            if (string.IsNullOrEmpty(IndexDb.ToString()))
                indx = 0;

            var ep = RedisConnection.GetEndPoints();
            var server = RedisConnection.GetServer(ep.First());
            if (string.IsNullOrEmpty(prefix))
                return server.Keys(database: indx).ToArray();
            else
                return server.Keys(database: indx, pattern: prefix + "*").ToArray();
        }

        public bool RemoveAllPrefix(string prefix)
        {
            try
            {
                var keys = GetRedisKeysByPrefix(prefix);
                IDatabase cache = GetIdDatabaseCacheV2();
                cache.KeyDeleteAsync(keys);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public T Get<T>(string key)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                string serializedTeams = cache.StringGet(key); //key cache = key_redis
                if (!String.IsNullOrEmpty(serializedTeams)) // neu ton tai key redis thi lay ra
                {
                    return JsonConvert.DeserializeObject<T>(serializedTeams);
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                //goShares.Instance.WriterLog(System.Reflection.MethodBase.GetCurrentMethod() + " - GetListRedis - " + ex.Message);
                //return default(T);
                throw ex;
            }
        }

        public bool Exists(string key)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                if (!String.IsNullOrEmpty(cache.StringGet(key))) // neu ton tai key redis thi lay ra
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TimeSpan? TimeToLive(string key)
        {
            try
            {
                IDatabase cache = GetIdDatabaseCacheV2();
                if (!String.IsNullOrEmpty(cache.StringGet(key))) // neu ton tai key redis thi lay ra
                {
                    return cache.KeyTimeToLive(key);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    internal class RedisConn
    {
        public string connect { get; set; }
        public int index { get; set; }
        public string async { get; set; }
        public ConnectionMultiplexer conMulti { get; set; }
        public IDatabase cache { get; set; }
    }
}
