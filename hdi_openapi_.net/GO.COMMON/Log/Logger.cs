﻿using System;
using log4net;

namespace GO.COMMON.Log
{
    public class Logger
    {
        #region Nuget
        // log4net ver 2.0.8
        #endregion

        #region Enum
        public enum TypeLog
        {
            FATAL,
            ERROR,
            WARN,
            INFO,
            DEGBUG
        }
        #endregion

        #region Contructor
        private static readonly Lazy<Logger> InitInstance = new Lazy<Logger>(() => new Logger());
        public static Logger Instance => InitInstance.Value;
        #endregion

        #region Method
        public void WriteLog(TypeLog typeLog, string mess, string Name)
        {
            try
            {
                ILog log = LogManager.GetLogger(Name);
                switch (typeLog)
                {
                    case TypeLog.FATAL:
                        log.Fatal(mess);
                        break;
                    case TypeLog.ERROR:
                        log.Error(mess);
                        break;
                    case TypeLog.WARN:
                        log.Warn(mess);
                        break;
                    case TypeLog.INFO:
                        log.Info(mess);
                        break;
                    case TypeLog.DEGBUG:
                        log.Debug(mess);
                        break;
                    default:
                        log.Info(mess);
                        break;
                }
            }
            catch (Exception)
            {
            }
        }
        public static string ConcatName(string Controller, string Method)
        {
            try
            {
                return Controller + " - " + Method;
            }
            catch (Exception ex)
            {
                Logger.Instance.WriteLog(TypeLog.ERROR, ex.Message, "Logger ConcatName");
                return Controller;
            }
        }
        #endregion
    }
}
