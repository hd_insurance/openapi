﻿using System;
using System.Collections;

namespace GO.HELPER.Utility
{
    public class goHashtable
    {
        #region Nuget
        // System.Collections 4.3.0
        #endregion
        #region Comment description
        //client ip, actioncode, createexecute, lastexecute, countexecute, pram object[]   
        //(createexecute: thời gian mà cliet gửi request lên sever, lastexecute: thời gian gần nhất mà client request,countexecute: số lần request)

        //nếu clienttracking = false. trong bảng behavercache
        //key = ActionCode + store
        //value = actioncode|createexecute|lastexecute|countexecute|pram object[]

        //nếu clienttracking = true. trong bảng behavercache
        //key = client_ip + ActionCode + store
        //value = actioncode|createexecute|lastexecute|countexecute|pram object[]
        #endregion

        #region Variable
        private static Hashtable hashtableCache = new Hashtable();
        #endregion

        #region Contructor
        private static readonly Lazy<goHashtable> _instance = new Lazy<goHashtable>(() =>
        {
            return new goHashtable();
        });

        /// <summary>
        /// Thuộc tính khởi tạo chó class (Lazy instance)
        /// </summary>
        public static goHashtable Instance { get => _instance.Value; }
        #endregion

        #region Method
        /// <summary>
        /// add hashtable
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Add(string key, object value)
        {
            try
            {
                if (!hashtableCache.ContainsKey(key))
                    hashtableCache.Add(key, value);
                else
                    hashtableCache[key] = value;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public T Get<T>(string key)
        {
            try
            {
                if (hashtableCache.Contains(key)) // true
                {
                    return (T)hashtableCache[key];
                }
                else
                {
                    return default(T);
                }
            }
            catch (Exception)
            {
                return default(T);
            }

        }

        public bool Exists(string key)
        {
            if (hashtableCache.Contains(key)) // true
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Remove(string key)
        {
            try
            {
                hashtableCache.Remove(key);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
