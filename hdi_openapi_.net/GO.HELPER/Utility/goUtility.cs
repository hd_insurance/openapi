﻿using ExcelDataReader;
using GO.DTO.Base;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace GO.HELPER.Utility
{
    public class goUtility
    {
        #region Enum
        public enum MoneyFomat
        {
            MONEY_TO_VIET,
            VIET_TO_ENGLISH,
            DOLLAR_TO_ENGLISH,
            DOLLAR_TO_VIET
        }
        #endregion
        #region Contructor
        private static readonly Lazy<goUtility> InitInstance = new Lazy<goUtility>(() => new goUtility());
        public static goUtility Instance => InitInstance.Value;
        #endregion

        #region Method

        #region Convert

        public JObject convertArrayToJsonParam(string[] strArray)
        {
            JObject jOutput = new JObject();
            int iTT = 0;
            foreach (string strValue in strArray)
            {
                string objName = "A" + iTT.ToString();
                jOutput.Add(objName, strValue);
                iTT++;
            }
            return jOutput;
        }
        public string ConvertToString(object value, string defaultValue)
        {
            try
            {
                return Convert.ToString(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public int ConvertToInt32(object value, int defaultValue)
        {
            try
            {
                return Convert.ToInt32(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public int? ConvertToNullableInt32(object value, int? defaultValue)
        {
            try
            {
                return Convert.ToInt32(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public long ConvertToInt64(object value, long defaultValue)
        {
            try
            {
                return Convert.ToInt64(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public double ConvertToDouble(object value, long defaultValue)
        {
            try
            {
                return Convert.ToDouble(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public long? ConvertToNullableInt64(object value, long? defaultValue)
        {
            try
            {
                return Convert.ToInt64(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public bool ConvertToBoolean(object value, bool defaultValue)
        {
            try
            {
                return Convert.ToBoolean(value.ToString().Trim());
            }
            catch
            {
                return defaultValue;
            }
        }

        public DateTime ConvertToDateTime(object value, DateTime defaultValue)
        {
            try
            {
                try
                {
                    DateTime datetime = new DateTime();
                    bool result = DateTime.TryParseExact(value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);
                    if (!result)
                    {
                        datetime = DateTime.ParseExact(value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //datetime = DateTime.Parse(value.ToString().Trim());
                    }
                    return datetime;
                }
                catch (Exception)
                {
                    try
                    {
                        DateTime datetime = new DateTime();
                        bool result = DateTime.TryParseExact(value.ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);
                        if (!result)
                        {
                            datetime = DateTime.ParseExact(value.ToString(), "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                            //datetime = DateTime.Parse(value.ToString().Trim());
                        }
                        return datetime;
                    }
                    catch (Exception)
                    {
                        DateTime datetime = new DateTime();
                        bool result = DateTime.TryParseExact(value.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);
                        if (!result)
                        {
                            datetime = DateTime.ParseExact(value.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                            //datetime = DateTime.Parse(value.ToString().Trim());
                        }
                        return datetime;
                    }
                }
            }
            catch (Exception ex)
            {
                return defaultValue;
            }
        }
        public DateTime ConvertToDateTimePay(object value, string strFomat)
        {
            try
            {
                DateTime datetime = new DateTime();
                bool result = DateTime.TryParseExact(value.ToString(), strFomat, null, DateTimeStyles.None, out datetime);
                if (!result)
                {
                    datetime = DateTime.Parse(value.ToString().Trim());
                }
                return datetime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ConvertToTime(object value, string defaultValue)
        {
            try
            {
                if (value?.ToString().Length == 5)
                {
                    value = value + ":00";
                }

                DateTime datetime = new DateTime();
                string time = defaultValue;

                bool result = DateTime.TryParseExact(value?.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);
                if (result)
                {
                    time = datetime.ToString("HH:mm:ss");
                }
                return time;
            }
            catch
            {
                return defaultValue;
            }
        }

        public DateTime ConvertToDateTime(string value)
        {
            try
            {
                return DateTime.Parse(value);
            }
            catch
            {
                return new DateTime();
            }
        }

        public DateTime? ConvertToNullableDateTime(object value, DateTime? defaultValue)
        {
            try
            {
                return DateTime.ParseExact(value.ToString(), "d/m/yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                return defaultValue;
            }
        }

        public DateTime stringToDate(object value, string fomat, DateTime defaultValue)
        {
            try
            {
                DateTime datetime = new DateTime();
                bool result = DateTime.TryParseExact(value.ToString(), fomat, CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime);
                if (!result)
                {
                    datetime = DateTime.Parse(value.ToString().Trim());
                }
                return datetime;
            }
            catch (Exception ex)
            {
                return defaultValue;
            }
        }

        public string DateToString(DateTime value, string fomat, string defaultValue)
        {
            try
            {
                return value.ToString(fomat);
            }
            catch (Exception ex)
            {
                return defaultValue;
            }
        }

        public double GetMoneyToDouble(object value, long defaultValue)
        {
            try
            {
                Regex regex = new Regex(",");
                string cleanString = regex.Replace(value.ToString(), "");
                return ConvertToDouble(cleanString, defaultValue);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
        public double GetMoneyVNDToDouble(object value, long defaultValue)
        {
            try
            {
                Regex regex = new Regex(".");
                string cleanString = regex.Replace(value.ToString(), "");
                return ConvertToDouble(cleanString, defaultValue);
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// hàm convert double => fees trong dynamic fees
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public double ConvertToDoubleFees(object value)
        {
            try
            {
                return Convert.ToDouble(value?.ToString().Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Fomat
        public string FomatMoneyVN(object value)
        {
            var fc = System.Globalization.CultureInfo.GetCultureInfo("vi-VN");
            try
            {
                return string.Format("{0:#,##0 VNĐ}", Convert.ToDouble(value.ToString().Trim())).Replace(',', '.');
                //return string.Format(fc, "{0:c}", Convert.ToDouble(value.ToString().Trim()));
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }

        public DateTime CheckFomatDate(object value, DateTime defaultValue)
        {
            try
            {
                DateTime datetime = new DateTime();
                bool result = DateTime.TryParseExact(value.ToString(), "dd/MM/yyyy", null, DateTimeStyles.None, out datetime);
                if (!result)
                {
                    datetime = defaultValue;
                }
                return datetime;
            }
            catch (Exception ex)
            {
                return defaultValue;
            }
        }

        public double FeesRound(double fees, string curency)
        {
            try
            {
                if (curency.Equals("VND"))
                    return Math.Round(fees, 0);
                else
                    return fees;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /// <summary>
        /// Lấy phí gốc từ phí vat
        /// </summary>
        /// <param name="fees"></param>
        /// <param name="vat"></param>
        /// <returns></returns>
        public double FeesRootByFeesVat(double fees, double vat)
        {
            try
            {
                // a + a*vat/100 = b => a = 100b/ (100+ vat)
                if (fees != null)
                    return 100 * fees / (100 + vat);
                else
                    return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExistsDouble(object value)
        {
            try
            {
                double a = Convert.ToDouble(value?.ToString().Trim());
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Get

        /// <summary>
        /// Bacth: Hash file content into string
        /// for example: 1e5e4212f86d8ecbe5acc956c97fa373
        /// </summary>
        /// <param name="file">file content - array of bytes</param>
        /// <returns>string with 32 characters of length</returns>
        public string HashFile(byte[] file)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            byte[] hashed = md5.ComputeHash(file);
            foreach (byte b in hashed)
                // convert to hexa
                sb.Append(b.ToString("x2").ToLower());

            // sb = set of hexa characters
            return sb.ToString();
        }

        /// <summary>
        /// Bacth: detemine path to store file
        /// for example: [1e]-[5e]-[42]-[1e5e4212f86d8ecbe5acc956c97fa373]
        /// </summary>
        /// <param name="file">file content - array of bytes</param>
        /// <returns>hashed path</returns>
        public List<string> GetPath(byte[] file)
        {
            string hashed = HashFile(file);
            List<string> toReturn = new List<string>(3);
            toReturn.Add(hashed.Substring(0, 2));
            toReturn.Add(hashed.Substring(2, 2));
            toReturn.Add(hashed.Substring(4, 2));
            toReturn.Add(hashed);
            return toReturn; // for example: [1e]-[5e]-[42]-[1e5e4212f86d8ecbe5acc956c97fa373]
        }

        /// <summary>
        /// Gets the object., 
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public object GetObject(object value, object valueIfNull)
        {
            if ((value != null) && (value != DBNull.Value))
            {
                return value;
            }
            return valueIfNull;
        }

        /// <summary>
        /// Gets the date time.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public DateTime GetDateTime(object value, DateTime valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is DateTime)
            {
                return (DateTime)value;
            }
            return DateTime.Parse(value.ToString());
        }

        public Decimal GetDecimal(object value, Decimal valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is Decimal)
            {
                return (Decimal)value;
            }
            return Decimal.Parse(value.ToString());
        }

        /// <summary>
        /// Gets the byte.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public byte GetByte(object value, byte valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is byte)
            {
                return (byte)value;
            }
            return byte.Parse(value.ToString());
        }

        /// <summary>
        /// Gets the boolean.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">if set to <c>true</c> [value if null].</param>
        /// <returns></returns>
        public bool GetBoolean(object value, bool valueIfNull)
        {
            value = GetObject(value, valueIfNull);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is bool)
            {
                return (bool)value;
            }
            if (!(value is byte))
            {
                return bool.Parse(value.ToString());
            }
            if (((byte)value) == 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the string. 
        /// </summary>
        /// <param name="dataReader">The data reader.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public string GetString(object value, string valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is string)
            {
                return (string)value;
            }
            return value.ToString();
        }

        /// <summary>
        /// Gets the single.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public float GetSingle(object value, float valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is float)
            {
                return (float)value;
            }
            return float.Parse(value.ToString());
        }

        /// <summary>
        /// Gets the int64.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public long GetInt64(object value, long valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is long)
            {
                return (long)value;
            }
            return long.Parse(value.ToString());
        }

        /// <summary>
        /// Gets the int32.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="valueIfNull">The value if null.</param>
        /// <returns></returns>
        public int GetInt32(object value, int valueIfNull)
        {
            value = GetObject(value, null);
            if (value == null)
            {
                return valueIfNull;
            }
            if (value is int)
            {
                return (int)value;
            }
            return int.Parse(value.ToString());
        }

        public string GetRemoteIp()
        {
            var context = HttpContext.Current;
            var ipList = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            return !string.IsNullOrEmpty(ipList) ? ipList.Split(',')[0] : context.Request.ServerVariables["REMOTE_ADDR"];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eff"></param>
        /// <param name="exp"></param>
        /// <param name="isLeap">năm nhuận không</param>
        /// <returns></returns>
        public int GetDayRange(DateTime eff, DateTime exp, bool isLeap = false)
        {
            int day = 0;
            if (eff.Day == exp.Day && eff.Month == exp.Month && !isLeap)
                day = (exp.Year - eff.Year) * 365;
            else
            {
                TimeSpan timeSpan = exp - eff;
                day = timeSpan.Days;
            }
            return day;
        }
        #endregion

        #region Random
        public string Random(int size)
        {
            var chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*".ToCharArray();
            var data = new byte[size];
            using (var crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            var result = new StringBuilder(size);
            foreach (var b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        public string RandomV2(int size)
        {
            var chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            var data = new byte[size];
            using (var crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            var result = new StringBuilder(size);
            foreach (var b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public string RandomOTP(int size)
        {
            var chars = "1234567890".ToCharArray();
            var data = new byte[size];
            using (var crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
            var result = new StringBuilder(size);
            foreach (var b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
        #endregion

        #region Cast Data

        /// <summary>
        /// Convert jobject to array object
        /// </summary>
        /// <param name="jObj">JOject convert source</param>
        /// <returns>Return array object</returns>
        public object[] CastJobjectArray(Newtonsoft.Json.Linq.JObject jObj)
        {
            List<object> ls = new List<object>();
            foreach (var item in jObj)
            {
                if (item.Value.Type == JTokenType.Null)
                    ls.Add(DBNull.Value);
                else
                    ls.Add(Convert.ChangeType(item.Value, item.Value.GetType()));
            }
            return ls.ToArray();
        }

        public bool CastToBoolean(object value)
        {
            try
            {
                string[] arrTrue = { "true", "1" };
                if (value != null && Array.Exists(arrTrue, i => i.Equals(value.ToString().ToLower())))
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region Base Get Response
        public BaseResponse GetResponse(bool isSuccess, object data, string error, string errorMessage, string key = "")
        {
            BaseResponse result = new BaseResponse();
            result.Success = isSuccess;
            result.Data = data;
            result.Error = error;
            result.ErrorMessage = errorMessage;
            result.Signature = GetSignatureResponse(isSuccess, data, error, errorMessage, key);
            return result;
        }
        #endregion

        #region Signature

        public string GetSignatureResponse(bool isSuccess, object data, string error, string errorMessage, string key)
        {
            string result = "";
            if (!string.IsNullOrEmpty(key))
                result = "";
            return result;
        }

        #endregion

        #region File
        public void CreateFolder(string path)
        {
            try
            {
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ExistsFile(string path)
        {
            try
            {
                if (System.IO.File.Exists(path))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Base64ToFile(string base64, string pathFile)
        {
            try
            {
                File.WriteAllBytes(pathFile, Convert.FromBase64String(base64));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetPathProject()
        {
            try
            {
                return System.IO.Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion

        #region Excel
        public DataTable ReadExcelToTable(string path)
        {
            try
            {
                FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader;
                //1. Reading Excel file
                if (Path.GetExtension(path).ToUpper() == ".XLS")
                {
                    //1.1 Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelDataReader.ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                //2. DataSet - The result of each spreadsheet will be created in the result.Tables
                DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
                DataTable table = result.Tables[0];
                //DataSet result = excelReader.AsDataSet();

                ////3. DataSet - Create column names from first row
                //excelReader.IsFirstRowAsColumnNames = false;
                excelReader.Close();
                return table;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable ReadExcelToTableNotHeader(string path)
        {
            try
            {
                FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader;
                //1. Reading Excel file
                if (Path.GetExtension(path).ToUpper() == ".XLS")
                {
                    //1.1 Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else
                {
                    //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelDataReader.ExcelReaderFactory.CreateOpenXmlReader(stream);
                }

                //2. DataSet - The result of each spreadsheet will be created in the result.Tables
                DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = false
                    }
                });
                DataTable table = result.Tables[0];
                //DataSet result = excelReader.AsDataSet();

                ////3. DataSet - Create column names from first row
                //excelReader.IsFirstRowAsColumnNames = false;
                excelReader.Close();
                return table;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region DataTable

        public List<T> DataTableToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        public List<T> DataTableToListUpper<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItemUpper<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItemUpper<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (System.Reflection.PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name.ToUpper() == column.ColumnName.ToUpper())
                    {
                        var value = dr[column.ColumnName];
                        if (value == null || value == DBNull.Value)
                        {
                            pro.SetValue(obj, null, null);
                        }
                        else
                        {
                            ConvertValueDataType(ref value, column.DataType, pro.PropertyType);
                            pro.SetValue(obj, value, null);
                        }
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (System.Reflection.PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                    {
                        //pro.SetValue(obj, dr[column.ColumnName] != DBNull.Value ? dr[column.ColumnName] : null, null); ;
                        var value = dr[column.ColumnName];
                        if (value == null || value == DBNull.Value)
                        {
                            pro.SetValue(obj, null, null);
                        }
                        else
                        {
                            ConvertValueDataType(ref value, column.DataType, pro.PropertyType);
                            pro.SetValue(obj, value, null);
                        }
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
        public static void ConvertValueDataType(ref object value, Type input, Type output)
        {
            if (input.Name != output.Name)
            {
                switch (output.Name)
                {
                    case "String":
                        value = goUtility.Instance.ConvertToString(value, string.Empty);
                        break;
                    case "Int16":
                    case "Int32":
                        value = goUtility.Instance.ConvertToInt32(value, 0);
                        break;
                    case "Double":
                        value = goUtility.Instance.ConvertToDouble(value, 0);
                        break;
                    case "Boolean":
                        value = goUtility.Instance.ConvertToBoolean(value, false);
                        break;
                }
            }
        }
        public void SetNameTableByDs(ref DataSet ds, string colunm)
        {
            try
            {
                if (!string.IsNullOrEmpty(colunm))
                {
                    for (var i = 0; i < ds.Tables.Count; i++)
                    {
                        ds.Tables[i].TableName = ds.Tables[i].Rows[0].Field<string>(colunm);
                        ds.Tables[i].Columns.Remove(colunm);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void SetNameTableByDs(ref DataSet ds, string colunm, int index)
        {
            try
            {
                if (!string.IsNullOrEmpty(colunm))
                {
                    if (index == ds.Tables.Count)
                    {
                        ds.Tables[index].TableName = ds.Tables[index].Rows[index].Field<string>(colunm);
                        ds.Tables[index].Columns.Remove(colunm);
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }
        #endregion

        #region Money Việt => Dollar
        /// <summary>
        /// 100000.00
        /// </summary>
        /// <param name="inumber"></param>
        /// <returns></returns>
        public string DocSoThanhChu(string inumber)
        {
            string strReturn = "";
            string s = inumber;
            while (s.Length > 0 && s.Substring(0, 1) == "0")
            {
                s = s.Substring(1);
            }
            if (s == "") return "";
            string[] so =
            {
                "không",
                "một",
                "hai",
                "ba",
                "bốn",
                "năm",
                "sáu",
                "bảy",
                "tám",
                "chín"
            };
            string[] hang =
                {"", "nghìn", "triệu", "tỷ" };
            int i = 0;
            int j = 0;
            int donvi = 0;
            int chuc = 0;
            int tram = 0;
            bool booAm = false;
            int decS = 0;
            decS = Convert.ToInt32(s);
            if (decS <
                0)
            {
                decS = -decS;
                //s = decS.ToString();
                booAm = true;
            }
            i = s.Length;

            //console.log("abcde"+s.substr(i-1,s.length));
            // con
            if (i == 0)
                strReturn = so[0] + strReturn;
            else
            {
                j = 0;
                while (i > 0)
                {
                    var input = strReturn;
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i = i - 1;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i = i - 1;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i = i - 1;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        strReturn = hang[j] + strReturn;
                    j = j + 1;
                    if (j > 3)
                        j = 1;
                    //Tránh lỗi, nếu dưới 13 số thì không có vấn đề.
                    //Hàm này chỉ dùng để đọc đến 9 số nên không phải bận tâm
                    if ((donvi == 1) && (chuc > 1))
                        strReturn = "mốt " + strReturn;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            strReturn = "lăm " + strReturn;
                        else
                            if (donvi > 0)
                            strReturn = so[donvi] + " " + strReturn;
                    }
                    if (chuc < 0)
                        break;//Hết số
                    else
                    {
                        if ((chuc == 0) && (donvi > 0))
                            strReturn = "linh " + strReturn;
                        if (chuc == 1)
                            strReturn = "mười " + strReturn;
                        if (chuc > 1)
                            strReturn = so[chuc] + " mươi " + strReturn;
                    }
                    if (tram < 0)
                        break;//Hết số
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0))
                            strReturn = so[tram] + " trăm " + strReturn;
                    }
                    if (input != strReturn)
                        strReturn = " " + strReturn;
                }
            }
            if (booAm)
                strReturn = "Âm " + strReturn;
            else if (strReturn.Length > 0)
                strReturn = strReturn.Trim();
            strReturn = strReturn.ElementAt(0).ToString().ToUpper() + strReturn.Substring(1).ToLower() + " đồng";
            return strReturn;
        }
        public string ConvertCurrencyToEnglish(string inumber)
        {
            string Temp = "";
            string Dollars = "", Cents = "";
            int DecimalPlace = 0, Count = 0;
            string[] Place = {
                "",
            " Thousand ",
            " Million ",
            " Billion ",
            " Trillion ",
        };
            DecimalPlace = inumber.IndexOf(".");
            if (DecimalPlace > 0)
            {
                string thapphan = inumber.Substring(DecimalPlace + 1) + "00";
                Temp = this.Left(this.MidStart(inumber, DecimalPlace + 1) + "00", 2);
                Cents = this.ConvertTens(Temp);
                inumber = (this.Left(inumber, DecimalPlace)).Trim();
            }
            Count = 0;
            while (inumber != "")
            {
                Temp = this.ConvertHundreds(this.Right(inumber, 3));
                if (Temp != "")
                {
                    Dollars = Temp + Place[Count] + Dollars;
                }
                if (inumber.Length > 3)
                {
                    inumber = this.Left(inumber, inumber.Length - 3);
                }
                else
                {
                    inumber = "";
                }
                Count = Count + 1;
            }
            if (Dollars == "")
            {
                Dollars = "No Dollars";
            }
            if (Dollars == "One")
            {
                Dollars = "One Dollar";
            }
            else
            {
                Dollars = Dollars.TrimEnd() + " Dollars";
            }
            if (Cents == "")
            {
                Cents = " And No Cents";
            }
            else if (Cents == "One")
            {
                Cents = " And One Cent";
            }
            else
            {
                Cents = " And " + Cents.TrimEnd() + " Cents";
            }
            return Dollars + Cents;
        }
        public string RemoveMark(string s)
        {
            string chold = "";
            string chnew = "";
            if (s.Length != 0)
            {
                string char1 = "â, ă, ấ, ầ, ậ, ẫ, ẩ, ắ, ằ, ẵ, ẳ, ặ, á, à, ả, ã, ạ";
                string char2 = "Â, Ă, Ấ, Ầ, Ậ, Ẫ, Ẩ, Ắ, Ằ, Ẵ, Ẳ, Ặ, Á, À, Ả, Ã, Ạ";
                string char3 = "ó, ò, ỏ, õ, ọ, ô, ố, ồ, ổ, ỗ, ộ, ơ, ớ, ờ, ợ, ở, ỡ";
                string char4 = "Ó, Ò, Ỏ, Õ, Ọ, Ô, Ố, Ồ, Ổ, Ỗ, Ộ, Ơ, Ớ, Ờ, Ợ, Ở, Ỡ";
                string char5 = "ư, ứ, ừ, ự, ử, ữ, ù, ú, ủ, ũ, ụ";
                string char6 = "Ư, Ứ, Ừ, Ự, Ử, Ữ, Ù, Ú, Ủ, Ũ, Ụ";
                string char7 = "ê, ế, ề, ệ, ể, ễ, è, é, ẻ, ẽ, ẹ";
                string char8 = "Ê, Ế, Ề, Ệ, Ể, Ễ, È, É, Ẻ, Ẽ, Ẹ";
                string char9 = "í, ì, ị, ỉ, ĩ";
                string char10 = "Í, Ì, Ỉ, Ĩ, Ị";
                string char11 = "ý, ỳ, ỵ, ỷ, ỹ";
                string char12 = "Ý, Ỳ, Ỵ, Ỷ, Ỹ";
                string char13 = "đ";
                string char14 = "Đ";
                string[] Char = {
                    char1, char2, char3, char4, char5, char6, char7, char8, char9, char10, char11, char12, char13, char14
                };

                for (var i = 0; i < s.Length; i++)
                {
                    chold = s.Substring(i, 1);
                    for (var k = 0; k <= 13; k++)
                    {
                        if (Char[k].IndexOf(chold) > -1 && chold.Trim().Length > 0)
                        {
                            switch (k)
                            {
                                case 0:
                                    chnew = "a"; break;
                                case 1:
                                    chnew = "A"; break;
                                case 2:
                                    chnew = "o"; break;
                                case 3:
                                    chnew = "O"; break;
                                case 4:
                                    chnew = "u"; break;
                                case 5:
                                    chnew = "U"; break;
                                case 6:
                                    chnew = "e"; break;
                                case 7:
                                    chnew = "E"; break;
                                case 8:
                                    chnew = "i"; break;
                                case 9:
                                    chnew = "I"; break;
                                case 10:
                                    chnew = "y"; break;
                                case 11:
                                    chnew = "Y"; break;
                                case 12:
                                    chnew = "d"; break;
                                case 13:
                                    chnew = "D"; break;
                            }
                            s = s.Replace(chold, chnew);
                        }
                    }
                }
                return s;
            }
            else
            {
                return s;
            }
        }
        public string Left(string num, int size)
        {
            if (size == 0) return "";
            return num.ToString().Substring(0, size);
        }
        public string MidStart(string MyNumber, int start)
        {
            return MyNumber.Substring(start);
        }
        public string ConvertTens(string MyTens)
        {
            string Result = "";
            if (Number(this.Left(MyTens, 1)) == 1)
            {
                int tempVal = Number(MyTens);
                if (tempVal == 10) Result = "Ten";
                if (tempVal == 11) Result = "Eleven";
                if (tempVal == 12) Result = "Twelve";
                if (tempVal == 13) Result = "Thirteen";
                if (tempVal == 14) Result = "Fourteen";
                if (tempVal == 15) Result = "Fifteen";
                if (tempVal == 16) Result = "Sixteen";
                if (tempVal == 17) Result = "Seventeen";
                if (tempVal == 18) Result = "Eighteen";
                if (tempVal == 19) Result = "Nineteen";
            }
            else
            {
                int tempVal = Number(this.Left(MyTens, 1));
                if (tempVal == 2) Result = "Twenty ";
                if (tempVal == 3) Result = "Thirty ";
                if (tempVal == 4) Result = "Forty ";
                if (tempVal == 5) Result = "Fifty ";
                if (tempVal == 6) Result = "Sixty ";
                if (tempVal == 7) Result = "Seventy ";
                if (tempVal == 8) Result = "Eighty ";
                if (tempVal == 9) Result = "Ninety ";
                Result = Result + this.ConvertDigit(this.Right(MyTens, 1));
            }
            return Result;
        }
        public string Right(string num, int size)
        {
            if (size == 0) return "";
            if (num.ToString().Length < size) return num.ToString();
            return num.ToString().Substring(num.ToString().Length - size, size);
        }
        public string ConvertDigit(string MyDigit)
        {
            if (Number(MyDigit) == 1) return "One";
            if (Number(MyDigit) == 2) return "Two";
            if (Number(MyDigit) == 3) return "Three";
            if (Number(MyDigit) == 4) return "Four";
            if (Number(MyDigit) == 5) return "Five";
            if (Number(MyDigit) == 6) return "Six";
            if (Number(MyDigit) == 7) return "Seven";
            if (Number(MyDigit) == 8) return "Eight";
            if (Number(MyDigit) == 9) return "Nine";
            else return "";
        }
        public string ConvertHundreds(string MyNumber)
        {
            string Result = "";
            if (Number(MyNumber) == 0) return "";
            MyNumber = this.Right("000" + MyNumber, 3);
            if (this.Left(MyNumber, 1) != "0")
            {
                Result = this.ConvertDigit(this.Left(MyNumber, 1)) + " Hundred ";
            }
            if (this.MidStartEnd(MyNumber, 1, 1) != "0")
            {
                Result = Result + this.ConvertTens(this.MidStart(MyNumber, 1));
            }
            else
            {
                Result = Result + this.ConvertDigit(this.MidStart(MyNumber, 2));
            }
            return Result.Trim();
        }
        public string MidStartEnd(string MyNumber, int start, int length)
        {
            return MyNumber.Substring(start, length);
        }
        public int Number(object MyDigit)
        {
            return Convert.ToInt32(MyDigit);
        }

        #endregion

        #region Money Dollar => Việt
        public string ConvertDollarsToVN(string inumber)
        {
            string Temp = "";
            string Dollars = "", Cents = "";
            int DecimalPlace, Count = 0;
            string[] Place = {
                " ",
            " nghìn ",
            " triệu ",
            " tỷ ",
            " nghìn tỷ ",
             };
            DecimalPlace = inumber.IndexOf(".");
            if (DecimalPlace > 0)
            {
                string thapphan = inumber.Substring(DecimalPlace + 1) + "00";
                Temp = this.Left(this.MidStart(inumber, DecimalPlace + 1) + "00", 2);
                Cents = this.ConvertTensVN(Temp);
                inumber = (this.Left(inumber, DecimalPlace)).Trim();
            }
            Count = 0;
            while (inumber != "")
            {
                Temp = this.ConvertHundredsVN(this.Right(inumber, 3));
                if (Temp != "")
                {
                    Dollars = Temp + Place[Count] + Dollars;
                }
                if (inumber.Length > 3)
                {
                    inumber = this.Left(inumber, inumber.Length - 3);
                }
                else
                {
                    inumber = "";
                }
                Count = Count + 1;
            }
            if (Dollars == "")
            {
                Dollars = "";
            }
            if (Dollars == "Một")
            {
                Dollars = "Một đô la Mỹ";
            }
            else
            {
                Dollars = Dollars.TrimEnd() + " đô la Mỹ";
            }
            if (Cents == "")
            {
                Cents = " ";
            }
            else if (Cents == "Một")
            {
                Cents = " và Một xen Mỹ";
            }
            else
            {
                Cents = " và " + Cents.TrimEnd() + " xen Mỹ";
            }
            return Dollars + Cents;
        }
        public string ConvertTensVN(string MyTens)
        {
            string Result = "";
            if (Number(this.Left(MyTens, 1)) == 1)
            {
                int tempVal = Number(MyTens);
                if (tempVal == 10) Result = "Mười";
                if (tempVal == 11) Result = "Mười một";
                if (tempVal == 12) Result = "Mười hai";
                if (tempVal == 13) Result = "Mười ba";
                if (tempVal == 14) Result = "Mười bốn";
                if (tempVal == 15) Result = "Mười lăm";
                if (tempVal == 16) Result = "Mười sáu";
                if (tempVal == 17) Result = "Mười bảy";
                if (tempVal == 18) Result = "Mười tám";
                if (tempVal == 19) Result = "Mười chín";
            }
            else
            {
                int tempVal = Number(this.Left(MyTens, 1));
                if (tempVal == 2) Result = "Hai mươi ";
                if (tempVal == 3) Result = "Ba mươi ";
                if (tempVal == 4) Result = "Bốn mươi ";
                if (tempVal == 5) Result = "Năm mươi ";
                if (tempVal == 6) Result = "Sáu mươi ";
                if (tempVal == 7) Result = "Bảy mươi ";
                if (tempVal == 8) Result = "Tám mươi ";
                if (tempVal == 9) Result = "Chín mươi ";
                Result = Result + this.ConvertDigitVN(this.Right(MyTens, 1));
            }
            return Result;
        }
        public string ConvertDigitVN(string MyDigit)
        {
            if (Number(MyDigit) == 1) return "Một";
            if (Number(MyDigit) == 2) return "Hai";
            if (Number(MyDigit) == 3) return "Ba";
            if (Number(MyDigit) == 4) return "Bốn";
            if (Number(MyDigit) == 5) return "Năm";
            if (Number(MyDigit) == 6) return "Sáu";
            if (Number(MyDigit) == 7) return "Bảy";
            if (Number(MyDigit) == 8) return "Tám";
            if (Number(MyDigit) == 9) return "Chín";
            else return "";
        }
        public string ConvertHundredsVN(string MyNumber)
        {
            string Result = "";
            if (Number(MyNumber) == 0) return "";
            MyNumber = this.Right("000" + MyNumber, 3);
            if (this.Left(MyNumber, 1) != "0")
            {
                Result = this.ConvertDigitVN(this.Left(MyNumber, 1)) + " Trăm ";
            }
            if (this.MidStartEnd(MyNumber, 1, 1) != "0")
            {
                Result = Result + this.ConvertTensVN(this.MidStart(MyNumber, 1));
            }
            else
            {
                Result = Result + this.ConvertDigitVN(this.MidStart(MyNumber, 2));
            }
            return Result.Trim();
        }
        #endregion

        #region Việt => English
        public string ConvertVNToEnglish(string inumber)
        {
            string Temp = "";
            string Dollars = "", Cents = "";
            int DecimalPlace = 0, Count = 0;
            string[] Place = {
                "",
            " Thousand ",
            " Million ",
            " Billion ",
            " Trillion ",
        };
            DecimalPlace = inumber.IndexOf(".");
            if (DecimalPlace > 0)
            {
                string thapphan = inumber.Substring(DecimalPlace + 1) + "00";
                Temp = this.Left(this.MidStart(inumber, DecimalPlace + 1) + "00", 2);
                Cents = this.ConvertTens(Temp);
                inumber = (this.Left(inumber, DecimalPlace)).Trim();
            }
            Count = 0;
            while (inumber != "")
            {
                Temp = this.ConvertHundreds(this.Right(inumber, 3));
                if (Temp != "")
                {
                    Dollars = Temp + Place[Count] + Dollars;
                }
                if (inumber.Length > 3)
                {
                    inumber = this.Left(inumber, inumber.Length - 3);
                }
                else
                {
                    inumber = "";
                }
                Count = Count + 1;
            }
            if (Dollars == "")
            {
                Dollars = "Vietnam dongs";
            }
            if (Dollars == "One")
            {
                Dollars = "One Vietnam dong";
            }
            else
            {
                Dollars = Dollars.TrimEnd() + " Vietnam dongs";
            }
            return Dollars;
        }
        #endregion
        public string ConvertMoneyToText(string inumber, MoneyFomat fomat)
        {
            string output = "";
            string input = "";
            double numInput = 0;
            string[] arr;
            switch (fomat)
            {
                case MoneyFomat.MONEY_TO_VIET:
                    numInput = GetMoneyToDouble(inumber, 0);
                    arr = numInput.ToString().Split('.');
                    input = numInput.ToString();
                    if (arr.Count() > 1)
                    {
                        input = arr[0];
                    }
                    output = DocSoThanhChu(input);
                    break;
                case MoneyFomat.VIET_TO_ENGLISH:
                    numInput = GetMoneyToDouble(inumber, 0);
                    arr = numInput.ToString().Split('.');
                    input = numInput.ToString();
                    if (arr.Count() > 1)
                    {
                        input = arr[0];
                    }
                    //output = DocSoThanhChu(input);
                    //output = RemoveMark(output);

                    output = ConvertVNToEnglish(input);
                    break;
                case MoneyFomat.DOLLAR_TO_VIET:
                    output = ConvertDollarsToVN(inumber);
                    break;
                case MoneyFomat.DOLLAR_TO_ENGLISH:
                    numInput = GetMoneyToDouble(inumber, 0);
                    input = numInput.ToString();
                    output = ConvertCurrencyToEnglish(input);
                    break;
            }

            return output;
        }

        #endregion

        public void getAge(DateTime v_dob, DateTime v_to, ref double age, ref int age_y, ref int age_m, ref int age_d)
        {
            TimeSpan ts = v_to.Subtract(v_dob);
            DateTime dateAge = DateTime.MinValue + ts;
            double agePlus = 0;
            int v_ageY, v_ageM, v_ageD = 0;
            v_ageY = dateAge.Year - 1;
            v_ageM = dateAge.Month - 1;
            v_ageD = dateAge.Day - 1;

            if (v_ageM != 0 || v_ageD != 0)
            {
                agePlus += 0.1;
            }
            age = v_ageY + agePlus;
            age_y = v_ageY;
            age_m = v_ageM;
            age_d = v_ageD;
        }
        public double getAge(DateTime v_dob, DateTime v_to)
        {
            double age = 0;
            TimeSpan ts = v_to.Subtract(v_dob);
            DateTime dateAge = DateTime.MinValue + ts;
            double agePlus = 0;
            int v_ageY, v_ageM, v_ageD = 0;
            v_ageY = dateAge.Year - 1;
            v_ageM = dateAge.Month - 1;
            v_ageD = dateAge.Day - 1;
            if (v_ageM != 0 || v_ageD != 0)
                agePlus += 0.1;
            age = v_ageY + agePlus;
            return age;
        }

        public int getAgeYear(DateTime v_dob, DateTime v_to)
        {
            double age = 0;
            TimeSpan ts = v_to.Subtract(v_dob);
            DateTime dateAge = DateTime.MinValue + ts;
            double agePlus = 0;
            int v_ageY, v_ageM, v_ageD = 0;
            v_ageY = dateAge.Year - 1;
            v_ageM = dateAge.Month - 1;
            v_ageD = dateAge.Day - 1;
            if (v_ageM != 0 || v_ageD != 0)
                agePlus += 0.1;
            age = v_ageY + agePlus;
            return v_ageY;
        }
        public int getAgeMonth(DateTime v_dob, DateTime v_to)
        {
            double age = 0;
            TimeSpan ts = v_to.Subtract(v_dob);
            DateTime dateAge = DateTime.MinValue + ts;
            double agePlus = 0;
            int v_ageY, v_ageM, v_ageD = 0;
            v_ageY = dateAge.Year - 1;
            v_ageM = dateAge.Month - 1;
            v_ageD = dateAge.Day - 1;
            if (v_ageM != 0 || v_ageD != 0)
                agePlus += 0.1;
            age = v_ageY + agePlus;
            return v_ageM;
        }
        public int getAgeDay(DateTime v_dob, DateTime v_to)
        {
            double age = 0;
            TimeSpan ts = v_to.Subtract(v_dob);
            DateTime dateAge = DateTime.MinValue + ts;
            double agePlus = 0;
            int v_ageY, v_ageM, v_ageD = 0;
            v_ageY = dateAge.Year - 1;
            v_ageM = dateAge.Month - 1;
            v_ageD = dateAge.Day - 1;
            if (v_ageM != 0 || v_ageD != 0)
                agePlus += 0.1;
            age = v_ageY + agePlus;
            return v_ageD;
        }

        //public double getAge(DateTime birthDate, DateTime laterDate)
        //{
        //    double age;
        //    age = laterDate.Year - birthDate.Year;


        //    //if (age > 0)
        //    //{
        //    //    age -= Convert.ToDouble(laterDate.Date < birthDate.Date.AddYears(age));
        //    //}
        //    //else
        //    //{
        //    //    age = 0;
        //    //}

        //    TimeSpan ts = laterDate.Subtract(birthDate);
        //    DateTime age1 = DateTime.MinValue + ts;
        //    string s = string.Format("{0} Years {1} months {2} days", age1.Year - 1, age1.Month - 1, age1.Day - 1);

        //    return age;
        //}
    }
}
