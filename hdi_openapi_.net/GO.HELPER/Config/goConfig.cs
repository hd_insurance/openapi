﻿using System;
using System.Configuration;

namespace GO.HELPER.Config
{
    public class goConfig
    {
        #region Contructor
        private static readonly Lazy<goConfig> InitInstance = new Lazy<goConfig>(() => new goConfig());
        public static goConfig Instance => InitInstance.Value;
        #endregion

        #region Method
        public string GetConnectionString(string strKey)
        {
            try
            {
                return ConfigurationManager.ConnectionStrings[strKey].ConnectionString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetAppConfig(string strKey)
        {
            try
            {
                return ConfigurationManager.AppSettings[strKey].ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
